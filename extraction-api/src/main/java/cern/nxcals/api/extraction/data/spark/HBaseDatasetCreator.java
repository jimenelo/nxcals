package cern.nxcals.api.extraction.data.spark;

import cern.nxcals.common.domain.ColumnMapping;
import cern.nxcals.common.domain.HBaseExtractionTask;
import cern.nxcals.common.domain.HBaseResource;
import cern.nxcals.common.utils.AvroUtils;
import cern.nxcals.common.utils.ConfigHolder;
import cern.nxcals.common.utils.StreamUtils;
import com.google.common.annotations.VisibleForTesting;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.apache.avro.Schema;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.catalyst.expressions.Expression;
import org.apache.spark.sql.catalyst.expressions.NamedExpression;
import org.apache.spark.sql.execution.datasources.hbase.HBaseTableCatalog;
import org.apache.spark.sql.functions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.StringJoiner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static cern.nxcals.api.extraction.data.ExtractionUtils.getHbaseTypeNameFor;
import static cern.nxcals.api.extraction.data.spark.DatasetCreatorUtils.decodeColumnNames;
import static cern.nxcals.api.extraction.data.spark.DatasetCreatorUtils.getColumnsFrom;
import static cern.nxcals.common.Constants.DATASET_BUILD_PARALLELIZE_ABOVE_SIZE;
import static cern.nxcals.common.Constants.DEFAULT_PARALLELIZE_ABOVE_SIZE;
import static cern.nxcals.common.utils.AvroUtils.getSchemaTypeFor;
import static cern.nxcals.common.utils.AvroUtils.isPrimitiveType;
import static cern.nxcals.common.utils.OptionalUtils.toStream;
import static java.lang.String.format;

@Slf4j
@RequiredArgsConstructor
class HBaseDatasetCreator implements DatasetCreator<HBaseExtractionTask> {
    public static final String CATALOG_FORMAT = "{\"table\": {\"namespace\": \"%s\", \"name\":\"%s\"},%s}";
    private static final String PREFIX = "\"rowkey\":\"id\",\"columns\":{\"entityKey\":{\"cf\":\"rowkey\",\"col\":\"id\",\"type\":\"string\"},";
    private static final String SUFFIX = "}";
    //using old connector still, needs to be updated later (jwozniak)
    @VisibleForTesting
    static final String HBASE_FORMAT = "org.apache.spark.sql.execution.datasources.hbase";
    private static final String KEY_DELIMITER = "__";

    @NonNull
    private final SparkSession session;

    @Override
    public Optional<Dataset<Row>> apply(HBaseExtractionTask task) {
        String[] projection = getColumnsFrom(decodeColumnNames(task.getColumns()));
        Map<String, String> catalog = HBaseCatalogUtils.createTableCatalog(task);

        return StreamUtils.of(task.getPredicates(), ConfigHolder
                .getInt(DATASET_BUILD_PARALLELIZE_ABOVE_SIZE, DEFAULT_PARALLELIZE_ABOVE_SIZE)).map(p -> {
            if (log.isDebugEnabled()) {
                log.debug("Querying with condition: {} and columns: {}", p, Arrays.toString(projection));
            }
            Dataset<Row> rawDataset = this.session.read().format(HBASE_FORMAT).options(catalog).load();
            Dataset<Row> datasetWithDecodedFields = decodeColumnNames(rawDataset);
            return datasetWithDecodedFields
                    .where(functions.expr(p))
                    .selectExpr(projection);
        }).reduce(Dataset::union);
    }

    @UtilityClass
    static class HBaseCatalogUtils {
        public Map<String, String> createTableCatalog(HBaseExtractionTask task) {
            HBaseResource resources = task.getResource();
            Set<ColumnMapping> columnsInSelection = new HashSet<>(task.getColumns());
            columnsInSelection.addAll(getColumnsFromPredicates(task.getPredicates(), resources.getSchema().get()));
            columnsInSelection.addAll(getParentColumns(columnsInSelection, resources.getSchema().get()));
            return tableCatalog(resources, columnsInSelection);
        }

        @VisibleForTesting
        Collection<ColumnMapping> getColumnsFromPredicates(Collection<String> predicates, Schema schema) {
            Collection<Expression> predicateExpressions = predicates.stream()
                    .map(predicate -> functions.expr(predicate).expr()).collect(Collectors.toList());
            Collection<String> columnNamesFromPredicates = predicateExpressions.stream()
                    .flatMap(expression -> getColumnNamesFromExpression(expression).stream())
                    .collect(Collectors.toList());
            return columnNamesFromPredicates.stream()
                    .flatMap(fieldName -> toStream(getColumnMappingFor(fieldName, schema)))
                    .collect(Collectors.toList());
        }

        private Collection<String> getColumnNamesFromExpression(Expression e) {
            return scala.collection.JavaConverters.asJavaCollection(e.references()).stream()
                    .map(NamedExpression::name)
                    .collect(Collectors.toList());
        }

        private Optional<ColumnMapping> getColumnMappingFor(String fieldPath, Schema schema) {
            Optional<Schema.Field> maybeField = AvroUtils.findField(schema, fieldPath);
            return maybeField.map(field -> {
                ColumnMapping mapping = ColumnMapping.fromField(field);
                return mapping.toBuilder().fieldName(fieldPath).build();
            });
        }

        /**
         * Add all parents of passed children, i.e. if there is column mapping for path a.b.c, then mappings for a and a.b
         * will be returned
         *
         * @param childrenColumns columns for which parents mappings should be returned
         * @param schema          dataset schema to obtain schema of parent fields
         * @return parent mappings
         */
        @VisibleForTesting
        Collection<ColumnMapping> getParentColumns(Collection<ColumnMapping> childrenColumns, Schema schema) {
            Stream<String> columnNames = childrenColumns.stream().map(ColumnMapping::getFieldName);
            Stream<String> parentsOfColumns = columnNames.flatMap(name -> generateParentNames(name).stream());
            return parentsOfColumns.flatMap(columnName -> toStream(getColumnMappingFor(columnName, schema)))
                    .collect(Collectors.toList());
        }

        @VisibleForTesting
        List<String> generateParentNames(String path) {
            if (path == null) {
                return Collections.emptyList();
            }
            StringBuilder parentName = new StringBuilder();
            List<String> parentNames = new ArrayList<>();
            List<String> pathParts = Arrays.asList(path.split("\\."));
            for (String pathPart : pathParts.subList(0, pathParts.size() - 1)) {
                parentName.append(pathPart);
                parentNames.add(parentName.toString());
                parentName.append(".");
            }
            return parentNames;
        }

        @VisibleForTesting
        Map<String, String> tableCatalog(HBaseResource resource, Collection<ColumnMapping> fields) {
            String namespace = resource.getNamespace();
            String tableName = resource.getTableName();
            Schema resourceSchema = resource.getSchema().get();
            log.debug("Creating catalog for namespace = {} table = {} with fields = {})", namespace, tableName, fields);

            Map<String, String> catalog = new LinkedHashMap<>();
            StringJoiner header = new StringJoiner(",", PREFIX, SUFFIX);
            catalog.put(HBaseTableCatalog.tableCatalog(), null); // to guarantee that catalog is first

            for (ColumnMapping field : fields) {
                Schema.Field resourceField = resourceSchema.getField(field.getFieldName());
                if (resourceField != null) {
                    header.add(headerRow(tableName, catalog, Pair.of(field, resourceField.schema())));
                }
            }
            catalog.put(HBaseTableCatalog.tableCatalog(), format(CATALOG_FORMAT, namespace, tableName, header));

            if (log.isDebugEnabled()) {
                log.debug("Creating catalog for namespace = {} table = {}: {})", namespace, tableName, catalog);
            }

            return catalog;
        }

        private String headerRow(String tableName, Map<String, String> catalog,
                Pair<ColumnMapping, Schema> mappingAndSchema) {
            ColumnMapping mapping = mappingAndSchema.getKey();
            String name = mapping.getFieldName();
            Schema schema = mappingAndSchema.getValue();
            Schema.Type type = getSchemaTypeFor(schema);
            if (isPrimitiveType(type)) {
                return format("\"%s\":{\"cf\":\"data\",\"col\":\"%s\",\"type\":\"%s\"}", name, name,
                        getHbaseTypeNameFor(schema));
            }
            String schemaName = schemaName(tableName, mapping);
            catalog.put(schemaName, schema.toString());
            return format("\"%s\":{\"cf\":\"data\",\"col\":\"%s\",\"avro\":\"%s\"}", name, name, schemaName);
        }

        private String schemaName(String tableName, ColumnMapping field) {
            return tableName + KEY_DELIMITER + field.getQualifiedName().toLowerCase();
        }

    }

}
