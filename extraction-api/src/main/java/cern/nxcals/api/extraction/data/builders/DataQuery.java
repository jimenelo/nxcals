/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.api.extraction.data.builders;

import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.EntityQuery;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.extraction.data.builders.fluent.QueryData;
import cern.nxcals.api.extraction.data.builders.fluent.v2.KeyValueStage;
import cern.nxcals.api.extraction.data.builders.fluent.v2.KeyValueStageLoop;
import cern.nxcals.api.extraction.data.builders.fluent.v2.VariableStageLoop;
import cern.nxcals.api.extraction.metadata.ServiceClientFactory;
import cern.nxcals.api.extraction.metadata.VariableService;
import cern.nxcals.api.extraction.metadata.queries.Variables;
import cern.nxcals.common.annotation.Experimental;
import cern.nxcals.common.utils.MapUtils;
import com.github.rutledgepaulv.qbuilders.conditions.Condition;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.Lists;
import lombok.NonNull;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.functions;
import org.apache.spark.sql.types.StructField;
import scala.collection.JavaConverters;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static cern.nxcals.common.SystemFields.NXC_EXTR_TIMESTAMP;
import static cern.nxcals.common.SystemFields.NXC_EXTR_VALUE;
import static cern.nxcals.common.SystemFields.NXC_EXTR_VARIABLE_NAME;
import static java.util.Arrays.asList;

public class DataQuery extends LegacyBuildersDataQuery {
    private DataQuery(SparkSession session) {
        super(session);
    }

    public static DataQuery builder(@NonNull SparkSession session) {
        return new DataQuery(session);
    }

    public static Dataset<Row> getFor(@NonNull SparkSession sparkSession, @NonNull TimeWindow timeWindow,
            @NonNull Variable variable) {
        return DataQuery.getFor(sparkSession, timeWindow, variable.getSystemSpec().getName(),
                variable.getVariableName());
    }

    public static Dataset<Row> getFor(@NonNull SparkSession sparkSession, @NonNull TimeWindow timeWindow,
            @NonNull Entity entity) {
        return DataQuery.getFor(sparkSession, timeWindow, entity.getSystemSpec().getName(),
                new EntityQuery(entity.getEntityKeyValues()));
    }

    @SafeVarargs
    public static Dataset<Row> getFor(@NonNull SparkSession spark, @NonNull TimeWindow timeWindow,
            @NonNull String system, @NonNull Map<String, Object>... keyValuesArr) {
        return DataQuery.getFor(spark, timeWindow, system,
                Arrays.stream(keyValuesArr).map(EntityQuery::new).toArray(EntityQuery[]::new));
    }

    public static Dataset<Row> getFor(@NonNull SparkSession spark, @NonNull TimeWindow timeWindow,
            @NonNull String system, @NonNull EntityQuery... entitiesQueries) {
        KeyValueStage<Dataset<Row>> dataQuery = DataQuery.builder(spark)
                .entities().system(system);
        KeyValueStageLoop<Dataset<Row>> entities = null;
        for (EntityQuery query : entitiesQueries) {
            entities = query.hasPatterns() ?
                    dataQuery.keyValuesLike(query.toMap()) :
                    dataQuery.keyValuesEq(query.getKeyValues());
        }
        if (entities == null) {
            throw new IllegalArgumentException("No entity query passed");
        }
        return entities.timeWindow(timeWindow).build();
    }

    public static Dataset<Row> getFor(@NonNull SparkSession spark, @NonNull TimeWindow timeWindow,
            @NonNull String system, @NonNull String... variables) {
        return DataQuery.getFor(spark, timeWindow, system, asList(variables));
    }

    public static Dataset<Row> getFor(@NonNull SparkSession spark, @NonNull TimeWindow timeWindow,
            @NonNull String system, @NonNull List<String> variables) {
        return DataQuery.getFor(spark, timeWindow, system, variables, Collections.emptyList());
    }

    public static Dataset<Row> getFor(@NonNull SparkSession spark, @NonNull TimeWindow timeWindow,
            @NonNull String system, @NonNull List<String> variables,
            @NonNull List<String> variablesLike) {
        if (variables.isEmpty() && variablesLike.isEmpty()) {
            throw new IllegalArgumentException("No variable names nor variable name patterns given");
        }
        VariableStageLoop<Dataset<Row>> builder = DataQuery.builder(spark).variables()
                .system(system)
                .nameIn(variables);

        variablesLike.forEach(builder::nameLike);

        return builder.timeWindow(timeWindow).build();
    }

    /**
     * Create a dataset with variable names as columns. Values are joined on timestamps. Experimental, may be changed in the future.
     *
     * @param spark      - active spark session
     * @param timeWindow - for data extraction
     * @param variables  - requested variables
     * @return - dataset with one column "nxcals_timestamp" and other columns named as variables
     */
    @Experimental
    public static Dataset<Row> getAsPivot(@NonNull SparkSession spark, @NonNull TimeWindow timeWindow,
            @NonNull Collection<Variable> variables) {
        if (variables.isEmpty()) {
            throw new IllegalArgumentException("No variables given");
        }

        Map<String, Dataset<Row>> datasetByVariable = getDatasetForVariables(spark, timeWindow, variables);

        Map<StructField, List<Map.Entry<String, Dataset<Row>>>> groupedByType = MapUtils.groupBy(datasetByVariable,
                datasetWithVariableName -> getVariableType(datasetWithVariableName.getValue()));

        List<Dataset<Row>> pivoted = new ArrayList<>();

        for (List<Map.Entry<String, Dataset<Row>>> datasets : groupedByType.values()) {
            List<Object> variableNames = Lists.transform(datasets, Map.Entry::getKey);

            Dataset<Row> union = datasets.stream().map(Map.Entry::getValue).reduce(Dataset::union)
                    .orElseThrow(() -> new RuntimeException("No dataset to union"));

            Dataset<Row> pivot = doPivotOnVariable(union, variableNames);

            pivoted.add(pivot);
        }

        return joinOnTimestamp(pivoted)
                .orElseThrow(() -> new RuntimeException("No datasets to join"));
    }

    /**
     * Create a dataset with variable names as columns. Values are joined on timestamps. Experimental, may be changed in the future.
     *
     * @param spark          - active spark session
     * @param timeWindow     - for data extraction
     * @param system         - system where variables are registered
     * @param variableNames- requested variables
     * @return - dataset with one column "nxcals_timestamp" and other columns named as variables
     */
    @Experimental
    public static Dataset<Row> getAsPivot(@NonNull SparkSession spark, @NonNull TimeWindow timeWindow,
            @NonNull String system, @NonNull Collection<String> variableNames) {
        VariableService service = ServiceClientFactory.createVariableService();
        Condition<Variables> condition = Variables.suchThat().systemName().eq(system).and().variableName()
                .in(variableNames);
        Set<Variable> variables = service.findAll(condition);
        return getAsPivot(spark, timeWindow, variables);
    }

    @VisibleForTesting
    static Map<String, Dataset<Row>> getDatasetForVariables(SparkSession spark, TimeWindow timeWindow,
            Collection<Variable> variables) {
        return variables.stream()
                .collect(Collectors.toMap(Variable::getVariableName, variable -> getFor(spark, timeWindow, variable)));
    }

    @VisibleForTesting
    static StructField getVariableType(Dataset<Row> dataset) {
        return dataset.schema().apply(NXC_EXTR_VALUE.getValue());
    }

    @VisibleForTesting
    static Dataset<Row> doPivotOnVariable(Dataset<Row> dataset, List<Object> variableNames) {
        return dataset.groupBy(dataset.col(NXC_EXTR_TIMESTAMP.getValue()))
                .pivot(dataset.col(NXC_EXTR_VARIABLE_NAME.getValue()),
                        JavaConverters.asScalaBuffer(variableNames).toList())
                .agg(functions.any_value(dataset.col(NXC_EXTR_VALUE.getValue())));
    }

    @VisibleForTesting
    static Optional<Dataset<Row>> joinOnTimestamp(Collection<Dataset<Row>> datasets) {
        return datasets.stream().reduce((x, y) ->
                x.join(y, NXC_EXTR_TIMESTAMP.getValue(), "fullouter"));
    }

    protected QueryData<Dataset<Row>> queryData() {
        return new QueryData<>(new SparkDatasetProducer(session));
    }

}
