package cern.nxcals.api.extraction.data.builders;

import cern.nxcals.api.domain.TimeWindow;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

@NoArgsConstructor(access = AccessLevel.PACKAGE)
@SuppressWarnings("java:S1610")
abstract class DevicePropertyStaticMethods {
    /**
     * Search for given device/property in given system within time window.
     *
     * @param sparkSession spark session
     * @param timeWindow   time window to query for
     * @param system       system name (CMW, PM ...)
     * @param device       device name
     * @param property     property name
     * @return Dataset with result in {@link Row}
     */
    //@deprecated use {@link DevicePropertyStaticMethods#getFor(SparkSession, String, String, String, TimeWindow)} instead
    //    @Deprecated
    public static Dataset<Row> getFor(@NonNull SparkSession sparkSession, @NonNull TimeWindow timeWindow,
            @NonNull String system, @NonNull String device, @NonNull String property) {
        return getFor(sparkSession, system, device, property, timeWindow);
    }

    /**
     * Search for given device/property in given system within time window.
     *
     * @param sparkSession spark session
     * @param system       system name (CMW, PM ...)
     * @param device       device name
     * @param property     property name
     * @param timeWindow   time window to query for
     * @return Dataset with result in {@link Row}
     */
    public static Dataset<Row> getFor(@NonNull SparkSession sparkSession, @NonNull String system,
            @NonNull String device, @NonNull String property, @NonNull TimeWindow timeWindow) {
        return ParameterDataQuery.builder(sparkSession)
                .system(system)
                .deviceEq(device)
                .propertyEq(property)
                .timeWindow(timeWindow)
                .build();
    }

    /**
     * Query for given parameter and return result dataset
     *
     * @param sparkSession spark session
     * @param system       system name
     * @param parameter    in form device/property
     * @param timeWindow   time window for query
     * @return result dataset
     */
    public static Dataset<Row> getFor(@NonNull SparkSession sparkSession, @NonNull String system,
            @NonNull String parameter, @NonNull TimeWindow timeWindow) {
        return ParameterDataQuery.builder(sparkSession)
                .system(system)
                .parameterEq(parameter)
                .timeWindow(timeWindow)
                .build();
    }
}
