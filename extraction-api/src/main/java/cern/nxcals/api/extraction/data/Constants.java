/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.api.extraction.data;

import cern.nxcals.common.utils.GlobalObjectMapperProvider;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import lombok.experimental.UtilityClass;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

/**
 * Holder class for constants common to both frontend (builders) and backend (api) layers.
 */
@UtilityClass
public class Constants {
    // TODO: pretty much all of this is never used
    private static final TypeFactory TYPE_FACTORY = GlobalObjectMapperProvider.get().getTypeFactory();
    private static final JavaType STRING_JAVA_TYPE = TYPE_FACTORY.constructType(String.class);
    private static final JavaType BOOLEAN_JAVA_TYPE = TYPE_FACTORY.constructType(Boolean.class);

    public static final JavaType KEY_VALUES_TYPE = TYPE_FACTORY.constructCollectionType(HashSet.class,
            TYPE_FACTORY.constructMapType(HashMap.class, STRING_JAVA_TYPE, STRING_JAVA_TYPE));
    private static final JavaType ALIASES_VALUE_TYPE = TYPE_FACTORY.constructCollectionType(ArrayList.class, String.class);
    public static final JavaType ALIASES_TYPE = TYPE_FACTORY.constructMapType(HashMap.class, STRING_JAVA_TYPE, ALIASES_VALUE_TYPE);
    public static final JavaType FIELDS_TYPE = TYPE_FACTORY.constructCollectionType(ArrayList.class, STRING_JAVA_TYPE);
    public static final JavaType VARIABLE_TYPE = TYPE_FACTORY.constructMapType(HashMap.class, STRING_JAVA_TYPE, BOOLEAN_JAVA_TYPE);

    public static final String SYSTEM_KEY = "system";
    public static final String START_TIME_KEY = "start_time";
    public static final String END_TIME_KEY = "end_time";
    public static final String KEY_VALUES_KEY = "key_values";
    public static final String KEY_VALUES_WITH_WILDCARDS_KEY = "key_values_with_wildcards";
    public static final String FIELDS_KEY = "fields_key";
    public static final String ALIASES_KEY = "aliases_key";
    public static final String VARIABLE_KEY = "variable";

    // With default JVM settings for stack size we can handle until 100 column expressions (spark 'OR' statement branches)
    // per each Spark dataset SQL statement
    // Please look here: https://issues.apache.org/jira/browse/SPARK-21720
    public static final int SPARK_COLUMN_CONDITION_MAX_SIZE = 100;
}
