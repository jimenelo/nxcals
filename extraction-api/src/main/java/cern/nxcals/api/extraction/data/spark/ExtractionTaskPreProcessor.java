package cern.nxcals.api.extraction.data.spark;

import cern.nxcals.common.domain.ColumnMapping;
import cern.nxcals.common.domain.ExtractionTask;
import cern.nxcals.common.domain.ExtractionTaskProcessor;
import cern.nxcals.common.domain.ExtractionUnit;
import cern.nxcals.common.domain.HBaseExtractionTask;
import cern.nxcals.common.domain.HdfsExtractionTask;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.apache.spark.sql.functions;

import static cern.nxcals.api.extraction.data.ExtractionUtils.STRING_SCHEMA;
import static cern.nxcals.common.SystemFields.NXC_EXTR_VARIABLE_FIELD_NAME;

@RequiredArgsConstructor
class ExtractionTaskPreProcessor implements ExtractionTaskProcessor<ExtractionTask> {
    @NonNull
    private final ExtractionUnit unit;

    @Override
    public ExtractionTask execute(@NonNull HBaseExtractionTask task) {
        if (unit.isVariableSearch()) {
            ColumnMapping column = getFieldMapping(task.getVariableFieldBoundColumnMapping());
            return task.toBuilder().column(column).build();
        }
        return task;
    }

    @Override
    public ExtractionTask execute(@NonNull HdfsExtractionTask task) {
        if (unit.isVariableSearch()) {
            ColumnMapping column = getFieldMapping(task.getVariableFieldBoundColumnMapping());
            return task.toBuilder().column(column).build();
        }
        return task;
    }

    private ColumnMapping getFieldMapping(ColumnMapping fieldMapping) {
        String fieldName = fieldMapping != null ? "'" + functions.lit(fieldMapping.getFieldName()) + "'" : null;
        //@formatter:off
        return ColumnMapping.builder()
                .fieldName(fieldName)
                .schemaJson(STRING_SCHEMA.toString())
                .alias(NXC_EXTR_VARIABLE_FIELD_NAME.getValue()).build();
        //@formatter:on
    }

}
