package cern.nxcals.api.authorization;

import cern.nxcals.api.extraction.data.builders.fluent.QueryData;
import com.google.common.annotations.VisibleForTesting;
import lombok.extern.slf4j.Slf4j;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.security.Credentials;
import org.apache.hadoop.security.token.TokenIdentifier;
import org.apache.hadoop.security.token.delegation.AbstractDelegationTokenIdentifier;
import org.apache.spark.SparkConf;
import org.apache.spark.security.HadoopDelegationTokenProvider;
import scala.Option;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Stream;

/**
 * We need to obtain initial delegation tokens <b><i>before</i></b> Spark tries to communicate with Hadoop.
 * <p>
 * In YARN, this static block will be executed when a new instance of RbacHadoopDelegationTokenProvider
 * is created by the scheduler during SparkSession creation
 * <p>
 * In local, Spark does not trigger execution of static block in this class,
 * therefore we need to call {@link #obtainAndSetDelegationTokensViaRbacIfRequired()} method
 * from {@link cern.nxcals.api.extraction.data.builders.SparkDatasetProducer#apply(QueryData)}
 * to ensure that tokens are obtained before the dataset built
 * <p>
 * Please, note that delegation tokens are created only ONCE during SparkSession creation (in YARN)
 * or during the first call to get Dataset (in local).
 * Any further update of the RBAC user won't cause new tokens generation.
 * I.e. only one user can be authenticated per application.
 */
@Slf4j
public class RbacHadoopDelegationTokenProvider implements HadoopDelegationTokenProvider {

    static final String FEATURE_TOGGLE = "NXCALS_RBAC_AUTH";

    private static final RbacAuthService RBAC_AUTH_SERVICE = new RbacAuthService();

    static {
        obtainAndSetDelegationTokensViaRbacIfRequired();
    }

    public static void obtainAndSetDelegationTokensViaRbacIfRequired() {
        if (isEnabled()) {
            RBAC_AUTH_SERVICE.obtainAndSetDelegationTokensViaRbacIfRequired();
        }
    }

    @VisibleForTesting
    static boolean isEnabled() {
        return Stream.<Function<String, String>>of(System::getProperty, System::getenv)
                .anyMatch(getter -> Boolean.parseBoolean(getter.apply(FEATURE_TOGGLE)));
    }

    @Override
    public String serviceName() {
        return "nxcals";
    }

    @Override
    public boolean delegationTokensRequired(SparkConf sparkConf, Configuration hadoopConf) {
        return isEnabled();
    }

    @Override
    public Option<Object> obtainDelegationTokens(Configuration hadoopConf, SparkConf sparkConf, Credentials creds) {
        RBAC_AUTH_SERVICE.obtainDelegationTokens(creds);

        Optional<Long> earliest = creds.getAllTokens().stream().flatMap(token -> {
            TokenIdentifier identifier;
            try {
                identifier = token.decodeIdentifier();
            } catch (IOException e) {
                throw new UncheckedIOException("Could not decode identifier", e);
            }

            if (!(identifier instanceof AbstractDelegationTokenIdentifier)) {
                return Stream.empty();
            }
            AbstractDelegationTokenIdentifier delegationIdentifier = (AbstractDelegationTokenIdentifier) identifier;

            return Stream.of(delegationIdentifier.getMaxDate());
        }).min(Long::compare);

        if (!earliest.isPresent()) {
            return Option.empty();
        }
        return Option.apply(earliest.get());
    }

}
