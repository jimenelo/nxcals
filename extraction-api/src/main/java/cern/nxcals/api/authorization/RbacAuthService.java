package cern.nxcals.api.authorization;

import cern.nxcals.api.custom.domain.DelegationToken;
import cern.nxcals.api.extraction.metadata.InternalServiceClientFactory;
import cern.nxcals.common.utils.Lazy;
import cern.nxcals.internal.extraction.metadata.InternalAuthorizationService;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.hadoop.security.Credentials;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.UncheckedIOException;

@Slf4j
@RequiredArgsConstructor(access = AccessLevel.PROTECTED)
public class RbacAuthService {

    private final Lazy<InternalAuthorizationService> authorizationService;
    private final RbacManager rbacManager;
    private final UgiManager ugiManager;
    private volatile boolean alreadyExecuted = false;

    protected RbacAuthService() {
        this(new Lazy<>(InternalServiceClientFactory::createAuthorizationService), new RbacManager(), new UgiManager());
    }

    protected synchronized void obtainAndSetDelegationTokensViaRbacIfRequired() {
        log.debug("Obtain delegation token via Rbac if required.");
        try {
            if (!alreadyExecuted) {
                log.debug("Delegation token is required. Obtaining it via RBAC.");
                obtainAndSetDelegationTokensViaRbac();
                alreadyExecuted = true;
            }
        } catch (Exception | ExceptionInInitializerError e) {
            // Spark logs this only at debug level
            log.error("Failed to initialize", e);
            throw e;
        }
    }

    private void obtainAndSetDelegationTokensViaRbac() {
        rbacManager.setupRbacIfMissing();

        // creates token for the user that contacts authorization service (user is from RBAC in this case)
        Credentials credentials = new Credentials();
        obtainDelegationTokens(credentials);

        // creates and sets UGI that has the same username as in the RBAC token to avoid access issues for .sparkStaging dirs
        String userName = rbacManager.findRbaToken().getUser().getName();
        ugiManager.createAndSetUser(userName, credentials);

        log.info("Delegation tokens successfully obtained via RBAC and added to \"{}\" user", userName);
    }

    protected void obtainDelegationTokens(Credentials credentials) {
        log.info("Creating Hadoop delegation tokens");
        DelegationToken delegationToken = authorizationService.get().createDelegationToken();
        byte[] tokenBytes = delegationToken.getStorage();

        try (DataInputStream stream = new DataInputStream(new ByteArrayInputStream(tokenBytes))) {
            credentials.readTokenStorageStream(stream);
        } catch (IOException e) {
            throw new UncheckedIOException("Could not read delegation tokens", e);
        }
    }
}
