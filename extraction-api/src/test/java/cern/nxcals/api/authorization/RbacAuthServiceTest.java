package cern.nxcals.api.authorization;

import cern.nxcals.api.custom.domain.DelegationToken;
import cern.nxcals.common.utils.Lazy;
import cern.nxcals.internal.extraction.metadata.InternalAuthorizationService;
import org.apache.hadoop.security.Credentials;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.UncheckedIOException;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class RbacAuthServiceTest {

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private RbacManager rbacManagerMock;

    @Mock
    private UgiManager ugiManagerMock;

    @Mock
    private InternalAuthorizationService authorizationService;

    private RbacAuthService rbacAuthService;

    @BeforeEach()
    public void init() {
        rbacAuthService = new RbacAuthService(new Lazy<>(() -> authorizationService), rbacManagerMock, ugiManagerMock);
    }

    @Test
    void shouldThrowIfTokenStorageIsMalformed() {
        //given
        DelegationToken delegationToken = DelegationToken.builder().withStorage(new byte[] { 1 }).build();
        when(authorizationService.createDelegationToken()).thenReturn(delegationToken);
        Credentials credentials = new Credentials();
        //when
        assertThrows(UncheckedIOException.class, () -> rbacAuthService.obtainDelegationTokens(credentials));
    }

    @Test
    void shouldObtainDelegationTokens() throws IOException {
        //given
        byte[] tokenStorage = new byte[] { 1 };
        DelegationToken delegationToken = DelegationToken.builder().withStorage(tokenStorage).build();
        when(authorizationService.createDelegationToken()).thenReturn(delegationToken);
        Credentials credentials = mock(Credentials.class);
        ArgumentCaptor<DataInputStream> argumentCaptor = ArgumentCaptor.forClass(DataInputStream.class);
        //when
        rbacAuthService.obtainDelegationTokens(credentials);
        //then
        verify(credentials).readTokenStorageStream(argumentCaptor.capture());
        assertArrayEquals(tokenStorage, argumentCaptor.getValue().readAllBytes());
    }

    @Test
    void shouldObtainAndSetDelegationTokensViaRbacIfRequired() {
        //given
        RbacAuthService rbacAuthServiceSpy = spy(rbacAuthService);
        String userName = "testUser";
        when(rbacManagerMock.findRbaToken().getUser().getName()).thenReturn(userName);
        ArgumentCaptor<Credentials> credentialsCaptor = ArgumentCaptor.forClass(Credentials.class);
        doNothing().when(rbacAuthServiceSpy).obtainDelegationTokens(credentialsCaptor.capture());
        //when
        rbacAuthServiceSpy.obtainAndSetDelegationTokensViaRbacIfRequired();
        //then
        verify(ugiManagerMock).createAndSetUser(userName, credentialsCaptor.getValue());
    }

    @Test
    void shouldObtainAndSetDelegationTokensViaRbacOnlyOnce() {
        //given
        RbacAuthService rbacAuthServiceSpy = spy(rbacAuthService);
        String userName = "testUser";
        when(rbacManagerMock.findRbaToken().getUser().getName()).thenReturn(userName);
        doNothing().when(rbacAuthServiceSpy).obtainDelegationTokens(any());
        //when
        rbacAuthServiceSpy.obtainAndSetDelegationTokensViaRbacIfRequired();
        rbacAuthServiceSpy.obtainAndSetDelegationTokensViaRbacIfRequired();
        //then
        verify(rbacAuthServiceSpy, times(1)).obtainDelegationTokens(any());
        verify(ugiManagerMock, times(1)).createAndSetUser(eq(userName), any());
    }
}