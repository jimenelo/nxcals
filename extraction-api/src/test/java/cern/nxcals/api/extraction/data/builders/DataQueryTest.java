package cern.nxcals.api.extraction.data.builders;

import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.EntityQuery;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.extraction.metadata.ServiceClientFactory;
import cern.nxcals.api.extraction.metadata.VariableService;
import cern.nxcals.common.SystemFields;
import cern.nxcals.common.testutils.CartesianProduct;
import com.github.rutledgepaulv.qbuilders.conditions.Condition;
import org.apache.commons.compress.utils.Sets;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructType;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.MockedStatic;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.when;

class DataQueryTest {

    private static final SparkSession[] sparkSessions = { mock(SparkSession.class), null };
    private static final TimeWindow[] timeWindows = {
            TimeWindow.between(Instant.ofEpochSecond(10), Instant.ofEpochSecond(50)), null };
    private static final String[] systems = { "SYSTEM", null };
    private static final Variable[] variables = { mock(Variable.class), null };
    private static final Entity[] entities = { mock(Entity.class), null };
    private static final String[] variablesNames = { "var", null };
    private static final ArrayList<Map<String, Object>> keyValues = new ArrayList<>(0);

    static {
        keyValues.add(Map.of("var", "var"));
        keyValues.add(null);
    }

    private static Stream<Arguments> getForVariableNPETestArgs() {
        return CartesianProduct.compose(sparkSessions, timeWindows, variables);
    }

    private static Stream<Arguments> getForEntityNPETestArgs() {
        return CartesianProduct.compose(sparkSessions, timeWindows, entities);
    }

    private static Stream<Arguments> getForVariableNameNPETestArgs() {
        return CartesianProduct.compose(sparkSessions, timeWindows, systems, variablesNames);
    }

    private static Stream<Arguments> getEntityWithKeyValueNPETestArgs() {
        return CartesianProduct.compose(sparkSessions, timeWindows, systems, keyValues.toArray());
    }

    private static Stream<Arguments> getEntityWithKeysInMapNPETestArgs() {
        return CartesianProduct.compose(sparkSessions, timeWindows, systems, keyValues.toArray(),
                keyValues.toArray());
    }

    @ParameterizedTest
    @MethodSource("getForVariableNPETestArgs")
    void shouldThrowNPEIfOneOfParamsIsNullOnGetForVariable(SparkSession session, TimeWindow timeWindow,
            Variable variable) {
        if (Stream.of(session, timeWindow, variable).anyMatch(Objects::isNull)) {
            assertThrows(NullPointerException.class,
                    () -> DataQuery.getFor(session, timeWindow, variable));
        }
    }

    @ParameterizedTest
    @MethodSource("getForEntityNPETestArgs")
    void shouldThrowNPEIfOneOfParamsIsNullOnGetForEntity(SparkSession session, TimeWindow timeWindow,
            Entity entity) {
        if (Stream.of(session, timeWindow, entity).anyMatch(Objects::isNull)) {
            assertThrows(NullPointerException.class,
                    () -> DataQuery.getFor(session, timeWindow, entity));
        }
    }

    @ParameterizedTest
    @MethodSource("getForVariableNameNPETestArgs")
    void shouldThrowNPEIfOneOfParametersIsNullOnGetForSingleVariableName(SparkSession sparkSession,
            TimeWindow timeWindow, String system, String variable) {
        if (Stream.of(sparkSession, timeWindow, system, variable).anyMatch(Objects::isNull)) {
            assertThrows(NullPointerException.class,
                    () -> DataQuery.getFor(sparkSession, timeWindow, system, variable));
        }
    }

    @ParameterizedTest
    @MethodSource("getForVariableNameNPETestArgs")
    void shouldThrowNPEIfOneOfParametersIsNullOnGetForMultipleVariableNames(SparkSession sparkSession,
            TimeWindow timeWindow, String system, String variable) {
        if (Stream.of(sparkSession, timeWindow, system, variable).anyMatch(Objects::isNull)) {
            assertThrows(NullPointerException.class,
                    () -> DataQuery.getFor(sparkSession, timeWindow, system, "name", variable));
        }
    }

    @ParameterizedTest
    @MethodSource("getForVariableNameNPETestArgs")
    void shouldThrowNPEIfOneOfParametersIsNullOnGetForListNames(SparkSession sparkSession, TimeWindow timeWindow,
            String system, String variable) {
        if (Stream.of(sparkSession, timeWindow, system, variable).anyMatch(Objects::isNull)) {
            assertThrows(NullPointerException.class,
                    () -> DataQuery.getFor(sparkSession, timeWindow, system, toList(variable)));
        }
    }

    @ParameterizedTest
    @MethodSource("getForVariableNameNPETestArgs")
    void shouldThrowNPEIfOneOfParametersIsNull(SparkSession sparkSession,
            TimeWindow timeWindow, String system, String variable) {
        if (Stream.of(sparkSession, timeWindow, system, variable).anyMatch(Objects::isNull)) {
            assertThrows(NullPointerException.class,
                    () -> DataQuery.getFor(sparkSession, timeWindow, system, List.of("VAR_NAME"), toList(variable)));
        }
    }

    @Test
    void shouldIllegalArgumentIfVariablesListIsEmpty() {
        TimeWindow timeWindow = TimeWindow.between(0, 1);
        SparkSession sessionMock = mock(SparkSession.class);
        assertThrows(IllegalArgumentException.class,
                () -> DataQuery.getFor(sessionMock, timeWindow, "SYSTEM", List.of()));
    }

    @ParameterizedTest
    @MethodSource("getEntityWithKeysInMapNPETestArgs")
    void getForWithKeyValuesAsMapShouldAndRestOfParamsThrowNPEIfOneOfParametersIsNull(SparkSession sparkSession,
            TimeWindow timeWindow, String system, Map<String, Object> keyValue, Map<String, Object> keyValueLike) {
        if (Stream.of(sparkSession, timeWindow, system, keyValue, keyValueLike).anyMatch(Objects::isNull)) {
            assertThrows(NullPointerException.class,
                    () -> DataQuery.getFor(sparkSession, timeWindow, system, new EntityQuery(keyValue, keyValueLike)));
        }
    }

    @ParameterizedTest
    @MethodSource("getEntityWithKeyValueNPETestArgs")
    void shouldThrowNPEIfOneOfParametersIsNullWithKeyValuesAsMap(SparkSession sparkSession,
            TimeWindow timeWindow, String system, Map<String, Object> keyValue) {
        if (Stream.of(sparkSession, timeWindow, system, keyValue).anyMatch(Objects::isNull)) {
            assertThrows(NullPointerException.class,
                    () -> DataQuery.getFor(sparkSession, timeWindow, system, keyValue));
        }
    }

    @Test
    void shouldThrowIllegalArgumentIfKeyValueMapIsEmpty() {
        TimeWindow timeWindow = TimeWindow.between(0, 1);
        SparkSession sessionMock = mock(SparkSession.class);
        assertThrows(IllegalArgumentException.class,
                () -> DataQuery.getFor(sessionMock, timeWindow, "SYSTEM", new HashMap<>()));
    }

    @Nested
    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    class PivotTest {
        final String timestampField = SystemFields.NXC_EXTR_TIMESTAMP.getValue();
        final String variableValueField = SystemFields.NXC_EXTR_VALUE.getValue();
        final String variableNameField = SystemFields.NXC_EXTR_VARIABLE_NAME.getValue();
        final String entityIdField = SystemFields.NXC_ENTITY_ID.getValue();
        SparkSession sparkSession;
        StructType schemaWithInt = DataTypes.createStructType(Arrays.asList(
                DataTypes.createStructField(timestampField, DataTypes.StringType, false),
                DataTypes.createStructField(variableNameField, DataTypes.StringType, false),
                DataTypes.createStructField(variableValueField, DataTypes.IntegerType, false),
                DataTypes.createStructField(entityIdField, DataTypes.LongType, false)
        ));

        StructType schemaWithString = DataTypes.createStructType(Arrays.asList(
                DataTypes.createStructField(timestampField, DataTypes.StringType, false),
                DataTypes.createStructField(variableNameField, DataTypes.StringType, false),
                DataTypes.createStructField(variableValueField, DataTypes.StringType, false),
                DataTypes.createStructField(entityIdField, DataTypes.LongType, false)
        ));

        @BeforeAll
        void setup() {
            sparkSession = SparkSession.builder()
                    .master("local[*]")
                    .appName("Test")
                    .getOrCreate();
        }

        @Test
        void shouldThrowIfNoVariablePassed() {
            assertThrows(IllegalArgumentException.class,
                    () -> DataQuery.getAsPivot(mock(SparkSession.class), TimeWindow.infinite(),
                            Collections.emptyList()));
        }

        @Test
        void testDoingPivotOnMultipleVariables() {
            // given
            List<String> variableNames = Arrays.asList("var1", "var2", "var3", "var4", "var5");
            List<Row> var1Rows = Arrays.asList(
                    RowFactory.create("2024-04-12", "var1", 1, 0L),
                    RowFactory.create("2024-04-13", "var1", 4, 0L)
            );

            List<Row> var2Rows = Arrays.asList(
                    RowFactory.create("2024-04-12", "var2", 2, 1L)
            );

            List<Row> var3Rows = Arrays.asList(
                    RowFactory.create("2024-04-12", "var3", "3", 2L),
                    RowFactory.create("2024-04-13", "var3", "6", 3L)
            );

            List<Row> var4Rows = Arrays.asList(
                    RowFactory.create("2024-04-14", "var4", 10, 0L),
                    RowFactory.create("2024-04-15", "var4", 20, 0L)
            );

            List<Variable> variables = variableNames.stream()
                    .map(variableName -> Variable.builder().variableName(variableName).systemSpec(mock(
                            SystemSpec.class)).build()).collect(Collectors.toList());

            withMockedDataQuery(mocked -> {
                mocked.when(() -> DataQuery.getFor(any(), any(), eq(variables.get(0))))
                        .thenReturn(sparkSession.createDataFrame(var1Rows, schemaWithInt));

                mocked.when(() -> DataQuery.getFor(any(), any(), eq(variables.get(1))))
                        .thenReturn(sparkSession.createDataFrame(var2Rows, schemaWithInt));

                mocked.when(() -> DataQuery.getFor(any(), any(), eq(variables.get(2))))
                        .thenReturn(sparkSession.createDataFrame(var3Rows, schemaWithString));

                mocked.when(() -> DataQuery.getFor(any(), any(), eq(variables.get(3))))
                        .thenReturn(sparkSession.createDataFrame(var4Rows, schemaWithInt));

                mocked.when(() -> DataQuery.getFor(any(), any(), eq(variables.get(4))))
                        .thenReturn(sparkSession.createDataFrame(Collections.emptyList(), schemaWithInt));

                // when
                Dataset<Row> result = DataQuery.getAsPivot(sparkSession, TimeWindow.infinite(), variables);

                // then
                checkColumns(result, timestampField, "var1", "var2", "var3", "var4", "var5");

                List<Row> rows = result.orderBy(timestampField).collectAsList();
                assertEquals(4, rows.size());
                Row row = rows.get(0);

                assertEquals("2024-04-12", row.getString(row.fieldIndex(timestampField)));
                assertEquals(1, row.getInt(row.fieldIndex("var1")));
                assertEquals(2, row.getInt(row.fieldIndex("var2")));
                assertEquals("3", row.getString(row.fieldIndex("var3")));
                assertNull(row.get(row.fieldIndex("var4")));
                assertNull(row.get(row.fieldIndex("var5")));
                row = rows.get(1);
                assertEquals("2024-04-13", row.getString(row.fieldIndex(timestampField)));
                assertEquals(4, row.getInt(row.fieldIndex("var1")));
                assertNull(row.get(row.fieldIndex("var2")));
                assertEquals("6", row.getString(row.fieldIndex("var3")));
                assertNull(row.get(row.fieldIndex("var4")));
                assertNull(row.get(row.fieldIndex("var5")));
                row = rows.get(2);
                assertEquals(10, row.getInt(row.fieldIndex("var4")));
                for (String columnName : List.of("var1", "var2", "var3", "var5")) {
                    assertNull(row.get(row.fieldIndex(columnName)));
                }
                row = rows.get(3);
                assertEquals(20, row.getInt(row.fieldIndex("var4")));
                for (String columnName : List.of("var1", "var2", "var3", "var5")) {
                    assertNull(row.get(row.fieldIndex(columnName)));
                }
            });
        }

        @Test
        void testDoingPivotOnMultipleVariablesAndEmptyDataset() {
            // given
            List<String> variableNames = Arrays.asList("var1", "var2", "var3");
            List<Row> emptyTestData = Collections.emptyList();

            Dataset<Row> emptyDataframe = sparkSession.createDataFrame(emptyTestData, schemaWithInt);

            List<Variable> variables = variableNames.stream()
                    .map(variableName -> Variable.builder().variableName(variableName).systemSpec(mock(
                            SystemSpec.class)).build()).collect(Collectors.toList());

            withMockedDataQuery(mocked -> {
                mocked.when(() -> DataQuery.getFor(any(), any(), any(Variable.class)))
                        .thenReturn(emptyDataframe);

                // when
                Dataset<Row> result = DataQuery.getAsPivot(sparkSession, TimeWindow.infinite(), variables);

                // then
                checkColumns(result, timestampField, "var1", "var2", "var3");
                assertTrue(result.isEmpty());
                assertEquals(DataTypes.StringType, result.schema().apply(timestampField).dataType());
                assertEquals(DataTypes.IntegerType, result.schema().apply("var1").dataType());
                assertEquals(DataTypes.IntegerType, result.schema().apply("var2").dataType());
                assertEquals(DataTypes.IntegerType, result.schema().apply("var3").dataType());
            });
        }

        @Test
        void testDoingPivotOnOneVariable() {
            // given
            List<String> variableNames = Collections.singletonList("var1");
            List<Row> singleVariableTestData = Collections.singletonList(
                    RowFactory.create("2024-04-12", "var1", 1, 1L)
            );
            Dataset<Row> singleVariableDataFrame = sparkSession.createDataFrame(singleVariableTestData, schemaWithInt);
            List<Variable> variables = variableNames.stream()
                    .map(variableName -> Variable.builder().variableName(variableName).systemSpec(mock(
                            SystemSpec.class)).build()).collect(Collectors.toList());

            withMockedDataQuery(mocked -> {
                mocked.when(() -> DataQuery.getFor(any(), any(), any(Variable.class)))
                        .thenReturn(singleVariableDataFrame);

                // when
                Dataset<Row> result = DataQuery.getAsPivot(sparkSession, TimeWindow.infinite(), variables);

                // then
                checkColumns(result, timestampField, "var1");

                List<Row> rows = result.orderBy(timestampField).collectAsList();
                assertEquals(1, rows.size());
                Row row = rows.get(0);

                assertEquals("2024-04-12", row.getString(row.fieldIndex(timestampField)));
                assertEquals(1, row.getInt(row.fieldIndex("var1")));
            });
        }

        @Test
        void testDoingPivotByVariableName() {
            // given
            String system = "CMW";
            List<String> variableNames = Collections.singletonList("var1");
            List<Row> singleVariableTestData = Collections.singletonList(
                    RowFactory.create("2024-04-12", "var1", 1, 1L)
            );
            Dataset<Row> singleVariableDataFrame = sparkSession.createDataFrame(singleVariableTestData, schemaWithInt);
            List<Variable> variables = variableNames.stream()
                    .map(variableName -> Variable.builder().variableName(variableName).systemSpec(mock(
                            SystemSpec.class)).build()).collect(Collectors.toList());

            withMockedServiceClientFactory(serviceFactory -> {
                withMockedDataQuery(dataQuery -> {
                    VariableService variableServiceMock = mock(VariableService.class);
                    serviceFactory.when(ServiceClientFactory::createVariableService).thenReturn(variableServiceMock);
                    dataQuery.when(() -> DataQuery.getFor(any(), any(), any(Variable.class)))
                            .thenReturn(singleVariableDataFrame);
                    when(variableServiceMock.findAll(any(Condition.class))).thenReturn(new HashSet<>(variables));

                    // when
                    Dataset<Row> result = DataQuery.getAsPivot(sparkSession, TimeWindow.infinite(), system,
                            variableNames);

                    // then
                    checkColumns(result, timestampField, "var1");

                    List<Row> rows = result.orderBy(timestampField).collectAsList();
                    assertEquals(1, rows.size());
                    Row row = rows.get(0);

                    assertEquals("2024-04-12", row.getString(row.fieldIndex(timestampField)));
                    assertEquals(1, row.getInt(row.fieldIndex("var1")));
                });
            });
        }

        private void withMockedDataQuery(Consumer<MockedStatic<DataQuery>> consumer) {
            try (MockedStatic<DataQuery> mocked = mockStatic(DataQuery.class)) {
                mocked.when(() -> DataQuery.getAsPivot(any(), any(), any())).thenCallRealMethod();
                mocked.when(() -> DataQuery.getAsPivot(any(), any(), any(), any())).thenCallRealMethod();
                mocked.when(() -> DataQuery.getDatasetForVariables(any(), any(), any())).thenCallRealMethod();
                mocked.when(() -> DataQuery.joinOnTimestamp(any())).thenCallRealMethod();
                mocked.when(() -> DataQuery.getVariableType(any())).thenCallRealMethod();
                mocked.when(() -> DataQuery.doPivotOnVariable(any(), any())).thenCallRealMethod();

                consumer.accept(mocked);
            }
        }

        private void checkColumns(Dataset<Row> dataset, String... expected) {
            assertEquals(Sets.newHashSet(expected), Sets.newHashSet(dataset.columns()));
        }

        private void withMockedServiceClientFactory(Consumer<MockedStatic<ServiceClientFactory>> consumer) {
            try (MockedStatic<ServiceClientFactory> clientFactory = mockStatic(ServiceClientFactory.class)) {
                consumer.accept(clientFactory);
            }
        }
    }

    private static <E> List<E> toList(E element) {
        if (element == null) {
            return null;
        }
        return List.of(element);
    }
}
