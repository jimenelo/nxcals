package cern.nxcals.api.extraction.data.spark;

import cern.nxcals.api.extraction.data.ExtractionUtils;
import org.apache.avro.Schema;
import org.apache.spark.sql.types.DataType;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

class ExtractionUtilsTest {
    private final Schema intSchema = Schema.create(Schema.Type.INT);
    private final Schema doubleSchema = Schema.create(Schema.Type.DOUBLE);

    private final Schema arrayUnionSchema = new Schema.Parser().parse(
            "[{\"type\":\"array\",\"items\":[\"float\",\"null\"]},\"null\"]");
    private final Schema recordSchema1 = new Schema.Parser().parse(
            "[{\"type\": \"record\", \"name\": \"test\", \"fields\" : [{\"name\": \"a\", \"type\": \"long\"}]},\"null\"]");

    @Test
    void shouldGetTypeFor() {
        //given
        String intSchema = "[\"int\"]";
        String longSchema = "[\"long\"]";
        String floatSchema = "[\"float\"]";
        String doubleSchema = "[\"double\"]";
        String stringSchema = "[\"string\"]";
        String bytesSchema = "[\"bytes\"]";
        String booleanSchema = "[\"boolean\"]";
        String unionSchema = "[\"double\",\"null\"]";
        String arraySchema = "[{\"type\":\"array\",\"items\":[\"float\",\"null\"]},\"null\"]";
        String recordSchema = "[{\"type\":\"record\",\"name\":\"float_array_2d\",\"namespace\":\"cern.nxcals\","
                + "\"fields\":[{\"name\":\"elements\",\"type\":[{\"type\":\"array\",\"items\":[\"float\",\"null\"]},\"null\"]},"
                + "{\"name\":\"rowCount\",\"type\":[\"int\",\"null\"]},{\"name\":\"columnCount\",\"type\":[\"int\",\"null\"]}]},\"null\"]";

        List<StructField> recordFields = new ArrayList<>();
        recordFields.add(DataTypes.createStructField("elements", DataTypes.createArrayType(DataTypes.FloatType), true));
        recordFields.add(DataTypes.createStructField("rowCount", DataTypes.IntegerType, true));
        recordFields.add(DataTypes.createStructField("columnCount", DataTypes.IntegerType, true));
        DataType expectedRecordType = DataTypes.createStructType(recordFields);

        //when
        DataType intType = ExtractionUtils.getDataTypeFor(new Schema.Parser().parse(intSchema));
        DataType longType = ExtractionUtils.getDataTypeFor(new Schema.Parser().parse(longSchema));
        DataType floatType = ExtractionUtils.getDataTypeFor(new Schema.Parser().parse(floatSchema));
        DataType doubleType = ExtractionUtils.getDataTypeFor(new Schema.Parser().parse(doubleSchema));
        DataType stringType = ExtractionUtils.getDataTypeFor(new Schema.Parser().parse(stringSchema));
        DataType bytesType = ExtractionUtils.getDataTypeFor(new Schema.Parser().parse(bytesSchema));
        DataType booleanType = ExtractionUtils.getDataTypeFor(new Schema.Parser().parse(booleanSchema));
        DataType unionType = ExtractionUtils.getDataTypeFor(new Schema.Parser().parse(unionSchema));
        DataType arrayType = ExtractionUtils.getDataTypeFor(new Schema.Parser().parse(arraySchema));
        DataType recordType = ExtractionUtils.getDataTypeFor(new Schema.Parser().parse(recordSchema));

        //then
        assertNotNull(intType);
        assertEquals(DataTypes.IntegerType, intType);
        assertNotNull(longType);
        assertEquals(DataTypes.LongType, longType);
        assertNotNull(floatType);
        assertEquals(DataTypes.FloatType, floatType);
        assertNotNull(doubleType);
        assertEquals(DataTypes.DoubleType, doubleType);
        assertNotNull(stringType);
        assertEquals(DataTypes.StringType, stringType);
        assertNotNull(bytesType);
        assertEquals(DataTypes.IntegerType, bytesType);
        assertNotNull(booleanType);
        assertEquals(DataTypes.BooleanType, booleanType);
        assertNotNull(unionType);
        assertEquals(DataTypes.DoubleType, unionType);
        assertNotNull(arrayType);
        assertEquals(DataTypes.createArrayType(DataTypes.FloatType), arrayType);
        assertNotNull(recordSchema);
        assertEquals(expectedRecordType, recordType);
    }

    @Test
    void shouldFailForUnknownType() {
        //given
        Schema intSchema = new Schema.Parser().parse(
                "[{ \"type\": \"enum\"," + "  \"name\": \"Suit\", \"symbols\" : [\"TEST1\", \"TEST2\"]}]");

        //when
        assertThrows(RuntimeException.class, () -> ExtractionUtils.getDataTypeFor(intSchema));
    }

    @Test
    void shouldHaveCorrectHbaseTypeNames() {
        assertEquals("int", ExtractionUtils.getHbaseTypeNameFor(intSchema));
        assertEquals("double", ExtractionUtils.getHbaseTypeNameFor(doubleSchema));
        assertEquals("binary", ExtractionUtils.getHbaseTypeNameFor(arrayUnionSchema));
        assertEquals("binary", ExtractionUtils.getHbaseTypeNameFor(recordSchema1));
    }
}
