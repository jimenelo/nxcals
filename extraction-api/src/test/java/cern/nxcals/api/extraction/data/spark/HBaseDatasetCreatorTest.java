package cern.nxcals.api.extraction.data.spark;

import cern.nxcals.common.domain.ColumnMapping;
import cern.nxcals.common.domain.HBaseExtractionTask;
import cern.nxcals.common.domain.HBaseResource;
import org.apache.avro.Schema;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.avro.SchemaConverters;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import static cern.nxcals.common.spark.MoreFunctions.createNullableField;
import static cern.nxcals.common.spark.MoreFunctions.createTypeFromFields;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class HBaseDatasetCreatorTest {
    private final static String STRING_FIELD_NAME = "string_field";
    private static final String array2dSchema =
            "[{\"type\":\"record\",\"name\":\"float_array_2d\",\"fields\":[{\"name\":\"elements\",\"type\""
                    + ":[{\"type\":\"array\",\"items\":[\"float\",\"null\"]},\"null\"]},{\"name\":\"rowCount\",\"type\":[\"int\",\"null\"]},\n"
                    + "{\"name\":\"columnCount\",\"type\":[\"int\",\"null\"]}]},\"null\"]";
    private static final String tableSchema = "{\"type\":\"record\",\"name\":\"CmwPT\"," + "\"fields\":["
            + String.format("{\"name\":\"%s\",\"type\":\"string\"},", STRING_FIELD_NAME)
            + "{\"name\":\"array_field\",\"type\":[{\"type\":\"array\",\"items\":[\"int\",\"null\"]},\"null\"]},"
            + "{\"name\":\"array2D_field\",\"type\":" + array2dSchema + "}]}";
    private SparkSession sessionMock;
    private HBaseDatasetCreator instance;

    @BeforeEach
    public void setUp() {
        this.sessionMock = mock(SparkSession.class, RETURNS_DEEP_STUBS);
        this.instance = new HBaseDatasetCreator(this.sessionMock);
    }

    @Test
    void shouldBuildTableCatalog() {
        //given
        String namespace = "default";
        String tableName = "1__2__1";

        String expectedCatalog = "{\"table\": {\"namespace\": \"default\", \"name\":\"1__2__1\"},"
                + "\"rowkey\":\"id\",\"columns\":{\"entityKey\":{\"cf\":\"rowkey\",\"col\":\"id\",\"type\":\"string\"},"
                + "\"array_field\":{\"cf\":\"data\",\"col\":\"array_field\",\"avro\":\"1__2__1__array_field\"},"
                + "\"array2D_field\":{\"cf\":\"data\",\"col\":\"array2D_field\",\"avro\":\"1__2__1__array2d_field\"},"
                + "\"string_field\":{\"cf\":\"data\",\"col\":\"string_field\",\"type\":\"string\"}}}";

        String expectedComplexTypeSchema = "[{\"type\":\"record\",\"name\":\"float_array_2d\","
                + "\"fields\":[{\"name\":\"elements\",\"type\":[{\"type\":\"array\",\"items\":[\"float\",\"null\"]},\"null\"]},"
                + "{\"name\":\"rowCount\",\"type\":[\"int\",\"null\"]},"
                + "{\"name\":\"columnCount\",\"type\":[\"int\",\"null\"]}]},\"null\"]";
        String tableSchema = "{\"type\":\"record\",\"name\":\"CmwPT\"," + "\"fields\":["
                + "{\"name\":\"string_field\",\"type\":\"string\"},"
                + "{\"name\":\"array_field\",\"type\":[{\"type\":\"array\",\"items\":[\"int\",\"null\"]},\"null\"]},"
                + "{\"name\":\"array2D_field\",\"type\":" + array2dSchema + "}]}";
        Schema parsedTableSchema = new Schema.Parser().parse(tableSchema);

        List<ColumnMapping> fields = new ArrayList<>();
        fields.add(ColumnMapping.builder().fieldName("array_field")
                //                .schemaJson(new Schema.Parser().parse("[{\"type\":\"array\",\"items\":[\"int\",\"null\"]},\"null\"]"))
                .build());
        fields.add(ColumnMapping.builder().fieldName("array2D_field")
                //                .schemaJson(parsedArray2dSchema)
                .build());
        fields.add(ColumnMapping.builder().fieldName("string_field").build());
        //                .schemaJson(new Schema.Parser().parse("[\"string\",\"null\"]")).build());
        //when
        HBaseResource resource = HBaseResource.builder().tableName(tableName).schemaJson(parsedTableSchema.toString())
                .namespace(namespace).isAccessible(true).build();
        Map<String, String> resultMap = HBaseDatasetCreator.HBaseCatalogUtils.tableCatalog(resource, fields);

        //then
        assertFalse(resultMap.isEmpty());
        assertEquals(3, resultMap.size());
        assertFalse(resultMap.containsKey("string_field"));
        assertNotNull(resultMap.get("catalog"));
        assertEquals(expectedCatalog, resultMap.get("catalog"));
        assertNotNull(resultMap.get("1__2__1__array2d_field"));
        assertEquals(expectedComplexTypeSchema, resultMap.get("1__2__1__array2d_field"));
    }

    @Test
    void shouldGetColumnsFromPredicate() {
        List<String> predicates = List.of(String.format("%s != 'STR'", STRING_FIELD_NAME));
        Schema parsedTableSchema = new Schema.Parser().parse(tableSchema);
        Collection<ColumnMapping> resultMappings = HBaseDatasetCreator.HBaseCatalogUtils.getColumnsFromPredicates(
                predicates,
                parsedTableSchema);
        assertTrue(resultMappings.stream().anyMatch(column -> column.getFieldName().equals(STRING_FIELD_NAME)));
    }

    @Test
    void shouldReturnParentColumnMappings() {
        String fieldName = "a.b.c.d";
        ColumnMapping mapping = ColumnMapping.builder()
                .fieldName(fieldName).schemaJson("[]").alias("alias")
                .build();
        // just it is easier to create
        StructType sparkSchema = createTypeFromFields(
                createNullableField("a", createTypeFromFields(
                        createNullableField("b", createTypeFromFields(
                                createNullableField("c", createTypeFromFields(
                                        createNullableField("d", DataTypes.IntegerType)
                                ))
                        ))
                ))
        );
        Schema schema = SchemaConverters.toAvroType(sparkSchema, false, "data0", "cern.nxcals");
        Collection<ColumnMapping> parentMappings = HBaseDatasetCreator.HBaseCatalogUtils.getParentColumns(
                List.of(mapping), schema);
        assertEquals(3, parentMappings.size());
        assertTrue(parentMappings.stream().anyMatch(parentMapping -> parentMapping.getFieldName().equals("a")));
        assertTrue(parentMappings.stream().anyMatch(parentMapping -> parentMapping.getFieldName().equals("a.b")));
        assertTrue(parentMappings.stream().anyMatch(parentMapping -> parentMapping.getFieldName().equals("a.b.c")));
    }

    @Test
    void shouldGenerateCorrectParentPathNames() {
        List<String> parentNames = HBaseDatasetCreator.HBaseCatalogUtils.generateParentNames("a.b.c.d");
        assertEquals(Set.of("a", "a.b", "a.b.c"), new HashSet<>(parentNames));
    }

    @Test
    void shouldCorrectlyCallSparkToLoadDatasetAndCallWhereAndSelectMethodsInCorrectOrder() {
        // given
        HBaseExtractionTask task = mock(HBaseExtractionTask.class);
        when(task.getPredicates()).thenReturn(List.of("x > 0"));
        HBaseResource hbaseResource = HBaseResource.builder()
                .isAccessible(false)
                .tableName("table")
                .namespace("nxcals")
                .schemaJson(tableSchema)
                .build();
        when(task.getResource()).thenReturn(hbaseResource);
        Dataset ds = mock(Dataset.class);
        when(sessionMock.read().format(HBaseDatasetCreator.HBASE_FORMAT).options(anyMap()).load()).thenReturn(ds);
        when(ds.schema()).thenReturn(new StructType());
        when(ds.select(any(Column[].class))).thenReturn(ds);

        Dataset nextDs = mock(Dataset.class);
        when(ds.where(any(Column.class))).thenReturn(nextDs);
        when(nextDs.selectExpr(any(String[].class))).thenReturn(nextDs);
        // when
        Optional<Dataset<Row>> result = instance.apply(task);
        // then
        assertTrue(result.isPresent());
        assertEquals(nextDs, result.get());
    }

}
