package cern.nxcals.api.extraction.data.builders;

import cern.nxcals.api.extraction.data.builders.fluent.ThreadLocalStorage;
import cern.nxcals.common.domain.CallDetails;
import org.apache.spark.sql.SparkSession;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class CallDetailsProviderTest {
    private final String APP_NAME = "Application";
    private SparkSession sessionMock =  mock(SparkSession.class, RETURNS_DEEP_STUBS );

    @BeforeEach
    void initialize() {
        when(sessionMock.sparkContext().appName()).thenReturn(APP_NAME);
    }

    @Test
    void ShouldSetCallDetailsFromEmptyStack() {
        CallDetailsProvider callDetailsProvider = new CallDetailsProvider(() -> new StackTraceElement[0]);
        callDetailsProvider.setCallDetails(sessionMock);

        CallDetails callDetails = ThreadLocalStorage.getCallDetails();
        assertEquals(APP_NAME, callDetails.getApplicationName());
        assertTrue(callDetails.getDeclaringClass().isEmpty());
        assertTrue(callDetails.getMethodName().isEmpty());
    }

    @Test
    void ShouldSetCallDetails() {
        CallDetailsProvider callDetailsProvider = new CallDetailsProvider();
        callDetailsProvider.setCallDetails(sessionMock);

        CallDetails callDetails = ThreadLocalStorage.getCallDetails();
        assertEquals(APP_NAME, callDetails.getApplicationName());
        assertFalse(callDetails.getDeclaringClass().isEmpty());
        assertFalse(callDetails.getMethodName().isEmpty());
    }
}