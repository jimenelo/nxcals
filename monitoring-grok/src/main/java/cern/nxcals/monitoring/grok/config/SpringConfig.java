/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.monitoring.grok.config;

import cern.nxcals.common.metrics.MetricsConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.support.DefaultConversionService;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

@EnableConfigurationProperties(cern.nxcals.monitoring.grok.domain.Configuration.class)
@Configuration
@Slf4j
@Import(MetricsConfig.class)
public class SpringConfig {
    private static final int THREADS = 1;

    @Bean
    public static ConversionService conversionService() {
        return new DefaultConversionService();
    }

    @Bean(name = "scheduledExecutor")
    public ScheduledExecutorService createScheduledExecutor() {
        return Executors.newScheduledThreadPool(THREADS);
    }

    @Bean
    public FileSystem fileSystem() {
        return FileSystems.getDefault();
    }

    @Bean
    public ScriptEngine scriptEngine() {
        return new ScriptEngineManager().getEngineByName("nashorn");
    }

}
