/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.monitoring.grok;

import cern.nxcals.monitoring.grok.service.FileProcessor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.ApplicationPidFileWriter;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
@Slf4j
public class Application {
    public static void main(String[] args) throws Exception {
        try {
            SpringApplication app = new SpringApplication(Application.class);
            app.addListeners(new ApplicationPidFileWriter());
            ApplicationContext ctx = app.run();

            FileProcessor scanner = ctx.getBean(FileProcessor.class);
            scanner.start();
        } catch (Exception ex) {
            log.error("Exception while running publisher app", ex);
            System.exit(1);
        }
    }
}
