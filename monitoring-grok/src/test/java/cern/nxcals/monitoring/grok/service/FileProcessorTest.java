package cern.nxcals.monitoring.grok.service;

import cern.nxcals.common.metrics.MetricsRegistry;
import cern.nxcals.common.metrics.MicrometerMetricsRegistry;
import cern.nxcals.monitoring.grok.domain.Configuration;
import cern.nxcals.monitoring.grok.domain.RuleConfig;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.script.Bindings;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.nio.file.FileSystems;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyDouble;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by jwozniak on 6/6/17.
 */

// TODO: when resolving NXCALS-2438 this must be reverted to:
// @RunWith(MockitoJUnitRunner.class)
@ExtendWith(MockitoExtension.class)
@SuppressWarnings("java:S2925") //sleep(x)
public class FileProcessorTest {

    private final ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();

    private final ScriptEngine scriptEngine = new ScriptEngineManager().getEngineByName("nashorn");
    private final FileScanner fileScanner = new FileScanner(FileSystems.getDefault());

    private MetricsRegistry metricsRegistry;
    private CountDownLatch latch;

    @Mock
    private MeterRegistry meterRegistry;

    @Mock
    private Counter testCounter;

    @BeforeEach
    public void setUp() throws Exception {
        when(meterRegistry.counter("test")).thenReturn(testCounter);
        metricsRegistry = new MicrometerMetricsRegistry(meterRegistry);
    }

    @AfterEach
    public void tearDown() throws Exception {
        reset(meterRegistry, testCounter);
    }

    @Test
    public void shouldFailOnNullFileScanner() {
        assertThrows(NullPointerException.class, () ->
                new FileProcessor(null, executor, metricsRegistry, mock(Configuration.class), scriptEngine));
    }

    @Test
    public void shouldFailOnNullExecutor() {
        assertThrows(NullPointerException.class, () ->
            new FileProcessor(fileScanner, null, metricsRegistry, mock(Configuration.class), scriptEngine));
    }

    @Test
    public void shouldFailOnNullMeterRegistry() {
        assertThrows(NullPointerException.class, () ->
            new FileProcessor(fileScanner, executor, null, mock(Configuration.class), scriptEngine));
    }

    @Test
    public void shouldFailOnNullConfiguration() {
        assertThrows(NullPointerException.class, () ->
            new FileProcessor(fileScanner, executor, metricsRegistry, null, scriptEngine));
    }

    @Test
    public void shouldFailOnNullScriptEngine() {
        assertThrows(NullPointerException.class, () ->
            new FileProcessor(fileScanner, executor, metricsRegistry, mock(Configuration.class), null));
    }

    @Test
    public void shouldGetEmptyFilePatternsExcludeListWhenRuleExcludePatternIsNotSpecified() {
        RuleConfig ruleConfig = new RuleConfig();
        ruleConfig.setFilePatternsInclude(Collections.singletonList(".*"));
        ruleConfig.setInitScript("");
        ruleConfig.setConditionScript("");
        ruleConfig.setLinePatterns(Collections.emptyList());

        assertNotNull(ruleConfig.getFilePatternsExclude());
        assertTrue(ruleConfig.getFilePatternsExclude().isEmpty());
    }

    @Test
    @Disabled("fix in NXCALS-2438")
    public void testProcessor() throws Exception {
        Configuration config = config();
        RuleConfig ruleConfig = rule("",
                "if(patterns[0].matcher(line).matches()) {metricsRegistry.incrementCounterBy('test', 1);}" +
                        "var matcher = patterns[1].matcher(line);" +
                        "if(matcher.matches()) {" +
                        "metricsRegistry.updateGaugeTo('test1', java.lang.Long.parseLong(matcher.group(1)));" +
                        "}", Collections.singleton(".*value=([0-9]+).*"));
        config.setRules(Collections.singletonList(ruleConfig));

        setUpMetricsRegistryMockExpectations(4);

        FileProcessor processor = processor(config, scriptEngine);
        processor.setMetricsPrefixGenerator(p -> "prefix");
        processor.start(); //file is not yet created
        writeToFile(); //write the file
        latch.await(10, TimeUnit.SECONDS);

        ArgumentCaptor<AtomicLong> argument = ArgumentCaptor.forClass(AtomicLong.class);
        verify(meterRegistry, Mockito.times(1)).gauge(eq("test1"), argument.capture());
        assertEquals(10, argument.getValue().longValue());

        verify(meterRegistry.counter("test"), Mockito.times(3)).increment(1.0);

        processor.destroy();
    }

    @Test
    @Disabled("fix in NXCALS-2438")
    public void testProcessorWithFirstMatcher() throws Exception {
        Configuration config = config();
        RuleConfig ruleConfig = rule("",
                "if(firstMatcher.test(line)) { metricsRegistry.incrementCounterBy('test', 1); }",
                Arrays.asList(".*dont count.*"));
        config.setRules(Collections.singletonList(ruleConfig));

        setUpMetricsRegistryMockExpectations(2);

        FileProcessor processor = processor(config, scriptEngine);
        processor.setMetricsPrefixGenerator(p -> "prefix");
        processor.start(); //file is not yet created
        writeToFile(); //write the file
        latch.await(10, TimeUnit.SECONDS);
        verify(meterRegistry.counter("test"), Mockito.times(2)).increment(1.0);

        processor.destroy();
    }

    @Test
    @Disabled("fix in NXCALS-2438")
    public void testProcessorWithAnyMatcher() throws Exception {
        Configuration config = config();
        RuleConfig ruleConfig = rule("", "if(anyMatcher.test(line)) { metricsRegistry.incrementCounterBy('test', 1); }",
                Arrays.asList(".*test.*", ".*superTest.*"));
        config.setRules(Collections.singletonList(ruleConfig));

        setUpMetricsRegistryMockExpectations(5);

        FileProcessor processor = processor(config, scriptEngine);
        processor.setMetricsPrefixGenerator(p -> "prefix");
        processor.start(); //file is not yet created
        writeToFile(); //write the file
        latch.await(10, TimeUnit.SECONDS);
        verify(meterRegistry.counter("test"), Mockito.times(5)).increment(1.0);

        processor.destroy();
    }

    @Test
    public void shouldHandleScriptEngineExceptions() throws Exception {
        Configuration config = config();
        RuleConfig ruleConfig = rule("", "if(firstMatcher.test(line)) {metricsRegistry.incrementCounterBy('test', 1);}",
                Collections.singleton(".*dont count.*"));
        config.setRules(Collections.singletonList(ruleConfig));
        ScriptEngine scriptEngine = mock(ScriptEngine.class);
        when(scriptEngine.eval(anyString(), any(Bindings.class))).thenThrow(new ScriptException(
                "This is an intentional exception for testing!"));
        when(scriptEngine.createBindings()).thenReturn(mock(Bindings.class));
        FileProcessor processor = processor(config, scriptEngine);
        processor.setMetricsPrefixGenerator(p -> "prefix");
        processor.start(); //file is not yet created
        TimeUnit.SECONDS.sleep(2); // wait for the processor to initialize and start scanning.

        writeToFile(); //write the file

        processor.destroy();
    }

    @Test
    public void shouldThrowExceptionWhenConfiguredWithWrongRules() throws Exception {
        Configuration config = config();
        RuleConfig ruleConfig = rule("", "if(misconfiguredParenthesis)))) { thisIsAWrongRuleConfiguration }",
                Arrays.asList(".*test.*", ".*superTest.*"));
        config.setRules(Collections.singletonList(ruleConfig));
        FileProcessor processor = new FileProcessor(fileScanner, Executors.newSingleThreadScheduledExecutor(r -> Thread.currentThread()), metricsRegistry, config, scriptEngine);
        assertThrows(IllegalArgumentException.class, () -> processor.start());
    }

    private FileProcessor processor(Configuration config, ScriptEngine scriptEngine) {
        return new FileProcessor(fileScanner, executor, metricsRegistry, config, scriptEngine);
    }

    private RuleConfig rule(String init, String condition, Collection<String> extraPatterns) {
        RuleConfig ruleConfig = new RuleConfig();
        ruleConfig.setName("testRule_" + UUID.randomUUID().toString());
        ruleConfig.setFilePatternsInclude(Collections.singletonList(".*"));
        ruleConfig.setFilePatternsExclude(Collections.emptyList());
        ruleConfig.setInitScript(init);
        ruleConfig.setConditionScript(condition);

        List<String> patterns = new ArrayList<>();
        patterns.add(".* ERR .*");
        if (extraPatterns != null) {
            patterns.addAll(extraPatterns);
        }
        ruleConfig.setLinePatterns(patterns);
        return ruleConfig;
    }

    private Configuration config() {
        Configuration config = new Configuration();
        config.setBasePath(".");
        config.setFileGlob("**/tailer-test.log");
        config.setDepth(1);
        config.setSearchEvery(1);
        config.setSearchEveryTimeUnit(TimeUnit.MILLISECONDS);
        return config;
    }

    private void writeToFile() {
        //create a file
        try {
            File temp = new File("./tailer-test.log");
            temp.createNewFile();
            temp.deleteOnExit();

            //write it
            BufferedWriter bw = new BufferedWriter(new FileWriter(temp));
            bw.write("START: This is the temporary file content\n");
            bw.flush();
            TimeUnit.MILLISECONDS.sleep(500); //here we should have the file ready to be read by the tailer.
            //The tailer starts from the end of the file so it must find it before we start writing any content to it for this test.
            bw.write("This is the ERR temporary file content\n");
            bw.write("This is the temporary  ERR file content\n");
            bw.write("This is the temporary file ERR content dont count\n");
            bw.write("This is the value=20 temporary file content\n");
            bw.write("This is a [test] line that must be captured: \n");
            bw.write("This is a superTest line that must be captured\n");
            bw.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void setUpMetricsRegistryMockExpectations(int expectedTotalInvocationCount) {
        latch = new CountDownLatch(expectedTotalInvocationCount);
        doAnswer(i -> {
            latch.countDown();
            return null;
        }).when(testCounter).increment(anyDouble());

        when(meterRegistry.gauge(eq("test1"), any(Number.class))).thenAnswer(i -> {
            latch.countDown();
            return i.getArguments()[1];
        });
    }

}
