package cern.nxcals.db;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import liquibase.Contexts;
import liquibase.LabelExpression;
import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.LiquibaseException;
import liquibase.resource.ClassLoaderResourceAccessor;
import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;

import java.sql.Connection;

/**
 * Created by msobiesz on 04/08/17.
 */
@Slf4j
public class Application {

    public static final String DEFAULT_CONTEXT_NAME = "default";

    public static void main(String[] args) {
        new Application().run(ConfigFactory.load());
    }

    @SuppressWarnings("squid:S00112")
    public void run(Config config) {
        AppConfig appConfig = this.getAppConfigFrom(config);
        try (Connection connection = ConnectionFactory.forDriver(appConfig.getClassDriver())
                .getConnectionFor(appConfig.getUrl(), appConfig.getUser(), appConfig.getPassword())) {
            Database database = DatabaseFactory.getInstance()
                    .findCorrectDatabaseImplementation(new JdbcConnection(connection));
            if (appConfig.isDropAll()) {
                log.info("********* droping all ****************");
                dropAll(appConfig.getInstallChangeLogPath(), database);
            }
            if (appConfig.isInstallEnabled()) {
                log.info("********* installing all *************");
                performUpdate(appConfig.getInstallChangeLogPath(), appConfig.getContextName(), database);
            }
            if (appConfig.isUpdateEnabled()) {
                log.info("********* updating all ***************");
                performUpdate(appConfig.getUpdateChangeLogPath(), appConfig.getContextName(), database);
            }
            if (appConfig.isScriptEnabled() && !appConfig.getScriptChangeLogFile().isEmpty()) {
                log.info("********* script " + appConfig.getScriptChangeLogFile() + " ***************");
                performUpdate(appConfig.getScriptChangeLogFile(), appConfig.getContextName(), database);
            }
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    @SuppressWarnings("squid:S2095")  // Not possible to use try-with-resources
    private void dropAll(String pathToChangeLog, Database database) throws LiquibaseException {
        Liquibase liquibase = new Liquibase(pathToChangeLog, new ClassLoaderResourceAccessor(), database);
        liquibase.dropAll();
    }

    @SuppressWarnings("squid:S2095")  // Not possible to use try-with-resources
    private void performUpdate(String pathToChangeLog, String contextName, Database database)
            throws LiquibaseException {
        Liquibase liquibase = new Liquibase(pathToChangeLog, new ClassLoaderResourceAccessor(), database);
        liquibase.update(new Contexts(contextName), new LabelExpression());

    }

    private AppConfig getAppConfigFrom(Config config) {
        boolean dropAll = config.hasPath("db.drop.all.before") && config.getBoolean("db.drop.all.before");
        return AppConfig.builder().user(config.getString("db.user"))
                .password(convertPasswordIfNeeded(config.getString("db.password"))).url(config.getString("db.url"))
                .installChangeLogPath(config.getString("db.install.changelog.path"))
                .updateChangeLogPath(config.getString("db.update.changelog.path"))
                .scriptChangeLogFile(config.getString("db.script.changelog.file"))
                .installEnabled(config.getBoolean("db.enable.install"))
                .updateEnabled(config.getBoolean("db.enable.update"))
                .scriptEnabled(config.getBoolean("db.enable.script"))
                .dropAll(dropAll)
                .classDriver(config.getString("db.driver.class"))
                .contextName(getContextName(config))
                .build();
    }

    // Original logic of the liquibase implementation: if the context is not specified - all contexts will be executed.
    // That's why we are passing mock DEFAULT_CONTEXT_NAME instead of empty context name in order to execute only changesets that are not bound to any context
    private String getContextName(Config config) {
        if (config.hasPath("db.context.name")) {
            String contextName = config.getString("db.context.name");
            if (!contextName.isEmpty()) {
                return contextName;
            }
        }
        return DEFAULT_CONTEXT_NAME;
    }

    private String convertPasswordIfNeeded(String password) {
        if (Base64.isBase64(password.getBytes())) {
            return new String(Base64.decodeBase64(password.getBytes()));
        }
        return password;
    }

    @Data
    @Builder
    private static class AppConfig {
        private final String user;
        private final String password;
        private final String url;
        private final String installChangeLogPath;
        private final String updateChangeLogPath;
        private final String scriptChangeLogFile;
        private final String classDriver;
        private final String contextName;
        private final boolean installEnabled;
        private final boolean updateEnabled;
        private final boolean scriptEnabled;
        private final boolean dropAll;
    }
}
