/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.api.extraction.metadata;

import cern.nxcals.api.converters.Py4jLongConverter;
import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.Hierarchy;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.domain.VariableHierarchies;
import cern.nxcals.api.domain.VariableHierarchyIds;
import cern.nxcals.api.extraction.metadata.feign.HierarchyClient;
import cern.nxcals.api.extraction.metadata.queries.Hierarchies;
import cern.nxcals.internal.extraction.metadata.InternalHierarchyService;
import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static cern.nxcals.common.Constants.HIERARCHY_SEPARATOR;

class HierarchyProvider extends AbstractProvider<Hierarchy, HierarchyClient, Hierarchies> implements InternalHierarchyService {
    HierarchyProvider(HierarchyClient httpClient) {
        super(httpClient);
    }

    @Override
    public Hierarchy create(@NonNull Hierarchy hierarchy) {
        return getHttpClient().create(hierarchy);
    }

    @Override
    public Hierarchy update(@NonNull Hierarchy hierarchy) {
        return getHttpClient().update(hierarchy);
    }

    @Override
    public void deleteLeaf(@NonNull Hierarchy hierarchy) {
        getHttpClient().deleteLeaf(hierarchy.getId());
    }

    @Override
    public Set<Hierarchy> createAll(@NonNull Set<Hierarchy> hierarchies) {
        return getHttpClient().createAll(hierarchies);
    }

    @Override
    public Set<Hierarchy> updateAll(@NonNull Set<Hierarchy> hierarchies) {
        return getHttpClient().updateAll(hierarchies);
    }

    @Override
    public void deleteAllLeaves(@NonNull Set<Hierarchy> hierarchies) {
        getHttpClient().deleteAllLeaves(hierarchies.stream().map(Hierarchy::getId).collect(Collectors.toSet()));
    }

    @Override
    public void setVariables(long hierarchyId, @NonNull Collection<Variable> variables) {
        setVariables(hierarchyId, variables.stream().map(Variable::getId).collect(Collectors.toSet()));
    }

    @Override
    public void setVariables(long hierarchyId, @NonNull Set<Long> variables) {
        getHttpClient().setVariables(hierarchyId, Py4jLongConverter.convert(variables));
    }

    @Override
    public void addVariables(long hierarchyId, @NonNull Set<Long> variables) {
        getHttpClient().addVariables(hierarchyId, Py4jLongConverter.convert(variables));
    }

    @Override
    public void removeVariables(long hierarchyId, @NonNull Set<Long> variableIds) {
        getHttpClient().removeVariables(hierarchyId, Py4jLongConverter.convert(variableIds));
    }

    @Override
    public Set<Variable> getVariables(long hierarchyId) {
        return getHttpClient().getVariables(hierarchyId);
    }

    @Override
    public void setEntities(long hierarchyId, @NonNull Collection<Entity> entities) {
        setEntities(hierarchyId, entities.stream().map(Entity::getId).collect(Collectors.toSet()));
    }

    @Override
    public void setEntities(long hierarchyId, @NonNull Set<Long> entities) {
        getHttpClient().setEntities(hierarchyId, Py4jLongConverter.convert(entities));
    }

    @Override
    public void removeEntities(long hierarchyId, @NonNull Set<Long> entityIds) {
        getHttpClient().removeEntities(hierarchyId, Py4jLongConverter.convert(entityIds));
    }

    @Override
    public void addEntities(long hierarchyId, @NonNull Set<Long> entities) {
        getHttpClient().addEntities(hierarchyId, Py4jLongConverter.convert(entities));
    }

    @Override
    public Set<Entity> getEntities(long hierarchyId) {
        return getHttpClient().getEntities(hierarchyId);
    }

    @Override
    public Set<Hierarchy> getTopLevel(@NonNull SystemSpec system) {
        return findAll(Hierarchies.suchThat().areTopLevel().and().systemId().eq(system.getId()));
    }

    @Override
    public List<Hierarchy> getChainTo(@NonNull Hierarchy hierarchy) {
        if (StringUtils.isEmpty(hierarchy.getNodePath())) {
            return Collections.singletonList(hierarchy);
        }

        Deque<String> pathSequence = new LinkedList<>();
        pathSequence.push(hierarchy.getNodePath());

        int index;
        while ((index = pathSequence.peek().lastIndexOf(HIERARCHY_SEPARATOR)) > 0) {
            pathSequence.push(pathSequence.peek().substring(0, index));
        }

        return findAll(Hierarchies.suchThat().systemId().eq(hierarchy.getSystemSpec().getId()).and().path().in(pathSequence))
                .stream()
                .sorted(Comparator.comparing(t -> t.getNodePath().length()))
                .collect(Collectors.toList());
    }

    @Override
    public Optional<Hierarchy> findById(long id) {
        return findOne(Hierarchies.suchThat().id().eq(id));
    }

    @Override
    public Set<Hierarchy> getHierarchiesForVariable(long variableId) {
        return this.getHierarchiesForVariables(Collections.singleton(variableId)).stream()
                .findFirst().map(VariableHierarchies::getHierarchies).orElse(Collections.emptySet());
    }

    @Override
    public Set<VariableHierarchies> getHierarchiesForVariables(@NonNull Set<Long> variableIds) {
        return getHttpClient().getHierarchiesForVariables(Py4jLongConverter.convert(variableIds));
    }

    @Override
    public Set<VariableHierarchyIds> getHierarchyIdsForVariables(@NonNull Set<Long> variableIds) {
        return getHttpClient().getHierarchyIdsForVariables(Py4jLongConverter.convert(variableIds));
    }
}
