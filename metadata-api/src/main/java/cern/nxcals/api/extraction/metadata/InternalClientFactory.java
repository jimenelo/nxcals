/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.api.extraction.metadata;

import cern.nxcals.api.extraction.metadata.feign.AuthorizationClient;
import cern.nxcals.api.extraction.metadata.feign.CompactionClient;
import cern.nxcals.api.extraction.metadata.feign.EntityChangelogClient;
import cern.nxcals.api.extraction.metadata.feign.EntityClient;
import cern.nxcals.api.extraction.metadata.feign.EntityResourceClient;
import cern.nxcals.api.extraction.metadata.feign.EntitySchemaClient;
import cern.nxcals.api.extraction.metadata.feign.GroupClient;
import cern.nxcals.api.extraction.metadata.feign.HierarchyChangelogClient;
import cern.nxcals.api.extraction.metadata.feign.HierarchyClient;
import cern.nxcals.api.extraction.metadata.feign.HierarchyVariablesChangelogClient;
import cern.nxcals.api.extraction.metadata.feign.PartitionClient;
import cern.nxcals.api.extraction.metadata.feign.PartitionResourceClient;
import cern.nxcals.api.extraction.metadata.feign.PartitionResourceHistoryClient;
import cern.nxcals.api.extraction.metadata.feign.SystemSpecClient;
import cern.nxcals.api.extraction.metadata.feign.VariableChangelogClient;
import cern.nxcals.api.extraction.metadata.feign.VariableClient;
import cern.nxcals.api.extraction.metadata.feign.VariableConfigChangelogClient;
import cern.nxcals.common.utils.Lazy;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * A factory class that creates services for all meta-data access points using Netflix Feign + Ribbon.
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
class InternalClientFactory extends AbstractClientFactory {
    private static final InternalClientFactory INSTANCE = new InternalClientFactory();

    private final Lazy<PartitionClient> partitionClient = new Lazy<>(() -> createServiceFor(PartitionClient.class));
    private final Lazy<PartitionResourceClient> partitionResourceClient = new Lazy<>(() -> createServiceFor(PartitionResourceClient.class));
    private final Lazy<PartitionResourceHistoryClient> partitionResourceHistoryClient = new Lazy<>(() -> createServiceFor(PartitionResourceHistoryClient.class));

    private final Lazy<SystemSpecClient> systemSpecClient = new Lazy<>(() -> createServiceFor(SystemSpecClient.class));
    private final Lazy<EntitySchemaClient> entitySchemaClient = new Lazy<>(() -> createServiceFor(EntitySchemaClient.class));
    private final Lazy<EntityClient> entityClient = new Lazy<>(() -> createServiceFor(EntityClient.class));
    private final Lazy<EntityResourceClient> resourceClient = new Lazy<>(() -> createServiceFor(EntityResourceClient.class));
    private final Lazy<GroupClient> groupClient = new Lazy<>(() -> createServiceFor(GroupClient.class));
    private final Lazy<VariableClient> variableService = new Lazy<>(() -> createServiceFor(VariableClient.class));
    private final Lazy<CompactionClient> compactionService = new Lazy<>(() -> createServiceFor(CompactionClient.class));
    private final Lazy<HierarchyClient> hierarchyClient = new Lazy<>(() -> createServiceFor(HierarchyClient.class));
    private final Lazy<HierarchyChangelogClient> hierarchyChangelogClient = new Lazy<>(() -> createServiceFor(HierarchyChangelogClient.class));
    private final Lazy<HierarchyVariablesChangelogClient> hierarchyVariablesChangelogClient = new Lazy<>(() -> createServiceFor(HierarchyVariablesChangelogClient.class));
    private final Lazy<VariableChangelogClient> variableChangelogClient = new Lazy<>(() -> createServiceFor(VariableChangelogClient.class));
    private final Lazy<VariableConfigChangelogClient> variableConfigChangelogClient = new Lazy<>(() -> createServiceFor(VariableConfigChangelogClient.class));
    private final Lazy<EntityChangelogClient> entityChangelogClient = new Lazy<>(() -> createServiceFor(EntityChangelogClient.class));
    private final Lazy<AuthorizationClient> authorizationClient = new Lazy<>(() -> createServiceFor(AuthorizationClient.class));


    static EntitySchemaClient createEntitySchemaService() {
        return INSTANCE.entitySchemaClient.get();
    }

    static EntityClient createEntityService() {
        return INSTANCE.entityClient.get();
    }

    static PartitionClient createPartitionService() {
        return INSTANCE.partitionClient.get();
    }

    static SystemSpecClient createSystemSpecService() {
        return INSTANCE.systemSpecClient.get();
    }

    static EntityResourceClient createResourceService() {
        return INSTANCE.resourceClient.get();
    }

    static GroupClient createGroupService() {
        return INSTANCE.groupClient.get();
    }

    static VariableClient createVariableService() {
        return INSTANCE.variableService.get();
    }

    static CompactionClient createCompactionService() {
        return INSTANCE.compactionService.get();
    }

    static HierarchyClient createHierarchyService() {
        return INSTANCE.hierarchyClient.get();
    }

    static HierarchyChangelogClient createHierarchyChangelogService() {
        return INSTANCE.hierarchyChangelogClient.get();
    }

    static HierarchyVariablesChangelogClient createHierarchyVariablesChangelogService() {
        return INSTANCE.hierarchyVariablesChangelogClient.get();
    }

    static VariableChangelogClient createVariableChangelogService() {
        return INSTANCE.variableChangelogClient.get();
    }

    static VariableConfigChangelogClient createVariableConfigChangelogService() {
        return INSTANCE.variableConfigChangelogClient.get();
    }

    static EntityChangelogClient createEntityChangelogService() {
        return INSTANCE.entityChangelogClient.get();
    }

    static AuthorizationClient createAuthorizationService() {
        return INSTANCE.authorizationClient.get();
    }

    static PartitionResourceClient createPartitionResourceService() {
        return INSTANCE.partitionResourceClient.get();
    }

    static PartitionResourceHistoryClient createPartitionResourceHistoryService() {
        return INSTANCE.partitionResourceHistoryClient.get();
    }


}