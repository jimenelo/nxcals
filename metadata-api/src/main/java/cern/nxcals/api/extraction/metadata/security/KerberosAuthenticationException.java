package cern.nxcals.api.extraction.metadata.security;

public class KerberosAuthenticationException extends RuntimeException {

    public KerberosAuthenticationException(String message) {
        super(message);
    }

    public KerberosAuthenticationException(String message, Throwable cause) {
        super(message, cause);
    }

}
