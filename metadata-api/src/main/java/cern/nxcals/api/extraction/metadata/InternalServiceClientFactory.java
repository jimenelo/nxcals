/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.api.extraction.metadata;

import cern.nxcals.common.utils.Lazy;
import cern.nxcals.internal.extraction.metadata.InternalAuthorizationService;
import cern.nxcals.internal.extraction.metadata.InternalCompactionService;
import cern.nxcals.internal.extraction.metadata.InternalEntityResourceService;
import cern.nxcals.internal.extraction.metadata.InternalEntitySchemaService;
import cern.nxcals.internal.extraction.metadata.InternalEntityService;
import cern.nxcals.internal.extraction.metadata.InternalGroupService;
import cern.nxcals.internal.extraction.metadata.InternalHierarchyService;
import cern.nxcals.internal.extraction.metadata.InternalPartitionResourceHistoryService;
import cern.nxcals.internal.extraction.metadata.InternalPartitionResourceService;
import cern.nxcals.internal.extraction.metadata.InternalPartitionService;
import cern.nxcals.internal.extraction.metadata.InternalSystemSpecService;
import cern.nxcals.internal.extraction.metadata.InternalVariableService;
import lombok.experimental.UtilityClass;

/**
 * WARNING: THIS IS NOT A PUBLIC API! INTERNAL USE ONLY, SUBJECT TO CHANGE AT ANY MOMENT, Please use ServiceClientFactory for public API.
 */
@SuppressWarnings("squid:UndocumentedApi")
@UtilityClass
public final class InternalServiceClientFactory {
    private static final Lazy<InternalSystemSpecService> systemSpecService = new Lazy<>(() -> new SystemSpecProvider(InternalClientFactory.createSystemSpecService()));
    private static final Lazy<InternalPartitionService> partitionService = new Lazy<>(() -> new PartitionProvider(InternalClientFactory.createPartitionService()));
    private static final Lazy<InternalEntitySchemaService> entitySchemaService = new Lazy<>(() -> new EntitySchemaProvider(InternalClientFactory.createEntitySchemaService()));
    private static final Lazy<InternalEntityService> entityService = new Lazy<>(() -> new EntityProvider(InternalClientFactory.createEntityService(), InternalServiceClientFactory.createSystemSpecService()));
    private static final Lazy<InternalEntityResourceService> resourceService = new Lazy<>(() -> new EntityResourceProvider(InternalClientFactory.createResourceService()));
    private static final Lazy<InternalGroupService> groupService = new Lazy<>(() -> new GroupProvider(InternalClientFactory.createGroupService()));
    private static final Lazy<InternalHierarchyService> hierarchyService = new Lazy<>(() -> new HierarchyProvider(InternalClientFactory.createHierarchyService()));
    private static final Lazy<InternalVariableService> variableService = new Lazy<>(() -> new VariableProvider(InternalClientFactory.createVariableService()));
    private static final Lazy<InternalCompactionService> compactionService = new Lazy<>(() -> new CompactionProvider(InternalClientFactory.createCompactionService()));
    private static final Lazy<InternalAuthorizationService> authorizationService = new Lazy<>(() -> new AuthorizationProvider(InternalClientFactory.createAuthorizationService()));
    private static final Lazy<InternalPartitionResourceService> partitionResourceService = new Lazy<>(() -> new PartitionResourceProvider(InternalClientFactory.createPartitionResourceService()));
    private static final Lazy<InternalPartitionResourceHistoryService> partitionResourceHistoryService = new Lazy<>(() -> new PartitionResourceHistoryProvider(InternalClientFactory.createPartitionResourceHistoryService()));


    public static InternalEntitySchemaService createEntitySchemaService() {
        return entitySchemaService.get();
    }

    public static InternalEntityService createEntityService() {
        return entityService.get();
    }

    public static InternalPartitionService createPartitionService() {
        return partitionService.get();
    }

    public static InternalSystemSpecService createSystemSpecService() {
        return systemSpecService.get();
    }

    public static InternalGroupService createGroupService() {
        return groupService.get();
    }

    public static InternalHierarchyService createHierarchyService() {
        return hierarchyService.get();
    }

    public static InternalEntityResourceService createEntityResourceService() {
        return resourceService.get();
    }

    public static InternalVariableService createVariableService() {
        return variableService.get();
    }

    public static InternalCompactionService createCompactionService() {
        return compactionService.get();
    }

    public static InternalAuthorizationService createAuthorizationService() {return authorizationService.get();}
    public static InternalPartitionResourceService createPartitionResourceService() {
        return partitionResourceService.get();
    }

    public static InternalPartitionResourceHistoryService createPartitionResourceHistoryService() {
        return partitionResourceHistoryService.get();
    }

}