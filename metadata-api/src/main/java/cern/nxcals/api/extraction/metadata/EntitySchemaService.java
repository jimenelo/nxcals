/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.api.extraction.metadata;

import cern.nxcals.api.domain.EntitySchema;
import cern.nxcals.api.extraction.metadata.queries.EntitySchemas;

public interface EntitySchemaService extends Queryable<EntitySchema, EntitySchemas> {
}