package cern.nxcals.api.extraction.metadata;

import cern.nxcals.api.domain.PartitionResourceHistory;
import cern.nxcals.api.extraction.metadata.feign.PartitionResourceHistoryClient;
import cern.nxcals.api.extraction.metadata.queries.PartitionResourceHistories;
import cern.nxcals.internal.extraction.metadata.InternalPartitionResourceHistoryService;

import java.util.Optional;

public class PartitionResourceHistoryProvider
        extends AbstractProvider<PartitionResourceHistory, PartitionResourceHistoryClient, PartitionResourceHistories>
        implements InternalPartitionResourceHistoryService {

    PartitionResourceHistoryProvider(PartitionResourceHistoryClient httpClient) {
        super(httpClient);
    }

    @Override
    public Optional<PartitionResourceHistory> findById(long id) {
        return findOne(PartitionResourceHistories.suchThat().id().eq(id));
    }

    @Override
    public PartitionResourceHistory create(PartitionResourceHistory partitionResourceInfo) {
        return getHttpClient().create(partitionResourceInfo);
    }

    @Override
    public PartitionResourceHistory update(PartitionResourceHistory partitionResourceInfo) {
        return getHttpClient().update(partitionResourceInfo);
    }

    @Override
    public void delete(long partitionResourceInfoId){
        getHttpClient().delete(partitionResourceInfoId);
    }

}