package cern.nxcals.api.extraction.metadata.security.kerberos;

public interface KerberosContext {
    String requestTokenFor(String servicePrincipal);
}
