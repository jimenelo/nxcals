/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.api.extraction.metadata;

import cern.nxcals.api.extraction.metadata.feign.EntityResourceClient;
import cern.nxcals.common.domain.ExtractionCriteria;
import cern.nxcals.common.domain.ExtractionUnit;
import cern.nxcals.internal.extraction.metadata.InternalEntityResourceService;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
class EntityResourceProvider implements InternalEntityResourceService {
    private final EntityResourceClient httpService;

    @Override
    public ExtractionUnit findBy(ExtractionCriteria criteria) {
        return httpService.findBy(criteria);
    }
}
