package cern.nxcals.api.extraction.metadata.feign;

import cern.nxcals.api.exceptions.AccessDeniedException;
import cern.nxcals.api.exceptions.FatalDataConflictRuntimeException;
import cern.nxcals.api.exceptions.NotFoundRuntimeException;
import cern.nxcals.api.exceptions.UnauthorizedException;
import cern.nxcals.common.errors.ApiError;
import cern.nxcals.common.utils.ConfigHolder;
import cern.nxcals.common.utils.GlobalObjectMapperProvider;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.annotations.VisibleForTesting;
import feign.Feign;
import feign.Request;
import feign.Response;
import feign.Response.Body;
import feign.Util;
import feign.codec.ErrorDecoder;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpStatus;

import java.io.IOException;
import java.io.Reader;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

import static cern.nxcals.api.extraction.metadata.security.PropertiesKeys.SERVICE_CONNECT_TIMEOUT_CONFIG;
import static cern.nxcals.api.extraction.metadata.security.PropertiesKeys.SERVICE_READ_TIMEOUT_CONFIG;
import static java.lang.String.format;

@Slf4j
public enum FeignBuilderProvider implements Supplier<Feign.Builder> {
    INSTANCE;

    private static final String UNAUTHORIZED = "Unauthorized call to remote service for method=%s, status=%s";
    private static final String FORBIDDEN = "Forbidden call to remote service for method=%s, status=%s";
    private static final String ILLEGAL = "Illegal call to remote service for method=%s, status=%s: %s";
    private static final String FATAL_CONFLICT = "Conflict detected for method=%s, status=%s: %s";
    private static final String GENERIC = "Unsuccessful call to remote service for method=%s, status=%s: %s";
    private static final String NOT_FOUND = "Not found: %s, status: %s: %s";

    private final ServiceClientErrorDecoder serviceClientErrorDecoder = new ServiceClientErrorDecoder();
    private final Request.Options options = createOptions();

    @Override
    public Feign.Builder get() {
        return Feign.builder()
                .requestInterceptor(new CallDetailsInterceptor())
                .options(options)
                .encoder(new JacksonEncoder(GlobalObjectMapperProvider.get()))
                .decoder(new JacksonDecoderBackport(GlobalObjectMapperProvider.get()))
                .errorDecoder(serviceClientErrorDecoder);
    }

    private Request.Options createOptions() {
        return new Request.Options(
                ConfigHolder.getInt(SERVICE_CONNECT_TIMEOUT_CONFIG.getPathInProperties(), 10 * 1000),
                TimeUnit.MILLISECONDS,
                ConfigHolder.getInt(SERVICE_READ_TIMEOUT_CONFIG.getPathInProperties(), 60 * 1000),
                TimeUnit.MILLISECONDS, true);
    }

    @VisibleForTesting
    static class ServiceClientErrorDecoder implements ErrorDecoder {
        @Override
        public Exception decode(String methodKey, Response response) {
            try {
                Body body = response.body();
                ApiError error;
                if (body != null) {
                    Reader reader = body.asReader(StandardCharsets.UTF_8);
                    error = GlobalObjectMapperProvider.get().readValue(reader, ApiError.class);
                } else {
                    error = null;
                }
                return exception(methodKey, response, error);
            } catch (Exception exception) {
                log.warn("Cannot read response body for {}", methodKey, exception);
                return generic(methodKey, response, exception.toString());
            }
        }

        private Exception exception(String methodKey, Response response, ApiError error) {
            switch (response.status()) {
            case HttpStatus.SC_UNAUTHORIZED:
                return unauthorized(methodKey, response);
            case HttpStatus.SC_FORBIDDEN:
                return forbidden(methodKey, response);
            case HttpStatus.SC_CONFLICT:
                return fatal(methodKey, response, error.toString());
            case HttpStatus.SC_BAD_REQUEST:
                return illegal(methodKey, response, error.toString());
            case HttpStatus.SC_NOT_FOUND:
                return notFound(methodKey, response, error.toString());
            default:
                if (error.isRepeatable()) {
                    return generic(methodKey, response, error.toString());
                } else {
                    return illegal(methodKey, response, error.toString());
                }
            }
        }

        private Exception unauthorized(String methodKey, Response response) {
            return new UnauthorizedException(format(UNAUTHORIZED, methodKey, response.status()));
        }

        private Exception forbidden(String methodKey, Response response) {
            return new AccessDeniedException(format(FORBIDDEN, methodKey, response.status()));
        }

        public Exception illegal(String methodKey, Response response, String error) {
            return new IllegalArgumentException(format(ILLEGAL, methodKey, response.status(), error));
        }

        private Exception fatal(String methodKey, Response response, String error) {
            return new FatalDataConflictRuntimeException(format(FATAL_CONFLICT, methodKey, response.status(), error));
        }

        private Exception generic(String methodKey, Response response, String error) {
            return new RuntimeException(format(GENERIC, methodKey, response.status(), error));
        }

        private Exception notFound(String methodKey, Response response, String error) {
            return new NotFoundRuntimeException(format(NOT_FOUND, methodKey, response.status(), error));
        }
    }

    /**
     * Maintains compatibility with older JacksonDecoder (changed since v10.3.0), that was capable of
     * decoding 404 messages to empty placeholders.
     *
     * @see <a href="https://github.com/OpenFeign/feign/blob/10.2.3/jackson/src/main/java/feign/jackson/JacksonDecoder.java#L48"></a>
     */
    private static class JacksonDecoderBackport extends JacksonDecoder {
        public JacksonDecoderBackport(ObjectMapper mapper) {
            super(mapper);
        }

        @Override
        public Object decode(Response response, Type type) throws IOException {
            if (response.status() == HttpStatus.SC_NOT_FOUND) {
                return Util.emptyValueOf(type);
            }
            return super.decode(response, type);
        }
    }
}
