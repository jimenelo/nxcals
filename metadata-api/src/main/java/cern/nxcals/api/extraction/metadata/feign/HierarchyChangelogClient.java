package cern.nxcals.api.extraction.metadata.feign;

import cern.nxcals.api.domain.HierarchyChangelog;
import feign.Body;
import feign.Headers;
import feign.Param;
import feign.RequestLine;

import java.util.Set;

import static cern.nxcals.common.web.Endpoints.HIERARCHIES_CHANGELOGS_FIND_ALL;
import static cern.nxcals.common.web.HttpHeaders.ACCEPT_APPLICATION_JSON;
import static cern.nxcals.common.web.HttpHeaders.CONTENT_TYPE_TEXT_PLAIN;
import static cern.nxcals.common.web.HttpVerbs.POST;

/**
 * Feign declarative service interface for consuming HierarchyChangelog service.
 */
@SuppressWarnings("squid:UndocumentedApi")
public interface HierarchyChangelogClient extends FeignQuerySupport<HierarchyChangelog> {

    @Headers({ ACCEPT_APPLICATION_JSON, CONTENT_TYPE_TEXT_PLAIN })
    @RequestLine(POST + HIERARCHIES_CHANGELOGS_FIND_ALL)
    @Body("{condition}")
    Set<HierarchyChangelog> findAll(@Param("condition") String condition);

}
