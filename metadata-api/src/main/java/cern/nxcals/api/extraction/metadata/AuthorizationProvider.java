/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.api.extraction.metadata;

import cern.nxcals.api.custom.domain.DelegationToken;
import cern.nxcals.api.extraction.metadata.feign.AuthorizationClient;
import cern.nxcals.internal.extraction.metadata.InternalAuthorizationService;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
class AuthorizationProvider implements InternalAuthorizationService {

    private final AuthorizationClient authorizationClient;

    @Override
    public DelegationToken createDelegationToken() {
        return authorizationClient.createDelegationToken();
    }

}
