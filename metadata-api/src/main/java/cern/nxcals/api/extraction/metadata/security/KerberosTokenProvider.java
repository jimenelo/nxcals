package cern.nxcals.api.extraction.metadata.security;

import cern.nxcals.api.extraction.metadata.security.kerberos.KerberosContext;
import cern.nxcals.common.utils.Lazy;
import feign.Request;
import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.net.URI;
import java.util.Optional;

@Slf4j
@RequiredArgsConstructor
@Builder
public class KerberosTokenProvider implements AuthTokenProvider {
    public static final String NEGOTIATE = "Negotiate ";
    private final Lazy<KerberosContext> kerberosContext;
    private final String serviceType;

    @Override
    public Optional<String> apply(Request request) {
        try {
            log.debug("Trying to authenticate using Kerberos...");
            String servicePrincipal = serviceType + "@" + URI.create(request.url()).getHost();
            String token = kerberosContext.get().requestTokenFor(servicePrincipal);
            return Optional.of(NEGOTIATE + token);
        } catch (Exception ex) {
            log.debug("Cannot obtain token from Kerberos", ex);
            throw new KerberosAuthenticationException("Cannot obtain token from Kerberos", ex);
        }
    }

    @Override
    public String toString() {
        return "kerberos";
    }

}
