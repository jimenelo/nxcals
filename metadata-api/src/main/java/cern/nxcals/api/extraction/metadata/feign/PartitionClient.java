/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.api.extraction.metadata.feign;

import cern.nxcals.api.domain.Partition;
import feign.Body;
import feign.Headers;
import feign.Param;
import feign.RequestLine;

import java.util.Set;

import static cern.nxcals.common.web.Endpoints.PARTITIONS;
import static cern.nxcals.common.web.Endpoints.PARTITIONS_FIND_ALL;
import static cern.nxcals.common.web.HttpHeaders.ACCEPT_APPLICATION_JSON;
import static cern.nxcals.common.web.HttpHeaders.CONTENT_TYPE_APPLICATION_JSON;
import static cern.nxcals.common.web.HttpHeaders.CONTENT_TYPE_TEXT_PLAIN;
import static cern.nxcals.common.web.HttpVerbs.POST;
import static cern.nxcals.common.web.HttpVerbs.PUT;

/**
 * Feign declarative service interface for consuming Partition service.
 */
@SuppressWarnings("squid:UndocumentedApi")
public interface PartitionClient extends FeignQuerySupport<Partition>{
    @Headers({ ACCEPT_APPLICATION_JSON, CONTENT_TYPE_TEXT_PLAIN})
    @RequestLine(POST + PARTITIONS_FIND_ALL)
    @Body("{condition}")
    @Override
    Set<Partition> findAll(@Param("condition") String condition);

    @Headers({ ACCEPT_APPLICATION_JSON, CONTENT_TYPE_APPLICATION_JSON })
    @RequestLine(value = PUT + PARTITIONS)
    Partition update(Partition partition);

    @Headers({ ACCEPT_APPLICATION_JSON, CONTENT_TYPE_APPLICATION_JSON })
    @RequestLine(POST + PARTITIONS)
    Partition create(Partition partition);
}
