/*
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.api.extraction.metadata;

import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.extraction.metadata.queries.Variables;
import cern.nxcals.api.metadata.queries.ConditionWithOptions;
import cern.nxcals.api.metadata.queries.VariableQueryWithOptions;
import cern.nxcals.common.annotation.Experimental;
import cern.nxcals.common.utils.MapUtils;
import cern.nxcals.common.utils.SetUtils;
import org.apache.avro.Schema;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.function.UnaryOperator.identity;

public interface VariableService extends Queryable<Variable, Variables> {
    /**
     * Creates variable as in the parameter
     * NOTE: this is a mutative method. It is not thread-safe or meant to be used in parallel
     *
     * @param variable to be created
     * @return Created variable
     */
    Variable create(Variable variable);

    /**
     * Updates an existing variable to the new values as in the parameter
     * NOTE: this is a mutative method. It is not thread-safe or meant to be used in parallel
     *
     * @param variable to be updated
     * @return Updated variable
     */
    Variable update(Variable variable);

    /**
     * Creates the given set of variables
     * NOTE: this is a mutative method. It is not thread-safe or meant to be used in parallel
     *
     * @param variables to be created
     * @return Created variables
     */
    Set<Variable> createAll(Set<Variable> variables);

    /**
     * Updates existing variables based on the provided set
     * NOTE: this is a mutative method. It is not thread-safe or meant to be used in parallel
     *
     * @param variables to be updated
     * @return Updated variables
     */
    Set<Variable> updateAll(Set<Variable> variables);

    /**
     * Deletes an existing variable
     *
     * @param variableId the id of the variable to be deleted
     */
    void delete(long variableId);

    /**
     * Deletes an existing set of variables
     *
     * @param variableIds the ids of the variables to be deleted
     */
    void deleteAll(Set<Long> variableIds);

    /**
     * Find all, but allows to pass additional options - experimental
     */
    @Experimental
    List<Variable> findAll(VariableQueryWithOptions condition);

    /**
     * Find one, but allows to pass additional options - experimental
     */
    @Experimental
    Optional<Variable> findOne(VariableQueryWithOptions condition);

    /**
     * Find all, which matches condition - experimental
     */
    @Experimental
    Set<Variable> findAll(
            ConditionWithOptions<cern.nxcals.api.metadata.queries.Variables, VariableQueryWithOptions> condition);

    /**
     * Find one or zero, which matches condition - experimental
     */
    @Experimental
    Optional<Variable> findOne(
            ConditionWithOptions<cern.nxcals.api.metadata.queries.Variables, VariableQueryWithOptions> condition);

    /**
     * Returns schemas for given variable
     */
    @Experimental
    Map<TimeWindow, Schema> findSchemasById(long variableId, TimeWindow timeWindow);

    /**
     * Returns schemas for given variable
     */
    @Experimental
    default Map<TimeWindow, Schema> findSchemas(Variable variable, TimeWindow timeWindow) {
        return findSchemasById(variable.getId(), timeWindow);
    }

    /**
     * Returns schemas for given variables
     */
    @Experimental
    Map<Long, Map<TimeWindow, Schema>> findSchemasById(Set<Long> variableIds, TimeWindow timeWindow);

    /**
     * Returns schemas for given variables
     */
    @Experimental
    default Map<Variable, Map<TimeWindow, Schema>> findSchemas(Set<Variable> variables, TimeWindow timeWindow) {
        Map<Long, Map<TimeWindow, Schema>> foundById = findSchemasById(SetUtils.map(variables, Variable::getId),
                timeWindow);
        Map<Long, Variable> idToVariable = variables.stream().collect(Collectors.toMap(Variable::getId, identity()));
        return MapUtils.mapKeys(foundById, idToVariable::get);
    }
}
