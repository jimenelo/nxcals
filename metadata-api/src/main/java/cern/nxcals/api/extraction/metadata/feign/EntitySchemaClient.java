/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.api.extraction.metadata.feign;

import cern.nxcals.api.domain.EntitySchema;
import feign.Body;
import feign.Headers;
import feign.Param;
import feign.RequestLine;

import java.util.Set;

import static cern.nxcals.common.web.Endpoints.SCHEMAS_FIND_ALL;
import static cern.nxcals.common.web.HttpHeaders.ACCEPT_APPLICATION_JSON;
import static cern.nxcals.common.web.HttpHeaders.CONTENT_TYPE_TEXT_PLAIN;
import static cern.nxcals.common.web.HttpVerbs.POST;

/**
 * Feign declarative service interface for consuming Schema service.
 */
@SuppressWarnings("squid:UndocumentedApi")
public interface EntitySchemaClient extends FeignQuerySupport<EntitySchema> {
    @Headers({ ACCEPT_APPLICATION_JSON, CONTENT_TYPE_TEXT_PLAIN})
    @RequestLine(POST + SCHEMAS_FIND_ALL)
    @Body("{condition}")
    @Override
    Set<EntitySchema> findAll(@Param("condition") String condition);
}
