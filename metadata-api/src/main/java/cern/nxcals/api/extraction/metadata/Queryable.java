package cern.nxcals.api.extraction.metadata;

import com.github.rutledgepaulv.qbuilders.builders.QBuilder;
import com.github.rutledgepaulv.qbuilders.conditions.Condition;

import java.util.Optional;
import java.util.Set;

/**
 * Query interface defining methods available on all services used for searching.
 * There are 2 methods: search for multiple values and search for one value.
 * @param <S> - return type
 * @param <Q> - metadata type
 */
public interface Queryable<S, Q extends QBuilder<Q>> {
    /**
     * Searching for multiple values.
     * @param condition
     * @return
     */
    Set<S> findAll(Condition<Q> condition);

    /**
     * Searching for one value. If the metadata returns multiple values the exception is thrown (@see {@link IllegalStateException})
     * @param condition
     * @return Optional with value or empty.
     * @throws IllegalStateException if found multiple values.
     */
    Optional<S> findOne(Condition<Q> condition);

    Optional<S> findById(long id);
}