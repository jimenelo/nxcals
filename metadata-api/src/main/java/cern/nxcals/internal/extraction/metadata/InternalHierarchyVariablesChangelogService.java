package cern.nxcals.internal.extraction.metadata;

import cern.nxcals.api.extraction.metadata.HierarchyVariablesChangelogService;

public interface InternalHierarchyVariablesChangelogService extends HierarchyVariablesChangelogService {
}
