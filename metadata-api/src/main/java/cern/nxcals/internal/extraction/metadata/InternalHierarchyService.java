package cern.nxcals.internal.extraction.metadata;

import cern.nxcals.api.extraction.metadata.HierarchyService;

public interface InternalHierarchyService extends HierarchyService {
}
