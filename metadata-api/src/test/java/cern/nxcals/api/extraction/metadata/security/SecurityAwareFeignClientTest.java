package cern.nxcals.api.extraction.metadata.security;

import com.google.common.collect.Lists;
import feign.Request;
import feign.RequestTemplate;
import org.apache.http.HttpHeaders;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSocketFactory;
import java.io.IOException;
import java.util.HashMap;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class SecurityAwareFeignClientTest {

    public static final String AUTH_STRING = "AuthString";
    @Mock
    private HostnameVerifier hostnameVerifier;
    @Mock
    private SSLSocketFactory sslSocketFactory;

    @Mock
    private AuthTokenProvider authProvider1, authProvider2;

    private Request request;

    @BeforeEach
    public void setUp() {
        request = Request.create(Request.HttpMethod.GET, "https://service.com", new HashMap<>(), Request.Body.empty(), new RequestTemplate());
    }

    @Test
    public void shouldExecuteProviders() throws IOException {
        //given
        when(authProvider1.apply(request)).thenReturn(Optional.of(AUTH_STRING));
        SecurityAwareFeignClient securityAwareFeignClient = SecurityAwareFeignClient.builder()
                .hostnameVerifier(hostnameVerifier)
                .sslContextFactory(sslSocketFactory)
                .authProviders(Lists.newArrayList(authProvider1, authProvider2))
                .build();

        //when

        Request r = securityAwareFeignClient.applyAuth(request);

        //then
        assertEquals(AUTH_STRING, r.headers().get((HttpHeaders.AUTHORIZATION)).iterator().next());
    }

    @Test
    public void shouldExecuteProvidersUntilFound() throws IOException {
        //given
        when(authProvider1.apply(request)).thenReturn(Optional.empty());
        when(authProvider2.apply(request)).thenReturn(Optional.of(AUTH_STRING));

        SecurityAwareFeignClient securityAwareFeignClient = SecurityAwareFeignClient.builder()
                .hostnameVerifier(hostnameVerifier)
                .sslContextFactory(sslSocketFactory)
                .authProviders(Lists.newArrayList(authProvider1, authProvider2))
                .build();

        //when

        Request r = securityAwareFeignClient.applyAuth(request);

        //then
        assertEquals(AUTH_STRING, r.headers().get((HttpHeaders.AUTHORIZATION)).iterator().next());
    }

    @Test
    public void shouldThrowIfAuthNotFound() throws IOException {
        //given
        when(authProvider1.apply(request)).thenReturn(Optional.empty());
        when(authProvider2.apply(request)).thenReturn(Optional.empty());

        SecurityAwareFeignClient securityAwareFeignClient = SecurityAwareFeignClient.builder()
                .hostnameVerifier(hostnameVerifier)
                .sslContextFactory(sslSocketFactory)
                .authProviders(Lists.newArrayList(authProvider1, authProvider2))
                .build();

        //when
        assertThrows(IllegalStateException.class, () -> securityAwareFeignClient.applyAuth(request));

        //then exception

    }
}