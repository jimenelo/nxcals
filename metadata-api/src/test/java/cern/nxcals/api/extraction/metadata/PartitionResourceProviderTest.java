package cern.nxcals.api.extraction.metadata;

import cern.nxcals.api.domain.EntitySchema;
import cern.nxcals.api.domain.Partition;
import cern.nxcals.api.domain.PartitionResource;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.extraction.metadata.feign.PartitionResourceClient;
import cern.nxcals.api.extraction.metadata.queries.PartitionResources;
import cern.nxcals.common.utils.ReflectionUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.Optional;

import static cern.nxcals.common.utils.RSQLUtils.toRSQL;
import static java.util.Collections.singletonMap;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.internal.verification.VerificationModeFactory.times;

@ExtendWith(MockitoExtension.class)
public class PartitionResourceProviderTest {
    private PartitionResourceProvider partitionResourceProvider;

    @Mock
    private PartitionResourceClient httpClient;
    private SystemSpec system;
    private Partition partition;
    private EntitySchema schema;


    @BeforeEach
    public void init() {
        partitionResourceProvider = new PartitionResourceProvider(httpClient);
        system = systemSpec("my-system");
        partition = partition("my-partition");
        schema = schema("my-schema");
        reset(httpClient);
    }

    @Test
    public void shouldCreate() {
        final long id = 1L;
        final long recVersion = 2L;

        PartitionResource partitionResourceOld = partitionResource(id, system, partition, schema);

        when(httpClient.create(partitionResourceOld)).thenReturn(partitionResource(id, system, partition, schema, recVersion));

        PartitionResource partitionResourceNew = partitionResourceProvider.create(partitionResourceOld);

        verify(httpClient, times(1)).create(partitionResourceOld);
        assertThat(partitionResourceNew).isEqualTo(partitionResource(id, system, partition, schema));
        assertThat(partitionResourceNew.getId()).isEqualTo(id);
        assertThat(partitionResourceNew.getSystemSpec()).isEqualTo(system);
        assertThat(partitionResourceNew.getPartition()).isEqualTo(partition);
        assertThat(partitionResourceNew.getSchema()).isEqualTo(schema);
        assertThat(partitionResourceNew.getRecVersion()).isEqualTo(recVersion);
    }

    @Test
    public void shouldUpdate() {
        final long id = 1L;
        final long recVersion = 2L;

        PartitionResource partitionResourceOld = partitionResource(id, system, partition, schema);
        when(httpClient.update(partitionResourceOld)).thenReturn(partitionResource(id, system, partition, schema, recVersion));

        PartitionResource partitionResourceNew = partitionResourceProvider.update(partitionResourceOld);

        verify(httpClient, times(1)).update(partitionResourceOld);
        assertThat(partitionResourceNew).isEqualTo(partitionResource(id, system, partition, schema, recVersion));
        assertThat(partitionResourceNew.getId()).isEqualTo(id);
        assertThat(partitionResourceNew.getSystemSpec()).isEqualTo(system);
        assertThat(partitionResourceNew.getPartition()).isEqualTo(partition);
        assertThat(partitionResourceNew.getSchema()).isEqualTo(schema);
        assertThat(partitionResourceNew.getRecVersion()).isEqualTo(recVersion);
    }

    @Test
    public void shouldFindById() {
        final long id = 1L;
        final long recVersion = 2L;

        String condition = toRSQL(PartitionResources.suchThat().id().eq(id));
        when(httpClient.findAll(condition)).thenReturn(Collections.singleton(partitionResource(id, system, partition,
                schema, recVersion)));

        Optional<PartitionResource> partitionResourceNew = partitionResourceProvider.findById(id);

        assertThat(partitionResourceNew).isPresent();

        verify(httpClient, times(1)).findAll(condition);

        assertThat(partitionResourceNew.get()).isEqualTo(partitionResource(id, system, partition, schema, recVersion));
        assertThat(partitionResourceNew.get().getId()).isEqualTo(id);
        assertThat(partitionResourceNew.get().getSystemSpec()).isEqualTo(system);
        assertThat(partitionResourceNew.get().getPartition()).isEqualTo(partition);
        assertThat(partitionResourceNew.get().getSchema()).isEqualTo(schema);
        assertThat(partitionResourceNew.get().getRecVersion()).isEqualTo(recVersion);
    }

    @Test
    public void shouldDelete() {
        final long partitionResourceId = 1L;
        doNothing().when(httpClient).delete(partitionResourceId);

        partitionResourceProvider.delete(partitionResourceId);

        verify(httpClient, times(1)).delete(partitionResourceId);
    }

    // helper methods

    private PartitionResource partitionResource(long id, SystemSpec systemSpec, Partition partition, EntitySchema schema) {
        return ReflectionUtils.builderInstance(PartitionResource.InnerBuilder.class)
                .id(id)
                .systemSpec(systemSpec)
                .partition(partition)
                .schema(schema)
                .build();
    }

    private PartitionResource partitionResource(long id, SystemSpec systemSpec, Partition partition, EntitySchema schema,
            long recVersion) {
        return ReflectionUtils.builderInstance(PartitionResource.InnerBuilder.class)
                .id(id)
                .systemSpec(systemSpec)
                .partition(partition)
                .schema(schema)
                .recVersion(recVersion)
                .build();
    }

    private SystemSpec systemSpec(String name) {
        return SystemSpec.builder().name(name).partitionKeyDefinitions("").timeKeyDefinitions("").entityKeyDefinitions("").build();
    }

    private Partition partition(String name) {
        return Partition.builder().keyValues(singletonMap("a", "b")).systemSpec(system).build();
    }

    private EntitySchema schema(String schemaJson) {
        return EntitySchema.builder().schemaJson(schemaJson).build();
    }
}


