/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.api.extraction.metadata;

import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.extraction.metadata.feign.SystemSpecClient;
import cern.nxcals.api.extraction.metadata.queries.SystemSpecs;
import cern.nxcals.common.utils.ReflectionUtils;
import com.github.rutledgepaulv.qbuilders.conditions.Condition;
import com.google.common.collect.Sets;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.Set;

import static cern.nxcals.common.utils.RSQLUtils.toRSQL;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Marcin Sobieszek
 * @date Jul 22, 2016 4:13:37 PM
 */
@ExtendWith(MockitoExtension.class)
public class SystemSpecProviderTest extends AbstractProviderTest {
    private SystemSpecProvider systemProvider;

    @Mock
    private SystemSpecClient httpService;

    private Set<SystemSpec> systems = Sets.newHashSet(SystemSpec.builder().name("system").entityKeyDefinitions("").partitionKeyDefinitions("").timeKeyDefinitions("").build());

    @BeforeEach
    public void setup() {
        this.systemProvider = new SystemSpecProvider(httpService);
        reset(httpService);
    }

    @Test
    public void shouldNotObtainSystemForNonExistingName() {
        Condition<SystemSpecs> query = SystemSpecs.suchThat().name().eq(SYSTEM_NAME);
        String queryString = toRSQL(query);

        when(this.httpService.findAll(queryString)).thenReturn(Collections.emptySet());
        SystemSpec data = this.systemProvider.findByName(SYSTEM_NAME).orElse(null);
        assertNull(data);
        verify(this.httpService, times(1)).findAll(queryString);
    }

    @Test
    public void shouldObtainSystemForExistingName() {

        for (long i = 0; i < 10; i++) {
            String name = "test" + i;
            SystemSpec identity = ReflectionUtils.builderInstance(SystemSpec.InnerBuilder.class).id(i).name(name).entityKeyDefinitions(SCHEMA1.toString())
                    .partitionKeyDefinitions(PARTITION_SCHEMA.toString()).timeKeyDefinitions(TIME_KEY_SCHEMA.toString())
                    .build();
            Condition<SystemSpecs> query = SystemSpecs.suchThat().name().eq(name);
            String queryString = toRSQL(query);

            when(this.httpService.findAll(queryString)).thenReturn(Sets.newHashSet(identity));
            SystemSpec data = this.systemProvider.findByName(name).orElseThrow(() -> new IllegalArgumentException("No such system name " + name));
            assertNotNull(data);
            assertEquals(identity, data);

            data = this.systemProvider.findByName(name).orElseThrow(() -> new IllegalArgumentException("No such system name " + name));
            assertNotNull(data);
            assertEquals(identity, data);
            verify(this.httpService, times(1)).findAll(queryString);

        }
    }

    @Test
    public void shouldFindAllSystems() {
        //when
        String queryString = toRSQL(SystemSpecs.suchThat().id().exists());
        when(this.httpService.findAll(queryString)).thenReturn(systems);

        //then
        Set<SystemSpec> systems = systemProvider.findAll();
        assertNotNull(systems);
        assertEquals(1,systems.size());

    }
}
