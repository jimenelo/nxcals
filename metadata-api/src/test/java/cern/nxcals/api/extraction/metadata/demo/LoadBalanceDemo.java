/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.api.extraction.metadata.demo;

import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.KeyValues;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.extraction.metadata.DomainTestConstants;
import cern.nxcals.api.extraction.metadata.InternalServiceClientFactory;
import cern.nxcals.api.extraction.metadata.ServiceClientFactory;
import cern.nxcals.api.extraction.metadata.SystemSpecService;
import cern.nxcals.api.extraction.metadata.TestSchemas;
import cern.nxcals.api.extraction.metadata.queries.SystemSpecs;
import cern.nxcals.internal.extraction.metadata.InternalEntityService;

import java.util.concurrent.TimeUnit;


public class LoadBalanceDemo {
    static {
        String USER = System.getProperty("user.name");
        String USER_HOME = System.getProperty("user.home");
        System.setProperty("service.url", "http://nxcals-" + USER + "1:19093,http://nxcals-" + USER + "2:19093");
        System.setProperty("kerberos.principal", USER);
        System.setProperty("kerberos.keytab", USER_HOME + "/.keytab");
    }

    @SuppressWarnings("java:S2925")
    public static void main(String[] args) throws Exception {
        SystemSpecService systemService = ServiceClientFactory.createSystemSpecService();
        SystemSpec systemSpec = systemService.findOne(SystemSpecs.suchThat().name().eq("MOCK-SYSTEM")).orElseThrow(() -> new IllegalArgumentException("No such system"));
        System.out.println(systemSpec);

        InternalEntityService entityService = InternalServiceClientFactory.createEntityService();

        KeyValues entityKeyValues = new KeyValues(1, DomainTestConstants.ENTITY_KEY_VALUES);
        KeyValues partitionKeyValues = new KeyValues(0, DomainTestConstants.PARTITION_KEY_VALUES);

        //Please stop one of the services (on one machine to see if the balancing works)
        for (int i = 0; i < 120; i++) {
            Entity entityData1 = entityService.findOrCreateEntityFor(systemSpec.getId(), entityKeyValues,
                    partitionKeyValues, TestSchemas.RECORD_VERSION_SCHEMA.toString(), System.currentTimeMillis() * 1000_000);
            System.out.println("EntityId=" + entityData1.getId());
            TimeUnit.SECONDS.sleep(3);
        }
    }
}
