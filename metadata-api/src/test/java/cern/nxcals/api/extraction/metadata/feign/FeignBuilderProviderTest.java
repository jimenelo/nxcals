package cern.nxcals.api.extraction.metadata.feign;

import cern.nxcals.api.exceptions.FatalDataConflictRuntimeException;
import cern.nxcals.api.exceptions.NotFoundRuntimeException;
import cern.nxcals.api.exceptions.UnauthorizedException;
import cern.nxcals.common.errors.ApiError;
import cern.nxcals.common.utils.GlobalObjectMapperProvider;
import com.fasterxml.jackson.databind.ObjectMapper;
import feign.Request;
import feign.RequestTemplate;
import feign.Response;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.HashMap;

import static org.apache.http.HttpStatus.SC_BAD_REQUEST;
import static org.apache.http.HttpStatus.SC_CONFLICT;
import static org.apache.http.HttpStatus.SC_NOT_FOUND;
import static org.apache.http.HttpStatus.SC_UNAUTHORIZED;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class FeignBuilderProviderTest {

    private static final String METHOD_KEY = "test_method_key";
    private static final int HTTP_STATUS_PRECONDITION_REQUIRED = 428;
    private static final Request REQUEST = Request
            .create(Request.HttpMethod.GET, "https://", new HashMap<>(), null, null, new RequestTemplate());

    private final FeignBuilderProvider.ServiceClientErrorDecoder decoder = new FeignBuilderProvider.ServiceClientErrorDecoder();
    private final ObjectMapper mapper = GlobalObjectMapperProvider.get();

    @SuppressWarnings("squid:UnusedPrivateMethod")
    private static Object[][] testData() {
        return new Object[][] {
                { SC_BAD_REQUEST, IllegalArgumentException.class },
                { SC_UNAUTHORIZED, UnauthorizedException.class },
                { SC_CONFLICT, FatalDataConflictRuntimeException.class },
                { HTTP_STATUS_PRECONDITION_REQUIRED, RuntimeException.class },
                { SC_NOT_FOUND, NotFoundRuntimeException.class }
        };
    }

    @ParameterizedTest
    @MethodSource("testData")
    public void shouldThrowProperErrors(int httpStatus, Class exceptionClass) throws Exception {
        Response r = Response.builder().status(httpStatus).request(REQUEST).headers(new HashMap<>())
                .body(mapper.writeValueAsString(ApiError.builder().message("any").build()).getBytes())
                .build();
        Exception e = decoder.decode(METHOD_KEY, r);
        assertEquals(exceptionClass, e.getClass());
    }
}
