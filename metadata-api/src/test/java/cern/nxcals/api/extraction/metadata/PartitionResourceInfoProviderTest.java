package cern.nxcals.api.extraction.metadata;

import cern.nxcals.api.domain.EntitySchema;
import cern.nxcals.api.domain.Partition;
import cern.nxcals.api.domain.PartitionResource;
import cern.nxcals.api.domain.PartitionResourceHistory;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.extraction.metadata.feign.PartitionResourceHistoryClient;
import cern.nxcals.api.extraction.metadata.queries.PartitionResourceHistories;
import cern.nxcals.common.utils.ReflectionUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Collections;

import static cern.nxcals.common.utils.RSQLUtils.toRSQL;
import static java.util.Collections.singletonMap;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.internal.verification.VerificationModeFactory.times;

@ExtendWith(MockitoExtension.class)
class PartitionResourceInfoProviderTest {
    private PartitionResourceHistoryProvider partitionResourceInfoProvider;

    @Mock
    private PartitionResourceHistoryClient httpClient;
    private PartitionResource partitionResource;
    private final String information = "Information";
    private final String compactionType = "Compaction Type";
    private final String storageType = "Storage Type";
    private final boolean isFixedSettings = false;
    private final Instant startTime = Instant.now();
    private final Instant endTime = startTime.plus(1, ChronoUnit.HOURS);

    @BeforeEach
    void init() {
        partitionResourceInfoProvider = new PartitionResourceHistoryProvider(httpClient);
        partitionResource = partitionResource();
        reset(httpClient);
    }

    @Test
    void shouldCreate() {
        final long id = 1L;
        final long recVersion = 2L;

        PartitionResourceHistory partitionResourceInfoOld = partitionResourceInformation(id, partitionResource,
                information, compactionType, storageType, isFixedSettings, startTime, endTime, recVersion);

        when(httpClient.create(partitionResourceInfoOld)).thenReturn(partitionResourceInformation(id, partitionResource,
                information, compactionType, storageType, isFixedSettings, startTime, endTime, recVersion));

        PartitionResourceHistory partitionResourceInfoNew = partitionResourceInfoProvider
                .create(partitionResourceInfoOld);

        verify(httpClient, times(1)).create(eq(partitionResourceInfoOld));
        assertThat(partitionResourceInfoNew).isEqualTo(partitionResourceInformation(id, partitionResource, information,
                compactionType, storageType, isFixedSettings, startTime, endTime, recVersion));
        assertThat(partitionResourceInfoNew.getId()).isEqualTo(id);
        assertThat(partitionResourceInfoNew.getPartitionResource()).isEqualTo(partitionResource);
        assertThat(partitionResourceInfoNew.getPartitionInformation()).isEqualTo(information);
        assertThat(partitionResourceInfoNew.getCompactionType()).isEqualTo(compactionType);
        assertThat(partitionResourceInfoNew.getStorageType()).isEqualTo(storageType);
        assertThat(partitionResourceInfoNew.isFixedSettings()).isEqualTo(isFixedSettings);
        assertThat(partitionResourceInfoNew.getValidity()).isEqualTo(TimeWindow.between(startTime, endTime));
        assertThat(partitionResourceInfoNew.getRecVersion()).isEqualTo(recVersion);
    }

    @Test
    void shouldUpdate() {
        final long id = 1L;
        final long recVersion = 2L;

        PartitionResourceHistory partitionResourceInfoOld = partitionResourceInformation(id, partitionResource,
                information, compactionType, storageType, isFixedSettings, startTime, endTime, recVersion);

        when(httpClient.update(partitionResourceInfoOld)).thenReturn(partitionResourceInformation(id, partitionResource,
                information, compactionType, storageType, isFixedSettings, startTime, endTime, recVersion));

        PartitionResourceHistory partitionResourceInfoNew = partitionResourceInfoProvider
                .update(partitionResourceInfoOld);

        verify(httpClient, times(1)).update(eq(partitionResourceInfoOld));
        assertThat(partitionResourceInfoNew).isEqualTo(partitionResourceInformation(id, partitionResource, information,
                compactionType, storageType, isFixedSettings, startTime, endTime, recVersion));
        assertThat(partitionResourceInfoNew.getId()).isEqualTo(id);
        assertThat(partitionResourceInfoNew.getPartitionResource()).isEqualTo(partitionResource);
        assertThat(partitionResourceInfoNew.getPartitionInformation()).isEqualTo(information);
        assertThat(partitionResourceInfoNew.getCompactionType()).isEqualTo(compactionType);
        assertThat(partitionResourceInfoNew.getStorageType()).isEqualTo(storageType);
        assertThat(partitionResourceInfoNew.isFixedSettings()).isEqualTo(isFixedSettings);
        assertThat(partitionResourceInfoNew.getValidity()).isEqualTo(TimeWindow.between(startTime, endTime));
        assertThat(partitionResourceInfoNew.getRecVersion()).isEqualTo(recVersion);
    }


    @Test
    void shouldFindById() {
        final long id = 1L;
        final long recVersion = 2L;

        String condition = toRSQL(PartitionResourceHistories.suchThat().id().eq(id));
        when(httpClient.findAll(condition)).thenReturn(Collections.singleton(partitionResourceInformation(id,
                partitionResource, information, compactionType, storageType, isFixedSettings, startTime, endTime,
                recVersion)));

        PartitionResourceHistory partitionResourceInfoNew = partitionResourceInfoProvider.findById(id).get();


        verify(httpClient, times(1)).findAll(eq(condition));
        assertThat(partitionResourceInfoNew).isEqualTo(partitionResourceInformation(id, partitionResource,
                information, compactionType, storageType, isFixedSettings, startTime, endTime, recVersion));
        assertThat(partitionResourceInfoNew.getId()).isEqualTo(id);
        assertThat(partitionResourceInfoNew.getPartitionResource()).isEqualTo(partitionResource);
        assertThat(partitionResourceInfoNew.getPartitionInformation()).isEqualTo(information);
        assertThat(partitionResourceInfoNew.getCompactionType()).isEqualTo(compactionType);
        assertThat(partitionResourceInfoNew.getStorageType()).isEqualTo(storageType);
        assertThat(partitionResourceInfoNew.isFixedSettings()).isEqualTo(isFixedSettings);
        assertThat(partitionResourceInfoNew.getValidity()).isEqualTo(TimeWindow.between(startTime, endTime));
        assertThat(partitionResourceInfoNew.getRecVersion()).isEqualTo(recVersion);
    }

    @Test
    void shouldDelete() {
        final long partitionResourceInfoId = 1L;
        doNothing().when(httpClient).delete(partitionResourceInfoId);

        partitionResourceInfoProvider.delete(partitionResourceInfoId);

        verify(httpClient, times(1)).delete(partitionResourceInfoId);
    }

    // helper methods

    private PartitionResourceHistory partitionResourceInformation(long id, PartitionResource pr, String info,
                                                                  String compactionType, String storageType, boolean isFixedSettings, Instant startTime, Instant endTime,
                                                                  long recVersion) {
        return ReflectionUtils.builderInstance(PartitionResourceHistory.InnerBuilder.class)
                .id(id)
                .partitionResource(partitionResource)
                .partitionInformation(info)
                .compactionType(compactionType)
                .storageType(storageType)
                .isFixedSettings(isFixedSettings)
                .validity(TimeWindow.between(startTime, endTime))
                .recVersion(recVersion)
                .build();
    }

    private PartitionResource partitionResource() {
        return PartitionResource.builder()
                .systemSpec(systemSpec("my-system"))
                .partition(partition("my-partition"))
                .schema(schema("my-schema")).build();
    }
    private SystemSpec systemSpec(String name) {
        return SystemSpec.builder().name(name).partitionKeyDefinitions("").timeKeyDefinitions("")
                .entityKeyDefinitions("").build();
    }

    private Partition partition(String name) {
        return Partition.builder().keyValues(singletonMap("a", "b")).systemSpec(systemSpec("my-system")).build();
    }

    private EntitySchema schema(String schemaJson) {
        return EntitySchema.builder().schemaJson(schemaJson).build();
    }

}
