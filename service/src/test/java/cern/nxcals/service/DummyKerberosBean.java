package cern.nxcals.service;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Since we don't use kerberos for the tests (all the mini-kdc was removed) and some beans depend on existance of the "kerberos" bean
 * we creat a dummy one of that name here (jwozniak)
 */
@Configuration
public class DummyKerberosBean {
    @Bean(name = "kerberos")

    public String kerberos() {
        return "KerberosDummyBean";
    }

}
