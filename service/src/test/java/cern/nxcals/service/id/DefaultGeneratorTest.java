/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.service.id;

import cern.nxcals.service.domain.SequenceType;
import cern.nxcals.service.internal.id.DbBasedIdPool;
import cern.nxcals.service.internal.id.DefaultGenerator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

import static cern.nxcals.service.domain.SequenceType.ENTITY;
import static cern.nxcals.service.domain.SequenceType.PARTITION;
import static cern.nxcals.service.domain.SequenceType.SCHEMA;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.times;

/**
 * @author Marcin Sobieszek
 * @date Jul 25, 2016 5:47:40 PM
 */
public class DefaultGeneratorTest {
    private static final int BATCH_SIZE = 20;

    @InjectMocks
    private DefaultGenerator idGenerator;

    @Mock
    private DbBasedIdPool dbPool;

    @BeforeEach
    public void init() {
        idGenerator = new DefaultGenerator();
        idGenerator.init();
        MockitoAnnotations.initMocks(this);
        ReflectionTestUtils.setField(this.idGenerator, "batchSize", BATCH_SIZE);
    }

    @Test
    public void shouldNotGetIdForUnknownSequenceType() {
        assertThrows(NullPointerException.class, () -> this.idGenerator.getIdFor(null));
    }

    @Test
    public void shouldGetIdForSinglePoolCall() {
        long expected = 1000L;
        int iterations = 10;
        Mockito.when(this.dbPool.getNextIdAndReserveQuantityLongSequence(SCHEMA, BATCH_SIZE)).thenReturn(expected);
        for (int i = 0; i < iterations; i++) {
            long obtained = this.idGenerator.getIdFor(SCHEMA);
            assertEquals(expected + (i % BATCH_SIZE), obtained);
        }
        Mockito.verify(this.dbPool, times(1)).getNextIdAndReserveQuantityLongSequence(SCHEMA, BATCH_SIZE);
    }

    @Test
    public void shouldGetIdForMultiplePoolCallsFromSingleThread() {
        long expected = 1000L;
        int iterations = 100;
        Mockito.when(this.dbPool.getNextIdAndReserveQuantityLongSequence(SCHEMA, BATCH_SIZE)).thenReturn(expected);
        for (int i = 0; i < iterations; i++) {
            long obtained = this.idGenerator.getIdFor(SCHEMA);
            assertEquals(expected + (i % BATCH_SIZE), obtained);
        }
        Mockito.verify(this.dbPool, times(iterations / BATCH_SIZE))
                .getNextIdAndReserveQuantityLongSequence(SCHEMA, BATCH_SIZE);
    }

    @Test
    public void shouldGetIdForMultiplePoolCallsFromManyThreads() throws ExecutionException, InterruptedException {
        long expected = 1000L;
        int iterations = 1000;
        List<Long> data = new ArrayList<>(iterations);
        Mockito.when(this.dbPool.getNextIdAndReserveQuantityLongSequence(SCHEMA, BATCH_SIZE)).thenReturn(expected);
        ExecutorService pool = Executors.newFixedThreadPool(5);
        try {
            CompletionService<Long> service = new ExecutorCompletionService<>(pool);
            for (int i = 0; i < iterations; i++) {
                service.submit(() -> this.idGenerator.getIdFor(SCHEMA));
            }
            for (int i = 0; i < iterations; i++) {
                Future<Long> result = service.take();
                data.add(result.get());
            }
            Map<Long, Long> results = data.stream().collect(
                    Collectors.groupingBy(Long::longValue, Collectors.counting()));
            for (int i = 0; i < BATCH_SIZE; i++) {
                assertEquals(Long.valueOf(iterations / BATCH_SIZE), results.get(expected + i));
            }
            Mockito.verify(this.dbPool, times(iterations / BATCH_SIZE))
                    .getNextIdAndReserveQuantityLongSequence(SCHEMA, BATCH_SIZE);
        } finally {
            pool.shutdown();
        }
    }

    @Test
    @SuppressWarnings("squid:S2925") //Thread.sleep warning
    public void shouldReturnCorrectIdForManyDifferentSequences() throws Exception {
        long expectedSchema = 2000L;
        long expectedEntity = 2002L;
        long expectedPartition = 4;
        int iterations = 1000;

        Map<SequenceType, Long> expectedNumber = new HashMap<>();
        expectedNumber.put(SCHEMA, expectedSchema);
        expectedNumber.put(ENTITY, expectedEntity);
        expectedNumber.put(PARTITION, expectedPartition);

        Map<SequenceType, Long> invocationNumber = new HashMap<>();
        Map<SequenceType, Long> lastValue = new ConcurrentHashMap<>();

        for (Map.Entry<SequenceType, Long> entry : expectedNumber.entrySet()) {
            invocationNumber.put(entry.getKey(), 0L);
            lastValue.put(entry.getKey(), -1L);
            Mockito.when(this.dbPool.getNextIdAndReserveQuantityLongSequence(entry.getKey(), BATCH_SIZE))
                    .thenReturn(entry.getValue());
        }

        ExecutorService pool = Executors.newFixedThreadPool(5);
        try {
            CompletionService<Long> service = new ExecutorCompletionService<>(pool);
            for (int i = 0; i < iterations; i++) {
                final SequenceType sequenceType = new ArrayList<>(expectedNumber.keySet())
                        .get(ThreadLocalRandom.current().nextInt(expectedNumber.keySet().size()));
                invocationNumber.put(sequenceType, invocationNumber.get(sequenceType) + 1);
                service.submit(() -> {
                    long idFor = this.idGenerator.getIdFor(sequenceType);
                    lastValue.put(sequenceType, idFor);
                    Thread.sleep(ThreadLocalRandom.current().nextInt(10));
                    return idFor;
                });

            }
            for (int i = 0; i < iterations; i++) {
                service.take();
            }

            lastValue.forEach((key, value) -> {
                long sequenceInvocationNumber = invocationNumber.get(key);
                if (value == -1L) {
                    assertEquals(0L, sequenceInvocationNumber);
                } else {
                    long effectiveId = value - expectedNumber.get(key);
                    long effectiveInvocationNumber = (sequenceInvocationNumber - 1) % BATCH_SIZE;
                    assertEquals(effectiveId, effectiveInvocationNumber);
                }
            });

        } finally {
            pool.shutdown();
        }
    }
}
