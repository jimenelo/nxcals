package cern.nxcals.service.repository;

import cern.nxcals.api.extraction.metadata.queries.PartitionResources;
import cern.nxcals.api.extraction.metadata.queries.Variables;
import cern.nxcals.service.BaseTest;
import cern.nxcals.service.domain.EntityData;
import cern.nxcals.service.domain.EntitySchemaData;
import cern.nxcals.service.domain.PartitionData;
import cern.nxcals.service.domain.PartitionResourceData;
import cern.nxcals.service.domain.SystemSpecData;
import cern.nxcals.service.domain.VariableData;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static cern.nxcals.common.utils.RSQLUtils.toRSQL;
import static cern.nxcals.service.rest.DomainTestConstants.ENTITY_KEY_VALUES_1;
import static cern.nxcals.service.rest.DomainTestConstants.ENTITY_KEY_VALUES_2;
import static cern.nxcals.service.rest.TestSchemas.ENTITY_SCHEMA_1;
import static cern.nxcals.service.rest.TestSchemas.ENTITY_SCHEMA_2;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

@Transactional(transactionManager = "jpaTransactionManager")
public class PartitionResourceRepositoryTest extends BaseTest {

    @Autowired
    private PartitionResourceRepository repository;
    private PartitionResourceData firstPartitionResource;
    private PartitionResourceData secondPartitionResource;
    private PartitionResourceData thirdPartitionResource;
    private SystemSpecData system;
    private SystemSpecData newSystem;
    private PartitionData partition;
    private EntitySchemaData schema;
    private EntityData entityData;

    @BeforeEach
    public void before() {
        setAuthentication();
        entityData = createAndSaveDefaultTestEntityKey();
        system = entityData.getPartition().getSystem();
        schema = entityData.getEntityHistories().first().getSchema();
        firstPartitionResource = partitionResourceData(system, schema, ENTITY_KEY_VALUES_1, ENTITY_SCHEMA_1);
        secondPartitionResource = partitionResourceData(system, schema, ENTITY_KEY_VALUES_2, ENTITY_SCHEMA_2);
        newSystem = createAndPersistSystemDataFor("my-new-system");
        thirdPartitionResource = partitionResourceData(newSystem, schema, ENTITY_KEY_VALUES_2, ENTITY_SCHEMA_2);
        partition = firstPartitionResource.getPartition();
    }

    @Test
    @Rollback
    public void shouldFindAllPartitionResourcesWithSameSystemId() {
        List<PartitionResourceData> results = repository.queryAll(
                toRSQL(PartitionResources.suchThat().systemId().eq(system.getId())),
                PartitionResourceData.class);
        assertThat(results).containsExactlyInAnyOrder(firstPartitionResource, secondPartitionResource);
    }

    @Test
    @Rollback
    public void shouldNotFindAnyResults() {
        List<PartitionResourceData> results1 = repository.queryAll(
                toRSQL(PartitionResources.suchThat().systemId().eq(system.getId())
                        .and().partitionId().eq(partition.getId())
                        .and().schemaId().eq(-1L)),
                PartitionResourceData.class);
        assertThat(results1).isEmpty();

        List<PartitionResourceData> results2 = repository.queryAll(
                toRSQL(PartitionResources.suchThat().systemId().eq(system.getId())
                        .and().partitionId().eq(-1L)
                        .and().schemaId().eq(schema.getId())),
                PartitionResourceData.class);
        assertThat(results2).isEmpty();

        List<PartitionResourceData> results3 = repository.queryAll(
                toRSQL(PartitionResources.suchThat().systemId().eq(-1L)
                        .and().partitionId().eq(partition.getId())
                        .and().schemaId().eq(schema.getId())),
                PartitionResourceData.class);
        assertThat(results3).isEmpty();
    }

    @Test
    @Rollback
    public void shouldFindOneResult() {
        List<PartitionResourceData> firstResults = repository.queryAll(
                toRSQL(PartitionResources.suchThat().systemId().eq(firstPartitionResource.getSystem().getId())
                        .and().partitionId().eq(firstPartitionResource.getPartition().getId())
                        .and().schemaId().eq(firstPartitionResource.getSchema().getId())),
                PartitionResourceData.class);
        assertThat(firstResults).hasSize(1);

        List<PartitionResourceData> secondResults = repository.queryAll(
                toRSQL(PartitionResources.suchThat().systemId().eq(secondPartitionResource.getSystem().getId())
                        .and().partitionId().eq(secondPartitionResource.getPartition().getId())
                        .and().schemaId().eq(secondPartitionResource.getSchema().getId())),
                PartitionResourceData.class);
        assertThat(secondResults).hasSize(1);
    }

    @Test
    @Rollback
    public void shouldFindAllPartitionResourcesWithSameSchemaId() {
        List<PartitionResourceData> results = repository.queryAll(
                toRSQL(PartitionResources.suchThat().schemaId().eq(schema.getId())),
                PartitionResourceData.class);
        assertThat(results).containsExactlyInAnyOrder(firstPartitionResource, secondPartitionResource, thirdPartitionResource);
    }

    @Test
    @Rollback
    public void shouldFailOnWrongRQSL() {
        String query = "This is not a good format";
        assertThrows(cz.jirutka.rsql.parser.RSQLParserException.class, () -> repository.queryAll(query, PartitionResourceData.class));
    }

    @Test
    @Rollback
    public void shouldNotAllowCreationOfDuplicatesPartitionResources() {
        PartitionResourceData duplicatedPartitionResource = partitionResourceData(system, schema, ENTITY_KEY_VALUES_1, ENTITY_SCHEMA_1);
        PartitionResourceData savedDuplicatedPartitionResource = repository.save(duplicatedPartitionResource);
        assertNotNull(savedDuplicatedPartitionResource);
        assertNotNull(savedDuplicatedPartitionResource.getId());

        //explicitly flush changes to trigger unique db constraint rules
        assertThrows(javax.persistence.PersistenceException.class, () -> entityManager.flush());
    }
}
