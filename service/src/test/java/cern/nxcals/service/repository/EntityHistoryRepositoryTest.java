/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.repository;

import cern.nxcals.api.extraction.metadata.queries.EntityHistories;
import cern.nxcals.api.utils.TimeUtils;
import cern.nxcals.service.BaseTest;
import cern.nxcals.service.domain.EntityData;
import cern.nxcals.service.domain.EntityHistoryData;
import cern.nxcals.service.domain.EntitySchemaData;
import cern.nxcals.service.domain.PartitionData;
import com.github.rutledgepaulv.qbuilders.conditions.Condition;
import com.google.common.collect.Sets;
import org.apache.avro.Schema;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.ConstraintViolationException;
import java.time.Instant;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import static cern.nxcals.common.utils.RSQLUtils.toRSQL;
import static cern.nxcals.service.rest.DomainTestConstants.ENTITY_KEY_VALUES_1;
import static cern.nxcals.service.rest.DomainTestConstants.ENTITY_KEY_VALUES_JSON;
import static cern.nxcals.service.rest.DomainTestConstants.PARTITION_KEY_VALUES_1;
import static cern.nxcals.service.rest.DomainTestConstants.PARTITION_KEY_VALUES_2;
import static cern.nxcals.service.rest.DomainTestConstants.PARTITION_KEY_VALUES_STRING_1;
import static cern.nxcals.service.rest.DomainTestConstants.PARTITION_KEY_VALUES_STRING_2;
import static cern.nxcals.service.rest.TestSchemas.ENTITY_SCHEMA_1;
import static cern.nxcals.service.rest.TestSchemas.ENTITY_SCHEMA_2;
import static cern.nxcals.service.rest.TestSchemas.PARTITION_SCHEMA_1;
import static cern.nxcals.service.rest.TestSchemas.PARTITION_SCHEMA_2;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@Transactional(transactionManager = "jpaTransactionManager")
public class EntityHistoryRepositoryTest extends BaseTest {

    @Autowired
    private EntityHistoryRepository entityHistoryRepository;

    @BeforeEach
    public void setUp() {
        setAuthentication();
    }

    @Rollback
    @Test
    public void shouldNotCreateHistoryWithoutPartition() {
        EntityData entity = createAndPersistEntity(TEST_NAME, ENTITY_KEY_VALUES_1, ENTITY_SCHEMA_1,
                PARTITION_KEY_VALUES_1, PARTITION_SCHEMA_1);
        createAndPersistEntityHistory(entity, null, createSchema(ENTITY_SCHEMA_1), TEST_RECORD_TIME);
        assertThrows(ConstraintViolationException.class, () -> entityManager.flush());
    }

    @Rollback
    @Test
    public void shouldNotCreateHistoryWithoutSchema() {
        EntityData entity = createAndPersistEntity(TEST_NAME, ENTITY_KEY_VALUES_1, ENTITY_SCHEMA_1,
                PARTITION_KEY_VALUES_1, PARTITION_SCHEMA_1);
        createAndPersistEntityHistory(entity, entity.getPartition(), null, TEST_RECORD_TIME);
        assertThrows(ConstraintViolationException.class, () -> entityManager.flush());
    }

    @Rollback
    @Test
    public void shouldNotCreateHistoryWithoutValidFromStamp() {
        EntityData entity = createAndPersistEntity(TEST_NAME, ENTITY_KEY_VALUES_1, ENTITY_SCHEMA_1,
                PARTITION_KEY_VALUES_1, PARTITION_SCHEMA_1);
        createAndPersistEntityHistory(entity, entity.getPartition(), createSchema(ENTITY_SCHEMA_1), null);
        assertThrows(ConstraintViolationException.class, () -> entityManager.flush());
    }

    @Test
    @Rollback
    public void shouldCreateEntityHist() {
        EntityData entity = createAndPersistEntity(TEST_NAME, ENTITY_KEY_VALUES_1, ENTITY_SCHEMA_1,
                PARTITION_KEY_VALUES_1, PARTITION_SCHEMA_1);
        Long historyId = createAndPersistEntityHistory(entity, entity.getPartition(), createSchema(ENTITY_SCHEMA_1),
                TEST_RECORD_TIME).getId();
        Optional<EntityHistoryData> optionalEntityHistory = entityHistoryRepository.findById(historyId);

        assertThat(optionalEntityHistory).isPresent();
        EntityHistoryData entityHistory = optionalEntityHistory.get();
        assertThat(entityHistory.getEntity().getKeyValues()).isEqualTo(ENTITY_KEY_VALUES_JSON);
        assertThat(entityHistory.getPartition().getKeyValues()).isEqualTo(PARTITION_KEY_VALUES_STRING_1);
        assertThat(entityHistory.getSchema().getContent()).isEqualTo(ENTITY_SCHEMA_1.toString());
        assertThat(entityHistory.getValidFromStamp()).isEqualTo(TimeUtils.getInstantFromNanos(TEST_RECORD_TIME));
        assertThat(entityHistory.getValidToStamp()).isNull();
    }

    @Test
    @Rollback
    public void shouldCountEntityHist() {
        EntityData entity = createAndPersistEntity(TEST_NAME, ENTITY_KEY_VALUES_1, ENTITY_SCHEMA_1,
                PARTITION_KEY_VALUES_1, PARTITION_SCHEMA_1);
        Long historyId = createAndPersistEntityHistory(entity, entity.getPartition(), createSchema(ENTITY_SCHEMA_1),
                TEST_RECORD_TIME).getId();
        assertEquals(1, entityHistoryRepository.countByEntityAndValidFromStampBetweenTimestamps(entity,
                TimeUtils.getInstantFromNanos(TEST_RECORD_TIME), TimeUtils.getInstantFromNanos(TEST_RECORD_TIME)));
    }

    @Test
    @Rollback
    public void shouldFindEntityHistoryByTimestamp() {
        EntityData entity = createAndPersistEntity(TEST_NAME, ENTITY_KEY_VALUES_1, ENTITY_SCHEMA_1,
                PARTITION_KEY_VALUES_1, PARTITION_SCHEMA_1);
        EntityHistoryData entityHistory = createAndPersistEntityHistory(entity, entity.getPartition(),
                createSchema(ENTITY_SCHEMA_1), TEST_RECORD_TIME);

        Condition<EntityHistories> condition = EntityHistories.suchThat().validFromStamp()
                .eq(TimeUtils.getInstantFromNanos(TEST_RECORD_TIME));
        Optional<EntityHistoryData> optionalEntityHistory = entityHistoryRepository
                .queryOne(toRSQL(condition), EntityHistoryData.class);

        assertThat(optionalEntityHistory).isPresent();
        EntityHistoryData foundEntityHistory = optionalEntityHistory.get();
        assertThat(foundEntityHistory.getId()).isEqualTo(entityHistory.getId());
        assertThat(foundEntityHistory.getValidFromStamp()).isEqualTo(TimeUtils.getInstantFromNanos(TEST_RECORD_TIME));
    }

    @Test
    @Rollback
    public void shouldUpdateEntityHistPartition() {
        EntityData entity = createAndPersistEntity(TEST_NAME, ENTITY_KEY_VALUES_1, ENTITY_SCHEMA_1,
                PARTITION_KEY_VALUES_1, PARTITION_SCHEMA_1);
        EntityHistoryData entityHistory = createAndPersistEntityHistory(entity, entity.getPartition(),
                createSchema(ENTITY_SCHEMA_1), TEST_RECORD_TIME);
        updateEntityHistoryPartition(entityHistory, TEST_NAME, PARTITION_KEY_VALUES_2, PARTITION_SCHEMA_2);

        Optional<EntityHistoryData> optionalEntityHistory = entityHistoryRepository.findById(entityHistory.getId());

        assertThat(optionalEntityHistory).isPresent();
        EntityHistoryData foundEntityHistory = optionalEntityHistory.get();
        assertThat(foundEntityHistory.getPartition().getSystem().getName()).isEqualTo(TEST_NAME);
        assertThat(foundEntityHistory.getPartition().getKeyValues()).isEqualTo(PARTITION_KEY_VALUES_STRING_2);
        assertThat(foundEntityHistory.getSchema().getContent()).isEqualTo(ENTITY_SCHEMA_1.toString());
        assertThat(foundEntityHistory.getValidFromStamp()).isEqualTo(TimeUtils.getInstantFromNanos(TEST_RECORD_TIME));
        assertThat(foundEntityHistory.getValidToStamp()).isNull();
    }

    @Test
    @Rollback
    public void shouldUpdateEntityHistSchema() {
        EntityData entity = createAndPersistEntity(TEST_NAME, ENTITY_KEY_VALUES_1, ENTITY_SCHEMA_1,
                PARTITION_KEY_VALUES_1, PARTITION_SCHEMA_1);
        EntityHistoryData entityHistory = createAndPersistEntityHistory(entity, entity.getPartition(),
                createSchema(ENTITY_SCHEMA_1), TEST_RECORD_TIME);
        updateEntityHistorySchema(entityHistory, ENTITY_SCHEMA_2);

        Optional<EntityHistoryData> optionalEntityHistory = entityHistoryRepository.findById(entityHistory.getId());

        assertThat(optionalEntityHistory).isPresent();
        EntityHistoryData foundEntityHistory = optionalEntityHistory.get();
        assertThat(foundEntityHistory.getPartition().getSystem().getName()).isEqualTo(TEST_NAME);
        assertThat(foundEntityHistory.getPartition().getKeyValues()).isEqualTo(PARTITION_KEY_VALUES_STRING_1);
        assertThat(foundEntityHistory.getSchema().getContent()).isEqualTo(ENTITY_SCHEMA_2.toString());
        assertThat(foundEntityHistory.getValidFromStamp()).isEqualTo(TimeUtils.getInstantFromNanos(TEST_RECORD_TIME));
        assertThat(foundEntityHistory.getValidToStamp()).isNull();
    }

    @Test
    @Rollback
    public void shouldUpdateEntityHistTimes() {
        EntityHistoryData entityHistory = createAndPersistDefaultTestEntityKeyHist();
        Instant testTime = TimeUtils.getInstantFromNanos(TEST_RECORD_TIME).plusSeconds(10);
        entityHistory.setValidFromStamp(testTime);
        entityHistory.setValidToStamp(testTime);
        entityHistoryRepository.save(entityHistory);

        Optional<EntityHistoryData> optionalUpdatedHistory = entityHistoryRepository.findById(entityHistory.getId());

        assertThat(optionalUpdatedHistory).isPresent();
        EntityHistoryData foundEntityHistory = optionalUpdatedHistory.get();
        assertThat(foundEntityHistory.getPartition().getSystem().getName()).isEqualTo(TEST_NAME);
        assertThat(foundEntityHistory.getPartition().getKeyValues()).isEqualTo(PARTITION_KEY_VALUES_STRING_1);
        assertThat(foundEntityHistory.getSchema().getContent()).isEqualTo(ENTITY_SCHEMA_1.toString());
        assertThat(foundEntityHistory.getValidFromStamp()).isEqualTo(testTime);
        assertThat(foundEntityHistory.getValidToStamp()).isEqualTo(testTime);
    }

    private EntityHistoryData updateEntityHistorySchema(EntityHistoryData entityHistory, Schema newSchemaContent) {
        EntitySchemaData newSchema = createSchema(newSchemaContent);
        entityHistory.setSchema(newSchema);
        return entityHistRepository.save(entityHistory);
    }

    private void updateEntityHistoryPartition(EntityHistoryData entityHistory, String systemName,
            Map<String, Object> partitionKeyValues, org.apache.avro.Schema partitionSchema) {
        PartitionData newPartition = createPartition(systemName, partitionKeyValues, partitionSchema);
        partitionRepository.save(newPartition);
        entityHistory.setPartition(newPartition);
        entityHistRepository.save(entityHistory);
    }

    @Test
    @Rollback
    public void shouldFindAllFirstEntriesByEntityInCollection() {
        EntityData entity = createAndPersistEntity(TEST_NAME, ENTITY_KEY_VALUES_1, ENTITY_SCHEMA_1,
                PARTITION_KEY_VALUES_1, PARTITION_SCHEMA_1);
        EntityHistoryData entityHistory = createAndPersistEntityHistory(entity, entity.getPartition(),
                createSchema(ENTITY_SCHEMA_1), TEST_RECORD_TIME);
        updateEntityHistorySchema(entityHistory, ENTITY_SCHEMA_2);

        Set<EntityHistoryData> historyEntries = entityHistoryRepository
                .findAllFirstEntriesByEntityIn(Sets.newHashSet(entity));

        assertThat(historyEntries).isNotNull();
        assertThat(historyEntries.size()).isEqualTo(1);
        assertThat(historyEntries).contains(entityHistory);
    }

    @Test
    @Rollback
    public void shouldFindAllLastEntriesByEntityInCollection() {
        EntityData entity = createAndPersistEntity(TEST_NAME, ENTITY_KEY_VALUES_1, ENTITY_SCHEMA_1,
                PARTITION_KEY_VALUES_1, PARTITION_SCHEMA_1);
        EntityHistoryData entityHistory = createAndPersistEntityHistory(entity, entity.getPartition(),
                createSchema(ENTITY_SCHEMA_1), TEST_RECORD_TIME);
        EntityHistoryData updatedEntityHistory = updateEntityHistorySchema(entityHistory, ENTITY_SCHEMA_2);

        Set<EntityHistoryData> historyEntries = entityHistoryRepository
                .findAllLatestEntriesByEntityIn(Sets.newHashSet(entity));

        assertThat(historyEntries).isNotNull();
        assertThat(historyEntries.size()).isEqualTo(1);
        assertThat(historyEntries).contains(updatedEntityHistory);
    }
}
