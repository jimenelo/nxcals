package cern.nxcals.service.repository;

import cern.nxcals.api.extraction.metadata.queries.Groups;
import cern.nxcals.common.utils.KeyValuesUtils;
import cern.nxcals.service.BaseTest;
import cern.nxcals.service.domain.EntityData;
import cern.nxcals.service.domain.GroupData;
import cern.nxcals.service.domain.SystemSpecData;
import cern.nxcals.service.domain.VariableData;
import com.github.rutledgepaulv.qbuilders.conditions.Condition;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.LongStream;

import static cern.nxcals.common.utils.RSQLUtils.toRSQL;
import static cern.nxcals.service.internal.GroupServiceTest.MAIN_USER;
import static cern.nxcals.service.repository.PartitionRepositoryTest.PARTITION_SCHEMA_1;
import static cern.nxcals.service.rest.TestSchemas.ENTITY_SCHEMA_1;
import static cern.nxcals.service.rest.TestSchemas.TIME_SCHEMA;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@Transactional(transactionManager = "jpaTransactionManager")
public class GroupRepositoryTest extends BaseTest {
    private SystemSpecData system;

    @Autowired
    protected GroupRepository repository;

    @BeforeEach
    public void setUp() {
        // needs authentication before anything can be persisted
        setAuthentication();

        system = createAndPersistSystemData(TEST_NAME, ENTITY_SCHEMA_1, PARTITION_SCHEMA_1, TIME_SCHEMA);

        WebAuthenticationDetails details = mock(WebAuthenticationDetails.class);
        when(details.getRemoteAddress()).thenReturn("remoteAddress");

        authenticate(MAIN_USER);
    }

    @Test
    @Rollback
    public void shouldNotFindGroup() {
        Condition<Groups> queryCondition = Groups.suchThat().id().eq(-1L);
        Optional<GroupData> found = repository.queryOne(toRSQL(queryCondition), GroupData.class);
        assertThat(found).isNotPresent();
    }

    @Test
    @Rollback
    public void shouldAllowNoOwner() {
        GroupData given = groupData("my-group-1", system);
        //        unauthenticate();

        repository.save(given);

        Condition<Groups> queryCondition = Groups.suchThat().name().eq("my-group-1");
        Optional<GroupData> found = repository.queryOne(toRSQL(queryCondition), GroupData.class);

        assertThat(found).isPresent();
        assertThat(found.get().hasOwner()).isFalse();
    }

    @Test
    @Rollback
    public void shouldFindByOwner() {
        GroupData given = groupDataWithUser("my-group-1", system, MAIN_USER);

        repository.save(given);

        Condition<Groups> queryCondition = Groups.suchThat().ownerName().eq(MAIN_USER);
        List<GroupData> found = repository.queryAll(toRSQL(queryCondition), GroupData.class);

        assertThat(found.isEmpty()).isFalse();
        for (GroupData groupData : found) {
            assertThat(groupData.getOwner()).isEqualTo(MAIN_USER);
        }
    }

    @Test
    @Rollback
    public void shouldFindGroupWithEntityKeyValues() {
        GroupData given = groupData("my-group-1", system);
        EntityData entity = entityData(system);
        given.add("associationA", entity);

        repository.save(given);

        Condition<Groups> queryCondition = Groups.suchThat().name().eq("my-group-1").and()
                .entityKeyValues().eq(given.getSystem().toSystemSpec(),
                        KeyValuesUtils.convertKeyValuesStringIntoMap(entity.getKeyValues()));
        Optional<GroupData> found = repository.queryOne(toRSQL(queryCondition), GroupData.class);

        assertThat(found).isPresent();

    }

    @Test
    @Rollback
    public void shouldFindGroupWithVariableName() {
        GroupData given = groupData("my-group-1", system);

        EntityData entity = entityData(system);
        VariableData variableData1 = variableData("v1", system, entity);
        VariableData variableData2 = variableData("v2", system, entity);
        given.add("associationA", variableData1);
        given.add("associationA", variableData2);

        repository.save(given);

        Condition<Groups> queryCondition = Groups.suchThat().name().eq("my-group-1").and().variableName().eq("v1");
        Optional<GroupData> found = repository.queryOne(toRSQL(queryCondition), GroupData.class);

        assertThat(found).isPresent();

    }

    @Test
    @Rollback
    public void shouldFindGroupWithVariableNameLike() {
        GroupData given = groupData("my-group-1", system);

        EntityData entity = entityData(system);
        VariableData variableData1 = variableData("v1-test", system, entity);
        VariableData variableData2 = variableData("v2-test", system, entity);
        given.add("associationA", variableData1);
        given.add("associationA", variableData2);

        repository.save(given);

        Condition<Groups> queryCondition = Groups.suchThat().name().eq("my-group-1").and().variableName().like("v1-t%");
        Optional<GroupData> found = repository.queryOne(toRSQL(queryCondition), GroupData.class);

        assertThat(found).isPresent();

    }

    @Test
    @Rollback
    public void shouldNotFindGroupWithVariableNameMissing() {
        GroupData given = groupData("my-group-1", system);

        EntityData entity = entityData(system);
        VariableData variableData1 = variableData("v1", system, entity);
        VariableData variableData2 = variableData("v2", system, entity);
        given.add("associationA", variableData1);
        given.add("associationA", variableData2);

        repository.save(given);

        Condition<Groups> queryCondition = Groups.suchThat().name().eq("my-group-1").and().variableName()
                .eq("v-not-there");
        Optional<GroupData> found = repository.queryOne(toRSQL(queryCondition), GroupData.class);

        assertThat(found).isEmpty();

    }

    @Test
    @Rollback
    public void shouldFindGroupWithVariable() {
        GroupData given = groupData("my-group-1", system);

        EntityData entity = entityData(system);
        VariableData variableData1 = variableData("v1", system, entity);
        VariableData variableData2 = variableData("v2", system, entity);
        given.add(null, variableData1);
        given.add(null, variableData2);
        given.add("associationA", variableData1);
        given.add("associationA", variableData2);
        given.add("associationB", variableData1);
        given.add("associationB", variableData2);

        repository.save(given);

        Condition<Groups> queryCondition = Groups.suchThat().name().eq("my-group-1");
        Optional<GroupData> found = repository.queryOne(toRSQL(queryCondition), GroupData.class);

        assertThat(found).isPresent();
        assertThat(found.get()).isEqualTo(given);
        assertThat(found.get().getVariables()).containsAll(given.getVariables());

        assertThat(found.get().getAssociatedVariables()).hasSize(3);
        assertThat(found.get().getAssociatedVariables()).containsKeys(null, "associationA", "associationB");
        assertThat(found.get().getAssociatedVariables().get(null)).containsExactlyInAnyOrder(variableData1,
                variableData2);
        assertThat(found.get().getAssociatedVariables().get("associationA")).containsExactlyInAnyOrder(variableData1,
                variableData2);
        assertThat(found.get().getAssociatedVariables().get("associationB")).containsExactlyInAnyOrder(variableData1,
                variableData2);
    }

    @Test
    @Rollback
    public void shouldAllowVariableUpdate() {
        GroupData given = groupData("my-group-1", system);

        EntityData entity = entityData(system);
        VariableData variableData1 = variableData("v1", system, entity);
        VariableData variableData2 = variableData("v2", system, entity);

        GroupData newGiven = repository.save(given);

        newGiven.add(null, variableData1);
        newGiven.add(null, variableData2);
        newGiven.add("associationA", variableData1);
        newGiven.add("associationA", variableData2);
        newGiven.add("associationB", variableData1);
        newGiven.add("associationB", variableData2);

        repository.save(newGiven);

        Condition<Groups> queryCondition = Groups.suchThat().name().eq("my-group-1");
        Optional<GroupData> found = repository.queryOne(toRSQL(queryCondition), GroupData.class);

        assertThat(found).isPresent();
        assertThat(found.get()).isEqualTo(given);
        assertThat(found.get().getVariables()).containsAll(given.getVariables());

        assertThat(found.get().getAssociatedVariables()).hasSize(3);
        assertThat(found.get().getAssociatedVariables()).containsKeys(null, "associationA", "associationB");
        assertThat(found.get().getAssociatedVariables().get(null)).containsExactlyInAnyOrder(variableData1,
                variableData2);
        assertThat(found.get().getAssociatedVariables().get("associationA")).containsExactlyInAnyOrder(variableData1,
                variableData2);
        assertThat(found.get().getAssociatedVariables().get("associationB")).containsExactlyInAnyOrder(variableData1,
                variableData2);
    }

    @Test
    @Rollback
    public void shouldFindGroupWithEntity() {
        GroupData given = groupData("my-group-1", system);
        EntityData entity = entityData(system);
        given.add(null, entity);
        given.add("associationA", entity);
        given.add("associationB", entity);

        repository.save(given);

        Condition<Groups> queryCondition = Groups.suchThat().name().eq("my-group-1");
        Optional<GroupData> found = repository.queryOne(toRSQL(queryCondition), GroupData.class);

        assertThat(found).isPresent();
        assertThat(found.get()).isEqualTo(given);
        assertThat(found.get().getEntities()).containsAll(given.getEntities());

        assertThat(found.get().getAssociatedEntities()).hasSize(3);
        assertThat(found.get().getAssociatedEntities()).containsKeys(null, "associationA", "associationB");
        assertThat(found.get().getAssociatedEntities().get(null)).containsExactlyInAnyOrder(entity);
        assertThat(found.get().getAssociatedEntities().get("associationA")).containsExactlyInAnyOrder(entity);
        assertThat(found.get().getAssociatedEntities().get("associationB")).containsExactlyInAnyOrder(entity);
    }

    @Test
    @Rollback
    public void shouldAllowGroupWithEntityUpdate() {
        GroupData given = groupData("my-group-1", system);
        EntityData entity = entityData(system);

        GroupData newGiven = repository.save(given);

        newGiven.add(null, entity);
        newGiven.add("associationA", entity);
        newGiven.add("associationB", entity);

        repository.save(newGiven);

        Condition<Groups> queryCondition = Groups.suchThat().name().eq("my-group-1");
        Optional<GroupData> found = repository.queryOne(toRSQL(queryCondition), GroupData.class);

        assertThat(found).isPresent();
        assertThat(found.get()).isEqualTo(given);
        assertThat(found.get().getEntities()).containsAll(given.getEntities());

        assertThat(found.get().getAssociatedEntities()).hasSize(3);
        assertThat(found.get().getAssociatedEntities()).containsKeys(null, "associationA", "associationB");
        assertThat(found.get().getAssociatedEntities().get(null)).containsExactlyInAnyOrder(entity);
        assertThat(found.get().getAssociatedEntities().get("associationA")).containsExactlyInAnyOrder(entity);
        assertThat(found.get().getAssociatedEntities().get("associationB")).containsExactlyInAnyOrder(entity);
    }

    @Test
    @Rollback
    public void shouldWorkWithLongLists() {
        Condition<Groups> queryCondition = Groups.suchThat().ownerName().eq(MAIN_USER).and().id()
                .in(LongStream.range(1, 1002).boxed().toArray(Long[]::new));
        repository.queryAll(toRSQL(queryCondition), GroupData.PLAIN, GroupData.class);
    }

    @Test
    @Rollback
    public void shouldAllowVariableAndEntityUpdate() {
        GroupData given = groupData("my-group-1", system);
        Condition<Groups> queryCondition = Groups.suchThat().name().eq("my-group-1");

        EntityData entity = entityData(system);
        VariableData variableData1 = variableData("v1", system, entity);
        VariableData variableData2 = variableData("v2", system, entity);

        repository.save(given);
        entityManager.flush();

        GroupData newGiven;

        newGiven = repository.queryOne(toRSQL(queryCondition), GroupData.class).get();
        newGiven.add(null, variableData1);
        newGiven.add(null, variableData2);
        repository.save(newGiven);
        entityManager.flush();

        newGiven = repository.queryOne(toRSQL(queryCondition), GroupData.class).get();
        newGiven.getVariables().clear();
        newGiven.add(null, variableData2);
        newGiven.add(null, entity);
        repository.save(newGiven);
        entityManager.flush();

        GroupData found = repository.queryOne(toRSQL(queryCondition), GroupData.class).get();

        assertThat(found).isEqualTo(given);
        assertThat(found.getVariables()).containsAll(newGiven.getVariables());
        assertThat(found.getEntities()).containsAll(newGiven.getEntities());

        assertThat(found.getAssociatedVariables()).hasSize(1);
        assertThat(found.getAssociatedVariables().get(null)).containsExactlyInAnyOrder(variableData2);

        assertThat(found.getAssociatedEntities()).hasSize(1);
        assertThat(found.getAssociatedEntities().get(null)).containsExactlyInAnyOrder(entity);
    }

    @Test
    @Rollback
    public void shouldAllowUpdate() {
        GroupData givenA = groupData("my-group-1", system);

        repository.save(givenA);

        Optional<GroupData> foundA;
        Optional<GroupData> foundB;
        Condition<Groups> queryConditionA = Groups.suchThat().name().eq("my-group-1");
        Condition<Groups> queryConditionB = Groups.suchThat().name().eq("my-group-2");

        foundA = repository.queryOne(toRSQL(queryConditionA), GroupData.class);
        foundB = repository.queryOne(toRSQL(queryConditionB), GroupData.class);
        assertThat(foundA).isPresent();
        assertThat(foundB).isNotPresent();
        assertThat(foundA.get()).isEqualTo(givenA);
        GroupData givenB = foundA.get();
        givenB.setName("my-group-2");

        repository.save(givenB);

        foundA = repository.queryOne(toRSQL(queryConditionA), GroupData.class);
        foundB = repository.queryOne(toRSQL(queryConditionB), GroupData.class);
        assertThat(foundA).isNotPresent();
        assertThat(foundB).isPresent();
        assertThat(foundB.get()).isEqualTo(givenB);
    }

    @Test
    @Rollback
    public void shouldAllowSaveWithIdAndRecVersion() {
        GroupData givenA = groupData("my-group-1", system);
        givenA.setId(100L);
        givenA.setRecVersion(100L);
        repository.save(givenA);
    }

    @Test
    @Rollback
    public void shouldAddPropertiesToSnapshot() {
        GroupData data = groupData("my-group", system);
        data.setLabel("my-label");

        repository.save(data);

        Optional<GroupData> found;

        Condition<Groups> queryCondition = Groups.suchThat().name().eq("my-group");
        found = repository.queryOne(toRSQL(queryCondition), GroupData.class);
        assertThat(found).isPresent();

        GroupData groupData = found.get();
        groupData.getProperties().put("prop_1", "val_1");
        groupData.getProperties().put("prop_2", "val_2");
        repository.save(groupData);

        found = repository.queryOne(toRSQL(queryCondition), GroupData.class);
        assertThat(found.get().getProperties().size()).isEqualTo(2);
    }

    @Test
    @Rollback
    public void shouldFindByType() {
        GroupData group = groupData("my-group", system);
        group.setLabel("my-group-label");
        repository.save(group);

        GroupData snap = groupData("my-snapshot", system);
        snap.setLabel("my-label");
        repository.save(snap);

        Condition<Groups> groups = Groups.suchThat().label().eq("my-group-label").and().name().eq("my-group");
        Condition<Groups> snaps = Groups.suchThat().label().eq("my-label").and().name().eq("my-snapshot");
        assertThat(repository.queryAll(toRSQL(groups), GroupData.class).size()).isEqualTo(1);
        assertThat(repository.queryAll(toRSQL(snaps), GroupData.class).size()).isEqualTo(1);
    }

    @Test
    @Rollback
    public void shouldNotAllowSameName() {
        repository.save(groupData("same-name", system));
        repository.save(groupData("same-name", system));

        Condition<Groups> groups = Groups.suchThat().name().eq("same-name");
        assertThrows(DataIntegrityViolationException.class, () -> repository.queryAll(toRSQL(groups), GroupData.class));
    }

    @Test
    @Rollback
    public void shoulAllowSameNameIfDifferentType() {
        repository.save(groupData("same-name", system, "my-group-label"));
        repository.save(groupData("same-name", system, "my-label"));

        Condition<Groups> groups = Groups.suchThat().name().eq("same-name");
        repository.queryAll(toRSQL(groups), GroupData.class);
    }

    @Test
    @Rollback
    public void shouldAllowSaveWithIdOnly() {
        GroupData givenA = groupData("my-group-1", system);
        givenA.setId(100L);
        repository.save(givenA);
    }

    @Test
    @Rollback
    public void shouldAllowSaveWithRecVersionOnly() {
        GroupData givenA = groupData("my-group-1", system);
        givenA.setRecVersion(100L);
        repository.save(givenA);
    }

    @Test
    @Rollback
    public void shouldAllowToCreateOnUpdate() {
        GroupData givenA = groupData("my-group-1", system);
        givenA.setId(100L);
        givenA.setRecVersion(100L);

        GroupData save;
        save = repository.save(givenA);
        assertThat(save.getId()).isEqualTo(100);
        assertThat(save.getRecVersion()).isEqualTo(100);

        givenA.setRecVersion(80L);
        save = repository.save(givenA);
        assertThat(save.getId()).isEqualTo(100);
        assertThat(save.getRecVersion()).isEqualTo(80);
    }
}
