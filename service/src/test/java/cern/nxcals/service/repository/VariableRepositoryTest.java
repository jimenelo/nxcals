package cern.nxcals.service.repository;

import cern.nxcals.api.domain.VariableDeclaredType;
import cern.nxcals.api.extraction.metadata.queries.Variables;
import cern.nxcals.service.BaseTest;
import cern.nxcals.service.domain.EntityData;
import cern.nxcals.service.domain.SystemSpecData;
import cern.nxcals.service.domain.VariableData;
import com.github.rutledgepaulv.qbuilders.conditions.Condition;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.stream.LongStream;

import static cern.nxcals.common.utils.RSQLUtils.toRSQL;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

@Transactional(transactionManager = "jpaTransactionManager")
public class VariableRepositoryTest extends BaseTest {

    @Autowired
    private VariableRepository variableRepository;
    private VariableData firstVariable;
    private VariableData secondVariable;
    private VariableData thirdVariableWithType;
    private SystemSpecData system;
    private EntityData entityData;

    @BeforeEach
    public void before() {
        setAuthentication();

        entityData = createAndSaveDefaultTestEntityKey();
        system = entityData.getPartition().getSystem();
        firstVariable = createAndPersistVariable("TEST_NAME", system, "Long Description");
        secondVariable = createAndPersistVariable("TEST_NAME2", system, "Very Long Description");
        thirdVariableWithType = createAndPersistVariable("TEST_NAME3", system, "Other",
                VariableDeclaredType.FUNDAMENTAL, null);
    }

    @Test
    @Rollback
    public void shouldAlwaysExtractFullGraph() {
        VariableData variable = createAndPersistVariable("my-name", system, "my-desc");
        variable.addConfig(createVariableConfigData(entityData, "fieldA", Instant.ofEpochSecond(1000), Instant.ofEpochSecond(2000)));
        variable.addConfig(createVariableConfigData(entityData, "fieldB", Instant.ofEpochSecond(2000), Instant.ofEpochSecond(3000)));

        persistVariable(variable);

        Condition<Variables> conditionA = Variables.suchThat().id().eq(variable.getId());
        assertThat(variableRepository.queryOne(toRSQL(conditionA), VariableData.class).get().getVariableConfigs()).hasSize(2);

        Condition<Variables> conditionB = Variables.suchThat().id().eq(variable.getId()).and().configsFieldName().eq("fieldA");
        assertThat(variableRepository.queryOne(toRSQL(conditionB), VariableData.class).get().getVariableConfigs()).hasSize(2);

        Condition<Variables> conditionC = Variables.suchThat().id().eq(variable.getId()).and().configsFieldName().eq("fieldB");
        assertThat(variableRepository.queryOne(toRSQL(conditionC), VariableData.class).get().getVariableConfigs()).hasSize(2);
    }

    @Test
    @Rollback
    public void shouldWorkWithLongLists() {
        Condition<Variables> queryCondition = Variables.suchThat().variableName().exists().and()
                .id().in(LongStream.range(1, 1002).boxed().toArray(Long[]::new));
        variableRepository.queryAll(toRSQL(queryCondition), VariableData.class);
    }

    @Test
    @Rollback
    public void shouldQueryOnExistenceOfInstants() {
        VariableData variable = createAndPersistVariable("my-name", system, "my-desc");
        variable.addConfig(createVariableConfigData(entityData, "fieldA", Instant.ofEpochSecond(1000), Instant.ofEpochSecond(2000)));
        variable.addConfig(createVariableConfigData(entityData, "fieldB", Instant.ofEpochSecond(2000), null));

        persistVariable(variable);

        Condition<Variables> conditionA = Variables.suchThat().id().eq(variable.getId()).and().configsValidToStamp().exists();
        assertThat(variableRepository.queryOne(toRSQL(conditionA), VariableData.class).get().getVariableConfigs()).hasSize(2);
    }

    @Test
    @Rollback
    public void shouldWorkWithNestedConditions() {

        Condition<Variables> nestedCondition = Variables.suchThat().systemName().eq(system.getName()).and()
                .or(Variables.suchThat().variableName().eq("TEST_NAME"),
                        Variables.suchThat().variableName().eq("TEST_NAME2"));

        List<VariableData> results = variableRepository.queryAll(
                toRSQL(nestedCondition),
                VariableData.class);

        assertThat(results).containsExactlyInAnyOrder(firstVariable, secondVariable);

    }

    @Test
    @Rollback
    public void shouldNotFindAnyResults() {
        List<VariableData> results = variableRepository.queryAll(
                toRSQL(Variables.suchThat().systemName().eq(system.getName()).and().variableName().eq("TEST_%4")),
                VariableData.class);
        assertThat(results).hasSize(0);
    }

    @Test
    @Rollback
    public void shouldFindOneResultsByRegexThatIsUsingName() {
        List<VariableData> results = variableRepository.queryAll(
                toRSQL(Variables.suchThat().systemName().eq(system.getName()).and().variableName().like("%AME2")),
                VariableData.class);

        assertThat(results).containsExactlyInAnyOrder(secondVariable);
    }

    @Test
    @Rollback
    public void shouldFindAllResultsByRegexThatIsUsingName() {
        List<VariableData> results = variableRepository.queryAll(
                toRSQL(Variables.suchThat().systemName().eq(system.getName()).and().variableName().like("TEST_NAM%")
                ), VariableData.class);

        assertThat(results).containsExactlyInAnyOrder(firstVariable, secondVariable, thirdVariableWithType);
    }

    @Test
    @Rollback
    public void shouldFindNoResultsByRegexThatIsUsingName() {
        List<VariableData> results = variableRepository.queryAll(
                toRSQL(Variables.suchThat().systemName().eq(system.getName()).and().variableName().like("TEST_NAM%22")
                ), VariableData.class);

        assertThat(results).hasSize(0);
    }

    @Test
    @Rollback
    public void shouldFindNoResultsBasedOnDescription() {
        List<VariableData> results = variableRepository.queryAll(
                toRSQL(Variables.suchThat().systemName().eq(system.getName()).and().description().like("%ON4")),
                VariableData.class);

        assertThat(results).hasSize(0);
    }

    @Test
    @Rollback
    public void shouldFindOneResultBasedOnDescription() {
        List<VariableData> results = variableRepository.queryAll(
                toRSQL(Variables.suchThat().systemName().eq(system.getName()).and().description()
                        .like("Very%on%escription")
                ), VariableData.class);

        assertThat(results).containsExactlyInAnyOrder(secondVariable);
    }

    @Test
    @Rollback
    public void shouldFindOneResultBasedOnTypeAndName() {
        Optional<VariableData> result = variableRepository.queryOne(
                toRSQL(Variables.suchThat().systemName().eq(system.getName()).and().declaredType()
                        .eq(VariableDeclaredType.FUNDAMENTAL).and().variableName().in("TEST_NAME3", "TEST_NAME1")
                ), VariableData.class);

        assertThat(result).containsSame(thirdVariableWithType);
    }

    @Test
    @Rollback
    public void shouldFindOneResultBasedOnTypeAndNameLike() {
        Optional<VariableData> result = variableRepository.queryOne(
                toRSQL(Variables.suchThat().systemName().eq(system.getName()).and().declaredType()
                        .eq(VariableDeclaredType.FUNDAMENTAL).and().variableName().like("TEST_NA%")
                ), VariableData.class);

        assertThat(result).containsSame(thirdVariableWithType);
    }

    @Test
    @Rollback
    public void shouldFindTypeNull() {
        String query = toRSQL(
                Variables.suchThat().systemName().eq(system.getName()).and().declaredType().doesNotExist().and()
                        .variableName().like("%"));

        List<VariableData> results = variableRepository.queryAll(query, VariableData.class);
        assertThat(results).containsExactlyInAnyOrder(firstVariable, secondVariable);
    }

    @Test
    @Rollback
    public void shouldFindUnitNull() {
        String query = toRSQL(
                Variables.suchThat().systemName().eq(system.getName()).and().unit().doesNotExist().and().variableName()
                        .like("%"));

        List<VariableData> results = variableRepository.queryAll(query, VariableData.class);
        assertThat(results).containsExactlyInAnyOrder(firstVariable, secondVariable, thirdVariableWithType);
    }

    @Test
    @Rollback
    public void shouldFindNameNotNull() {
        String query = toRSQL(Variables.suchThat().systemName().eq(system.getName()).and().variableName().exists());

        List<VariableData> results = variableRepository.queryAll(query, VariableData.class);
        assertThat(results).containsExactlyInAnyOrder(firstVariable, secondVariable, thirdVariableWithType);
    }

    @Test
    @Rollback
    public void shouldFindNameByPattern() {
        String query = toRSQL(
                Variables.suchThat().systemName().eq(system.getName()).and().variableName().pattern(".*ME2$"));

        List<VariableData> results = variableRepository.queryAll(query, VariableData.class);
        assertThat(results).containsExactlyInAnyOrder(secondVariable);
    }

    @Test
    @Rollback
    public void shouldFindNameByLexicallyAfter() {
        String query = toRSQL(Variables.suchThat().systemName().eq(system.getName()).and().variableName()
                .lexicallyAfter("TEST_NAME"));

        List<VariableData> results = variableRepository.queryAll(query, VariableData.class);
        assertThat(results).containsExactlyInAnyOrder(secondVariable, thirdVariableWithType);
    }

    @Test
    @Rollback
    public void shouldFindAllResultsByRegexThatIsUsingDescription() {
        List<VariableData> results = variableRepository.queryAll(
                toRSQL(Variables.suchThat().systemName().eq(system.getName()).and().description().like("%ong%")),
                VariableData.class);

        assertThat(results).containsExactlyInAnyOrder(firstVariable, secondVariable);
    }

    @Test
    @Rollback
    public void shouldNotFindWithWrongSystemName() {
        List<VariableData> results = variableRepository.queryAll(
                toRSQL(Variables.suchThat().systemName().eq("DUMMY_SYSTEM").and().variableName().like("TEST_%4")),
                VariableData.class);

        assertThat(results).hasSize(0);
    }

    @Test
    @Rollback
    public void shouldFindAllVariablesByName() {
        List<VariableData> results = variableRepository.queryAll(
                toRSQL(Variables.suchThat().systemName().eq(system.getName()).and().variableName()
                        .in(firstVariable.getVariableName(), secondVariable.getVariableName())
                ), VariableData.class);

        assertThat(results).containsExactlyInAnyOrder(firstVariable, secondVariable);
    }

    @Test
    @Rollback
    public void shouldFindByQuery() {
        //given

        //when
        List<VariableData> results = variableRepository.queryAll(
                toRSQL(Variables.suchThat().systemName().eq(system.getName()).and().variableName()
                        .eq(firstVariable.getVariableName())),
                VariableData.class);

        //then
        assertThat(results).containsExactlyInAnyOrder(firstVariable);
    }

    @Test
    @Rollback
    public void shouldFindByQuerySystemId() {
        //given

        //when
        List<VariableData> results = variableRepository.queryAll(
                toRSQL(Variables.suchThat().systemName().eq(system.getName()).and().systemId()
                        .eq(firstVariable.getSystem().getId()).and().variableName()
                        .eq(firstVariable.getVariableName())), VariableData.class);

        //then
        assertThat(results).containsExactlyInAnyOrder(firstVariable);
    }

    @Test
    @Rollback
    public void shouldFailOnWrongRQSL() {
        String query = "This is not a good format";
        assertThrows(cz.jirutka.rsql.parser.RSQLParserException.class, () -> variableRepository.queryAll(query, VariableData.class));
    }

    @Test
    public void shouldThrowOnMultipleValues() {
        //when
        assertThrows(InvalidDataAccessApiUsageException.class, () -> variableRepository.queryOne(
                toRSQL(Variables.suchThat().systemName().eq(system.getName()).and().variableName()
                        .in(firstVariable.getVariableName(), secondVariable.getVariableName())
                ), VariableData.class));
    }

    @Test
    @Rollback
    public void shouldNotAllowCreationOfLogicalVariableDuplicates() {
        String variableName = "MY:TEST:VARIABLE";
        String duplicateVariableName = "MY:TEST:VARIABLE";

        VariableData variable = createVariable(variableName);
        VariableData savedVariable = variableRepository.save(variable);
        assertNotNull(savedVariable);
        assertNotNull(savedVariable.getId());

        VariableData duplicateVariable = createVariable(duplicateVariableName);
        variableRepository.save(duplicateVariable);
        //explicitly flush changes to trigger unique db constraint rules
        assertThrows(javax.persistence.PersistenceException.class, () -> entityManager.flush());
    }

    private VariableData createVariable(String variableName) {
        VariableData variableData = new VariableData();
        variableData.setVariableName(variableName);
        variableData.setSystem(system);
        return variableData;
    }
}
