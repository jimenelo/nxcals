package cern.nxcals.service.internal.compaction;

import cern.nxcals.api.domain.TimeEntityPartitionType;
import cern.nxcals.common.domain.AdaptiveCompactionJob;
import cern.nxcals.common.paths.StagingPath;
import cern.nxcals.service.domain.PartitionResourceData;
import cern.nxcals.service.domain.PartitionResourceHistoryData;
import cern.nxcals.service.internal.PartitionResourceHistoryService;
import cern.nxcals.service.internal.PartitionResourceService;
import com.google.common.collect.Lists;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocatedFileStatus;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.RemoteIterator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.util.Collection;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AdaptiveCompactionJobCreatorTest {


    @Mock
    private PartitionResourceService partitionResourceService;
    @Mock
    private PartitionResourceHistoryService partitionResourceHistoryService;
    private final long sortThreshold = 2 * 1024 * 2014;
    private final long maxPartitionSize = AdaptiveCompactionJobCreator.ONE_GB_FILE_SIZE;
    private final int avroParquetSizeRation = 4;
    private final String dataPrefix = "/project/nxcals/nxcals_pro/data";
    private final String stagingPrefix = "/project/nxcals/nxcals_pro/staging";
    @Mock
    private FileSystem fileSystem;
    private AdaptiveCompactionJobCreator creator;
    @Mock
    private RemoteIterator<LocatedFileStatus> fileIterator;
    @Mock
    private LocatedFileStatus file;

    @Mock
    private FileStatus file1, file2, file3;

    @Mock
    private PartitionResourceData partitionResourceData;

    @Mock
    private PartitionResourceHistoryData partitionResourceHistoryData;

    @BeforeEach
    void setUp() {
        creator = new AdaptiveCompactionJobCreator(partitionResourceService, partitionResourceHistoryService,
                sortThreshold, maxPartitionSize, avroParquetSizeRation, dataPrefix, stagingPrefix, fileSystem);
    }


    @Test
    void shouldCreateJob() {
        //given
        String stagingBasePath = "/project/nxcals/nxcals_pro/staging/ETL1/1/2/3/2022-07-05";
        String dataBasePath = "/project/nxcals/nxcals_pro/data/1/2/3/2022/7/5";

        StagingPath stagingPath = new StagingPath(new Path(stagingBasePath), true);
        Collection<FileStatus> files = Lists.newArrayList(file1, file2, file3);
        when(file1.getPath()).thenReturn(new Path(stagingBasePath + "/01/file1.avro"));
        when(file2.getPath()).thenReturn(new Path(stagingBasePath + "/01/file2.avro"));
        when(file3.getPath()).thenReturn(new Path(stagingBasePath + "/02/file3.avro"));
        long jobSize = (long) 4.5 * AdaptiveCompactionJobCreator.ONE_GB_FILE_SIZE;
        long totalSize = 10 * AdaptiveCompactionJobCreator.ONE_GB_FILE_SIZE;

        when(partitionResourceService.findOrCreatePartitionResourceData(1, 2, 3)).thenReturn(partitionResourceData);
        when(partitionResourceHistoryService.findOrCreatePartitionResourceInfoFor(argThat(v -> true), argThat(v -> true))).thenReturn(partitionResourceHistoryData);
        when(partitionResourceHistoryData.getInformation()).thenReturn(TimeEntityPartitionType.of(4, 2).toString());

        //when
        AdaptiveCompactionJob job = creator.createJob(stagingPath, files, jobSize, totalSize, "prefix");

        //then
        assertEquals(dataBasePath, job.getDestinationDir().getPath());
        assertEquals("", job.getFilePrefix()); //no prefix should be used
        assertEquals(3, job.getFiles().size());
        assertEquals(stagingBasePath + "/01/file1.avro", job.getFiles().get(0).getPath());
        assertEquals(stagingBasePath + "/01/file2.avro", job.getFiles().get(1).getPath());
        assertEquals(stagingBasePath + "/02/file3.avro", job.getFiles().get(2).getPath());

        assertEquals(1, job.getSystemId());
        assertEquals(2, job.getPartitionId());
        assertEquals(3, job.getSchemaId());
        assertEquals(TimeEntityPartitionType.of(4, 2), job.getPartitionType());

    }

    @Nested
    class SelectPartitioningTypeTests {
        @Test
        void shouldSelectTypeSmallerThan11GB() {
            //given
            long size = 11 * AdaptiveCompactionJobCreator.ONE_GB_FILE_SIZE;
            //when
            TimeEntityPartitionType type = AdaptiveCompactionJobCreator.selectPartitioningType(size);

            //then
            assertEquals(TimeEntityPartitionType.of(4, 3), type);
        }

        @Test
        void shouldSelectTypeSmallest() {
            //given
            long size = (long) (0.5 * AdaptiveCompactionJobCreator.ONE_GB_FILE_SIZE);
            //when
            TimeEntityPartitionType type = AdaptiveCompactionJobCreator.selectPartitioningType(size);

            //then
            assertEquals(TimeEntityPartitionType.of(1, 1), type);
        }

        @Test
        void shouldSelectType2To4Gb() {
            //given
            long size = (long) (1.52 * AdaptiveCompactionJobCreator.ONE_GB_FILE_SIZE);
            //when
            TimeEntityPartitionType type = AdaptiveCompactionJobCreator.selectPartitioningType(size);

            //then
            assertEquals(TimeEntityPartitionType.of(2, 2), type);
        }

        @Test
        void shouldSelectType1To1Gb() {
            //given - it will be rounded to 1
            long size = (long) (1.4 * AdaptiveCompactionJobCreator.ONE_GB_FILE_SIZE);
            //when
            TimeEntityPartitionType type = AdaptiveCompactionJobCreator.selectPartitioningType(size);

            //then
            assertEquals(TimeEntityPartitionType.of(1, 1), type);
        }

        @Test
        void shouldSelectType4Gb() {
            //given
            long size = (long) (3.99999 * AdaptiveCompactionJobCreator.ONE_GB_FILE_SIZE);
            //when
            TimeEntityPartitionType type = AdaptiveCompactionJobCreator.selectPartitioningType(size);

            //then
            assertEquals(TimeEntityPartitionType.of(2, 2), type);
        }

        @Test
        void shouldSelectTypeOver4Gb() {
            //given
            long size = (long) (4.501 * AdaptiveCompactionJobCreator.ONE_GB_FILE_SIZE);
            //when
            TimeEntityPartitionType type = AdaptiveCompactionJobCreator.selectPartitioningType(size);

            //then
            //first smaller than 10 GB
            assertEquals(TimeEntityPartitionType.of(3, 2), type);
        }

        @Test
        void shouldSelectTypeOver6Gb() {
            //given
            long size = (long) (6.501 * AdaptiveCompactionJobCreator.ONE_GB_FILE_SIZE);
            //when
            TimeEntityPartitionType type = AdaptiveCompactionJobCreator.selectPartitioningType(size);

            //then
            assertEquals(TimeEntityPartitionType.of(4, 2), type);
        }

        @Test
        void shouldSelectTypeOverTheLimit() {
            //given
            long size = 1500 * AdaptiveCompactionJobCreator.ONE_GB_FILE_SIZE;
            //when
            TimeEntityPartitionType type = AdaptiveCompactionJobCreator.selectPartitioningType(size);

            //then
            assertEquals(TimeEntityPartitionType.of(96, 14), type);
        }
    }

    @Nested
    class PathCheckerTests {
        @Test
        void shouldNotVerifyJobPathAsDataExistsWithNoResourceInfoAssigned() throws IOException {
            //given
            StagingPath stagingPath = new StagingPath(
                    new Path("/project/nxcals/nxcals_pro/staging/ETL1/1/2/3/2022-07-05"), true);
            when(fileSystem.exists(
                    argThat(p -> p.toString().equals("/project/nxcals/nxcals_pro/data/1/2/3/2022/7/5")))).thenReturn(
                    true);
            when(fileSystem.listFiles(argThat(
                            p -> p.toString().equals("/project/nxcals/nxcals_pro/data/1/2/3/2022/7/5")),
                    anyBoolean()))
                    .thenReturn(fileIterator);

            when(fileIterator.hasNext()).thenReturn(true).thenReturn(false);
            when(fileIterator.next()).thenReturn(file);
            when(file.isFile()).thenReturn(true);
            when(file.getPath()).thenReturn(new Path("/project/nxcals/nxcals_pro/data/1/2/3/2022/7/5/file.parquet"));
            //when
            boolean output = creator.getPathChecker().test(stagingPath);
            //then
            assertFalse(output);
        }

        @Test
        void shouldVerifyJobPathAsDataExistsAndResourceInfoAssigned() throws IOException {
            //given
            StagingPath stagingPath = new StagingPath(
                    new Path("/project/nxcals/nxcals_pro/staging/ETL1/1/2/3/2022-07-05"), true);
            when(fileSystem.exists(
                    argThat(p -> p.toString().equals("/project/nxcals/nxcals_pro/data/1/2/3/2022/7/5")))).thenReturn(
                    true);
            when(fileSystem.listFiles(argThat(
                            p -> p.toString().equals("/project/nxcals/nxcals_pro/data/1/2/3/2022/7/5")),
                    anyBoolean()))
                    .thenReturn(fileIterator);

            when(fileIterator.hasNext()).thenReturn(true).thenReturn(false);
            when(fileIterator.next()).thenReturn(file);
            when(file.isFile()).thenReturn(true);
            when(file.getPath()).thenReturn(new Path("/project/nxcals/nxcals_pro/data/1/2/3/2022/7/5/file.parquet"));

            when(partitionResourceService
                    .findBySystemIdAndPartitionIdAndSchemaId(1, 2, 3))
                    .thenReturn(Optional.of(partitionResourceData));
            when(partitionResourceHistoryService
                    .findByPartitionResourceAndTimestamp(partitionResourceData, stagingPath.getDate().toInstant()))
                    .thenReturn(Optional.of(partitionResourceHistoryData));
            //when
            boolean output = creator.getPathChecker().test(stagingPath);
            //then
            assertTrue(output);
        }

        @Test
        void shouldVerifyJobPathWithNoData() throws IOException {
            //given
            StagingPath stagingPath = new StagingPath(
                    new Path("/project/nxcals/nxcals_pro/staging/ETL1/1/2/3/2022-07-05"), true);
            when(fileSystem.exists(
                    argThat(p -> p.toString().equals("/project/nxcals/nxcals_pro/data/1/2/3/2022/7/5")))).thenReturn(
                    true);
            when(fileSystem.listFiles(argThat(
                            p -> p.toString().equals("/project/nxcals/nxcals_pro/data/1/2/3/2022/7/5")),
                    anyBoolean()))
                    .thenReturn(fileIterator);

            when(fileIterator.hasNext()).thenReturn(false);
            //when
            boolean output = creator.getPathChecker().test(stagingPath);
            //then
            assertTrue(output);
        }
    }
}
