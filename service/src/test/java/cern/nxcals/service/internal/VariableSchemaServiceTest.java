package cern.nxcals.service.internal;

import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.EntityHistory;
import cern.nxcals.api.domain.EntitySchema;
import cern.nxcals.api.domain.Partition;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.domain.VariableConfig;
import cern.nxcals.common.domain.ExtractionCriteria;
import cern.nxcals.common.utils.ReflectionUtils;
import cern.nxcals.service.internal.extraction.ExtractionSourceConfigService;
import cern.nxcals.service.internal.extraction.VariableSourceConfig;
import org.apache.avro.Schema;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class VariableSchemaServiceTest {
    private static final Instant NOW = Instant.now();
    private static final String SCHEMA_JSON = "{\"type\":\"record\",\"name\":\"device\",\"fields\":[{\"name\":\"device\",\"type\":\"string\"},{\"name\":\"field\",\"type\":\"long\"}]}";
    private static final Schema SCHEMA = new Schema.Parser().parse(SCHEMA_JSON);
    private static final String FIELD_NAME = "field";
    private static final long VARIABLE_ID = 1L;

    @Test
    void shouldFindSchemas() {
        // given
        Set<Long> ids = Set.of(VARIABLE_ID);
        Instant dayBefore = NOW.minus(1, ChronoUnit.DAYS);
        TimeWindow timeWindow = TimeWindow.between(dayBefore, NOW);
        ExtractionCriteria criteria = ExtractionCriteria.builder().variableId(VARIABLE_ID).timeWindow(timeWindow)
                .build();
        ExtractionSourceConfigService extractionService = mock(ExtractionSourceConfigService.class);
        when(extractionService.getVariableConfigsFor(criteria)).thenReturn(
                Set.of(
                        new VariableSourceConfig(getEntity(), getVariableConfig(FIELD_NAME), getVariable())
                )
        );

        VariableSchemaService service = new VariableSchemaService(extractionService);

        // when
        Map<Long, Map<Schema, Set<TimeWindow>>> result = service.findSchemas(ids, timeWindow);

        // then
        assertFalse(result.isEmpty());

        assertTrue(result.containsKey(VARIABLE_ID));
        assertTrue(result.get(VARIABLE_ID).containsKey(Schema.create(Schema.Type.LONG)));
        assertEquals(Set.of(timeWindow), result.get(VARIABLE_ID).get(Schema.create(Schema.Type.LONG)));
    }

    @Test
    void shouldFindSchemasForVariablePointingToEntity() {
        // given
        Set<Long> ids = Set.of(VARIABLE_ID);
        Instant dayBefore = NOW.minus(1, ChronoUnit.DAYS);
        TimeWindow timeWindow = TimeWindow.between(dayBefore, NOW);
        ExtractionCriteria criteria = ExtractionCriteria.builder().variableId(VARIABLE_ID).timeWindow(timeWindow)
                .build();
        ExtractionSourceConfigService extractionService = mock(ExtractionSourceConfigService.class);
        when(extractionService.getVariableConfigsFor(criteria)).thenReturn(
                Set.of(
                        new VariableSourceConfig(getEntity(), getVariableConfig(null), getVariable())
                )
        );

        VariableSchemaService service = new VariableSchemaService(extractionService);

        // when
        Map<Long, Map<Schema, Set<TimeWindow>>> result = service.findSchemas(ids, timeWindow);

        // then
        assertFalse(result.isEmpty());

        assertTrue(result.containsKey(VARIABLE_ID));
        assertTrue(result.get(VARIABLE_ID).containsKey(SCHEMA));
        assertEquals(Set.of(timeWindow), result.get(VARIABLE_ID).get(SCHEMA));
    }

    private Entity getEntity() {
        TreeSet<EntityHistory> historySortedSet = new TreeSet<>();
        historySortedSet.add(
                getHistory(TimeWindow.before(NOW))
        );
        return Entity.builder()
                .entityHistory(historySortedSet)
                .entityKeyValues(Collections.emptyMap())
                .systemSpec(mock(SystemSpec.class))
                .partition(mock(Partition.class))
                .build();
    }

    private EntityHistory getHistory(TimeWindow timeWindow) {
        return EntityHistory.builder()
                .partition(mock(Partition.class))
                .entity(mock(Entity.class))
                .entitySchema(getSchema())
                .validity(timeWindow)
                .build();
    }

    private EntitySchema getSchema() {
        return EntitySchema.builder()
                .schemaJson(SCHEMA_JSON)
                .build();
    }

    private VariableConfig getVariableConfig(String fieldName) {
        return VariableConfig.builder()
                .fieldName(fieldName)
                .validity(TimeWindow.before(NOW))
                .build();
    }

    private Variable getVariable() {
        Variable variable = Variable.builder()
                .systemSpec(mock(SystemSpec.class))
                .variableName("SomeName")
                .build();
        ReflectionUtils.enhanceWithId(variable, VARIABLE_ID);
        return variable;
    }
}