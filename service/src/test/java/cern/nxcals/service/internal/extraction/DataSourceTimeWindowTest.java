package cern.nxcals.service.internal.extraction;

import cern.nxcals.api.domain.TimeWindow;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import java.time.ZoneId;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class DataSourceTimeWindowTest {

    private Clock clockMock;

    @BeforeEach
    public void init() {
        this.clockMock = mock(Clock.class);
        when(this.clockMock.getZone()).thenReturn(ZoneId.systemDefault());
    }

    @Test
    void shouldGetDataBothSourceTimeWindows() {
        //given
        Instant startTime = Instant.ofEpochSecond(2 * 60);
        Instant endTime = Instant.ofEpochSecond(4 * 60);

        Instant now = Instant.ofEpochSecond(3 * 60 + DataSourceTimeWindow.HBASE_PERIOD_HOURS * 3600);
        when(this.clockMock.instant()).thenReturn(now);

        //when
        DataSourceTimeWindow dsTimeWindow = DataSourceTimeWindow.getTimeWindows(TimeWindow.between(startTime, endTime),
                clockMock);

        //then
        assertNotNull(dsTimeWindow);
        assertTrue(dsTimeWindow.getHdfsTimeWindow().isPresent());
        assertTrue(dsTimeWindow.getHbaseTimeWindow().isPresent());
        assertEquals(dsTimeWindow.getHdfsTimeWindow().get().getStartTime(), startTime);
        assertEquals(dsTimeWindow.getHbaseTimeWindow().get().getEndTime(), endTime);
        assertEquals(dsTimeWindow.getHdfsTimeWindow().get().getEndTime(),
                dsTimeWindow.getHbaseTimeWindow().get().getStartTime().minus(Duration.ofNanos(1)));
        assertTrue(dsTimeWindow.getHdfsTimeWindow().get().getStartTime()
                .compareTo(dsTimeWindow.getHdfsTimeWindow().get().getEndTime()) < 0);
        assertTrue(dsTimeWindow.getHbaseTimeWindow().get().getStartTime()
                .compareTo(dsTimeWindow.getHbaseTimeWindow().get().getEndTime()) < 0);
    }

    @Test
    void shouldGetOnlyHdfsSourceTimeWindow() {
        //given
        Instant startTime = Instant.ofEpochSecond(2 * 60);
        Instant endTime = Instant.ofEpochSecond(4 * 60);

        Instant now = Instant.ofEpochSecond(5 * 60 + DataSourceTimeWindow.HBASE_PERIOD_HOURS * 3600);
        when(this.clockMock.instant()).thenReturn(now);

        //when
        DataSourceTimeWindow dsTimeWindow = DataSourceTimeWindow.getTimeWindows(TimeWindow.between(startTime, endTime),
                clockMock);

        //then
        assertNotNull(dsTimeWindow);
        assertTrue(dsTimeWindow.getHdfsTimeWindow().isPresent());
        assertEquals(dsTimeWindow.getHdfsTimeWindow().get().getStartTime(), startTime);
        assertEquals(dsTimeWindow.getHdfsTimeWindow().get().getEndTime(), endTime);
        assertTrue(dsTimeWindow.getHdfsTimeWindow().get().getStartTime()
                .compareTo(dsTimeWindow.getHdfsTimeWindow().get().getEndTime()) < 0);
    }

    @Test
    void shouldGetOnlyHbaseSourceTimeWindow() {
        //given
        Instant startTime = Instant.ofEpochSecond(2 * 60);
        Instant endTime = Instant.ofEpochSecond(4 * 60);

        Instant now = Instant.ofEpochSecond(60 + DataSourceTimeWindow.HBASE_PERIOD_HOURS * 3600);
        when(this.clockMock.instant()).thenReturn(now);

        //when
        DataSourceTimeWindow dsTimeWindow = DataSourceTimeWindow.getTimeWindows(TimeWindow.between(startTime, endTime),
                clockMock);

        //then
        assertNotNull(dsTimeWindow);
        assertTrue(dsTimeWindow.getHbaseTimeWindow().isPresent());
        assertEquals(dsTimeWindow.getHbaseTimeWindow().get().getStartTime(), startTime);
        assertEquals(dsTimeWindow.getHbaseTimeWindow().get().getEndTime(), endTime);
        assertTrue(dsTimeWindow.getHbaseTimeWindow().get().getStartTime()
                .compareTo(dsTimeWindow.getHbaseTimeWindow().get().getEndTime()) < 0);
    }

    @Test
    void shouldNotAcceptWrongTimeWindow() {
        //given
        Instant startTime = Instant.ofEpochSecond(3 * 60);
        Instant endTime = Instant.ofEpochSecond(2 * 60);
        TimeWindow between = TimeWindow.between(startTime, endTime);

        //when
        assertThrows(IllegalArgumentException.class, () -> DataSourceTimeWindow.getTimeWindows(between));
    }

}
