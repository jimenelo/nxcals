package cern.nxcals.service.internal;

import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.EntityHistory;
import cern.nxcals.api.domain.EntitySchema;
import cern.nxcals.api.domain.Partition;
import cern.nxcals.api.domain.Resource;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.TimeEntityPartitionType;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.common.SystemFields;
import cern.nxcals.common.domain.EntityExtractionResource;
import cern.nxcals.common.domain.EntityKeyValues;
import cern.nxcals.common.domain.ExtractionCriteria;
import cern.nxcals.common.domain.VariableExtractionResource;
import cern.nxcals.service.config.DataLocationProperties;
import cern.nxcals.service.domain.EntityData;
import cern.nxcals.service.domain.EntityHistoryData;
import cern.nxcals.service.domain.EntitySchemaData;
import cern.nxcals.service.domain.PartitionData;
import cern.nxcals.service.domain.PartitionPropertiesData;
import cern.nxcals.service.domain.PartitionResourceHistoryData;
import cern.nxcals.service.domain.SystemSpecData;
import cern.nxcals.service.domain.VariableConfigData;
import cern.nxcals.service.domain.VariableData;
import cern.nxcals.service.internal.hdfs.DateGrouping;
import cern.nxcals.service.internal.hdfs.DateGroupingType;
import cern.nxcals.service.repository.SystemSpecRepository;
import cern.nxcals.service.rest.exceptions.NotFoundRuntimeException;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterators;
import com.google.common.collect.Lists;
import lombok.Builder;
import lombok.Data;
import lombok.SneakyThrows;
import lombok.ToString;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.Connection;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.net.URI;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static cern.nxcals.api.domain.TimeWindow.between;
import static cern.nxcals.common.Constants.PART_FIELDS_SEPARATOR;
import static java.time.ZoneOffset.UTC;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.emptyMap;
import static java.util.Collections.singletonList;
import static java.util.Collections.singletonMap;
import static java.util.Optional.of;
import static java.util.concurrent.TimeUnit.DAYS;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.TestInstance.Lifecycle.PER_CLASS;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@TestInstance(PER_CLASS)
public class ExtractionResourceServiceTest {
    private static final String DEVICE = "device";
    private static final String KEY_SCHEMA =
            "{\"type\":\"record\",\"name\":\"CmwKey\",\"namespace\":\"cern.nxcals\",\"fields\":" + "[{\"name\":\""
                    + DEVICE + "\",\"type\":\"string\"},{\"name\":\"property\",\"type\":\"string\"}]}";
    private static final String DEVICE_NAME = DEVICE + "_name";
    private static final String KEY_VALUES =
            "{\"" + DEVICE + "\": \"" + DEVICE_NAME + "\", \"property\": \"Acquisition\"}";
    private static final String NAME = "name";
    private static final AtomicLong ID_GENERATOR = new AtomicLong();
    private static final String SYSTEM_NAME = "CMW";
    private static final long SEVEN_DAYS = DAYS.toSeconds(7);
    private static final Map<String, Object> KEY_VALUES_MAP_1 = ImmutableMap.of(DEVICE, "a", "property", "b");
    private static final Map<String, Object> KEY_VALUES_MAP_2 = ImmutableMap.of(DEVICE, "c", "property", "d");
    private EntityService entityService;
    private VariableService variableService;
    private SystemSpecData systemSpecData;
    private ExtractionResourceService instance;
    private PartitionResourceService partitionResourceService;
    private PartitionResourceHistoryService partitionResourceHistoryService;
    private Connection hbaseConnection;
    private Admin hbaseAdmin;
    private Random rand = new Random();
    private DateTimeFormatter formatter;

    @SneakyThrows
    @BeforeEach
    public void setUp() {
        DataLocationProperties properties = new DataLocationProperties();
        properties.setHbaseNamespace("namespace");
        properties.setHdfsDataDir("/data");
        properties.setSuffix("*.parquet");
        this.entityService = mock(EntityService.class);
        this.variableService = mock(VariableService.class);
        SystemSpecRepository systemRepository = mock(SystemSpecRepository.class);
        Connection hbaseConnection = mock(Connection.class);
        this.hbaseAdmin = mock(Admin.class);
        this.partitionResourceHistoryService = mock(PartitionResourceHistoryService.class);
        this.partitionResourceService = mock(PartitionResourceService.class);
        when(hbaseConnection.getAdmin()).thenReturn(hbaseAdmin);
        when(hbaseAdmin.tableExists(any())).thenReturn(true);
        when(hbaseAdmin.isTableAvailable(any())).thenReturn(true);
        when(hbaseAdmin.isTableEnabled(any())).thenReturn(true);

        Function<EntityHistory, String> pathBuilder = entityHistory -> String.join(Path.SEPARATOR,
                properties.getHdfsDataDir(),
                String.valueOf(entityHistory.getPartition().getSystemSpec().getId()),
                String.valueOf(entityHistory.getPartition().getId()),
                String.valueOf(entityHistory.getEntitySchema().getId()));

        this.instance = new ExtractionResourceService(properties,
                new DateGrouping(ChronoUnit.DAYS, DateGroupingType.DAYS), hbaseTableMaker(), pathBuilder,
                entityService, variableService, systemRepository, partitionResourceService,
                partitionResourceHistoryService, hbaseConnection, Duration.ofHours(2));
        this.systemSpecData = systemData();
        when(systemRepository.findByName(eq(SYSTEM_NAME))).thenReturn(of(this.systemSpecData));
        formatter = this.instance.getDataDirDatePattern();
    }

    private List<PartitionResourceHistoryData> buildPartitionResourceHistoryData(TimeWindow tw, int timeParts,
            int entityBuckets) {
        List<PartitionResourceHistoryData> ret = new ArrayList<>();
        Instant startDay = tw.getStartTime().truncatedTo(ChronoUnit.DAYS);
        Instant endDayPlusOne = tw.getEndTime().truncatedTo(ChronoUnit.DAYS).plus(1, ChronoUnit.DAYS);
        Instant currentDay = startDay;
        while (currentDay.isBefore(endDayPlusOne)) {
            PartitionResourceHistoryData data = new PartitionResourceHistoryData();
            data.setInformation(TimeEntityPartitionType.of(timeParts, entityBuckets).toString());
            data.setValidFromStamp(currentDay);
            data.setValidToStamp(currentDay.plus(1, ChronoUnit.DAYS));
            ret.add(data);
            currentDay = currentDay.plus(1, ChronoUnit.DAYS);
        }
        return ret;
    }

    private EntityHistory buildEntityHistory(TimeWindow window, long systemId, long partitionId, long schemaId,
            long entityId) {

        //mock everything here, only IDs are needed for the HDFS paths.
        SystemSpec system = mock(SystemSpec.class);
        when(system.getId()).thenReturn(systemId);
        Partition partition = mock(Partition.class);
        when(partition.getSystemSpec()).thenReturn(system);
        when(partition.getId()).thenReturn(partitionId);
        EntitySchema schema = mock(EntitySchema.class);
        when(schema.getId()).thenReturn(schemaId);
        Entity entity = mock(Entity.class);
        when(entity.getId()).thenReturn(entityId);

        return EntityHistory.builder()
                .entity(entity)
                .entitySchema(schema)
                .partition(partition)
                .validity(window)
                .build();
    }

    public Object[][] resourceParams() {
        long systemId = id();
        long partitionId = id();
        long schemaId = id();
        long entityId = 100;
        String entityBucketForTwo = Long.toString(ExtractionResourceService.getEntityBucket(entityId, 2));
        String entityBucketForTen = Long.toString(ExtractionResourceService.getEntityBucket(entityId, 10));

        Instant time = Instant.now().minus(10, ChronoUnit.DAYS).truncatedTo(ChronoUnit.DAYS);

        TimeWindow oneHourWindow = between(time.plus(1, ChronoUnit.HOURS), time.plus(2, ChronoUnit.HOURS));
        EntityHistory oneHourHistory = buildEntityHistory(oneHourWindow, systemId, partitionId, schemaId, entityId);

        TimeWindow tenHoursWindow = between(time, time.plus(10, ChronoUnit.HOURS).plus(10, ChronoUnit.MINUTES));
        EntityHistory tenHoursHistory = buildEntityHistory(tenHoursWindow, systemId, partitionId, schemaId, entityId);

        TimeWindow threeDaysWindow = between(time.plus(1, ChronoUnit.HOURS),
                time.plus(2, ChronoUnit.DAYS).plus(5, ChronoUnit.HOURS));
        EntityHistory threeDaysHistory = buildEntityHistory(threeDaysWindow, systemId, partitionId, schemaId, entityId);

        return new Object[][] {

                new Object[] { oneHourWindow, //short window within one day
                        Lists.newArrayList(oneHourHistory),
                        ImmutableMap.of(oneHourHistory,
                                Lists.newArrayList(buildPartitionResourceHistoryData(oneHourWindow, 2, 2))),
                        (Consumer<Resource>) res -> {
                            verifyPaths(res, Lists.newArrayList(path(time, "0", entityBucketForTwo)));
                            assertEquals(1, res.getHdfsPaths().size());
                        }
                },

                new Object[] { oneHourWindow, //short window within one day
                        Lists.newArrayList(oneHourHistory),
                        ImmutableMap.of(oneHourHistory,
                                Lists.newArrayList(buildPartitionResourceHistoryData(oneHourWindow, 2, 10))),
                        (Consumer<Resource>) res -> {
                            verifyPaths(res, Lists.newArrayList(path(time, "0", entityBucketForTen)));
                            assertEquals(1, res.getHdfsPaths().size());
                        }
                },

                new Object[] { tenHoursWindow, //10 hours window that gives 11 time slots
                        Lists.newArrayList(tenHoursHistory),
                        ImmutableMap.of(tenHoursHistory,
                                Lists.newArrayList(buildPartitionResourceHistoryData(tenHoursWindow, 24, 10))),
                        (Consumer<Resource>) res -> {
                            verifyPaths(res, Lists.newArrayList( //checking some of them
                                    path(time, "0", entityBucketForTen),
                                    path(time, "5", entityBucketForTen),
                                    path(time, "10", entityBucketForTen)));
                            assertEquals(11, res.getHdfsPaths().size());
                        }
                },

                new Object[] { threeDaysWindow, //3 days window, starting from second hour, ending 5 hours
                        Lists.newArrayList(threeDaysHistory),
                        ImmutableMap.of(threeDaysHistory,
                                Lists.newArrayList(buildPartitionResourceHistoryData(threeDaysWindow, 24, 10))),
                        (Consumer<Resource>) res -> {
                            verifyPaths(res, Lists.newArrayList( //checking some of them
                                    path(time, "1", entityBucketForTen),
                                    path(time, "5", entityBucketForTen),
                                    path(time, "10", entityBucketForTen),
                                    path(time, "23", entityBucketForTen),
                                    path(time.plus(1, ChronoUnit.DAYS), "*", entityBucketForTen), //mid day with *
                                    path(time.plus(2, ChronoUnit.DAYS), "0", entityBucketForTen),
                                    path(time.plus(2, ChronoUnit.DAYS), "1", entityBucketForTen),
                                    path(time.plus(2, ChronoUnit.DAYS), "2", entityBucketForTen),
                                    path(time.plus(2, ChronoUnit.DAYS), "3", entityBucketForTen),
                                    path(time.plus(2, ChronoUnit.DAYS), "4", entityBucketForTen),
                                    path(time.plus(2, ChronoUnit.DAYS), "5", entityBucketForTen)
                            ));
                            assertEquals(30, res.getHdfsPaths().size());
                        }
                },

                new Object[] { threeDaysWindow, //3 days window, starting from second hour, ending 5 hours
                        Lists.newArrayList(threeDaysHistory),
                        ImmutableMap.of(threeDaysHistory, Lists.newArrayList( //one day is missing from resources
                                buildPartitionResourceHistoryData(TimeWindow.between(threeDaysWindow.getStartTime(),
                                        threeDaysWindow.getEndTime().minus(1, ChronoUnit.DAYS)), 24, 10))),
                        (Consumer<Resource>) res -> {
                            verifyPaths(res, Lists.newArrayList( //checking some of them
                                    path(time, "1", entityBucketForTen),
                                    path(time, "5", entityBucketForTen),
                                    path(time, "10", entityBucketForTen),
                                    path(time, "23", entityBucketForTen),
                                    path(time.plus(1, ChronoUnit.DAYS), "*", entityBucketForTen), //mid day with *
                                    "/*.parquet" //old style path for missing partition resource history day
                            ));
                            assertEquals(25, res.getHdfsPaths().size());
                        }
                }

        };
    }

    public String path(Instant date, String timePartitions, String entityBuckets) {
        return
                formatter.format(ZonedDateTime.ofInstant(date, ZoneId.of("UTC"))) + "/" +
                        SystemFields.NXC_TIME_PARTITION_COLUMN.getValue() +
                        "=" +
                        timePartitions +
                        PART_FIELDS_SEPARATOR +
                        SystemFields.NXC_ENTITY_BUCKET_COLUMN.getValue() +
                        "=" +
                        entityBuckets;
    }

    public void verifyPaths(Resource res, List<String> containList) {
        Set<String> paths = res.getHdfsPaths().stream().map(URI::toString).collect(toSet());
        containList.forEach(subPath -> assertTrue(paths.stream().anyMatch(path -> path.contains(subPath))));
    }

    @SneakyThrows
    @ParameterizedTest
    @MethodSource("resourceParams")
    void shouldCorrectlyGenerateResourcesForAdaptiveSearches(TimeWindow queryWindow,
            List<EntityHistory> histories,
            Map<EntityHistory, List<PartitionResourceHistoryData>> partResMap,
            Consumer<Resource> verifier
    ) {
        //given
        when(hbaseAdmin.isTableEnabled(argThat(t -> true))).thenReturn(true);
        this.instance.setPartitionResourceHistoryProvider((eh, tw) -> partResMap.get(eh));

        //when
        Resource resource = this.instance.buildResource(queryWindow, histories);

        //then
        assertNotNull(resource);
        //        System.out.println(queryWindow);
        //        System.out.println(resource.getHdfsPaths());
        verifier.accept(resource);
    }

    @SneakyThrows
    @ParameterizedTest
    @MethodSource("variableParams")
    public void withSeveralVariablesConfigsAndHistories(VariableParam param) {
        TimeWindow criteriaW = param.getWindows().getCriteriaWindow();
        Map<String, Boolean> varNames = param.getVarNames();

        // given
        when(hbaseAdmin.isTableEnabled(argThat(t -> true))).thenReturn(param.tableExists);

        ExtractionCriteria criteria = ExtractionCriteria.builder().systemName(SYSTEM_NAME).variables(varNames)
                .entities(emptyList()).timeWindow(criteriaW).build();
        Set<VariableData> data = variableData(param);

        Set<String> inVars = varNames.entrySet().stream().filter(e -> !e.getValue()).map(Entry::getKey)
                .collect(toSet());
        when(this.variableService.findBySystemIdAndVariableNameIn(eq(systemSpecData.getId()), eq(inVars)))
                .thenReturn(data.stream().filter(d -> inVars.contains(d.getVariableName())).collect(toSet()));

        Set<String> likeVars = varNames.entrySet().stream().filter(Entry::getValue).map(Entry::getKey).collect(toSet());
        likeVars.forEach(
                name -> when(
                        this.variableService.findBySystemIdAndVariableNameLike(eq(systemSpecData.getId()), eq(name)))
                        .thenReturn(
                                data.stream().filter(d -> likeVars.contains(d.getVariableName())).collect(toSet())));

        data.stream().flatMap(v -> v.getVariableConfigs().stream()).forEach(vc -> {
            TimeWindow configValidity = criteriaW.intersect(vc.validity());
            when(this.entityService
                    .findAll(any(), eq(configValidity.getStartTimeNanos()), eq(configValidity.getEndTimeNanos())))
                    .thenReturn(singletonList(vc.getEntity()));
        });
        // when
        Collection<VariableExtractionResource> resources = this.instance.findVariableResourcesBy(criteria);

        // then
        TimeWindow resourceWindow = param.getWindows().getDataWindow().intersect(criteriaW);
        long resourceSize = getVariableResourcesSize(data, resourceWindow);
        assertEquals(resourceSize, resources.size());
        this.verifyVariableResources(criteriaW, data, resources, param);

    }

    @ParameterizedTest
    @MethodSource("entityParams")
    public void withSeveralEntitiesAndHistories(EntityParam param) {
        TimeWindow criteriaW = param.getWindows().getCriteriaWindow();
        List<EntityKeyValues> entityKeyValuesList = param.getEntityKeyValuesList();

        // given
        ExtractionCriteria criteria = ExtractionCriteria.builder().systemName(SYSTEM_NAME).variables(emptyMap())
                .entities(entityKeyValuesList).timeWindow(criteriaW).build();
        List<EntityDataWithPartitionResources> data = entityData(param);
        List<EntityData> entityDataList = data.stream().map(EntityDataWithPartitionResources::getEntityData)
                .collect(toList());
        Map<EntityHistory, List<PartitionResourceHistoryData>> partitionResourceMap = new HashMap<>();
        data.forEach(elt -> partitionResourceMap.putAll(elt.getPartitionResourceHistoryDataMap()));

        when(entityService.findAll(anyString(), eq(criteriaW.getStartTimeNanos()), eq(criteriaW.getEndTimeNanos())))
                .thenReturn(entityDataList);
        TimeWindow resourceWindow = param.getWindows().getDataWindow().intersect(criteriaW);
        long resourceSize = getEntityResourcesSize(entityDataList, resourceWindow);
        this.instance.setPartitionResourceHistoryProvider((eh, tw) -> partitionResourceMap.get(eh));
        // when
        Collection<EntityExtractionResource> resources = this.instance.findEntityResourcesBy(criteria);

        // then
        assertEquals(resourceSize, resources.size());
        verifyEntityResources(criteriaW, entityDataList, resources, data);
    }

    @Test
    public void shouldThrowIllegalStateExceptionWhenOneOfEntitiesWasNotFound() {
        EntityParam param = (EntityParam) entityParams()[0][0];
        TimeWindow criteriaW = param.getWindows().getCriteriaWindow();
        List<EntityKeyValues> entityKeyValuesList = param.getEntityKeyValuesList();
        entityKeyValuesList.forEach(kvs ->
                kvs.getKeyValues().entrySet().forEach(entry ->
                        entry.setValue(entry.getValue().toBuilder().wildcard(false).build())));
        assert (entityKeyValuesList.stream().noneMatch(EntityKeyValues::isAnyWildcard));

        // given
        ExtractionCriteria criteria = ExtractionCriteria.builder().systemName(SYSTEM_NAME).variables(emptyMap())
                .entities(entityKeyValuesList).timeWindow(criteriaW).build();
        List<EntityData> data = entityData(param).stream().map(EntityDataWithPartitionResources::getEntityData)
                .collect(toList());

        when(entityService.findAll(anyString(), eq(criteriaW.getStartTimeNanos()), eq(criteriaW.getEndTimeNanos())))
                .thenReturn(data.subList(0, 1));
        // when
        assertThrows(IllegalArgumentException.class, () -> this.instance.findEntityResourcesBy(criteria));
    }

    @Test
    public void shouldThrowIllegalStateExceptionWhenOneOfEntitiesSearchedByIdWasNotFound() {
        EntityParam param = (EntityParam) entityParams()[0][0];
        TimeWindow criteriaW = param.getWindows().getCriteriaWindow();
        List<EntityKeyValues> entityKeyValuesList = param.getEntityKeyValuesList();

        // given
        ExtractionCriteria criteria = ExtractionCriteria.builder().systemName(SYSTEM_NAME).variables(emptyMap())
                .entities(entityKeyValuesList).entityId(Long.MIN_VALUE).timeWindow(criteriaW).build();
        List<EntityData> data = entityData(param).stream().map(EntityDataWithPartitionResources::getEntityData)
                .collect(toList());

        when(entityService.findAll(anyString(), eq(criteriaW.getStartTimeNanos()), eq(criteriaW.getEndTimeNanos())))
                .thenReturn(data);
        // when
        assertThrows(IllegalArgumentException.class, () -> this.instance.findEntityResourcesBy(criteria));
    }

    @SneakyThrows
    @Test
    public void shouldThrowIllegalStateExceptionWhenOneOfVariablesWasNotFound() {
        VariableParam param = (VariableParam) variableParams()[0][0];
        TimeWindow criteriaW = param.getWindows().getCriteriaWindow();
        Map<String, Boolean> varNames = param.getVarNames();

        // given
        when(hbaseAdmin.isTableEnabled(argThat(t -> true))).thenReturn(param.tableExists);

        ExtractionCriteria criteria = ExtractionCriteria.builder().systemName(SYSTEM_NAME).variables(varNames)
                .entities(emptyList()).timeWindow(criteriaW).build();
        Set<VariableData> data = variableData(param);

        Set<String> inVars = varNames.entrySet().stream().filter(e -> !e.getValue()).map(Entry::getKey)
                .collect(toSet());
        when(this.variableService.findBySystemIdAndVariableNameIn(eq(systemSpecData.getId()), eq(inVars)))
                .thenReturn(data.stream().filter(d -> inVars.contains(d.getVariableName())).skip(1).collect(toSet()));

        Set<String> likeVars = varNames.entrySet().stream().filter(Entry::getValue).map(Entry::getKey).collect(toSet());
        likeVars.forEach(
                name -> when(
                        this.variableService.findBySystemIdAndVariableNameLike(eq(systemSpecData.getId()), eq(name)))
                        .thenReturn(
                                data.stream().filter(d -> likeVars.contains(d.getVariableName())).collect(toSet())));

        data.stream().flatMap(v -> v.getVariableConfigs().stream()).forEach(vc -> {
            TimeWindow configValidity = criteriaW.intersect(vc.validity());
            when(this.entityService
                    .findAll(any(), eq(configValidity.getStartTimeNanos()), eq(configValidity.getEndTimeNanos())))
                    .thenReturn(singletonList(vc.getEntity()));
        });
        // then
        assertThrows(IllegalArgumentException.class, () -> this.instance.findVariableResourcesBy(criteria));
    }

    @SneakyThrows
    @Test
    public void shouldThrowIllegalStateExceptionWhenOneOfVariablesSearchedByIdWasNotFound() {
        VariableParam param = (VariableParam) variableParams()[0][0];
        TimeWindow criteriaW = param.getWindows().getCriteriaWindow();
        Map<String, Boolean> varNames = param.getVarNames();

        // given
        when(hbaseAdmin.isTableEnabled(argThat(t -> true))).thenReturn(param.tableExists);

        ExtractionCriteria criteria = ExtractionCriteria.builder().systemName(SYSTEM_NAME).variables(varNames)
                .entities(emptyList()).variableId(Long.MIN_VALUE).timeWindow(criteriaW).build();
        Set<VariableData> data = variableData(param);

        Set<String> inVars = varNames.entrySet().stream().filter(e -> !e.getValue()).map(Entry::getKey)
                .collect(toSet());
        when(this.variableService.findBySystemIdAndVariableNameIn(eq(systemSpecData.getId()), eq(inVars)))
                .thenReturn(data.stream().filter(d -> inVars.contains(d.getVariableName())).collect(toSet()));
        when(this.variableService.findExactlyByIdIn(Set.of(Long.MIN_VALUE))).thenThrow(
                new NotFoundRuntimeException(""));

        Set<String> likeVars = varNames.entrySet().stream().filter(Entry::getValue).map(Entry::getKey).collect(toSet());
        likeVars.forEach(
                name -> when(
                        this.variableService.findBySystemIdAndVariableNameLike(eq(systemSpecData.getId()), eq(name)))
                        .thenReturn(
                                data.stream().filter(d -> likeVars.contains(d.getVariableName())).collect(toSet())));

        data.stream().flatMap(v -> v.getVariableConfigs().stream()).forEach(vc -> {
            TimeWindow configValidity = criteriaW.intersect(vc.validity());
            when(this.entityService
                    .findAll(any(), eq(configValidity.getStartTimeNanos()), eq(configValidity.getEndTimeNanos())))
                    .thenReturn(singletonList(vc.getEntity()));
        });
        // then
        assertThrows(IllegalArgumentException.class, () -> this.instance.findVariableResourcesBy(criteria));
    }

    private List<EntityDataWithPartitionResources> entityData(EntityParam param) {
        List<EntityConfig> configs = getEntityConfig(param);
        return configs.stream().map(this::getEntityData).collect(toList());
    }

    private List<EntityConfig> getEntityConfig(EntityParam param) {
        List<EntityConfig> ret = new LinkedList<>();
        for (Map<String, Object> keys : param.getEntityKeyValuesList().stream().map(EntityKeyValues::getAsMap)
                .collect(toList())) {
            Windows windows = param.getWindows();
            Collection<HistoryConfig> hConfigs = windows.getHistoryConfigs().apply(windows.getDataWindow());
            ret.add(new EntityConfig(id(), keys.toString(), hConfigs));
        }
        return ret;
    }

    private List<VarConfig> getVarConfigs(VariableParam param) {
        long entityId = id();
        long delta = SEVEN_DAYS / param.getConfigNo();
        Windows windows = param.getWindows();
        TimeWindow dataW = windows.getDataWindow();
        List<VarConfig> ret = new ArrayList<>();
        for (String varName : param.getVarNames().keySet()) {
            for (int i = 0; i < param.getConfigNo(); i++) {
                TimeWindow window = between(dataW.getStartTime().plusSeconds(i * delta),
                        dataW.getStartTime().plusSeconds((i + 1) * delta));
                Collection<HistoryConfig> hConfigs = windows.getHistoryConfigs().apply(dataW);
                EntityConfig ec = new EntityConfig(entityId, NAME, hConfigs);
                ret.add(new VarConfig(varName, window, ec));
            }
        }
        return ret;
    }

    private long getVariableResourcesSize(Collection<VariableData> data, TimeWindow resourceWindow) {
        return getEntityHistoryDataFilteredBy(data, resourceWindow).count();
    }

    private Stream<EntityHistoryData> getEntityHistoryDataFilteredBy(Collection<VariableData> data,
            TimeWindow resourceWindow) {
        return data.stream().flatMap(vd -> vd.getVariableConfigs().stream())
                .filter(c -> c.validity().rightLimit(c.validity().getEndTime().minusNanos(1))
                        .intersects(resourceWindow)).flatMap(c -> c.getEntity().getEntityHistories().stream()
                        .filter(h -> h.validity().intersect(resourceWindow)
                                .intersects(c.validity().rightLimit(c.validity().getEndTime().minusNanos(1)))));
    }

    private long getEntityResourcesSize(List<EntityData> data, TimeWindow resourceWindow) {
        return getEntityHistoryDataFilteredBy(data, resourceWindow).count();
    }

    private Stream<EntityHistoryData> getEntityHistoryDataFilteredBy(List<EntityData> data, TimeWindow resourceWindow) {
        return data.stream().flatMap(ed -> ed.getEntityHistories().stream())
                .filter(h -> !h.validity().intersect(resourceWindow).isEmpty());
    }

    private void verifyVariableResources(TimeWindow criteriaW, Set<VariableData> data,
            Collection<VariableExtractionResource> resources,
            VariableParam param) {
        for (VariableExtractionResource resource : resources) {
            TimeWindow intersect = criteriaW.intersect(resource.getTimeWindow());
            assertEquals(intersect, resource.getTimeWindow());
            assertTrue(data.stream().flatMap(v -> v.getVariableConfigs().stream()).map(VariableConfigData::getEntity)
                    .map(EntityData::toEntity).collect(toSet()).contains(resource.getEntityHistory().getEntity()));
            assertTrue(data.stream().flatMap(v -> v.getVariableConfigs().stream())
                    .map(VariableConfigData::toVariableConfig).collect(toSet()).contains(resource.getVariableConfig()));
            assertEquals(1, resource.getResource().getHbaseTableNames().size());
            assertEquals(param.tableExists, resource.getResource().isHbaseTableAccessible());

        }
    }

    private void verifyEntityResources(TimeWindow criteriaW, List<EntityData> data,
            Collection<EntityExtractionResource> resources,
            List<EntityDataWithPartitionResources> entityDataWithPartitionResources) {
        //entityid -> map of resources
        Map<Long, Map<EntityHistory, List<PartitionResourceHistoryData>>> map =
                entityDataWithPartitionResources
                        .stream()
                        .collect(Collectors.toMap(e -> e.getEntityData().getId(),
                                EntityDataWithPartitionResources::getPartitionResourceHistoryDataMap));

        for (EntityExtractionResource resource : resources) {
            TimeWindow intersect = criteriaW.intersect(resource.getTimeWindow());
            assertEquals(intersect, resource.getTimeWindow());
            assertTrue(data.stream().map(EntityData::toEntity).collect(toSet())
                    .contains(resource.getEntityHistory().getEntity()));
            assertEquals(1, resource.getResource().getHbaseTableNames().size());

            long entityId = resource.getEntityHistory().getEntity().getId();
            Map<EntityHistory, List<PartitionResourceHistoryData>> partResMap = map.get(entityId);
            List<PartitionResourceHistoryData> partResorceList = partResMap.get(resource.getEntityHistory());

            // Do not delete - this is useful for understanding what is going on here:

            //            System.out.println("EntityHistory from: " + resource.getEntityHistory().getValidity().getStartTime() + " to: " + resource.getEntityHistory().getValidity().getEndTime());
            //            System.out.println("Criteria tw: " + criteriaW);
            //            System.out.println("Resource tw: " + resource.getTimeWindow());
            //            System.out.println("Intersect tw: " + intersect);
            //            partResorceList.forEach(r -> System.out.println("from: " + r.getValidFromStamp() + " " + r.getInformation()));
            //            System.out.println("Partiton resource size: " + partResorceList.size());
            //            System.out.println(resource.getResource().getHdfsPaths());
            //            System.out.println("HDFS paths size: " + resource.getResource().getHdfsPaths().size());

            Instant startDay = intersect.getStartTime().truncatedTo(ChronoUnit.DAYS);
            Instant endDayPlusOne = intersect.getEndTime().truncatedTo(ChronoUnit.DAYS).plus(1, ChronoUnit.DAYS);
            Instant currentDay = startDay;
            while (currentDay.isBefore(endDayPlusOne)) {
                String date = this.instance.getDataDirDatePattern()
                        .format(ZonedDateTime.ofInstant(currentDay, ZoneId.of("UTC")));
                //                System.out.println("Checking date: " + date);
                //This is corse-grained checking only
                assertTrue(resource.getResource().getHdfsPaths().stream()
                        .anyMatch(path -> path.toString().contains(date)));
                currentDay = currentDay.plus(1, ChronoUnit.DAYS);
            }

        }
    }

    private Set<VariableData> variableData(VariableParam param) {
        List<VarConfig> configs = getVarConfigs(param);
        Map<String, List<VarConfig>> configByVarName = configs.stream()
                .collect(Collectors.groupingBy(VarConfig::getVariableName));
        Set<VariableData> ret = new HashSet<>();
        configByVarName.forEach((name, configList) -> {
            VariableData vd = new VariableData();
            vd.setId((long) name.hashCode());
            vd.setVariableName(name);
            vd.setSystem(this.systemSpecData);
            vd.setRecVersion(0L);
            for (VarConfig c : configList) {
                EntityDataWithPartitionResources ed = getEntityData(c.getEntityConfig());
                VariableConfigData config = getVariableConfigData(c.getWindow(), ed.getEntityData(), name);
                vd.addConfig(config);
            }
            ret.add(vd);
        });
        return ret;
    }

    private EntityDataWithPartitionResources getEntityData(EntityConfig ec) {
        EntityData ed = entityData(ec.getEntityId(),
                Iterators.getLast(ec.getHistoryConfig().iterator()).getPartitionId());

        Map<EntityHistory, List<PartitionResourceHistoryData>> partitionResourceHistoryDataMap = new HashMap<>();

        for (HistoryConfig hc : ec.getHistoryConfig()) {
            EntityHistoryData eh = entityHistoryData(hc);
            ed.addEntityHist(eh);

            partitionResourceHistoryDataMap.put(eh.toEntityHist(ed.toEntity()), hc.partitionResourceHistoryDataList);
        }

        return new EntityDataWithPartitionResources(ed, partitionResourceHistoryDataMap);
    }

    private VariableConfigData getVariableConfigData(TimeWindow window, EntityData ed, String fieldName) {
        VariableConfigData config = new VariableConfigData();
        config.setId(id());
        config.setValidFromStamp(window.getStartTime());
        config.setValidToStamp(window.getEndTime());
        config.setEntity(ed);
        config.setRecVersion(0L);
        config.setFieldName(fieldName);
        return config;
    }

    private SystemSpecData systemData() {
        SystemSpecData system = new SystemSpecData();
        system.setId(id());
        system.setName(SYSTEM_NAME);
        system.setEntityKeyDefs(KEY_SCHEMA);
        system.setPartitionKeyDefs(KEY_SCHEMA);
        system.setTimeKeyDefs(KEY_SCHEMA);
        return system;
    }

    private EntitySchemaData schemaData(long id) {
        EntitySchemaData sd = new EntitySchemaData();
        sd.setContent(KEY_SCHEMA.replace(DEVICE, DEVICE + id));
        sd.setId(id);
        return sd;
    }

    private PartitionData partitionData(long id) {
        PartitionData pd = new PartitionData();
        pd.setKeyValues(KEY_VALUES.replace(DEVICE_NAME, DEVICE_NAME + id));
        pd.setProperties(new PartitionPropertiesData(false));
        pd.setSystem(systemSpecData);
        pd.setRecVersion(0L);
        pd.setId(id);
        return pd;
    }

    private EntityHistoryData entityHistoryData(HistoryConfig config) {
        EntityHistoryData eh = new EntityHistoryData();
        eh.setId(config.getHistoryId());
        eh.setSchema(schemaData(config.getSchemaId()));
        eh.setPartition(partitionData(config.getPartitionId()));
        eh.setValidFromStamp(config.getHistoryWindow().getStartTime());
        eh.setValidToStamp(config.getHistoryWindow().getEndTime());
        return eh;
    }

    private EntityData entityData(long id, long partitionId) {
        EntityData ed = new EntityData();
        ed.setId(id);
        ed.setKeyValues(KEY_VALUES.replace(DEVICE_NAME, DEVICE_NAME + id));
        ed.setPartition(partitionData(partitionId));
        ed.setRecVersion(id());
        return ed;
    }

    private long id() {
        return ID_GENERATOR.incrementAndGet();
    }

    @SuppressWarnings("squid:UnusedPrivateMethod")
    private Object[][] variableParams() {
        Instant now = LocalDateTime.now().toLocalDate().atStartOfDay().toInstant(UTC);
        TimeWindow window = between(now.minusSeconds(SEVEN_DAYS), now);
        long threeDays = DAYS.toSeconds(3);

        List<VariableParam> singleHistory = getSingleHistoryVarParams(window, threeDays);
        List<VariableParam> ret = new LinkedList<>(singleHistory);
        List<VariableParam> twoHistories = singleHistory.stream().map(vp -> {
            VariableParam.VariableParamBuilder builder = vp.toBuilder();
            return builder.windows(vp.getWindows().toBuilder().historyConfigs(twoHistories()).build()).build();
        }).collect(toList());
        ret.addAll(twoHistories);
        List<VariableParam> severalVariables = ret.stream().map(VariableParam::toBuilder)
                .map(b -> b.varNames(ImmutableMap.of("a", true, "b", false)).build()).collect(toList());
        ret.addAll(severalVariables);
        return ret.stream().map(Collections::singleton).map(Set::toArray).toArray(Object[][]::new);
    }

    private List<VariableParam> getSingleHistoryVarParams(TimeWindow window, long threeDays) {
        List<VariableParam> singleHistory = new ArrayList<>();
        for (int i : asList(1, 2, 3, 6)) {
            singleHistory.add(new VariableParam(singletonMap(NAME, false), i,
                    new Windows(window, window, this::singleConfig), true));
            singleHistory.add(new VariableParam(singletonMap(NAME, true), i,
                    new Windows(window.rightLimit(window.getEndTime().minusSeconds(threeDays)), window,
                            this::singleConfig), true));
            singleHistory.add(new VariableParam(singletonMap(NAME, false), i,
                    new Windows(window.rightLimit(window.getEndTime().plusSeconds(threeDays)), window,
                            this::singleConfig), true));
            singleHistory.add(new VariableParam(singletonMap(NAME, true), i,
                    new Windows(window.leftLimit(window.getStartTime().minusSeconds(threeDays)), window,
                            this::singleConfig), true));
            singleHistory.add(new VariableParam(singletonMap(NAME, false), i,
                    new Windows(window.leftLimit(window.getStartTime().plusSeconds(threeDays)), window,
                            this::singleConfig), true));
            singleHistory.add(new VariableParam(singletonMap(NAME, true), i,
                    new Windows(window, window.rightLimit(window.getEndTime().minusSeconds(threeDays)),
                            this::singleConfig), true));
            singleHistory.add(new VariableParam(singletonMap(NAME, false), i,
                    new Windows(window, window.rightLimit(window.getEndTime().plusSeconds(threeDays)),
                            this::singleConfig), true));
            singleHistory.add(new VariableParam(singletonMap(NAME, true), i,
                    new Windows(window, window.leftLimit(window.getStartTime().minusSeconds(threeDays)),
                            this::singleConfig), true));
            singleHistory.add(new VariableParam(singletonMap(NAME, false), i,
                    new Windows(window, window.leftLimit(window.getStartTime().plusSeconds(threeDays)),
                            this::singleConfig), true));
            singleHistory.add(new VariableParam(singletonMap(NAME, false), i,
                    new Windows(window, window.leftLimit(window.getStartTime().plusSeconds(threeDays)),
                            this::singleConfig), false));
        }
        return singleHistory;
    }

    @SuppressWarnings("squid:UnusedPrivateMethod")
    private Object[][] entityParams() {
        Instant now = LocalDateTime.now().toLocalDate().atStartOfDay().toInstant(UTC);
        TimeWindow window = between(now.minusSeconds(SEVEN_DAYS), now);
        long threeDays = DAYS.toSeconds(3);

        List<EntityParam> singleHistory = getSingleHistoryParams(window, threeDays);
        List<EntityParam> ret = new LinkedList<>(singleHistory);
        ret.addAll(getHistoriesWithTheSameSchemaParams(singleHistory));
        ret.addAll(getThreeHistoriesWithTwoSchemasAndPartitions(singleHistory));
        List<EntityParam> severalEntities = ret.stream().map(EntityParam::toBuilder).map(b -> b.entityKeyValuesList(
                ImmutableList.of(EntityKeyValues.builder().fromMap(true, KEY_VALUES_MAP_1).build(),
                        EntityKeyValues.builder().fromMap(true, KEY_VALUES_MAP_2).build())).build()).collect(toList());
        ret.addAll(severalEntities);
        return ret.stream().map(Collections::singleton).map(Set::toArray).toArray(Object[][]::new);
    }

    private List<EntityParam> getThreeHistoriesWithTwoSchemasAndPartitions(List<EntityParam> singleHistory) {
        return singleHistory.stream().map(vp -> {
            EntityParam.EntityParamBuilder builder = vp.toBuilder();
            return builder.windows(vp.getWindows().toBuilder()
                    .historyConfigs(severalHistories(asList(Pair.of(1L, 1L), Pair.of(2L, 2L), Pair.of(2L, 1L))))
                    .build()).build();
        }).collect(toList());
    }

    private List<EntityParam> getHistoriesWithTheSameSchemaParams(List<EntityParam> singleHistory) {
        return singleHistory.stream().map(vp -> {
            EntityParam.EntityParamBuilder builder = vp.toBuilder();
            return builder.windows(vp.getWindows().toBuilder()
                    .historyConfigs(severalHistories(asList(Pair.of(1L, 2L), Pair.of(1L, 4L)))).build()).build();
        }).collect(toList());
    }

    private List<EntityParam> getSingleHistoryParams(TimeWindow window, long threeDays) {
        List<EntityParam> singleHistory = new ArrayList<>();
        singleHistory
                .add(new EntityParam(singletonList(EntityKeyValues.builder().fromMap(true, KEY_VALUES_MAP_1).build()),
                        new Windows(window, window, this::singleConfig)));
        singleHistory
                .add(new EntityParam(singletonList(EntityKeyValues.builder().fromMap(true, KEY_VALUES_MAP_1).build()),
                        new Windows(window.rightLimit(window.getEndTime().minusSeconds(threeDays)), window,
                                this::singleConfig)));
        singleHistory
                .add(new EntityParam(singletonList(EntityKeyValues.builder().fromMap(true, KEY_VALUES_MAP_1).build()),
                        new Windows(window.rightLimit(window.getEndTime().plusSeconds(threeDays)), window,
                                this::singleConfig)));
        singleHistory
                .add(new EntityParam(singletonList(EntityKeyValues.builder().fromMap(true, KEY_VALUES_MAP_1).build()),
                        new Windows(window.leftLimit(window.getStartTime().minusSeconds(threeDays)), window,
                                this::singleConfig)));
        singleHistory
                .add(new EntityParam(singletonList(EntityKeyValues.builder().fromMap(true, KEY_VALUES_MAP_1).build()),
                        new Windows(window.leftLimit(window.getStartTime().plusSeconds(threeDays)), window,
                                this::singleConfig)));
        singleHistory
                .add(new EntityParam(singletonList(EntityKeyValues.builder().fromMap(true, KEY_VALUES_MAP_1).build()),
                        new Windows(window, window.rightLimit(window.getEndTime().minusSeconds(threeDays)),
                                this::singleConfig)));
        singleHistory
                .add(new EntityParam(singletonList(EntityKeyValues.builder().fromMap(true, KEY_VALUES_MAP_1).build()),
                        new Windows(window, window.rightLimit(window.getEndTime().plusSeconds(threeDays)),
                                this::singleConfig)));
        singleHistory
                .add(new EntityParam(singletonList(EntityKeyValues.builder().fromMap(true, KEY_VALUES_MAP_1).build()),
                        new Windows(window, window.leftLimit(window.getStartTime().minusSeconds(threeDays)),
                                this::singleConfig)));
        singleHistory
                .add(new EntityParam(singletonList(EntityKeyValues.builder().fromMap(true, KEY_VALUES_MAP_1).build()),
                        new Windows(window, window.leftLimit(window.getStartTime().plusSeconds(threeDays)),
                                this::singleConfig)));
        return singleHistory;
    }

    private Function<TimeWindow, Collection<HistoryConfig>> twoHistories() {
        return dataW -> {
            TimeWindow h1Window = dataW.rightLimit(dataW.getEndTime().minusSeconds(SEVEN_DAYS / 2));
            //            TimeWindow h2Window = dataW.leftLimit(dataW.getStartTime().plusSeconds(SEVEN_DAYS / 2));
            TimeWindow h2Window = dataW.leftLimit(h1Window.getEndTime());
            return asList(new HistoryConfig(id(), id(), id(), h1Window, generatePartitionResourceHistoryData(h1Window)),
                    new HistoryConfig(id(), id(), id(), h2Window, generatePartitionResourceHistoryData(h1Window)));
        };
    }

    private Function<TimeWindow, Collection<HistoryConfig>> severalHistories(
            List<Pair<Long, Long>> schemaAndPartitionIds) {
        return dataW -> {
            List<HistoryConfig> ret = new ArrayList<>();
            for (int i = 0; i < schemaAndPartitionIds.size() - 1; i++) {
                Pair<Long, Long> schemaAndPartitionId = schemaAndPartitionIds.get(i);
                TimeWindow hw = dataW.leftLimit(dataW.getStartTime().plusSeconds(i))
                        .rightLimit(dataW.getStartTime().plusSeconds(i + 1));
                ret.add(new HistoryConfig(id(), schemaAndPartitionId.getKey(), schemaAndPartitionId.getValue(), hw,
                        generatePartitionResourceHistoryData(hw)));
            }
            Pair<Long, Long> schemaAndPartitionId = schemaAndPartitionIds.get(schemaAndPartitionIds.size() - 1);
            TimeWindow hw = dataW.leftLimit(ret.get(ret.size() - 1).getHistoryWindow().getEndTime())
                    .rightLimit(dataW.getEndTime());
            ret.add(new HistoryConfig(id(), schemaAndPartitionId.getKey(), schemaAndPartitionId.getValue(), hw,
                    generatePartitionResourceHistoryData(hw)));

            return ret;
        };
    }

    private Collection<HistoryConfig> singleConfig(TimeWindow dataW) {
        return Collections.singleton(
                new HistoryConfig(id(), id(), id(), dataW, generatePartitionResourceHistoryData(dataW)));
    }

    private List<PartitionResourceHistoryData> generatePartitionResourceHistoryData(TimeWindow tw) {
        List<PartitionResourceHistoryData> ret = new ArrayList<>();
        Instant startDay = tw.getStartTime().truncatedTo(ChronoUnit.DAYS);
        Instant endDayPlusOne = tw.getEndTime().truncatedTo(ChronoUnit.DAYS).plus(1, ChronoUnit.DAYS);
        Instant currentDay = startDay;
        while (currentDay.isBefore(endDayPlusOne)) {
            PartitionResourceHistoryData data = new PartitionResourceHistoryData();
            data.setInformation(TimeEntityPartitionType.of(2 + rand.nextInt(10), 2 + rand.nextInt(10)).toString());
            data.setValidFromStamp(currentDay);
            data.setValidToStamp(currentDay.plus(1, ChronoUnit.DAYS));
            ret.add(data);
            currentDay = currentDay.plus(1, ChronoUnit.DAYS);
        }
        return ret;
    }

    @Data
    private static class EntityDataWithPartitionResources {
        private final EntityData entityData;
        private final Map<EntityHistory, List<PartitionResourceHistoryData>> partitionResourceHistoryDataMap;
    }

    @Data
    private static class VarConfig {
        private final String variableName;
        private final TimeWindow window;
        private final EntityConfig entityConfig;
    }

    @Data
    private static class EntityConfig {
        private final long entityId;
        private final String entityName;
        private final Collection<HistoryConfig> historyConfig;
    }

    @Data
    private static class HistoryConfig {
        private final long historyId;
        private final long schemaId;
        private final long partitionId;
        private final TimeWindow historyWindow;
        private final List<PartitionResourceHistoryData> partitionResourceHistoryDataList;
    }

    @Data
    @Builder(toBuilder = true)
    private static class VariableParam {
        private final Map<String, Boolean> varNames;
        private final int configNo;
        private final Windows windows;
        private final boolean tableExists;
    }

    @Data
    @Builder(toBuilder = true)
    @ToString
    private static class EntityParam {
        private final List<EntityKeyValues> entityKeyValuesList;
        private final Windows windows;
    }

    @Data
    @Builder(toBuilder = true)
    private static class Windows {
        private final TimeWindow criteriaWindow;
        private final TimeWindow dataWindow;
        private final Function<TimeWindow, Collection<HistoryConfig>> historyConfigs;

    }

    private Function<EntityHistory, String> hbaseTableMaker() {
        return entityHistory -> String.join("__", String.valueOf(entityHistory.getPartition().getSystemSpec().getId()),
                String.valueOf(entityHistory.getPartition().getId()),
                String.valueOf(entityHistory.getEntitySchema().getId()));
    }

}
