package cern.nxcals.service.internal.compaction;

import cern.nxcals.common.domain.DataProcessingJob;
import cern.nxcals.common.paths.StagingPath;
import lombok.Builder;
import lombok.Data;
import lombok.NonNull;
import lombok.ToString;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.net.URI;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;

@ExtendWith(MockitoExtension.class)
public class DataProcessingJobCreatorTest extends CompactionTestUtils {
    @Mock
    private FileSystem fs;

    private DataProcessingJobCreatorImpl creator;

    @BeforeEach
    void init() {
        creator = new DataProcessingJobCreatorImpl(SORT_ABOVE, MAX_PARTITION_SIZE, AVRO_PARQUET_RATIO, "x", "y", fs);
    }

    @Nested
    class createMethodTests {

        @Test
        void shouldCreateOneJobForSmallFilesBucketed() {
            long fileSize = MAX_PARTITION_SIZE / 1024;
            Set<FileStatus> files = generateMixedFiles(fileSize, 20, 0);

            Optional<DataProcessingJobImpl> data = creator.create(STAGING_PATH, files);
            assertTrue(data.isPresent());

            DataProcessingJobImpl job = data.get();

            assertEquals(files.size(), job.getFiles().size());
            assertEquals(files.size() * fileSize, job.getJobSize());
            assertTrue(job.getFilePrefix().endsWith("MAX_P-"));
        }

        @Test
        void shouldCreateOneJobForSmallFilesUnBucketed() {
            long fileSize = MAX_PARTITION_SIZE / 1024;
            Set<FileStatus> files = generateMixedFiles(fileSize, 0, 100);

            Optional<DataProcessingJobImpl> data = creator.create(STAGING_PATH, files);
            assertTrue(data.isPresent());

            DataProcessingJobImpl job = data.get();

            assertEquals(files.size(), job.getFiles().size());
            assertEquals(files.size() * fileSize, job.getJobSize());
            assertEquals("0_MAX_P-", job.getFilePrefix());
        }

        @Test
        void shouldCreateOneJobForSmallFilesMixed() {
            long fileSize = MAX_PARTITION_SIZE / 1024;
            Set<FileStatus> files = generateMixedFiles(fileSize, 100, 100);

            Optional<DataProcessingJobImpl> data = creator.create(STAGING_PATH, files);
            assertTrue(data.isPresent());

            DataProcessingJobImpl job = data.get();

            assertEquals(files.size(), job.getFiles().size());
            assertEquals(files.size() * fileSize, job.getJobSize());
            assertEquals("0_MAX_P-", job.getFilePrefix());
        }

        @Test
        void shouldNotCreateMultipleJobsForLargeFilesBucketed() {
            long fileSize = MAX_PARTITION_SIZE + 1;
            Set<FileStatus> files = generateMixedFiles(fileSize, 100, 0);

            Optional<DataProcessingJobImpl> data = creator.create(STAGING_PATH, files);
            assertTrue(data.isPresent());

            DataProcessingJobImpl job = data.get();


        }

        @Test
        void shouldCreateOneJobForLargeFilesUnbucketed() {
            long fileSize = MAX_PARTITION_SIZE + 1;
            Set<FileStatus> files = generateMixedFiles(fileSize, 0, 100);

            Optional<DataProcessingJobImpl> data = creator.create(STAGING_PATH, files);
            assertTrue(data.isPresent());
        }

        @Test
        void shouldCreateOneJobForLargeFilesMixed() {
            long fileSize = MAX_PARTITION_SIZE + 1;
            Set<FileStatus> files = generateMixedFiles(fileSize, 100, 100);

            Optional<DataProcessingJobImpl> data = creator.create(STAGING_PATH, files);
            assertTrue(data.isPresent());
        }

        @Test
        void shouldCreateCorrectPrefix() {
            long fileSize = MAX_PARTITION_SIZE + 1;
            Set<FileStatus> files = generateBucketedFiles(1, fileSize, "04");

            Optional<DataProcessingJobImpl> data = creator.create(STAGING_PATH, files);
            assertTrue(data.isPresent());

            DataProcessingJobImpl job = data.get();

            assertEquals("4_MAX_P-", job.getFilePrefix());
        }

        @Test
        void shouldNotCreateJobsIfPathCheckerReturnsFalse() {
            creator.pathCheckerResult = false;

            Set<FileStatus> files = Set.of(mock(FileStatus.class));

            Optional<DataProcessingJobImpl> data = creator.create(STAGING_PATH, files);
            assertFalse(data.isPresent());
        }

        @Test
        public void shouldNotAcceptNullFiles() {
            assertThrows(IllegalArgumentException.class, () -> creator.create(mock(StagingPath.class), null));
        }

        @Test
        public void shouldNotAcceptEmptyFiles() {
            assertThrows(IllegalArgumentException.class,
                    () -> creator.create(mock(StagingPath.class), new HashSet<>()));
        }

        //Additional tests to check if the previous format of time partitions (hours prefixed with 0) will parse with the new
        //partitioning schema (just longs).
        @Test
        public void shouldParsePrefixedTime() {
            assertEquals(2L, Long.parseLong("02"));
            assertEquals(1L, Long.parseLong("01"));
            assertEquals(0L, Long.parseLong("00"));
        }
    }

    @Nested
    class ConstructorTests {
        @Test
        void shouldFailOnNegativeSortThreshold() {
            assertThrows(IllegalArgumentException.class, () ->
                    new DataProcessingJobCreatorImpl(-1, MAX_PARTITION_SIZE, AVRO_PARQUET_RATIO, "x", "y", fs));
        }

        @Test
        void shouldFailOnNegativePartitionSize() {
            assertThrows(IllegalArgumentException.class, () ->
                    new DataProcessingJobCreatorImpl(SORT_ABOVE, -1, AVRO_PARQUET_RATIO, "x", "y", fs));
        }

        @Test
        void shouldFailOnNegativeOrZeroAvroParquetRatio() {
            assertThrows(IllegalArgumentException.class, () ->
                    new DataProcessingJobCreatorImpl(SORT_ABOVE, MAX_PARTITION_SIZE, 0, "x", "y", fs));
        }
    }

    @Data
    @Builder
    static class DataProcessingJobImpl implements DataProcessingJob {
        private final String id = UUID.randomUUID().toString();
        private final String filePrefix;
        private final URI destinationDir;
        private final URI sourceDir;

        //This is a list instead of set as it is much easier to test it afterwards.
        @ToString.Exclude
        private final List<URI> files;

        private final long totalSize;
        private final long jobSize;
        private final int partitionCount;
        private final long systemId;
        private final boolean sortEnabled;

        @Override
        public JobType getType() {
            return JobType.COMPACT;
        }
    }

    static class DataProcessingJobCreatorImpl extends DataProcessingJobCreator<DataProcessingJobImpl> {
        boolean pathCheckerResult = true;

        DataProcessingJobCreatorImpl(long sortAbove, long maxPartitionSize, int avroParquetSizeRatio,
                @NonNull String outputPrefix, @NonNull String stagingPrefix, @NonNull FileSystem fs) {
            super(sortAbove, maxPartitionSize, avroParquetSizeRatio, outputPrefix, stagingPrefix, fs);
        }

        @Override
        protected Predicate<StagingPath> getPathChecker() {
            return x -> pathCheckerResult;
        }

        @Override
        protected DataProcessingJobImpl createJob(StagingPath group, Collection<FileStatus> files, long jobSize,
                long totalSize, String filePrefix) {
            return DataProcessingJobImpl.builder()
                    .filePrefix(filePrefix)
                    .systemId(group.getSystemId())
                    .sourceDir(group.toPath().toUri())
                    .destinationDir(getHdfsPathCreator().toOutputUri(group))
                    .totalSize(totalSize)
                    .jobSize(jobSize)
                    .partitionCount(1)
                    .sortEnabled(jobSize > getSortAbove())
                    .files(files.stream().map(FileStatus::getPath).map(Path::toUri).collect(Collectors.toList()))
                    .build();
        }

        @Override
        public DataProcessingJob.JobType getJobType() {
            return DataProcessingJob.JobType.COMPACT;
        }
    }

}
