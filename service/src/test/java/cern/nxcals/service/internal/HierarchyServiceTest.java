package cern.nxcals.service.internal;

import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.Group;
import cern.nxcals.api.domain.Hierarchy;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.domain.VariableConfig;
import cern.nxcals.api.domain.Visibility;
import cern.nxcals.api.extraction.metadata.queries.Hierarchies;
import cern.nxcals.common.utils.ReflectionUtils;
import cern.nxcals.service.BaseTest;
import cern.nxcals.service.domain.EntityData;
import cern.nxcals.service.domain.HierarchyData;
import cern.nxcals.service.domain.VariableData;
import cern.nxcals.service.rest.exceptions.ConfigDataConflictException;
import cern.nxcals.service.rest.exceptions.NotFoundRuntimeException;
import com.github.rutledgepaulv.qbuilders.conditions.Condition;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSortedSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;
import org.apache.commons.collections.CollectionUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import static cern.nxcals.common.utils.RSQLUtils.toRSQL;
import static cern.nxcals.service.internal.GroupServiceTest.MAIN_USER;
import static cern.nxcals.service.rest.DomainTestConstants.PARTITION_KEY_VALUES_1;
import static cern.nxcals.service.rest.TestSchemas.ENTITY_DOUBLE_SCHEMA_KEY_1;
import static cern.nxcals.service.rest.TestSchemas.ENTITY_STRING_SCHEMA_KEY_1;
import static java.util.stream.Collectors.toSet;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Transactional(transactionManager = "jpaTransactionManager")
@Rollback
public class HierarchyServiceTest extends BaseTest {
    private SystemSpec SYSTEM_SPEC_ALT;
    private SystemSpec SYSTEM_SPEC;
    private EntityData entity;

    @Autowired
    private HierarchyService service;

    @Autowired
    private VariableService variableService;

    @Autowired
    private EntityService entityService;

    @Autowired
    private GroupService groupService;

    @BeforeEach
    public void init() {
        setAuthentication();

        SYSTEM_SPEC = createAndPersistSystem("my-system");
        SYSTEM_SPEC_ALT = createAndPersistSystem("my-other-system");
        entity = createAndSaveDefaultTestEntityKey();
    }

    // create

    @Test
    public void shouldCreateFirstLevelHierarchy() {
        Hierarchy node = Hierarchy.builder().name("A").systemSpec(SYSTEM_SPEC).build();
        Hierarchy hierarchy = service.create(node).toHierarchy();

        assertEqualsHierarchy(node, hierarchy);
        assertEquals("/A", hierarchy.getNodePath());
        assertNull(hierarchy.getParent());

        assertEqualsHierarchy(hierarchy, queryByPath(hierarchy.getNodePath()));
    }

    @Test
    public void shouldTestSystemSpecCachingWhenCreatingHierarchies() {
        Hierarchy node = Hierarchy.builder().name("A").systemSpec(SYSTEM_SPEC).build();
        SystemSpec sys1 = node.getSystemSpec();
        Hierarchy hierarchy = service.create(node).toHierarchy();
        SystemSpec sys2 = hierarchy.getSystemSpec();

        assertTrue(sys1.hashCode() == sys2.hashCode());
    }

    @Test
    public void shouldNotCreateWithExisting() {
        Hierarchy node = Hierarchy.builder().name("A").systemSpec(SYSTEM_SPEC).build();
        Hierarchy hierarchy = service.create(node).toHierarchy();
        assertThrows(ConfigDataConflictException.class, () -> service.create(hierarchy));
    }

    @Test
    public void shouldNotAllowSameLevelNodesWithTheSameName() {
        Hierarchy nodeA = Hierarchy.builder().name("A").systemSpec(SYSTEM_SPEC).build();
        service.create(nodeA);

        Hierarchy nodeB = Hierarchy.builder().name("A").systemSpec(SYSTEM_SPEC).build();
        assertThrows(ConstraintViolationException.class, () -> service.create(nodeB));
    }

    @Test
    public void shouldAllowSameLevelNodesWithDifferentName() {
        Hierarchy nodeA = Hierarchy.builder().name("A").systemSpec(SYSTEM_SPEC).build();
        service.create(nodeA);

        Hierarchy nodeB = Hierarchy.builder().name("A-2").systemSpec(SYSTEM_SPEC).build();
        service.create(nodeB);
    }

    @Test
    public void shouldNotAllowSameLevelNodesWithTheSameNameDifferentSystems() {
        Hierarchy nodeA = Hierarchy.builder().name("A").systemSpec(SYSTEM_SPEC).build();
        service.create(nodeA);

        Hierarchy nodeB = Hierarchy.builder().name("A").systemSpec(SYSTEM_SPEC_ALT).build();
        assertThrows(ConstraintViolationException.class, () -> service.create(nodeB));
    }

    @Test
    public void shouldAllowDifferentLevelNodesWithTheSameName() {
        Hierarchy nodeA = Hierarchy.builder().name("A").systemSpec(SYSTEM_SPEC).build();
        Hierarchy parent = service.create(nodeA).toHierarchy();

        Hierarchy nodeB = Hierarchy.builder().name("A").parent(parent).systemSpec(SYSTEM_SPEC).build();
        service.create(nodeB);
    }

    @Test
    public void shouldNotCreateHierarchiesWithInconsistentSystems() {
        Hierarchy nodeA = createHierarchy("A", SYSTEM_SPEC).toHierarchy();
        Hierarchy nodeB = createHierarchy("A-B", nodeA, SYSTEM_SPEC).toHierarchy();
        assertThrows(IllegalArgumentException.class,
                () -> createHierarchy("A-C", nodeA, SYSTEM_SPEC_ALT).toHierarchy());
    }

    @Test
    public void shouldCreateSecondLevelHierarchies() {
        HierarchyData nodeA = createHierarchy("A", SYSTEM_SPEC);
        Hierarchy parent = queryByPath("/A");
        HierarchyData nodeB = createHierarchy("B", parent, SYSTEM_SPEC);

        assertEquals("/A/B", nodeB.getViewData().getNodePath());
        assertEquals("A", parent.getName());
        assertEquals("B", nodeB.getName());
        assertEquals("A", nodeB.getParent().getName());

        assertThat(queryByPath("/A").getChildren()).containsExactlyInAnyOrder(nodeB.toHierarchy());
        assertThat(queryByPathLike("/A/%")).containsExactlyInAnyOrder(nodeB);
    }

    @Test
    public void shouldCreateChainHierarchy() {
        Hierarchy nodeA = createHierarchy("A", SYSTEM_SPEC).toHierarchy();
        Hierarchy nodeB = createHierarchy("B", nodeA, SYSTEM_SPEC).toHierarchy();
        Hierarchy nodeC = createHierarchy("C", nodeA, SYSTEM_SPEC).toHierarchy();
        Hierarchy nodeD = createHierarchy("D", nodeB, SYSTEM_SPEC).toHierarchy();

        assertPathExists("/A");
        assertPathExists("/A/B");
        assertPathExists("/A/C");
        assertPathExists("/A/B/D");
    }

    @Test
    public void shouldSecondLevelCreateChainHierarchy() {
        Hierarchy nodeA = createHierarchy("A", SYSTEM_SPEC).toHierarchy();
        Hierarchy nodeB = createHierarchy("B", nodeA, SYSTEM_SPEC).toHierarchy();

        assertPathExists("/A");
        assertPathExists("/A/B");

        Hierarchy parent = queryByPath("/A/B");

        Hierarchy nodeC = createHierarchy("C", parent, SYSTEM_SPEC).toHierarchy();
        Hierarchy nodeD = createHierarchy("D", nodeC, SYSTEM_SPEC).toHierarchy();

        Hierarchy lvl1Found = queryByPath("/A");
        Hierarchy lvl2Found = queryByPath("/A/B");
        Hierarchy lvl3Found = queryByPath("/A/B/C");
        Hierarchy lvl4Found = queryByPath("/A/B/C/D");

        assertEquals(lvl4Found.getParent(), lvl3Found);
        assertEquals(lvl3Found.getParent(), lvl2Found);
        assertEquals(lvl2Found.getParent(), lvl1Found);
    }

    @Test
    public void shouldNotChainCreate() {
        Hierarchy nodeA = Hierarchy.builder().name("A").systemSpec(SYSTEM_SPEC).build();
        assertThrows(IllegalArgumentException.class,
                () -> Hierarchy.builder().name("B").parent(nodeA).systemSpec(SYSTEM_SPEC).build()
        );
    }

    // create all

    @Test
    public void shouldCreateAllFirstLevelHierarchies() {
        Set<Hierarchy> roots = new HashSet<>();
        roots.add(Hierarchy.builder().name("A").systemSpec(SYSTEM_SPEC).build());
        roots.add(Hierarchy.builder().name("B").systemSpec(SYSTEM_SPEC).build());
        roots.add(Hierarchy.builder().name("C").systemSpec(SYSTEM_SPEC).build());

        Map<String, Hierarchy> createdRoots = service.createAll(roots).stream().map(HierarchyData::toHierarchy)
                .collect(Collectors.toMap(Hierarchy::getName, Function.identity()));

        assertEquals(roots.size(), createdRoots.size());
        for (Hierarchy rootNode : roots) {
            Hierarchy hierarchy = createdRoots.get(rootNode.getName());
            assertEqualsHierarchy(rootNode, hierarchy);
            assertEquals("/" + rootNode.getName(), hierarchy.getNodePath());
            assertNull(hierarchy.getParent());

            assertEqualsHierarchy(hierarchy, queryByPath(hierarchy.getNodePath()));
        }

    }

    @Test
    public void shouldNotCreateAllIfContainsExistingNode() {
        Hierarchy existing = createHierarchy("A", SYSTEM_SPEC).toHierarchy(); // existing node
        assertTrue(existing.hasId());

        Set<Hierarchy> nodes = new HashSet<>();
        nodes.add(Hierarchy.builder().name("A").systemSpec(SYSTEM_SPEC).build());
        nodes.add(Hierarchy.builder().name("B").systemSpec(SYSTEM_SPEC).build());
        nodes.add(Hierarchy.builder().name("C").systemSpec(SYSTEM_SPEC).build());

        assertThrows(ConstraintViolationException.class, () -> service.createAll(nodes));
    }

    @Test
    public void shouldNotAllowCreateAllWithSameLevelNodesAndTheSameName() {
        Hierarchy existing = createHierarchy("A", SYSTEM_SPEC).toHierarchy(); // existing node
        assertTrue(existing.hasId());

        Set<Hierarchy> nodes = new HashSet<>();
        nodes.add(Hierarchy.builder().name("A").systemSpec(SYSTEM_SPEC).build());
        nodes.add(Hierarchy.builder().name("B").systemSpec(SYSTEM_SPEC).build());

        assertThrows(ConstraintViolationException.class, () -> service.createAll(nodes));
    }

    @Test
    public void shouldNotAllowCreateAllWithSameLevelNodesAndSameNameButDifferentSystems() {
        Hierarchy existing = createHierarchy("A", SYSTEM_SPEC).toHierarchy();// existing node
        assertTrue(existing.hasId());

        Set<Hierarchy> nodes = new HashSet<>();
        nodes.add(Hierarchy.builder().name("A").systemSpec(SYSTEM_SPEC_ALT).build());
        nodes.add(Hierarchy.builder().name("B").systemSpec(SYSTEM_SPEC_ALT).build());

        assertThrows(ConstraintViolationException.class, () -> service.createAll(nodes));
    }

    @Test
    public void shouldAllowCreateAllWithDifferentLevelNodesWithTheSameName() {
        Hierarchy parent = createHierarchy("A", SYSTEM_SPEC).toHierarchy(); // existing node
        assertTrue(parent.hasId());

        Hierarchy nodeA = Hierarchy.builder().name("A").parent(parent).systemSpec(SYSTEM_SPEC).build();
        Hierarchy nodeB = Hierarchy.builder().name("B").systemSpec(SYSTEM_SPEC_ALT).build(); //no parent here
        Set<Hierarchy> nodes = new HashSet<>();
        nodes.add(nodeA);
        nodes.add(nodeB);

        Map<String, Hierarchy> createdNodes = service.createAll(nodes).stream().map(HierarchyData::toHierarchy)
                .collect(Collectors.toMap(Hierarchy::getName, Function.identity()));
        assertEquals(nodes.size(), createdNodes.size());

        Hierarchy createdNodeA = createdNodes.get("A");
        assertEqualsHierarchy(nodeA, createdNodeA);
        assertEquals("/A/A", createdNodeA.getNodePath());
        assertEquals(parent, nodeA.getParent());

        Hierarchy createdNodeB = createdNodes.get("B");
        assertEqualsHierarchy(nodeB, createdNodeB);
        assertEquals("/B", createdNodeB.getNodePath());
        assertNull(createdNodeB.getParent());

        for (Hierarchy node : nodes) {
            Hierarchy hierarchy = createdNodes.get(node.getName());
            assertTrue(hierarchy.hasId());
            Hierarchy fetchedHierarchy = Iterables.getOnlyElement(service.findAll(toRSQL(Hierarchies.suchThat().id()
                    .in(hierarchy.getId())))).toHierarchy();
            assertEqualsHierarchy(hierarchy, fetchedHierarchy);
        }
    }

    @Test
    public void shouldNotPermitCreateAllHierarchiesUnderStructureWithInconsistentSystems() {
        Hierarchy existing = createHierarchy("A", SYSTEM_SPEC).toHierarchy();
        assertTrue(existing.hasId());

        Set<Hierarchy> nodes = new HashSet<>();
        nodes.add(Hierarchy.builder().name("A-B").parent(existing).systemSpec(SYSTEM_SPEC).build());
        nodes.add(Hierarchy.builder().name("A-C").parent(existing).systemSpec(SYSTEM_SPEC_ALT).build());

        assertThrows(IllegalArgumentException.class, () -> service.createAll(nodes));
    }

    @Test
    public void shouldCreateAllSecondLevelHierarchies() {
        Hierarchy parent = createHierarchy("A", SYSTEM_SPEC).toHierarchy(); //existing node
        assertTrue(parent.hasId());

        Set<Hierarchy> nodes = new HashSet<>();
        nodes.add(Hierarchy.builder().name("B").parent(parent).systemSpec(SYSTEM_SPEC).build());
        nodes.add(Hierarchy.builder().name("C").systemSpec(SYSTEM_SPEC_ALT).build());

        Map<String, HierarchyData> createdNodes = service.createAll(nodes).stream()
                .collect(Collectors.toMap(HierarchyData::getName, Function.identity()));
        assertEquals(nodes.size(), createdNodes.size());

        HierarchyData nodeB = createdNodes.get("B");
        assertNotNull(nodeB);
        assertEquals("/A/B", nodeB.getViewData().getNodePath());
        assertEquals("A", parent.getName());
        assertEquals("B", nodeB.getName());
        assertEquals("A", nodeB.getParent().getName());

        assertThat(queryByPath("/A").getChildren()).containsExactlyInAnyOrder(nodeB.toHierarchy());
        assertThat(queryByPathLike("/A/%")).containsExactlyInAnyOrder(nodeB);
    }

    @Test
    public void shouldCreateAllChainHierarchies() {
        Hierarchy nodeA = createHierarchy("A", SYSTEM_SPEC).toHierarchy();
        Hierarchy nodeAA = createHierarchy("AA", nodeA, SYSTEM_SPEC).toHierarchy();
        assertTrue(nodeA.hasId());

        Hierarchy nodeB = Hierarchy.builder().name("B").parent(nodeA).systemSpec(SYSTEM_SPEC).build();
        Hierarchy nodeC = Hierarchy.builder().name("C").parent(nodeAA).systemSpec(SYSTEM_SPEC).build();
        Hierarchy nodeAAA = Hierarchy.builder().name("AAA").systemSpec(SYSTEM_SPEC).build();

        Set<Hierarchy> nodes = new HashSet<>(Arrays.asList(nodeB, nodeC, nodeAAA));
        Set<HierarchyData> createdNodes = service.createAll(nodes);
        assertNotNull(createdNodes);
        assertEquals(nodes.size(), createdNodes.size());

        assertPathExists("/A");
        assertPathExists("/A/AA");
        assertPathExists("/AAA");
        assertPathExists("/A/B");
        assertPathExists("/A/AA/C");
    }

    // update

    @Test
    @Rollback
    public void shouldNotAllowConcurrentChange() {
        service.create(Hierarchy.builder().name("A-1").systemSpec(SYSTEM_SPEC).build());

        Condition<Hierarchies> queryCondition = Hierarchies.suchThat().name().eq("A-1");
        Hierarchy found = service.findAll(toRSQL(queryCondition)).iterator().next().toHierarchy();

        Hierarchy newHierarchy1 = found.toBuilder().name("A-2").build();
        Hierarchy newHierarchy2 = found.toBuilder().name("A-3").build();

        service.update(newHierarchy1);
        assertThrows(ObjectOptimisticLockingFailureException.class, () -> service.update(newHierarchy2));
    }

    @Test
    public void shouldNotAllowSameLevelNodesWithTheSameNameOnUpdate() {
        Hierarchy nodeA = Hierarchy.builder().name("A").systemSpec(SYSTEM_SPEC).build();
        Hierarchy nodeB = Hierarchy.builder().name("A-2").systemSpec(SYSTEM_SPEC).build();
        service.create(nodeA);
        Hierarchy nodeC = service.create(nodeB).toHierarchy();

        Hierarchy nodeD = nodeC.toBuilder().name("A").build();

        assertThrows(ConstraintViolationException.class, () -> service.update(nodeD));
    }

    @Test
    public void shouldUpdateToTopLevel() {
        Hierarchy nodeA = createHierarchy("A", SYSTEM_SPEC).toHierarchy();
        Hierarchy nodeC = createHierarchy("B", nodeA, SYSTEM_SPEC).toHierarchy();

        assertPathExists("/A/B");

        nodeC = nodeC.toBuilder().parent(null).build();

        service.update(nodeC);

        assertPathAbsent("/A/B");
        assertPathExists("/A");
        assertPathExists("/B");
    }

    @Test
    public void shouldNotCreateOnUpdate() {
        assertThrows(NotFoundRuntimeException.class, () ->
                service.update(Hierarchy.builder().name("A").systemSpec(SYSTEM_SPEC).build()));
    }

    @Test
    public void shouldNotMixUpdateAndCreate() {
        Hierarchy nodeA = createHierarchy("A", SYSTEM_SPEC).toHierarchy();
        Hierarchy nodeC = createHierarchy("C", SYSTEM_SPEC).toHierarchy();

        assertPathExists("/A");
        assertPathExists("/C");

        Hierarchy newNodeB = Hierarchy.builder().name("B").systemSpec(SYSTEM_SPEC).parent(nodeA).build();
        assertThrows(IllegalArgumentException.class, () -> nodeC.toBuilder().parent(newNodeB).build());
    }

    @Test
    public void shouldUpdateParent() {
        Hierarchy nodeA = createHierarchy("A", SYSTEM_SPEC).toHierarchy();
        Hierarchy nodeB = createHierarchy("B", SYSTEM_SPEC).toHierarchy();
        Hierarchy nodeC = createHierarchy("C", nodeA, SYSTEM_SPEC).toHierarchy();

        assertPathAbsent("/B/C");
        assertPathExists("/A/C");

        service.update(queryByPath("/A/C").toBuilder().parent(queryByPath("/B")).build());

        assertPathExists("/B/C");
        assertPathAbsent("/A/C");
    }

    @Test
    public void shouldUpdateFromTopLevel() {
        Hierarchy nodeA = createHierarchy("A", SYSTEM_SPEC).toHierarchy();
        Hierarchy nodeB = createHierarchy("B", SYSTEM_SPEC).toHierarchy();

        assertPathAbsent("/A/B");
        assertPathExists("/A");
        assertPathExists("/B");

        nodeB = nodeB.toBuilder().parent(nodeA).build();

        service.update(nodeB);

        assertPathExists("/A/B");
        assertPathExists("/A");
        assertPathAbsent("/B");
    }

    @Test
    public void shouldDetectInconsistenciesOnUpdate() {
        Hierarchy nodeA = createHierarchy("A", SYSTEM_SPEC).toHierarchy();
        Hierarchy nodeB = createHierarchy("A-B", nodeA, SYSTEM_SPEC).toHierarchy();
        Hierarchy nodeC = createHierarchy("A-C", nodeA, SYSTEM_SPEC).toHierarchy();

        Hierarchy nodeD = nodeC.toBuilder().name("A-B").build();

        assertThrows(ConstraintViolationException.class, () -> service.update(nodeD));
    }

    @Test
    public void shouldDetectInconsistenciesOnUpdateToTopLevel() {
        Hierarchy nodeA = createHierarchy("A", SYSTEM_SPEC).toHierarchy();
        Hierarchy nodeB = createHierarchy("A-B", nodeA, SYSTEM_SPEC).toHierarchy();
        Hierarchy nodeC = createHierarchy("A-C", nodeA, SYSTEM_SPEC).toHierarchy();

        Hierarchy nodeD = nodeC.toBuilder().name("A").parent(null).build();

        assertThrows(ConstraintViolationException.class, () -> service.update(nodeD));
    }

    @Test
    public void shouldUpdateFirstLevelHierarchy() {
        Hierarchy nodeOld = Hierarchy.builder().name("A").systemSpec(SYSTEM_SPEC).build();
        service.create(nodeOld).toHierarchy();
        Hierarchy foundOld = queryByPath("/A");

        Hierarchy nodeNew = foundOld.toBuilder().description("another desc").build();
        Hierarchy hierarchyNew = service.update(nodeNew).toHierarchy();
        Hierarchy foundNew = queryByPath("/A");

        assertEqualsHierarchy(nodeNew, hierarchyNew);
        assertEqualsHierarchy(foundNew, hierarchyNew);
    }

    @Test
    public void shouldNotUpdateWithAbsent() {
        Hierarchy node = Hierarchy.builder().name("A").systemSpec(SYSTEM_SPEC).build();
        assertThrows(NotFoundRuntimeException.class, () -> service.update(node));
    }

    @Test
    public void shouldNotUpdateWithFake() {
        Hierarchy node = ReflectionUtils.builderInstance(Hierarchy.InnerBuilder.class).id(-1).name("FAKE").systemSpec(SYSTEM_SPEC).build();
        assertThrows(NotFoundRuntimeException.class, () -> service.update(node));
    }

    @Test
    public void shouldFailOnCircleOnRoot() {
        Hierarchy nodeA = createHierarchy("A", SYSTEM_SPEC).toHierarchy();
        Hierarchy nodeB = createHierarchy("B", nodeA, SYSTEM_SPEC).toHierarchy();
        Hierarchy nodeC = createHierarchy("C", nodeB, SYSTEM_SPEC).toHierarchy();
        Hierarchy nodeD = createHierarchy("D", nodeC, SYSTEM_SPEC).toHierarchy();

        Hierarchy updateNode = nodeA.toBuilder().parent(nodeC).build();
        assertThrows(IllegalStateException.class, () -> service.update(updateNode));
    }

    @Test
    public void shouldFailOnCircleOnNonRoot() {
        Hierarchy nodeA = createHierarchy("A", SYSTEM_SPEC).toHierarchy();
        Hierarchy nodeB = createHierarchy("B", nodeA, SYSTEM_SPEC).toHierarchy();
        Hierarchy nodeC = createHierarchy("C", nodeB, SYSTEM_SPEC).toHierarchy();
        Hierarchy nodeD = createHierarchy("D", nodeC, SYSTEM_SPEC).toHierarchy();

        Hierarchy updateNode = nodeB.toBuilder().parent(nodeD).build();
        assertThrows(IllegalStateException.class, () -> service.update(updateNode));
    }

    @Test
    public void shouldNotModifyHierarchiesWithInconsistentSystems() {
        Hierarchy nodeA = createHierarchy("A", SYSTEM_SPEC).toHierarchy();
        Hierarchy nodeB = createHierarchy("B", nodeA, SYSTEM_SPEC).toHierarchy();
        Hierarchy nodeC = createHierarchy("C", nodeB, SYSTEM_SPEC).toHierarchy();
        Hierarchy nodeD = createHierarchy("D", nodeC, SYSTEM_SPEC).toHierarchy();

        Hierarchy lvl4Found = queryByPath("/A/B/C/D");
        Hierarchy lvl4NodeNew = lvl4Found.toBuilder().systemSpec(SYSTEM_SPEC_ALT).build();
        assertThrows(IllegalArgumentException.class, () -> service.update(lvl4NodeNew));
    }

    @Test
    public void shouldNotModifyHierarchiesWithInconsistentSystemsMiddleOfChain() {
        Hierarchy nodeA = createHierarchy("A", SYSTEM_SPEC).toHierarchy();
        Hierarchy nodeB = createHierarchy("B", nodeA, SYSTEM_SPEC).toHierarchy();
        Hierarchy nodeC = createHierarchy("C", nodeB, SYSTEM_SPEC).toHierarchy();

        Hierarchy found = queryByPath("/A").toBuilder().systemSpec(SYSTEM_SPEC_ALT).build();
        assertThrows(IllegalArgumentException.class, () -> service.update(found));
    }

    @Test
    public void shouldNotChainUpdate() {
        Hierarchy nodeA = createHierarchy("A1", SYSTEM_SPEC).toHierarchy();
        Hierarchy nodeB = createHierarchy("B1", nodeA, SYSTEM_SPEC).toHierarchy();

        assertPathExists("/A1");
        assertPathExists("/A1/B1");

        service.update(nodeB.toBuilder().name("B2").parent(nodeA.toBuilder().name("A2").build()).build());

        assertPathExists("/A1");
        assertPathExists("/A1/B2");
        assertPathAbsent("/A2/B2");
    }

    @Test
    public void shouldNotModifyAnyParents() {
        Hierarchy nodeA = createHierarchy("A", SYSTEM_SPEC).toHierarchy();
        Hierarchy nodeB = createHierarchy("B", nodeA, SYSTEM_SPEC).toHierarchy();
        Hierarchy nodeC = createHierarchy("C", nodeB, SYSTEM_SPEC).toHierarchy();
        Hierarchy nodeD = createHierarchy("D", nodeC, SYSTEM_SPEC).toHierarchy();

        assertPathExists("/A");
        assertPathExists("/A/B");
        assertPathExists("/A/B/C");
        assertPathExists("/A/B/C/D");

        Hierarchy updateNode = nodeD.toBuilder().parent(nodeC.toBuilder().parent(nodeA).build()).build();
        service.update(updateNode);

        assertPathExists("/A");
        assertPathExists("/A/B");
        assertPathExists("/A/B/C");
        assertPathExists("/A/B/C/D");

        assertPathAbsent("/A/C");
        assertPathAbsent("/A/C/D");
    }

    // update all

    @Test
    public void shouldNotAllowUpdateAllSameLevelNodesWithTheSameNameOnUpdate() {
        Set<Hierarchy> hierarchies = new HashSet<>();

        Hierarchy nodeA = Hierarchy.builder().name("A").systemSpec(SYSTEM_SPEC).build();
        Hierarchy nodeB = Hierarchy.builder().name("A-2").systemSpec(SYSTEM_SPEC).build();
        hierarchies.add(nodeA);
        hierarchies.add(nodeB);

        Map<String, Hierarchy> storedHierarchies = service.createAll(hierarchies).stream()
                .map(HierarchyData::toHierarchy)
                .collect(Collectors.toMap(Hierarchy::getName, Function.identity()));

        Hierarchy nodeC = storedHierarchies.get("A-2").toBuilder().name("A").build();
        assertThrows(ConstraintViolationException.class, () -> service.updateAll(Collections.singleton(nodeC)));
    }

    @Test
    public void shouldUpdateAllToTopLevel() {
        Hierarchy nodeA = createHierarchy("A", SYSTEM_SPEC).toHierarchy();
        Hierarchy nodeB = createHierarchy("B", nodeA, SYSTEM_SPEC).toHierarchy();
        Hierarchy nodeC = createHierarchy("C", nodeB, SYSTEM_SPEC).toHierarchy();

        assertPathExists("/A/B");
        assertPathExists("/A/B/C");

        nodeB = nodeB.toBuilder().parent(null).build();
        nodeC = nodeC.toBuilder().parent(null).build();

        service.updateAll(Sets.newHashSet(nodeB, nodeC));

        assertPathAbsent("/A/B");
        assertPathExists("/A");
        assertPathExists("/B");
        assertPathExists("/C");
    }

    @Test
    public void shouldNotCreateOnUpdateAll() {
        Hierarchy existingHierarchy = createHierarchy("A", SYSTEM_SPEC).toHierarchy();

        Hierarchy newHierarchy = Hierarchy.builder().name("B").parent(existingHierarchy)
                .systemSpec(SYSTEM_SPEC).build();
        Hierarchy updatedHierarchy = existingHierarchy.toBuilder().description("Updated description").build();

        assertThrows(NotFoundRuntimeException.class, () -> service.updateAll(Sets.newHashSet(updatedHierarchy, newHierarchy)));
    }

    @Test
    public void shouldUpdateAllParents() {
        Hierarchy nodeA = createHierarchy("A", SYSTEM_SPEC).toHierarchy();
        Hierarchy nodeB = createHierarchy("B", SYSTEM_SPEC).toHierarchy();
        Hierarchy nodeC = createHierarchy("C", nodeA, SYSTEM_SPEC).toHierarchy();
        Hierarchy nodeD = createHierarchy("D", nodeB, SYSTEM_SPEC).toHierarchy();

        assertPathAbsent("/B/C");
        assertPathExists("/A/C");

        Hierarchy updatedNodeC = queryByPath("/A/C").toBuilder().parent(queryByPath("/B")).build();
        Hierarchy updatedNodeD = queryByPath("/B/D").toBuilder().parent(null).build();

        service.updateAll(Sets.newHashSet(updatedNodeC, updatedNodeD));

        assertPathExists("/B/C");

        assertPathAbsent("/A/C");
        assertPathAbsent("/B/D");
    }

    @Test
    public void shouldDetectInconsistenciesOnUpdateAllToTopLevel() {
        Hierarchy nodeA = createHierarchy("A", SYSTEM_SPEC).toHierarchy();

        Hierarchy nodeB = Hierarchy.builder().name("A-B").parent(nodeA).systemSpec(SYSTEM_SPEC).build();
        Hierarchy nodeC = Hierarchy.builder().name("A-C").parent(nodeA).systemSpec(SYSTEM_SPEC).build();

        Map<String, Hierarchy> hierarchies = service.createAll(Sets.newHashSet(nodeB, nodeC)).stream()
                .map(HierarchyData::toHierarchy).collect(Collectors.toMap(Hierarchy::getName, Function.identity()));
        assertEquals(2, hierarchies.size());

        Hierarchy updatedNodeB = hierarchies.get("A-B").toBuilder().name("B").parent(null).build();
        Hierarchy updatedNodeC = hierarchies.get("A-C").toBuilder().name("A").parent(null).build();
        assertThrows(ConstraintViolationException.class, () -> service.updateAll(Sets.newHashSet(updatedNodeB, updatedNodeC)));
    }

    @Test
    public void shouldNotUpdateAllWithWhenHierarchyDoesNotExist() {
        Hierarchy nodeA = Hierarchy.builder().name("A").systemSpec(SYSTEM_SPEC).build();
        Hierarchy nodeB = Hierarchy.builder().name("B").systemSpec(SYSTEM_SPEC).build();
        assertThrows(NotFoundRuntimeException.class, () -> service.updateAll(Sets.newHashSet(nodeA, nodeB)));
    }

    @Test
    public void shouldNotUpdateAllWithFake() {
        Hierarchy node = ReflectionUtils.builderInstance(Hierarchy.InnerBuilder.class).id(-1).name("FAKE")
                .systemSpec(SYSTEM_SPEC).build();
        assertThrows(NotFoundRuntimeException.class, () -> service.updateAll(Collections.singleton(node)));
    }

    @Test
    public void shouldFailOnUpdateAllWhenCircleOnRoot() {
        Hierarchy nodeA = createHierarchy("A", SYSTEM_SPEC).toHierarchy();
        Hierarchy nodeB = createHierarchy("B", nodeA, SYSTEM_SPEC).toHierarchy();
        Hierarchy nodeC = createHierarchy("C", nodeB, SYSTEM_SPEC).toHierarchy();
        Hierarchy nodeD = createHierarchy("D", nodeC, SYSTEM_SPEC).toHierarchy();

        Hierarchy updateNodeA = nodeA.toBuilder().parent(nodeC).build(); // creates circle
        Hierarchy updateNodeB = nodeB.toBuilder().description("updated description").build();
        assertThrows(IllegalStateException.class, () -> service.updateAll(Sets.newHashSet(updateNodeA, updateNodeB)));
    }

    @Test
    public void shouldFailOnUpdateAllWhenCircleOnNonRoot() {
        Hierarchy nodeA = createHierarchy("A", SYSTEM_SPEC).toHierarchy();
        Hierarchy nodeB = createHierarchy("B", nodeA, SYSTEM_SPEC).toHierarchy();
        Hierarchy nodeC = createHierarchy("C", nodeB, SYSTEM_SPEC).toHierarchy();
        Hierarchy nodeD = createHierarchy("D", nodeC, SYSTEM_SPEC).toHierarchy();

        Hierarchy updateNodeA = nodeA.toBuilder().name("AA").build();
        Hierarchy updateNodeB = nodeB.toBuilder().parent(nodeD).build(); //creates circle
        assertThrows(IllegalStateException.class, () -> service.updateAll(Sets.newHashSet(updateNodeA, updateNodeB)));
    }

    @Test
    public void shouldNotModifyHierarchiesWithUpdateAllWhenInconsistentSystems() {
        Hierarchy nodeA = createHierarchy("A", SYSTEM_SPEC).toHierarchy();
        Hierarchy nodeB = createHierarchy("B", nodeA, SYSTEM_SPEC).toHierarchy();
        Hierarchy nodeC = createHierarchy("C", nodeB, SYSTEM_SPEC).toHierarchy();
        Hierarchy nodeD = createHierarchy("D", nodeC, SYSTEM_SPEC).toHierarchy();

        Hierarchy lvl4Found = queryByPath("/A/B/C/D");
        Hierarchy lvl4NodeUpdated = lvl4Found.toBuilder().systemSpec(SYSTEM_SPEC_ALT).build();
        assertThrows(IllegalArgumentException.class, () -> service.updateAll(Collections.singleton(lvl4NodeUpdated)));
    }

    @Test
    public void shouldNotUpdateAllHierarchiesWithInconsistentSystemsMiddleOfChain() {
        Hierarchy nodeA = createHierarchy("A", SYSTEM_SPEC).toHierarchy();
        Hierarchy nodeB = createHierarchy("B", nodeA, SYSTEM_SPEC).toHierarchy();
        Hierarchy nodeC = createHierarchy("C", nodeB, SYSTEM_SPEC).toHierarchy();

        Hierarchy found = queryByPath("/A").toBuilder().systemSpec(SYSTEM_SPEC_ALT).build();
        assertThrows(IllegalArgumentException.class, () -> service.updateAll(Collections.singleton(found)));
    }

    @Test
    public void shouldNotChainUpdateAll() {
        Hierarchy nodeA = createHierarchy("A1", SYSTEM_SPEC).toHierarchy();
        Hierarchy nodeB = createHierarchy("B1", nodeA, SYSTEM_SPEC).toHierarchy();

        assertPathExists("/A1");
        assertPathExists("/A1/B1");

        Hierarchy updatedNodeB = nodeB.toBuilder().name("B2").parent(nodeA.toBuilder().name("A2").build()).build();

        Set<HierarchyData> hierarchyData = service.updateAll(Collections.singleton(updatedNodeB));
        assertEquals(1, hierarchyData.size());

        assertPathExists("/A1");
        assertPathExists("/A1/B2");
        assertPathAbsent("/A2/B2");
    }

    @Test
    public void shouldNotModifyAnyParentsWithUpdateAll() {
        Hierarchy nodeA = createHierarchy("A", SYSTEM_SPEC).toHierarchy();
        Hierarchy nodeB = createHierarchy("B", nodeA, SYSTEM_SPEC).toHierarchy();
        Hierarchy nodeC = createHierarchy("C", nodeB, SYSTEM_SPEC).toHierarchy();
        Hierarchy nodeD = createHierarchy("D", nodeC, SYSTEM_SPEC).toHierarchy();

        assertPathExists("/A");
        assertPathExists("/A/B");
        assertPathExists("/A/B/C");
        assertPathExists("/A/B/C/D");

        Hierarchy updateNode = nodeD.toBuilder().parent(nodeC.toBuilder().parent(nodeA).build()).build();
        service.updateAll(Collections.singleton(updateNode));

        assertPathExists("/A");
        assertPathExists("/A/B");
        assertPathExists("/A/B/C");
        assertPathExists("/A/B/C/D");

        assertPathAbsent("/A/C");
        assertPathAbsent("/A/C/D");
    }

    // deleteLeaf

    @Test
    public void shouldDelete() {
        Hierarchy parent = service.create(Hierarchy.builder().name("A").systemSpec(SYSTEM_SPEC).build()).toHierarchy();
        Hierarchy child = service.create(Hierarchy.builder().name("B").systemSpec(SYSTEM_SPEC).parent(parent).build())
                .toHierarchy();

        assertPathExists("/A/B");
        service.deleteLeaf(child.getId());
        assertPathAbsent("/A/B");
        assertPathExists("/A");
    }

    @Test
    public void shouldFailOnDeleteAbsent() {
        assertThrows(NotFoundRuntimeException.class, () -> service.deleteLeaf(1000));
    }

    @Test
    public void shouldFailOnDeletingNonLeaf() {
        Hierarchy parent = service.create(Hierarchy.builder().name("A").systemSpec(SYSTEM_SPEC).build()).toHierarchy();
        Hierarchy child = service.create(Hierarchy.builder().name("B").systemSpec(SYSTEM_SPEC).parent(parent).build())
                .toHierarchy();

        assertPathExists("/A/B");

        assertThrows(IllegalStateException.class, () -> service.deleteLeaf(parent.getId()));
    }

    // deleteAllLeaves

    @Test
    public void shouldDeleteAllLeaves() {
        Hierarchy parent = createHierarchy("A", SYSTEM_SPEC).toHierarchy();
        Hierarchy child1 = createHierarchy("B", parent, SYSTEM_SPEC).toHierarchy();
        Hierarchy child2 = createHierarchy("BB", parent, SYSTEM_SPEC).toHierarchy();

        assertPathExists("/A/B");
        assertPathExists("/A/BB");
        service.deleteAllLeaves(Sets.newHashSet(child1.getId(), child2.getId()));
        assertPathAbsent("/A/B");
        assertPathAbsent("/A/BB");
        assertPathExists("/A");
    }

    @Test
    public void shouldFailOnDeleteAllLeavesWhenNodesAbsent() {
        Hierarchy parent = createHierarchy("A", SYSTEM_SPEC).toHierarchy();
        Hierarchy child = createHierarchy("B", parent, SYSTEM_SPEC).toHierarchy();
        assertPathExists("/A/B");

        long nonExistingId = 1000;
        assertThrows(NotFoundRuntimeException.class, () -> service.deleteAllLeaves(Sets.newHashSet(child.getId(), nonExistingId)));
    }

    @Test
    public void shouldFailOnDeleteAllLeavesWhenNodeNonLeaf() {
        Hierarchy node = createHierarchy("TEST", SYSTEM_SPEC).toHierarchy();
        Hierarchy parent = createHierarchy("A", SYSTEM_SPEC).toHierarchy();
        createHierarchy("B", parent, SYSTEM_SPEC);

        assertPathExists("/TEST");
        assertPathExists("/A/B");
        assertThrows(IllegalStateException.class, () -> service.deleteAllLeaves(Sets.newHashSet(node.getId(), parent.getId())));
    }

    // fetch

    @Test
    public void shouldQueryByParent() {
        Hierarchy nodeA = createHierarchy("A", SYSTEM_SPEC).toHierarchy();
        Hierarchy nodeB = createHierarchy("B", nodeA, SYSTEM_SPEC).toHierarchy();
        Hierarchy nodeC = createHierarchy("C", nodeA, SYSTEM_SPEC).toHierarchy();
        Hierarchy nodeD = createHierarchy("D", nodeB, SYSTEM_SPEC).toHierarchy();

        assertThat(queryByParentId(nodeA.getId())).contains(nodeB, nodeC);
        assertThat(queryByParentId(nodeB.getId())).contains(nodeD);
        assertThat(queryByParentId(nodeC.getId())).isEmpty();
    }

    @Test
    public void shouldGetTopLevelHierarchyByPath() {
        HierarchyData nodeA = createHierarchy("A", SYSTEM_SPEC);
        HierarchyData nodeB = createHierarchy("B", nodeA.toHierarchy(), SYSTEM_SPEC);
        HierarchyData nodeC = createHierarchy("C", SYSTEM_SPEC);
        HierarchyData nodeD = createHierarchy("D", nodeC.toHierarchy(), SYSTEM_SPEC);

        Condition<Hierarchies> query = Hierarchies.suchThat().areTopLevel().and().systemName()
                .eq(SYSTEM_SPEC.getName());
        assertThat(service.findAll(toRSQL(query))).containsExactlyInAnyOrder(nodeA, nodeC);
    }

    // get/set variables and entities

    @Test
    public void shouldSkipAlreadyAttachedVariables() {
        Hierarchy node = createHierarchy("A", SYSTEM_SPEC).toHierarchy();

        Variable variable = createVariable("VV1", "field");
        service.addVariables(node.getId(), Sets.newHashSet(variable.getId()));
        service.addVariables(node.getId(), Sets.newHashSet(variable.getId()));
        service.addVariables(node.getId(), Sets.newHashSet(variable.getId()));
        service.addVariables(node.getId(), Sets.newHashSet(variable.getId()));

        assertThat(service.getVariables(node.getId())).hasSize(1);
    }

    @Test
    public void shouldAdd1001Variables() {
        Set<Long> variables = new HashSet<>();
        for (int i = 0; i < 1001; i++) {
            variables.add(createVariable("v" + i, "field").getId());
        }

        Hierarchy node = createHierarchy("A", SYSTEM_SPEC).toHierarchy();

        service.setVariables(node.getId(), variables);

        Set<Variable> variables1 = service.getVariables(node.getId()).stream().map(VariableData::toVariable).collect(
                toSet());
        assertThat(variables1).hasSize(1001);

        service.addVariables(node.getId(), Sets.newHashSet(createVariable("VV1", "field").getId()));

        Set<Variable> variables2 = service.getVariables(node.getId()).stream().map(VariableData::toVariable).collect(
                toSet());
        assertThat(variables2).hasSize(1002);
    }

    @Test
    public void shouldSkipAlreadyAttachedEntities() {
        Hierarchy node = createHierarchy("A", SYSTEM_SPEC).toHierarchy();

        Entity entity = createEntity("ee1");
        service.addEntities(node.getId(), Sets.newHashSet(entity.getId()));
        service.addEntities(node.getId(), Sets.newHashSet(entity.getId()));
        service.addEntities(node.getId(), Sets.newHashSet(entity.getId()));
        service.addEntities(node.getId(), Sets.newHashSet(entity.getId()));

        assertThat(service.getEntities(node.getId())).hasSize(1);
    }

    @Test
    public void shouldAddMoreThan1kEntities() {
        Set<Long> entities = new HashSet<>();
        for (int i = 0; i < 1001; i++) {
            entities.add(createEntity("e" + i).getId());
        }

        Hierarchy node = createHierarchy("A", SYSTEM_SPEC).toHierarchy();

        service.setEntities(node.getId(), entities);

        Set<Entity> entities1 = service.getEntities(node.getId()).stream().map(EntityData::toEntity).collect(toSet());
        assertThat(entities1).hasSize(1001);

        service.addEntities(node.getId(),  Sets.newHashSet(createEntity("ee1").getId()));

        Set<Entity> entities2 = service.getEntities(node.getId()).stream().map(EntityData::toEntity).collect(toSet());
        assertThat(entities2).hasSize(1002);
    }

    @Test
    public void shouldAddVariablesToExistingHierarchy() {
        Hierarchy node = createHierarchy("A", SYSTEM_SPEC).toHierarchy();

        Variable v1 = createVariable("v1", "field");
        Variable v2 = createVariable("v2", "field");

        service.setVariables(node.getId(), Sets.newHashSet(v1.getId()));
        assertThat(service.getVariables(node.getId())).extracting(VariableData::toVariable)
                .containsExactlyInAnyOrder(v1);

        service.addVariables(node.getId(), Sets.newHashSet(v2.getId()));
        assertThat(service.getVariables(node.getId())).extracting(VariableData::toVariable)
                .containsExactlyInAnyOrder(v1, v2);

        service.removeVariables(node.getId(), Sets.newHashSet(v1.getId()));
        assertThat(service.getVariables(node.getId())).extracting(VariableData::toVariable)
                .containsExactlyInAnyOrder(v2);
    }

    @Test
    public void shouldUpdateVariablesOfExistingHierarchy() {
        Hierarchy node = createHierarchy("A", SYSTEM_SPEC).toHierarchy();

        Variable v1 = createVariable("v1", "field");
        Variable v2 = createVariable("v2", "field");
        Variable v3 = createVariable("v3", "field");

        service.setVariables(node.getId(), Sets.newHashSet(v1.getId(), v2.getId()));
        Set<Variable> variables = service.getVariables(node.getId()).stream().map(VariableData::toVariable).collect(
                toSet());

        variables.remove(v2);
        variables.add(v3);

        service.setVariables(node.getId(), variables.stream().map(Variable::getId).collect(toSet()));

        assertThat(service.getVariables(node.getId())).extracting(VariableData::toVariable)
                .containsExactlyInAnyOrder(v1, v3);
    }

    @Test
    public void shouldAddEntitiesToExistingHierarchy() {
        Hierarchy node = createHierarchy("A", SYSTEM_SPEC).toHierarchy();

        Entity e1 = createEntity("e1");
        Entity e2 = createEntity("e2");

        service.setEntities(node.getId(), Sets.newHashSet(e1.getId()));
        assertThat(service.getEntities(node.getId())).extracting(EntityData::toEntity)
                .containsExactlyInAnyOrder(e1);

        service.addEntities(node.getId(), Sets.newHashSet(e2.getId()));
        assertThat(service.getEntities(node.getId())).extracting(EntityData::toEntity)
                .containsExactlyInAnyOrder(e1, e2);

        service.removeEntities(node.getId(), Sets.newHashSet(e1.getId()));
        assertThat(service.getEntities(node.getId())).extracting(EntityData::toEntity)
                .containsExactlyInAnyOrder(e2);
    }

    @Test
    public void shouldUpdateEntitiesOfExistingHierarchy() {
        Hierarchy node = createHierarchy("A", SYSTEM_SPEC).toHierarchy();

        Entity e1 = createEntity("e1");
        Entity e2 = createEntity("e2");
        Entity e3 = createEntity("e3");

        service.setEntities(node.getId(), Sets.newHashSet(e1.getId(), e2.getId()));
        Set<Entity> entities = service.getEntities(node.getId()).stream().map(EntityData::toEntity).collect(toSet());

        entities.remove(e2);
        entities.add(e3);

        service.setEntities(node.getId(), entities.stream().map(Entity::getId).collect(toSet()));
        entities = service.getEntities(node.getId()).stream().map(EntityData::toEntity).collect(toSet());

        assertThat(entities).containsExactlyInAnyOrder(e1, e3);
    }

    @Test
    public void shouldThrowOnGetHierarchiesForVariablesWhenVariablesSetIsNull() {
        assertThrows(NullPointerException.class, () -> service.getHierarchiesForVariables(null));
    }

    @Test
    public void shouldReturnEmptySetOnGetHierarchiesForVariablesWhenVariableDoesNotExist() {
        Map<Long, Set<HierarchyData>> hierarchiesByVariableId = service
                .getHierarchiesForVariables(Collections.singleton(1234L));
        assertThat(hierarchiesByVariableId).isEmpty();
    }

    @Test
    public void shouldReturnEmptySetOnGetHierarchiesForVariablesWhenVariableHasNoAssociationsWithHierarchy() {
        Variable v1 = createVariable("test-no-association", "test-field");
        Variable v2 = createVariable("test-group-association", "test-snapshot");
        Group snapshot = createSnapshot("test-snapshot", Collections.singleton(v2));
        assertTrue(snapshot.hasId());

        Map<Long, Set<HierarchyData>> hierarchiesByVariableId = service
                .getHierarchiesForVariables(Sets.newHashSet(v1.getId(), v2.getId(), 1234L));
        assertThat(hierarchiesByVariableId).isEmpty();
    }

    @Test
    public void shouldGetHierarchiesForVariables() {
        Hierarchy node = createHierarchy("A", SYSTEM_SPEC).toHierarchy();
        Hierarchy node2 = createHierarchy("B", SYSTEM_SPEC).toHierarchy();

        Variable v1 = createVariable("test-hierarchy-var", "test-field");
        Variable v2 = createVariable("test-hierarchy-var2", "test-field");

        service.setVariables(node.getId(), Sets.newHashSet(v1.getId(), v2.getId()));
        service.setVariables(node2.getId(), Collections.singleton(v1.getId()));

        Map<Long, Set<HierarchyData>> hierarchiesByVariableId = service
                .getHierarchiesForVariables(Sets.newHashSet(v1.getId(), v2.getId()));
        assertThat(hierarchiesByVariableId).isNotEmpty();
        assertThat(hierarchiesByVariableId.size()).isEqualTo(2);

        Set<HierarchyData> hierarchiesForVar1 = hierarchiesByVariableId.get(v1.getId());
        assertThat(hierarchiesForVar1).isNotEmpty();
        assertThat(hierarchiesForVar1.size()).isEqualTo(2);
        assertEquals(Sets.newHashSet(node.getId(), node2.getId()),
                hierarchiesForVar1.stream().map(HierarchyData::getId).collect(toSet()));

        Set<HierarchyData> hierarchiesForVar2 = hierarchiesByVariableId.get(v2.getId());
        assertThat(hierarchiesForVar2).isNotEmpty();
        assertThat(hierarchiesForVar2.size()).isEqualTo(1);
        assertEquals(Collections.singleton(node.getId()),
                hierarchiesForVar2.stream().map(HierarchyData::getId).collect(toSet()));
    }

    @Test
    public void shouldGetHierarchiesForOver1kVariables() {
        Hierarchy node = createHierarchy("A", SYSTEM_SPEC).toHierarchy();
        int numOfVariables = 1010;
        Set<Variable> variables = createVariables("test-hierarchy-var", "test-field", numOfVariables);
        Set<Long> variableIds = variables.stream().map(Variable::getId).collect(toSet());
        service.setVariables(node.getId(), variableIds);

        Map<Long, Set<HierarchyData>> hierarchiesByVariableId = service
                .getHierarchiesForVariables(variableIds);
        assertThat(hierarchiesByVariableId).isNotEmpty();
        assertThat(hierarchiesByVariableId.size()).isEqualTo(numOfVariables);

        hierarchiesByVariableId.forEach((k, v) -> {
            assertTrue(variableIds.contains(k));
            assertThat(v).isNotEmpty();
            assertThat(v.size()).isEqualTo(1);
            assertEquals(Collections.singleton(node.getId()), v.stream().map(HierarchyData::getId).collect(toSet()));
        });
    }

    @Test
    public void shouldThrowOnGetHierarchyIdsForVariablesWhenVariablesSetIsNull() {
        assertThrows(NullPointerException.class, () -> service.getHierarchyIdsForVariables(null));
    }

    @Test
    public void shouldReturnEmptySetOnGetHierarchyIdsForVariablesWhenVariableDoesNotExist() {
        Map<Long, Set<Long>> hierarchiesByVariableId = service
                .getHierarchyIdsForVariables(Collections.singleton(1234L));
        assertThat(hierarchiesByVariableId).isEmpty();
    }

    @Test
    public void shouldReturnEmptySetOnGetHierarchyIdsForVariablesWhenVariableHasNoAssociationsWithHierarchy() {
        Variable v1 = createVariable("test-no-association", "test-field");
        Variable v2 = createVariable("test-group-association", "test-snapshot");
        Group snapshot = createSnapshot("test-snapshot", Collections.singleton(v2));
        assertTrue(snapshot.hasId());

        Map<Long, Set<Long>> hierarchiesByVariableId = service
                .getHierarchyIdsForVariables(Sets.newHashSet(v1.getId(), v2.getId(), 1234L));
        assertThat(hierarchiesByVariableId).isEmpty();
    }

    @Test
    public void shouldGetHierarchyIdsForVariables() {
        Hierarchy node = createHierarchy("A", SYSTEM_SPEC).toHierarchy();
        Hierarchy node2 = createHierarchy("B", SYSTEM_SPEC).toHierarchy();

        Variable v1 = createVariable("test-hierarchy-var", "test-field");
        Variable v2 = createVariable("test-hierarchy-var2", "test-field");

        service.setVariables(node.getId(), Sets.newHashSet(v1.getId(), v2.getId()));
        service.setVariables(node2.getId(), Collections.singleton(v1.getId()));

        Map<Long, Set<Long>> hierarchyIdsForVariables = service
                .getHierarchyIdsForVariables(Sets.newHashSet(v1.getId(), v2.getId()));
        assertThat(hierarchyIdsForVariables).isNotEmpty();
        assertThat(hierarchyIdsForVariables.size()).isEqualTo(2);

        Set<Long> hierarchyIdsForVar1 = hierarchyIdsForVariables.get(v1.getId());
        assertThat(hierarchyIdsForVar1).isNotEmpty();
        assertThat(hierarchyIdsForVar1.size()).isEqualTo(2);
        assertThat(hierarchyIdsForVar1).containsOnly(node.getId(), node2.getId());

        Set<Long> hierarchyIdsForVar2 = hierarchyIdsForVariables.get(v2.getId());
        assertThat(hierarchyIdsForVar2).isNotEmpty();
        assertThat(hierarchyIdsForVar2.size()).isEqualTo(1);
        assertThat(hierarchyIdsForVar2).containsOnly(node.getId());
    }

    @Test
    public void shouldGetHierarchyIdsForOver1kVariables() {
        Hierarchy node = createHierarchy("A", SYSTEM_SPEC).toHierarchy();
        int numOfVariables = 1010;
        Set<Variable> variables = createVariables("test-hierarchy-var", "test-field", numOfVariables);
        Set<Long> variableIds = variables.stream().map(Variable::getId).collect(toSet());
        service.setVariables(node.getId(), variableIds);

        Map<Long, Set<Long>> hierarchiesByVariableId = service
                .getHierarchyIdsForVariables(variableIds);
        assertThat(hierarchiesByVariableId).isNotEmpty();
        assertThat(hierarchiesByVariableId.size()).isEqualTo(numOfVariables);

        hierarchiesByVariableId.forEach((k, v) -> {
            assertTrue(variableIds.contains(k));
            assertThat(v).isNotEmpty();
            assertThat(v.size()).isEqualTo(1);
            assertThat(v).containsOnly(node.getId());
        });
    }

    //helper methods

    private Entity createEntity(String value) {
        SystemSpec systemSpec = entity.getPartition().getSystem().toSystemSpec();
        return entityService.createEntity(systemSpec.getId(),
                ImmutableMap.of(ENTITY_STRING_SCHEMA_KEY_1, value, ENTITY_DOUBLE_SCHEMA_KEY_1, 1d),
                PARTITION_KEY_VALUES_1).toEntity();
    }

    private Variable createVariable(String name, String field) {
        SystemSpec systemSpec = entity.getPartition().getSystem().toSystemSpec();
        VariableConfig config = VariableConfig.builder().entityId(entity.getId()).fieldName(field).build();
        Variable variableData = Variable.builder().variableName(name).configs(ImmutableSortedSet.of(config))
                .systemSpec(systemSpec).build();
        // we need to have the variable registered
        return variableService.create(variableData).toVariable();
    }

    private Set<Variable> createVariables(String namePrefix, String field, int numOfVariables) {
        SystemSpec systemSpec = entity.getPartition().getSystem().toSystemSpec();
        VariableConfig config = VariableConfig.builder().entityId(entity.getId()).fieldName(field).build();

        Set<Variable> variablesToCreate = new HashSet<>();
        for (int i = 0; i < numOfVariables; i++) {
            Variable variable = Variable.builder().variableName(String.join("_", namePrefix, Integer.toString(i + 1)))
                    .configs(ImmutableSortedSet.of(config)).systemSpec(systemSpec).build();
            variablesToCreate.add(variable);
        }
        return variableService.createAll(variablesToCreate).stream().map(VariableData::toVariable).collect(toSet());
    }

    private Group createSnapshot(String name, Set<Variable> variables) {
        authenticate(MAIN_USER);

        Group snapshot = groupService.create(Group.builder().label("SNAPSHOT").name(name)
                .description("test-desc").systemSpec(SYSTEM_SPEC).visibility(Visibility.PUBLIC).build()).toGroup();
        if (CollectionUtils.isNotEmpty(variables)) {
            Set<Long> variableIds = variables.stream().map(Variable::getId).collect(toSet());
            groupService.setVariables(snapshot.getId(), Collections.singletonMap("other", variableIds));
        }
        return snapshot;
    }

    private void assertEqualsHierarchy(Hierarchy hierarchyA, Hierarchy hierarchyB) {
        assertEquals(hierarchyA.getParent(), hierarchyB.getParent());
        assertEquals(hierarchyA.getNodePath(), hierarchyB.getNodePath());
        assertEquals(hierarchyA.getName(), hierarchyB.getName());
        assertEquals(hierarchyA.getDescription(), hierarchyB.getDescription());
        assertEquals(hierarchyA.getSystemSpec(), hierarchyB.getSystemSpec());
    }

    private Collection<HierarchyData> queryByPathLike(String pathLike) {
        Condition<Hierarchies> query = Hierarchies.suchThat().systemId().eq(SYSTEM_SPEC.getId()).and().path()
                .like(pathLike);
        return service.findAll(toRSQL(query));
    }

    private Set<Hierarchy> queryByParentId(long id) {
        Condition<Hierarchies> query = Hierarchies.suchThat().systemId().eq(SYSTEM_SPEC.getId()).and().parentId().eq(id);
        return service.findAll(toRSQL(query)).stream().map(HierarchyData::toHierarchy).collect(toSet());
    }

    private Hierarchy queryByPath(String path) {
        Condition<Hierarchies> query = Hierarchies.suchThat().systemId().eq(SYSTEM_SPEC.getId()).and().path().eq(path);
        List<HierarchyData> found = service.findAll(toRSQL(query));
        return Iterables.getOnlyElement(found).toHierarchy();
    }

    private void assertPathExists(String path) {
        Condition<Hierarchies> query = Hierarchies.suchThat().systemId().eq(SYSTEM_SPEC.getId()).and().path().eq(path);
        assertEquals(1, service.findAll(toRSQL(query)).size());
    }

    private void assertPathAbsent(String path) {
        Condition<Hierarchies> query = Hierarchies.suchThat().systemId().eq(SYSTEM_SPEC.getId()).and().path().eq(path);
        assertEquals(0, service.findAll(toRSQL(query)).size());
    }

    private HierarchyData createHierarchy(String name, SystemSpec systemSpec) {
        final HierarchyData hierarchyData = service
                .create(Hierarchy.builder().name(name).systemSpec(systemSpec).build());
        return hierarchyData;
    }

    private HierarchyData createHierarchy(String name, Hierarchy hierarchy, SystemSpec systemSpec) {
        return service.create(Hierarchy.builder().name(name).parent(hierarchy).systemSpec(systemSpec).build());
    }
}
