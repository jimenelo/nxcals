package cern.nxcals.service.internal;

import cern.nxcals.api.domain.PartitionResource;
import cern.nxcals.api.domain.PartitionResourceHistory;
import cern.nxcals.api.domain.TimeEntityPartitionType;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.utils.TimeUtils;
import cern.nxcals.service.BaseTest;
import cern.nxcals.service.domain.PartitionResourceData;
import cern.nxcals.service.domain.PartitionResourceHistoryData;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Transactional(transactionManager = "jpaTransactionManager")
@Rollback
class PartitionResourceHistoryServiceTest extends BaseTest {

    @Autowired
    private PartitionResourceHistoryService service;

    private String information = TimeEntityPartitionType.of(10, 10).toString();
    private String compactionType = "Daily";
    private String storageType = "HBase";
    private boolean isFixedSettings = false;
    private Instant startTime = TimeUtils.getInstantFromNanos(TimeUtils.getNanosFromInstant(Instant.now()) - 1000L);
    private Instant endTime = TimeUtils.getInstantFromNanos(TimeUtils.getNanosFromInstant(startTime) + 100L);

    @BeforeEach
    void setUp() {
        setAuthentication();
    }

    @Test
    void shouldCreateNewPartitionResourceHistory() {
        //given
        PartitionResourceData partitionResource = createDefaultPartitionResourceData();
        PartitionResourceHistory pri = createPartitionResourceHistory(partitionResource.toPartitionResource(),
                information, compactionType, storageType, isFixedSettings, startTime, endTime);

        //when
        PartitionResourceHistoryData priData = service.create(pri);

        //then
        assertNotNull(priData);
        assertEquals(priData.getPartitionResource(), partitionResource);
        assertEquals(priData.getInformation(), information);
        assertEquals(priData.getCompactionType(), compactionType);
        assertEquals(priData.getStorageType(), storageType);
        assertEquals(priData.isFixedSettings(), isFixedSettings);
        assertEquals(priData.getValidFromStamp(), startTime);
        assertEquals(priData.getValidToStamp(), endTime);
    }

    @Test
    void shouldUpdatePartitionResourceHistory() {
        //given
        PartitionResourceData partitionResource = createDefaultPartitionResourceData();
        PartitionResourceHistory pri = createPartitionResourceHistory(partitionResource.toPartitionResource(),
                information, compactionType, storageType, isFixedSettings, startTime, endTime);
        PartitionResourceHistoryData priData = service.create(pri);
        String newInformation = "New Information";
        String newCompactionType = "New Compaction Type";
        String newStorageType = "New Storage Type";

        //when
        entityManager.flush();
        priData.setInformation(newInformation);
        priData.setCompactionType(newCompactionType);
        priData.setStorageType(newStorageType);
        PartitionResourceHistoryData updatedPartitionResourceHistory = service
                .update(priData.toPartitionResourceHistory());

        //then
        assertNotNull(updatedPartitionResourceHistory);
        assertEquals(newInformation, updatedPartitionResourceHistory.getInformation());
        assertEquals(newCompactionType, updatedPartitionResourceHistory.getCompactionType());
        assertEquals(newStorageType, updatedPartitionResourceHistory.getStorageType());
        assertEquals(partitionResource.getId(), updatedPartitionResourceHistory.getPartitionResource().getId());
        assertEquals(startTime, updatedPartitionResourceHistory.getValidFromStamp());
        assertEquals(endTime, updatedPartitionResourceHistory.getValidToStamp());
    }

    @Test
    void shouldUpdateMultipleTimesPartitionResourceHistory() {
        PartitionResourceData partitionResource = createDefaultPartitionResourceData();
        PartitionResourceHistory pri = createPartitionResourceHistory(partitionResource.toPartitionResource(),
                information, compactionType, storageType, isFixedSettings, startTime, endTime);
        PartitionResourceHistoryData priData = service.create(pri);
        String newInformation = "New Information";
        String newCompactionType = "New Compaction Type";
        String newStorageType = "New Storage Type";

        entityManager.flush();
        priData.setInformation(newInformation);
        priData.setCompactionType(newCompactionType);
        priData.setStorageType(newStorageType);
        PartitionResourceHistoryData updatedPartitionResourceHistory = service
                .update(priData.toPartitionResourceHistory());

        assertNotNull(updatedPartitionResourceHistory);
        assertEquals(newInformation, updatedPartitionResourceHistory.getInformation());
        assertEquals(newCompactionType, updatedPartitionResourceHistory.getCompactionType());
        assertEquals(newStorageType, updatedPartitionResourceHistory.getStorageType());
        assertEquals(partitionResource.getId(), updatedPartitionResourceHistory.getPartitionResource().getId());
        assertEquals(startTime, updatedPartitionResourceHistory.getValidFromStamp());
        assertEquals(endTime, updatedPartitionResourceHistory.getValidToStamp());

        updatedPartitionResourceHistory.setInformation(information);
        updatedPartitionResourceHistory.setCompactionType(compactionType);
        updatedPartitionResourceHistory.setStorageType(storageType);
        PartitionResourceHistoryData reupdatedPartitionResourceHistory = service
                .update(updatedPartitionResourceHistory.toPartitionResourceHistory());

        assertNotNull(reupdatedPartitionResourceHistory);
        assertEquals(information, reupdatedPartitionResourceHistory.getInformation());
        assertEquals(compactionType, reupdatedPartitionResourceHistory.getCompactionType());
        assertEquals(storageType, reupdatedPartitionResourceHistory.getStorageType());
        assertEquals(partitionResource.getId(), reupdatedPartitionResourceHistory.getPartitionResource().getId());
        assertEquals(startTime, reupdatedPartitionResourceHistory.getValidFromStamp());
        assertEquals(endTime, reupdatedPartitionResourceHistory.getValidToStamp());
    }

    @Test
    void shouldFindOnlyExistentPartitionResourceHistory() {
        //given
        PartitionResourceData partitionResource = createDefaultPartitionResourceData();
        PartitionResourceHistory pri = createPartitionResourceHistory(partitionResource.toPartitionResource(),
                information, compactionType, storageType, isFixedSettings, startTime, endTime);
        PartitionResourceHistoryData priData = service.create(pri);

        //when
        Optional<PartitionResourceHistoryData> partitionResourceInformationFound = service.findById(priData.getId());
        Optional<PartitionResourceHistoryData> partitionResourceInformationNotFound = service.findById(-1L);

        //then
        assertTrue(partitionResourceInformationFound.isPresent());
        assertFalse(partitionResourceInformationNotFound.isPresent());
    }

    @Test
    void shouldFindByPartitionResourceId() {
        //given
        PartitionResourceData partitionResource = createDefaultPartitionResourceData();
        PartitionResourceHistory pri = createPartitionResourceHistory(partitionResource.toPartitionResource(),
                information, compactionType, storageType, isFixedSettings, startTime, endTime);
        PartitionResourceHistoryData priData = service.create(pri);

        //when
        List<PartitionResourceHistoryData> data = service
                .findByPartitionResourceId(partitionResource.getId());

        //then
        assertNotNull(data);
        assertEquals(1, data.size());
        assertEquals(data.get(0).getPartitionResource().getId(), partitionResource.getId());
    }

    @Test
    void shouldFindByStorageType() {
        //given
        PartitionResourceData partitionResource = createDefaultPartitionResourceData();
        PartitionResourceHistory pri = createPartitionResourceHistory(partitionResource.toPartitionResource(),
                information, compactionType, storageType, isFixedSettings, startTime, endTime);
        PartitionResourceHistoryData priData = service.create(pri);

        //when
        List<PartitionResourceHistoryData> partitionResourceInformationsWithSameStorageType = service.findByStorageType(storageType);

        //then
        assertTrue(partitionResourceInformationsWithSameStorageType.size() > 0);
    }

    @Test
    void shouldDeletePartitionResourceHistory() {
        //given
        PartitionResourceData partitionResource = createDefaultPartitionResourceData();
        PartitionResourceHistory pri = createPartitionResourceHistory(partitionResource.toPartitionResource(),
                information, compactionType, storageType, isFixedSettings, startTime, endTime);
        PartitionResourceHistoryData priData = service.create(pri);
        long partitionResourceInformationId = priData.getId();
        Optional<PartitionResourceHistoryData> justCreatedPartitionResourceHistory = service
                .findById(partitionResourceInformationId);
        assertTrue(justCreatedPartitionResourceHistory.isPresent());

        //when
        service.delete(partitionResourceInformationId);
        Optional<PartitionResourceHistoryData> deletedPartitionResourceHistory = service
                .findById(partitionResourceInformationId);

        //then
        assertFalse(deletedPartitionResourceHistory.isPresent());
    }

    @Test
    void shouldFindByPartitionResourceAndTimestamp() {
        //given
        PartitionResourceData partitionResource = createDefaultPartitionResourceData();
        PartitionResourceHistory pri = createPartitionResourceHistory(partitionResource.toPartitionResource(),
                information, compactionType, storageType, isFixedSettings, startTime, endTime);
        PartitionResourceHistoryData priData = service.create(pri);

        //when
        PartitionResourceHistoryData info = service.findByPartitionResourceAndTimestamp(partitionResource, startTime.plus(1, ChronoUnit.NANOS)).get();

        //then
        assertEquals(priData, info);

    }

    @Test
    void shouldFindNext() {
        //given
        PartitionResourceData partitionResource = createDefaultPartitionResourceData();
        PartitionResourceHistory pri = createPartitionResourceHistory(partitionResource.toPartitionResource(),
                information, compactionType, storageType, isFixedSettings, startTime, endTime);
        PartitionResourceHistory pri2 = createPartitionResourceHistory(partitionResource.toPartitionResource(),
                information, compactionType, storageType, isFixedSettings, endTime,
                endTime.plus(100, ChronoUnit.SECONDS));
        PartitionResourceHistoryData priData = service.create(pri);
        PartitionResourceHistoryData priData2 = service.create(pri2);

        //when
        PartitionResourceHistoryData info = service.findNextFor(pri).get();

        //then
        assertEquals(priData2, info);

    }

    @Test
    void shouldFindPrevious() {
        //given
        PartitionResourceData partitionResource = createDefaultPartitionResourceData();
        PartitionResourceHistory pri = createPartitionResourceHistory(partitionResource.toPartitionResource(),
                information, compactionType, storageType, isFixedSettings, startTime, endTime);
        PartitionResourceHistory pri2 = createPartitionResourceHistory(partitionResource.toPartitionResource(),
                information, compactionType, storageType, isFixedSettings, endTime,
                endTime.plus(100, ChronoUnit.SECONDS));
        PartitionResourceHistoryData priData = service.create(pri);
        PartitionResourceHistoryData priData2 = service.create(pri2);

        //when
        PartitionResourceHistoryData info = service.findPreviousFor(pri2).get();

        //then
        assertEquals(priData, info);

    }

    @Test
    void shouldFindAdjecent() {
        //given
        PartitionResourceData partitionResource = createDefaultPartitionResourceData();
        PartitionResourceHistory pri = createPartitionResourceHistory(partitionResource.toPartitionResource(),
                information, compactionType, storageType, isFixedSettings, startTime, endTime);
        PartitionResourceHistory pri2 = createPartitionResourceHistory(partitionResource.toPartitionResource(),
                information, compactionType, storageType, isFixedSettings, endTime,
                endTime.plus(100, ChronoUnit.SECONDS));
        PartitionResourceHistoryData priData = service.create(pri);
        PartitionResourceHistoryData priData2 = service.create(pri2);

        //when
        PartitionResourceHistoryData adj1 = service.findAdjacentFor(pri).get();
        PartitionResourceHistoryData adj2 = service.findAdjacentFor(pri2).get();

        //then
        assertEquals(priData2, adj1);
        assertEquals(priData, adj2);
    }

    @Test
    void shouldFindOrCreatePartitionResourceInfoForWithPreviousAdjacent() {
        //given
        PartitionResourceData partitionResource = createDefaultPartitionResourceData();
        PartitionResourceHistory pri = createPartitionResourceHistory(partitionResource.toPartitionResource(),
                information, compactionType, storageType, isFixedSettings, startTime, endTime);
        PartitionResourceHistory pri2 = createPartitionResourceHistory(partitionResource.toPartitionResource(),
                information, compactionType, storageType, isFixedSettings, endTime,
                endTime.plus(100, ChronoUnit.SECONDS));
        PartitionResourceHistoryData priData = service.create(pri);
        PartitionResourceHistoryData priData2 = service.create(pri2);

        PartitionResourceHistory newPri = pri2.toBuilder()
                .validity(TimeWindow.between(endTime.plus(100, ChronoUnit.SECONDS), endTime.plus(200, ChronoUnit.SECONDS)))
                .partitionInformation(TimeEntityPartitionType.of(11, 10).toString())
                .build();
        //when
        PartitionResourceHistoryData found = service.findOrCreatePartitionResourceInfoFor(partitionResource, newPri);


        //then
        TimeEntityPartitionType type = TimeEntityPartitionType.from(found.getInformation());
        assertEquals(10, type.getTimePartitionCount()); //should be changed to 10, the one from the previous
        assertEquals(endTime.plus(100, ChronoUnit.SECONDS), found.getValidFromStamp());
        assertEquals(endTime.plus(200, ChronoUnit.SECONDS), found.getValidToStamp());

    }

    @Test
    void shouldFindOrCreatePartitionResourceInfoForWithNew() {
        //given
        PartitionResourceData partitionResource = createDefaultPartitionResourceData();
        PartitionResourceHistory pri = createPartitionResourceHistory(partitionResource.toPartitionResource(),
                information, compactionType, storageType, isFixedSettings, startTime, endTime);
        PartitionResourceHistory pri2 = createPartitionResourceHistory(partitionResource.toPartitionResource(),
                information, compactionType, storageType, isFixedSettings, endTime,
                endTime.plus(100, ChronoUnit.SECONDS));
        PartitionResourceHistoryData priData = service.create(pri);
        PartitionResourceHistoryData priData2 = service.create(pri2);

        PartitionResourceHistory newPri = pri2.toBuilder()
                .validity(TimeWindow.between(endTime.plus(100, ChronoUnit.SECONDS), endTime.plus(200, ChronoUnit.SECONDS)))
                .partitionInformation(TimeEntityPartitionType.of(100, 10).toString())
                .build();
        //when
        PartitionResourceHistoryData found = service.findOrCreatePartitionResourceInfoFor(partitionResource, newPri);


        //then
        TimeEntityPartitionType type = TimeEntityPartitionType.from(found.getInformation());
        assertEquals(100, type.getTimePartitionCount()); //new value should be used, change is too big (>33%)
        assertEquals(endTime.plus(100, ChronoUnit.SECONDS), found.getValidFromStamp());
        assertEquals(endTime.plus(200, ChronoUnit.SECONDS), found.getValidToStamp());

    }

    @Test
    void shouldCreatePartitionResourceHistory() {
        //given
        PartitionResourceData partitionResource = createDefaultPartitionResourceData();
        TimeEntityPartitionType type = TimeEntityPartitionType.of(10, 10);

        //when
        PartitionResourceHistory partitionResourceHistory = service
                .createPartitionResourceHistory(partitionResource, startTime, type);

        //then
        assertEquals(type.toString(), partitionResourceHistory.getPartitionInformation());
        assertEquals(startTime, partitionResourceHistory.getValidity().getStartTime());
        assertEquals(startTime.plus(1, ChronoUnit.DAYS), partitionResourceHistory.getValidity().getEndTime());
        assertEquals(partitionResource.toPartitionResource(), partitionResourceHistory.getPartitionResource());
        assertEquals(partitionResource.toPartitionResource(), partitionResourceHistory.getPartitionResource());

    }


    @Test
    void shouldFailIfPartitionResourceIsNotSet() {
        assertThrows(NullPointerException.class, () -> createPartitionResourceHistory(null,
                information, compactionType, storageType, isFixedSettings, startTime, endTime));
    }

    @Test
    void shouldFailIfPartitionInformationIsNotSet() {
        PartitionResource partitionResource = createDefaultPartitionResourceData().toPartitionResource();
        assertThrows(NullPointerException.class, () -> createPartitionResourceHistory(partitionResource,
                null, compactionType, storageType, isFixedSettings, startTime, endTime));
    }

    @Test
    void shouldFailIfCompactionTypeIsNotSet() {
        PartitionResource partitionResource = createDefaultPartitionResourceData().toPartitionResource();
        assertThrows(NullPointerException.class, () -> createPartitionResourceHistory(partitionResource,
                information, null, storageType, isFixedSettings, startTime, endTime));
    }

    @Test
    void shouldFailIfStorageTypeIsNotSet() {
        PartitionResource partitionResource = createDefaultPartitionResourceData().toPartitionResource();
        assertThrows(NullPointerException.class, () -> createPartitionResourceHistory(partitionResource,
                information, compactionType, null, isFixedSettings, startTime, endTime));
    }

    @Test
    void shouldFailIfStartTimeIsNotSet() {
        PartitionResource partitionResource = createDefaultPartitionResourceData().toPartitionResource();
        assertThrows(NullPointerException.class, () -> createPartitionResourceHistory(partitionResource,
                information, compactionType, storageType, isFixedSettings, null, endTime));
    }

    @Test
    void shouldFailIfEndTimeIsNotSet() {
        PartitionResource partitionResource = createDefaultPartitionResourceData().toPartitionResource();
        assertThrows(NullPointerException.class, () -> createPartitionResourceHistory(partitionResource,
                information, compactionType, storageType, isFixedSettings, startTime, null));
    }

}
