package cern.nxcals.service.internal.extraction;

import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.service.internal.extraction.ExtractionService.SelectionCriteria;
import org.apache.avro.Schema;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

import static cern.nxcals.common.Constants.HBASE_ROW_KEY_DELIMITER;
import static cern.nxcals.common.Schemas.TIMESTAMP;
import static cern.nxcals.service.internal.extraction.DefaultPredicateProvider.AND;
import static cern.nxcals.service.internal.extraction.DefaultPredicateProvider.ENTITY_ID;
import static cern.nxcals.service.internal.extraction.DefaultPredicateProvider.HBASE_KEY_ID_NAME;
import static cern.nxcals.service.internal.extraction.DefaultPredicateProvider.HDFS_TIME_PREDICATE;
import static cern.nxcals.service.internal.extraction.DefaultPredicateProvider.IN;
import static cern.nxcals.service.internal.extraction.DefaultPredicateProvider.STRING_EQUALS_AND;
import static java.util.stream.Collectors.toMap;
import static java.util.stream.Collectors.toSet;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class DefaultPredicateProviderTest {
    private static final String SYSTEM_NAME = "dummy";
    private static final String PREDICATE = "(x > 1)";
    private static final String LONG_PREDICATE = "(x == 1 and x < 1 and x > 1 and x != 2 or x != 3 or x != 100)";
    private DefaultPredicateProvider instance;

    @BeforeEach
    void setUp() {
        Function<String, Schema.Field> timestampFieldProvider = mock(TimestampProvider.class);
        when(timestampFieldProvider.apply(SYSTEM_NAME)).thenReturn(
                new Schema.Field(TIMESTAMP.getFieldName(), TIMESTAMP.getSchema(), null, (Object) null));
        this.instance = new DefaultPredicateProvider(timestampFieldProvider, -1);
    }

    @Test
    void shouldGetSingleHBasePredicate() {
        SelectionCriteria criteria = getSelectionCriteria(10);
        List<String> predicates = this.instance.getHBasePredicate(criteria);
        assertEquals(1, predicates.size());
    }

    @Test
    void shouldGetMultipleHBasePredicate() {
        SelectionCriteria criteria = getSelectionCriteria(150);
        List<String> predicates = this.instance.getHBasePredicate(criteria);
        assertEquals(2, predicates.size());
    }

    @Test
    void shouldGetMultipleHBasePredicateWithVariablePredicates() {
        Set<Long> ids = LongStream.rangeClosed(1, 150).boxed().collect(toSet());
        Map<Long, Optional<String>> variablePredicates = ids.stream()
                .collect(Collectors.toMap(x -> x, x -> Optional.of(LONG_PREDICATE)));
        SelectionCriteria criteria = getSelectionCriteria(variablePredicates);
        List<String> predicates = this.instance.getHBasePredicate(criteria);
        assertEquals(3, predicates.size());
    }

    @Test
    void shouldGetMultipleHBasePredicateWithSalting() {
        int saltBuckets = 10;
        Function<String, Schema.Field> timestampFieldProvider = mock(TimestampProvider.class);
        when(timestampFieldProvider.apply(SYSTEM_NAME)).thenReturn(
                new Schema.Field(TIMESTAMP.getFieldName(), TIMESTAMP.getSchema(), null, (Object) null));
        DefaultPredicateProvider predicateProvider = new DefaultPredicateProvider(timestampFieldProvider, saltBuckets);

        SelectionCriteria criteria = getSelectionCriteria(50);
        List<String> predicates = predicateProvider.getHBasePredicate(criteria);
        assertEquals(5, predicates.size());
    }

    @Test
    void shouldGetHdfsPredicate() {
        SelectionCriteria criteria = getSelectionCriteria(10);
        List<String> predicates = this.instance.getHdfsPredicate(criteria);
        assertEquals(1, predicates.size());

        TimeWindow tw = criteria.getTimeWindow();
        String timePredicate = String.format(HDFS_TIME_PREDICATE, TIMESTAMP.getFieldName(), tw.getStartTimeNanos(),
                TIMESTAMP.getFieldName(), tw.getEndTimeNanos());
        String idsPredicate = String.format(IN, ENTITY_ID, StringUtils.join(criteria.getEntityIds(), ","));
        String expectedPredicate = String.format(AND, timePredicate, idsPredicate);

        assertEquals(expectedPredicate, predicates.get(0));
    }

    @Test
    void shouldNotIncludeEmptyEntityIdInPartIfEmptyInHdfsPredicate() {
        SelectionCriteria criteria = getSelectionCriteria(Map.of(1L, Optional.of(PREDICATE)));
        List<String> predicates = this.instance.getHdfsPredicate(criteria);
        assertEquals(1, predicates.size());

        TimeWindow tw = criteria.getTimeWindow();
        String timePredicate = String.format(HDFS_TIME_PREDICATE, TIMESTAMP.getFieldName(), tw.getStartTimeNanos(),
                TIMESTAMP.getFieldName(), tw.getEndTimeNanos());
        String idsPredicate = String.format(STRING_EQUALS_AND, ENTITY_ID, 1L, PREDICATE);
        String expectedPredicate = String.format(AND, timePredicate, idsPredicate);

        assertEquals(expectedPredicate, predicates.get(0));
    }

    @Test
    void shouldGetHdfsPredicateWithVariablePredicates() {
        SelectionCriteria criteria = getSelectionCriteria(
                Map.of(0L, Optional.of(""), 1L, Optional.of(PREDICATE), 2L, Optional.empty()));
        List<String> predicates = this.instance.getHdfsPredicate(criteria);
        assertEquals(1, predicates.size());

        TimeWindow tw = criteria.getTimeWindow();
        String timePredicate = String.format(HDFS_TIME_PREDICATE, TIMESTAMP.getFieldName(), tw.getStartTimeNanos(),
                TIMESTAMP.getFieldName(), tw.getEndTimeNanos());
        String idsPredicate = String.join(" or ",
                String.format(STRING_EQUALS_AND, ENTITY_ID, 1L, PREDICATE),
                String.format(IN, ENTITY_ID, "0,2"));
        String expectedPredicate = String.format(AND, timePredicate, idsPredicate);

        assertEquals(expectedPredicate, predicates.get(0));
    }

    @Test
    void shouldThrowOnInvalidSaltBucketsNumber() {
        Function<String, Schema.Field> timestampFieldProvider = mock(TimestampProvider.class);
        when(timestampFieldProvider.apply(SYSTEM_NAME)).thenReturn(
                new Schema.Field(TIMESTAMP.getFieldName(), TIMESTAMP.getSchema(), null, (Object) null));
        assertThrows(IllegalArgumentException.class, () -> new DefaultPredicateProvider(timestampFieldProvider, 0));
    }

    @Test
    void shouldGetHBasePredicateForSingleEntity() {
        //given
        int saltBuckets = 5;
        int start = 2;
        int end = 3;
        Function<String, Schema.Field> timestampFieldProvider = mock(TimestampProvider.class);
        when(timestampFieldProvider.apply(SYSTEM_NAME)).thenReturn(
                new Schema.Field(TIMESTAMP.getFieldName(), TIMESTAMP.getSchema(), null, (Object) null));
        DefaultPredicateProvider predicateProvider = new DefaultPredicateProvider(timestampFieldProvider, saltBuckets);
        TimeWindow window = TimeWindow.between(start, end);
        SystemSpec spec = mock(SystemSpec.class);
        Map<Long, Optional<String>> variablePredicates = Map.of(1L, Optional.of(PREDICATE));
        when(spec.getName()).thenReturn(SYSTEM_NAME);
        SelectionCriteria criteria = new SelectionCriteria(spec, window, variablePredicates);
        //when
        List<String> predicates = predicateProvider.getHBasePredicate(criteria);
        //then
        assertEquals(1, predicates.size());
        String[] splitPredicates = predicates.get(0).split(" or ");
        assertEquals(saltBuckets, splitPredicates.length);

        // the start predicate should start on plus one nanos (+1) from the provided timestamp,
        // in order to avoid record intersects with the HDFS query
        String startKey = "4" + HBASE_ROW_KEY_DELIMITER + "1" + HBASE_ROW_KEY_DELIMITER + (Long.MAX_VALUE - 2 + 1);
        String endKey = "4" + HBASE_ROW_KEY_DELIMITER + "1" + HBASE_ROW_KEY_DELIMITER + (Long.MAX_VALUE - 3);
        String expected = String.format("(%s >= \"%s\" and %s < \"%s\")) and (%s)", HBASE_KEY_ID_NAME, endKey,
                HBASE_KEY_ID_NAME, startKey, PREDICATE);
        assertEquals(expected, splitPredicates[4]);
    }

    private SelectionCriteria getSelectionCriteria(int idsCount) {
        Set<Long> ids = LongStream.rangeClosed(1, idsCount).boxed().collect(toSet());
        return getSelectionCriteria(ids.stream().collect(toMap(x -> x, x -> Optional.empty())));
    }

    private SelectionCriteria getSelectionCriteria(Map<Long, Optional<String>> predicates) {
        TimeWindow window = TimeWindow.between(0, 1);
        SystemSpec spec = mock(SystemSpec.class);
        when(spec.getName()).thenReturn(SYSTEM_NAME);
        return new SelectionCriteria(spec, window, predicates);
    }

    private interface TimestampProvider extends Function<String, Schema.Field> {
    }

}
