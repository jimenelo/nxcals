package cern.nxcals.service.internal;

import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.Group;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.domain.VariableConfig;
import cern.nxcals.api.extraction.metadata.queries.Groups;
import cern.nxcals.service.BaseTest;
import cern.nxcals.service.domain.EntityData;
import cern.nxcals.service.domain.GroupData;
import cern.nxcals.service.domain.SystemSpecData;
import cern.nxcals.service.domain.VariableData;
import cern.nxcals.service.domain.VisibilityData;
import cern.nxcals.service.rest.exceptions.AccessDeniedRuntimeException;
import cern.nxcals.service.rest.exceptions.NotFoundRuntimeException;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSortedSet;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static cern.nxcals.common.utils.RSQLUtils.toRSQL;
import static cern.nxcals.service.internal.GroupServiceTest.MAIN_USER;
import static cern.nxcals.service.internal.GroupServiceTest.OTHER_USER;
import static cern.nxcals.service.rest.DomainTestConstants.PARTITION_KEY_VALUES_1;
import static cern.nxcals.service.rest.TestSchemas.ENTITY_DOUBLE_SCHEMA_KEY_1;
import static cern.nxcals.service.rest.TestSchemas.ENTITY_STRING_SCHEMA_KEY_1;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Transactional(transactionManager = "jpaTransactionManager")
@Rollback
public class SecureGroupServiceTest extends BaseTest {

    @Autowired
    private SecureGroupService secureGroupService;

    @Autowired
    private GroupService rawGroupService;

    @Autowired
    private EntityService entityService;

    @Autowired
    private VariableService variableService;

    private SystemSpec system;

    @BeforeEach
    public void init() {
        // it is needed for persisting user and otherUser
        setAuthentication();
        system = createAndPersistSystem("my-system");
    }

    @Test
    public void shouldNotSeeProtectedGroups() {
        authenticate(MAIN_USER);
        Group group = protectedGroup("my-group", system);
        secureGroupService.create(group).toGroup();

        authenticate(MAIN_USER);
        assertTrue(exists("my-group"));

        authenticate(OTHER_USER);
        assertFalse(exists("my-group"));
    }

    @Test
    public void shouldNotSeePrivateGroups() {
        authenticate(MAIN_USER);

        SystemSpecData systemSpecData = systemRepository.findById(system.getId()).orElse(null);

        GroupData groupA = groupDataWithUser("my-public-group", systemSpecData, MAIN_USER);
        groupA.setVisibility(VisibilityData.PUBLIC);
        GroupData groupB = groupDataWithUser("my-protected-group", systemSpecData, MAIN_USER);
        groupB.setVisibility(VisibilityData.PROTECTED);
        GroupData groupC = groupDataWithUser("my-private-group", systemSpecData, MAIN_USER);
        groupC.setVisibility(VisibilityData.PRIVATE);

        persistGroup(groupA);
        persistGroup(groupB);
        persistGroup(groupC);

        authenticate(MAIN_USER);
        assertTrue(exists("my-public-group"));
        assertTrue(exists("my-protected-group"));
        assertFalse(exists("my-private-group"));

        authenticate(OTHER_USER);
        assertTrue(exists("my-public-group"));
        assertFalse(exists("my-protected-group"));
        assertFalse(exists("my-private-group"));
    }

    @Test
    public void shouldNotUpdateProtectedGroupsIfNotOwner() {
        authenticate(MAIN_USER);

        Group groupOld = protectedGroup("my-group", system);
        Group groupNew = secureGroupService.create(groupOld).toGroup();

        authenticate(OTHER_USER);
        assertThrows(NotFoundRuntimeException.class,
                () -> secureGroupService.update(groupNew.toBuilder().description("new_desc").build()));
    }

    @Test
    public void shouldNotUpdatePublicGroupsIfNotOwner() {
        authenticate(MAIN_USER);

        Group groupOld = publicGroup("my-group", system);
        Group groupNew = secureGroupService.create(groupOld).toGroup();

        authenticate(OTHER_USER);
        assertThrows(AccessDeniedRuntimeException.class,
                () -> secureGroupService.update(groupNew.toBuilder().description("new_desc").build()));
    }

    @Test
    public void shouldNotEditNonOwnedGroups() {
        authenticate(MAIN_USER);

        Group groupOld = publicGroup("my-group", system);
        Group groupNewA = secureGroupService.create(groupOld).toGroup();

        assertEquals(MAIN_USER, groupNewA.getOwner());
        assertTrue(groupNewA.isVisible());

        authenticate(OTHER_USER);

        assertThrows(AccessDeniedRuntimeException.class, () -> secureGroupService.update(groupNewA).toGroup());
    }

    // test: delete

    @Test
    public void shouldNotDeleteGroupIfItDoesNotExist() {
        authenticate(MAIN_USER);
        long nonExistingGroupId = 1234L;
        assertThrows(NotFoundRuntimeException.class, () -> secureGroupService.delete(nonExistingGroupId));
    }

    @Test
    public void shouldNotDeletePublicGroupIfNotOwner() {
        authenticate(MAIN_USER);

        Group group = secureGroupService.create(publicGroup("my-group", system)).toGroup();

        authenticate(OTHER_USER);
        assertThrows(AccessDeniedRuntimeException.class, () -> secureGroupService.delete(group.getId()));
    }

    @Test
    public void shouldNotDeleteProtectedGroupIfNotOwner() {
        authenticate(MAIN_USER);

        GroupData group = secureGroupService.create(protectedGroup("my-group", system));

        authenticate(OTHER_USER);
        assertThrows(NotFoundRuntimeException.class, () -> secureGroupService.delete(group.getId()));
    }

    @Test
    public void shouldDeleteProtectedGroupIfOwner() {
        authenticate(MAIN_USER);

        Group group = secureGroupService.create(protectedGroup("my-group", system)).toGroup();

        secureGroupService.delete(group.getId());
    }

    @Test
    public void shouldDeletePublicGroupIfOwner() {
        authenticate(MAIN_USER);

        Group groupNew = secureGroupService.create(publicGroup("my-group", system)).toGroup();

        secureGroupService.delete(groupNew.getId());
    }

    // test: setVariables

    @Test
    public void shouldNotAllowSetVariablesOnPrivateGroup() {
        authenticate(MAIN_USER);

        GroupData privateDataGroup = createPrivateGroup("my-super-private-group", system);
        assertNotNull(privateDataGroup.getId());

        Map<String, Set<Long>> associations = new HashMap<>();
        Variable variable = createVariable(system, createEntity(system, "entity1"), "my-variable", "my-field");
        associations.put("private-association", Collections.singleton(variable.getId()));

        assertThrows(NotFoundRuntimeException.class,
                () -> secureGroupService.setVariables(privateDataGroup.getId(), associations));
    }

    @Test
    public void shouldNotAllowModifyToSetVariablesOnGroupOfOtherUser() {
        authenticate(MAIN_USER);

        Group group = secureGroupService.create(publicGroup("my-group", system)).toGroup();

        assertEquals(MAIN_USER, group.getOwner());
        assertTrue(group.isVisible());

        Map<String, Set<Long>> associations = new HashMap<>();
        Variable variable = createVariable(system, createEntity(system, "entity1"), "my-variable", "my-field");
        associations.put("association", Collections.singleton(variable.getId()));

        authenticate(OTHER_USER);
        assertThrows(AccessDeniedRuntimeException.class,
                () -> secureGroupService.setVariables(group.getId(), associations));
    }

    @Test
    public void shouldNotAllowModifyToSetVariablesOnProtectedGroupOfOtherUser() {
        authenticate(MAIN_USER);

        Group group = secureGroupService.create(protectedGroup("my-group", system)).toGroup();

        assertEquals(MAIN_USER, group.getOwner());
        assertFalse(group.isVisible());

        Map<String, Set<Long>> associations = new HashMap<>();
        Variable variable = createVariable(system, createEntity(system, "entity1"), "my-variable", "my-field");
        associations.put("association", Collections.singleton(variable.getId()));

        authenticate(OTHER_USER);
        assertThrows(NotFoundRuntimeException.class,
                () -> secureGroupService.setVariables(group.getId(), associations));
    }

    @Test
    public void shouldAllowModifyToSetVariables() {
        authenticate(MAIN_USER);

        Group groupPublic = secureGroupService.create(publicGroup("my-group-public", system)).toGroup();
        assertEquals(MAIN_USER, groupPublic.getOwner());
        assertTrue(groupPublic.isVisible());

        Group groupProtected = secureGroupService.create(protectedGroup("my-group-protected", system)).toGroup();
        assertEquals(MAIN_USER, groupProtected.getOwner());
        assertFalse(groupProtected.isVisible());

        Map<String, Set<Long>> associations = new HashMap<>();
        Variable variable = createVariable(system, createEntity(system, "entity1"), "my-variable", "my-field");
        associations.put("association", Collections.singleton(variable.getId()));

        secureGroupService.setVariables(groupPublic.getId(), associations);
        secureGroupService.setVariables(groupProtected.getId(), associations);

        Map<String, Set<VariableData>> variablesPublic = secureGroupService.getVariables(groupPublic.getId());
        assertEquals(1, variablesPublic.size());
        assertEquals(1, variablesPublic.get("association").size());
        assertEquals(variable, variablesPublic.get("association").iterator().next().toVariable());

        Map<String, Set<VariableData>> variablesProtected = secureGroupService.getVariables(groupProtected.getId());
        assertEquals(1, variablesProtected.size());
        assertEquals(1, variablesProtected.get("association").size());
        assertEquals(variable, variablesProtected.get("association").iterator().next().toVariable());
    }

    // test: addVariables

    @Test
    public void shouldNotAllowAddVariablesOnPrivateGroup() {
        authenticate(MAIN_USER);

        GroupData privateDataGroup = createPrivateGroup("my-super-private-group", system);
        assertNotNull(privateDataGroup.getId());

        Map<String, Set<Long>> associations = new HashMap<>();
        Variable variable = createVariable(system, createEntity(system, "entity1"), "my-variable", "my-field");
        associations.put("private-association", Collections.singleton(variable.getId()));

        assertThrows(NotFoundRuntimeException.class,
                () -> secureGroupService.addVariables(privateDataGroup.getId(), associations));
    }

    @Test
    public void shouldNotAllowModifyToAddVariablesOnGroupOfOtherUser() {
        authenticate(MAIN_USER);

        Group group = secureGroupService.create(publicGroup("my-group", system)).toGroup();

        assertEquals(MAIN_USER, group.getOwner());
        assertTrue(group.isVisible());

        Map<String, Set<Long>> associations = new HashMap<>();
        Variable variable = createVariable(system, createEntity(system, "entity1"), "my-variable", "my-field");
        associations.put("association", Collections.singleton(variable.getId()));

        authenticate(OTHER_USER);
        assertThrows(AccessDeniedRuntimeException.class,
                () -> secureGroupService.addVariables(group.getId(), associations));
    }

    @Test
    public void shouldNotAllowModifyToAddVariablesOnProtectedGroupOfOtherUser() {
        authenticate(MAIN_USER);

        Group group = secureGroupService.create(protectedGroup("my-group", system)).toGroup();

        assertEquals(MAIN_USER, group.getOwner());
        assertFalse(group.isVisible());

        Map<String, Set<Long>> associations = new HashMap<>();
        Variable variable = createVariable(system, createEntity(system, "entity1"), "my-variable", "my-field");
        associations.put("association", Collections.singleton(variable.getId()));

        authenticate(OTHER_USER);
        assertThrows(NotFoundRuntimeException.class,
                () -> secureGroupService.addVariables(group.getId(), associations));
    }

    @Test
    public void shouldAllowModifyToAddVariables() {
        authenticate(MAIN_USER);

        Group groupPublic = secureGroupService.create(publicGroup("my-group-public", system)).toGroup();
        assertEquals(MAIN_USER, groupPublic.getOwner());
        assertTrue(groupPublic.isVisible());

        Group groupProtected = secureGroupService.create(protectedGroup("my-group-protected", system)).toGroup();
        assertEquals(MAIN_USER, groupProtected.getOwner());
        assertFalse(groupProtected.isVisible());

        Map<String, Set<Long>> associations = new HashMap<>();
        Variable variable = createVariable(system, createEntity(system, "entity1"), "my-variable", "my-field");
        associations.put("association", Collections.singleton(variable.getId()));

        secureGroupService.addVariables(groupPublic.getId(), associations);
        secureGroupService.addVariables(groupProtected.getId(), associations);

        Map<String, Set<VariableData>> variablesPublic = secureGroupService.getVariables(groupPublic.getId());
        assertEquals(1, variablesPublic.size());
        assertEquals(1, variablesPublic.get("association").size());
        assertEquals(variable, variablesPublic.get("association").iterator().next().toVariable());

        Map<String, Set<VariableData>> variablesProtected = secureGroupService.getVariables(groupProtected.getId());
        assertEquals(1, variablesProtected.size());
        assertEquals(1, variablesProtected.get("association").size());
        assertEquals(variable, variablesProtected.get("association").iterator().next().toVariable());
    }

    // test: getVariables

    @Test
    public void shouldNotAllowGetVariablesOfPrivateGroup() {
        authenticate(MAIN_USER);

        GroupData privateDataGroup = createPrivateGroup("my-super-private-group", system);
        assertNotNull(privateDataGroup.getId());

        Map<String, Set<Long>> associations = new HashMap<>();
        Variable variable = createVariable(system, createEntity(system, "entity1"), "my-variable", "my-field");
        associations.put("private-association", Collections.singleton(variable.getId()));
        rawGroupService.setVariables(privateDataGroup.getId(),
                associations); // create associations directly on the group
        entityManager.flush();

        assertThrows(NotFoundRuntimeException.class, () -> secureGroupService.getVariables(privateDataGroup.getId()));
    }

    @Test
    public void shouldNotAllowGetVariablesOfProtectedGroupOfOtherUser() {
        authenticate(MAIN_USER);

        Group group = secureGroupService.create(protectedGroup("my-group", system)).toGroup();

        assertEquals(MAIN_USER, group.getOwner());
        assertFalse(group.isVisible());

        Map<String, Set<Long>> associations = new HashMap<>();
        Variable variable = createVariable(system, createEntity(system, "entity1"), "my-variable", "my-field");
        associations.put("association", Collections.singleton(variable.getId()));
        rawGroupService.setVariables(group.getId(), associations); // create associations directly on the group
        entityManager.flush();

        authenticate(OTHER_USER);
        assertThrows(NotFoundRuntimeException.class, () -> secureGroupService.getVariables(group.getId()));
    }

    @Test
    public void shouldAllowGetVariablesOnPublicGroupOfOtherUser() {
        authenticate(MAIN_USER);

        Group group = secureGroupService.create(publicGroup("my-group", system)).toGroup();

        assertEquals(MAIN_USER, group.getOwner());
        assertTrue(group.isVisible());

        Map<String, Set<Long>> associations = new HashMap<>();
        Variable variable = createVariable(system, createEntity(system, "entity1"), "my-variable", "my-field");
        associations.put("association", Collections.singleton(variable.getId()));
        rawGroupService.setVariables(group.getId(), associations); // create associations directly on the group
        entityManager.flush();

        authenticate(OTHER_USER);
        Map<String, Set<VariableData>> variables = secureGroupService.getVariables(group.getId());
        assertEquals(1, variables.size());
        assertEquals(1, variables.get("association").size());
        assertEquals(variable, variables.get("association").iterator().next().toVariable());
    }

    @Test
    public void shouldAllowGetVariablesOnPersonalGroups() {
        authenticate(MAIN_USER);

        Group groupPublic = secureGroupService.create(publicGroup("my-group-public", system)).toGroup();
        assertEquals(MAIN_USER, groupPublic.getOwner());
        assertTrue(groupPublic.isVisible());

        Group groupProtected = secureGroupService.create(protectedGroup("my-group-protected", system)).toGroup();
        assertEquals(MAIN_USER, groupProtected.getOwner());
        assertFalse(groupProtected.isVisible());

        Map<String, Set<Long>> associations = new HashMap<>();
        Variable variable = createVariable(system, createEntity(system, "entity1"), "my-variable", "my-field");
        associations.put("association", Collections.singleton(variable.getId()));

        rawGroupService.setVariables(groupPublic.getId(), associations); // create associations directly on the group
        rawGroupService.setVariables(groupProtected.getId(), associations);
        entityManager.flush();

        Map<String, Set<VariableData>> variablesPublic = secureGroupService.getVariables(groupPublic.getId());
        assertEquals(1, variablesPublic.size());
        assertEquals(1, variablesPublic.get("association").size());
        assertEquals(variable, variablesPublic.get("association").iterator().next().toVariable());

        Map<String, Set<VariableData>> variablesProtected = secureGroupService.getVariables(groupProtected.getId());
        assertEquals(1, variablesProtected.size());
        assertEquals(1, variablesProtected.get("association").size());
        assertEquals(variable, variablesProtected.get("association").iterator().next().toVariable());
    }

    // test: removeVariables

    @Test
    public void shouldNotAllowRemoveVariablesOnPrivateGroup() {
        authenticate(MAIN_USER);

        GroupData privateDataGroup = createPrivateGroup("my-super-private-group", system);
        assertNotNull(privateDataGroup.getId());

        Map<String, Set<Long>> associations = new HashMap<>();
        Variable variable = createVariable(system, createEntity(system, "entity1"), "my-variable", "my-field");
        associations.put("private-association", Collections.singleton(variable.getId()));
        rawGroupService.setVariables(privateDataGroup.getId(),
                associations); // create associations directly on the group
        entityManager.flush();

        assertThrows(NotFoundRuntimeException.class,
                () -> secureGroupService.removeVariables(privateDataGroup.getId(), associations));
    }

    @Test
    public void shouldNotAllowModifyToRemoveVariablesOnGroupOfOtherUser() {
        authenticate(MAIN_USER);

        Group group = secureGroupService.create(publicGroup("my-group", system)).toGroup();

        assertEquals(MAIN_USER, group.getOwner());
        assertTrue(group.isVisible());

        Map<String, Set<Long>> associations = new HashMap<>();
        Variable variable = createVariable(system, createEntity(system, "entity1"), "my-variable", "my-field");
        associations.put("association", Collections.singleton(variable.getId()));
        rawGroupService.setVariables(group.getId(), associations); // create associations directly on the group
        entityManager.flush();

        authenticate(OTHER_USER);
        assertThrows(AccessDeniedRuntimeException.class,
                () -> secureGroupService.removeVariables(group.getId(), associations));
    }

    @Test
    public void shouldNotAllowModifyToRemoveVariablesOnProtectedGroupOfOtherUser() {
        authenticate(MAIN_USER);

        Group group = secureGroupService.create(protectedGroup("my-group", system)).toGroup();

        assertEquals(MAIN_USER, group.getOwner());
        assertFalse(group.isVisible());

        Map<String, Set<Long>> associations = new HashMap<>();
        Variable variable = createVariable(system, createEntity(system, "entity1"), "my-variable", "my-field");
        associations.put("association", Collections.singleton(variable.getId()));
        rawGroupService.setVariables(group.getId(), associations); // create associations directly on the group
        entityManager.flush();

        authenticate(OTHER_USER);
        assertThrows(NotFoundRuntimeException.class,
                () -> secureGroupService.removeVariables(group.getId(), associations));
    }

    @Test
    public void shouldAllowModifyToRemoveVariables() {
        authenticate(MAIN_USER);

        Group groupPublic = secureGroupService.create(publicGroup("my-group-public", system)).toGroup();
        assertEquals(MAIN_USER, groupPublic.getOwner());
        assertTrue(groupPublic.isVisible());

        Group groupProtected = secureGroupService.create(protectedGroup("my-group-protected", system)).toGroup();
        assertEquals(MAIN_USER, groupProtected.getOwner());
        assertFalse(groupProtected.isVisible());

        Map<String, Set<Long>> associations = new HashMap<>();
        Variable variable = createVariable(system, createEntity(system, "entity1"), "my-variable", "my-field");
        associations.put("association", Collections.singleton(variable.getId()));

        rawGroupService.setVariables(groupPublic.getId(), associations); // create associations directly on the group
        rawGroupService.setVariables(groupProtected.getId(), associations);
        entityManager.flush();

        secureGroupService.removeVariables(groupPublic.getId(), associations);
        secureGroupService.removeVariables(groupProtected.getId(), associations);

        Map<String, Set<VariableData>> variablesPublic = secureGroupService.getVariables(groupPublic.getId());
        assertTrue(variablesPublic.isEmpty());

        Map<String, Set<VariableData>> variablesProtected = secureGroupService.getVariables(groupProtected.getId());
        assertTrue(variablesProtected.isEmpty());
    }

    // test: setEntities

    @Test
    public void shouldNotAllowSetEntitiesOnPrivateGroup() {
        authenticate(MAIN_USER);

        GroupData privateDataGroup = createPrivateGroup("my-super-private-group", system);
        assertNotNull(privateDataGroup.getId());

        Map<String, Set<Long>> associations = new HashMap<>();
        Entity entity = createEntity(system, "entity");
        associations.put("private-association", Collections.singleton(entity.getId()));

        assertThrows(NotFoundRuntimeException.class,
                () -> secureGroupService.setEntities(privateDataGroup.getId(), associations));
    }

    @Test
    public void shouldNotAllowModifyToSetEntitiesOnGroupOfOtherUser() {
        authenticate(MAIN_USER);

        Group group = secureGroupService.create(publicGroup("my-group", system)).toGroup();

        assertEquals(MAIN_USER, group.getOwner());
        assertTrue(group.isVisible());

        Map<String, Set<Long>> associations = new HashMap<>();
        Entity entity = createEntity(system, "entity");
        associations.put("association", Collections.singleton(entity.getId()));

        authenticate(OTHER_USER);
        assertThrows(AccessDeniedRuntimeException.class,
                () -> secureGroupService.setEntities(group.getId(), associations));
    }

    @Test
    public void shouldNotAllowModifyToSetEntitiesOnProtectedGroupOfOtherUser() {
        authenticate(MAIN_USER);

        Group group = secureGroupService.create(protectedGroup("my-group", system)).toGroup();

        assertEquals(MAIN_USER, group.getOwner());
        assertFalse(group.isVisible());

        Map<String, Set<Long>> associations = new HashMap<>();
        Entity entity = createEntity(system, "entity");
        associations.put("association", Collections.singleton(entity.getId()));

        authenticate(OTHER_USER);
        assertThrows(NotFoundRuntimeException.class, () -> secureGroupService.setEntities(group.getId(), associations));
    }

    @Test
    public void shouldAllowModifyToSetEntities() {
        authenticate(MAIN_USER);

        Group groupPublic = secureGroupService.create(publicGroup("my-group-public", system)).toGroup();
        assertEquals(MAIN_USER, groupPublic.getOwner());
        assertTrue(groupPublic.isVisible());

        Group groupProtected = secureGroupService.create(protectedGroup("my-group-protected", system)).toGroup();
        assertEquals(MAIN_USER, groupProtected.getOwner());
        assertFalse(groupProtected.isVisible());

        Map<String, Set<Long>> associations = new HashMap<>();
        Entity entity = createEntity(system, "entity");
        associations.put("association", Collections.singleton(entity.getId()));

        secureGroupService.setEntities(groupPublic.getId(), associations);
        secureGroupService.setEntities(groupProtected.getId(), associations);

        Map<String, Set<EntityData>> entitiesPublic = secureGroupService.getEntities(groupPublic.getId());
        assertEquals(1, entitiesPublic.size());
        assertEquals(1, entitiesPublic.get("association").size());
        assertEquals(entity, entitiesPublic.get("association").iterator().next().toEntity());

        Map<String, Set<EntityData>> entitiesProtected = secureGroupService.getEntities(groupProtected.getId());
        assertEquals(1, entitiesProtected.size());
        assertEquals(1, entitiesProtected.get("association").size());
        assertEquals(entity, entitiesProtected.get("association").iterator().next().toEntity());
    }

    // test: addEntities

    @Test
    public void shouldNotAllowAddEntitiesOnPrivateGroup() {
        authenticate(MAIN_USER);

        GroupData privateDataGroup = createPrivateGroup("my-super-private-group", system);
        assertNotNull(privateDataGroup.getId());

        Map<String, Set<Long>> associations = new HashMap<>();
        Entity entity = createEntity(system, "entity");
        ;
        associations.put("private-association", Collections.singleton(entity.getId()));

        assertThrows(NotFoundRuntimeException.class,
                () -> secureGroupService.addEntities(privateDataGroup.getId(), associations));
    }

    @Test
    public void shouldNotAllowModifyToAddEntitiesOnGroupOfOtherUser() {
        authenticate(MAIN_USER);

        Group group = secureGroupService.create(publicGroup("my-group", system)).toGroup();

        assertEquals(MAIN_USER, group.getOwner());
        assertTrue(group.isVisible());

        Map<String, Set<Long>> associations = new HashMap<>();
        Entity entity = createEntity(system, "entity");
        ;
        associations.put("association", Collections.singleton(entity.getId()));

        authenticate(OTHER_USER);
        assertThrows(AccessDeniedRuntimeException.class,
                () -> secureGroupService.addEntities(group.getId(), associations));
    }

    @Test
    public void shouldNotAllowModifyToAddEntitiesOnProtectedGroupOfOtherUser() {
        authenticate(MAIN_USER);

        Group group = secureGroupService.create(protectedGroup("my-group", system)).toGroup();

        assertEquals(MAIN_USER, group.getOwner());
        assertFalse(group.isVisible());

        Map<String, Set<Long>> associations = new HashMap<>();
        Entity entity = createEntity(system, "entity");
        associations.put("association", Collections.singleton(entity.getId()));

        authenticate(OTHER_USER);
        assertThrows(NotFoundRuntimeException.class, () -> secureGroupService.addEntities(group.getId(), associations));
    }

    @Test
    public void shouldAllowModifyToAddEntities() {
        authenticate(MAIN_USER);

        Group groupPublic = secureGroupService.create(publicGroup("my-group-public", system)).toGroup();
        assertEquals(MAIN_USER, groupPublic.getOwner());
        assertTrue(groupPublic.isVisible());

        Group groupProtected = secureGroupService.create(protectedGroup("my-group-protected", system)).toGroup();
        assertEquals(MAIN_USER, groupProtected.getOwner());
        assertFalse(groupProtected.isVisible());

        Map<String, Set<Long>> associations = new HashMap<>();
        Entity entity = createEntity(system, "entity");
        associations.put("association", Collections.singleton(entity.getId()));

        secureGroupService.addEntities(groupPublic.getId(), associations);
        secureGroupService.addEntities(groupProtected.getId(), associations);

        Map<String, Set<EntityData>> entitiesPublic = secureGroupService.getEntities(groupPublic.getId());
        assertEquals(1, entitiesPublic.size());
        assertEquals(1, entitiesPublic.get("association").size());
        assertEquals(entity, entitiesPublic.get("association").iterator().next().toEntity());

        Map<String, Set<EntityData>> entitiesProtected = secureGroupService.getEntities(groupProtected.getId());
        assertEquals(1, entitiesProtected.size());
        assertEquals(1, entitiesProtected.get("association").size());
        assertEquals(entity, entitiesProtected.get("association").iterator().next().toEntity());
    }

    // test: getEntities

    @Test
    public void shouldNotAllowGetEntitiesOfPrivateGroup() {
        authenticate(MAIN_USER);

        GroupData privateDataGroup = createPrivateGroup("my-super-private-group", system);
        assertNotNull(privateDataGroup.getId());

        Map<String, Set<Long>> associations = new HashMap<>();
        Entity entity = createEntity(system, "entity");
        associations.put("private-association", Collections.singleton(entity.getId()));
        rawGroupService.setEntities(privateDataGroup.getId(),
                associations); // create associations directly on the group
        entityManager.flush();

        assertThrows(NotFoundRuntimeException.class, () -> secureGroupService.getEntities(privateDataGroup.getId()));
    }

    @Test
    public void shouldNotAllowGetEntitiesOfProtectedGroupOfOtherUser() {
        authenticate(MAIN_USER);

        Group group = secureGroupService.create(protectedGroup("my-group", system)).toGroup();

        assertEquals(MAIN_USER, group.getOwner());
        assertFalse(group.isVisible());

        Map<String, Set<Long>> associations = new HashMap<>();
        Entity entity = createEntity(system, "entity");
        associations.put("association", Collections.singleton(entity.getId()));
        rawGroupService.setEntities(group.getId(), associations); // create associations directly on the group
        entityManager.flush();

        authenticate(OTHER_USER);
        assertThrows(NotFoundRuntimeException.class, () -> secureGroupService.getEntities(group.getId()));
    }

    @Test
    public void shouldAllowGetEntitiesOnPublicGroupOfOtherUser() {
        authenticate(MAIN_USER);

        Group group = secureGroupService.create(publicGroup("my-group", system)).toGroup();

        assertEquals(MAIN_USER, group.getOwner());
        assertTrue(group.isVisible());

        Map<String, Set<Long>> associations = new HashMap<>();
        Entity entity = createEntity(system, "entity");
        associations.put("association", Collections.singleton(entity.getId()));
        rawGroupService.setEntities(group.getId(), associations); // create associations directly on the group
        entityManager.flush();

        authenticate(OTHER_USER);
        Map<String, Set<EntityData>> entities = secureGroupService.getEntities(group.getId());
        assertEquals(1, entities.size());
        assertEquals(1, entities.get("association").size());
        assertEquals(entity, entities.get("association").iterator().next().toEntity());
    }

    @Test
    public void shouldAllowGetEntitiesOnPersonalGroups() {
        authenticate(MAIN_USER);

        Group groupPublic = secureGroupService.create(publicGroup("my-group-public", system)).toGroup();
        assertEquals(MAIN_USER, groupPublic.getOwner());
        assertTrue(groupPublic.isVisible());

        Group groupProtected = secureGroupService.create(protectedGroup("my-group-protected", system)).toGroup();
        assertEquals(MAIN_USER, groupProtected.getOwner());
        assertFalse(groupProtected.isVisible());

        Map<String, Set<Long>> associations = new HashMap<>();
        Entity entity = createEntity(system, "entity");
        associations.put("association", Collections.singleton(entity.getId()));

        rawGroupService.setEntities(groupPublic.getId(), associations); // create associations directly on the group
        rawGroupService.setEntities(groupProtected.getId(), associations);
        entityManager.flush();

        Map<String, Set<EntityData>> entitiesPublic = secureGroupService.getEntities(groupPublic.getId());
        assertEquals(1, entitiesPublic.size());
        assertEquals(1, entitiesPublic.get("association").size());
        assertEquals(entity, entitiesPublic.get("association").iterator().next().toEntity());

        Map<String, Set<EntityData>> entitiesProtected = secureGroupService.getEntities(groupProtected.getId());
        assertEquals(1, entitiesProtected.size());
        assertEquals(1, entitiesProtected.get("association").size());
        assertEquals(entity, entitiesProtected.get("association").iterator().next().toEntity());
    }

    // test: removeEntities

    @Test
    public void shouldNotAllowRemoveEntitiesOnPrivateGroup() {
        authenticate(MAIN_USER);

        GroupData privateDataGroup = createPrivateGroup("my-super-private-group", system);
        assertNotNull(privateDataGroup.getId());

        Map<String, Set<Long>> associations = new HashMap<>();
        Entity entity = createEntity(system, "entity");
        associations.put("private-association", Collections.singleton(entity.getId()));
        rawGroupService.setEntities(privateDataGroup.getId(),
                associations); // create associations directly on the group
        entityManager.flush();

        assertThrows(NotFoundRuntimeException.class,
                () -> secureGroupService.removeEntities(privateDataGroup.getId(), associations));
    }

    @Test
    public void shouldNotAllowModifyToRemoveEntitiesOnGroupOfOtherUser() {
        authenticate(MAIN_USER);

        Group group = secureGroupService.create(publicGroup("my-group", system)).toGroup();

        assertEquals(MAIN_USER, group.getOwner());
        assertTrue(group.isVisible());

        Map<String, Set<Long>> associations = new HashMap<>();
        Entity entity = createEntity(system, "entity");
        associations.put("association", Collections.singleton(entity.getId()));
        rawGroupService.setEntities(group.getId(), associations); // create associations directly on the group
        entityManager.flush();

        authenticate(OTHER_USER);
        assertThrows(AccessDeniedRuntimeException.class,
                () -> secureGroupService.removeEntities(group.getId(), associations));
    }

    @Test
    public void shouldNotAllowModifyToRemoveEntitiesOnProtectedGroupOfOtherUser() {
        authenticate(MAIN_USER);

        Group group = secureGroupService.create(protectedGroup("my-group", system)).toGroup();

        assertEquals(MAIN_USER, group.getOwner());
        assertFalse(group.isVisible());

        Map<String, Set<Long>> associations = new HashMap<>();
        Entity entity = createEntity(system, "entity");
        associations.put("association", Collections.singleton(entity.getId()));
        rawGroupService.setEntities(group.getId(), associations); // create associations directly on the group
        entityManager.flush();

        authenticate(OTHER_USER);
        assertThrows(NotFoundRuntimeException.class,
                () -> secureGroupService.removeEntities(group.getId(), associations));
    }

    @Test
    public void shouldAllowModifyToRemoveEntities() {
        authenticate(MAIN_USER);

        Group groupPublic = secureGroupService.create(publicGroup("my-group-public", system)).toGroup();
        assertEquals(MAIN_USER, groupPublic.getOwner());
        assertTrue(groupPublic.isVisible());

        Group groupProtected = secureGroupService.create(protectedGroup("my-group-protected", system)).toGroup();
        assertEquals(MAIN_USER, groupProtected.getOwner());
        assertFalse(groupProtected.isVisible());

        Map<String, Set<Long>> associations = new HashMap<>();
        Entity entity = createEntity(system, "entity");
        associations.put("association", Collections.singleton(entity.getId()));

        rawGroupService.setEntities(groupPublic.getId(), associations); // create associations directly on the group
        rawGroupService.setEntities(groupProtected.getId(), associations);
        entityManager.flush();

        secureGroupService.removeEntities(groupPublic.getId(), associations);
        secureGroupService.removeEntities(groupProtected.getId(), associations);

        Map<String, Set<EntityData>> entitiesPublic = secureGroupService.getEntities(groupPublic.getId());
        assertTrue(entitiesPublic.isEmpty());

        Map<String, Set<EntityData>> entitiesProtected = secureGroupService.getEntities(groupProtected.getId());
        assertTrue(entitiesProtected.isEmpty());
    }

    // helper methods

    private boolean exists(String s) {
        return !secureGroupService.findAll(toRSQL(Groups.suchThat().name().eq(s))).isEmpty();
    }

    private GroupData createPrivateGroup(String name, SystemSpec system) {
        SystemSpecData systemData = systemRepository.findById(system.getId()).orElseThrow(
                () -> new IllegalStateException(String.format("System by id [ %s ] does not exist!", system.getId())));

        GroupData groupData = new GroupData();
        groupData.setName(name);
        groupData.setDescription("this is a private group created for testing");
        groupData.setSystem(systemData);
        groupData.setOwner(MAIN_USER);
        groupData.setVisibility(VisibilityData.PRIVATE);
        return groupRepository.save(groupData);
    }

    private Entity createEntity(SystemSpec system, String name) {
        Map<String, Object> keyValues = ImmutableMap.of(ENTITY_STRING_SCHEMA_KEY_1, name, ENTITY_DOUBLE_SCHEMA_KEY_1,
                1d);
        return entityService.createEntity(system.getId(), keyValues, PARTITION_KEY_VALUES_1).toEntity();
    }

    private Variable createVariable(SystemSpec system, Entity entity, String name, String field) {
        VariableConfig config = VariableConfig.builder().entityId(entity.getId()).fieldName(field).build();
        Variable variable = Variable.builder().variableName(name).configs(ImmutableSortedSet.of(config))
                .systemSpec(system).build();
        VariableData variableData = variableService.create(variable);
        return variableData.toVariable();
    }

}
