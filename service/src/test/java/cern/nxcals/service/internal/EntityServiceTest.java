/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.internal;

import cern.nxcals.api.domain.CreateEntityRequest;
import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.extraction.metadata.queries.Entities;
import cern.nxcals.service.BaseTest;
import cern.nxcals.service.domain.EntityData;
import cern.nxcals.service.domain.EntityHistoryData;
import cern.nxcals.service.domain.EntitySchemaData;
import cern.nxcals.service.domain.PartitionData;
import cern.nxcals.service.domain.SystemSpecData;
import cern.nxcals.service.rest.exceptions.ConfigDataConflictException;
import cern.nxcals.service.rest.exceptions.NotFoundRuntimeException;
import com.google.common.collect.Sets;
import org.apache.avro.Schema;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.SortedSet;
import java.util.function.Function;
import java.util.stream.Collectors;

import static cern.nxcals.api.utils.TimeUtils.getInstantFromNanos;
import static cern.nxcals.common.utils.KeyValuesUtils.convertMapIntoAvroSchemaString;
import static cern.nxcals.common.utils.RSQLUtils.toRSQL;
import static cern.nxcals.service.rest.DomainTestConstants.ENTITY_KEY_VALUES_1;
import static cern.nxcals.service.rest.DomainTestConstants.ENTITY_KEY_VALUES_1_2;
import static cern.nxcals.service.rest.DomainTestConstants.ENTITY_KEY_VALUES_2;
import static cern.nxcals.service.rest.DomainTestConstants.PARTITION_KEY_VALUES_1;
import static cern.nxcals.service.rest.DomainTestConstants.PARTITION_KEY_VALUES_1_2;
import static cern.nxcals.service.rest.DomainTestConstants.PARTITION_KEY_VALUES_2;
import static cern.nxcals.service.rest.TestSchemas.ENTITY_SCHEMA_1;
import static cern.nxcals.service.rest.TestSchemas.ENTITY_SCHEMA_2;
import static cern.nxcals.service.rest.TestSchemas.ENTITY_STRING_SCHEMA_KEY_1;
import static cern.nxcals.service.rest.TestSchemas.PARTITION_SCHEMA_1;
import static cern.nxcals.service.rest.TestSchemas.PARTITION_SCHEMA_2;
import static cern.nxcals.service.rest.TestSchemas.PARTITION_STRING_SCHEMA_KEY_1;
import static cern.nxcals.service.rest.TestSchemas.TIME_SCHEMA;
import static cern.nxcals.service.rest.TestSchemas.createRandomSchema;
import static cern.nxcals.service.rest.TestSchemas.getFullSchema;
import static java.util.stream.Collectors.toCollection;
import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Transactional(transactionManager = "jpaTransactionManager")
@Rollback
public class EntityServiceTest extends BaseTest {

    @BeforeEach
    public void setUp() {
        setAuthentication();
    }

    @Test
    public void shouldNotCreateEntityKeyForNonExistingSystem() {
        assertThrows(NotFoundRuntimeException.class, () ->
                service.findOrCreateEntityFor(-1L, ENTITY_KEY_VALUES_1, PARTITION_KEY_VALUES_1,
                    getFullSchema(ENTITY_SCHEMA_1, PARTITION_SCHEMA_1).toString(), TEST_RECORD_TIME)
        );

    }

    @Test
    public void shouldNotGetResultForLateEntityWithSchemaNotPresentedInTheHistory() {
        SystemSpecData system = createAndPersistSystemData(TEST_NAME, ENTITY_SCHEMA_1, PARTITION_SCHEMA_1,
                TIME_SCHEMA);
        Long systemId = system.getId();

        service.findOrCreateEntityFor(systemId, ENTITY_KEY_VALUES_1, PARTITION_KEY_VALUES_1,
                getFullSchema(ENTITY_SCHEMA_1, PARTITION_SCHEMA_1).toString(), TEST_RECORD_TIME);
        service.findOrCreateEntityFor(systemId, ENTITY_KEY_VALUES_1, PARTITION_KEY_VALUES_1, ENTITY_SCHEMA_2.toString(),
                100 * TEST_RECORD_TIME);
        assertThrows(ConfigDataConflictException.class, () ->
                service.findOrCreateEntityFor(systemId, ENTITY_KEY_VALUES_1, PARTITION_KEY_VALUES_1, ENTITY_SCHEMA_2.toString(),
                TEST_RECORD_TIME + 10));
    }

    @Test
    public void shouldNotGetResultForLateEntityWithPartitionNotPresentedInTheHistory() {

        long systemId = createAndPersistSystemData(TEST_NAME, ENTITY_SCHEMA_1, PARTITION_SCHEMA_1, TIME_SCHEMA)
                .getId();

        HashMap<String, Object> newPartitionKeyValues = new HashMap<>(PARTITION_KEY_VALUES_1);
        newPartitionKeyValues.put(PARTITION_STRING_SCHEMA_KEY_1, "new_value");

        service.findOrCreateEntityFor(systemId, ENTITY_KEY_VALUES_1, PARTITION_KEY_VALUES_1,
                getFullSchema(ENTITY_SCHEMA_1, PARTITION_SCHEMA_1).toString(), TEST_RECORD_TIME);

        service.findOrCreateEntityFor(systemId, ENTITY_KEY_VALUES_1, newPartitionKeyValues,
                getFullSchema(ENTITY_SCHEMA_1, PARTITION_SCHEMA_1).toString(), 100 * TEST_RECORD_TIME);

        assertThrows(ConfigDataConflictException.class, () ->
                service.findOrCreateEntityFor(systemId, ENTITY_KEY_VALUES_1, newPartitionKeyValues,
                getFullSchema(ENTITY_SCHEMA_1, PARTITION_SCHEMA_1).toString(), TEST_RECORD_TIME + 10));
    }

    @Test
    public void shouldRejectHistoryWithDifferentSchemaAndSameCreationTime() {
        Long systemId = createAndPersistSystemData(TEST_NAME, ENTITY_SCHEMA_1, PARTITION_SCHEMA_1, TIME_SCHEMA)
                .getId();

        service.findOrCreateEntityFor(systemId, ENTITY_KEY_VALUES_1, PARTITION_KEY_VALUES_1,
                getFullSchema(ENTITY_SCHEMA_1, PARTITION_SCHEMA_1).toString(), TEST_RECORD_TIME);
        assertThrows(ConfigDataConflictException.class, () ->
                service.findOrCreateEntityFor(systemId, ENTITY_KEY_VALUES_1, PARTITION_KEY_VALUES_1, ENTITY_SCHEMA_2.toString(),
                TEST_RECORD_TIME));
    }

    @Test
    public void shouldCreateEntity() {
        Long systemId = createAndPersistSystemData(TEST_NAME, ENTITY_SCHEMA_1, PARTITION_SCHEMA_1, TIME_SCHEMA)
                .getId();

        Long schemaId = service.findOrCreateEntityFor(systemId, ENTITY_KEY_VALUES_1, PARTITION_KEY_VALUES_1,
                getFullSchema(ENTITY_SCHEMA_1, PARTITION_SCHEMA_1).toString(), TEST_RECORD_TIME).getEntityHistories()
                .first().getSchema().getId();
        Optional<EntitySchemaData> optionalSchema = schemaRepository.findById(schemaId);

        assertThat(optionalSchema).isPresent();

        EntitySchemaData foundSchema = optionalSchema.get();
        assertThat(foundSchema.getContent()).isEqualTo(getFullSchema(ENTITY_SCHEMA_1, PARTITION_SCHEMA_1).toString());
    }

    @Test
    public void shouldFindOrCreateEntityWithId() {
        EntityData entity = createAndPersistEntity(TEST_NAME, ENTITY_KEY_VALUES_1, ENTITY_SCHEMA_1,
                PARTITION_KEY_VALUES_1, PARTITION_SCHEMA_1);
        createAndPersistEntityHistory(entity, entity.getPartition(), createSchema(ENTITY_SCHEMA_1), TEST_RECORD_TIME);

        EntityData foundEntity = service
                .findOrCreateEntityFor(entity.getPartition().getSystem().getId(), entity.getId(),
                        entity.getPartition().getId(), ENTITY_SCHEMA_1.toString(), TEST_RECORD_TIME);

        assertThat(foundEntity.getEntityHistories().first().getSchema().getContent())
                .isEqualTo(ENTITY_SCHEMA_1.toString());
        assertThat(foundEntity.getId()).isEqualTo(entity.getId());
    }

    @Test
    public void shouldFindOrCreateEntityWithIdForNextTimestamp() {
        EntityData entity = createAndPersistEntity(TEST_NAME, ENTITY_KEY_VALUES_1, ENTITY_SCHEMA_1,
                PARTITION_KEY_VALUES_1, PARTITION_SCHEMA_1);
        createAndPersistEntityHistory(entity, entity.getPartition(), createSchema(ENTITY_SCHEMA_1), TEST_RECORD_TIME);

        EntityData foundEntity = service
                .findOrCreateEntityFor(entity.getPartition().getSystem().getId(), entity.getId(),
                        entity.getPartition().getId(), ENTITY_SCHEMA_1.toString(), TEST_RECORD_TIME + TEST_RECORD_TIME);

        assertThat(foundEntity.getEntityHistories().first().getSchema().getContent())
                .isEqualTo(ENTITY_SCHEMA_1.toString());
    }

    @Test
    public void shouldFindOrCreateEntityWithIdForDifferentSchema() {
        EntityData entity = createAndPersistEntity(TEST_NAME, ENTITY_KEY_VALUES_1, ENTITY_SCHEMA_1,
                PARTITION_KEY_VALUES_1, PARTITION_SCHEMA_1);
        createAndPersistEntityHistory(entity, entity.getPartition(), createSchema(ENTITY_SCHEMA_1), TEST_RECORD_TIME);

        EntityData foundEntity = service
                .findOrCreateEntityFor(entity.getPartition().getSystem().getId(), entity.getId(),
                        entity.getPartition().getId(), ENTITY_SCHEMA_2.toString(), TEST_RECORD_TIME + TEST_RECORD_TIME);

        assertThat(foundEntity.getEntityHistories()).hasSize(1);
        EntityHistoryData entityHistory = foundEntity.getEntityHistories().first();

        assertThat(entityHistory.getValidFromStamp())
                .isEqualTo(getInstantFromNanos(TEST_RECORD_TIME + TEST_RECORD_TIME));
        assertThat(entityHistory.getValidToStamp()).isNull();
    }

    private void verifyTest(String testSchemaContent) {
        EntityData entity = createAndPersistEntity(TEST_NAME, ENTITY_KEY_VALUES_1, ENTITY_SCHEMA_1,
                PARTITION_KEY_VALUES_1, PARTITION_SCHEMA_1);
        createAndPersistEntityHistory(entity, entity.getPartition(), createSchema(ENTITY_SCHEMA_1), TEST_RECORD_TIME);

        HashMap<String, Object> newPartitionKeyValues = new HashMap<>(PARTITION_KEY_VALUES_1);
        newPartitionKeyValues.put(PARTITION_STRING_SCHEMA_KEY_1, "new_value");

        PartitionData newPartition = createPartition(entity.getPartition().getSystem(), newPartitionKeyValues,
                PARTITION_SCHEMA_1);
        partitionRepository.save(newPartition);

        EntityData foundEntity = service
                .findOrCreateEntityFor(entity.getPartition().getSystem().getId(), entity.getId(), newPartition.getId(),
                        testSchemaContent, TEST_RECORD_TIME + TEST_RECORD_TIME);

        assertThat(foundEntity.getEntityHistories()).hasSize(1);

        EntityHistoryData entityHistory = foundEntity.getEntityHistories().first();

        assertThat(entityHistory.getValidFromStamp())
                .isEqualTo(getInstantFromNanos(TEST_RECORD_TIME + TEST_RECORD_TIME));
        assertThat(entityHistory.getValidToStamp()).isNull();
    }

    @Test
    public void shouldFindOrCreateEntityWithIdForDifferentPartition() {
        verifyTest(ENTITY_SCHEMA_1.toString());
    }

    @Test
    public void shouldFindOrCreateEntityWithIdForDifferentPartitionAndSchema() {
        verifyTest(ENTITY_SCHEMA_2.toString());
    }

    @Test
    public void shouldFailToFindOrCreateEntityWithIdForDifferentPartitionAndSchemaWithUsedTimestamp() {
        EntityData entity = createAndPersistEntity(TEST_NAME, ENTITY_KEY_VALUES_1, ENTITY_SCHEMA_1,
                PARTITION_KEY_VALUES_1, PARTITION_SCHEMA_1);
        createAndPersistEntityHistory(entity, entity.getPartition(), createSchema(ENTITY_SCHEMA_1), TEST_RECORD_TIME);

        HashMap<String, Object> newPartitionKeyValues = new HashMap<>(PARTITION_KEY_VALUES_1);
        newPartitionKeyValues.put(PARTITION_STRING_SCHEMA_KEY_1, "new_value");

        PartitionData newPartition = createPartition(entity.getPartition().getSystem(), newPartitionKeyValues,
                PARTITION_SCHEMA_1);
        partitionRepository.save(newPartition);

        assertThrows(ConfigDataConflictException.class, () ->
                service.findOrCreateEntityFor(entity.getPartition().getSystem().getId(), entity.getId(), newPartition.getId(),
                ENTITY_SCHEMA_2.toString(), TEST_RECORD_TIME));
    }

    @Test
    public void shouldFailOnFindOrCreateEntityWithIdWithWrongSystem() {
        EntityData entity = createAndPersistEntity(TEST_NAME, ENTITY_KEY_VALUES_1, ENTITY_SCHEMA_1,
                PARTITION_KEY_VALUES_1, PARTITION_SCHEMA_1);
        createAndPersistEntityHistory(entity, entity.getPartition(), createSchema(ENTITY_SCHEMA_1), TEST_RECORD_TIME);

        //This is another system
        SystemSpecData system = createAndPersistSystemData(TEST_NAME, ENTITY_SCHEMA_1, ENTITY_SCHEMA_1,
                ENTITY_SCHEMA_1);
        Long systemId = system.getId();

        assertThrows(IllegalArgumentException.class, () ->
                service.findOrCreateEntityFor(systemId, entity.getId(), entity.getPartition().getId(),
                ENTITY_SCHEMA_1.toString(), TEST_RECORD_TIME));
    }

    @Test
    public void shouldUpdateEntitySchema() {
        EntityData entity = createAndPersistEntity(TEST_NAME, ENTITY_KEY_VALUES_1, ENTITY_SCHEMA_1,
                PARTITION_KEY_VALUES_1, PARTITION_SCHEMA_1);
        Long systemId = findOrCreateAndPersistSystemData(TEST_NAME, ENTITY_SCHEMA_1, PARTITION_SCHEMA_1, TIME_SCHEMA)
                .getId();
        createAndPersistEntityHistory(entity, entity.getPartition(), createSchema(ENTITY_SCHEMA_1), TEST_RECORD_TIME);

        // TODO: Confirm why in the previous test there was no need to increment the recordTimestamp, it should not be possible to send the same as far as I know?
        Long schemaId = service.findOrCreateEntityFor(systemId, ENTITY_KEY_VALUES_1, PARTITION_KEY_VALUES_1,
                getFullSchema(ENTITY_SCHEMA_1, PARTITION_SCHEMA_1).toString(), TEST_RECORD_TIME + 10)
                .getEntityHistories().first().getSchema().getId();

        Optional<EntitySchemaData> optionalSchema = schemaRepository.findById(schemaId);

        assertThat(optionalSchema).isPresent();

        EntitySchemaData foundSchema = optionalSchema.get();

        assertThat(foundSchema.getContent()).isEqualTo(getFullSchema(ENTITY_SCHEMA_1, PARTITION_SCHEMA_1).toString());
        assertThat(foundSchema.getId()).isEqualTo(schemaId);
    }

    @Test
    public void shouldUpdateEntityPartition() {
        EntityData entity = createAndPersistEntity(TEST_NAME, ENTITY_KEY_VALUES_1, ENTITY_SCHEMA_1,
                PARTITION_KEY_VALUES_1, PARTITION_SCHEMA_1);

        createAndPersistEntityHistory(entity, entity.getPartition(), createSchema(ENTITY_SCHEMA_1), TEST_RECORD_TIME);

        Map<String, Object> newPartitionKeyValues = new HashMap<>(PARTITION_KEY_VALUES_1);
        newPartitionKeyValues.put(PARTITION_STRING_SCHEMA_KEY_1, "new_value");

        Long systemId = entity.getPartition().getSystem().getId();

        Long entityId = service.findOrCreateEntityFor(systemId, ENTITY_KEY_VALUES_1, newPartitionKeyValues,
                getFullSchema(ENTITY_SCHEMA_1, PARTITION_SCHEMA_1).toString(), TEST_RECORD_TIME + 10).getId();

        Optional<EntityData> optionalEntity = entityRepository.findById(entityId);

        assertThat(optionalEntity).isPresent();

        EntityData foundEntity = optionalEntity.get();

        assertThat(foundEntity.getPartition().getKeyValues())
                .isEqualTo(convertMapIntoAvroSchemaString(newPartitionKeyValues, PARTITION_SCHEMA_1.toString()));
    }

    @Test
    public void shouldThrowExceptionOnUpdateWhenEntityDoesNotExists() {
        EntityData entity = createAndPersistEntity(TEST_NAME, ENTITY_KEY_VALUES_1, ENTITY_SCHEMA_1,
                PARTITION_KEY_VALUES_1, PARTITION_SCHEMA_1);

        Map<String, Object> newEntityKeyValues = new HashMap<>(ENTITY_KEY_VALUES_1);
        newEntityKeyValues.put(ENTITY_STRING_SCHEMA_KEY_1, "new_value");

        Entity updatedEntityData = entity.toEntity().toBuilder().entityKeyValues(newEntityKeyValues).build();

        EntityData nonExistingEntity = createEntity(TEST_NAME, ENTITY_KEY_VALUES_1, ENTITY_SCHEMA_1,
                PARTITION_KEY_VALUES_1, PARTITION_SCHEMA_1);
        nonExistingEntity.setRecVersion(0L);
        nonExistingEntity.getPartition().setRecVersion(0L);
        Entity nonExistingEntityData = nonExistingEntity.toEntity().toBuilder().build();

        assertThrows(NotFoundRuntimeException.class, () ->
                service.updateEntities(Arrays.asList(updatedEntityData, nonExistingEntityData)));
    }

    @Test
    public void shouldThrowExceptionWhenProvidedUpdateEntityIsOlderVersion() {
        EntityData entity = createAndPersistEntity(TEST_NAME, ENTITY_KEY_VALUES_1, ENTITY_SCHEMA_1,
                PARTITION_KEY_VALUES_1, PARTITION_SCHEMA_1);

        Map<String, Object> oldEntityKeyValues = new HashMap<>(ENTITY_KEY_VALUES_1);
        oldEntityKeyValues.put(ENTITY_STRING_SCHEMA_KEY_1, "old_value");

        Map<String, Object> newEntityKeyValues = new HashMap<>(ENTITY_KEY_VALUES_1);
        newEntityKeyValues.put(ENTITY_STRING_SCHEMA_KEY_1, "new_value");

        Entity oldEntityData = entity.toEntity().toBuilder().entityKeyValues(oldEntityKeyValues).build();

        Entity updatedEntityData = entity.toEntity().toBuilder().entityKeyValues(newEntityKeyValues).build();

        service.updateEntities(Collections.singletonList(updatedEntityData));

        assertThrows(ObjectOptimisticLockingFailureException.class, () ->
                service.updateEntities(Collections.singletonList(oldEntityData)));
    }

    @Test
    public void shouldUpdateAndLockMoreThan1kEntities() {
        // This is mandatory to constantly ensure that we overcome the oracle limitations described by
        // issue: "ORA-01795: maximum number of expressions in a list is 1000"
        int items = 1234;

        List<Entity> toUpdateEntities = new ArrayList<>();
        Instant lockUntilEpochNanos = Instant.now().plus(2, ChronoUnit.HOURS);
        for (int i = 0; i < items; i++) {
            Schema randomSchema = createRandomSchema("entity_type_" + i);
            EntityData entity = createAndPersistEntity(TEST_NAME + i, ENTITY_KEY_VALUES_1, randomSchema,
                    PARTITION_KEY_VALUES_1, PARTITION_SCHEMA_1);

            Entity updatedEntityData = entity.toEntity().toBuilder().lockedUntilStamp(lockUntilEpochNanos).build();

            toUpdateEntities.add(updatedEntityData);
        }
        List<EntityData> updatedEntities = new ArrayList<>(service.updateEntities(toUpdateEntities));

        boolean noneNullLockedUntilStamp = updatedEntities.stream().map(EntityData::getLockedUntilStamp)
                .noneMatch(Objects::isNull);
        assertTrue(noneNullLockedUntilStamp);
        assertEquals(items, updatedEntities.size());
    }

    @Test
    public void shouldUpdateAndLockOneEntity() {
        EntityData entity = createAndPersistEntity(TEST_NAME, ENTITY_KEY_VALUES_1, ENTITY_SCHEMA_1,
                PARTITION_KEY_VALUES_1, PARTITION_SCHEMA_1);

        Instant lockUntilEpochNanos = Instant.now().plus(2, ChronoUnit.HOURS);

        Entity updatedEntityData = entity.toEntity().toBuilder().lockedUntilStamp(lockUntilEpochNanos).build();

        LinkedList<EntityData> updatedEntities = new LinkedList<>(
                service.updateEntities(Collections.singletonList(updatedEntityData)));

        assertThat(updatedEntities).isNotNull();
        assertThat(updatedEntities).hasSize(1);

        EntityData updatedEntity = updatedEntities.getFirst();

        assertThat(updatedEntity.getId()).isEqualTo(entity.getId());
        assertThat(updatedEntity.getLockedUntilStamp()).isEqualTo(lockUntilEpochNanos);
    }

    @Test
    public void shouldUpdateAndUnlockOneEntity() {
        EntityData entity = createAndPersistEntity(TEST_NAME, ENTITY_KEY_VALUES_1, ENTITY_SCHEMA_1,
                PARTITION_KEY_VALUES_1, PARTITION_SCHEMA_1, Instant.now().plus(2, ChronoUnit.HOURS));

        Entity updatedEntityData = entity.toEntity().toBuilder().unlock().build();

        LinkedList<EntityData> updatedEntities = new LinkedList<>(
                service.updateEntities(Collections.singletonList(updatedEntityData)));

        assertThat(updatedEntities).hasSize(1);
        EntityData updatedEntity = updatedEntities.getFirst();
        assertThat(updatedEntity.getId()).isEqualTo(entity.getId());
        assertThat(updatedEntity.getLockedUntilStamp()).isNull();
    }

    @Test
    public void shouldUpdateEntityMultipleTimes() {
        EntityData entity = createAndPersistEntity(TEST_NAME, ENTITY_KEY_VALUES_1, ENTITY_SCHEMA_1,
                PARTITION_KEY_VALUES_1, PARTITION_SCHEMA_1, Instant.now().plus(2, ChronoUnit.HOURS));

        Entity updatedEntityData = entity.toEntity().toBuilder().unlock().build();

        List<EntityData> updatedEntities = service.updateEntities(Collections.singletonList(updatedEntityData));
        entityManager.flush();
        assertThat(updatedEntities).hasSize(1);
        EntityData updatedEntity = updatedEntities.iterator().next();
        assertThat(updatedEntity.getId()).isEqualTo(entity.getId());
        assertThat(updatedEntity.getLockedUntilStamp()).isNull();
        assertThat(updatedEntity.getRecVersion()).isEqualTo(1L);

        Entity lockedEntity = updatedEntity.toEntity().toBuilder()
                .lockedUntilStamp(Instant.now().plus(2, ChronoUnit.HOURS)).build();

        List<EntityData> lastUpdatedEntities = service.updateEntities(Collections.singletonList(lockedEntity));
        entityManager.flush();
        assertThat(updatedEntities).hasSize(1);
        EntityData lastUpdatedEntity = lastUpdatedEntities.iterator().next();
        assertThat(lastUpdatedEntity.getId()).isEqualTo(lockedEntity.getId());
        assertThat(lastUpdatedEntity.getLockedUntilStamp()).isEqualTo(lockedEntity.getLockedUntilStamp());
        assertThat(lastUpdatedEntity.getRecVersion()).isEqualTo(2L);
    }

    @Test
    public void shouldCreateEntityWithNewKeyValues() {
        EntityData entity = createAndPersistEntity(TEST_NAME, ENTITY_KEY_VALUES_1, ENTITY_SCHEMA_1,
                PARTITION_KEY_VALUES_1, PARTITION_SCHEMA_1);

        Long systemId = entity.getPartition().getSystem().getId();

        Map<String, Object> newEntityKeysValues = new HashMap<>(ENTITY_KEY_VALUES_1);
        newEntityKeysValues.put(ENTITY_STRING_SCHEMA_KEY_1, "new_value");

        service.findOrCreateEntityFor(systemId, newEntityKeysValues, PARTITION_KEY_VALUES_1, ENTITY_SCHEMA_1.toString(),
                TEST_RECORD_TIME);

        String newEntityKeyValuesString = convertMapIntoAvroSchemaString(newEntityKeysValues,
                ENTITY_SCHEMA_1.toString());

        Optional<EntityData> optionalEntity = entityRepository.queryOne(
                toRSQL(Entities.suchThat().systemId().eq(systemId).and().keyValues()
                        .eq(newEntityKeyValuesString)), EntityData.class);

        assertThat(optionalEntity).isPresent();
        EntityData newEntity = optionalEntity.get();
        assertThat(newEntity.getKeyValues()).isEqualTo(newEntityKeyValuesString);
    }

    @Test
    public void shouldCreateEntityHistory() {
        EntityData entity = createAndPersistEntity(TEST_NAME, ENTITY_KEY_VALUES_1, ENTITY_SCHEMA_1,
                PARTITION_KEY_VALUES_1, PARTITION_SCHEMA_1);

        createAndPersistEntityHistory(entity, entity.getPartition(), createSchema(ENTITY_SCHEMA_1), TEST_RECORD_TIME);

        Map<String, Object> newPartitionKeyValues = new HashMap<>(PARTITION_KEY_VALUES_1);
        newPartitionKeyValues.put(PARTITION_STRING_SCHEMA_KEY_1, "new_value");

        Long systemId = entity.getPartition().getSystem().getId();

        EntityData entityWithChangedPartition = service
                .findOrCreateEntityFor(systemId, ENTITY_KEY_VALUES_1, newPartitionKeyValues,
                        getFullSchema(ENTITY_SCHEMA_1, PARTITION_SCHEMA_1).toString(), TEST_RECORD_TIME + 1);

        assertThat(entityWithChangedPartition.getId()).isEqualTo(entity.getId());
        assertThat(entityWithChangedPartition.getEntityHistories()).hasSize(1);
    }

    @Test
    public void shouldGetLatestHistoryFromExistingEntity() {
        Long systemId = createAndPersistSystemData(TEST_NAME, ENTITY_SCHEMA_1, PARTITION_SCHEMA_1, TIME_SCHEMA)
                .getId();

        EntityData entity1 = service.findOrCreateEntityFor(systemId, ENTITY_KEY_VALUES_1, PARTITION_KEY_VALUES_1,
                getFullSchema(ENTITY_SCHEMA_1, PARTITION_SCHEMA_1).toString(), TEST_RECORD_TIME);

        EntityData found1 = service.findOrCreateEntityFor(systemId, ENTITY_KEY_VALUES_1, PARTITION_KEY_VALUES_1,
                getFullSchema(ENTITY_SCHEMA_1, PARTITION_SCHEMA_1).toString(), TEST_RECORD_TIME);

        EntityData found2 = service.findOrCreateEntityFor(systemId, ENTITY_KEY_VALUES_1, PARTITION_KEY_VALUES_1,
                getFullSchema(ENTITY_SCHEMA_1, PARTITION_SCHEMA_1).toString(), TEST_RECORD_TIME + 1_000_000);

        assertThat(found1).isNotNull();
        assertThat(found2).isNotNull();
        assertThat(found1).isEqualTo(entity1);
        assertThat(found2).isEqualTo(found2);
        assertThat(found1.getEntityHistories()).hasSize(1);
        assertThat(found2.getEntityHistories()).hasSize(1);
    }

    @Test
    public void shouldGetHistoryForExistingEntityWithDifferentPartitionFromThePast() {
        Long systemId = createAndPersistSystemData(TEST_NAME, ENTITY_SCHEMA_1, PARTITION_SCHEMA_1, TIME_SCHEMA)
                .getId();

        Map<String, Object> newPartitionKeyValues = new HashMap<>(PARTITION_KEY_VALUES_1);
        newPartitionKeyValues.put(PARTITION_STRING_SCHEMA_KEY_1, "new_value");

        EntityData entity1 = service.findOrCreateEntityFor(systemId, ENTITY_KEY_VALUES_1, PARTITION_KEY_VALUES_1,
                getFullSchema(ENTITY_SCHEMA_1, PARTITION_SCHEMA_1).toString(), TEST_RECORD_TIME);
        //this creates a new historical entry with new partition
        service.findOrCreateEntityFor(systemId, ENTITY_KEY_VALUES_1, newPartitionKeyValues,
                getFullSchema(ENTITY_SCHEMA_1, PARTITION_SCHEMA_1).toString(), 100 * TEST_RECORD_TIME);
        EntityData found = service.findOrCreateEntityFor(systemId, ENTITY_KEY_VALUES_1, PARTITION_KEY_VALUES_1,
                getFullSchema(ENTITY_SCHEMA_1, PARTITION_SCHEMA_1).toString(), TEST_RECORD_TIME);

        assertThat(found).isNotNull();
        assertThat(found).isEqualTo(entity1);
        assertThat(found.getPartition().getKeyValues())
                .isEqualTo(convertMapIntoAvroSchemaString(newPartitionKeyValues, PARTITION_SCHEMA_1.toString()));
        assertThat(found.getEntityHistories()).hasSize(1);
        assertThat(found.getEntityHistories().first().getValidFromStamp())
                .isEqualTo(getInstantFromNanos(TEST_RECORD_TIME));
        assertThat(found.getEntityHistories().first().getValidToStamp())
                .isEqualTo(getInstantFromNanos(100 * TEST_RECORD_TIME));
        assertThat(found.getEntityHistories().first().getPartition().getKeyValues())
                .isEqualTo(convertMapIntoAvroSchemaString(PARTITION_KEY_VALUES_1, PARTITION_SCHEMA_1.toString()));
        assertThat(found.getEntityHistories().first().getSchema().getContent())
                .isEqualTo(getFullSchema(ENTITY_SCHEMA_1, PARTITION_SCHEMA_1).toString());
    }

    @Test
    public void shouldNotCreateHistoryForLateDataWithDifferentSchemaBeforeEntityCreationTime() {
        Long systemId = createAndPersistSystemData(TEST_NAME, ENTITY_SCHEMA_1, PARTITION_SCHEMA_1, TIME_SCHEMA)
                .getId();

        service.findOrCreateEntityFor(systemId, ENTITY_KEY_VALUES_1, PARTITION_KEY_VALUES_1,
                getFullSchema(ENTITY_SCHEMA_1, PARTITION_SCHEMA_1).toString(), 100 * TEST_RECORD_TIME);
        assertThrows(ConfigDataConflictException.class, () ->
                service.findOrCreateEntityFor(systemId, ENTITY_KEY_VALUES_1, PARTITION_KEY_VALUES_1, ENTITY_SCHEMA_2.toString(),
                TEST_RECORD_TIME));
    }

    @Test
    public void shouldThrowOnExtendFirstHistoryWhenExtendFromIsEqualToValidFromAndDifferentSchema() {
        Long systemId = createAndPersistSystemData(TEST_NAME, ENTITY_SCHEMA_1, PARTITION_SCHEMA_1, TIME_SCHEMA)
                .getId();
        long timestamp = 100 * TEST_RECORD_TIME;

        EntityData entity1 = service.findOrCreateEntityFor(systemId, ENTITY_KEY_VALUES_1, PARTITION_KEY_VALUES_1,
                getFullSchema(ENTITY_SCHEMA_1, PARTITION_SCHEMA_1).toString(), timestamp);

        try {
            service.extendEntityFirstHistoryDataFor(entity1.getId(), ENTITY_SCHEMA_2.toString(), timestamp);
        } catch (ConfigDataConflictException ex) {
            String message = ex.getMessage();
            assertThat(message).startsWith("Schema not matching for first history");
            return;
        }
        fail("Should throw exception when trying to extend history with different schema and same fromTime!");
    }

    @Test
    public void shouldReturnExistingFirstHistoryWhenExtendFromIsEqualToValidFrom() {
        Long systemId = createAndPersistSystemData(TEST_NAME, ENTITY_SCHEMA_1, PARTITION_SCHEMA_1, TIME_SCHEMA)
                .getId();
        long timestamp = 100 * TEST_RECORD_TIME;

        String entitySchema = getFullSchema(ENTITY_SCHEMA_1, PARTITION_SCHEMA_1).toString();

        EntityData entity1 = service.findOrCreateEntityFor(systemId, ENTITY_KEY_VALUES_1, PARTITION_KEY_VALUES_1,
                entitySchema, timestamp);

        EntityData entity2 = service.extendEntityFirstHistoryDataFor(entity1.getId(), entitySchema, timestamp);

        assertThat(entity1).isNotNull();
        assertThat(entity2).isNotNull();
        assertThat(entity2.getEntityHistories()).hasSize(1);
        assertThat(entity2.getEntityHistories().first()).isEqualTo(entity1.getEntityHistories().first());
    }

    @Test
    public void shouldThrowOnExistingFirstHistoryWhenExtendFromIsAfterValidFromAndDifferentSchema() {
        Long systemId = createAndPersistSystemData(TEST_NAME, ENTITY_SCHEMA_1, PARTITION_SCHEMA_1, TIME_SCHEMA)
                .getId();
        long timestamp = 100 * TEST_RECORD_TIME;

        String entitySchema = getFullSchema(ENTITY_SCHEMA_1, PARTITION_SCHEMA_1).toString();

        EntityData entity1 = service.findOrCreateEntityFor(systemId, ENTITY_KEY_VALUES_1, PARTITION_KEY_VALUES_1,
                entitySchema, timestamp);

        long afterTimestamp = timestamp + 1000;
        try {
            service.extendEntityFirstHistoryDataFor(entity1.getId(), ENTITY_SCHEMA_2.toString(), afterTimestamp);
        } catch (ConfigDataConflictException ex) {
            String message = ex.getMessage();
            assertThat(message).startsWith("Schema not matching for first history");
            return;
        }
        fail("Should throw exception when trying to extend history with different schema and after fromTime!");
    }

    @Test
    public void shouldReturnExistingFirstHistoryWhenExtendFromIsAfterValidFrom() {
        Long systemId = createAndPersistSystemData(TEST_NAME, ENTITY_SCHEMA_1, PARTITION_SCHEMA_1, TIME_SCHEMA)
                .getId();
        long timestamp = 100 * TEST_RECORD_TIME;

        String entitySchema = getFullSchema(ENTITY_SCHEMA_1, PARTITION_SCHEMA_1).toString();

        EntityData entity1 = service.findOrCreateEntityFor(systemId, ENTITY_KEY_VALUES_1, PARTITION_KEY_VALUES_1,
                entitySchema, timestamp);

        long afterTimestamp = timestamp + 1000;
        EntityData entity2 = service.extendEntityFirstHistoryDataFor(entity1.getId(), entitySchema, afterTimestamp);

        assertThat(entity1).isNotNull();
        assertThat(entity2).isNotNull();
        assertThat(entity2.getEntityHistories()).hasSize(1);
        assertThat(entity2.getEntityHistories().first()).isEqualTo(entity1.getEntityHistories().first());
    }


    @Test
    public void shouldExtendHistoryForLateDataWithTheSameSchemaBeforeEntityCreationTime() {
        Long systemId = createAndPersistSystemData(TEST_NAME, ENTITY_SCHEMA_1, PARTITION_SCHEMA_1, TIME_SCHEMA)
                .getId();

        EntityData entity1 = service.findOrCreateEntityFor(systemId, ENTITY_KEY_VALUES_1, PARTITION_KEY_VALUES_1,
                getFullSchema(ENTITY_SCHEMA_1, PARTITION_SCHEMA_1).toString(), 100 * TEST_RECORD_TIME);
        EntityData entity2 = service.findOrCreateEntityFor(systemId, ENTITY_KEY_VALUES_1, PARTITION_KEY_VALUES_1,
                getFullSchema(ENTITY_SCHEMA_1, PARTITION_SCHEMA_1).toString(), TEST_RECORD_TIME);

        assertThat(entity1).isNotNull();
        assertThat(entity2).isNotNull();
        assertThat(entity1).isEqualTo(entity2);
        assertThat(entity2.getEntityHistories()).hasSize(1);
        assertThat(entity2.getEntityHistories().first().getValidFromStamp())
                .isEqualTo(getInstantFromNanos(TEST_RECORD_TIME));
        assertThat(entity2.getEntityHistories().first().getValidToStamp()).isNull();
    }

    @Test
    public void shouldExtendFirstHistoryForMigration() {
        Long systemId = createAndPersistSystemData(TEST_NAME, ENTITY_SCHEMA_1, PARTITION_SCHEMA_1, TIME_SCHEMA)
                .getId();
        long timestamp = 100 * TEST_RECORD_TIME;

        EntityData entity1 = service.findOrCreateEntityFor(systemId, ENTITY_KEY_VALUES_1, PARTITION_KEY_VALUES_1,
                getFullSchema(ENTITY_SCHEMA_1, PARTITION_SCHEMA_1).toString(), timestamp);

        EntityData entity2 = service.extendEntityFirstHistoryDataFor(entity1.getId(), ENTITY_SCHEMA_2.toString(), 0);

        assertThat(entity1).isNotNull();
        assertThat(entity2).isNotNull();
        assertThat(entity2.getEntityHistories()).hasSize(1);
        assertThat(entity2.getEntityHistories().first().getValidFromStamp()).isEqualTo(getInstantFromNanos(0));
        assertThat(entity2.getEntityHistories().first().getValidToStamp()).isEqualTo(getInstantFromNanos(timestamp));
    }

    @Test
    public void shouldAcceptEntityWithPartitionNotPresentedInTheHistory() {
        Long systemId = createAndPersistSystemData(TEST_NAME, ENTITY_SCHEMA_1, PARTITION_SCHEMA_1, TIME_SCHEMA)
                .getId();

        Map<String, Object> newPartitionKeyValues = new HashMap<>(PARTITION_KEY_VALUES_1);
        newPartitionKeyValues.put(PARTITION_STRING_SCHEMA_KEY_1, "new_value");

        service.findOrCreateEntityFor(systemId, ENTITY_KEY_VALUES_1, PARTITION_KEY_VALUES_1,
                getFullSchema(ENTITY_SCHEMA_1, PARTITION_SCHEMA_1).toString(), TEST_RECORD_TIME);
        EntityData entity = service.findOrCreateEntityFor(systemId, ENTITY_KEY_VALUES_1, newPartitionKeyValues,
                getFullSchema(ENTITY_SCHEMA_1, PARTITION_SCHEMA_1).toString(), 100 * TEST_RECORD_TIME);

        assertThat(entity).isNotNull();
        assertThat(entity.getEntityHistories()).hasSize(1);
    }

    @Test
    public void shouldFindEntityHistForAdvancingTimeWindow() {
        //given
        Long systemId = createAndPersistSystemData(TEST_NAME, ENTITY_SCHEMA_1, PARTITION_SCHEMA_1, TIME_SCHEMA)
                .getId();

        Map<String, Object> newPartitionKeyValues1 = new HashMap<>(PARTITION_KEY_VALUES_1);
        newPartitionKeyValues1.put(PARTITION_STRING_SCHEMA_KEY_1, "value1");
        Map<String, Object> newPartitionKeyValues2 = new HashMap<>(PARTITION_KEY_VALUES_1);
        newPartitionKeyValues2.put(PARTITION_STRING_SCHEMA_KEY_1, "value2");
        Map<String, Object> newPartitionKeyValues3 = new HashMap<>(PARTITION_KEY_VALUES_1);
        newPartitionKeyValues3.put(PARTITION_STRING_SCHEMA_KEY_1, "value3");
        Map<String, Object> newPartitionKeyValues4 = new HashMap<>(PARTITION_KEY_VALUES_1);
        newPartitionKeyValues4.put(PARTITION_STRING_SCHEMA_KEY_1, "value4");
        Map<String, Object> newPartitionKeyValues5 = new HashMap<>(PARTITION_KEY_VALUES_1);
        newPartitionKeyValues5.put(PARTITION_STRING_SCHEMA_KEY_1, "value5");

        EntityData entity = service.findOrCreateEntityFor(systemId, ENTITY_KEY_VALUES_1, newPartitionKeyValues1,
                getFullSchema(ENTITY_SCHEMA_1, PARTITION_SCHEMA_1).toString(), TEST_RECORD_TIME + 100);
        checkEntityHistory(entity, TEST_RECORD_TIME + 100);

        entity = service.findOrCreateEntityFor(systemId, ENTITY_KEY_VALUES_1, newPartitionKeyValues2,
                getFullSchema(ENTITY_SCHEMA_1, PARTITION_SCHEMA_1).toString(), TEST_RECORD_TIME + 200);
        checkEntityHistory(entity, TEST_RECORD_TIME + 200);

        entity = service.findOrCreateEntityFor(systemId, ENTITY_KEY_VALUES_1, newPartitionKeyValues3,
                getFullSchema(ENTITY_SCHEMA_1, PARTITION_SCHEMA_1).toString(), TEST_RECORD_TIME + 300);
        checkEntityHistory(entity, TEST_RECORD_TIME + 300);

        entity = service.findOrCreateEntityFor(systemId, ENTITY_KEY_VALUES_1, newPartitionKeyValues4,
                getFullSchema(ENTITY_SCHEMA_1, PARTITION_SCHEMA_1).toString(), TEST_RECORD_TIME + 400);
        checkEntityHistory(entity, TEST_RECORD_TIME + 400);

        entity = service.findOrCreateEntityFor(systemId, ENTITY_KEY_VALUES_1, newPartitionKeyValues5,
                getFullSchema(ENTITY_SCHEMA_1, PARTITION_SCHEMA_1).toString(), TEST_RECORD_TIME + 500);
        checkEntityHistory(entity, TEST_RECORD_TIME + 500);

        //when
        EntityData entityWithHistForTimeWindow = service.findAll(
                toRSQL(Entities.suchThat().systemId().eq(systemId).and().keyValues()
                        .eq(entity.getPartition().getSystem().toSystemSpec(), ENTITY_KEY_VALUES_1)),
                TEST_RECORD_TIME + 100, TEST_RECORD_TIME + 300).get(0);
        SortedSet<EntityHistoryData> filteredHistory = entityWithHistForTimeWindow.getEntityHistories();

        //then
        assertThat(filteredHistory).hasSize(3);

        EntityHistoryData[] entityHistsArray = filteredHistory.toArray(new EntityHistoryData[0]);
        assertThat(entityHistsArray[0].getPartition().getKeyValues())
                .isEqualTo(convertMapIntoAvroSchemaString(newPartitionKeyValues3, PARTITION_SCHEMA_1.toString()));
        assertThat(entityHistsArray[1].getPartition().getKeyValues())
                .isEqualTo(convertMapIntoAvroSchemaString(newPartitionKeyValues2, PARTITION_SCHEMA_1.toString()));
        assertThat(entityHistsArray[2].getPartition().getKeyValues())
                .isEqualTo(convertMapIntoAvroSchemaString(newPartitionKeyValues1, PARTITION_SCHEMA_1.toString()));
    }

    @Test
    public void shouldFetchEntityWithoutHistoryInThatTimeWindow() {
        //given
        EntityData entity = createAndPersistEntity(TEST_NAME, ENTITY_KEY_VALUES_1, ENTITY_SCHEMA_1,
                PARTITION_KEY_VALUES_1, PARTITION_SCHEMA_1);

        //when
        EntityData entityWithoutHistory = service.findAll(
                toRSQL(Entities.suchThat().systemId().eq(entity.getPartition().getSystem().getId()).and().keyValues()
                        .eq(entity.getPartition().getSystem().toSystemSpec(), ENTITY_KEY_VALUES_1)), TEST_RECORD_TIME,
                TEST_RECORD_TIME).get(0);

        //then
        assertThat(entityWithoutHistory).isNotNull();
        assertThat(entityWithoutHistory.getEntityHistories()).isEmpty();
    }

    @Test
    public void shouldThrowExceptionWhenTryingToRegexWithNull() {
        assertThrows(NullPointerException.class, () -> service.findAll(null));
    }

    @Test
    public void shouldFindEntityWithGivenRegex() {
        EntityData entity = createAndPersistEntity(TEST_NAME, ENTITY_KEY_VALUES_1, ENTITY_SCHEMA_1,
                PARTITION_KEY_VALUES_1, PARTITION_SCHEMA_1);
        createAndPersistEntityHistory(entity, entity.getPartition(), createSchema(ENTITY_SCHEMA_1), TEST_RECORD_TIME);

        Long systemId = entity.getPartition().getSystem().getId();

        EntityData newEntity = service.findOrCreateEntityFor(systemId, ENTITY_KEY_VALUES_1, PARTITION_KEY_VALUES_1,
                getFullSchema(ENTITY_SCHEMA_1, PARTITION_SCHEMA_1).toString(), TEST_RECORD_TIME + 1);

        String regex = "%trin%";
        List<EntityData> foundKeys = entityRepository
                .queryAll(toRSQL(Entities.suchThat().keyValues().like(regex)), EntityData.class);

        assertThat(foundKeys).containsExactly(newEntity);
    }

    @Test
    public void shouldFindEntityById() {
        EntityData entity = createAndPersistEntity(TEST_NAME, ENTITY_KEY_VALUES_1, ENTITY_SCHEMA_1,
                PARTITION_KEY_VALUES_1, PARTITION_SCHEMA_1);

        createAndPersistEntityHistory(entity, entity.getPartition(), createSchema(ENTITY_SCHEMA_1), TEST_RECORD_TIME);

        EntityData fetchedEntity = service.findAll(toRSQL(Entities.suchThat().id().eq(entity.getId()))).get(0);

        assertThat(fetchedEntity).isNotNull();
        assertThat(fetchedEntity.getEntityHistories()).hasSize(1);
    }

    @Test
    public void shouldFindEntityBySystemId() {
        EntityData entity = createAndPersistEntity(TEST_NAME, ENTITY_KEY_VALUES_1, ENTITY_SCHEMA_1,
                PARTITION_KEY_VALUES_1, PARTITION_SCHEMA_1);

        createAndPersistEntityHistory(entity, entity.getPartition(), createSchema(ENTITY_SCHEMA_1), TEST_RECORD_TIME);

        EntityData fetchedEntity = service
                .findAll(toRSQL(Entities.suchThat().systemId().eq(entity.getPartition().getSystem().getId()))).get(0);

        assertThat(fetchedEntity).isNotNull();
        assertThat(fetchedEntity.getEntityHistories()).hasSize(1);
    }

    @Test
    public void shouldReturnEmptySetWithSearchByIdNotFound() {
        assertThat(service.findAll(toRSQL(Entities.suchThat().id().eq(12345L)))).hasSize(0);

    }

    @Test
    public void shouldFindAllEntitiesById() {
        EntityData entity1 = createAndPersistEntity(TEST_NAME, ENTITY_KEY_VALUES_1, ENTITY_SCHEMA_1,
                PARTITION_KEY_VALUES_1, PARTITION_SCHEMA_1);

        createAndPersistEntityHistory(entity1, entity1.getPartition(), createSchema(ENTITY_SCHEMA_1), TEST_RECORD_TIME);

        EntityData entity2 = createAndPersistEntity(TEST_NAME, ENTITY_KEY_VALUES_2, ENTITY_SCHEMA_2,
                PARTITION_KEY_VALUES_2, PARTITION_SCHEMA_2);

        createAndPersistEntityHistory(entity2, entity2.getPartition(), createSchema(ENTITY_SCHEMA_2), TEST_RECORD_TIME);

        List<EntityData> fetchedEntities = service
                .findAll(toRSQL(Entities.suchThat().id().in(entity1.getId(), entity2.getId())));

        assertThat(fetchedEntities).hasSize(2);
        assertThat(fetchedEntities).doesNotContainNull();

        List<Long> fetchedIds = fetchedEntities.stream().map(EntityData::getId).collect(toList());
        assertThat(fetchedIds).contains(entity1.getId(), entity2.getId());

        LinkedList<SortedSet<EntityHistoryData>> fetchedHistories = fetchedEntities.stream()
                .map(EntityData::getEntityHistories).collect(toCollection(LinkedList::new));

        assertThat(fetchedHistories.getFirst()).hasSize(1);
        assertThat(fetchedHistories.getLast()).hasSize(1);
    }

    @Test
    public void shouldFetchEmptyListWhenTryingToFindAllNonExistingEntitiesById() {
        List<EntityData> fetchedEntities = service.findAll(toRSQL(Entities.suchThat().id().in(12345L, 123456L)));
        assertThat(fetchedEntities).hasSize(0);
    }

    @Test
    public void shouldThrowExceptionWhenGivenNullListOfIds() {
        assertThrows(NullPointerException.class, () -> service.findAll(null, 1, 2));
    }

    @Test
    public void shouldCreateEntityWithoutHistory() {
        // given
        SystemSpecData system = createAndPersistSystemData(TEST_NAME, ENTITY_SCHEMA_1, PARTITION_SCHEMA_1,
                TIME_SCHEMA);
        EntityData entity = service.createEntity(system.getId(), ENTITY_KEY_VALUES_1, PARTITION_KEY_VALUES_1);
        // when
        Optional<EntityData> optionalEntity = entityRepository.findById(entity.getId());

        // then
        assertThat(optionalEntity).isPresent();
        EntityData foundEntity = optionalEntity.get();
        assertThat(foundEntity).isNotNull();
        assertThat(foundEntity.getEntityHistories()).isEmpty();
        assertThat(foundEntity.getPartition().getKeyValues())
                .isEqualTo(convertMapIntoAvroSchemaString(PARTITION_KEY_VALUES_1, system.getPartitionKeyDefs()));
    }

    @Test
    public void shouldNotCreateEntityWithoutHistoryTwice() {// given
        SystemSpecData system = createAndPersistSystemData(TEST_NAME, ENTITY_SCHEMA_1, PARTITION_SCHEMA_1,
                TIME_SCHEMA);
        // when
        EntityData entity = service.createEntity(system.getId(), ENTITY_KEY_VALUES_1, PARTITION_KEY_VALUES_1);

        // then
        Optional<EntityData> optionalEntity = entityRepository.findById(entity.getId());
        assertThat(optionalEntity).isPresent();

        // when
        assertThrows(ConfigDataConflictException.class, () -> service.createEntity(system.getId(), ENTITY_KEY_VALUES_1, PARTITION_KEY_VALUES_1));
        entityManager.flush();
    }

    @Test
    public void shouldCreateEntityWithoutHistoryAndAddNewOneOnData() {
        Function<SystemSpecData, EntityData> entityMaker = system -> service
                .createEntity(system.getId(), ENTITY_KEY_VALUES_1, PARTITION_KEY_VALUES_1);
        this.shouldCreateEntityWithoutHistoryAndModifyItBy(entityMaker, (entityId, systemId, schema) -> service
                .findOrCreateEntityFor(systemId, ENTITY_KEY_VALUES_1, PARTITION_KEY_VALUES_1, schema,
                        TEST_RECORD_TIME));
    }

    @Test
    public void shouldCreateEntityWithoutHistoryAndExtendIt() {
        Function<SystemSpecData, EntityData> entityMaker = system -> service
                .createEntity(system.getId(), ENTITY_KEY_VALUES_1, PARTITION_KEY_VALUES_1);
        this.shouldCreateEntityWithoutHistoryAndModifyItBy(entityMaker, (entityId, systemId, schema) -> service
                .extendEntityFirstHistoryDataFor(entityId, schema, TEST_RECORD_TIME));
    }

    @Test
    public void shouldCreateEntitiesWithDifferentSystems() {
        // given
        SystemSpecData system1 = createAndPersistSystemData(SYSTEM_NAME, ENTITY_SCHEMA_1, PARTITION_SCHEMA_1,
                TIME_SCHEMA);
        SystemSpecData system2 = createAndPersistSystemData(SYSTEM_NAME2, ENTITY_SCHEMA_2, PARTITION_SCHEMA_2,
                TIME_SCHEMA);
        CreateEntityRequest request1 = CreateEntityRequest.builder().systemId(system1.getId())
                .entityKeyValues(ENTITY_KEY_VALUES_1).partitionKeyValues(PARTITION_KEY_VALUES_1).build();
        CreateEntityRequest request2 = CreateEntityRequest.builder().systemId(system2.getId())
                .entityKeyValues(ENTITY_KEY_VALUES_2).partitionKeyValues(PARTITION_KEY_VALUES_2).build();
        Set<EntityData> entities = service.createEntities(Sets.newHashSet(request1, request2));
        // when
        Set<EntityData> foundEntities = entityRepository.findAllByIdIn(entities.stream().map(EntityData::getId).collect(
                Collectors.toSet()));
        // then
        assertThat(foundEntities.size()).isEqualTo(2);
        assertThat(foundEntities).extracting(EntityData::getEntityHistories).allMatch(Set::isEmpty);
        assertThat(foundEntities).extracting(EntityData::getKeyValues).containsAll(Sets.newHashSet(
                convertMapIntoAvroSchemaString(ENTITY_KEY_VALUES_1, system1.getEntityKeyDefs()),
                convertMapIntoAvroSchemaString(ENTITY_KEY_VALUES_2, system2.getEntityKeyDefs())
        ));
        assertThat(foundEntities).extracting(data -> data.getPartition().getKeyValues()).containsAll(Sets.newHashSet(
                convertMapIntoAvroSchemaString(PARTITION_KEY_VALUES_1, system1.getPartitionKeyDefs()),
                convertMapIntoAvroSchemaString(PARTITION_KEY_VALUES_2, system2.getPartitionKeyDefs())
        ));
    }

    @Test
    public void shouldCreateEntitiesWithSameSystem() {
        // given
        SystemSpecData system = createAndPersistSystemData(SYSTEM_NAME, ENTITY_SCHEMA_1, PARTITION_SCHEMA_1,
                TIME_SCHEMA);
        CreateEntityRequest request1 = CreateEntityRequest.builder().systemId(system.getId())
                .entityKeyValues(ENTITY_KEY_VALUES_1).partitionKeyValues(PARTITION_KEY_VALUES_1).build();
        CreateEntityRequest request2 = CreateEntityRequest.builder().systemId(system.getId())
                .entityKeyValues(ENTITY_KEY_VALUES_1_2).partitionKeyValues(PARTITION_KEY_VALUES_1).build();
        Set<EntityData> entities = service.createEntities(Sets.newHashSet(request1, request2));
        // when
        Set<EntityData> foundEntities = entityRepository.findAllByIdIn(entities.stream().map(EntityData::getId).collect(
                Collectors.toSet()));
        // then
        assertThat(foundEntities.size()).isEqualTo(2);
        assertThat(foundEntities).extracting(EntityData::getEntityHistories).allMatch(Set::isEmpty);
        assertThat(foundEntities).extracting(EntityData::getKeyValues).containsAll(Sets.newHashSet(
                convertMapIntoAvroSchemaString(ENTITY_KEY_VALUES_1, system.getEntityKeyDefs()),
                convertMapIntoAvroSchemaString(ENTITY_KEY_VALUES_1_2, system.getEntityKeyDefs())
        ));
        assertThat(foundEntities).extracting(data -> data.getPartition().getKeyValues()).allMatch(kv ->
                kv.equals(convertMapIntoAvroSchemaString(PARTITION_KEY_VALUES_1, system.getPartitionKeyDefs())
                ));
    }

    @Test
    public void shouldCreateEntitiesWithSameSystemAndDifferentPartitions() {
        // given
        SystemSpecData system = createAndPersistSystemData(SYSTEM_NAME, ENTITY_SCHEMA_1, PARTITION_SCHEMA_1,
                TIME_SCHEMA);
        CreateEntityRequest request1 = CreateEntityRequest.builder().systemId(system.getId())
                .entityKeyValues(ENTITY_KEY_VALUES_1).partitionKeyValues(PARTITION_KEY_VALUES_1).build();
        CreateEntityRequest request2 = CreateEntityRequest.builder().systemId(system.getId())
                .entityKeyValues(ENTITY_KEY_VALUES_1_2).partitionKeyValues(PARTITION_KEY_VALUES_1_2).build();
        Set<EntityData> entities = service.createEntities(Sets.newHashSet(request1, request2));
        // when
        Set<EntityData> foundEntities = entityRepository.findAllByIdIn(entities.stream().map(EntityData::getId).collect(
                Collectors.toSet()));
        // then
        assertThat(foundEntities.size()).isEqualTo(2);
        assertThat(foundEntities).extracting(EntityData::getEntityHistories).allMatch(Set::isEmpty);
        assertThat(foundEntities).extracting(EntityData::getKeyValues).containsAll(Sets.newHashSet(
                convertMapIntoAvroSchemaString(ENTITY_KEY_VALUES_1, system.getEntityKeyDefs()),
                convertMapIntoAvroSchemaString(ENTITY_KEY_VALUES_1_2, system.getEntityKeyDefs())
        ));
        assertThat(foundEntities).extracting(data -> data.getPartition().getKeyValues()).containsAll(Sets.newHashSet(
                convertMapIntoAvroSchemaString(PARTITION_KEY_VALUES_1, system.getPartitionKeyDefs()),
                convertMapIntoAvroSchemaString(PARTITION_KEY_VALUES_1_2, system.getPartitionKeyDefs())
        ));
    }

    @Test
    public void shouldNotCreateEntitiesTwice() {
        // given
        SystemSpecData system1 = createAndPersistSystemData(SYSTEM_NAME, ENTITY_SCHEMA_1, PARTITION_SCHEMA_1,
                TIME_SCHEMA);
        SystemSpecData system2 = createAndPersistSystemData(SYSTEM_NAME2, ENTITY_SCHEMA_2, PARTITION_SCHEMA_2,
                TIME_SCHEMA);
        CreateEntityRequest request1 = CreateEntityRequest.builder().systemId(system1.getId())
                .entityKeyValues(ENTITY_KEY_VALUES_1).partitionKeyValues(PARTITION_KEY_VALUES_1).build();
        CreateEntityRequest request2 = CreateEntityRequest.builder().systemId(system2.getId())
                .entityKeyValues(ENTITY_KEY_VALUES_2).partitionKeyValues(PARTITION_KEY_VALUES_2).build();
        //when
        EntityData existingEntity = service.createEntity(system1.getId(), ENTITY_KEY_VALUES_1, PARTITION_KEY_VALUES_1);
        //then
        Optional<EntityData> optionalEntity = entityRepository.findById(existingEntity.getId());
        assertThat(optionalEntity).isPresent();
        // when
        assertThrows(ConfigDataConflictException.class, () -> service.createEntities(Sets.newHashSet(request1, request2)));
    }

    @Test
    public void shouldThrowExceptionOnCreateEntitiesWhenInputIsNull() {
        //when
        assertThrows(NullPointerException.class, () -> service.createEntities(null));
    }

    @Test
    public void shouldReturnEmptySetOnCreateEntitiesWhenInputIsEmpty() {
        //when
        Set<EntityData> entities = service.createEntities(Collections.emptySet());
        assertThat(entities).isNotNull();
        assertThat(entities).isEmpty();
    }

    private void shouldCreateEntityWithoutHistoryAndModifyItBy(Function<SystemSpecData, EntityData> entityMaker,
            TriConsumer<Long, Long, String> extender) {
        // given
        SystemSpecData system = createAndPersistSystemData(TEST_NAME, ENTITY_SCHEMA_1, PARTITION_SCHEMA_1,
                TIME_SCHEMA);
        // when
        EntityData entity = entityMaker.apply(system);

        // then
        Optional<EntityData> optionalEntity = entityRepository.findById(entity.getId());
        assertThat(optionalEntity).isPresent();
        EntityData foundEntity = optionalEntity.get();
        assertThat(foundEntity.getEntityHistories()).isEmpty();

        // given
        String schema = getFullSchema(ENTITY_SCHEMA_1, PARTITION_SCHEMA_1).toString();
        // when
        extender.accept(foundEntity.getId(), system.getId(), schema);
        // then
        optionalEntity = entityRepository.findById(entity.getId());
        assertThat(optionalEntity).isPresent();
        foundEntity = optionalEntity.get();
        assertThat(foundEntity.getEntityHistories()).isNotEmpty();
        EntityHistoryData firstHistory = foundEntity.getEntityHistories().first();
        assertThat(firstHistory.getSchema().getContent()).isEqualTo(schema);
        assertThat(firstHistory.getValidToStamp()).isNull();
        assertThat(firstHistory.getValidFromStamp()).isEqualTo(getInstantFromNanos(TEST_RECORD_TIME));
    }

    private void checkEntityHistory(EntityData entity, long start) {
        assertThat(entity.getEntityHistories()).hasSize(1);
        assertThat(entity.getEntityHistories().first().getValidFromStamp()).isEqualTo(getInstantFromNanos(start));
        assertThat(entity.getEntityHistories().first().getValidToStamp()).isNull();
    }

    private interface TriConsumer<T, U, R> {
        void accept(T t, U u, R r);
    }
}
