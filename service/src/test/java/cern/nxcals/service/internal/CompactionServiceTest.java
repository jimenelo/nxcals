/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.internal;

import cern.nxcals.api.domain.TimeEntityPartitionType;
import cern.nxcals.api.utils.TimeUtils;
import cern.nxcals.common.domain.DataProcessingJob;
import cern.nxcals.common.domain.DataProcessingJob.JobType;
import cern.nxcals.common.utils.HdfsFileUtils;
import cern.nxcals.service.config.CompactionProperties;
import cern.nxcals.service.config.DataLocationProperties;
import cern.nxcals.service.config.HdfsPathSearchProperties;
import cern.nxcals.service.domain.PartitionResourceHistoryData;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.net.URI;
import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringJoiner;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class CompactionServiceTest {
    private static final String STAGING_DIR = "staging";
    private static final String RESTAGING_DIR = "restaging";
    private static final String DATA_DIR = "data";
    private static final String HDFS_ROOT = "../fake_hdfs/compaction-service-hdfs/";
    private static final long COMPACTION_MARGIN = 24L;
    private static FileSystem fs;

    @Mock
    private PartitionResourceService partitionResourceService;
    @Mock
    private PartitionResourceHistoryService partitionResourceHistoryService;
    @Mock
    private CompactionProperties compactionConfig;
    @Mock
    private DataLocationProperties locationConfig;
    @Mock
    private Clock clock;

    private CompactionService service;

    @Mock
    private PartitionResourceHistoryData prhd;

    @BeforeAll
    public static void preInit() throws IOException {
        fs = FileSystem.getLocal(new Configuration(false));
        fs.setWorkingDirectory(new Path(HDFS_ROOT));
    }

    @BeforeEach
    public void init() throws IOException {
        // exactly one day before
        when(compactionConfig.getTolerancePeriodHours()).thenReturn(COMPACTION_MARGIN);
        // blocking functionality as there is no reliable way to test it (it depends on the file modification timestamp)
        when(compactionConfig.getMinFileModificationPeriodHours()).thenReturn((long) Integer.MIN_VALUE);
        when(compactionConfig.getMaxPartitionSize()).thenReturn((long) Integer.MAX_VALUE);
        when(compactionConfig.getSortThreshold()).thenReturn((long) Integer.MAX_VALUE);
        when(compactionConfig.getAvroParquetSizeRatio()).thenReturn(1);
        when(locationConfig.getHdfsDataDir()).thenReturn(DATA_DIR);
        when(locationConfig.getHdfsStagingDir()).thenReturn(STAGING_DIR);
        when(locationConfig.getHdfsRestagingDir()).thenReturn(RESTAGING_DIR);
        when(clock.instant()).thenReturn(Instant.now());


        service = new CompactionService(null,
                partitionResourceService, partitionResourceHistoryService,
                fs, clock, locationConfig, compactionConfig);
    }

    private void mockPartitionResource() {
        //mocking partition resource calls
        when(partitionResourceHistoryService.findOrCreatePartitionResourceInfoFor(argThat((v) -> true), (argThat((v) -> true)))).thenReturn(prhd);
        when(prhd.getInformation()).thenReturn(TimeEntityPartitionType.of(1, 1).toString());
    }

    @Test
    public void shouldFindAllJobsByRequestedType() throws IOException {
        mockPartitionResource();
        List<DataProcessingJob> jobs = service.getDataProcessJobs(100, JobType.ADAPTIVE_COMPACT);
        assertEquals(3, jobs.size());
        assertEquals(JobType.ADAPTIVE_COMPACT, jobs.iterator().next().getType());
    }

    @Test
    public void shouldFindAllJobsByRequestedTypeForOldCompact() throws IOException {
        //Path checker set to true for all
        service = new CompactionService(p -> true,
                partitionResourceService, partitionResourceHistoryService,
                fs, clock, locationConfig, compactionConfig);
        List<DataProcessingJob> jobs = service.getDataProcessJobs(100, JobType.COMPACT);
        assertEquals(4, jobs.size());
        assertEquals(JobType.COMPACT, jobs.iterator().next().getType());
    }

    @Test
    public void shouldFindOldCompact() throws IOException {
        //One directory in staging exists where we have already data for (simulating old data)
        List<DataProcessingJob> jobs = service.getDataProcessJobs(100, JobType.COMPACT);
        assertEquals(1, jobs.size());
    }


    @Test
    public void shouldFindJobsWithLimit() throws IOException {
        mockPartitionResource();
        List<DataProcessingJob> jobs = service.getDataProcessJobs(1, JobType.ADAPTIVE_COMPACT);
        assertEquals(1, jobs.size());
    }

    @Test
    public void shouldIgnoreNonDataFiles() throws IOException {
        mockPartitionResource();
        List<DataProcessingJob> jobs = service.getDataProcessJobs(100, JobType.ADAPTIVE_COMPACT);

        assertEquals(3, jobs.size());
        for (DataProcessingJob job : jobs) {
            for (URI file : job.getFiles()) {
                assertTrue(file.getPath().endsWith("avro") || file.getPath().endsWith("parquet"));
            }
        }
    }

    @Test
    public void shouldFindDailyAndHourlyBuckets() throws IOException {
        mockPartitionResource();
        List<DataProcessingJob> jobs = service.getDataProcessJobs(100, JobType.ADAPTIVE_COMPACT);

        Map<String, List<URI>> jobMap = jobs.stream()
                .collect(Collectors.toMap(t -> t.getDestinationDir().toString(), DataProcessingJob::getFiles));

        assertEquals(6, jobMap.get(DATA_DIR + "/2/100/200/2018/1/1").size());
        assertEquals(1, jobMap.get(DATA_DIR + "/2/300/300/2018/1/1").size());
        assertEquals(2, jobMap.get(DATA_DIR + "/2/300/300/2018/1/2").size());
    }

    @Test
    public void shouldSkipEmptyFiles() throws IOException {
        mockPartitionResource();
        List<DataProcessingJob> jobs = service.getDataProcessJobs(100, JobType.ADAPTIVE_COMPACT);

        Map<String, List<URI>> jobMap = jobs.stream()
                .collect(Collectors.toMap(t -> t.getDestinationDir().toString(), DataProcessingJob::getFiles));

        assertEquals(6, jobMap.get(DATA_DIR + "/2/100/200/2018/1/1").size());
    }

    @Test
    public void shouldSkipIfAfterCompactionMargin() throws IOException {
        mockPartitionResource();
        Instant now = TimeUtils.getInstantFromString("2018-01-02 01:01:00.000");
        Instant before = now.minus(Duration.ofHours(COMPACTION_MARGIN));

        String beforeString = HdfsFileUtils
                .expandDateToNestedPaths(TimeUtils.getStringFromInstant(before).split(" ")[0]);

        when(clock.instant()).thenReturn(now);

        List<DataProcessingJob> jobs = service.getDataProcessJobs(100, JobType.ADAPTIVE_COMPACT);

        List<String> collect = jobs.stream()
                .map(DataProcessingJob::getDestinationDir)
                .map(URI::getPath)
                .collect(Collectors.toList());

        for (String path : collect) {
            String date = path.substring(path.indexOf("2018"));
            assertTrue(beforeString.compareTo(date) <= 0);
        }
    }

    @Test
    public void shouldFindAnyFilesWhenNoSearchPropertiesSpecified() {
        String workingDir = fs.getWorkingDirectory().toString();

        when(compactionConfig.getFileSearchProperties()).thenReturn(new EnumMap<>(DataProcessingJob.JobType.class));
        this.service = new CompactionService(null, partitionResourceService, partitionResourceHistoryService,
                fs, clock, locationConfig, compactionConfig);

        List<DataProcessingJob> restageJobs = service.getDataProcessJobs(50, JobType.RESTAGE);

        Set<String> dataDirs = restageJobs.stream().flatMap(j -> j.getFiles().stream())
                .map(f -> new Path(f).getParent().getParent().toString())
                .collect(Collectors.toSet());

        assertEquals(5, dataDirs.size());
        assertTrue(dataDirs.contains(createPath(workingDir, RESTAGING_DIR, "TOPIC1/1/200/1500/2018-01-20")));
        assertTrue(dataDirs.contains(createPath(workingDir, RESTAGING_DIR, "TOPIC1/1/100/1000/2018-01-20")));
        assertTrue(dataDirs.contains(createPath(workingDir, RESTAGING_DIR, "TOPIC1/1/100/1000/2018-01-01")));
        assertTrue(dataDirs.contains(createPath(workingDir, RESTAGING_DIR, "TOPIC2/2/100/2000/2018-01-01")));
        assertTrue(dataDirs.contains(createPath(workingDir, RESTAGING_DIR, "TOPIC2/3/300/3000/2018-03-01")));
    }

    @Test
    public void shouldFindAnyFilesWhenSearchPropertiesSpecifiedForOtherJobTypes() {
        String workingDir = fs.getWorkingDirectory().toString();

        HdfsPathSearchProperties searchProperties = new HdfsPathSearchProperties();
        searchProperties.setTopic("TOPIC1");
        Map<JobType, HdfsPathSearchProperties> searchPropertiesMap = new EnumMap<>(JobType.class);
        searchPropertiesMap.put(JobType.COMPACT, searchProperties);

        when(compactionConfig.getFileSearchProperties()).thenReturn(searchPropertiesMap);
        this.service = new CompactionService(null, partitionResourceService, partitionResourceHistoryService,
                fs, clock, locationConfig, compactionConfig);

        List<DataProcessingJob> restageJobs = service.getDataProcessJobs(50, JobType.RESTAGE);

        Set<String> dataDirs = restageJobs.stream().flatMap(j -> j.getFiles().stream())
                .map(f -> new Path(f).getParent().getParent().toString())
                .collect(Collectors.toSet());

        assertEquals(5, dataDirs.size());
        assertTrue(dataDirs.contains(createPath(workingDir, RESTAGING_DIR, "TOPIC1/1/200/1500/2018-01-20")));
        assertTrue(dataDirs.contains(createPath(workingDir, RESTAGING_DIR, "TOPIC1/1/100/1000/2018-01-20")));
        assertTrue(dataDirs.contains(createPath(workingDir, RESTAGING_DIR, "TOPIC1/1/100/1000/2018-01-01")));
        assertTrue(dataDirs.contains(createPath(workingDir, RESTAGING_DIR, "TOPIC2/2/100/2000/2018-01-01")));
        assertTrue(dataDirs.contains(createPath(workingDir, RESTAGING_DIR, "TOPIC2/3/300/3000/2018-03-01")));
    }

    @Test
    public void shouldFindFilesBasedOnProvidedSearchProperties() {
        String workingDir = fs.getWorkingDirectory().toString();

        HdfsPathSearchProperties searchProperties = new HdfsPathSearchProperties();
        searchProperties.setSystem("1");
        searchProperties.setPartition("200");
        Map<JobType, HdfsPathSearchProperties> searchPropertiesMap = new EnumMap<>(JobType.class);
        searchPropertiesMap.put(JobType.RESTAGE, searchProperties);

        when(compactionConfig.getFileSearchProperties()).thenReturn(searchPropertiesMap);
        this.service = new CompactionService(null, partitionResourceService, partitionResourceHistoryService,
                fs, clock, locationConfig, compactionConfig);

        List<DataProcessingJob> restageJobs = service.getDataProcessJobs(50, JobType.RESTAGE);

        Set<String> dataDirs = restageJobs.stream().flatMap(j -> j.getFiles().stream())
                .map(f -> new Path(f).getParent().getParent().toString())
                .collect(Collectors.toSet());

        assertEquals(1, dataDirs.size());
        assertTrue(dataDirs.contains(createPath(workingDir, RESTAGING_DIR, "TOPIC1/1/200/1500/2018-01-20")));
    }

    private String createPath(String... tokens) {
        StringJoiner pathJoiner = new StringJoiner(Path.SEPARATOR);
        for (String token : tokens) {
            pathJoiner.add(token);
        }
        return pathJoiner.toString();
    }
}
