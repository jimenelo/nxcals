package cern.nxcals.service.internal.compaction;

import cern.nxcals.api.domain.PartitionResourceHistory;
import cern.nxcals.api.domain.TimeEntityPartitionType;
import cern.nxcals.common.domain.AdaptiveMergeCompactionJob;
import cern.nxcals.service.domain.PartitionResourceHistoryData;
import cern.nxcals.service.internal.PartitionResourceHistoryService;
import cern.nxcals.service.internal.PartitionResourceService;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;

import java.io.IOException;
import java.util.Set;

import static cern.nxcals.service.internal.compaction.DataProcessingJobCreator.sizeOf;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AdaptiveMergeCompactionJobCreatorTest extends CompactionTestUtils {
    @Mock
    private FileSystem fs;
    @Mock
    private PartitionResourceService partitionResourceService;
    @Mock
    private PartitionResourceHistoryService partitionResourceHistoryService;
    @Captor
    ArgumentCaptor<TimeEntityPartitionType> typeCaptor;

    private AdaptiveMergeCompactionJobCreator creator;

    @BeforeEach
    void init() {
        creator = new AdaptiveMergeCompactionJobCreator(SORT_ABOVE, MAX_PARTITION_SIZE, AVRO_PARQUET_RATIO, "x", "y",
                fs, partitionResourceService, partitionResourceHistoryService, x -> true);
    }

    @Nested
    class AdaptiveMergeCompactionJobCreatorTestNested {
        @BeforeEach
        void init() {
            PartitionResourceHistoryData prhData = mock(PartitionResourceHistoryData.class);
            PartitionResourceHistory prh = mock(PartitionResourceHistory.class);
            when(partitionResourceHistoryService.createPartitionResourceHistory(any(), any(),
                    typeCaptor.capture())).thenReturn(prh);
            when(prhData.getInformation()).thenAnswer((Answer<String>) invocation -> typeCaptor.getValue().toString());
            when(partitionResourceHistoryService.findOrCreatePartitionResourceInfoFor(any(), any())).thenReturn(
                    prhData);
        }

        @AfterEach
        void resetMocks() {
            reset(partitionResourceHistoryService);
        }

        @Test
        void shouldReturnJobWithoutPrefix() {
            long fileSize = MAX_PARTITION_SIZE / 1024;
            Set<FileStatus> files = generateUnbucketedFiles(1, fileSize);

            long jobSize = sizeOf(files);
            AdaptiveMergeCompactionJob job = creator.createJob(STAGING_PATH, files, jobSize, jobSize, "prefix");

            assertEquals("", job.getFilePrefix());
        }

        @Test
        void shouldNotPartitionSmallFiles() {
            long fileSize = MAX_PARTITION_SIZE / 1024;
            Set<FileStatus> files = generateBucketedFiles(1, fileSize, "06");

            long jobSize = sizeOf(files);
            AdaptiveMergeCompactionJob job = creator.createJob(STAGING_PATH, files, jobSize, jobSize, "prefix");

            assertEquals(files.size(), job.getFiles().size());
            assertEquals(files.size() * fileSize, job.getJobSize());
            assertEquals(1, job.getPartitionCount());
        }

        @Test
        void shouldPartitionLargeFiles() {
            long fileSize = MAX_PARTITION_SIZE + 1;
            Set<FileStatus> files = generateUnbucketedFiles(1, fileSize);

            long jobSize = sizeOf(files);
            AdaptiveMergeCompactionJob job = creator.createJob(STAGING_PATH, files, jobSize, jobSize, "prefix");

            assertEquals(files.size(), job.getFiles().size());
            assertEquals(files.size() * fileSize, job.getJobSize());
            assertEquals(1, job.getPartitionCount());
        }

        @Test
        void shouldPartitionLargeFilesWithRatio() {
            creator = new AdaptiveMergeCompactionJobCreator(SORT_ABOVE, MAX_PARTITION_SIZE, 5, "x", "y", fs,
                    partitionResourceService, partitionResourceHistoryService, x -> true);

            long fileSize = MAX_PARTITION_SIZE - 1;
            Set<FileStatus> files = generateUnbucketedFiles(10, fileSize);

            long jobSize = sizeOf(files);
            AdaptiveMergeCompactionJob job = creator.createJob(STAGING_PATH, files, jobSize, jobSize, "prefix");

            assertEquals(files.size(), job.getFiles().size());
            assertEquals(files.size() * fileSize, job.getJobSize());
            assertEquals(12, job.getPartitionCount());
        }

        @Test
        void shouldNotSortSmallJobs() {
            long fileSize = SORT_ABOVE / 1024;
            Set<FileStatus> files = generateMixedFiles(fileSize, 100, 100);

            long jobSize = sizeOf(files);
            AdaptiveMergeCompactionJob job = creator.createJob(STAGING_PATH, files, jobSize, jobSize, "prefix");

            assertFalse(job.isSortEnabled());
        }

        @Test
        void shouldSortLargeJobs() {
            long fileSize = SORT_ABOVE + 1;
            Set<FileStatus> files = generateMixedFiles(fileSize, 100, 100);

            long jobSize = sizeOf(files);
            AdaptiveMergeCompactionJob job = creator.createJob(STAGING_PATH, files, jobSize, jobSize, "prefix");

            assertTrue(job.isSortEnabled());
        }

        @Test
        void shouldSortLargeJobsWithRatio() {
            int ratio = 5;
            creator = new AdaptiveMergeCompactionJobCreator(SORT_ABOVE, MAX_PARTITION_SIZE, ratio, "x", "y", fs,
                    partitionResourceService, partitionResourceHistoryService, x -> true);
            long fileSize = SORT_ABOVE * ratio + 1;
            Set<FileStatus> files = generateMixedFiles(fileSize, 1, 0);

            long jobSize = sizeOf(files);
            AdaptiveMergeCompactionJob job = creator.createJob(STAGING_PATH, files, jobSize, jobSize, "prefix");

            assertTrue(job.isSortEnabled());
        }

        @Test
        void shouldNotSortLargeJobsWithRatio() {
            int ratio = 5;
            creator = new AdaptiveMergeCompactionJobCreator(SORT_ABOVE, MAX_PARTITION_SIZE, ratio, "x", "y", fs,
                    partitionResourceService, partitionResourceHistoryService, x -> true);
            long fileSize = SORT_ABOVE * ratio - 1;
            Set<FileStatus> files = generateMixedFiles(fileSize, 1, 0);

            long jobSize = sizeOf(files);
            AdaptiveMergeCompactionJob job = creator.createJob(STAGING_PATH, files, jobSize, jobSize, "prefix");

            assertFalse(job.isSortEnabled());
        }
    }

    @Nested
    class PathCheckerTests {

        void mockThatReportFileExist(boolean exist) throws IOException {
            Path reportFile = new Path(creator.getHdfsPathCreator().toRestageReportFileUri(STAGING_PATH));
            lenient().when(fs.exists(reportFile)).thenReturn(exist);
        }

        @Test
        void shouldReturnTrueWhenReportExists() throws IOException {
            mockThatReportFileExist(true);
            assertTrue(creator.getPathChecker().test(STAGING_PATH));
        }

        @Test
        void shouldReturnFalseWhenReportDoesntExists() throws IOException {
            mockThatReportFileExist(false);
            assertFalse(creator.getPathChecker().test(STAGING_PATH));
        }

        @Test
        void shouldReturnFalseAdditionalPathCheckerReturnFalse() throws IOException {
            creator = new AdaptiveMergeCompactionJobCreator(SORT_ABOVE, MAX_PARTITION_SIZE, 1, "x", "y", fs,
                    partitionResourceService, partitionResourceHistoryService, x -> false);
            mockThatReportFileExist(true);
            assertFalse(creator.getPathChecker().test(STAGING_PATH));
        }
    }
}
