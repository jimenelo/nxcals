package cern.nxcals.service.rest;

import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.Hierarchy;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.domain.VariableConfig;
import cern.nxcals.api.domain.VariableDeclaredType;
import cern.nxcals.api.domain.VariableHierarchies;
import cern.nxcals.api.domain.VariableHierarchyIds;
import cern.nxcals.api.extraction.metadata.queries.Hierarchies;
import cern.nxcals.common.utils.GlobalObjectMapperProvider;
import cern.nxcals.common.utils.ReflectionUtils;
import cern.nxcals.service.domain.EntityData;
import cern.nxcals.service.domain.GroupData;
import cern.nxcals.service.domain.HierarchyData;
import cern.nxcals.service.domain.HierarchyViewData;
import cern.nxcals.service.domain.PartitionData;
import cern.nxcals.service.domain.PartitionPropertiesData;
import cern.nxcals.service.domain.SystemSpecData;
import cern.nxcals.service.domain.VariableConfigData;
import cern.nxcals.service.domain.VariableData;
import cern.nxcals.service.domain.VisibilityData;
import cern.nxcals.service.internal.HierarchyService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSortedSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static cern.nxcals.common.utils.KeyValuesUtils.convertMapIntoAvroSchemaString;
import static cern.nxcals.common.utils.RSQLUtils.toRSQL;
import static cern.nxcals.common.web.Endpoints.BATCH;
import static cern.nxcals.common.web.Endpoints.ENTITIES;
import static cern.nxcals.common.web.Endpoints.HIERARCHIES;
import static cern.nxcals.common.web.Endpoints.HIERARCHIES_FIND_ALL;
import static cern.nxcals.common.web.Endpoints.IDS_SUFFIX;
import static cern.nxcals.common.web.Endpoints.VARIABLES;
import static cern.nxcals.service.rest.DomainTestConstants.ENTITY_KEY_VALUES_1;
import static cern.nxcals.service.rest.DomainTestConstants.PARTITION;
import static cern.nxcals.service.rest.DomainTestConstants.PARTITION_KEY_VALUES_JSON_1;
import static cern.nxcals.service.rest.DomainTestConstants.SYSTEM_SPEC;
import static cern.nxcals.service.rest.TestSchemas.ENTITY_DOUBLE_SCHEMA_KEY_1;
import static cern.nxcals.service.rest.TestSchemas.ENTITY_SCHEMA_1;
import static cern.nxcals.service.rest.TestSchemas.ENTITY_STRING_SCHEMA_KEY_1;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
public class HierarchyControllerTest {
    private ObjectMapper mapper = GlobalObjectMapperProvider.get();

    private MockMvc mockMvc;

    private final SystemSpecData system = system();

    @Mock
    private HierarchyService service;
    @InjectMocks
    private HierarchyController controller;

    @BeforeEach
    public void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(controller)
                .setControllerAdvice(new GlobalControllerExceptionHandler())
                .build();
    }

    @Test
    public void shouldCreateHierarchy() throws Exception {
        Hierarchy nodeParent = hierarchy("A", "descA", system, null);
        Hierarchy node = hierarchy("B", "descB", system, nodeParent);
        HierarchyData hierarchyParent = hierarchyData("A", "descA", system, null);
        HierarchyData hierarchy = hierarchyData("B", "descB", system, hierarchyParent);
        when(service.create(node)).thenReturn(hierarchy);

        mockMvc.perform(post(HIERARCHIES)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(node)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("id").value(10L))
                .andExpect(jsonPath("name").value("B"))
                .andExpect(jsonPath("description").value("descB"))
                .andExpect(jsonPath("nodePath").value("/A/B"))
                .andExpect(jsonPath("recVersion").value(100L))
                .andExpect(jsonPath("parent.id").value(10L))
                .andExpect(jsonPath("parent.name").value("A"))
                .andExpect(jsonPath("parent.description").value("descA"))
                .andExpect(jsonPath("parent.nodePath").value("/A"))
                .andExpect(jsonPath("systemSpec.id").value(system.getId()))
                .andExpect(jsonPath("systemSpec.name").value(system.getName()));

    }

    @Test
    public void shouldCreateMultipleHierarchies() throws Exception {
        Hierarchy nodeParent = hierarchy("A", "descA", system, null);
        Hierarchy nodeB = hierarchy(11L, "B", "descB", system, nodeParent);
        Hierarchy nodeBB = hierarchy(12L, "BB", "descBB", system, nodeParent);
        Hierarchy nodeC = hierarchy(13L, "C", "descC", system, null);
        Set<Hierarchy> hierarchiesToCreate = new HashSet<>(Arrays.asList(nodeB, nodeBB, nodeC));

        HierarchyData hierarchyParent = hierarchyData("A", "descA", system, null);
        HierarchyData hierarchyB = hierarchyData(11L, "B", "descB", system, hierarchyParent);
        HierarchyData hierarchyBB = hierarchyData(12L, "BB", "descBB", system, hierarchyParent);
        HierarchyData hierarchyC = hierarchyData(13L, "C", "descC", system, null);
        Set<HierarchyData> hierarchies = new HashSet<>(Arrays.asList(hierarchyB, hierarchyBB, hierarchyC));

        when(service.createAll(Sets.newHashSet(nodeB, nodeBB, nodeC))).thenReturn(hierarchies);

        mockMvc.perform(post(HIERARCHIES + BATCH)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(hierarchiesToCreate)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(3)))
                .andExpect(jsonPath("$[0].name").value("B"))
                .andExpect(jsonPath("$[0].id").value(11L))
                .andExpect(jsonPath("$[0].nodePath").value("/A/B"))
                .andExpect(jsonPath("$[0].systemSpec.id").value(system.getId()))
                .andExpect(jsonPath("$[0].parent.id").value(10L))
                .andExpect(jsonPath("$[0].parent.name").value("A"))
                .andExpect(jsonPath("$[0].parent.nodePath").value("/A"))

                .andExpect(jsonPath("$[1].name").value("C"))
                .andExpect(jsonPath("$[1].id").value(13L))
                .andExpect(jsonPath("$[1].nodePath").value("/C"))
                .andExpect(jsonPath("$[1].systemSpec.id").value(system.getId()))
                .andExpect(jsonPath("$[1].parent").isEmpty())

                .andExpect(jsonPath("$[2].name").value("BB"))
                .andExpect(jsonPath("$[2].id").value(12L))
                .andExpect(jsonPath("$[2].nodePath").value("/A/BB"))
                .andExpect(jsonPath("$[2].systemSpec.id").value(system.getId()))
                .andExpect(jsonPath("$[2].parent.id").value(10L))
                .andExpect(jsonPath("$[2].parent.name").value("A"))
                .andExpect(jsonPath("$[2].parent.nodePath").value("/A"));
    }

    @Test
    public void shouldUpdateHierarchy() throws Exception {
        Hierarchy nodeParent = hierarchy("A", "descA", system, null);
        Hierarchy node = hierarchy("B", "descB", system, nodeParent);
        HierarchyData hierarchyParent = hierarchyData("A", "descA", system, null);
        HierarchyData hierarchy = hierarchyData("B", "descB", system, hierarchyParent);
        when(service.update(node)).thenReturn(hierarchy);

        mockMvc.perform(put(HIERARCHIES)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(node)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("id").value(10L))
                .andExpect(jsonPath("name").value("B"))
                .andExpect(jsonPath("description").value("descB"))
                .andExpect(jsonPath("nodePath").value("/A/B"))
                .andExpect(jsonPath("recVersion").value(100L))
                .andExpect(jsonPath("parent.id").value(10L))
                .andExpect(jsonPath("parent.name").value("A"))
                .andExpect(jsonPath("parent.description").value("descA"))
                .andExpect(jsonPath("parent.nodePath").value("/A"))
                .andExpect(jsonPath("systemSpec.id").value(system.getId()))
                .andExpect(jsonPath("systemSpec.name").value(system.getName()));
    }

    @Test
    public void shouldUpdateMultipleHierarchies() throws Exception {
        Hierarchy nodeParent = hierarchy("A", "descA", system, null);
        Hierarchy node = hierarchy(11L, "B", "descB", system, nodeParent);
        Hierarchy node2 = hierarchy(12L, "C", "descC", system, nodeParent);
        Hierarchy node3 = hierarchy(13L, "D", "descC", system, null);
        Set<Hierarchy> hierarchiesToUpdate = new HashSet<>(Arrays.asList(node, node2, node3));

        HierarchyData hierarchyParent = hierarchyData("A", "descA", system, null);
        HierarchyData hierarchy = hierarchyData(11L, "B", "descB", system, hierarchyParent);
        HierarchyData hierarchy2 = hierarchyData(12L, "C", "descB", system, hierarchyParent);
        HierarchyData hierarchy3 = hierarchyData(13L, "D", "descD", system, null);
        Set<HierarchyData> hierarchies = new HashSet<>(Arrays.asList(hierarchy, hierarchy2, hierarchy3));

        when(service.updateAll(hierarchiesToUpdate)).thenReturn(hierarchies);

        mockMvc.perform(put(HIERARCHIES + BATCH)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(hierarchiesToUpdate)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(3)))
                .andExpect(jsonPath("$[0].name").value("D"))
                .andExpect(jsonPath("$[0].id").value(13L))
                .andExpect(jsonPath("$[0].nodePath").value("/D"))
                .andExpect(jsonPath("$[0].systemSpec.id").value(system.getId()))
                .andExpect(jsonPath("$[0].parent").isEmpty())

                .andExpect(jsonPath("$[1].name").value("B"))
                .andExpect(jsonPath("$[1].id").value(11L))
                .andExpect(jsonPath("$[1].nodePath").value("/A/B"))
                .andExpect(jsonPath("$[1].systemSpec.id").value(system.getId()))
                .andExpect(jsonPath("$[1].parent.id").value(10L))
                .andExpect(jsonPath("$[1].parent.name").value("A"))
                .andExpect(jsonPath("$[1].parent.nodePath").value("/A"))

                .andExpect(jsonPath("$[2].name").value("C"))
                .andExpect(jsonPath("$[2].id").value(12L))
                .andExpect(jsonPath("$[2].nodePath").value("/A/C"))
                .andExpect(jsonPath("$[2].systemSpec.id").value(system.getId()))
                .andExpect(jsonPath("$[2].parent.id").value(10L))
                .andExpect(jsonPath("$[2].parent.name").value("A"))
                .andExpect(jsonPath("$[2].parent.nodePath").value("/A"));
    }

    @Test
    public void shouldDeleteLeaf() throws Exception {
        doNothing().when(service).deleteLeaf(10L);

        mockMvc.perform(delete(HIERARCHIES + "/" + 10L)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        //                .andDo(print());

        verify(service, times(1)).deleteLeaf(10L);
    }

    @Test
    public void shouldDeleteAllLeaves() throws Exception {
        Set<Long> hierarchyIds = new HashSet<>(Arrays.asList(10L, 11L, 12L));
        doNothing().when(service).deleteAllLeaves(hierarchyIds);

        mockMvc.perform(delete(HIERARCHIES + BATCH)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(hierarchyIds)))
                .andExpect(status().isOk());
        //                .andDo(print());

        verify(service, times(1)).deleteAllLeaves(hierarchyIds);
    }

    @Test
    public void shouldReturnCollectionOfHierarchyData() throws Exception {
        HierarchyData hierarchyParent = hierarchyData("A", "descA", system, null);
        HierarchyData hierarchy = hierarchyData("B", "descB", system, hierarchyParent);

        String rsql = toRSQL(Hierarchies.suchThat().systemName().eq(system.getName()).and().name().eq("A"));
        when(service.findAll(argThat(arg -> arg.equals(rsql)))).thenReturn(Collections.singletonList(hierarchy));

        mockMvc.perform(post(HIERARCHIES_FIND_ALL)
                        .contentType(MediaType.TEXT_PLAIN_VALUE)
                        .content(rsql))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id").value(10L))
                .andExpect(jsonPath("$[0].name").value("B"))
                .andExpect(jsonPath("$[0].description").value("descB"))
                .andExpect(jsonPath("$[0].nodePath").value("/A/B"))
                .andExpect(jsonPath("$[0].parent.id").value(10L))
                .andExpect(jsonPath("$[0].parent.name").value("A"))
                .andExpect(jsonPath("$[0].parent.description").value("descA"))
                .andExpect(jsonPath("$[0].parent.nodePath").value("/A"))
                .andExpect(jsonPath("$[0].systemSpec.id").value(system.getId()))
                .andExpect(jsonPath("$[0].systemSpec.name").value(system.getName()));
    }

    @Test
    public void shouldReturnCollectionOfVariablesForHierarchy() throws Exception {
        when(service.getVariables(eq(20L))).thenReturn(Sets.newHashSet(
                variableData(1, "v1"),
                variableData(2, "v2"),
                variableData(3, "v3"))
        );

        when(service.getVariables(eq(10L))).thenReturn(Sets.newHashSet(
                variableData(3, "v3"),
                variableData(4, "v4"),
                variableData(5, "v5"))
        );

        mockMvc.perform(post(HIERARCHIES + "/20" + VARIABLES)
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(3)))
                .andExpect(jsonPath("$[*].variableName").value(containsInAnyOrder("v1", "v2", "v3")))
                .andExpect(jsonPath("$[*].id").value(containsInAnyOrder(1, 2, 3)));

        mockMvc.perform(post(HIERARCHIES + "/10" + VARIABLES)
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(3)))
                .andExpect(jsonPath("$[*].variableName").value(containsInAnyOrder("v3", "v4", "v5")))
                .andExpect(jsonPath("$[*].id").value(containsInAnyOrder(3, 4, 5)));
    }

    @Test
    public void shouldSetCollectionOfVariablesForHierarchy() throws Exception {
        mockMvc.perform(put(HIERARCHIES + "/10" + VARIABLES + IDS_SUFFIX)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(mapper.writeValueAsString(Collections.singleton(variable(20, "v1").getId()))))
                .andExpect(status().isOk());
    }

    @Test
    public void shouldAddCollectionOfVariablesForHierarchy() throws Exception {
        mockMvc.perform(post(HIERARCHIES + "/10" + VARIABLES + IDS_SUFFIX)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(mapper.writeValueAsString(Collections.singleton(variable(20, "v1").getId()))))
                .andExpect(status().isOk());
    }

    @Test
    public void shouldRemoveCollectionOfVariablesForHierarchy() throws Exception {
        mockMvc.perform(delete(HIERARCHIES + "/10" + VARIABLES + IDS_SUFFIX)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(mapper.writeValueAsString(Collections.singleton(variable(20, "v1").getId()))))
                .andExpect(status().isOk());
    }

    @Test
    public void shouldReturnCollectionOfEntitiesForHierarchy() throws Exception {
        when(service.getEntities(eq(20L))).thenReturn(Sets.newHashSet(
                entityData(1, "e1"),
                entityData(2, "e2"),
                entityData(3, "e3"))
        );

        when(service.getEntities(eq(10L))).thenReturn(Sets.newHashSet(
                entityData(3, "e3"),
                entityData(4, "e4"),
                entityData(5, "e5"))
        );

        mockMvc.perform(post(HIERARCHIES + "/20" + ENTITIES)
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(3)))
                .andExpect(jsonPath("$[*].id").value(containsInAnyOrder(1, 2, 3)));

        mockMvc.perform(post(HIERARCHIES + "/10" + ENTITIES)
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(3)))
                .andExpect(jsonPath("$[*].id").value(containsInAnyOrder(3, 4, 5)));
    }

    @Test
    public void shouldSetCollectionOfEntitiesForHierarchy() throws Exception {
        mockMvc.perform(put(HIERARCHIES + "/10" + ENTITIES + IDS_SUFFIX)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(mapper.writeValueAsString(Collections.singleton(entity().getId()))))
                .andExpect(status().isOk());
    }

    @Test
    public void shouldAddCollectionOfEntitiesForHierarchy() throws Exception {
        mockMvc.perform(post(HIERARCHIES + "/10" + ENTITIES + IDS_SUFFIX)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(mapper.writeValueAsString(Collections.singleton(entity().getId()))))
                .andExpect(status().isOk());
    }

    @Test
    public void shouldRemoveCollectionOfEntitiesForHierarchy() throws Exception {
        mockMvc.perform(delete(HIERARCHIES + "/10" + ENTITIES + IDS_SUFFIX)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(mapper.writeValueAsString(Collections.singleton(entity().getId()))))
                .andExpect(status().isOk());
    }

    @Test
    public void shouldGetHierarchiesForVariables() throws Exception {
        Set<Long> variableIds = Sets.newHashSet(1L, 2L);

        Map<Long, Set<HierarchyData>> hierarchiesPerVariable = new HashMap<>();
        Set<HierarchyData> variableHierarchies1 = Sets.newHashSet(hierarchyData(1L, "A", "descA", system, null),
                hierarchyData(2L, "B", "descB", system, null));

        Set<HierarchyData> variableHierarchies2 = Sets.newHashSet(hierarchyData(1L, "A", "descA", system, null));

        hierarchiesPerVariable.put(1L, variableHierarchies1);
        hierarchiesPerVariable.put(2L, variableHierarchies2);
        when(service.getHierarchiesForVariables(variableIds)).thenReturn(hierarchiesPerVariable);

        MvcResult result = mockMvc.perform(post(HIERARCHIES + BATCH + VARIABLES + IDS_SUFFIX)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(mapper.writeValueAsString(variableIds)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(variableIds.size()))).andReturn();

        Set<VariableHierarchies> variableHierarchies = mapper
                .readValue(result.getResponse().getContentAsString(), new TypeReference<Set<VariableHierarchies>>() {
                });
        assertEquals(2, variableHierarchies.size());

        Map<Long, Set<Hierarchy>> hierarchiesByVariableId = variableHierarchies.stream().collect(Collectors.toMap(
                VariableHierarchies::getVariableId, VariableHierarchies::getHierarchies));
        Set<Hierarchy> hierarchies = hierarchiesByVariableId.get(1L);
        assertNotNull(hierarchies);
        assertEquals(2, hierarchies.size());
        assertArrayEquals(new long[] { 1L, 2L },
                hierarchies.stream().mapToLong(Hierarchy::getId).sorted().toArray());

        Set<Hierarchy> hierarchies2 = hierarchiesByVariableId.get(2L);
        assertNotNull(hierarchies2);
        assertEquals(1, hierarchies2.size());
        assertEquals(1L, Iterables.getOnlyElement(hierarchies2).getId());

    }

    @Test
    public void shouldGetHierarchyIdsForVariables() throws Exception {
        Set<Long> variableIds = Sets.newHashSet(1L, 2L);

        Map<Long, Set<Long>> hierarchyIdsPerVariable = new HashMap<>();
        Set<Long> variableHierarchies1 = Sets.newHashSet(1L, 2L);

        Set<Long> variableHierarchies2 = Sets.newHashSet(1L);

        hierarchyIdsPerVariable.put(1L, variableHierarchies1);
        hierarchyIdsPerVariable.put(2L, variableHierarchies2);
        when(service.getHierarchyIdsForVariables(variableIds)).thenReturn(hierarchyIdsPerVariable);

        MvcResult result = mockMvc.perform(post(HIERARCHIES + IDS_SUFFIX + BATCH + VARIABLES + IDS_SUFFIX)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(mapper.writeValueAsString(variableIds)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(variableIds.size()))).andReturn();

        Set<VariableHierarchyIds> variableHierarchyIds = mapper
                .readValue(result.getResponse().getContentAsString(), new TypeReference<Set<VariableHierarchyIds>>() {
                });
        assertEquals(2, variableHierarchyIds.size());

        Map<Long, Set<Long>> hierarchyIdsByVariableId = variableHierarchyIds.stream().collect(Collectors.toMap(
                VariableHierarchyIds::getVariableId, VariableHierarchyIds::getHierarchyIds));
        Set<Long> hierarchies = hierarchyIdsByVariableId.get(1L);
        assertNotNull(hierarchies);
        assertEquals(2, hierarchies.size());
        assertThat(hierarchies).containsOnly(1L, 2L);

        Set<Long> hierarchies2 = hierarchyIdsByVariableId.get(2L);
        assertNotNull(hierarchies2);
        assertEquals(1, hierarchies2.size());
        assertThat(hierarchies2).containsOnly(1L);
    }

    // helper methods

    private Hierarchy hierarchy(String name, String description, SystemSpecData system, Hierarchy parent,
            Hierarchy... children) {
        return hierarchy(10L, name, description, system, parent, children);
    }

    private Hierarchy hierarchy(long id, String name, String description, SystemSpecData system, Hierarchy parent,
            Hierarchy... children) {
        return ReflectionUtils.builderInstance(Hierarchy.InnerBuilder.class)
                .id(id)
                .name(name)
                .description(description)
                .systemSpec(system.toSystemSpec())
                .nodePath((parent == null ? "" : parent.getNodePath()) + "/" + name)
                .parent(parent)
                .children(Sets.newHashSet(children))
                .recVersion(100L)
                .build();
    }

    private HierarchyData hierarchyData(String name, String description, SystemSpecData system, HierarchyData parent,
            HierarchyData... children) {
        return hierarchyData(10L, name, description, system, parent, children);
    }

    private HierarchyData hierarchyData(long id, String name, String description, SystemSpecData system,
            HierarchyData parent, HierarchyData... children) {
        HierarchyData hierarchyData = new HierarchyData();
        hierarchyData.setId(id);
        hierarchyData.setName(name);
        hierarchyData.setSystem(system);
        HierarchyViewData viewData = new HierarchyViewData();
        viewData.setLevel(12);
        viewData.setLeaf(true);
        viewData.setNodePath((parent == null ? "" : parent.getViewData().getNodePath()) + "/" + name);
        hierarchyData.setViewData(viewData);
        hierarchyData.setParent(parent);
        hierarchyData.setRecVersion(100L);
        hierarchyData.setChildren(Sets.newHashSet(children));
        hierarchyData.setGroup(new GroupData());
        hierarchyData.getGroup().setDescription(description);
        hierarchyData.getGroup().setSystem(system);
        hierarchyData.getGroup().setVisibility(VisibilityData.PRIVATE);
        hierarchyData.getGroup().setLabel("LABEL");
        return hierarchyData;
    }

    private Variable variable(long id, String name) {
        VariableConfig config = VariableConfig.builder().entityId(20).fieldName("field").build();
        return ReflectionUtils.builderInstance(Variable.InnerBuilder.class).id(id).variableName(name)
                .configs(ImmutableSortedSet.of(config)).systemSpec(system.toSystemSpec()).build();
    }

    private VariableData variableData(long id, String name) {
        VariableData variable = new VariableData();
        variable.setVariableName(name);
        variable.setSystem(system);
        variable.setDescription("desc");
        variable.setDeclaredType(VariableDeclaredType.MATRIX_NUMERIC);
        variable.setId(id);
        variable.setRecVersion(20L);

        VariableConfigData configData = new VariableConfigData();
        configData.setVariable(variable);
        configData.setFieldName("field");
        configData.setEntity(entityData(10, "string"));
        configData.setId(id);
        configData.setRecVersion(20L);
        variable.addConfig(configData);
        return variable;
    }

    private static Entity entity() {
        return Entity.builder()
                .entityKeyValues(ENTITY_KEY_VALUES_1)
                .systemSpec(SYSTEM_SPEC)
                .partition(PARTITION)
                .entityHistory(Collections.emptySortedSet()).build();
    }

    private EntityData entityData(long id, String value) {
        Map<String, Object> keyValues = ImmutableMap.of(ENTITY_STRING_SCHEMA_KEY_1, value, ENTITY_DOUBLE_SCHEMA_KEY_1,
                1d);
        EntityData entityData = new EntityData();
        entityData.setId(id);
        entityData.setRecVersion(20L);
        entityData.setKeyValues(convertMapIntoAvroSchemaString(keyValues, ENTITY_SCHEMA_1.toString()));
        PartitionData partitionData = new PartitionData();
        partitionData.setId(10L);
        partitionData.setRecVersion(20L);
        partitionData.setSystem(system);
        partitionData.setKeyValues(PARTITION_KEY_VALUES_JSON_1);
        partitionData.setProperties(new PartitionPropertiesData(false));
        entityData.setPartition(partitionData);
        return entityData;
    }

    private SystemSpecData system() {
        SystemSpecData systemSpecData = new SystemSpecData();
        systemSpecData.setId(10L);
        systemSpecData.setName("SYSTEM_NAME");
        systemSpecData.setEntityKeyDefs("EKD");
        systemSpecData.setPartitionKeyDefs("PKD");
        systemSpecData.setTimeKeyDefs("TKD");
        return systemSpecData;
    }
}
