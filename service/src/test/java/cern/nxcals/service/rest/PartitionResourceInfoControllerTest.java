package cern.nxcals.service.rest;

import cern.nxcals.api.domain.PartitionResourceHistory;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.utils.TimeUtils;
import cern.nxcals.common.utils.GlobalObjectMapperProvider;
import cern.nxcals.common.utils.ReflectionUtils;
import cern.nxcals.service.domain.PartitionResourceData;
import cern.nxcals.service.domain.PartitionResourceHistoryData;
import cern.nxcals.service.internal.PartitionResourceHistoryService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.Instant;

import static cern.nxcals.common.web.Endpoints.PARTITION_RESOURCE_HISTORIES;
import static cern.nxcals.service.rest.DomainTestConstants.PARTITION_RESOURCE;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
public class PartitionResourceInfoControllerTest {
    private ObjectMapper mapper = GlobalObjectMapperProvider.get();

    @Mock
    PartitionResourceData partitionResourceData;

    private MockMvc mockMvc;

    @Mock
    private PartitionResourceHistoryService service;

    @InjectMocks
    private PartitionResourceHistoryController controller;

    private Instant startTime = TimeUtils.getInstantFromNanos(TimeUtils.getNanosFromInstant(Instant.now()) - 10000);
    private Instant endTime = TimeUtils.getInstantFromNanos(TimeUtils.getNanosFromInstant(Instant.now()) + 10000);

    @BeforeEach
    public void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(controller)
                .setControllerAdvice(new GlobalControllerExceptionHandler())
                .build();
    }

    // create
    @Test
    public void shouldCreatePartitionResourceHistory() throws Exception {
        when(partitionResourceData.toPartitionResource()).thenReturn(PARTITION_RESOURCE);

        PartitionResourceHistory partitionResourceHistoryToCreate = partitionResourceInformation();
        PartitionResourceHistoryData partitionResourceInformationCreated = partitionResourceInformationData();

        when(service.create(partitionResourceHistoryToCreate)).thenReturn(partitionResourceInformationCreated);

        mockMvc.perform(post(PARTITION_RESOURCE_HISTORIES)
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(partitionResourceHistoryToCreate)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("id").value(42L))
                .andExpect(jsonPath("partitionResource.id").value(PARTITION_RESOURCE.getId()))
                .andExpect(jsonPath("partitionInformation").value("Information"))
                .andExpect(jsonPath("compactionType").value("Compaction"))
                .andExpect(jsonPath("storageType").value("Storage"))
                .andExpect(jsonPath("fixedSettings").value(false))
                .andExpect(jsonPath("validity").exists())
                .andExpect(jsonPath("recVersion").value(2L));
    }

    // update
    @Test
    public void shouldUpdatePartitionResourceHistory() throws Exception {
        when(partitionResourceData.toPartitionResource()).thenReturn(PARTITION_RESOURCE);
        PartitionResourceHistory partitionResourceHistoryToCreate = partitionResourceInformation();
        PartitionResourceHistoryData partitionResourceInformationCreated = partitionResourceInformationData();

        when(service.update(partitionResourceHistoryToCreate)).thenReturn(partitionResourceInformationCreated);

        mockMvc.perform(put(PARTITION_RESOURCE_HISTORIES)
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(partitionResourceHistoryToCreate)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("id").value(42L))
                .andExpect(jsonPath("partitionResource.id").value(PARTITION_RESOURCE.getId()))
                .andExpect(jsonPath("partitionInformation").value("Information"))
                .andExpect(jsonPath("compactionType").value("Compaction"))
                .andExpect(jsonPath("storageType").value("Storage"))
                .andExpect(jsonPath("fixedSettings").value(false))
                .andExpect(jsonPath("validity").exists())
                .andExpect(jsonPath("recVersion").value(2L));
    }

    // delete
    @Test
    public void shouldDeletePartitionResourceHistory() throws Exception {
        long partitionResourceInformationId = 1234L;
        doNothing().when(service).delete(partitionResourceInformationId);
        mockMvc.perform(delete(PARTITION_RESOURCE_HISTORIES + "/" + partitionResourceInformationId))
                .andExpect(status().isOk());
        //                .andDo(print());
    }

    private PartitionResourceHistoryData partitionResourceInformationData() {
        PartitionResourceHistoryData pri = new PartitionResourceHistoryData();
        pri.setId(42L);
        pri.setPartitionResource(partitionResourceData);
        pri.setInformation("Information");
        pri.setCompactionType("Compaction");
        pri.setStorageType("Storage");
        pri.setFixedSettings(false);
        pri.setValidFromStamp(startTime);
        pri.setValidToStamp(endTime);
        pri.setRecVersion(2L);
        return pri;
    }

    private PartitionResourceHistory partitionResourceInformation() {
        return ReflectionUtils.builderInstance(PartitionResourceHistory.InnerBuilder.class)
                .partitionResource(PARTITION_RESOURCE)
                .partitionInformation("Information")
                .compactionType("Compaction")
                .storageType("Storage")
                .isFixedSettings(false)
                .validity(TimeWindow.between(startTime, endTime))
                .recVersion(2L)
                .build();
    }
}
