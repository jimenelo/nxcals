/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.rest;

import org.apache.avro.Schema;
import org.apache.avro.SchemaBuilder;

public class TestSchemas {

    private TestSchemas() {
        //Test static class
    }

    public static final String ENTITY_STRING_SCHEMA_KEY_1 = "entity_string_1";
    public static final String ENTITY_DOUBLE_SCHEMA_KEY_1 = "entity_double_1";
    public static final String ENTITY_STRING_SCHEMA_KEY_2 = "entity_string_2";
    public static final String ENTITY_DOUBLE_SCHEMA_KEY_2 = "entity_double_2";

    public static final String PARTITION_STRING_SCHEMA_KEY_1 = "partition_string_1";
    public static final String PARTITION_DOUBLE_SCHEMA_KEY_1 = "partition_double_1";
    public static final String PARTITION_STRING_SCHEMA_KEY_2 = "partition_string11";
    public static final String PARTITION_DOUBLE_SCHEMA_KEY_2 = "partition_double_2";

    public static final String TIME_DOUBLE_SCHEMA_KEY = "time_double";
    public static final String RECORD_VERSION_STRING_SCHEMA_KEY = "record_version_string";

    public static final Schema getFullSchema(Schema... schemas) {
        SchemaBuilder.FieldAssembler<Schema> fieldBuilder = SchemaBuilder.record("schema").fields();
        for (Schema schema : schemas) {
            for (Schema.Field field : schema.getFields()) {
                fieldBuilder.name(field.name()).type(field.schema()).noDefault();
            }
        }
        return fieldBuilder.endRecord();
    }

    public static Schema createRandomSchema(String name) {
        return SchemaBuilder.record(name).fields()
                .name(ENTITY_STRING_SCHEMA_KEY_1).type().stringType().noDefault()
                .name(ENTITY_DOUBLE_SCHEMA_KEY_1).type().doubleType().noDefault()
                .endRecord();
    }

    public static final Schema ENTITY_SCHEMA_1 = SchemaBuilder.record("entity_type_1").fields()
            .name(ENTITY_STRING_SCHEMA_KEY_1).type().stringType().noDefault()
            .name(ENTITY_DOUBLE_SCHEMA_KEY_1).type().doubleType().noDefault()
            .endRecord();

    public static final Schema ENTITY_SCHEMA_2 = SchemaBuilder.record("entity_type_2").fields()
            .name(ENTITY_STRING_SCHEMA_KEY_2).type().stringType().noDefault()
            .name(ENTITY_DOUBLE_SCHEMA_KEY_2).type().doubleType().noDefault()
            .endRecord();

    public static final Schema PARTITION_SCHEMA_1 = SchemaBuilder.record("partition_type_1").fields()
            .name(PARTITION_STRING_SCHEMA_KEY_1).type().stringType().noDefault()
            .name(PARTITION_DOUBLE_SCHEMA_KEY_1).type().doubleType().noDefault()
            .endRecord();

    public static final Schema PARTITION_SCHEMA_2 = SchemaBuilder.record("partition_type_2").fields()
            .name(PARTITION_STRING_SCHEMA_KEY_2).type().stringType().noDefault()
            .name(PARTITION_DOUBLE_SCHEMA_KEY_2).type().doubleType().noDefault()
            .endRecord();

    public static final Schema TIME_SCHEMA = SchemaBuilder.record("test_type").fields()
            .name(TIME_DOUBLE_SCHEMA_KEY).type().doubleType().noDefault()
            .endRecord();

    public static final Schema RECORD_VERSION_SCHEMA = SchemaBuilder.record("test_type").fields()
            .name(RECORD_VERSION_STRING_SCHEMA_KEY).type().stringType().noDefault()
            .endRecord();
}
