package cern.nxcals.service.domain;

import org.apache.logging.log4j.core.util.Assert;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

class VariableDataTest {

    @Test
    void addAndRemoveConfig() {
        VariableData data = new VariableData();
        VariableConfigData config = mock(VariableConfigData.class);
        data.addConfig(config);
        data.removeConfig(config);
        verify(config, times(1)).setVariable(data);
        Assert.isEmpty(data.getVariableConfigs());
    }
}