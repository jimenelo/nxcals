package cern.nxcals.service.security;

import cern.accsoft.ccs.ccda.client.rbac.Role;
import cern.accsoft.ccs.ccda.client.rbac.User;
import cern.accsoft.ccs.ccda.client.rbac.UserRole;
import cern.nxcals.service.internal.security.RbaUserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Optional;
import java.util.UUID;
import java.util.function.UnaryOperator;

import static cern.nxcals.service.BaseTest.kerberosNameFrom;
import static java.util.Collections.singleton;
import static java.util.function.UnaryOperator.identity;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class UserPrincipalServiceTest {
    private static final String REALM = "UNIT_TEST.CH";
    private static final String PERMISSION = "UNIT_TEST_PERMISSION:WRITE";
    private RbaUserService rbaUserService;
    private UserPrincipalService instance;

    @BeforeEach
    public void setUp() {
        this.rbaUserService = mock(RbaUserService.class);
        this.instance = new UserPrincipalService(rbaUserService, REALM);
    }

    @Test
    void loadUserByQualifiedUsername() {
        loadUser(username -> kerberosNameFrom(username, REALM));
    }

    @Test
    void loadUserByPlainUsername() {
        loadUser(identity());
    }

    private void loadUser(UnaryOperator<String> converter) {
        String username = UUID.randomUUID().toString();
        String qualifiedName = converter.apply(username);

        User user = createMockUserFor(username);
        when(rbaUserService.findBy(username)).thenReturn(Optional.of(user));

        UserDetails userDetails = this.instance.loadUserByUsername(qualifiedName);

        assertEquals(username, userDetails.getUsername());
        assertTrue(
                userDetails.getAuthorities().stream().map(GrantedAuthority::getAuthority).anyMatch(PERMISSION::equals));

    }

    @Test
    void shouldFailForEmpty() {
        assertThrows(UsernameNotFoundException.class, () -> instance.loadUserByUsername(""));
    }

    @Test
    void shouldFailWithoutData() {
        when(rbaUserService.findBy(any())).thenReturn(Optional.empty());
        assertThrows(UsernameNotFoundException.class, () -> instance.loadUserByUsername("username"));
    }

    private User createMockUserFor(String username) {
        UserRole userRole = UserRole.builder().role(Role.builder().name(PERMISSION).build()).build();
        return User.builder().name(username).roles(singleton(userRole)).build();
    }
}
