package cern.nxcals.service.security.rbac;

import cern.rbac.common.RbaToken;
import cern.rbac.common.test.TestTokenBuilder;
import com.google.common.collect.Iterators;
import org.junit.jupiter.api.Test;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

class RbacToAuthenticationConverterImplTest {

    @Test
    void shouldCreateDefaultRbacUserDetails() {
        String username = "acclog";
        String role = "editor";
        RbacToAuthenticationConverterImpl converter = new RbacToAuthenticationConverterImpl();
        RbaToken token = TestTokenBuilder.newInstance().username(username).roles(role).build();

        //when
        Authentication auth = converter.convert(token);

        //then
        assertNotNull(auth);
        Object principal = auth.getPrincipal();
        assertNotNull(principal);
        assertTrue(principal instanceof UserDetails);
        UserDetails details = (UserDetails) principal;
        assertEquals(username, details.getUsername());
        assertEquals(1, details.getAuthorities().size());
        assertEquals(role, Iterators.getOnlyElement(details.getAuthorities().iterator()).getAuthority());
    }
}
