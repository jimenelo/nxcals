/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service;

import cern.nxcals.service.repository.CustomRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.ApplicationPidFileWriter;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories(transactionManagerRef = "jpaTransactionManager", repositoryBaseClass = CustomRepository.class)
@EnableAspectJAutoProxy(proxyTargetClass = true)
@Slf4j
public class Application {
    public static void main(String[] args) {
        try {
            SpringApplication app = new SpringApplication(Application.class);
            app.addListeners(new ApplicationPidFileWriter());
            ConfigurableApplicationContext context = app.run(args);
            IdGeneratorFactory.setContext(context);
        } catch (Exception ex) {
            log.error("Cannot start service appplication", ex);
            System.exit(-1);
        }
    }
}
