package cern.nxcals.service.util;

import cern.nxcals.api.domain.OperationType;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class OperationTypeConverter implements AttributeConverter<OperationType, String> {

    @Override
    public String convertToDatabaseColumn(OperationType operationType) {
        return operationType.getShortName();
    }

    @Override
    public OperationType convertToEntityAttribute(String dbData) {
        return OperationType.fromShortName(dbData);
    }

}
