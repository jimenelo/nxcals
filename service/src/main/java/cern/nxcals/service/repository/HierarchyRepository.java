package cern.nxcals.service.repository;

import cern.nxcals.service.domain.HierarchyData;
import cern.nxcals.service.domain.VariableWithHierarchy;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Set;

/**
 * N.B. before adding any new method that may fire changelog triggers, please, ensure that this method execution
 * will match the pointcut {@link cern.nxcals.service.tracing.TraceDbActionAspect#executeTransaction()}
 */
public interface HierarchyRepository
        extends BaseRepository<HierarchyData>, RsqlRepository<HierarchyData>, RsqlGraphRepository<HierarchyData> {

    @Query(value = "select v.key.variableId, h "
            + "from HierarchyData h "
            + "join VariableDataWithAssociation v on v.key.groupId = h.group.id "
            + "where v.key.association = '__in__hierarchy__'  and v.key.variableId in :variableIds"
    )
    @EntityGraph(HierarchyData.PLAIN)
    // for some unknown reason the following statement
    // "select new cern.nxcals.service.domain.VariableWithHierarchy( v.key.variableId, h )"
    // creates h as proxy. That's why we have to use a list of objects instead.
    // The first element is the variableId, the second a HierarchyData instance
    // created accordingly to the used entity graph specification
    Set<List<Object>> findAllByVariableIdIn(@Param("variableIds") Set<Long> variableIds);

    @Query(value = "select new cern.nxcals.service.domain.VariableWithHierarchy( v.key.variableId, h.id ) "
            + "from HierarchyData h "
            + "join VariableDataWithAssociation v on v.key.groupId = h.group.id "
            + "where v.key.association = '__in__hierarchy__'  and v.key.variableId in :variableIds"
    )
    Set<VariableWithHierarchy<Long>> findVariableWithHierarchiesByVariableIdIn(@Param("variableIds") Set<Long> variableIds);

}
