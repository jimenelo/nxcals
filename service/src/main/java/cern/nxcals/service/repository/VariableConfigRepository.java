/*
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.service.repository;

import cern.nxcals.service.domain.VariableConfigData;

import java.util.Set;

/**
 * N.B. before adding any new method that may fire changelog triggers, please, ensure that this method execution
 * will match the pointcut {@link cern.nxcals.service.tracing.TraceDbActionAspect#executeTransaction()}
 */
public interface VariableConfigRepository
        extends BaseRepository<VariableConfigData>, RsqlRepository<VariableConfigData>, RsqlGraphRepository<VariableConfigData> {

    Set<VariableConfigData> findAllByVariableId(long variableId);

}
