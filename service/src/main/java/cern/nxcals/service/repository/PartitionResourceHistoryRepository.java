package cern.nxcals.service.repository;

import cern.nxcals.service.domain.PartitionResourceData;
import cern.nxcals.service.domain.PartitionResourceHistoryData;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

public interface PartitionResourceHistoryRepository extends BaseRepository<PartitionResourceHistoryData>,
        RsqlRepository<PartitionResourceHistoryData>, RsqlGraphRepository<PartitionResourceHistoryData> {

    List<PartitionResourceHistoryData> findByPartitionResourceId(long partitionResourceId);

    List<PartitionResourceHistoryData> findByStorageType(String storageType);

    /**
     * Finds the partition resource info that is valid for this given timestamp (the timestamp is inside the  <validFromStamp;validToStamp) range.
     *
     * @param partitionResource an {@link PartitionResourceData} instance to be associated with information
     * @param timestamp         the time {@link Instant} that corresponds to a point of time that the entry is valid for
     * @return an {@link PartitionResourceHistoryData} instance that is valid in the provided point of time
     */
    @Query("select d from PartitionResourceHistoryData d where d.partitionResource = :partitionResource  and (d.validFromStamp <= :timestamp and d.validToStamp > :timestamp)")
    Optional<PartitionResourceHistoryData> findByPartitionResourceAndTimestamp(@Param("partitionResource") PartitionResourceData partitionResource,
                                                                               @Param("timestamp") Instant timestamp);


    /**
     * Finds the partition resource info that is valid for this given time window <fromStamp-toStamp> range.
     * Copied from EntityHistoryRepository as for time condition.
     *
     * @param partitionResource an {@link PartitionResourceData} instance to be associated with information
     * @param fromStamp         the time {@link Instant} of the query start (inclusive)
     * @param toStamp           the time {@link Instant} of the query end (inclusive)
     * @return an {@link PartitionResourceHistoryData} instance that is valid in the provided point of time
     */
    @Query("select d from PartitionResourceHistoryData d where d.partitionResource = :partitionResource  and (d.validFromStamp <= :end and d.validToStamp > :start)")
    List<PartitionResourceHistoryData> findByPartitionResourceAndTimestamps(@Param("partitionResource") PartitionResourceData partitionResource,
                                                                            @Param("start") Instant fromStamp, @Param("end") Instant toStamp);

    /**
     * Finds partition resource info for a given resource id and timestamp == validToStamp.
     *
     * @param partitionResourceId an {@link PartitionResourceData} instance id to be associated with information
     * @param timestamp           the time {@link Instant} that equals validToStamp
     * @return an {@link PartitionResourceHistoryData} instance that is valid in the provided point of time
     */
    Optional<PartitionResourceHistoryData> findByPartitionResourceIdAndValidToStamp(long partitionResourceId,
                                                                                    Instant timestamp);

    /**
     * Finds partition resource info for a given resource id and timestamp == validFromStamp.
     *
     * @param partitionResourceId an {@link PartitionResourceData} instance id to be associated with information
     * @param timestamp           the time {@link Instant} that equals validFromStamp
     * @return an {@link PartitionResourceHistoryData} instance that is valid in the provided point of time
     */
    Optional<PartitionResourceHistoryData> findByPartitionResourceIdAndValidFromStamp(long partitionResourceId,
                                                                                      Instant timestamp);




}
