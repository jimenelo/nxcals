package cern.nxcals.service.repository;

import cern.nxcals.service.domain.PartitionResourceData;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;


public interface PartitionResourceRepository extends BaseRepository<PartitionResourceData>, RsqlRepository<PartitionResourceData>, RsqlGraphRepository<PartitionResourceData> {

    @Query(value = "select p "
            + "from PartitionResourceData p "
            + "where p.system.id = :systemId and p.partition.id = :partitionId and p.schema.id = :schemaId")
    Optional<PartitionResourceData> findBySystemIdAndPartitionIdAndSchemaId(@Param("systemId") long systemId, @Param("partitionId") long partitionId, @Param("schemaId") long schemaId);

}
