/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.repository;

import cern.nxcals.service.domain.SystemSpecData;

import java.util.Optional;
import java.util.Set;

/**
 * N.B. before adding any new method that may fire changelog triggers, please, ensure that this method execution
 * will match the pointcut {@link cern.nxcals.service.tracing.TraceDbActionAspect#executeTransaction()}
 */
public interface SystemSpecRepository extends BaseRepository<SystemSpecData>, RsqlRepository<SystemSpecData> {
    Optional<SystemSpecData> findByName(String name);
    Set<SystemSpecData> findAll();
}