package cern.nxcals.service.security.rbac;

import cern.rbac.common.RbaToken;
import cern.rbac.common.RbacConfiguration;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.security.core.Authentication;

import javax.servlet.http.HttpServletRequest;
import java.nio.ByteBuffer;
import java.util.Base64;
import java.util.Optional;

import static cern.rbac.common.RbaToken.EMPTY_TOKEN;

@RequiredArgsConstructor
@Slf4j
public class DefaultRbacAuthenticationService implements RbacAuthenticationService {
    private static final Base64.Decoder DECODER = Base64.getDecoder();
    private final RbacToAuthenticationConverter rbacToAuthenticationConverter;

    @Override
    public RbaToken extractRbacToken(HttpServletRequest httpRequest) {
        return Optional.ofNullable(httpRequest.getHeader(HttpHeaders.AUTHORIZATION))
                .map(header -> {
                    try {
                        RbaToken token = RbaToken.parseAndValidate(ByteBuffer.wrap(DECODER.decode(header)),
                                RbacConfiguration.getCurrent());
                        log.debug("Received client RBAC token: {}", token);
                        return token;
                    } catch (Exception rbaException) {
                        log.trace(
                                "Error while de-serializing the RBAC token from header {} (please do not leave this TRACE enabled, header may contain sensitive info). Continuing as anonymous...",
                                header, rbaException);
                        return EMPTY_TOKEN;
                    }
                }).orElse(EMPTY_TOKEN);
    }

    @Override
    public Optional<Authentication> convertRbacTokenToAuthentication(RbaToken rbaToken) {
        return Optional.of(rbaToken) //
                .filter(RbaToken::isValid) //
                .filter(token -> token.getUser() != null) //
                .map(rbacToAuthenticationConverter::convert);
    }

}

