package cern.nxcals.service.security;

import cern.nxcals.service.rest.exceptions.AccessDeniedRuntimeException;
import lombok.experimental.UtilityClass;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

@UtilityClass
public class AuthenticationFacade {
    public static Authentication getAuthentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

    public static UserDetails getCurrentUser() {
        Authentication authentication = getAuthentication();
        if (authentication == null) {
            throw new AccessDeniedRuntimeException("No authentication object was found. Please, use RBAC or Kerberos.");
        }
        Object principal = authentication.getPrincipal();
        if (principal instanceof UserDetails) {
            return (UserDetails) principal;
        } else {
            throw new AccessDeniedRuntimeException(
                    String.format("Not supported authentication principals (%s). Please, use RBAC or Kerberos.",
                            principal.getClass().getName()));
        }
    }

    public static void setAuthentication(Authentication authentication) {
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }
}