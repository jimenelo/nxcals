package cern.nxcals.service.security;

import cern.nxcals.service.repository.EntityRepository;
import cern.nxcals.service.repository.SystemSpecRepository;
import cern.nxcals.service.security.resolvers.PermissionResolver;
import org.aopalliance.intercept.MethodInvocation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.expression.method.DefaultMethodSecurityExpressionHandler;
import org.springframework.security.access.expression.method.MethodSecurityExpressionOperations;
import org.springframework.security.authentication.AuthenticationTrustResolver;
import org.springframework.security.authentication.AuthenticationTrustResolverImpl;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.util.Set;

/**
 * Class extending {@link org.springframework.security.access.expression.method.DefaultMethodSecurityExpressionHandler} from Spring.
 * Method {@link RootChangeMethodSecurityExpressionHandler#createSecurityExpressionRoot(Authentication, MethodInvocation)}
 * is overridden so NXCALS custom security expression operations could be invoked in {@link @PreAuthorize} annotation.
 *
 * All the implementations of {@link cern.nxcals.service.security.resolvers.PermissionResolver} annotated by {@link org.springframework.stereotype.Component}} annotation
 * are autowired here and passed to {@link MethodSecurityExpressionRoot}.
 *
 * @author wjurasz
 * @author kpodsiad
 */
@Component
public class RootChangeMethodSecurityExpressionHandler extends DefaultMethodSecurityExpressionHandler {

    private AuthenticationTrustResolver trustResolver = new AuthenticationTrustResolverImpl();

    @Autowired
    private SystemSpecRepository systemRepository;

    @Autowired
    private EntityRepository entityRepository;

    @Autowired
    private Set<PermissionResolver> permissionResolvers;

    @Value(value = "${nxcals.security.rbac.role.prefix}")
    private String rolePrefix;

    @Override
    protected MethodSecurityExpressionOperations createSecurityExpressionRoot(
            Authentication authentication, MethodInvocation invocation) {
        MethodSecurityExpressionRoot root = new MethodSecurityExpressionRoot(authentication, systemRepository,
                entityRepository, permissionResolvers, rolePrefix);
        root.setPermissionEvaluator(getPermissionEvaluator());
        root.setTrustResolver(this.trustResolver);
        root.setRoleHierarchy(getRoleHierarchy());
        return root;
    }
}
