package cern.nxcals.service.security.rbac;

import cern.rbac.common.RbaToken;
import org.springframework.security.core.Authentication;

/**
 * @author mgabriel.
 */
public interface RbacToAuthenticationConverter {

    /**
     * @param token a valid RBAC token
     * @return the authentication for use in Spring Security
     */
    Authentication convert(RbaToken token);
}
