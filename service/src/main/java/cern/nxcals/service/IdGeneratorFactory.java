/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.service;

import cern.nxcals.common.utils.Lazy;
import cern.nxcals.service.internal.IdGenerator;
import lombok.Setter;
import lombok.experimental.UtilityClass;
import org.springframework.context.ApplicationContext;

/**
 * @author Marcin Sobieszek
 * @date Jul 26, 2016 12:08:49 PM
 */
@UtilityClass
public class IdGeneratorFactory {
    @Setter
    private static ApplicationContext context;
    private static Lazy<IdGenerator> generator = new Lazy<>(() -> context.getBean(IdGenerator.class));

    public static IdGenerator getGenerator() {
        return generator.get();
    }
}
