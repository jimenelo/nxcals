package cern.nxcals.service.rest;

import cern.nxcals.api.domain.PartitionResourceHistory;
import cern.nxcals.service.domain.PartitionResourceHistoryData;
import cern.nxcals.service.internal.PartitionResourceHistoryService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;
import java.util.stream.Collectors;

import static cern.nxcals.common.web.Endpoints.PARTITION_RESOURCE_HISTORIES;
import static cern.nxcals.common.web.Endpoints.PARTITION_RESOURCE_HISTORIES_FIND_ALL;

@RestController
@Slf4j
@RequiredArgsConstructor
public class PartitionResourceHistoryController {
    private final PartitionResourceHistoryService service;

    @PostMapping(value = PARTITION_RESOURCE_HISTORIES, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public PartitionResourceHistory create(@RequestBody PartitionResourceHistory partitionResourceInfo) {
        return service.create(partitionResourceInfo).toPartitionResourceHistory();
    }

    @PutMapping(value = PARTITION_RESOURCE_HISTORIES, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public PartitionResourceHistory update(@RequestBody PartitionResourceHistory partitionResource) {
        return service.update(partitionResource).toPartitionResourceHistory();
    }

    @DeleteMapping(value = PARTITION_RESOURCE_HISTORIES + "/{id}")
    public void delete(@PathVariable("id") long partitionResourceInfoId) {
        service.delete(partitionResourceInfoId);
    }

    @PostMapping(value = PARTITION_RESOURCE_HISTORIES_FIND_ALL, consumes = MediaType.TEXT_PLAIN_VALUE)
    public Set<PartitionResourceHistory> findAll(@RequestBody String search) {
        log.debug("QueryAll:[{}]", search);
        return service.findAll(search).stream()
                .map(PartitionResourceHistoryData::toPartitionResourceHistory)
                .collect(Collectors.toSet());
    }
}
