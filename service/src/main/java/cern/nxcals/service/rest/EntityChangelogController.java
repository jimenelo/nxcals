/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.rest;

import cern.nxcals.api.domain.EntityChangelog;
import cern.nxcals.service.domain.EntityChangelogData;
import cern.nxcals.service.internal.EntityChangelogService;
import io.micrometer.core.annotation.Timed;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;
import java.util.stream.Collectors;

import static cern.nxcals.common.web.Endpoints.ENTITY_CHANGELOGS_FIND_ALL;

@RestController
@Slf4j
@RequiredArgsConstructor
@Timed
public class EntityChangelogController {

    private final EntityChangelogService internalEntityChangelogService;

    @PostMapping(value = ENTITY_CHANGELOGS_FIND_ALL, consumes = MediaType.TEXT_PLAIN_VALUE)
    public Set<EntityChangelog> findAllEntityChangelogs(@RequestBody String search) {
        return internalEntityChangelogService.findAll(search).stream()
                .map(EntityChangelogData::toEntityChangelog).collect(Collectors.toSet());
    }

}
