package cern.nxcals.service.rest;

import cern.nxcals.common.errors.ApiError;
import cern.nxcals.service.rest.exceptions.AccessDeniedRuntimeException;
import cern.nxcals.service.rest.exceptions.ConfigDataConflictException;
import cern.nxcals.service.rest.exceptions.NotFoundRuntimeException;
import cern.nxcals.service.rest.exceptions.VersionMismatchException;
import com.google.common.base.Throwables;
import cz.jirutka.rsql.parser.RSQLParserException;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.ConcurrencyFailureException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.persistence.OptimisticLockException;
import java.util.function.Function;

import static java.lang.String.format;

@RestControllerAdvice
@Slf4j
public class GlobalControllerExceptionHandler {
    private static final Function<String, String> constraintMessageMapper = new ConstraintViolationMessageMapper();

    static final String INTERNAL_ERROR_FORMAT = "Internal Server Error: %s";
    static final String VERSION_MISMATCH_ERROR_FORMAT = "Version Mismatch Error: %s";
    static final String CONSTRAINT_VIOLATION_DEFAULT_ERROR_FORMAT = "Constraint Violation Error: %s";
    static final String NOT_FOUND_ERROR_FORMAT = "Resource Not Found: %s";
    static final String FORBIDDEN_ERROR_FORMAT = "No Permission To Access Path: %s";
    static final String DATA_CONFLICT_ERROR_FORMAT = "Permanent Conflict In Data: %s";
    static final String ILLEGAL_ARGUMENT_ERROR_FORMAT = "Illegal Argument: %s";

    @ExceptionHandler(RuntimeException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ApiError handleBasicExpression(Exception exception) {
        return ApiError.builder()
                .message(format(INTERNAL_ERROR_FORMAT, messageFrom(exception)))
                .stacktrace(Throwables.getStackTraceAsString(exception))
                .build();
    }

    @ExceptionHandler(NotFoundRuntimeException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ApiError handleNoContent(Exception exception) {
        return ApiError.builder()
                .message(format(NOT_FOUND_ERROR_FORMAT, messageFrom(exception)))
                .stacktrace(Throwables.getStackTraceAsString(exception))
                .build();
    }

    @ExceptionHandler({ AccessDeniedException.class, AccessDeniedRuntimeException.class })
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public ApiError handleAccessDenied(Exception exception) {
        return ApiError.builder()
                .message(format(FORBIDDEN_ERROR_FORMAT, messageFrom(exception)))
                .stacktrace(Throwables.getStackTraceAsString(exception))
                .build();
    }

    @ExceptionHandler({ VersionMismatchException.class, OptimisticLockException.class, ConcurrencyFailureException.class })
    //This is the closest I could get to say - please repeat this request as another session modified the record.
    //CONFLICT means that it cannot be repeated, it is a real conflict forever.
    @ResponseStatus(HttpStatus.PRECONDITION_REQUIRED)
    public ApiError handleVersionMismatchException(Exception exception) {
        return ApiError.builder()
                .message(format(VERSION_MISMATCH_ERROR_FORMAT, messageFrom(exception)))
                .stacktrace(Throwables.getStackTraceAsString(exception))
                .build();
    }

    @ExceptionHandler({ ConstraintViolationException.class, DataIntegrityViolationException.class })
    @ResponseStatus(HttpStatus.PRECONDITION_REQUIRED)
    public ApiError handleConstraintViolationException(Exception exception) {
        return ApiError.builder()
                .code("NXCALS-0001") // TODO: this is just a placeholder: codes, their structure, numbering will be resolved in: NXCALS-4009
                .message(constraintMessageMapper.apply(messageFrom(exception)))
                .stacktrace(Throwables.getStackTraceAsString(exception))
                .build();
    }

    @ExceptionHandler(ConfigDataConflictException.class)
    //This must never be repeated, it is a fatal conflict
    @ResponseStatus(HttpStatus.CONFLICT)
    public ApiError handleConfigDataConflictException(Exception exception) {
        return ApiError.builder()
                .message(format(DATA_CONFLICT_ERROR_FORMAT, messageFrom(exception)))
                .stacktrace(Throwables.getStackTraceAsString(exception))
                .repeatable(false)
                .build();
    }

    @ExceptionHandler({ IllegalArgumentException.class, RSQLParserException.class })
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ApiError handleIllegalArgumentException(Exception exception) {
        return ApiError.builder()
                .message(format(ILLEGAL_ARGUMENT_ERROR_FORMAT, messageFrom(exception)))
                .stacktrace(Throwables.getStackTraceAsString(exception))
                .repeatable(false)
                .build();
    }

    private String messageFrom(Exception exception) {
        String message = exception.getMessage();
        log.warn(message, exception);
        return message;
    }
}
