/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.rest.exceptions;

import cern.nxcals.api.domain.Group;

public class AccessDeniedRuntimeException extends RuntimeException {

    public static AccessDeniedRuntimeException notGroupOwner(Group group) {
        return new AccessDeniedRuntimeException(
                String.format("Unable to modify group [ %s ]: not the owner", group.getName()));
    }

    public static AccessDeniedRuntimeException notGroupOwner(long groupId) {
        return new AccessDeniedRuntimeException(String.format("Unable to modify group [ %s ]: not the owner", groupId));
    }

    public AccessDeniedRuntimeException(String message) {
        super(message);
    }
}
