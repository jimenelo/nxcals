package cern.nxcals.service.rest;

import com.google.common.collect.ImmutableMap;

import java.util.Map;
import java.util.function.Function;
import java.util.regex.Pattern;

import static cern.nxcals.service.rest.GlobalControllerExceptionHandler.CONSTRAINT_VIOLATION_DEFAULT_ERROR_FORMAT;
import static java.lang.String.format;

public class ConstraintViolationMessageMapper implements Function<String, String> {
    private Map<Pattern, String> messages = ImmutableMap.<Pattern, String>builder()
            .put(Pattern.compile(".*ORA-00001: unique constraint \\(.*GV_UK\\) violated.*"), "Association with Variable already exists")
            .put(Pattern.compile(".*ORA-00001: unique constraint \\(.*GE_UK\\) violated.*"), "Association with Entity already exists")
            .build();

    @Override
    public String apply(String message) {
        for (Map.Entry<Pattern, String> pattern : messages.entrySet()) {
            if (pattern.getKey().matcher(message).matches()) {
                return pattern.getValue();
            }
        }
        return format(CONSTRAINT_VIOLATION_DEFAULT_ERROR_FORMAT, message);
    }
}