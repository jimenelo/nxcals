package cern.nxcals.service.config;

import cern.nxcals.service.internal.hdfs.DateGroupingType;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.time.temporal.ChronoUnit;

@ConfigurationProperties(prefix = "data.location")
@Data
@NoArgsConstructor
public class DataLocationProperties {
    private String hbaseNamespace;
    private String hdfsDataDir;
    private String hdfsStagingDir;
    private String hdfsRestagingDir;
    private String suffix;
    private ChronoUnit dateGranularity = ChronoUnit.DAYS;
    private DateGroupingType dateGroupingType = DateGroupingType.DAYS;
    private int hbaseRpcTimeout = 60000;
    private int hbaseClientOperationTimeout = 1200000;
}
