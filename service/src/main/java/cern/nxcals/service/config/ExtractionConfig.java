package cern.nxcals.service.config;

import cern.nxcals.service.domain.SystemSpecData;
import cern.nxcals.service.internal.extraction.Utils;
import cern.nxcals.service.repository.SystemSpecRepository;
import org.apache.avro.Schema;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

@Configuration
public class ExtractionConfig {

    @Bean
    public Function<String, List<Schema.Field>> systemDefinitionFieldsProvider(SystemSpecRepository systemRepository) {
        return name -> {
            SystemSpecData systemSpec = getSystemSpecData(systemRepository, name);
            List<Schema.Field> fields = new ArrayList<>();
            fields.addAll(Utils.extractSchemaFields(systemSpec.getEntityKeyDefs()));
            fields.addAll(Utils.extractSchemaFields(systemSpec.getPartitionKeyDefs()));
            fields.addAll(Utils.extractSchemaFields(systemSpec.getRecordVersionKeyDefs()));
            return fields;
        };
    }

    @Bean
    public Function<String, Schema.Field> timestampFieldProvider(SystemSpecRepository systemRepository) {
        return name -> {
            SystemSpecData systemSpec = getSystemSpecData(systemRepository, name);
            return Utils.extractSchemaFields(systemSpec.getTimeKeyDefs()).get(0);
        };
    }

    private SystemSpecData getSystemSpecData(SystemSpecRepository systemRepository, String name) {
        return systemRepository.findByName(name)
                .orElseThrow(() -> new IllegalArgumentException("No system found " + "with name " + name));
    }

}
