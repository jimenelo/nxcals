package cern.nxcals.service.config;

import cern.nxcals.api.domain.EntityHistory;
import cern.nxcals.service.internal.hdfs.DateGrouping;
import lombok.AllArgsConstructor;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Profile;

import java.io.IOException;
import java.util.function.Function;

import static cern.nxcals.common.Constants.HBASE_TABLE_NAME_DELIMITER;
import static cern.nxcals.common.Constants.PERMANENT_TABLE_PREFIX;

@Configuration
@AllArgsConstructor
public class DataAccessConfig {

    private final DataLocationProperties properties;

    @Bean
    @Profile("!single-table")
    @Qualifier("hbaseTableProvider")
    public Function<EntityHistory, String> multiTable() {
        return entityHistory -> {
            String tableName = String.join(HBASE_TABLE_NAME_DELIMITER,
                    String.valueOf(entityHistory.getPartition().getSystemSpec().getId()),
                    String.valueOf(entityHistory.getPartition().getId()),
                    String.valueOf(entityHistory.getEntitySchema().getId()));
            if (entityHistory.getPartition().getProperties().isUpdatable()) {
                tableName = PERMANENT_TABLE_PREFIX + HBASE_TABLE_NAME_DELIMITER + tableName;
            }
            return tableName;
        };
    }

    @Bean
    @Profile("single-table")
    @Qualifier("hbaseTableProvider")
    public Function<EntityHistory, String> singleTable() {
        return entityHistory -> String.valueOf(entityHistory.getPartition().getSystemSpec().getId());
    }

    @Bean
    @Qualifier("hdfsPathProvider")
    public Function<EntityHistory, String> hdfsPathPrefix() {
        return entityHistory -> String.join(Path.SEPARATOR,
                properties.getHdfsDataDir(),
                String.valueOf(entityHistory.getPartition().getSystemSpec().getId()),
                String.valueOf(entityHistory.getPartition().getId()),
                String.valueOf(entityHistory.getEntitySchema().getId()));
    }

    @Bean
    public DateGrouping pathBuilder() {
        return new DateGrouping(properties.getDateGranularity(), properties.getDateGroupingType());
    }

    @Bean
    @DependsOn("kerberos")
    public Connection hbaseConnection() throws IOException {
        org.apache.hadoop.conf.Configuration config = HBaseConfiguration.create();
        config.setInt("hbase.rpc.timeout", properties.getHbaseRpcTimeout());
        config.setInt("hbase.client.operation.timeout", properties.getHbaseClientOperationTimeout());

        return ConnectionFactory.createConnection(HBaseConfiguration.create(config));
    }
}