package cern.nxcals.service.config;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.time.Duration;
import java.time.temporal.ChronoUnit;

/**
 * Tracing filter properties with defaults.
 */
@ConfigurationProperties("tracing")
@Data
@NoArgsConstructor
public class TracingProperties {
    private boolean includeQueryString = true;
    private boolean includePayload = true;
    private int maxPayloadLength = 10000;
    private boolean includeHeaders = false;
    private boolean includeClientInfo = true;
    private int maxTopElements = 100;
    private Duration inactivityDuration = Duration.of(2, ChronoUnit.HOURS);
    private boolean removeUserRealm = true;
    private Duration displayStatsFreq = Duration.of(60, ChronoUnit.SECONDS);
}
