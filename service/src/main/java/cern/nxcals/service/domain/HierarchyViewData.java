package cern.nxcals.service.domain;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Setter
@Getter
@Table(name = "v_hierarchy_paths")
public class HierarchyViewData implements Serializable {
    @Id
    @Column(name = "hierarchy_id")
    private long id;

    @Column(name = "node_path", updatable = false, insertable = false)
    private String nodePath;

    @Column(name = "leaf", updatable = false, insertable = false)
    @Convert(disableConversion = true)
    @Type(type = "org.hibernate.type.NumericBooleanType")
    private boolean leaf;

    @Column(name = "level_no", updatable = false, insertable = false)
    private int level;
}