package cern.nxcals.service.domain;

import cern.nxcals.api.domain.Visibility;

import java.util.Arrays;

public enum VisibilityData {
    PRIVATE, PROTECTED, PUBLIC;

    private static final String VISIBILITY_OPTIONS = Arrays.toString(Visibility.values());

    public static VisibilityData of(Visibility visibility) {
        if (visibility == Visibility.PUBLIC) {
            return VisibilityData.PUBLIC;
        }
        return VisibilityData.PROTECTED;
    }

    public Visibility toVisibility() {
        if (this == VisibilityData.PUBLIC) {
            return Visibility.PUBLIC;
        } else if (this == VisibilityData.PROTECTED) {
            return Visibility.PROTECTED;
        }
        throw new IllegalStateException(
                String.format("Cannot convert [ %s ] to visibility! Available options: [%s]"
                        , this.name(), VISIBILITY_OPTIONS));
    }
}
