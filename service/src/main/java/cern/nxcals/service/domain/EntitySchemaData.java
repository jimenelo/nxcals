/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.domain;

import cern.nxcals.api.domain.EntitySchema;
import cern.nxcals.common.utils.ReflectionUtils;
import cern.nxcals.service.exception.SchemaOverrideException;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import static cern.nxcals.service.domain.SequenceType.SCHEMA;

@Entity
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
@Table(name = "SCHEMAS")
@SuppressWarnings("squid:S2160") //override equals while we do it in the base class - jwozniak
public class EntitySchemaData extends StandardPersistentEntityWithVersion {

    private static final long serialVersionUID = 39304950565223841L;
    private String content;
    private String contentHash;

    @Id
    @Column(name = "SCHEMA_ID")
    @Override
    public Long getId() {
        return super.getId();
    }

    @Override
    protected SequenceType sequenceType() {
        return SCHEMA;
    }

    @NotNull
    @Column(name = "SCHEMA_CONTENT", columnDefinition = "CLOB", nullable = false)
    public String getContent() {
        return content;
    }

    public void setContent(String value) {
        if (content == null) {
            content = value;
        } else if (!content.equals(value)) {
            throw new SchemaOverrideException("Cannot change the schema content for schema " + getId());
        }
    }

    @NotNull
    @Column(name = "SCHEMA_CONTENT_HASH", nullable = false)
    public String getContentHash() {
        return contentHash;
    }

    public void setContentHash(String hash) {
        if (contentHash == null) {
            contentHash = hash;
        } else if (!contentHash.equals(hash)) {
            throw new SchemaOverrideException("Cannot change the schema content hash for schema " + getId());
        }
    }

    public EntitySchema toEntitySchema() {
        return ReflectionUtils.builderInstance(EntitySchema.InnerBuilder.class).id(getId()).schemaJson(getContent()).build();
    }
}
