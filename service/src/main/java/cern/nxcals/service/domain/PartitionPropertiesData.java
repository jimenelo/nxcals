package cern.nxcals.service.domain;

import cern.nxcals.api.domain.PartitionProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PartitionPropertiesData implements Serializable {


    private static final long serialVersionUID = 6293229615670043778L;

    private Boolean updatable;

    public PartitionProperties toPartitionProperties() {
        return new PartitionProperties(updatable);
    }

}
