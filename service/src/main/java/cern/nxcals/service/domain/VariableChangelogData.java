package cern.nxcals.service.domain;

import cern.nxcals.api.domain.OperationType;
import cern.nxcals.api.domain.VariableChangelog;
import cern.nxcals.api.domain.VariableDeclaredType;
import cern.nxcals.common.utils.ReflectionUtils;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.hibernate.annotations.Immutable;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.Instant;

@Entity
@Getter
@Table(name = "VARIABLES_CHANGELOG")
@Immutable
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class VariableChangelogData implements Serializable {
    private static final long serialVersionUID = 3665546057712833699L;

    @Id
    @EqualsAndHashCode.Include
    private long id;
    private long variableId;
    private String oldVariableName;
    private String newVariableName;
    private String oldDescription;
    private String newDescription;
    private String oldUnit;
    private String newUnit;
    private Long oldSystemId;
    private Long newSystemId;
    @Enumerated(EnumType.STRING)
    private VariableDeclaredType oldDeclaredType;
    @Enumerated(EnumType.STRING)
    private VariableDeclaredType newDeclaredType;
    private OperationType opType;
    private Instant createTimeUtc;
    private String transactionId;
    private String module;
    private String action;
    private String clientInfo;
    private Long recVersion;

    public VariableChangelog toVariableChangelog() {
        return ReflectionUtils.builderInstance(VariableChangelog.InnerBuilder.class)
                .id(getId())
                .variableId(getVariableId())
                .oldVariableName(getOldVariableName()).newVariableName(getNewVariableName())
                .oldDescription(getOldDescription()).newDescription(getNewDescription())
                .oldUnit(getOldUnit()).newUnit(getNewUnit()).oldSystemId(getOldSystemId())
                .newSystemId(getNewSystemId()).oldDeclaredType(getOldDeclaredType())
                .newDeclaredType(getNewDeclaredType())
                .createTimeUtc(getCreateTimeUtc()).opType(getOpType()).clientInfo(getClientInfo())
                .build();
    }

}
