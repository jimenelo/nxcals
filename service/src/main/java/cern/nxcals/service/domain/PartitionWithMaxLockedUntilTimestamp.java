package cern.nxcals.service.domain;

import lombok.Data;

import java.time.Instant;

/**
 * This pojo is used to return a typesafe projection of partitionId with max locked until timestamp.
 */
@Data
public class PartitionWithMaxLockedUntilTimestamp {
    private final Long id;
    private final Instant maxLockedUntilTimestamp;
}
