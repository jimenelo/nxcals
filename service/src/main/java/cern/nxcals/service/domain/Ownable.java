package cern.nxcals.service.domain;

import static org.apache.commons.lang3.StringUtils.isEmpty;

public interface Ownable {
    String getOwner();

    default boolean hasOwner() {
        return !isEmpty(getOwner());
    }
}
