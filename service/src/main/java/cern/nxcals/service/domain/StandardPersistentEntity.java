/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.service.domain;

import cern.nxcals.service.exception.IdentityOverrideException;
import cern.nxcals.service.internal.IdGenerator;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

/**
 * Defines class identity as equality of class identifiers. An identity if not set is generated for the first time when
 * its {code getId()} method gets called. Thanks to that we can use class instances in hash based data structures
 * without having to persist them first.
 * An identity once set cannot be changed.
 * Base class for all persistent objects which all have to be versioned {@see JPA's optimistic locking facility}.
 * In fact when we use generated ID we have to use the version.
 * Otherwise we can get into hibernate troubles in recognizing the new objects correctly.
 * NOTE: Please DO NOT INHERIT from this class directly, use @{@link StandardPersistentEntityWithVersion} to have required version.
 * This class in only here since it is impossible to have both id & record in one and use annotation MappedSuperclass as we have to override id in the inheriting classes.
 *
 * @author Marcin Sobieszek
 * @author jwozniak
 * @date Jul 19, 2016 10:33:38 AM
 */
@Slf4j
@ToString
public abstract class StandardPersistentEntity implements PersistentEntity {
    private static final long serialVersionUID = 5571431926723951019L;
    private Long id;

    @Override
    public Long getId() {
        if (this.id == null) {
            this.id = IdGenerator.getDetault().getIdFor(this.sequenceType());
            log.trace("Got id={} for sequenceType={}", id,this.sequenceType());
        }
        return this.id;
    }

    public void setId(Long id) {
        if (this.id != null && !this.id.equals(id)) {
                throw new IdentityOverrideException("Cannot override already set id " + this.id + ", with " + id);
        }
        this.id = id;
    }

    protected SequenceType sequenceType() {
        return SequenceType.DEFAULT;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof StandardPersistentEntity)) {
            return false;
        }
        StandardPersistentEntity other = (StandardPersistentEntity) object;
        return this.getId().equals(other.getId());
    }

    @Override
    public int hashCode() {
        return this.getId().hashCode();
    }

}
