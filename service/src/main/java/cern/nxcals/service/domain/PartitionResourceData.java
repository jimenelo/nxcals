package cern.nxcals.service.domain;

import cern.nxcals.api.domain.PartitionResource;
import cern.nxcals.common.utils.ReflectionUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "PARTITION_RESOURCES")
@Setter
@Getter
@Access(value = AccessType.FIELD)
@ToString(callSuper = true)
@SuppressWarnings("squid:S2160") //override equals while we do it in the base class
public class PartitionResourceData extends StandardPersistentEntityWithVersion {

    @NotNull
    @ManyToOne
    @JoinColumn(name = "system_id")
    private SystemSpecData system;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "partition_id")
    private PartitionData partition;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "schema_id")
    private EntitySchemaData schema;

    @Id
    @Column(name = "PARTITION_RESOURCE_ID")
    @Access(value =  AccessType.PROPERTY)
    @Override
    public Long getId() { return super.getId(); }

    @Override
    protected SequenceType sequenceType() {
        return SequenceType.PARTITION_RESOURCE;
    }

    public PartitionResource toPartitionResource() {
        return ReflectionUtils.builderInstance(PartitionResource.InnerBuilder.class)
                .id(getId())
                .systemSpec(getSystem().toSystemSpec())
                .partition(getPartition().toPartition())
                .schema(getSchema().toEntitySchema())
                .recVersion(getRecVersion()).build();
    }

}
