package cern.nxcals.service.domain;

import cern.nxcals.api.domain.HierarchyVariablesChangelog;
import cern.nxcals.api.domain.OperationType;
import cern.nxcals.common.utils.ReflectionUtils;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.hibernate.annotations.Immutable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.Instant;

@Entity
@Getter
@Table(name = "V_HIERARCHY_VARS_CHANGELOG")
@Immutable
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class HierarchyVariablesChangelogData implements Serializable {
    private static final long serialVersionUID = 3665546057712843699L;

    @Id
    @EqualsAndHashCode.Include
    private long id;
    private Long oldVariableId;
    private Long newVariableId;
    private Long oldHierarchyId;
    private Long newHierarchyId;
    private Long oldSystemId;
    private Long newSystemId;
    private OperationType opType;
    private Instant createTimeUtc;
    private String transactionId;
    private String module;
    private String action;
    private String clientInfo;

    public HierarchyVariablesChangelog toHierarchyVariablesChangelog() {
        return ReflectionUtils.builderInstance(HierarchyVariablesChangelog.InnerBuilder.class).id(getId())
                .oldVariableId(getOldVariableId()).newVariableId(getNewVariableId())
                .oldHierarchyId(getOldHierarchyId())
                .newHierarchyId(getNewHierarchyId())
                .oldSystemId(getOldSystemId())
                .newSystemId(getNewSystemId())
                .createTimeUtc(getCreateTimeUtc()).opType(getOpType())
                .clientInfo(getClientInfo())
                .build();
    }
}
