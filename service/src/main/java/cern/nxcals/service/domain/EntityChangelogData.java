package cern.nxcals.service.domain;

import cern.nxcals.api.domain.EntityChangelog;
import cern.nxcals.api.domain.OperationType;
import cern.nxcals.common.utils.ReflectionUtils;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.hibernate.annotations.Immutable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.Instant;

import static cern.nxcals.common.utils.KeyValuesUtils.convertKeyValuesStringIntoMap;

@Entity
@Getter
@Immutable
@Table(name = "ENTITIES_CHANGELOG")
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class EntityChangelogData implements Serializable {
    private static final long serialVersionUID = 3665546057712833699L;

    @Id
    @EqualsAndHashCode.Include
    private long id;
    private long entityId;
    private String oldKeyValues;
    private String newKeyValues;
    private Long oldPartitionId;
    private Long newPartitionId;
    private Long oldSystemId;
    private Long newSystemId;
    private Instant newLockedUntilStamp;
    private Instant oldLockedUntilStamp;
    private Instant createTimeUtc;
    private OperationType opType;
    private String transactionId;
    private String module;
    private String action;
    private String clientInfo;
    private Long recVersion;

    public EntityChangelog toEntityChangelog() {
        EntityChangelog.InnerBuilder builder = ReflectionUtils.builderInstance(EntityChangelog.InnerBuilder.class)
                .id(getId())
                .oldLockedUntilStamp(getOldLockedUntilStamp())
                .newLockedUntilStamp(getNewLockedUntilStamp()).oldPartitionId(getOldPartitionId())
                .newPartitionId(getNewPartitionId())
                .oldSystemId(getOldSystemId()).newSystemId(getNewSystemId()).createTimeUtc(getCreateTimeUtc())
                .entityId(getEntityId())
                .opType(getOpType()).clientInfo(getClientInfo());
        if (getOldKeyValues() != null) {
            builder = builder.oldKeyValues(convertKeyValuesStringIntoMap(getOldKeyValues()));
        }
        if (getNewKeyValues() != null) {
            builder = builder.newKeyValues(convertKeyValuesStringIntoMap(getNewKeyValues()));
        }
        return builder.build();
    }
}
