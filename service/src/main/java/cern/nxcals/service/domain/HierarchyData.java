package cern.nxcals.service.domain;

import cern.nxcals.api.domain.Hierarchy;
import cern.nxcals.api.domain.HierarchyView;
import cern.nxcals.common.utils.ReflectionUtils;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedEntityGraphs;
import javax.persistence.NamedSubgraph;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static cern.nxcals.service.domain.SequenceType.HIERARCHY;

@NamedEntityGraphs({
        @NamedEntityGraph(
                name = HierarchyData.WITH_VARIABLES,
                attributeNodes = {
                        @NamedAttributeNode("system"),
                        @NamedAttributeNode(value = "group", subgraph = GroupData.WITH_VARIABLES),
                },
                subgraphs = {
                        @NamedSubgraph(
                                name = GroupData.WITH_VARIABLES,
                                attributeNodes = {
                                        @NamedAttributeNode(value = "variables", subgraph = VariableDataWithAssociation.WITH_VARIABLE),
                                }
                        ),
                        @NamedSubgraph(
                                name = VariableDataWithAssociation.WITH_VARIABLE,
                                attributeNodes = {
                                        @NamedAttributeNode(value = "variable", subgraph = VariableData.WITH_CONFIGS)
                                }
                        ),
                        @NamedSubgraph(
                                name = VariableData.WITH_CONFIGS,
                                attributeNodes = {
                                        @NamedAttributeNode("system"),
                                        @NamedAttributeNode(value = "variableConfigs", subgraph = VariableConfigData.WITH_ENTITY),
                                }
                        ),
                        @NamedSubgraph(
                                name = VariableConfigData.WITH_ENTITY,
                                attributeNodes = {
                                        @NamedAttributeNode(value = "variable"),
                                        @NamedAttributeNode(value = "entity", subgraph = EntityData.WITH_PARTITION)
                                }
                        ),
                        @NamedSubgraph(
                                name = EntityData.WITH_PARTITION,
                                attributeNodes = {
                                        @NamedAttributeNode(value = "partition", subgraph = PartitionData.WITH_SYSTEM)
                                }
                        ),
                        @NamedSubgraph(
                                name = PartitionData.WITH_SYSTEM,
                                attributeNodes = {
                                        @NamedAttributeNode("system")
                                }
                        )
                }
        ),
        @NamedEntityGraph(
                name = HierarchyData.WITH_VARIABLES_FOR_VARIABLES_REMOVAL,
                attributeNodes = {
                        @NamedAttributeNode(value = "group", subgraph = GroupData.WITH_VARIABLES),
                },
                subgraphs = {
                        @NamedSubgraph(
                                name = GroupData.WITH_VARIABLES,
                                attributeNodes = {
                                        @NamedAttributeNode(value = "variables", subgraph = VariableDataWithAssociation.WITH_VARIABLE),
                                }
                        ),
                        @NamedSubgraph(
                                name = VariableDataWithAssociation.WITH_VARIABLE,
                                attributeNodes = {
                                        @NamedAttributeNode(value = "variable")
                                }
                        )
                }
        ),
        @NamedEntityGraph(
                name = HierarchyData.WITH_VARIABLE_ASSOCIATIONS,
                attributeNodes = {
                        @NamedAttributeNode(value = "group", subgraph = GroupData.WITH_VARIABLES),
                },
                subgraphs = {
                        @NamedSubgraph(
                                name = GroupData.WITH_VARIABLES,
                                attributeNodes = {
                                        @NamedAttributeNode(value = "variables"),
                                }
                        )
                }
        ),
        @NamedEntityGraph(
                name = HierarchyData.WITH_ENTITIES,
                attributeNodes = {
                        @NamedAttributeNode("system"),
                        @NamedAttributeNode(value = "group", subgraph = GroupData.WITH_ENTITIES),
                },
                subgraphs = {
                        @NamedSubgraph(
                                name = GroupData.WITH_ENTITIES,
                                attributeNodes = {
                                        @NamedAttributeNode(value = "entities", subgraph = EntityDataWithAssociation.WITH_ENTITY)
                                }
                        ),
                        @NamedSubgraph(
                                name = EntityDataWithAssociation.WITH_ENTITY,
                                attributeNodes = {
                                        @NamedAttributeNode(value = "entity", subgraph = EntityData.WITH_PARTITION)
                                }
                        ),
                        @NamedSubgraph(
                                name = EntityData.WITH_PARTITION,
                                attributeNodes = {
                                        @NamedAttributeNode(value = "partition", subgraph = PartitionData.WITH_SYSTEM)
                                }
                        ),
                        @NamedSubgraph(
                                name = PartitionData.WITH_SYSTEM,
                                attributeNodes = {
                                        @NamedAttributeNode("system")
                                }
                        )
                }
        ),
        @NamedEntityGraph(
                name = HierarchyData.PLAIN,
                attributeNodes = {
                        @NamedAttributeNode("system"),
                        @NamedAttributeNode(value = "parent", subgraph = HierarchyData.MINIMUM),
                        @NamedAttributeNode(value = "children", subgraph = HierarchyData.MINIMUM),
                        @NamedAttributeNode(value = "group", subgraph = GroupData.MINIMUM),
                        @NamedAttributeNode("viewData"),
                },
                subgraphs = {
                        @NamedSubgraph(
                                name = HierarchyData.MINIMUM,
                                attributeNodes = {
                                        @NamedAttributeNode("system"),
                                        @NamedAttributeNode(value = "group", subgraph = GroupData.MINIMUM),
                                        @NamedAttributeNode("viewData"),
                                }
                        ),
                        @NamedSubgraph(
                                name = GroupData.MINIMUM,
                                attributeNodes = {
                                        @NamedAttributeNode("system"),
                                }
                        )
                }
        ),
        @NamedEntityGraph(
                name = HierarchyData.MINIMUM,
                attributeNodes = {
                        @NamedAttributeNode("system"),
                        @NamedAttributeNode(value = "group", subgraph = GroupData.MINIMUM),
                },
                subgraphs = {
                        @NamedSubgraph(
                                name = GroupData.MINIMUM,
                                attributeNodes = {
                                        @NamedAttributeNode("system"),
                                }
                        )
                }
        ),
        @NamedEntityGraph(
                name = HierarchyData.MINIMUM_WITH_PATH,
                attributeNodes = {
                        @NamedAttributeNode("system"),
                        @NamedAttributeNode("viewData"),
                        @NamedAttributeNode(value = "group", subgraph = GroupData.MINIMUM),
                },
                subgraphs = {
                        @NamedSubgraph(
                                name = GroupData.MINIMUM,
                                attributeNodes = {
                                        @NamedAttributeNode("system"),
                                }
                        )
                }
        )
})
@Entity
@Setter
@Getter
@Table(name = "hierarchies")
@Access(value = AccessType.FIELD)
@SuppressWarnings({ "squid:S2160", "squid:S1710" }) //override equals while we do it in the base class
public class HierarchyData extends StandardPersistentEntityWithVersion {
    public static final String WITH_ENTITIES = "hierarchies-with-entities";
    public static final String WITH_VARIABLES = "hierarchies-with-variables";
    public static final String WITH_VARIABLES_FOR_VARIABLES_REMOVAL = "hierarchies-with-variables-for-variables-removal";
    public static final String WITH_VARIABLE_ASSOCIATIONS = "hierarchies-with-variable-associations";
    public static final String PLAIN = "hierarchies-without-variables-and-entities";
    public static final String MINIMUM = "hierarchies-minimum";
    public static final String MINIMUM_WITH_PATH = "hierarchies-minimum-with-path";

    private static final long serialVersionUID = 1610359187630026864L;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "group_id")
    private GroupData group;

    /**
     * Fetch type LAZY here in order to avoid the N+1 extra queries per self-referenced instances of this class.
     * Extra queries per each child and parent properties, that "live" outside the JPA Entity Graph context.
     * The lazy strategy kicks-in on the self-referenced instances that try to eagerly load their parent reference!
     * There we don't need to fetch the parent reference, but, with the default fetch strategy, hibernate
     * is trying to load it on class initialization (initial/intended db query executed by that time).
     * This results to extra queries to fetch properties that are never being used, as for child/parent
     * references we map only properties required by {@link HierarchyView} DTO.
     */
    @ManyToOne(cascade = { CascadeType.REFRESH, CascadeType.DETACH, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @JoinColumn(name = "parent_id")
    private HierarchyData parent;

    @OneToOne
    @JoinColumn(name = "system_id")
    private SystemSpecData system;

    @OneToMany(mappedBy = "parent")
    private Set<HierarchyData> children = new HashSet<>();

    @ManyToOne(cascade = { CascadeType.REFRESH, CascadeType.DETACH, // ManyToOne just for Lazy
            CascadeType.MERGE }, fetch = FetchType.LAZY)
    @JoinColumn(name = "hierarchy_id", updatable = false, insertable = false)
    private HierarchyViewData viewData;

    @Column(name = "parent_id", updatable = false, insertable = false)
    @Setter(AccessLevel.PRIVATE)
    private Long parentId;

    @Column(name = "hierarchy_name")
    private String name;

    @Id
    @Column(name = "hierarchy_id")
    @Override
    @Access(value = AccessType.PROPERTY)
    public Long getId() {
        return super.getId();
    }

    @Override
    protected SequenceType sequenceType() {
        return HIERARCHY;
    }

    public Hierarchy toHierarchy() {
        return toBuilder()
                .children(getChildren().stream().map(HierarchyData::toHierarchyView).collect(Collectors.toSet()))
                .parent(Optional.ofNullable(getParent()).map(HierarchyData::toHierarchyView).orElse(null))
                .build();
    }

    private HierarchyView toHierarchyView() {
        return toBuilder().build();
    }

    private Hierarchy.InnerBuilder toBuilder() {
        return ReflectionUtils.builderInstance(Hierarchy.InnerBuilder.class)
                .id(getId())
                .name(getName())
                .systemSpec(getSystem().toSystemSpec())
                .description(getGroup().getDescription())
                .level(getViewData().getLevel())
                .leaf(getViewData().isLeaf())
                .nodePath(getViewData().getNodePath())
                .recVersion(getRecVersion());
    }
}
