package cern.nxcals.service.internal;

import cern.nxcals.api.domain.EntitySchema;
import cern.nxcals.api.domain.Partition;
import cern.nxcals.api.domain.PartitionResource;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.service.domain.EntitySchemaData;
import cern.nxcals.service.domain.PartitionData;
import cern.nxcals.service.domain.PartitionResourceData;
import cern.nxcals.service.domain.SystemSpecData;
import cern.nxcals.service.repository.PartitionRepository;
import cern.nxcals.service.repository.PartitionResourceRepository;
import cern.nxcals.service.repository.SchemaRepository;
import cern.nxcals.service.repository.SystemSpecRepository;
import cern.nxcals.service.rest.exceptions.ConfigDataConflictException;
import cern.nxcals.service.rest.exceptions.NotFoundRuntimeException;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Optional;


@Service
@Slf4j
@AllArgsConstructor
public class PartitionResourceService {

    @NonNull
    @PersistenceContext
    private final EntityManager em;
    @NonNull
    private final PartitionResourceRepository repository;
    @NonNull
    private final SystemSpecRepository systemRepository;
    @NonNull
    private final PartitionRepository partitionRepository;
    @NonNull
    private final SchemaRepository schemaRepository;

    @Transactional(transactionManager = "jpaTransactionManager", readOnly = true)
    public Optional<PartitionResourceData> findById(long id) {
        return repository.findById(id);
    }

    @Transactional(transactionManager = "jpaTransactionManager", readOnly = true)
    public List<PartitionResourceData> findAll(String search){
        return repository.queryAll(search, PartitionResourceData.class);
    }

    @Transactional(transactionManager = "jpaTransactionManager", readOnly = true)
    public Optional<PartitionResourceData> findBySystemIdAndPartitionIdAndSchemaId(long systemId, long partitionId, long schemaId) {
        return repository.findBySystemIdAndPartitionIdAndSchemaId(systemId, partitionId, schemaId);
    }

    @Transactional(transactionManager = "jpaTransactionManager")
    public PartitionResourceData create(@NonNull PartitionResource partitionResource) {
        log.debug("Creating new PartitionResourceData from {}", partitionResource);
        if (partitionResource.hasId()) {
            throw new ConfigDataConflictException(
                    String.format("There is already a Partition Resource with id= %d", partitionResource.getId()));
        }
        PartitionResourceData result = new PartitionResourceData();
        PartitionResourceData res = save(copyFrom(result, partitionResource));
        log.debug("PartitionResourceData created {}", res);
        em.flush(); //we have to flush in order for the next find to find the created object.
        return res;
    }

    @Transactional(transactionManager = "jpaTransactionManager")
    public PartitionResourceData update(@NonNull PartitionResource partitionResource) {
        PartitionResourceData existingPartitionResource = obtainFrom(partitionResource);
        em.detach(existingPartitionResource);
        PartitionResourceData newPR = copyFrom(existingPartitionResource, partitionResource);
        PartitionResourceData saved = save(newPR);
        em.flush();
        return saved;
    }

    @Transactional(transactionManager = "jpaTransactionManager")
    public void delete(long partitionResourceDataId) {
        repository.deleteById(partitionResourceDataId);
        em.flush();
    }

    @Transactional(transactionManager = "jpaTransactionManager")
    public PartitionResourceData findOrCreatePartitionResourceData(long systemId, long partitionId, long schemaId) {
        log.debug("Trying to find PartitionResourceData for system={}, partition={}, schema={}", systemId, partitionId,
                schemaId);

        PartitionResourceData partitionResourceData = findBySystemIdAndPartitionIdAndSchemaId(systemId, partitionId,
                schemaId).orElseGet(() -> {
            log.debug("PartitionResourceData not found, creating a new one...");
            SystemSpec systemSpec = this.systemRepository.findById(systemId)
                    .orElseThrow(() -> new IllegalArgumentException("No such system id=" + systemId)).toSystemSpec();
            Partition partition = this.partitionRepository.findById(partitionId)
                    .orElseThrow(() -> new IllegalArgumentException("No such partition id=" + partitionId))
                    .toPartition();
            EntitySchema entitySchema = this.schemaRepository.findById(schemaId)
                    .orElseThrow(() -> new IllegalArgumentException("No such schema id=" + schemaId)).toEntitySchema();

            return create(
                    PartitionResource.builder().partition(partition).schema(entitySchema).systemSpec(systemSpec)
                            .build());

        });

        log.debug("Found PartitionResourceData={}", partitionResourceData);
        return partitionResourceData;
    }

    // Utility functions
    private PartitionResourceData obtainById(long id) {
        return repository.findById(id).orElseThrow(() -> NotFoundRuntimeException.missingPartitionResource(id));
    }

    private PartitionResourceData obtainFrom(PartitionResource partitionResource) {
        return obtainById(partitionResource.getId());
    }

    private PartitionResourceData save(PartitionResourceData partitionResource) {
        return repository.save(partitionResource);
    }

    private PartitionResourceData copyFrom(PartitionResourceData result,
            PartitionResource partitionResource) {
        SystemSpecData system = findSystemSpecDataOrThrow(partitionResource.getSystemSpec());
        PartitionData partition = findPartitionDataOrThrow(partitionResource.getPartition());
        EntitySchemaData schema = findEntitySchemaDataOrThrow(partitionResource.getSchema());
        result.setSystem(system);
        result.setPartition(partition);
        result.setSchema(schema);
        if (partitionResource.hasVersion()) {
            result.setRecVersion(partitionResource.getRecVersion());
        }
        return result;
    }

    private SystemSpecData findSystemSpecDataOrThrow(SystemSpec system) {
        return systemRepository.findById(system.getId()).orElseThrow(() -> NotFoundRuntimeException.missingSystem(system));
    }

    private PartitionData findPartitionDataOrThrow(Partition partition) {
        return partitionRepository.findById(partition.getId()).orElseThrow(() -> NotFoundRuntimeException.missingPartition(partition.getId()));
    }

    private EntitySchemaData findEntitySchemaDataOrThrow(EntitySchema schema) {
        return schemaRepository.findById(schema.getId()).orElseThrow(() -> NotFoundRuntimeException.missingSchema(schema.getId()));
    }
}
