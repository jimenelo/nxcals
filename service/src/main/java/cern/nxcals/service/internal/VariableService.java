/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.service.internal;

import cern.nxcals.api.domain.Identifiable;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.domain.VariableConfig;
import cern.nxcals.api.domain.Versionable;
import cern.nxcals.api.extraction.metadata.queries.Variables;
import cern.nxcals.api.metadata.queries.VariableQueryWithOptions;
import cern.nxcals.service.domain.EntityData;
import cern.nxcals.service.domain.StandardPersistentEntityWithVersion;
import cern.nxcals.service.domain.SystemSpecData;
import cern.nxcals.service.domain.VariableConfigData;
import cern.nxcals.service.domain.VariableData;
import cern.nxcals.service.internal.component.BulkUtils;
import cern.nxcals.service.repository.EntityRepository;
import cern.nxcals.service.repository.SystemSpecRepository;
import cern.nxcals.service.repository.VariableRepository;
import cern.nxcals.service.rest.exceptions.ConfigDataConflictException;
import cern.nxcals.service.rest.exceptions.NotFoundRuntimeException;
import com.github.rutledgepaulv.qbuilders.conditions.Condition;
import com.google.common.collect.Sets;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import static cern.nxcals.common.utils.RSQLUtils.toRSQL;
import static cern.nxcals.service.internal.metadata.QueryOptionsProcessors.createQueryProcessor;
import static cern.nxcals.service.internal.metadata.QueryOptionsProcessors.createResultProcessor;
import static java.util.stream.Collectors.toSet;

/**
 * The implementation of the {@link cern.nxcals.service.internal.VariableService} interface.
 */
@Service
@Slf4j
@AllArgsConstructor
public class VariableService {
    @NonNull
    @PersistenceContext
    private final EntityManager entityManager;
    @NonNull
    private final VariableRepository variableRepository;
    @NonNull
    private final EntityRepository entityRepository;
    @NonNull
    private final SystemSpecRepository systemRepository;

    @Transactional(transactionManager = "jpaTransactionManager")
    public VariableData create(@NonNull Variable variable) {
        VariableData data = copyProperties(new VariableData(), variable);
        return save(data);
    }

    @Transactional(transactionManager = "jpaTransactionManager")
    public Set<VariableData> createAll(@NonNull Set<Variable> variables) {
        Set<VariableData> data = variables.stream().map(v -> copyProperties(new VariableData(), v))
                .collect(Collectors.toSet());
        return saveAll(data);
    }

    @Transactional(transactionManager = "jpaTransactionManager")
    public VariableData update(@NonNull Variable variable) {
        verifyIfVariableExists(variable);
        VariableData existingVariableData = obtainOneFrom(variable);
        return save(updateProperties(existingVariableData, variable));
    }

    @Transactional(transactionManager = "jpaTransactionManager")
    public Set<VariableData> updateAll(@NonNull Set<Variable> variables) {
        variables.forEach(this::verifyIfVariableExists);
        Map<Long, VariableData> existingVariables = obtainAllFrom(variables).stream()
                .collect(Collectors.toMap(VariableData::getId, Function.identity()));
        Set<VariableData> updatedData = variables.parallelStream()
                .map(v -> updateProperties(existingVariables.get(v.getId()), v)).collect(toSet());
        return saveAll(updatedData);
    }

    private void verifyIfVariableExists(Variable variable) {
        if (!variable.hasId() || !variable.hasVersion()) {
            throw new IllegalArgumentException("Cannot update variable that has not been yet created");
        }
    }

    @Transactional(transactionManager = "jpaTransactionManager", readOnly = true)
    public List<VariableData> findAll(String search) {
        return findAll(search, VariableQueryWithOptions.getDefaultInstance());
    }

    @Transactional(transactionManager = "jpaTransactionManager", readOnly = true)
    public List<VariableData> findAll(String search, VariableQueryWithOptions queryOptions) {
        return findAllVariables(search, queryOptions);
    }

    @Transactional(transactionManager = "jpaTransactionManager", readOnly = true)
    public Set<VariableData> findExactlyByIdIn(@NonNull Set<Long> variableIds) {
        Set<VariableData> found = variableRepository.findAllByIdIn(variableIds);
        return verifyVariables(variableIds, found);
    }

    @Transactional(transactionManager = "jpaTransactionManager", readOnly = true)
    public Set<VariableData> findBySystemIdAndVariableNameIn(long systemId, Set<String> variableNames) {
        return new HashSet<>(BulkUtils.batchAndApply(variableNames,
                names -> obtainFrom(Variables.suchThat().systemId().eq(systemId).and().variableName().in(names))));
    }

    @Transactional(transactionManager = "jpaTransactionManager", readOnly = true)
    public Set<VariableData> findBySystemIdAndVariableNameLike(long systemId, String variableNameExpr) {
        return obtainFrom(Variables.suchThat().systemId().eq(systemId).and().variableName().like(variableNameExpr));
    }

    @Transactional(transactionManager = "jpaTransactionManager")
    public void delete(long variableId) {
        this.variableRepository.deleteById(variableId);
    }

    @Transactional(transactionManager = "jpaTransactionManager")
    public void deleteAll(Set<Long> variableIds) {
        BulkUtils.batchAndApply(variableIds, ids -> {
            variableRepository.deleteAllById(ids);
            return ids;
        });
    }

    private Set<VariableData> verifyVariables(Set<Long> variables, Set<VariableData> variableData) {
        if (variableData.size() < variables.size()) {
            Set<Long> existing = variableData.stream().map(VariableData::getId).collect(toSet());
            throw NotFoundRuntimeException.missingVariables(Sets.difference(variables, existing));
        }
        return variableData;
    }

    private VariableData copyProperties(VariableData variableData, Variable variable) {
        SystemSpecData system = getSystemSpecOrThrow(variable);
        variableData.setSystem(system);
        variableData.setVariableName(variable.getVariableName());
        variableData.setDescription(variable.getDescription());
        variableData.setUnit(variable.getUnit());
        variableData.setDeclaredType(variable.getDeclaredType());
        setIdAndVersion(variableData, variable);
        copyConfigs(variableData, variable);
        return variableData;
    }

    private VariableData updateProperties(VariableData existingVariableData, Variable variable) {
        entityManager.detach(existingVariableData);
        existingVariableData.getVariableConfigs().clear(); // clear configs to accept the updated ones
        return copyProperties(existingVariableData, variable);
    }

    private void copyConfigs(VariableData variableData, Variable variable) {
        for (VariableConfig config : variable.getConfigs()) {
            variableData.addConfig(copyConfigProperties(new VariableConfigData(), config));
        }
    }

    private VariableConfigData copyConfigProperties(VariableConfigData configData, VariableConfig config) {
        EntityData entity = getEntityOrThrow(config.getEntityId());
        configData.setEntity(entity);
        configData.setFieldName(config.getFieldName());
        configData.setValidFromStamp(config.getValidity().getStartTime());
        configData.setValidToStamp(config.getValidity().getEndTime());
        configData.setPredicate(config.getPredicate());
        setIdAndVersion(configData, config);
        return configData;
    }

    private <T extends Identifiable & Versionable> void setIdAndVersion(StandardPersistentEntityWithVersion target,
            T source) {
        if (source.hasId()) {
            target.setId(source.getId());
        }
        if (source.hasVersion()) {
            target.setRecVersion(source.getRecVersion());
        }
    }

    private EntityData getEntityOrThrow(long entityId) {
        return entityRepository.findById(entityId).orElseThrow(() -> NotFoundRuntimeException.missingEntity(entityId));
    }

    private SystemSpecData getSystemSpecOrThrow(Variable variable) {
        return systemRepository.findById(variable.getSystemSpec().getId())
                .orElseThrow(() -> NotFoundRuntimeException.missingSystem(variable.getSystemSpec()));
    }

    private VariableData save(VariableData variable) {
        try {
            //TODO: remove as per NXCALS-3291
            VariableData data = variableRepository.save(variable);
            // here we flush to induce PersistenceException early and wrap it in ConfigDataConflictException
            entityManager.flush();
            return data;
        } catch (PersistenceException e) {
            throw new ConfigDataConflictException(e);
        }
    }

    private Set<VariableData> saveAll(Set<VariableData> variables) {
        try {
            Set<VariableData> data = Sets.newHashSet(variableRepository.saveAll(variables));
            entityManager.flush();
            return data;
        } catch (PersistenceException e) {
            throw new ConfigDataConflictException(e);
        }
    }

    private Set<VariableData> obtainFrom(Condition<Variables> query) {
        return new HashSet<>(findAll(toRSQL(query)));
    }

    private Set<VariableData> obtainAllFrom(Set<Variable> variables) {
        Set<Long> variableIds = variables.stream().map(Variable::getId).collect(toSet());
        return new HashSet<>(
                BulkUtils.batchAndApply(variableIds, ids -> obtainFrom(Variables.suchThat().id().in(ids))));
    }

    private VariableData obtainOneFrom(Variable variable) {
        return variableRepository
                .queryOne(toRSQL(Variables.suchThat().id().eq(variable.getId())), VariableData.WITH_CONFIGS,
                        VariableData.class)
                .orElseThrow(() -> NotFoundRuntimeException.missingVariable(variable.getId()));
    }

    private List<VariableData> findAllVariables(String search, VariableQueryWithOptions queryOptions) {
        Optional<String> entityGraphName = Optional.of(getEntityGraphName(queryOptions.getQueryType()));
        return this.variableRepository.queryAll(search, VariableData.class, entityGraphName,
                createQueryProcessor(queryOptions, VariableData.class), createResultProcessor(queryOptions));
    }

    private String getEntityGraphName(VariableQueryWithOptions.VariableQueryType queryType) {
        if (Objects.requireNonNull(queryType) == VariableQueryWithOptions.VariableQueryType.WITHOUT_VARIABLE_CONFIGS) {
            return VariableData.WITHOUT_CONFIGS;
        }
        return VariableData.WITH_CONFIGS;
    }
}
