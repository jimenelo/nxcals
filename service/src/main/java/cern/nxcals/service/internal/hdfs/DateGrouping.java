package cern.nxcals.service.internal.hdfs;

import cern.nxcals.api.domain.TimeWindow;
import lombok.AllArgsConstructor;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static java.time.ZonedDateTime.ofInstant;


/*
Date grouping is aimed at limiting the number of paths we generate. We substitute all the whole units (YEARS, MONTHS) by globs.

For instance:
Rather than generating a list of ["2010\1\1", "2010\1\2", ... , "2010\12\31"] we generate one glob: "2010\*\*"

Same goes for months:
Rather than: ["2010\3\1", "2010\3\2", ... , "2010\3\31"], we generate: "2010\3*"

Please consult DateGroupingTest for expected behaviour on edge cases
*/

@AllArgsConstructor
public class DateGrouping {
    private static final ZoneId ZONE = ZoneId.of("UTC");

    private final ChronoUnit granularity;
    private final DateGroupingType grouping;

    public DateGrouping() {
        granularity = ChronoUnit.DAYS;
        grouping = DateGroupingType.YEARS_MONTHS_AND_DAYS;
    }

    public Set<String> group(TimeWindow window) {
        return group(window.getStartTime(), window.getEndTime());
    }

    public Set<String> group(Instant from, Instant to) {
        if (from.isAfter(to)) {
            return Collections.emptySet();
        }

        Instant fromT = from.truncatedTo(granularity);
        Instant toT = to.truncatedTo(granularity).plus(1, granularity);

        if (to.isAfter(toT)){
            toT = toT.plus(1, granularity);
        }

        final HashSet<String> acc = new HashSet<>();
        group(ofInstant(fromT, ZONE), ofInstant(toT, ZONE), grouping, acc);
        return acc;
    }

    private void group(ZonedDateTime from, ZonedDateTime to, DateGroupingType grouping, Set<String> acc) {
        ZonedDateTime min = from.with(grouping.getAdjuster());
        ZonedDateTime max = to.with(grouping.getAdjuster());

        if (min.equals(max)) {
            if (grouping.hasNext()) {
                group(from, to, grouping.getNext(), acc);
            }
            return;
        }

        if (min.isBefore(from)) {
            min = min.plus(1, grouping.getUnit());
            group(from, min, grouping.getNext(), acc);
        }

        while(min.isBefore(max)) {
            acc.add(grouping.getFormatter().format(min));
            min = min.plus(1, grouping.getUnit());
        }

        if (max.isBefore(to)) {
            group(max, to, grouping.getNext(), acc);
        }
    }
}
