/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.internal.extraction;

import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.utils.TimeUtils;
import com.google.common.base.Preconditions;
import lombok.RequiredArgsConstructor;

import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import java.util.Optional;

@RequiredArgsConstructor
class DataSourceTimeWindow {
    static final int HBASE_PERIOD_HOURS = 48;

    private final TimeWindow hdfsTimeWindow;
    private final TimeWindow hbaseTimeWindow;

    static DataSourceTimeWindow getTimeWindows(TimeWindow window) {
        return getTimeWindows(window, Clock.systemDefaultZone());
    }

    static DataSourceTimeWindow getTimeWindows(TimeWindow window, Clock clock) {
        Preconditions.checkArgument(!window.isEmpty(), "Empty window passed to DataSourceTimeWindow");

        Instant split = clock.instant().minus(Duration.ofHours(HBASE_PERIOD_HOURS));
        long splitNanos = TimeUtils.getNanosFromInstant(split);

        if (splitNanos <= window.getStartTimeNanos()) {
            return new DataSourceTimeWindow(null, window);
        }

        if (splitNanos >= window.getEndTimeNanos()) {
            return new DataSourceTimeWindow(window, null);
        }

        return new DataSourceTimeWindow(window.rightLimit(split.minus(Duration.ofNanos(1))), window.leftLimit(split));
    }

    Optional<TimeWindow> getHdfsTimeWindow() {
        return Optional.ofNullable(hdfsTimeWindow);
    }

    Optional<TimeWindow> getHbaseTimeWindow() {
        return Optional.ofNullable(hbaseTimeWindow);
    }
}
