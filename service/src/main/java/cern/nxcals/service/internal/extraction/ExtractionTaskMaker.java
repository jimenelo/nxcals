package cern.nxcals.service.internal.extraction;

import cern.nxcals.common.domain.ColumnMapping;
import cern.nxcals.common.domain.ExtractionCriteria;
import cern.nxcals.common.domain.ExtractionResource;
import cern.nxcals.common.domain.HBaseExtractionTask;
import cern.nxcals.common.domain.HBaseResource;
import cern.nxcals.common.domain.HdfsExtractionTask;
import cern.nxcals.service.internal.extraction.ExtractionService.SelectionCriteria;
import com.google.common.collect.Iterables;
import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static cern.nxcals.common.SystemFields.NXC_EXTR_VALUE;
import static com.google.common.base.Preconditions.checkArgument;
import static java.util.stream.Collectors.toSet;
import static org.apache.commons.collections.CollectionUtils.isEmpty;

@Slf4j
@Component
@RequiredArgsConstructor
public class ExtractionTaskMaker {
    private final PredicateProvider predicateProvider;

    Optional<HBaseExtractionTask> hbaseTask(TaskInput input) {
        Optional<HBaseResource> resources = getHBaseResourcesFrom(input.getResources());
        return resources.map(r -> HBaseExtractionTask.builder().resource(r).columns(input.getMappings())
                .predicates(predicateProvider.getHBasePredicate(input.getCriteria()))
                .variableFieldBoundColumnMapping(input.getVariableFieldMapping()).build());
    }

    Optional<HdfsExtractionTask> hdfsTask(TaskInput input) {
        Optional<Set<URI>> resources = getHdfsResourcesFrom(input.getResources());
        return resources.map(r -> HdfsExtractionTask.builder().paths(r).columns(input.getMappings())
                .predicates(predicateProvider.getHdfsPredicate(input.getCriteria()))
                .variableFieldBoundColumnMapping(input.getVariableFieldMapping()).build());
    }

    private Optional<HBaseResource> getHBaseResourcesFrom(List<? extends ExtractionResource> involvedResources) {
        if (isEmpty(involvedResources)) {
            log.debug("No entity resources matching query, will return empty HBase resource!");
            return Optional.empty();
        }
        Set<HBaseResource> hBase = involvedResources.stream().flatMap(r -> r.getResource().getHbaseTableNames().stream()
                        .map(table -> HBaseResource.builder().tableName(table)
                                .schemaJson(r.getEntityHistory().getEntitySchema().getSchema().toString())
                                .namespace(r.getResource().getHbaseNamespace())
                                .isAccessible(r.getResource().isHbaseTableAccessible()).build()))
                .filter(HBaseResource::isAccessible).collect(toSet());
        if (hBase.isEmpty()) {
            log.debug("Hbase table might yet not be accessible for {}", involvedResources);
            return Optional.empty();
        }
        checkArgument(hBase.size() == 1, "Internal error, found " + hBase.size() + " HBase "
                + "tables for the same system, partition and schema");
        return Optional.of(Iterables.getOnlyElement(hBase));
    }

    private Optional<Set<URI>> getHdfsResourcesFrom(List<? extends ExtractionResource> resources) {
        if (isEmpty(resources)) {
            return Optional.empty();
        }
        Set<URI> hdfsResources = resources.stream().flatMap(r -> r.getResource().getHdfsPaths().stream())
                .collect(toSet());
        return Optional.of(hdfsResources);
    }

    @Data
    static class TaskInput {
        @NonNull
        private final List<ColumnMapping> mappings;
        @NonNull
        private final SelectionCriteria criteria;
        @NonNull
        private final List<? extends ExtractionResource> resources;
        @NonNull
        private final ExtractionCriteria extractionCriteria;

        public ColumnMapping getVariableFieldMapping() {
            if (extractionCriteria.isVariableSearch()) {
                for (ColumnMapping mapping : mappings) {
                    if (NXC_EXTR_VALUE.getValue().equals(mapping.getAlias())) {
                        return mapping;
                    }
                }
            }
            return null;
        }
    }
}
