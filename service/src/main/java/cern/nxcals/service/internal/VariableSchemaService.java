package cern.nxcals.service.internal;

import cern.nxcals.api.domain.EntityHistory;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.domain.VariableConfig;
import cern.nxcals.common.domain.ExtractionCriteria;
import cern.nxcals.common.utils.AvroUtils;
import cern.nxcals.service.internal.extraction.ExtractionSourceConfigService;
import cern.nxcals.service.internal.extraction.VariableSourceConfig;
import com.google.common.collect.Maps;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.apache.avro.Schema;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import static cern.nxcals.api.utils.EncodingUtils.sanitizeIfNeeded;

@Service
@RequiredArgsConstructor
public class VariableSchemaService {
    @NonNull
    private final ExtractionSourceConfigService extractionSourceConfigService;

    @Transactional(transactionManager = "jpaTransactionManager")
    public Map<Long, Map<Schema, Set<TimeWindow>>> findSchemas(Set<Long> variableIds, TimeWindow timeWindow) {
        Collection<VariableSourceConfig> configs = extractionSourceConfigService.getVariableConfigsFor(
                toExtractionCriteria(variableIds, timeWindow));

        return groupSchemasByVariables(configs, timeWindow);
    }

    private ExtractionCriteria toExtractionCriteria(Set<Long> variableIds, TimeWindow timeWindow) {
        return ExtractionCriteria.builder()
                .timeWindow(timeWindow)
                .variableIds(variableIds)
                .build();
    }

    private Map<Long, Map<Schema, Set<TimeWindow>>> groupSchemasByVariables(
            Collection<VariableSourceConfig> configs,
            TimeWindow timeWindow) {
        HashMap<Long, HashMap<Schema, HashSet<TimeWindow>>> result = new HashMap<>();
        for (VariableSourceConfig config : configs) {
            long variableId = config.getVariable().getId();

            HashMap<Schema, HashSet<TimeWindow>> schemas = result.computeIfAbsent(variableId, x -> new HashMap<>());

            for (EntityHistory history : config.getEntity().getEntityHistory()) {
                TimeWindow configValidity = history.getValidity().intersect(timeWindow);

                Schema entitySchema = history.getEntitySchema().getSchema();

                Optional<Schema> maybeFieldSchema = findSchema(entitySchema, config.getVariableConfig());
                maybeFieldSchema.ifPresent(
                        schema -> schemas.computeIfAbsent(schema, x -> new HashSet<>()).add(configValidity));
            }
        }

        return mergeTimeWindows(result);
    }

    private Optional<Schema> findSchema(Schema entitySchema, VariableConfig variableConfig) {
        if (variableConfig.isFieldBound()) {
            String fieldName = variableConfig.getFieldName();
            Optional<Schema.Field> maybeField = AvroUtils.findField(entitySchema,
                    sanitizeIfNeeded(fieldName));

            return maybeField.map(Schema.Field::schema);
        } else {
            return Optional.of(entitySchema);
        }
    }

    private Map<Long, Map<Schema, Set<TimeWindow>>> mergeTimeWindows(
            HashMap<Long, HashMap<Schema, HashSet<TimeWindow>>> result) {
        return Maps.transformValues(result, value ->
                Maps.transformValues(value, TimeWindow::mergeTimeWindows)
        );
    }


}
