package cern.nxcals.service.internal.compaction;

import cern.nxcals.api.domain.PartitionResourceHistory;
import cern.nxcals.api.domain.TimeEntityPartitionType;
import cern.nxcals.common.domain.AdaptiveCompactionJob;
import cern.nxcals.common.domain.DataProcessingJob;
import cern.nxcals.common.paths.StagingPath;
import cern.nxcals.common.utils.HdfsFileUtils;
import cern.nxcals.service.domain.PartitionResourceData;
import cern.nxcals.service.domain.PartitionResourceHistoryData;
import cern.nxcals.service.internal.PartitionResourceHistoryService;
import cern.nxcals.service.internal.PartitionResourceService;
import com.google.common.annotations.VisibleForTesting;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.UncheckedIOException;
import java.net.URI;
import java.time.ZonedDateTime;
import java.util.Collection;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * A job creator for the adaptive compaction job where we partition by timestamp (1) and bucket by entity_id (2) depending on the size of the data.
 * It tries to create 1 GB files (but partitioned by time & entity bucketed. This will keep the number of files similar to previous algorithm but
 * provide additional ways of pruning the partitions.
 */
@Slf4j
public class AdaptiveCompactionJobCreator extends DataProcessingJobCreator<AdaptiveCompactionJob> {
    @VisibleForTesting
    static final long ONE_GB_FILE_SIZE = 1024 * 1024 * 1024L;

    private static final long ROW_GROUP_MIN_SIZE = 16 * 1024 * 1024L;
    private static final long ROW_GROUP_MAX_SIZE = 512 * 1024 * 1024L;
    private static final long DEFAULT_HDFS_BLOCK_SIZE = 256 * 1024 * 1024L;
    private static final long EXTENDED_HDFS_BLOCK_SIZE = 512 * 1024 * 1024L;




    private static final int ROW_GROUP_COUNT = 200;

    @NonNull
    private final PartitionResourceService partitionResourceService;
    @NonNull
    private final PartitionResourceHistoryService partitionResourceHistoryService;


    @SuppressWarnings("squid:S00107")
    public AdaptiveCompactionJobCreator(
            @NonNull PartitionResourceService partitionResourceService,
            @NonNull PartitionResourceHistoryService partitionResourceHistoryService,
            long sortThreshold,
            long maxPartitionSize,
            int avroParquetSizeRatio,
            @NonNull String dataPrefix,
            @NonNull String stagingPrefix,
            @NonNull FileSystem fs) {

        super(sortThreshold, maxPartitionSize, avroParquetSizeRatio, dataPrefix, stagingPrefix, fs);
        this.partitionResourceService = partitionResourceService;
        this.partitionResourceHistoryService = partitionResourceHistoryService;

    }

    private boolean isPathValidForActiveCompaction(StagingPath path) {
        Path outputPath = new Path(this.getHdfsPathCreator().toOutputUri(path));
        try {
            //Either there is no files in the target dir or the resource info already exists (this is the late data case
            //that we process according to existing info).
            return !HdfsFileUtils.hasDataFiles(getFs(), outputPath) || resourceInfoExists(path);
        } catch (UncheckedIOException ex) {
            log.error("Got exception while checking if provided path exists for {}. Operation would be omitted",
                    getJobType(), ex);
            return false;
        }
    }


    //Checks if resource & resourceInfo exist for this path.
    private boolean resourceInfoExists(StagingPath path) {
        return partitionResourceService.findBySystemIdAndPartitionIdAndSchemaId(
                path.getSystemId(),
                path.getPartitionId(),
                path.getSchemaId())
                .filter(partitionResourceData -> partitionResourceHistoryService
                        .findByPartitionResourceAndTimestamp(partitionResourceData, path.getDate().toInstant())
                        .isPresent())
                .isPresent();
    }

    /**
     * This verification must check if there is data in the 'data' directory for this partition/day.
     * If there is data on pro, and the adaptive compaction partitioning is not assigned it means we cannot process this
     * directory with this algorithm (old one has to be used).
     *
     * @return predicate for a path
     */
    @Override
    public Predicate<StagingPath> getPathChecker() {
        return this::isPathValidForActiveCompaction;
    }

    @VisibleForTesting
    static TimeEntityPartitionType selectPartitioningType(long totalDirSize) {
        double targetNbOf1GBFiles = totalDirSize / (double) ONE_GB_FILE_SIZE;
        return TimeEntityPartitionType.ofGbFiles(Math.round(targetNbOf1GBFiles));
    }

    @Override
    protected AdaptiveCompactionJob createJob(StagingPath stagingPath, Collection<FileStatus> files, long jobSize, long totalSize, String filePrefix) {
        List<URI> uris = files.stream().map(FileStatus::getPath).map(Path::toUri).collect(Collectors.toList());
        if (log.isInfoEnabled()) {
            log.info(
                    "Creating AdaptiveCompactionJob for staging path={}, files-count={}, jobSize={}, total day size={}",
                    stagingPath, uris.size(), jobSize, totalSize);
        }
        TimeEntityPartitionType partitionType = getPartitionType(stagingPath, totalSize);
        long parquetRowGroupSize = calculateParquetRowGroupSize(totalSize);
        long hdfsBlockSize = calculateHdfsBlockSize(parquetRowGroupSize);

        return AdaptiveCompactionJob.builder()
                .filePrefix(
                        "") // file prefix is not fixed, it will be given by the compactor according to partition/bucket.
                .systemId(stagingPath.getSystemId())
                .partitionId(stagingPath.getPartitionId())
                .schemaId(stagingPath.getSchemaId())
                .sourceDir(stagingPath.toPath().toUri())
                .destinationDir(getHdfsPathCreator().toOutputUri(stagingPath))
                .totalSize(totalSize)
                .jobSize(jobSize)
                .partitionType(partitionType)
                .sortEnabled(jobSize > getSortAbove())
                .files(uris)
                .parquetRowGroupSize(parquetRowGroupSize)
                .hdfsBlockSize(hdfsBlockSize)
                .build();
    }

    static long calculateParquetRowGroupSize(long totalSize) {
        //aiming at ~200 row groups as default , with min 16MB, max 256MB (HDFS block size),
        //using multiples of 16MB to normalize.
        long parquetRowGroupSize = (((totalSize / ROW_GROUP_COUNT) / ROW_GROUP_MIN_SIZE) + 1) * ROW_GROUP_MIN_SIZE;
        return Math.min(parquetRowGroupSize, ROW_GROUP_MAX_SIZE); //upper bound is needed.
    }

    static long calculateHdfsBlockSize(long parquetRowGroupSize) {
        return parquetRowGroupSize > DEFAULT_HDFS_BLOCK_SIZE ? EXTENDED_HDFS_BLOCK_SIZE : DEFAULT_HDFS_BLOCK_SIZE;
    }

    private TimeEntityPartitionType getPartitionType(StagingPath stagingPath, long totalSize) {
        return AdaptiveCompactionJobCreator.getPartitionType(this.partitionResourceService,
                this.partitionResourceHistoryService, stagingPath, totalSize);
    }

    static TimeEntityPartitionType getPartitionType(PartitionResourceService prService,
            PartitionResourceHistoryService prhService,
            StagingPath stagingPath, long totalSize) {
        long systemId = stagingPath.getSystemId();
        long partitionId = stagingPath.getPartitionId();
        long schemaId = stagingPath.getSchemaId();
        ZonedDateTime date = stagingPath.getDate();

        PartitionResourceData prData = prService
                .findOrCreatePartitionResourceData(systemId, partitionId, schemaId);
        TimeEntityPartitionType type = selectPartitioningType(totalSize);

        PartitionResourceHistory newPrh = prhService
                .createPartitionResourceHistory(prData, date.toInstant(), type);

        PartitionResourceHistoryData priData = prhService
                .findOrCreatePartitionResourceInfoFor(prData, newPrh);

        return TimeEntityPartitionType.from(priData.getInformation());
    }

    @Override
    public DataProcessingJob.JobType getJobType() {
        return DataProcessingJob.JobType.ADAPTIVE_COMPACT;
    }
}
