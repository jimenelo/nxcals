package cern.nxcals.service.internal;

import cern.nxcals.api.domain.Hierarchy;
import cern.nxcals.api.domain.HierarchyView;
import cern.nxcals.api.domain.Identifiable;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.extraction.metadata.queries.Hierarchies;
import cern.nxcals.service.domain.EntityData;
import cern.nxcals.service.domain.GroupData;
import cern.nxcals.service.domain.HierarchyData;
import cern.nxcals.service.domain.SystemSpecData;
import cern.nxcals.service.domain.VariableData;
import cern.nxcals.service.domain.VariableWithHierarchy;
import cern.nxcals.service.domain.VisibilityData;
import cern.nxcals.service.internal.component.BulkUtils;
import cern.nxcals.service.repository.HierarchyRepository;
import cern.nxcals.service.repository.SystemSpecRepository;
import cern.nxcals.service.rest.exceptions.ConfigDataConflictException;
import cern.nxcals.service.rest.exceptions.NotFoundRuntimeException;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Sets;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.StreamSupport;

import static cern.nxcals.common.utils.RSQLUtils.toRSQL;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.mapping;
import static java.util.stream.Collectors.toMap;
import static java.util.stream.Collectors.toSet;

@Service
@Slf4j
@AllArgsConstructor
public class HierarchyService {
    private static final String HIERARCHY_GROUP_LABEL = "HIERARCHY";
    private static final String HIERARCHY_ASSOCIATION = "__in__hierarchy__";
    @NonNull
    @PersistenceContext
    private final EntityManager em;
    @NonNull
    private final HierarchyRepository repository;
    @NonNull
    private final VariableService variableService;
    @NonNull
    private final EntityService entityService;
    @NonNull
    private final SystemSpecRepository systemRepository;

    @Transactional(transactionManager = "jpaTransactionManager", readOnly = true)
    public List<HierarchyData> findAll(String search) {
        return repository.queryAll(search, HierarchyData.PLAIN, HierarchyData.class);
    }

    @Transactional(transactionManager = "jpaTransactionManager")
    public HierarchyData create(@NonNull Hierarchy hierarchy) {
        HierarchyData toSave = createFrom(hierarchy, this::findSystemDataFor,
                p -> obtainFrom(p, HierarchyData.MINIMUM));
        return validateAndSave(toSave);
    }

    @Transactional(transactionManager = "jpaTransactionManager")
    public Set<HierarchyData> createAll(@NonNull Set<Hierarchy> hierarchies) {
        Map<Long, SystemSpecData> systemDataMap = findAllSystemDataFor(hierarchies.stream()
                .map(h -> h.getSystemSpec().getId()).collect(toSet()));
        Set<HierarchyView> hierarchyParents = hierarchies.stream()
                .map(Hierarchy::getParent).filter(Objects::nonNull).collect(toSet());
        Map<Long, HierarchyData> parentHierarchyDataMap = obtainMapFrom(hierarchyParents, HierarchyData.MINIMUM);

        Set<HierarchyData> hierarchiesToSave = new HashSet<>();
        for (Hierarchy hierarchy : hierarchies) {
            HierarchyData toSave = createFrom(hierarchy, s -> systemDataMap.get(s.getId()),
                    p -> parentHierarchyDataMap.get(p.getId()));
            validate(toSave);
            hierarchiesToSave.add(toSave);
        }
        return saveAllAndRefresh(hierarchiesToSave);
    }

    private HierarchyData createFrom(Hierarchy hierarchy, Function<SystemSpec, SystemSpecData> systemProvider,
            Function<HierarchyView, HierarchyData> parentProvider) {
        if (hierarchy.hasId()) {
            throw ConfigDataConflictException.conflictOnHierarchy(hierarchy);
        }
        SystemSpecData system = systemProvider.apply(hierarchy.getSystemSpec());
        HierarchyData toSave = newHierarchy(hierarchy.getName(), hierarchy.getDescription(), system);
        if (hierarchy.getParent() != null) {
            HierarchyData parent = parentProvider.apply(hierarchy.getParent());
            verifySystems(hierarchy.getSystemSpec(), parent.getSystem());
            toSave.setParent(parent);
        }
        return toSave;
    }

    @Transactional(transactionManager = "jpaTransactionManager")
    public HierarchyData update(@NonNull Hierarchy hierarchy) {
        HierarchyData toUpdate = updateProperties(obtainFrom(hierarchy, HierarchyData.MINIMUM), hierarchy,
                parent -> obtainFrom(parent, HierarchyData.MINIMUM));
        return validateAndSave(toUpdate);
    }

    @Transactional(transactionManager = "jpaTransactionManager")
    public Set<HierarchyData> updateAll(@NonNull Set<Hierarchy> hierarchies) {
        Map<Long, HierarchyData> parentHierarchiesById = obtainMapFrom(hierarchies.stream().map(Hierarchy::getParent)
                .filter(Objects::nonNull).collect(toSet()), HierarchyData.MINIMUM);
        Map<Long, HierarchyData> hierarchiesById = obtainMapFrom(Sets.newHashSet(hierarchies), HierarchyData.MINIMUM);

        Set<HierarchyData> hierarchiesToUpdate = new HashSet<>();
        for (Hierarchy hierarchy : hierarchies) {
            HierarchyData toUpdate = updateProperties(hierarchiesById.get(hierarchy.getId()), hierarchy,
                    parent -> parentHierarchiesById.get(parent.getId()));
            validate(toUpdate);
            hierarchiesToUpdate.add(toUpdate);
        }
        return saveAllAndRefresh(hierarchiesToUpdate);
    }

    private HierarchyData updateProperties(HierarchyData existing, Hierarchy hierarchy,
            Function<HierarchyView, HierarchyData> parentProvider) {
        em.detach(existing);
        SystemSpec newHierarchySystem = hierarchy.getSystemSpec();
        verifySystems(newHierarchySystem, existing.getSystem());
        if (hierarchy.getParent() != null) {
            HierarchyData parent = parentProvider.apply(hierarchy.getParent());
            verifySystems(newHierarchySystem, parent.getSystem());
            existing.setParent(parent);
        } else {
            existing.setParent(null);
        }
        existing.setName(hierarchy.getName());
        existing.getGroup().setDescription(hierarchy.getDescription());
        existing.setRecVersion(hierarchy.getRecVersion());
        verifyNoCycle(existing);
        return existing;
    }

    @Transactional(transactionManager = "jpaTransactionManager")
    public void deleteLeaf(long hierarchyId) {
        HierarchyData hierarchy = obtainFrom(hierarchyId, HierarchyData.MINIMUM_WITH_PATH);

        if (!hierarchy.getViewData().isLeaf()) {
            throw new IllegalStateException("Hierarchy " + hierarchyId + " is not a leaf");
        }

        repository.deleteById(hierarchyId);
    }

    @Transactional(transactionManager = "jpaTransactionManager")
    public void deleteAllLeaves(Set<Long> hierarchyIds) {
        Map<Long, HierarchyData> hierarchyDataMap = obtainMapFromIds(hierarchyIds, HierarchyData.MINIMUM_WITH_PATH);
        long[] noLeafIds = hierarchyDataMap.values().stream().filter(h -> !h.getViewData().isLeaf())
                .mapToLong(HierarchyData::getId).toArray();
        if (ArrayUtils.isNotEmpty(noLeafIds)) {
            throw new IllegalStateException(String.format("Hierarchies %s are not leaves", Arrays.toString(noLeafIds)));
        }
        repository.deleteAll(hierarchyDataMap.values());
    }

    private void verifyNoCycle(HierarchyData entity) {
        HierarchyData current = entity.getParent();
        while (current != null) {
            if (entity.getId().equals(current.getId())) {
                throw new IllegalStateException(
                        "Hierarchy update would create a cyclical reference at " + current.getName());
            }
            current = current.getParent();
        }
    }

    @Transactional(transactionManager = "jpaTransactionManager")
    public void setVariables(long hierarchyId, @NonNull Set<Long> variables) {
        HierarchyData hierarchy = obtainFrom(hierarchyId, HierarchyData.WITH_VARIABLES);
        hierarchy.getGroup().setAssociatedVariables(
                ImmutableMap.of(HIERARCHY_ASSOCIATION, variableService.findExactlyByIdIn(variables)));
        save(hierarchy);
    }

    @Transactional(transactionManager = "jpaTransactionManager")
    public void addVariables(long hierarchyId, @NonNull Set<Long> variables) {
        HierarchyData hierarchy = obtainFrom(hierarchyId, HierarchyData.WITH_VARIABLE_ASSOCIATIONS);
        hierarchy.getGroup().addAssociatedVariables(
                ImmutableMap.of(HIERARCHY_ASSOCIATION, variableService.findExactlyByIdIn(variables)));
        save(hierarchy);
    }

    @Transactional(transactionManager = "jpaTransactionManager")
    public void removeVariables(long hierarchyId, Set<Long> variables) {
        HierarchyData hierarchy = obtainFrom(hierarchyId, HierarchyData.WITH_VARIABLES_FOR_VARIABLES_REMOVAL);
        hierarchy.getGroup().removeAssociatedVariables(
                ImmutableMap.of(HIERARCHY_ASSOCIATION, variableService.findExactlyByIdIn(variables)));
        save(hierarchy);
    }

    @Transactional(transactionManager = "jpaTransactionManager", readOnly = true)
    public Set<VariableData> getVariables(long hierarchyId) {
        return obtainFrom(hierarchyId, HierarchyData.WITH_VARIABLES).getGroup().getAssociatedVariables()
                .getOrDefault(HIERARCHY_ASSOCIATION, Collections.emptySet());
    }

    @Transactional(transactionManager = "jpaTransactionManager")
    public void setEntities(long hierarchyId, @NonNull Set<Long> entities) {
        HierarchyData hierarchy = obtainFrom(hierarchyId, HierarchyData.WITH_ENTITIES);
        hierarchy.getGroup().setAssociatedEntities(
                ImmutableMap.of(HIERARCHY_ASSOCIATION, entityService.findExactlyByIdIn(entities)));
        save(hierarchy);
    }

    @Transactional(transactionManager = "jpaTransactionManager")
    public void addEntities(long hierarchyId, @NonNull Set<Long> entities) {
        HierarchyData hierarchy = obtainFrom(hierarchyId, HierarchyData.WITH_ENTITIES);
        hierarchy.getGroup().addAssociatedEntities(
                ImmutableMap.of(HIERARCHY_ASSOCIATION, entityService.findExactlyByIdIn(entities)));
        save(hierarchy);
    }

    @Transactional(transactionManager = "jpaTransactionManager")
    public void removeEntities(long hierarchyId, @NonNull Set<Long> entities) {
        HierarchyData hierarchy = obtainFrom(hierarchyId, HierarchyData.WITH_ENTITIES);
        hierarchy.getGroup().removeAssociatedEntities(
                ImmutableMap.of(HIERARCHY_ASSOCIATION, entityService.findExactlyByIdIn(entities)));
        save(hierarchy);
    }

    @Transactional(transactionManager = "jpaTransactionManager", readOnly = true)
    public Set<EntityData> getEntities(long hierarchyId) {
        return obtainFrom(hierarchyId, HierarchyData.WITH_ENTITIES).getGroup().getAssociatedEntities()
                .getOrDefault(HIERARCHY_ASSOCIATION, Collections.emptySet());
    }

    @Transactional(transactionManager = "jpaTransactionManager", readOnly = true)
    public Map<Long, Set<HierarchyData>> getHierarchiesForVariables(@NonNull Set<Long> variableIds) {
        return getHierarchyForVariables(variableIds, this::getRawVariableHierarchiesAndConvert);
    }

    private Collection<VariableWithHierarchy<HierarchyData>> getRawVariableHierarchiesAndConvert(
            Set<Long> variableIds) {
        Set<List<Object>> raw = repository.findAllByVariableIdIn(variableIds);
        return raw.stream().map(l -> new VariableWithHierarchy<>((Long) l.get(0), (HierarchyData) l.get(1)))
                .collect(toSet());
    }

    @Transactional(transactionManager = "jpaTransactionManager", readOnly = true)
    public Map<Long, Set<Long>> getHierarchyIdsForVariables(@NonNull Set<Long> variableIds) {
        return getHierarchyForVariables(variableIds, repository::findVariableWithHierarchiesByVariableIdIn);
    }

    private <T> Map<Long, Set<T>> getHierarchyForVariables(Set<Long> variableIds,
            Function<Set<Long>, Collection<VariableWithHierarchy<T>>> supplier) {
        Collection<VariableWithHierarchy<T>> variableWithHierarchies = BulkUtils.batchAndApply(variableIds, supplier);

        return variableWithHierarchies.stream().collect(groupingBy(VariableWithHierarchy::getVariableId,
                mapping(VariableWithHierarchy::getHierarchy, toSet())));
    }
    // helper methods

    private void verifySystems(@NonNull SystemSpec systemSpecA, @NonNull SystemSpecData systemSpecB) {
        if (systemSpecA.getId() != systemSpecB.getId()) {
            throw new IllegalArgumentException(
                    String.format("Cannot assign hierarchy of system [%1$s] to a hierarchy of system [%2$s]",
                            systemSpecA, systemSpecB));
        }
    }

    private HierarchyData newHierarchy(String name, String description, SystemSpecData system) {
        HierarchyData hierarchyData = new HierarchyData();
        hierarchyData.setName(name);
        hierarchyData.setSystem(system);
        hierarchyData.setGroup(new GroupData());
        hierarchyData.getGroup().setName(UUID.randomUUID().toString());
        hierarchyData.getGroup().setDescription(description);
        hierarchyData.getGroup().setSystem(system);
        hierarchyData.getGroup().setVisibility(VisibilityData.PRIVATE);
        hierarchyData.getGroup().setLabel(HIERARCHY_GROUP_LABEL);
        return hierarchyData;
    }

    private HierarchyData obtainFrom(HierarchyView hierarchy, String graph) {
        if (!hierarchy.hasId()) {
            throw NotFoundRuntimeException.missingHierarchy(hierarchy);
        }
        return obtainFrom(hierarchy.getId(), graph);
    }

    private HierarchyData obtainFrom(long hierarchyId, String graph) {
        return repository.queryOne(toRSQL(Hierarchies.suchThat().id().eq(hierarchyId)), graph, HierarchyData.class)
                .orElseThrow(() -> NotFoundRuntimeException.missingHierarchy(hierarchyId));
    }

    private Set<HierarchyData> obtainAllFrom(Set<Long> hierarchyIds, String graph) {
        return Sets.newHashSet(repository
                .queryAll(toRSQL(Hierarchies.suchThat().id().in(hierarchyIds)), graph, HierarchyData.class));
    }

    private Map<Long, HierarchyData> obtainMapFrom(Set<HierarchyView> hierarchies, String graph) {
        Map<Boolean, List<HierarchyView>> hierarchiesByValidity = hierarchies.stream()
                .collect(groupingBy(Identifiable::hasId));
        List<HierarchyView> hierarchiesWithNoId = hierarchiesByValidity
                .computeIfAbsent(false, k -> Collections.emptyList());
        if (!hierarchiesWithNoId.isEmpty()) {
            throw NotFoundRuntimeException.missingHierarchies(hierarchiesWithNoId);
        }
        Set<Long> hierarchyIds = hierarchiesByValidity.computeIfAbsent(true, k -> Collections.emptyList())
                .stream().map(Identifiable::getId).collect(toSet());
        return obtainMapFromIds(hierarchyIds, graph);
    }

    private Map<Long, HierarchyData> obtainMapFromIds(Set<Long> hierarchyIds, String graph) {
        if (CollectionUtils.isEmpty(hierarchyIds)) {
            return Collections.emptyMap();
        }
        Map<Long, HierarchyData> hierarchyDataMap = obtainAllFrom(hierarchyIds, graph)
                .stream().collect(toMap(HierarchyData::getId, Function.identity()));
        if (hierarchyIds.size() != hierarchyDataMap.size()) {
            throw NotFoundRuntimeException
                    .missingHierarchyIds(Sets.difference(hierarchyIds, hierarchyDataMap.keySet()));
        }
        return hierarchyDataMap;
    }

    private SystemSpecData findSystemDataFor(SystemSpec systemSpec) {
        return systemRepository.findById(systemSpec.getId())
                .orElseThrow(() -> NotFoundRuntimeException.missingSystem(systemSpec));
    }

    private Map<Long, SystemSpecData> findAllSystemDataFor(@NonNull Set<Long> systemIds) {
        Map<Long, SystemSpecData> systemSpecMap = systemRepository.findAllByIdIn(systemIds).stream()
                .collect(toMap(SystemSpecData::getId, Function.identity()));
        if (systemIds.size() != systemSpecMap.size()) {
            throw NotFoundRuntimeException.missingSystems(Sets.difference(systemIds, systemSpecMap.keySet()));
        }
        return systemSpecMap;
    }

    private HierarchyData save(HierarchyData hierarchy) {
        HierarchyData data = repository.save(hierarchy);
        flush();
        return data;
    }

    private HierarchyData saveAndRefresh(HierarchyData hierarchy) {
        HierarchyData data = save(hierarchy);
        em.refresh(data);
        return data;
    }

    private HierarchyData validateAndSave(HierarchyData hierarchy) {
        validate(hierarchy);
        return saveAndRefresh(hierarchy);
    }

    private Set<HierarchyData> saveAllAndRefresh(Set<HierarchyData> hierarchies) {
        Iterable<HierarchyData> savedHierarchies = repository.saveAll(hierarchies);
        flush();
        return refreshAll(savedHierarchies);
    }

    private Set<HierarchyData> refreshAll(Iterable<HierarchyData> hierarchies) {
        em.clear(); //clear cache to fetch hierarchies with view data directly from db
        Set<Long> hierarchyIds = StreamSupport.stream(hierarchies.spliterator(), false)
                .map(HierarchyData::getId).collect(toSet());
        return obtainAllFrom(hierarchyIds, HierarchyData.PLAIN);
    }

    private void validate(HierarchyData hierarchy) {
        Objects.requireNonNull(hierarchy, "Hierarchy must not be null!");
        Objects.requireNonNull(hierarchy.getGroup(), "Hierarchy internal group must not be null!");
        if (!VisibilityData.PRIVATE.equals(hierarchy.getGroup().getVisibility())) {
            throw new IllegalStateException(String.format("Hierarchy group visibility must strictly be %s,"
                    + " but got %s", VisibilityData.PRIVATE, hierarchy.getGroup().getVisibility()));
        }
    }

    private void flush() {
        try {
            //TODO: remove as per NXCALS-3291
            em.flush(); // here we flush to induce PersistenceException early and wrap it in ConfigDataConflictException
        } catch (PersistenceException exception) {
            try {
                throw (Exception) exception.getCause();
            } catch (ConstraintViolationException cause) {
                throw cause;
            } catch (Exception cause) {
                throw new ConfigDataConflictException(exception);
            }
        }
    }
}
