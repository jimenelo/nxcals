package cern.nxcals.service.internal.extraction;

import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.VariableConfig;
import cern.nxcals.common.Schemas;
import cern.nxcals.common.domain.ColumnMapping;
import cern.nxcals.common.domain.EntityExtractionResource;
import cern.nxcals.common.domain.ExtractionCriteria;
import cern.nxcals.common.domain.ExtractionResource;
import cern.nxcals.common.domain.VariableExtractionResource;
import cern.nxcals.common.utils.AvroUtils;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.Sets;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.avro.Schema;
import org.apache.avro.Schema.Field;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;

import static cern.nxcals.api.utils.EncodingUtils.desanitizeIfNeeded;
import static cern.nxcals.api.utils.EncodingUtils.sanitizeIfNeeded;
import static cern.nxcals.common.SystemFields.NXC_EXTR_ENTITY_ID;
import static cern.nxcals.common.SystemFields.NXC_EXTR_TIMESTAMP;
import static cern.nxcals.common.SystemFields.NXC_EXTR_VALUE;
import static cern.nxcals.common.SystemFields.NXC_EXTR_VARIABLE_NAME;
import static cern.nxcals.common.SystemFields.isSystemField;
import static cern.nxcals.service.internal.extraction.Utils.STRING_SCHEMA;
import static com.google.common.base.Preconditions.checkArgument;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

@Slf4j
@Component
@AllArgsConstructor
public class ColumnMapper {
    private static final String STRING_SCHEMA_JSON = STRING_SCHEMA.toString();
    @VisibleForTesting
    static final String VARIABLE_NAME = NXC_EXTR_VARIABLE_NAME.getValue();
    static final String TIMESTAMP = NXC_EXTR_TIMESTAMP.getValue();
    static final String VALUE = NXC_EXTR_VALUE.getValue();
    static final String ENTITY_ID = NXC_EXTR_ENTITY_ID.getValue();
    private static final ColumnMapping entityIdColumnMapping = ColumnMapping.builder()
            .fieldName(Schemas.ENTITY_ID.getFieldName()).alias(ENTITY_ID)
            .schemaJson(Schemas.ENTITY_ID.getSchema().toString()).build();

    @NonNull
    private final Function<String, Field> timestampFieldProvider;
    @NonNull
    private final Function<String, List<Schema.Field>> systemDefinitionFieldsProvider;

    List<ColumnMapping> enhanceEmptyVariableDatasetMappings(Collection<ColumnMapping> mappings, Set<String> systems,
            boolean isFieldBound) {
        List<ColumnMapping> ret = new ArrayList<>(mappings);
        boolean hasValueColumn = ret.stream().anyMatch(column -> column.getAlias().equals(VALUE));
        if (isFieldBound && !hasValueColumn) {
            ret.add(ColumnMapping.builder().fieldName(VALUE).schemaJson(STRING_SCHEMA_JSON).build());
        }
        ret.add(ColumnMapping.builder().fieldName(VARIABLE_NAME).schemaJson(STRING_SCHEMA_JSON).build());
        return enhanceWithCommonVariableMappings(ret, systems);
    }

    List<ColumnMapping> enhanceWithCommonEntityMappings(List<ColumnMapping> mappings, SystemSpec system) {
        return enhanceWithCommonEntityMappings(mappings, Sets.newHashSet(system.getName()));
    }

    List<ColumnMapping> enhanceWithCommonEntityMappings(List<ColumnMapping> mappings, Set<String> systemNames) {
        List<ColumnMapping> ret = new ArrayList<>(mappings);
        ret.add(entityIdColumnMapping);
        for (String systemName : systemNames) {
            this.addSystemDefinitionFieldsMappingIfNeeded(ret, systemName);
            this.addTimeFieldMappingIfNeeded(ret, systemName);
        }
        return ret;
    }

    List<ColumnMapping> enhanceWithCommonVariableMappings(List<ColumnMapping> mappings, SystemSpec system) {
        return enhanceWithCommonVariableMappings(mappings, Sets.newHashSet(system.getName()));
    }

    List<ColumnMapping> enhanceWithCommonVariableMappings(List<ColumnMapping> mappings, Set<String> systemNames) {
        List<ColumnMapping> ret = new ArrayList<>(mappings);
        ret.add(entityIdColumnMapping);
        for (String systemName : systemNames) {
            this.addTimeFieldMappingIfNeeded(ret, systemName);
            this.setAliasForTimestampColumn(ret, systemName);
        }
        return ret;
    }

    private void addSystemDefinitionFieldsMappingIfNeeded(List<ColumnMapping> mappings, String systemName) {
        for (Schema.Field field : this.systemDefinitionFieldsProvider.apply(systemName)) {
            if (mappings.stream().noneMatch(m -> field.name().equals(m.getFieldName()))) {
                mappings.add(ColumnMapping.fromField(field));
            }
        }
    }

    private void addTimeFieldMappingIfNeeded(List<ColumnMapping> mappings, String systemName) {
        Schema.Field timeField = timestampFieldProvider.apply(systemName);
        if (mappings.stream().noneMatch(m -> timeField.name().equals(m.getFieldName()))) {
            mappings.add(ColumnMapping.fromField(timeField));
        }
    }

    private void setAliasForTimestampColumn(List<ColumnMapping> mappings, String systemName) {
        Schema.Field timeField = timestampFieldProvider.apply(systemName);
        for (int i = 0; i < mappings.size(); i++) {
            ColumnMapping mapping = mappings.get(i);
            if (timeField.name().equals(mapping.getFieldName())) {
                mappings.set(i, mapping.toBuilder().alias(TIMESTAMP).build());
                return;
            }
        }
    }

    List<ColumnMapping> getVariableMappings(Collection<VariableExtractionResource> resources,
            ExtractionCriteria criteria) {
        checkVariableFieldAssignments(resources);
        boolean isFieldBound = resources.stream().anyMatch(VariableExtractionResource::isFieldBound);

        if (isFieldBound) {
            return createVariableFieldMappings(resources);
        }
        return getEntityMappings(toEntityExtractionResource(resources), criteria);
    }

    private void checkVariableFieldAssignments(Collection<VariableExtractionResource> resources) {
        boolean isFieldBound = resources.stream().allMatch(VariableExtractionResource::isFieldBound);
        boolean isEntityBound = resources.stream().noneMatch(VariableExtractionResource::isFieldBound);
        checkArgument(isEntityBound || isFieldBound,
                "Cannot query variables pointing to single fields and entire entities");
    }

    private List<ColumnMapping> createVariableFieldMappings(Collection<VariableExtractionResource> resources) {
        Map<String, ColumnMapping> mappings = new HashMap<>();

        for (VariableExtractionResource resource : resources) {
            VariableConfig config = resource.getVariableConfig();
            String fieldName = sanitizeIfNeeded(config.getFieldName());

            if (isSystemField(fieldName) || mappings.get(fieldName) != null) {
                continue;
            }

            Schema entitySchema = resource.getEntityHistory().getEntitySchema().getSchema();
            Optional<Schema> maybeSchema = getSchemaForField(entitySchema, fieldName);
            maybeSchema.ifPresent(schema -> mappings.put(fieldName,
                    columnMapping(fieldName, VALUE, schema.toString())));
        }

        return new ArrayList<>(mappings.values());
    }

    private Optional<Schema> getSchemaForField(Schema schema, String fieldName) {
        return AvroUtils.findField(schema, fieldName).map(Field::schema);
    }

    private Collection<EntityExtractionResource> toEntityExtractionResource(
            Collection<VariableExtractionResource> resources) {
        Function<VariableExtractionResource, EntityExtractionResource> mapper = variableResource ->
                EntityExtractionResource.builder()
                        .resource(variableResource.getResource())
                        .entityHistory(variableResource.getEntityHistory())
                        .timeWindow(variableResource.getTimeWindow())
                        .build();
        return resources.stream().map(mapper).collect(toSet());
    }

    List<ColumnMapping> getEntityMappings(Collection<EntityExtractionResource> resources, ExtractionCriteria criteria) {
        Set<Schema> schemas = resources.stream().map(this::toHistorySchema).collect(toSet());
        Map<String, List<ColumnMapping>> mappings = new HashMap<>();
        for (Schema schema : schemas) {
            for (Field field : schema.getFields()) {
                String fieldName = field.name();
                if (isSystemField(fieldName) || mappings.get(fieldName) != null) {
                    continue;
                }
                List<String> aliases = getFieldAliases(desanitizeIfNeeded(fieldName), criteria.getAliasFields());
                mappings.put(fieldName, toColumnMapping(fieldName, aliases, field.schema().toString()));
            }
        }
        return mappings.values().stream().flatMap(List::stream).collect(toList());
    }

    private Schema toHistorySchema(ExtractionResource r) {
        return r.getEntityHistory().getEntitySchema().getSchema();
    }

    private List<ColumnMapping> toColumnMapping(String fieldName, List<String> fieldAliases, String schema) {
        if (fieldAliases.isEmpty()) {
            return Collections.singletonList(columnMapping(fieldName, fieldName, schema));
        }
        return fieldAliases.stream().map(a -> columnMapping(fieldName, a, schema)).collect(toList());
    }

    private ColumnMapping columnMapping(String fieldName, String alias, String schema) {
        return ColumnMapping.builder().fieldName(fieldName).alias(alias).schemaJson(schema).build();
    }

    private List<String> getFieldAliases(String fieldName, Map<String, List<String>> aliases) {
        return aliases.entrySet().stream().filter(e -> e.getValue().contains(fieldName)).map(Map.Entry::getKey)
                .collect(toList());
    }
}
