package cern.nxcals.service.internal.extraction;

import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.service.internal.extraction.ExtractionService.SelectionCriteria;
import com.google.common.annotations.VisibleForTesting;
import lombok.NonNull;
import org.apache.avro.Schema;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.StringJoiner;
import java.util.function.Function;

import static cern.nxcals.common.Constants.HBASE_ROW_KEY_DELIMITER;
import static cern.nxcals.common.SystemFields.NXC_EXTR_ENTITY_ID;
import static java.util.Collections.singletonList;

@Service
public class DefaultPredicateProvider implements PredicateProvider {
    private static final int NO_SALT_BUCKETS = -1;
    private static final int CONDITION_STRING_MAX_SIZE = 10000;
    static final String ENTITY_ID = NXC_EXTR_ENTITY_ID.getValue();
    static final String HBASE_KEY_ID_NAME = "entityKey";
    @VisibleForTesting
    static final String HDFS_TIME_PREDICATE = "%s >= %d and %s <= %d";
    static final String HBASE_PREDICATE = "(%1$s >= \"%2$s\" and %1$s < \"%3$s\")";
    @VisibleForTesting
    static final String STRING_EQUALS_AND = "(%s == %s and (%s))";
    @VisibleForTesting
    static final String AND = "(%s) and (%s)";
    @VisibleForTesting
    static final String IN = "(%s in (%s))";

    private final Function<String, Schema.Field> timestampFieldProvider;
    private final int saltBuckets;

    @Autowired
    public DefaultPredicateProvider(Function<String, Schema.Field> timestampFieldProvider,
            @Value("${extraction.service.hbase.salt.buckets:10}") int saltBuckets) {
        this.timestampFieldProvider = timestampFieldProvider;
        if (saltBuckets <= 0 && saltBuckets != NO_SALT_BUCKETS) {
            throw new IllegalArgumentException(
                    "Provided salt buckets number is not valid. Should be >0 or -1. Current value: " + saltBuckets);
        }
        this.saltBuckets = saltBuckets;
    }

    @Override
    public List<String> getHdfsPredicate(@NonNull SelectionCriteria criteria) {
        TimeWindow window = criteria.getTimeWindow();
        String timestampField = timestampFieldProvider.apply(criteria.getSystemSpec().getName()).name();
        String timePredicate = String.format(HDFS_TIME_PREDICATE, timestampField, window.getStartTimeNanos(),
                timestampField, window.getEndTimeNanos());

        String variableAndEntityPredicatesString = getVariableAndEntityPredicates(criteria);

        String predicate = String.format(AND, timePredicate, variableAndEntityPredicatesString);
        return singletonList(predicate);
    }

    private String getVariableAndEntityPredicates(SelectionCriteria criteria) {
        StringJoiner predicatesString = new StringJoiner(" or ");

        Map<Long, String> entityIdsWithPredicate = criteria.getEntityIdWithNotEmptyPredicate();
        Set<Long> entityIdsWithoutPredicate = criteria.getEntityIdsWithoutPredicate();

        if (!entityIdsWithPredicate.isEmpty()) {
            predicatesString.add(getPredicateFromEntityIdAndPredicates(entityIdsWithPredicate));
        }
        if (!entityIdsWithoutPredicate.isEmpty()) {
            predicatesString.add(getPredicateFromEntityId(entityIdsWithoutPredicate));
        }
        return predicatesString.toString();
    }

    private String getPredicateFromEntityIdAndPredicates(Map<Long, String> entityIdAndPredicates) {
        StringJoiner predicatesString = new StringJoiner(" or ");
        for (Map.Entry<Long, String> entityIdWithPredicate : entityIdAndPredicates.entrySet()) {
            predicatesString.add(
                    String.format(STRING_EQUALS_AND, ENTITY_ID, entityIdWithPredicate.getKey(),
                            entityIdWithPredicate.getValue()));
        }
        return predicatesString.toString();
    }

    private String getPredicateFromEntityId(Set<Long> entityIds) {
        return String.format(IN, ENTITY_ID, StringUtils.join(entityIds, ","));
    }

    @Override
    public List<String> getHBasePredicate(@NonNull SelectionCriteria criteria) {
        List<String> predicates = new ArrayList<>();
        Map<Long, Optional<String>> variableEntityPredicates = criteria.getEntitiesWithPredicates();
        StringJoiner joiner = new StringJoiner(" or ");
        for (Long entityId : criteria.getEntityIds()) {
            String entityFilter = createPeriodFilter(entityId, criteria.getTimeWindow());
            Optional<String> maybePredicate = variableEntityPredicates.get(entityId);
            if (maybePredicate.isPresent() && StringUtils.isNotEmpty(maybePredicate.get())) {
                joiner.add(String.format(AND, entityFilter, maybePredicate.get()));
            } else {
                joiner.add(entityFilter);
            }
            if (joiner.length() > CONDITION_STRING_MAX_SIZE) {
                predicates.add(joiner.toString());
                joiner = new StringJoiner(" or ");
            }
        }
        if (joiner.length() > 0) {
            predicates.add(joiner.toString());
        }
        return predicates;
    }

    private String createPeriodFilter(long entityId, TimeWindow window) {
        Pair<String, String> between = getReverseTimestampRowKeyPredicatesFor(entityId, window);
        if (saltBuckets == NO_SALT_BUCKETS) {
            return String.format(HBASE_PREDICATE, HBASE_KEY_ID_NAME, between.getRight(), between.getLeft());
        } else {
            StringJoiner bucketsJoiner = new StringJoiner(" or ");
            for (int i = 0; i < saltBuckets; i++) {
                String bucketPrefix = i + HBASE_ROW_KEY_DELIMITER;
                String bucketFilter = String
                        .format(HBASE_PREDICATE, HBASE_KEY_ID_NAME, bucketPrefix + between.getRight(),
                                bucketPrefix + between.getLeft());
                bucketsJoiner.add(bucketFilter);
            }
            return bucketsJoiner.toString();
        }
    }

    private Pair<String, String> getReverseTimestampRowKeyPredicatesFor(long entityId, TimeWindow window) {
        // the start predicate should start on plus one nanos (+1) from the provided timestamp,
        // in order to avoid record intersects with the HDFS query
        String startPredicate = buildReverseTimestampRowKeyPredicate(entityId, window.getStartTimeNanos() - 1);
        String endPredicate = buildReverseTimestampRowKeyPredicate(entityId, window.getEndTimeNanos());
        return Pair.of(startPredicate, endPredicate);
    }

    private String buildReverseTimestampRowKeyPredicate(long entityId, long timestampNanos) {
        long reversedTimestampNanos = Long.MAX_VALUE - timestampNanos;
        return entityId + HBASE_ROW_KEY_DELIMITER + reversedTimestampNanos;
    }
}
