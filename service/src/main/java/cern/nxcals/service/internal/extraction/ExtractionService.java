package cern.nxcals.service.internal.extraction;

import cern.nxcals.api.domain.EntitySchema;
import cern.nxcals.api.domain.Partition;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.domain.VariableConfig;
import cern.nxcals.api.utils.EncodingUtils;
import cern.nxcals.common.domain.ColumnMapping;
import cern.nxcals.common.domain.EntityExtractionResource;
import cern.nxcals.common.domain.ExtractionCriteria;
import cern.nxcals.common.domain.ExtractionResource;
import cern.nxcals.common.domain.ExtractionTask;
import cern.nxcals.common.domain.ExtractionUnit;
import cern.nxcals.common.domain.ExtractionUnit.VariableMapping;
import cern.nxcals.common.domain.VariableExtractionResource;
import cern.nxcals.common.utils.OptionalUtils;
import cern.nxcals.service.domain.EntityData;
import cern.nxcals.service.domain.VariableData;
import cern.nxcals.service.internal.EntityService;
import cern.nxcals.service.internal.ExtractionResourceService;
import cern.nxcals.service.internal.VariableService;
import cern.nxcals.service.internal.extraction.ExtractionTaskMaker.TaskInput;
import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.avro.Schema;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static cern.nxcals.api.utils.EncodingUtils.desanitizeIfNeeded;
import static cern.nxcals.service.internal.extraction.DataSourceTimeWindow.getTimeWindows;
import static java.util.Collections.emptyMap;
import static java.util.Collections.singletonList;
import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;
import static java.util.stream.Collectors.toSet;
import static lombok.AccessLevel.PACKAGE;
import static org.apache.commons.lang3.StringUtils.defaultIfEmpty;

@Slf4j
@Service
@RequiredArgsConstructor(access = PACKAGE)
public class ExtractionService {
    @NonNull
    private final ExtractionResourceService resourceService;
    @NonNull
    private final EntityService entityService;
    @NonNull
    private final VariableService variableService;
    @NonNull
    private final ColumnMapper columnMapper;
    @NonNull
    private final ExtractionTaskMaker taskMaker;

    @Transactional(transactionManager = "jpaTransactionManager", readOnly = true)
    public ExtractionUnit findExtractionUnitForEntities(ExtractionCriteria criteria) {
        Collection<EntityExtractionResource> resources = resourceService.findEntityResourcesBy(criteria);
        List<DatasourceTaskInput<EntityExtractionResource>> inputs = createExtractionTaskInputsByDatasource(
                resources, criteria.getTimeWindow()
        );
        Set<String> systems = findSystems(criteria);
        List<ColumnMapping> projectionMappings = columnMapper.getEntityMappings(resources, criteria);
        List<ColumnMapping> emptyDSMappings = columnMapper.enhanceWithCommonEntityMappings(projectionMappings, systems);
        List<ExtractionTask> tasks = getEntityExtractionTasksFrom(inputs, criteria, projectionMappings);

        return ExtractionUnit.builder()
                .tasks(tasks)
                .criteria(criteria)
                .mappings(emptyMap())
                .emptyDatasetMapping(emptyDSMappings)
                .build();
    }

    @Transactional(transactionManager = "jpaTransactionManager", readOnly = true)
    public ExtractionUnit findExtractionUnitForVariables(ExtractionCriteria criteria) {
        Collection<VariableExtractionResource> resources = resourceService.findVariableResourcesBy(criteria);
        List<DatasourceTaskInput<VariableExtractionResource>> taskInputs = createExtractionTaskInputsByDatasource(
                resources, criteria.getTimeWindow()
        );
        List<ColumnMapping> projectionMappings = this.columnMapper.getVariableMappings(resources, criteria);
        List<ExtractionTask> tasks = getVariableExtractionTasksFrom(taskInputs, criteria, projectionMappings);
        Set<String> systems = findSystems(criteria);

        List<ColumnMapping> emptyDSMappings = columnMapper.enhanceEmptyVariableDatasetMappings(projectionMappings,
                systems, isFieldBound(resources));

        return ExtractionUnit.builder()
                .tasks(tasks)
                .criteria(criteria)
                .mappings(getEntityToVariableMappings(resources))
                .emptyDatasetMapping(emptyDSMappings)
                .build();
    }

    private List<ExtractionTask> getEntityExtractionTasksFrom(
            List<DatasourceTaskInput<EntityExtractionResource>> inputs,
            ExtractionCriteria criteria, List<ColumnMapping> projectionMappings) {
        return inputs.stream()
                .map(taskInput -> createEntityExtractionTasksFrom(criteria, projectionMappings, taskInput))
                .flatMap(OptionalUtils::toStream)
                .collect(toList());
    }

    private Optional<? extends ExtractionTask> createEntityExtractionTasksFrom(
            ExtractionCriteria criteria,
            List<ColumnMapping> projectionMappings,
            DatasourceTaskInput<EntityExtractionResource> taskInput) {

        ResourcePartition partition = taskInput.getPartition();
        List<EntityExtractionResource> entityExtractionResources = taskInput.getGroupedResources();
        ResourceProjection projection = entitiesProjection(partition.getSchema(), projectionMappings,
                criteria);

        SelectionCriteria selection = createEntitySelection(partition, entityExtractionResources);
        List<ColumnMapping> columns = columnMapper.enhanceWithCommonEntityMappings(projection.getMappings(),
                selection.systemSpec);
        TaskInput input = new TaskInput(columns, selection, entityExtractionResources, criteria);
        return taskInput.getDatasourceTaskMaker().apply(input);
    }

    private SelectionCriteria createEntitySelection(ResourcePartition partition,
            List<EntityExtractionResource> resources) {
        Map<Long, Optional<String>> entityIds = resources.stream()
                .map(resource -> resource.getEntityHistory().getEntity().getId())
                .collect(Collectors.toMap(id -> id, id -> Optional.empty()));
        return new SelectionCriteria(partition.getSystemSpec(), partition.getWindow(), entityIds);
    }

    private List<ExtractionTask> getVariableExtractionTasksFrom(
            List<DatasourceTaskInput<VariableExtractionResource>> inputs,
            ExtractionCriteria criteria, List<ColumnMapping> projectionMappings) {

        return inputs.stream()
                .map(taskInput -> createVariableExtractionTasksFrom(taskInput, criteria, projectionMappings))
                .flatMap(List::stream)
                .collect(toList());
    }

    private List<ExtractionTask> createVariableExtractionTasksFrom(
            DatasourceTaskInput<VariableExtractionResource> taskInput,
            ExtractionCriteria criteria,
            List<ColumnMapping> projectionMappings) {

        List<ExtractionTask> ret = new ArrayList<>();
        ResourcePartition partition = taskInput.getPartition();
        List<VariableExtractionResource> variableExtractionResources = taskInput.getGroupedResources();
        ResourceProjection projection = createVariableProjection(partition.getSchema(),
                variableExtractionResources, criteria, projectionMappings);

        if (projection.isSingleFieldBoundVariableSearch()) {
            for (ColumnMapping singleFieldMapping : projection.getMappings()) {
                List<VariableExtractionResource> matchingResources = filterByMapping(variableExtractionResources,
                        singleFieldMapping);
                SelectionCriteria selection = createVariableSelection(partition, matchingResources);
                List<ColumnMapping> columns = columnMapper.enhanceWithCommonVariableMappings(
                        singletonList(singleFieldMapping), selection.systemSpec);

                TaskInput input = new TaskInput(columns, selection, matchingResources, criteria);
                taskInput.getDatasourceTaskMaker().apply(input).ifPresent(ret::add);
            }
        } else {
            SelectionCriteria selection = createVariableSelection(partition, variableExtractionResources);
            List<ColumnMapping> columns = columnMapper.enhanceWithCommonVariableMappings(projection.getMappings(),
                    selection.systemSpec);
            TaskInput input = new TaskInput(columns, selection, variableExtractionResources, criteria);
            taskInput.getDatasourceTaskMaker().apply(input).ifPresent(ret::add);
        }
        return ret;
    }

    private ResourceProjection createVariableProjection(Schema schema, List<VariableExtractionResource> resources,
            ExtractionCriteria criteria, List<ColumnMapping> globalMappings) {
        boolean isFieldBound = resources.stream()
                .anyMatch(r -> r.getVariableConfig().getFieldName() != null);
        if (isFieldBound) {
            return variablesProjection(resources, globalMappings);
        }
        return entitiesProjection(schema, globalMappings, criteria);
    }

    private SelectionCriteria createVariableSelection(ResourcePartition partition,
            List<VariableExtractionResource> resources) {
        // gather predicates which are to the same entity
        Map<Long, List<VariableExtractionResource>> entityIdWithResources = resources.stream()
                .collect(Collectors.groupingBy(
                        e -> e.getEntityHistory().getEntity().getId()
                ));
        // merge those predicates into one
        Map<Long, Optional<String>> entityIdWithPredicates = entityIdWithResources.entrySet().stream()
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        entry -> mergePredicates(entry.getValue())
                ));
        return new SelectionCriteria(partition.getSystemSpec(), partition.getWindow(), entityIdWithPredicates);
    }

    private Optional<String> mergePredicates(List<VariableExtractionResource> resources) {
        Predicate<VariableExtractionResource> isVariablePredicateEmpty = resource -> StringUtils.isEmpty(
                resource.getVariableConfig().getPredicate());
        if (resources.stream().anyMatch(isVariablePredicateEmpty)) {
            return Optional.empty(); // if at least one predicate is empty, then all values from entity should be read and other predicates don't matter
        }
        Stream<String> predicates = resources.stream().map(x -> x.getVariableConfig().getPredicate());
        return predicates.reduce((x, y) -> x + " or " + y);
    }

    private List<VariableExtractionResource> filterByMapping(List<VariableExtractionResource> extractionResources,
            ColumnMapping mapping) {
        return extractionResources.stream()
                .filter(e -> this.entitySchemaContainsField(e, mapping.getFieldName()))
                .filter(e -> defaultIfEmpty(mapping.getFieldName(), "").equals(sanitizedFieldNameOf(e)))
                .collect(toList());
    }

    private boolean entitySchemaContainsField(ExtractionResource extractionResource, String fieldName) {
        return extractionResource.getEntityHistory().getEntitySchema().getSchema().getField(fieldName) != null;
    }

    private ResourceProjection variablesProjection(List<VariableExtractionResource> resources,
            List<ColumnMapping> globalMappings) {
        if (globalMappings.size() == 1) {
            return new ResourceProjection(globalMappings, true);
        }
        Set<String> variableFields = resources.stream().map(this::sanitizedFieldNameOf).collect(toSet());
        List<ColumnMapping> unionMappings = globalMappings.stream()
                .filter(m -> variableFields.contains(m.getFieldName())).collect(toList());
        return new ResourceProjection(unionMappings, true);
    }

    private String sanitizedFieldNameOf(VariableExtractionResource r) {
        return EncodingUtils.sanitizeIfNeeded(r.getVariableConfig().getFieldName());
    }

    private boolean isFieldBound(Collection<VariableExtractionResource> resources) {
        return resources.stream().anyMatch(VariableExtractionResource::isFieldBound) || resources.isEmpty();
    }

    /**
     * Creates resource projection. If there's no aliases defined the returned projection will contain all the fields
     * defined by the projectionMappings parameter with the irrelevant fields (the ones that are not defined in the
     * partition schema) set as null.
     * However, in case of aliases the outcome will be different - > please see the comments below
     *
     * @param schema             partition schema definition
     * @param projectionMappings combined mappings for all relevant resources (all columns that will be displayed in the
     *                           dataset)
     * @param criteria           querying criteria
     * @return projection columns of the given partition
     */
    private ResourceProjection entitiesProjection(Schema schema, List<ColumnMapping> projectionMappings,
            ExtractionCriteria criteria) {
        List<ColumnMapping> partitionMappings = new ArrayList<>(projectionMappings.size());
        Set<String> schemaFieldNames = schema.getFields().stream().map(Schema.Field::name).collect(toSet());
        for (ColumnMapping mapping : projectionMappings) {
            if (schemaFieldNames.contains(mapping.getFieldName())) {
                partitionMappings.add(mapping);
                continue;
            }
            /*
             * if a global mapping field does not exist in the given partition, yet it's referenced by an alias we do
             * not add it to the partition projected fields as the alias may be used by another column from this
             * partition.
             * example:
             *  given P1: a->alias, P2: b->alias, P3: c->c where PN represents a partition, {a, b, c} - are the field
             *  names.
             *  Global mappings in such case will contain the following mappings: a->alias, b->alias, c->c
             *  however the outcome projection will contain only the following column names: alias, c
             */
            if (criteria.getAliasFields().values().stream().noneMatch(set -> set.contains(mapping.getFieldName()))) {
                partitionMappings.add(mapping.toBuilder().fieldName(null).build());
            }
        }
        /*
         * given the example above we need to add missing alias column for the partition that contains the c
         * field, which is not referenced by an alias so that the result partition conforms to the desired output:
         * alias, c
         */
        pickOnlyOneColumnForAlias(partitionMappings, criteria.getAliasFields());
        addMissingAliasesIfNeeded(projectionMappings, partitionMappings, criteria.getAliasFields());
        partitionMappings.sort(comparing(ColumnMapping::getQualifiedName));
        return new ResourceProjection(partitionMappings, false);
    }

    private void pickOnlyOneColumnForAlias(List<ColumnMapping> partitionMappings, Map<String, List<String>> aliases) {
        Set<String> columnNames = partitionMappings.stream().map(ColumnMapping::getFieldName)
                .map(EncodingUtils::desanitizeIfNeeded)
                .collect(Collectors.toSet());
        Map<String, List<String>> mutableAliasFieldsCopy = copyMap(aliases);
        mutableAliasFieldsCopy.values().forEach(l -> l.removeIf(field -> !columnNames.contains(field)));
        partitionMappings.removeIf(m -> aliasDoesNotMatchMapping(mutableAliasFieldsCopy, m));
    }

    private Map<String, List<String>> copyMap(Map<String, List<String>> map) {
        return map.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, e -> new ArrayList<>(e.getValue())));
    }

    private boolean aliasDoesNotMatchMapping(Map<String, List<String>> fields, ColumnMapping mapping) {
        return fields.containsKey(mapping.getAlias()) && !fields.get(mapping.getAlias()).get(0)
                .equals(desanitizeIfNeeded(mapping.getFieldName()));
    }

    private void addMissingAliasesIfNeeded(List<ColumnMapping> globalMappings, List<ColumnMapping> partitionMappings,
            Map<String, List<String>> aliases) {
        if (Utils.isMapEmpty(aliases)) {
            return;
        }
        if (globalMappings.size() == partitionMappings.size()) {
            return;
        }
        for (Map.Entry<String, List<String>> aliasAndFields : aliases.entrySet()) {
            if (partitionMappings.stream().noneMatch(m -> m.getAlias().equals(aliasAndFields.getKey()))) {
                Optional<ColumnMapping> exampleAlias = globalMappings.stream()
                        .filter(m -> aliasAndFields.getValue().stream().anyMatch(f -> f.equals(m.getFieldName()))
                                && m.getAlias().equals(aliasAndFields.getKey())).findFirst();
                exampleAlias.ifPresent(alias -> partitionMappings.add(alias.toBuilder().fieldName(null).build()));
            }
        }
    }

    private Map<Long, List<VariableMapping>> getEntityToVariableMappings(
            Collection<VariableExtractionResource> resources) {
        Map<Long, Map<TimeWindow, Map<String, String>>> mappings = new HashMap<>();
        for (VariableExtractionResource e : resources) {
            long entityId = e.getEntityHistory().getEntity().getId();
            mappings.computeIfAbsent(entityId, k -> new HashMap<>())
                    .computeIfAbsent(e.getTimeWindow(), k -> new HashMap<>())
                    .put(getFieldNameFrom(e.getVariableConfig()),
                            e.getVariableConfig().getVariableName()); // field name can be null - wtf
        }
        Map<Long, List<VariableMapping>> ret = new HashMap<>();
        mappings.forEach((entityId, timeWindowToFieldNameAndVariableName) -> {
            List<VariableMapping> variableMappings = timeWindowToFieldNameAndVariableName.entrySet().stream()
                    .map(this::toVariableMapping)
                    .collect(toList());
            ret.put(entityId, variableMappings);
        });
        return ret;
    }

    private VariableMapping toVariableMapping(Map.Entry<TimeWindow, Map<String, String>> e) {
        return VariableMapping.builder().window(e.getKey()).fieldToVariableName(e.getValue()).build();
    }

    private static String getFieldNameFrom(VariableConfig config) {
        return config.getFieldName() != null ? EncodingUtils.sanitizeIfNeeded(config.getFieldName()) : null;
    }

    private Set<String> findSystems(ExtractionCriteria criteria) {
        HashSet<String> systems = new HashSet<>();
        systems.addAll(findSystemNamesForEntityIds(criteria.getEntityIds()));
        systems.addAll(findSystemNamesForVariableIds(criteria.getVariableIds()));
        if (criteria.getSystemName() != null) {
            systems.add(criteria.getSystemName());
        }
        return systems;
    }

    private Set<String> findSystemNamesForEntityIds(Set<Long> ids) {
        Set<EntityData> entities = entityService.findExactlyByIdIn(ids);
        return entities.stream().map(entity -> entity.toEntity().getSystemSpec().getName()).collect(toSet());
    }

    private Set<String> findSystemNamesForVariableIds(Set<Long> ids) {
        Set<VariableData> variables = variableService.findExactlyByIdIn(ids);
        return variables.stream().map(variable -> variable.toVariable().getSystemSpec().getName()).collect(toSet());
    }

    @Data
    private static class ResourceProjection {
        private final List<ColumnMapping> mappings;
        private final boolean isSingleFieldBoundVariableSearch;
    }

    @Data
    static class ResourcePartition {
        @NonNull
        private final Partition partition;
        @NonNull
        private final EntitySchema entitySchema;
        @NonNull
        private final TimeWindow window;

        ResourcePartition(@NonNull ExtractionResource resource, @NonNull TimeWindow window) {
            this.partition = resource.getEntityHistory().getPartition();
            this.entitySchema = resource.getEntityHistory().getEntitySchema();
            this.window = window;
        }

        private Schema getSchema() {
            return entitySchema.getSchema();
        }

        private SystemSpec getSystemSpec() {
            return partition.getSystemSpec();
        }
    }

    @Data
    static class SelectionCriteria {
        private final SystemSpec systemSpec;
        private final TimeWindow timeWindow;
        private final Map<Long, Optional<String>> entitiesWithPredicates;     // entity id and predicate/empty string

        public Set<Long> getEntityIds() {
            return entitiesWithPredicates.keySet();
        }

        public Set<Long> getEntityIdsWithoutPredicate() {
            return entitiesWithPredicates.entrySet().stream()
                    .filter(entry -> !entry.getValue().isPresent() || StringUtils.isEmpty(entry.getValue().get()))
                    .map(Map.Entry::getKey)
                    .collect(toSet());
        }

        public Map<Long, String> getEntityIdWithNotEmptyPredicate() {
            return entitiesWithPredicates.entrySet().stream()
                    .filter(entry -> entry.getValue().isPresent() && StringUtils.isNotEmpty(entry.getValue().get()))
                    .collect(toMap(Map.Entry::getKey, entry -> entry.getValue().get()));
        }
    }

    private <T extends ExtractionResource> List<DatasourceTaskInput<T>> createExtractionTaskInputsByDatasource(
            Collection<T> extractionResources, TimeWindow timeWindow) {
        DataSourceTimeWindow timeWindows = getTimeWindows(timeWindow);
        List<DatasourceTaskInput<T>> tasks = new ArrayList<>();

        Stream.of(timeWindow)
                .flatMap(tw -> getResourcePartitionWithResources(extractionResources, tw, true).entrySet().stream())
                .map(entry -> new DatasourceTaskInput<>(taskMaker::hbaseTask, entry.getKey(), entry.getValue()))
                .forEach(tasks::add);
        OptionalUtils.toStream(timeWindows.getHbaseTimeWindow())
                .flatMap(tw -> getResourcePartitionWithResources(extractionResources, tw, false).entrySet().stream())
                .map(entry -> new DatasourceTaskInput<>(taskMaker::hbaseTask, entry.getKey(), entry.getValue()))
                .forEach(tasks::add);
        OptionalUtils.toStream(timeWindows.getHdfsTimeWindow())
                .flatMap(tw -> getResourcePartitionWithResources(extractionResources, tw, false).entrySet().stream())
                .map(entry -> new DatasourceTaskInput<>(taskMaker::hdfsTask, entry.getKey(), entry.getValue()))
                .forEach(tasks::add);
        return tasks;
    }

    @Data
    private static class DatasourceTaskInput<T extends ExtractionResource> {
        private final Function<TaskInput, Optional<? extends ExtractionTask>> datasourceTaskMaker;
        private final ResourcePartition partition;
        private final List<T> groupedResources;
    }

    /**
     * Filters out partitions which are/are not updatable and which are valid in given time window, and group them by
     * partition and schema (ResourcePartition)
     *
     * @param resources       extraction resources to filter and group
     * @param tw              time window, in which resources must be valid
     * @param isDataUpdatable if resources must be updatable
     * @return filtered resources grouped by
     */
    private <T extends ExtractionResource> Map<ResourcePartition, List<T>> getResourcePartitionWithResources(
            Collection<T> resources, TimeWindow tw, boolean isDataUpdatable) {
        Predicate<T> isValidInTimeWindow = resource -> resource.getTimeWindow().intersects(tw);
        Predicate<T> isUpdatable = resource -> resource.getEntityHistory().getPartition().getProperties().isUpdatable();
        Predicate<T> filter = isValidInTimeWindow.and(resource -> isUpdatable.test(resource) == isDataUpdatable);
        return resources.stream()
                .filter(filter)
                .collect(groupingBy(r -> new ResourcePartition(r, r.getTimeWindow().intersect(tw))));
    }
}
