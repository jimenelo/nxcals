/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.service.internal;

import cern.nxcals.service.IdGeneratorFactory;
import cern.nxcals.service.domain.SequenceType;

/**
 * @author Marcin Sobieszek
 * @date Jul 19, 2016 10:32:35 AM
 */
@FunctionalInterface
public interface IdGenerator {

    long getIdFor(SequenceType type);

    static IdGenerator getDetault() {
        return IdGeneratorFactory.getGenerator();
    }
}
