/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.internal;

import cern.nxcals.common.concurrent.AutoCloseableLock;
import cern.nxcals.service.config.DataLocationProperties;
import com.google.common.util.concurrent.Striped;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.util.StringUtils;

import java.util.concurrent.locks.Lock;

@EnableConfigurationProperties(DataLocationProperties.class)
class BaseService {
    private final Striped<Lock> locks = Striped.lazyWeakLock(1000);

    BaseService() { }

    AutoCloseableLock getLockFor(String name) {
        if (!StringUtils.hasLength(name)) {
            throw new IllegalArgumentException("Name must not be null");
        }
        return AutoCloseableLock.getFor(this.locks.get(name));
    }
}
