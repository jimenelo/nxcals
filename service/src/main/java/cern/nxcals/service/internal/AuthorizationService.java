package cern.nxcals.service.internal;

import cern.nxcals.api.custom.domain.DelegationToken;
import lombok.extern.slf4j.Slf4j;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.security.token.ClientTokenUtil;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Cluster;
import org.apache.hadoop.security.UserGroupInformation;
import org.apache.hadoop.security.token.Token;
import org.apache.hadoop.yarn.conf.YarnConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.security.PrivilegedExceptionAction;
import java.util.Map;

@Service
@Slf4j
public class AuthorizationService {

    private final Map<String, TokenProvider> providers;

    @Autowired
    public AuthorizationService(Map<String, TokenProvider> providers) {
        this.providers = providers;
        log.info("Using token providers {}", providers);
    }

    public DelegationToken createDelegationToken(String userName) {
        UserGroupInformation user = getUser(userName);

        providers.entrySet().forEach(entry -> {
            String beanId = entry.getKey();
            TokenProvider provider = entry.getValue();

            addToken(user, beanId, provider);
        });

        byte[] storage = writeTokenStorage(user);

        return DelegationToken.builder().withStorage(storage).build();
    }

    private static UserGroupInformation getUser(String userName) {
        UserGroupInformation serviceUser;
        try {
            serviceUser = UserGroupInformation.getCurrentUser();
        } catch (IOException e) {
            throw new UncheckedIOException("Could not get service user", e);
        }

        return UserGroupInformation.createProxyUser(userName, serviceUser);
    }

    private static void addToken(UserGroupInformation user, String beanId, TokenProvider provider) {
        Token<?> token;
        try {
            token = user.doAs(provider);
        } catch (IOException e) {
            throw new UncheckedIOException("Failed to obtain token from " + beanId + " for user " + user, e);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new IllegalStateException("Failed to obtain token from " + beanId + " for user " + user, e);
        }

        log.debug("Got token from {} for user {}: {}", beanId, user, token);

        user.addToken(token);
    }

    private static byte[] writeTokenStorage(UserGroupInformation user) {
        ByteArrayOutputStream byteOutput = new ByteArrayOutputStream();
        try (DataOutputStream dataOutput = new DataOutputStream(byteOutput)) {
            user.getCredentials().writeTokenStorageToStream(dataOutput);
        } catch (IOException e) {
            throw new UncheckedIOException("Could not write credentials for user " + user, e);
        }
        return byteOutput.toByteArray();
    }

    @FunctionalInterface
    public interface TokenProvider extends PrivilegedExceptionAction<Token<?>> {
        // @see PrivilegedExceptionAction
    }

    @Component
    public static class YarnTokenProvider implements TokenProvider {

        @Override
        public Token<?> run() throws IOException, InterruptedException {
            Configuration conf = new Configuration();
            Cluster cluster = new Cluster(new YarnConfiguration(conf));
            return cluster.getDelegationToken(new Text("yarn"));
        }

    }

    @Component
    public static class HdfsTokenProvider implements TokenProvider {

        @Override
        public Token<?> run() throws IOException {
            Configuration conf = new Configuration();
            try (FileSystem fileSystem = FileSystem.get(conf)) {
                return fileSystem.getDelegationToken("yarn");
            }
        }

    }

    @Component
    public static class HBaseTokenProvider implements TokenProvider {

        @Override
        public Token<?> run() throws IOException {
            Configuration conf = HBaseConfiguration.create();
            try (Connection connection = ConnectionFactory.createConnection(conf)) {
                return ClientTokenUtil.obtainToken(connection);
            }
        }

    }

}
