package cern.nxcals.integrationtests.service;

import cern.nxcals.api.domain.Hierarchy;
import cern.nxcals.api.domain.HierarchyChangelog;
import cern.nxcals.api.domain.OperationType;
import cern.nxcals.api.extraction.metadata.queries.HierarchyChangelogs;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeAll;

import java.time.Duration;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class HierarchyChangelogServiceTest extends AbstractTest {

    private static Hierarchy nodeA;
    private static Hierarchy nodeB;
    private static Hierarchy nodeC;

    @BeforeAll
    public static void createHierarchy() {
        nodeA = hierarchyService
                .create(Hierarchy.builder().name(getRandomString()).systemSpec(SYSTEM).build());
        nodeB = hierarchyService
                .create(Hierarchy.builder().name(getRandomString()).parent(nodeA).systemSpec(SYSTEM).build());
        nodeC = hierarchyService
                .create(Hierarchy.builder().name(getRandomString()).systemSpec(SYSTEM).build());
    }

    @Test
    public void shouldFindHierarchyChangelog() {
        assertNotNull(nodeB);
        HierarchyChangelog foundHierarchyChangelog = getInsertedHierarchyChangelog(nodeB.getId());
        assertEquals(nodeB.getId(), foundHierarchyChangelog.getHierarchyId());
        assertEquals(nodeB.getName(), foundHierarchyChangelog.getNewHierarchyName());
        assertEquals(nodeB.getParent().getId(), (long) foundHierarchyChangelog.getNewParentId());
    }

    @Test
    public void shouldFindChangelogsByTime() {
        assertNotNull(nodeA);
        Set<HierarchyChangelog> foundHierarchyChangelogs = hierarchyChangelogService
                .findAll(HierarchyChangelogs.suchThat().creationTimeUtc().notOlderThan(Duration.ofHours(1)));
        assertFalse(foundHierarchyChangelogs.isEmpty());
        assertTrue(foundHierarchyChangelogs.stream().anyMatch(c -> c.getHierarchyId() == nodeA.getId()));
    }

    @Test
    public void shouldFindChangelogsByOperationType() {
        assertNotNull(nodeA);
        Set<HierarchyChangelog> foundHierarchyChangelogs = hierarchyChangelogService
                .findAll(HierarchyChangelogs.suchThat().opType().eq(OperationType.INSERT));
        assertFalse(foundHierarchyChangelogs.isEmpty());
        assertTrue(foundHierarchyChangelogs.stream().anyMatch(c -> c.getHierarchyId() == nodeA.getId()));
    }

    private HierarchyChangelog getInsertedHierarchyChangelog(Long hierarchyId) {
        return hierarchyChangelogService
                .findOne(HierarchyChangelogs.suchThat().hierarchyId().eq(hierarchyId).and().opType()
                        .eq(OperationType.INSERT))
                .orElseThrow(() -> new IllegalStateException("no data"));
    }

    private HierarchyChangelog getUpdatedHierarchyChangelog(Long entityId) {
        return hierarchyChangelogService
                .findOne(HierarchyChangelogs.suchThat().hierarchyId().eq(entityId).and()
                        .opType().eq(OperationType.UPDATE))
                .orElseThrow(() -> new IllegalStateException("no data"));
    }

    @Test
    public void shouldUpdateHierarchyAndFindHierarchyChangelog() {
        assertNotNull(nodeB);

        //check for changelog
        HierarchyChangelog firstChangelog = getInsertedHierarchyChangelog(nodeA.getId());
        assertNotNull(firstChangelog);
        //update Entity
        Hierarchy changedHierarchy = nodeB.toBuilder().parent(nodeC).build();

        Hierarchy updatedHierarchy = hierarchyService.update(changedHierarchy);
        HierarchyChangelog secondChangelog = getUpdatedHierarchyChangelog(updatedHierarchy.getId());

        assertEquals(nodeA.getId(), (long) secondChangelog.getOldParentId());
        assertEquals(nodeC.getId(), (long) secondChangelog.getNewParentId());
    }
}
