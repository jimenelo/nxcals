package cern.nxcals.integrationtests.service;

import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.extraction.metadata.ServiceClientFactory;
import cern.nxcals.api.extraction.metadata.SystemSpecService;
import cern.rbac.client.authentication.AuthenticationClient;
import cern.rbac.client.authentication.AuthenticationException;
import cern.rbac.common.RbaToken;
import cern.rbac.util.holder.ClientTierTokenHolder;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.SpringBootDependencyInjectionTestExecutionListener;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(SpringExtension.class)
@TestPropertySource("classpath:application.yml")

//jwozniak - due to a bug (mockito in spring boot, https://github.com/spring-projects/spring-boot/issues/6520) this is needed.
//Otherwise there is an exception when spring boot tries to reset mocks that actually are not used here.
@TestExecutionListeners(listeners = { SpringBootDependencyInjectionTestExecutionListener.class })
public class RbacAccessTest {
    //    static {
    //        //Please leave this one just in case you want to run those test from IDE for debugging
    //        System.setProperty("service.url", "https://nxcals-jwozniak1.cern.ch:19093"); //Do not use localhost here.
    //    }

    //This has to be done on the acclog user as otherwise we would have to encrypt
    //all our developers passwords inside the ansible-vault.
    private final String user = "acclog";

    @Value("${acclog.password}")
    private String password;

    @AfterEach
    public void tearDown() {
        ClientTierTokenHolder.clear();
    }

    @Test
    public void shouldAccessServiceWithRbac() throws AuthenticationException {

        AuthenticationClient authenticationClient = AuthenticationClient.create();
        RbaToken token = authenticationClient.loginExplicit(user, password);
        ClientTierTokenHolder.setRbaToken(token);

        SystemSpecService systemSpecService = ServiceClientFactory.createSystemSpecService();
        Set<SystemSpec> all = systemSpecService.findAll();
        assertTrue(all.size() > 0);
    }

}
