/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.integrationtests.service;

import cern.nxcals.api.domain.KeyValues;
import cern.nxcals.api.domain.Partition;
import cern.nxcals.api.domain.PartitionProperties;
import cern.nxcals.api.extraction.metadata.queries.Partitions;
import com.google.common.collect.ImmutableMap;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Created by jwozniak on 02/07/17.
 */
public class PartitionServiceTest extends AbstractTest {

    @Test
    public void shouldFindPartition() {
        KeyValues entityKeyValues = new KeyValues(ENTITY_KEY_VALUES.get("device").hashCode(), ENTITY_KEY_VALUES);
        KeyValues partitionKeyValues = new KeyValues(0, PARTITION_KEY_VALUES);
        internalEntityService
                .findOrCreateEntityFor(SYSTEM.getId(), entityKeyValues, partitionKeyValues, "brokenSchema1", RECORD_TIMESTAMP);

        Partition partitionData = internalPartitionService.findOne(Partitions.suchThat().systemId().eq(SYSTEM.getId()).and().keyValues().eq(SYSTEM, PARTITION_KEY_VALUES))
                .orElseThrow(() -> new IllegalArgumentException("No such partition"));
        assertNotNull(partitionData);

        assertEquals(PARTITION_KEY_VALUES, partitionData.getKeyValues());
    }

    @Test
    public void shouldCreatePartition() {
        Partition partition = Partition.builder().properties(new PartitionProperties(true)).systemSpec(SYSTEM)
                .keyValues(PARTITION_KEY_VALUES_1)
                .build();
        Partition savedPartition = internalPartitionService.create(partition);
        assertNotNull(savedPartition);
        assertEquals(SYSTEM, savedPartition.getSystemSpec());
        assertEquals(PARTITION_KEY_VALUES_1, savedPartition.getKeyValues());
    }

    @Test
    public void shouldNotFindPartition() {
        Partition partitionData = internalPartitionService.findOne(
            Partitions.suchThat().systemId().eq(SYSTEM.getId()).and().keyValues().eq(SYSTEM,
                        ImmutableMap.of("specification", "SHOULD_NOT_BE_FOUND_PARTITION_KEY_VALUES"))).orElse(null);
        assertNull(partitionData);
    }

    @Test
    public void shouldThrowOptimisticLockException() {
        KeyValues entityKeyValues = new KeyValues(ENTITY_KEY_VALUES.get("device").hashCode(), ENTITY_KEY_VALUES);
        KeyValues partitionKeyValues = new KeyValues(0, PARTITION_KEY_VALUES);
        internalEntityService
                .findOrCreateEntityFor(SYSTEM.getId(), entityKeyValues, partitionKeyValues, "brokenSchema1", RECORD_TIMESTAMP);
        Partition partitionData = internalPartitionService.findOne(Partitions.suchThat().systemId().eq(SYSTEM.getId()).and().keyValues().eq(SYSTEM, PARTITION_KEY_VALUES))
                .orElseThrow(() -> new IllegalArgumentException("No such partition"));
        assertNotNull(partitionData);
        assertEquals(PARTITION_KEY_VALUES, partitionData.getKeyValues());
        assertFalse(partitionData.getProperties().isUpdatable());
        assertEquals(0L, partitionData.getRecVersion());
        Partition newPartition = partitionData.toBuilder().properties(PartitionProperties.builder().updatable(true).build()).build();
        Partition updatedPartition =  internalPartitionService.update(newPartition);
        assertTrue(updatedPartition.getProperties().isUpdatable());
        assertEquals(1L, updatedPartition.getRecVersion());
        Partition anotherNewPartition = partitionData.toBuilder().properties(PartitionProperties.builder().updatable(true).build()).build();
        assertThrows(RuntimeException.class, () -> internalPartitionService.update(anotherNewPartition));
   }

}
