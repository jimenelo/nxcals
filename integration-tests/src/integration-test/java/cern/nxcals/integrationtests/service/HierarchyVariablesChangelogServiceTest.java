package cern.nxcals.integrationtests.service;

import cern.nxcals.api.domain.Hierarchy;
import cern.nxcals.api.domain.HierarchyVariablesChangelog;
import cern.nxcals.api.domain.OperationType;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.extraction.metadata.queries.HierarchyVariablesChangelogs;
import cern.nxcals.api.extraction.metadata.queries.Variables;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeAll;

import java.time.Duration;
import java.util.Collections;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class HierarchyVariablesChangelogServiceTest extends AbstractTest {

    private static Hierarchy nodeA;
    private static Hierarchy nodeB;
    private final Variable variableA = getOrCreateVariable("my-hierarchy-variable-A");
    private final Variable variableB = getOrCreateVariable("my-hierarchy-variable-B");

    @BeforeAll
    public static void createHierarchy() {
        nodeA = hierarchyService
                .create(Hierarchy.builder().name(getRandomString()).systemSpec(SYSTEM).build());
        nodeB = hierarchyService
                .create(Hierarchy.builder().name(getRandomString()).systemSpec(SYSTEM).build());
    }

    @Test
    public void shouldAddVariableToHierarchyAndFindChangelog() {
        assertNotNull(nodeA);
        assertThat(hierarchyService.getVariables(nodeA.getId())).isEmpty();
        hierarchyService.setVariables(nodeA.getId(), Collections.singleton(variableB.getId()));
        assertThat(hierarchyService.getVariables(nodeA.getId())).contains(variableB);
        HierarchyVariablesChangelog foundChangelog = getAddedVariableChangelog(nodeA.getId(), variableB.getId());
        assertEquals(foundChangelog.getNewHierarchyId().longValue(), nodeA.getId());
        assertEquals(foundChangelog.getNewVariableId().longValue(), variableB.getId());

    }

    @Test
    public void shouldRemoveVariableFromHierarchyAndFindChangelog() {
        assertNotNull(nodeA);
        hierarchyService.setVariables(nodeA.getId(), Collections.singleton(variableA.getId()));
        assertThat(hierarchyService.getVariables(nodeA.getId())).contains(variableA);
        hierarchyService.removeVariables(nodeA.getId(), Collections.singleton(variableA.getId()));
        assertThat(hierarchyService.getVariables(nodeA.getId())).isEmpty();
        HierarchyVariablesChangelog foundChangelog = getRemovedVariableChangelog(nodeA.getId(), variableA.getId());
        assertEquals(foundChangelog.getOldHierarchyId().longValue(), nodeA.getId());
        assertEquals(foundChangelog.getOldVariableId().longValue(), variableA.getId());
    }

    @Test
    public void shouldFindChangelogsByTime() {
        assertNotNull(nodeB);
        hierarchyService.setVariables(nodeB.getId(), Collections.singleton(variableB.getId()));
        assertThat(hierarchyService.getVariables(nodeB.getId())).contains(variableB);
        Set<HierarchyVariablesChangelog> foundHierarchyChangelogs = hierarchyVariablesChangelogService
                .findAll(HierarchyVariablesChangelogs.suchThat().creationTimeUtc().notOlderThan(Duration.ofHours(1)));
        assertFalse(foundHierarchyChangelogs.isEmpty());
    }

    @Test
    public void shouldFindChangelogsByOperationType() {
        assertNotNull(nodeB);
        hierarchyService.setVariables(nodeB.getId(), Collections.singleton(variableA.getId()));
        assertThat(hierarchyService.getVariables(nodeB.getId())).contains(variableA);
        Set<HierarchyVariablesChangelog> foundHierarchyChangelogs = hierarchyVariablesChangelogService
                .findAll(HierarchyVariablesChangelogs.suchThat().opType().eq(OperationType.INSERT));
        assertFalse(foundHierarchyChangelogs.isEmpty());
    }

    private Variable getOrCreateVariable(String name) {
        return variableService
                .findOne(Variables.suchThat().systemName().eq(MOCK_SYSTEM_NAME).and().variableName().eq(name))
                .orElseGet(() -> variableService.create(getVariableData(name)));
    }

    private HierarchyVariablesChangelog getAddedVariableChangelog(Long hierarchyId, Long variableId) {
        return hierarchyVariablesChangelogService
                .findOne(HierarchyVariablesChangelogs.suchThat().newHierarchyId().eq(hierarchyId).and()
                        .newVariableId().eq(variableId).and().opType()
                        .eq(OperationType.INSERT))
                .orElseThrow(() -> new IllegalStateException("no data"));
    }

    private HierarchyVariablesChangelog getRemovedVariableChangelog(Long hierarchyId, Long variableId) {
        return hierarchyVariablesChangelogService
                .findOne(HierarchyVariablesChangelogs.suchThat().oldHierarchyId().eq(hierarchyId).and().oldVariableId().eq(variableId).and().opType()
                        .eq(OperationType.DELETE))
                .orElseThrow(() -> new IllegalStateException("no data"));
    }
}
