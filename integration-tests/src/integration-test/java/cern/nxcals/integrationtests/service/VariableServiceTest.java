/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.integrationtests.service;

import avro.shaded.com.google.common.collect.ImmutableSet;
import cern.nxcals.api.domain.Identifiable;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.domain.VariableConfig;
import cern.nxcals.api.domain.VariableDeclaredType;
import cern.nxcals.api.exceptions.FatalDataConflictRuntimeException;
import cern.nxcals.api.exceptions.NotFoundRuntimeException;
import cern.nxcals.api.extraction.metadata.queries.Variables;
import cern.nxcals.api.metadata.queries.VariableQueryWithOptions;
import cern.nxcals.api.utils.TimeUtils;
import cern.nxcals.common.utils.ReflectionUtils;
import com.github.rutledgepaulv.qbuilders.conditions.Condition;
import com.google.common.collect.ImmutableSortedSet;
import org.assertj.core.util.Sets;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.UUID;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toSet;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class VariableServiceTest extends AbstractTest {
    @Test
    public void shouldCreateAndFindVariable() {
        //lets first find some existing entity for which we will create variable
        // given
        String variableName = getRandomName(VARIABLE_NAME_PREFIX);
        Variable variableData = this.getVariableData(variableName);
        // when
        variableService.create(variableData);
        // then
        assertNotNull(variableData);
        assertEquals(variableName, variableData.getVariableName());

        Variable foundVariable = getVariable(variableName);
        assertEquals(variableName, foundVariable.getVariableName());
    }

    @Test
    public void shouldCreateAndExtendVariableConfigs() {
        // given
        String variableName = getRandomName(VARIABLE_NAME_PREFIX);
        Variable variableData = this.getVariableData(variableName);
        // when
        Variable variable = variableService.create(variableData);

        SortedSet<VariableConfig> configData = variable.getConfigs();
        Instant now = Instant.now();
        VariableConfig existingConfig = configData.first();
        VariableConfig modifiedConfig = existingConfig.toBuilder().validity(TimeWindow.between(null, now)).build();
        VariableConfig newConfig = VariableConfig.builder().entityId(existingConfig.getEntityId())
                .fieldName(existingConfig.getFieldName()).validity(TimeWindow.between(now, null)).build();
        ImmutableSortedSet<VariableConfig> newConfigs = ImmutableSortedSet.of(modifiedConfig, newConfig);
        Variable updatedData = variable.toBuilder().configs(newConfigs).build();
        // when
        variable = variableService.update(updatedData);

        // then
        assertNotNull(variable);
        assertEquals(variableName, variableData.getVariableName());

        Variable foundVariable = getVariable(variableName);
        assertEquals(variableName, foundVariable.getVariableName());
        assertEquals(newConfigs, foundVariable.getConfigs());

        ImmutableSortedSet<VariableConfig> newestConfig = ImmutableSortedSet
                .of(VariableConfig.builder().entityId(existingConfig.getEntityId()).fieldName("field_ala").build());
        updatedData = foundVariable.toBuilder().configs(newestConfig).build();
        // when
        variable = variableService.update(updatedData);

        // then
        assertNotNull(variable);
        assertEquals(variableName, variableData.getVariableName());

        foundVariable = getVariable(variableName);
        assertEquals(variableName, foundVariable.getVariableName());
        assertEquals(newestConfig, foundVariable.getConfigs());
    }

    @Test
    public void shouldCreateAndUpdateVariable() {
        String variableName = getRandomName(VARIABLE_NAME_PREFIX);
        Variable variableData = this.getVariableData(variableName);
        // when
        Variable persistedVariable = variableService.create(variableData);

        Variable foundVariable = getVariable(variableName);
        assertEquals(persistedVariable.getId(), foundVariable.getId());
        String newVariableName = getRandomName(VARIABLE_NAME_PREFIX);

        Variable newVariable = foundVariable.toBuilder().variableName(newVariableName).build();
        Variable updated = variableService.update(newVariable);
        assertEquals(newVariableName, updated.getVariableName());
        newVariableName = getRandomName(VARIABLE_NAME_PREFIX);

        newVariable = updated.toBuilder().variableName(newVariableName).build();
        updated = variableService.update(newVariable);
        assertEquals(newVariableName, updated.getVariableName());
    }

    @Test
    public void shouldNotUpdateVariableWithNonExistingId() {
        // given
        Variable sourceVariable = this.getVariableData(getRandomName(VARIABLE_NAME_PREFIX));
        long nonExistingId = findNonExistingVariableId();
        Variable variable = ReflectionUtils.builderInstance(Variable.InnerBuilder.class)
                .id(nonExistingId)
                .variableName(sourceVariable.getVariableName()).systemSpec(sourceVariable.getSystemSpec())
                .description(sourceVariable.getDescription()).unit(sourceVariable.getUnit()).build();
        assertEquals(nonExistingId, variable.getId());

        // when
        assertThrows(NotFoundRuntimeException.class, () -> variableService.update(variable));
    }

    private long findNonExistingVariableId() {
        long upperBound = -500_000L;
        long step = 50_000L;
        long lowerBound = Math.subtractExact(upperBound, step * 100);
        for (long variableId = upperBound; variableId >= lowerBound; variableId -= step) {
            if (!variableService.findById(variableId).isPresent()) {
                return variableId;
            }
        }
        throw new IllegalStateException(
                "Could not find non-existing variable id with upper bound value: " + upperBound);
    }

    @Test
    public void shouldNotUpdateVariableWithOlderRecordVersion() {
        // given
        Variable variable = variableService.create(this.getVariableData(getRandomName(VARIABLE_NAME_PREFIX)));
        assertNotNull(variable);
        assertNotEquals(Identifiable.NOT_SET, variable.getId());
        assertEquals(0L, variable.getRecVersion());

        String updatedVariableName = getRandomName(VARIABLE_NAME_PREFIX);
        Variable updatedVariable = variableService.update(variable.toBuilder()
                .variableName(updatedVariableName).build());
        assertNotNull(updatedVariable);
        assertEquals(variable.getId(), updatedVariable.getId());
        assertEquals(updatedVariableName, updatedVariable.getVariableName());
        assertEquals(1L, updatedVariable.getRecVersion());

        // use the original variableData as update input to simulate call with older/outdated data
        assertThrows(RuntimeException.class, () -> variableService.update(variable));
    }

    @Test
    public void shouldNotFindVariable() {
        String nonExistingSystem = "NoSuchSystem";
        String nonExistentVariable = "NoSuchVariable" + System.currentTimeMillis();
        Variable foundVariable = variableService.findOne(
                        Variables.suchThat().systemName().eq(nonExistingSystem).and().variableName().eq(nonExistentVariable))
                .orElse(null);
        assertNull(foundVariable);
    }

    @Test
    public void shouldFindBySystemNameAndVariableNameLike() {
        // given
        String variableNameSuffix = getRandomString() + "findBySystemAndVariable";
        String variableName = VARIABLE_NAME_PREFIX + variableNameSuffix;
        Variable variableData = this.getVariableData(variableName);
        // when
        variableService.create(variableData);

        String variableLike = VARIABLE_NAME_LIKE_PREFIX + variableNameSuffix;
        Set<Variable> fetchedVariables = variableService.findAll(
                Variables.suchThat().systemName().eq(SYSTEM.getName()).and().variableName().like(variableLike));
        //then
        assertEquals(1, fetchedVariables.size());
    }

    @Test
    public void shouldNotCreateSeveralVariablesWithTheSameName() {
        // given
        String variableName = getRandomName(VARIABLE_NAME_PREFIX);
        Variable variable = this.getVariableData(variableName);
        // when
        variableService.create(variable);
        assertThrows(FatalDataConflictRuntimeException.class, () -> variableService.create(variable));
    }

    @Test
    public void shouldNotUpdateVariableToExistingName() {
        // given
        String variableOneName = getRandomName(VARIABLE_NAME_PREFIX);
        String variableTwoName = getRandomName(VARIABLE_NAME_PREFIX);
        Variable variableOne = this.getVariableData(variableOneName);
        Variable variableTwo = this.getVariableData(variableTwoName);
        variableService.create(variableOne);
        Variable variableTwoCreated = variableService.create(variableTwo);
        Variable variableTwoWithNameOne = variableTwoCreated.toBuilder().variableName(variableOneName).build();

        // when
        assertThrows(FatalDataConflictRuntimeException.class, () -> variableService.update(variableTwoWithNameOne));
    }

    @Test
    public void shouldFindByVariableNameIn() {
        // given
        String variableSuffix = "findByVariableName";

        String variableName1 = getRandomName(VARIABLE_NAME_PREFIX) + variableSuffix;
        Variable variableData = this.getVariableData(variableName1);
        variableService.create(variableData);

        String variableName2 = getRandomName(VARIABLE_NAME_2_PREFIX) + variableSuffix;
        Variable variableData2 = this.getVariableData(variableName2);
        variableService.create(variableData2);

        //when
        Set<Variable> variables = variableService.findAll(Variables.suchThat().variableName()
                .in(variableData.getVariableName(), variableData2.getVariableName()));

        //then
        assertEquals(2, variables.size());
        Set<String> collect = variables.stream().map(Variable::getVariableName).collect(toSet());
        assertTrue(collect.contains(variableData.getVariableName()));
        assertTrue(collect.contains(variableData2.getVariableName()));
    }

    @Test
    public void shouldAlwaysExtractFullVariables() {
        String name = getRandomName(VARIABLE_NAME_PREFIX);
        Variable variable = getVariableData(name);

        long entityId = variable.getConfigs().first().getEntityId();

        VariableConfig configA = VariableConfig.builder().entityId(entityId).fieldName("fieldA")
                .validity(TimeWindow.before(1000)).build();
        VariableConfig configB = VariableConfig.builder().entityId(entityId).fieldName("fieldB")
                .validity(TimeWindow.after(1000)).build();

        Variable createdVariable = variableService.create(
                variable.toBuilder().configs(Sets.newTreeSet(configA, configB)).build());

        final Condition<Variables> conditionA = Variables.suchThat().id().eq(createdVariable.getId());
        assertThat(variableService.findOne(conditionA).get().getConfigs()).hasSize(2);
        final Condition<Variables> conditionB = Variables.suchThat().id().eq(createdVariable.getId()).and()
                .configsFieldName().eq("fieldA");
        assertThat(variableService.findOne(conditionB).get().getConfigs()).hasSize(2);
        final Condition<Variables> conditionC = Variables.suchThat().id().eq(createdVariable.getId()).and()
                .configsFieldName().eq("fieldB");
        assertThat(variableService.findOne(conditionC).get().getConfigs()).hasSize(2);
    }

    @Test
    public void shouldFindByDeclaredTypeAndNameIn() {
        // given
        String variableNameSuffix = "findByDeclaredTypeAndNameIn";

        String variableName1 = getRandomName(VARIABLE_NAME_PREFIX) + variableNameSuffix;
        Variable variableData = this.getVariableData(variableName1, VariableDeclaredType.FUNDAMENTAL);
        variableService.create(variableData);

        String variableName2 = getRandomName(VARIABLE_NAME_2_PREFIX) + variableNameSuffix;
        Variable variableData2 = this.getVariableData(variableName2);
        variableService.create(variableData2);

        //when
        Set<Variable> variables = variableService.findAll(
                Variables.suchThat().declaredType().eq(VariableDeclaredType.FUNDAMENTAL).and().variableName()
                        .in(variableData.getVariableName(), variableData2.getVariableName()));

        //then
        assertEquals(1, variables.size());
        Set<String> collect = variables.stream().map(Variable::getVariableName).collect(toSet());
        assertTrue(collect.contains(variableData.getVariableName()));
        assertFalse(collect.contains(variableData2.getVariableName()));
    }

    @Test
    public void shouldFindByDeclaredTypeAndNameLike() {
        // given
        String variableNameSuffix = getRandomString() + "findByDeclaredTypeAndNameLike";

        String variableName1 = VARIABLE_NAME_PREFIX + variableNameSuffix;
        Variable variableData = this.getVariableData(variableName1, VariableDeclaredType.FUNDAMENTAL);
        variableService.create(variableData);

        String variableName2 = VARIABLE_NAME_2_PREFIX + variableNameSuffix;
        Variable variableData2 = this.getVariableData(variableName2);
        variableService.create(variableData2);

        //when
        String variableLike = VARIABLE_NAME_LIKE_PREFIX + variableNameSuffix;
        Set<Variable> variables = variableService.findAll(
                Variables.suchThat().declaredType().eq(VariableDeclaredType.FUNDAMENTAL).and().variableName()
                        .like(variableLike));

        //then
        assertEquals(1, variables.size());
        Set<String> collect = variables.stream().map(Variable::getVariableName).collect(toSet());
        assertTrue(collect.contains(variableData.getVariableName()));
        assertFalse(collect.contains(variableData2.getVariableName()));
    }

    @Test
    public void shouldFindByDeclaredTypeNullAndNameLike() {
        // given
        String variableNameSuffix = getRandomString() + "findByDeclaredTypeNullAndNameLike";
        String variableName1 = VARIABLE_NAME_PREFIX + variableNameSuffix;
        Variable variableData = this.getVariableData(variableName1, VariableDeclaredType.FUNDAMENTAL);
        variableService.create(variableData);

        String variableName2 = VARIABLE_NAME_2_PREFIX + variableNameSuffix;
        Variable variableData2 = this.getVariableData(variableName2);
        variableService.create(variableData2);

        //when
        String variableLike = "VAR%" + variableNameSuffix;
        Set<Variable> variables = variableService
                .findAll(Variables.suchThat().declaredType().doesNotExist().and().variableName().like(variableLike));

        //then
        assertEquals(1, variables.size());
        Set<String> collect = variables.stream().map(Variable::getVariableName).collect(toSet());
        assertFalse(collect.contains(variableData.getVariableName()));
        assertTrue(collect.contains(variableData2.getVariableName()));
    }

    @Test
    public void shouldFindByVariableNameInReturnEmptySetWhenNotFound() {
        // given
        String nonExistingVariableName = "does_not_exist" + System.currentTimeMillis();

        //when
        Set<Variable> variables = variableService
                .findAll(Variables.suchThat().variableName().in(nonExistingVariableName));

        //then
        assertEquals(0, variables.size());
    }

    @Test
    public void shouldFindByVariableNameInReturnEmptySetWhenEmptyArrayAsArgs() {
        //given

        //when

        //then
        assertThrows(IllegalArgumentException.class,
                () -> variableService.findAll(Variables.suchThat().variableName().in()));
    }

    @Test
    public void shouldFindByVariableNameInThrowForNullArgument() {
        // given

        //when
        assertThrows(NullPointerException.class, () ->
                variableService.findAll(Variables.suchThat().variableName().in((String[]) null)));
    }

    @Test
    public void shouldUpdateVariableWithRecVersionBiggerThanZero() {
        String testVariableName = "VARIABLE_NAME" + UUID.randomUUID().toString() + "testingMultipleVariableUpdates";
        Variable variableData = this.getVariableData(testVariableName);

        //registerG
        Variable newVariable = variableService.create(variableData);
        assertEquals(0, newVariable.getRecVersion());
        assertNotEquals(0, newVariable.getId());
        long newVariableRecVersion = newVariable.getRecVersion();

        //update part 1
        VariableConfig updatedVariableConfig = newVariable.getConfigs().iterator().next().toBuilder()
                .fieldName("updatedField").build();
        SortedSet<VariableConfig> updateConfigs = new TreeSet<>();
        updateConfigs.add(updatedVariableConfig);
        String newUnit = "updates/test";
        Variable initiallyUpdatedVariable = variableService
                .update(newVariable.toBuilder().configs(updateConfigs).unit(newUnit).build());
        assertNotNull(initiallyUpdatedVariable);
        assertEquals(newVariable.getId(), initiallyUpdatedVariable.getId());
        assertEquals(newUnit, initiallyUpdatedVariable.getUnit());
        assertEquals(newVariableRecVersion + 1, initiallyUpdatedVariable.getRecVersion());
        assertEquals(updatedVariableConfig.getFieldName(),
                initiallyUpdatedVariable.getConfigs().iterator().next().getFieldName());

        //update part 2
        String newDescription = "Oops! I forgot to initially update description! Let's do it now!";
        Variable description = initiallyUpdatedVariable.toBuilder().description(newDescription).build();

        Variable lastUpdatedVariable = variableService.update(description);
        assertNotNull(lastUpdatedVariable);
        assertEquals(newDescription, lastUpdatedVariable.getDescription());
        assertEquals(newVariableRecVersion + 2, lastUpdatedVariable.getRecVersion());

    }

    @Test
    public void shouldCreateMultipleVariables() {
        String variableName = getRandomName(VARIABLE_NAME_PREFIX);
        Variable variableData1 = this.getVariableData(variableName + "1");
        Variable variableData2 = this.getVariableData(variableName + "2");
        // when
        Set<Variable> createdVariables = variableService.createAll(ImmutableSet.of(variableData1, variableData2));
        // then
        assertNotNull(createdVariables);
        for (Variable createdVariable : createdVariables) {
            assertTrue(createdVariable.getVariableName().contains(variableName));
        }
    }

    @Test
    public void shouldUpdateMultipleVariables() {
        String variableName = getRandomName(VARIABLE_NAME_PREFIX);
        Variable variableData1 = this.getVariableData(variableName + "1");
        Variable variableData2 = this.getVariableData(variableName + "2");
        // when
        Set<Variable> createdVariables = variableService.createAll(ImmutableSet.of(variableData1, variableData2));
        // then
        assertNotNull(createdVariables);

        Set<Variable> variablesToUpdate = createdVariables.stream()
                .map(v -> v.toBuilder().variableName(getRandomName(VARIABLE_NAME_PREFIX)).build()).collect(
                        toSet());
        Set<Variable> updated = variableService.updateAll(variablesToUpdate);
        assertTrue(updated.stream().map(Variable::getVariableName).collect(toSet())
                .containsAll(variablesToUpdate.stream().map(Variable::getVariableName).collect(toSet())));
    }

    @Test
    public void shouldDeleteVariable() {
        String variableName = getRandomName(VARIABLE_NAME_PREFIX);
        Variable variableData = this.getVariableData(variableName);
        // when
        Variable createdVariable = variableService.create(variableData);
        // then
        assertNotNull(createdVariable);
        assertEquals(variableName, createdVariable.getVariableName());

        Variable foundVariable = getVariable(variableName);
        assertEquals(variableName, foundVariable.getVariableName());
        variableService.delete(createdVariable.getId());
        assertEquals(Optional.empty(), variableService.findById(createdVariable.getId()));
    }

    @Test
    public void shouldDelete1kVariables() {
        String variableName = getRandomName(VARIABLE_NAME_PREFIX);
        Set<Variable> variables = IntStream.range(1, 1101).mapToObj(i -> this.getVariableData(variableName + i))
                .collect(toSet());
        Set<Variable> createdVariables = variableService.createAll(variables);
        assertEquals(1100, createdVariables.size());
        variableService.deleteAll(createdVariables.stream().map(Variable::getId).collect(toSet()));
        for (Variable createdVariable : createdVariables) {
            assertFalse(variableService.findById(createdVariable.getId()).isPresent());
        }

    }

    @Test
    public void shouldUpdate1kVariables() {
        String variableName = getRandomName(VARIABLE_NAME_PREFIX);
        Set<Variable> variables = IntStream.range(1, 1101).mapToObj(i -> this.getVariableData(variableName + i))
                .collect(toSet());
        Set<Variable> createdVariables = variableService.createAll(variables);
        assertEquals(1100, createdVariables.size());
        Set<Variable> updatedVariables = createdVariables.stream()
                .map(v -> v.toBuilder().variableName(v.getVariableName() + "_updated").build()).collect(toSet());
        variableService.updateAll(updatedVariables);
        Set<Variable> foundData = variableService.findAll(Variables.suchThat().variableName()
                .in(updatedVariables.stream().map(Variable::getVariableName).collect(toSet())));
        assertEquals(1100, foundData.size());
        variableService.deleteAll(createdVariables.stream().map(Variable::getId).collect(toSet()));
    }

    @Test
    void shouldThrowExceptionWhenCreatingVariableWithNoEntity() {
        SystemSpec systemSpec = systemService.findByName("CMW")
                .orElseThrow(() -> new IllegalArgumentException("No such system"));

        VariableConfig variableConfig = VariableConfig.builder()
                .validity(TimeWindow.between(TimeUtils.getInstantFromString("2019-03-19 00:00:00.000"), null))
                .build();

        Variable variable = Variable.builder().variableName("DEMO:VAR" + UUID.randomUUID()).systemSpec(systemSpec)
                .description("Demo variable")
                .declaredType(VariableDeclaredType.NUMERIC).configs(ImmutableSortedSet.of(variableConfig))
                .build();

        assertThrows(NotFoundRuntimeException.class, () -> variableService.create(variable));
    }

    @Nested
    @DisplayName("Test queries withOptions")
    public class VariableQueryWithOptionTests {
        String variableName = getRandomName(VARIABLE_NAME_PREFIX);
        final int numberOfVariables = 100;
        Set<Variable> createdVariables;

        @BeforeEach
        void createVariables() {
            Set<Variable> variables = IntStream.range(0, numberOfVariables)
                    .mapToObj(i -> getVariableData(variableName + i))
                    .collect(toSet());
            createdVariables = variableService.createAll(variables);
        }

        @AfterEach
        void deleteVariables() {
            variableService.deleteAll(createdVariables.stream().map(Variable::getId).collect(toSet()));
        }

        @Test
        void shouldGetSortedDescListOfVariables() {
            // given
            VariableQueryWithOptions query = newVariables().variableName().like(variableName + "%").withOptions()
                    .orderBy().variableName()
                    .desc();

            // when
            List<Variable> variables = variableService.findAll(query);

            // then
            assertAll(
                    () -> assertEquals(numberOfVariables, variables.size()),
                    () -> assertEquals(variableName + "99", variables.get(0).getVariableName()),
                    () -> assertEquals(variableName + "98", variables.get(1).getVariableName())
            );
        }

        @Test
        void shouldGetSortedAscListOfVariables() {
            // given
            VariableQueryWithOptions query = newVariables().variableName().like(variableName + "%").withOptions()
                    .orderBy().variableName()
                    .asc();

            // when
            List<Variable> variables = variableService.findAll(query);

            // then
            assertAll(
                    () -> assertEquals(numberOfVariables, variables.size()),
                    () -> assertEquals(variableName + "0", variables.get(0).getVariableName()),
                    () -> assertEquals(variableName + "1", variables.get(1).getVariableName())
            );
        }

        @Test
        void shouldGetLimitedAndOffsetListOfVariables() {
            // given
            VariableQueryWithOptions query = newVariables().variableName().like(variableName + "%").withOptions()
                    .orderBy().variableName()
                    .asc().limit(10).offset(10);

            // when
            List<Variable> variables = variableService.findAll(query);

            // then
            assertAll(
                    () -> assertEquals(10, variables.size()),
                    () -> assertEquals(variableName + "18", variables.get(0).getVariableName()), // 0, 1, 10, 11...
                    () -> assertEquals(variableName + "19", variables.get(1).getVariableName())
            );
        }

        @Test
        void shouldGetVariableWithConfig() {
            // given
            VariableQueryWithOptions query = newVariables().variableName().like(variableName + "%").withOptions()
                    .limit(1);

            // when
            Optional<Variable> maybeVariable = variableService.findOne(query);

            // then
            assertAll(
                    () -> assertTrue(maybeVariable.isPresent()),
                    () -> assertFalse(maybeVariable.get().getConfigs().isEmpty())
            );
        }

        @Test
        void shouldGetVariableWithoutConfig() {
            // given
            VariableQueryWithOptions query = newVariables().variableName().like(variableName + "%").withOptions()
                    .limit(1).noConfigs();

            // when
            Optional<Variable> maybeVariable = variableService.findOne(query);

            // then
            assertAll(
                    () -> assertTrue(maybeVariable.isPresent()),
                    () -> assertTrue(maybeVariable.get().getConfigs().isEmpty())
            );
        }

        private cern.nxcals.api.metadata.queries.Variables newVariables() {
            return cern.nxcals.api.metadata.queries.Variables.suchThat();
        }
    }
}
