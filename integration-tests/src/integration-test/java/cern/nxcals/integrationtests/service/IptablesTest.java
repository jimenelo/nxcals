package cern.nxcals.integrationtests.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@TestPropertySource("classpath:application.yml")
public class IptablesTest {
    @Value("${private_service.url}")
    private String privateServicesUrls;

    @Value("${public_service.url}")
    private String publicServicesUrls;

    private final int timeout = 3000;

    @Test
    public void shouldNotBeAbleToConnectToPrivateServices() {
        String[] hosts = getHosts(privateServicesUrls);
        int numberOfUnrecheableServices = 0;

        for (String privateServiceHost : hosts) {
            String privateServiceUrl = getUrl(privateServiceHost);
            int port = getPort(privateServiceHost);
            try {
                try (Socket sock = new Socket()) {
                    sock.connect(new InetSocketAddress(privateServiceUrl, port), timeout);
                }
            } catch (IOException exception) {
                numberOfUnrecheableServices++;
            }
        }
        assertEquals(hosts.length, numberOfUnrecheableServices);
    }

    @Test
    public void shouldConnectToPublicServices() {
        String[] hosts = getHosts(publicServicesUrls);
        int numberOfUnrecheableServices = 0;

        for (String publicServiceHost : hosts) {
            try {
                String publicServiceUrl = getUrl(publicServiceHost);
                int port = getPort(publicServiceHost);
                try(Socket sock = new Socket()){
                    sock.connect(new InetSocketAddress(publicServiceUrl, port), timeout);
                }
            } catch (IOException exception) {
                numberOfUnrecheableServices++;
            }
        }
        assertEquals(0, numberOfUnrecheableServices);
    }

    private String[] getHosts(String fullUrl) {
        return fullUrl.replaceAll(" ", "").split(",");
    }

    private String getUrl(String fullUrl) {
        String[] urlAndPort = fullUrl.split(":");
        return urlAndPort[1].replaceAll("/", "");
    }

    private int getPort(String fullUrl) {
        String[] urlAndPort = fullUrl.split(":");
        return Integer.parseInt(urlAndPort[2]);
    }
}