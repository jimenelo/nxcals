package cern.nxcals.integrationtests.service;

import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.Hierarchy;
import cern.nxcals.api.domain.HierarchyView;
import cern.nxcals.api.domain.KeyValues;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.domain.VariableHierarchies;
import cern.nxcals.api.domain.VariableHierarchyIds;
import cern.nxcals.api.extraction.metadata.queries.Hierarchies;
import cern.nxcals.api.extraction.metadata.queries.Variables;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toSet;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class HierarchyServiceTest extends AbstractTest {

    private static final String NAME_PREFIX = "HierarchyServiceTest-";

    private String getRandomName() {
        return getRandomName(NAME_PREFIX);
    }

    @AfterAll
    public static void cleanUp() {
        for (int i = 0; i < 10; i++) {
            Set<Hierarchy> hierarchies = hierarchyService.findAll(
                    Hierarchies.suchThat().name().like(NAME_PREFIX + "%"));
            if (hierarchies.isEmpty()) {
                break;
            }
            hierarchyService.deleteAllLeaves(hierarchies.stream().filter(Hierarchy::isLeaf).collect(toSet()));
        }
        Set<Variable> variables = variableService.findAll(Variables.suchThat().variableName().like(NAME_PREFIX + "%"));
        variableService.deleteAll(variables.stream().map(Variable::getId).collect(toSet()));
    }

    @Test
    public void shouldCreateHierarchy() {
        Hierarchy nodeA = hierarchyService.create(
                Hierarchy.builder().name(getRandomName()).systemSpec(SYSTEM).build());
        Hierarchy nodeB = hierarchyService.create(
                Hierarchy.builder().name(getRandomName()).parent(nodeA).systemSpec(SYSTEM).build());

        Hierarchy createdParent = getByPath(nodeA.getNodePath());
        Hierarchy createdChild = getByPath(nodeB.getNodePath());

        assertNotNull(createdChild);
        assertNotNull(createdParent);

        assertThat(createdParent.isLeaf()).isFalse();
        assertThat(createdChild.isLeaf()).isTrue();

        assertThat(createdParent.getLevel()).isEqualTo(1);
        assertThat(createdChild.getLevel()).isEqualTo(2);

        assertEquals(createdParent.getName(), createdChild.getParent().getName());
        assertEquals(1, createdParent.getChildren().size());
    }

    @Test
    public void shouldCreateAllHierarchies() {
        Hierarchy nodeA = hierarchyService
                .create(Hierarchy.builder().name(getRandomName()).systemSpec(SYSTEM).build());

        Hierarchy nodeB = Hierarchy.builder().name(getRandomName()).parent(nodeA).systemSpec(SYSTEM).build();
        Hierarchy nodeC = Hierarchy.builder().name(getRandomName()).parent(nodeA).systemSpec(SYSTEM).build();
        Set<Hierarchy> toCreate = new HashSet<>(Arrays.asList(nodeB, nodeC));

        hierarchyService.createAll(toCreate);

        Hierarchy createdParent = getByPath(nodeA.getNodePath());
        assertNotNull(createdParent);
        assertThat(createdParent.isLeaf()).isFalse();
        assertThat(createdParent.getLevel()).isEqualTo(1);
        assertEquals(2, createdParent.getChildren().size());

        for (Hierarchy node : toCreate) {
            Hierarchy createdChild = getByPath(node.getNodePath());
            assertEquals(createdParent.getName(), createdChild.getParent().getName());
            assertNotNull(createdChild);
            assertThat(createdChild.isLeaf()).isTrue();
            assertThat(createdChild.getLevel()).isEqualTo(2);
        }
    }

    @Test
    public void shouldUpdateHierarchy() {
        Hierarchy nodeA = hierarchyService
                .create(Hierarchy.builder().name(getRandomName()).description("desc-A").systemSpec(SYSTEM).build());

        Hierarchy newNodeA = hierarchyService.update(nodeA.toBuilder().description("desc-B").build());

        Hierarchy updated = getByPath(newNodeA.getNodePath());

        assertNotNull(updated);
        assertEquals(newNodeA, updated);
        assertEquals("desc-B", updated.getDescription());
    }

    @Test
    public void shouldUpdateAllHierarchies() {
        Hierarchy nodeA = Hierarchy.builder().name(getRandomName()).description("desc-A").systemSpec(SYSTEM).build();
        Hierarchy nodeB = Hierarchy.builder().name(getRandomName()).description("desc-B").systemSpec(SYSTEM).build();

        Set<Hierarchy> createdNodes = hierarchyService.createAll(Sets.newHashSet(nodeA, nodeB));

        assertNotNull(createdNodes);
        assertEquals(2, createdNodes.size());
        Set<Hierarchy> nodesToUpdate = new HashSet<>();
        for (Hierarchy node : createdNodes) {
            nodesToUpdate.add(node.toBuilder().description(node.getDescription() + "_updated").build());
        }
        Set<Hierarchy> updatedNodes = hierarchyService.updateAll(nodesToUpdate);
        assertNotNull(updatedNodes);
        assertEquals(2, updatedNodes.size());

        for (Hierarchy updatedNode : updatedNodes) {
            Hierarchy fetched = getByPath(updatedNode.getNodePath());
            assertNotNull(fetched);
            assertEquals(updatedNode, fetched);
            assertTrue(fetched.getDescription().endsWith("_updated"));
        }
    }

    @Test
    public void shouldDeleteHierarchy() {
        Hierarchy nodeA = hierarchyService
                .create(Hierarchy.builder().name(getRandomName()).systemSpec(SYSTEM).build());
        Hierarchy nodeB = hierarchyService
                .create(Hierarchy.builder().name(getRandomName()).parent(nodeA).systemSpec(SYSTEM).build());

        assertThat(hierarchyService.findById(nodeA.getId())).isPresent();
        assertThat(hierarchyService.findById(nodeB.getId())).isPresent();

        hierarchyService.deleteLeaf(nodeB);

        assertThat(hierarchyService.findById(nodeA.getId())).isPresent();
        assertThat(hierarchyService.findById(nodeB.getId())).isNotPresent();
    }

    @Test
    public void shouldDeleteAllLeafHierarchies() {
        Hierarchy nodeA = hierarchyService
                .create(Hierarchy.builder().name(getRandomName()).systemSpec(SYSTEM).build());
        assertThat(hierarchyService.findById(nodeA.getId())).isPresent();

        Hierarchy node1 = Hierarchy.builder().name(getRandomName()).parent(nodeA).systemSpec(SYSTEM).build();
        Hierarchy node2 = Hierarchy.builder().name(getRandomName()).parent(nodeA).systemSpec(SYSTEM).build();

        Set<Hierarchy> createdNodes = hierarchyService.createAll(Sets.newHashSet(node1, node2));
        assertNotNull(createdNodes);
        assertEquals(2, createdNodes.size());

        for (Hierarchy node : createdNodes) {
            assertThat(hierarchyService.findById(node.getId())).isPresent();
        }

        hierarchyService.deleteAllLeaves(createdNodes);

        assertThat(hierarchyService.findById(nodeA.getId())).isPresent();
        for (Hierarchy node : createdNodes) {
            assertThat(hierarchyService.findById(node.getId())).isNotPresent();
        }
    }

    @Test
    public void shouldCreateHierarchyAndManageVariables() {
        Hierarchy nodeA = hierarchyService
                .create(Hierarchy.builder().name(getRandomName()).systemSpec(SYSTEM).build());
        Variable myVariableA = getOrCreateVariable("my-hierarchy-variable-A");
        Variable myVariableB = getOrCreateVariable("my-hierarchy-variable-B");

        assertThat(hierarchyService.getVariables(nodeA.getId())).isEmpty();

        hierarchyService.setVariables(nodeA.getId(), Collections.singleton(myVariableA.getId()));
        assertThat(hierarchyService.getVariables(nodeA.getId())).contains(myVariableA);

        hierarchyService.addVariables(nodeA.getId(), Collections.singleton(myVariableB.getId()));
        assertThat(hierarchyService.getVariables(nodeA.getId())).containsExactlyInAnyOrder(myVariableA,
                myVariableB);

        hierarchyService.removeVariables(nodeA.getId(), Collections.singleton(myVariableA.getId()));
        assertThat(hierarchyService.getVariables(nodeA.getId())).containsExactlyInAnyOrder(myVariableB);

        hierarchyService.setVariables(nodeA.getId(), Collections.<Long>emptySet());
        assertThat(hierarchyService.getVariables(nodeA.getId())).isEmpty();
    }

    @Test
    public void shouldCreateHierarchyAndManageMultipleVariables() {
        Hierarchy nodeA = hierarchyService.create(
                Hierarchy.builder().name(getRandomName()).systemSpec(SYSTEM).build());

        Variable myVariable = getOrCreateVariable("my-hierarchy-variable-A");

        Set<Variable> myVariables = Sets.newHashSet(myVariable,
                getOrCreateVariable("my-hierarchy-variable-B"),
                getOrCreateVariable("my-hierarchy-variable-C"),
                getOrCreateVariable("my-hierarchy-variable-D"),
                getOrCreateVariable("my-hierarchy-variable-E"));

        Variable myNewVariable = getOrCreateVariable("my-hierarchy-variable-F");

        hierarchyService.setVariables(nodeA.getId(),
                myVariables.stream().map(Variable::getId).collect(Collectors.toSet()));
        assertThat(hierarchyService.getVariables(nodeA.getId())).contains(myVariable);

        myVariables.remove(myVariable);
        myVariables.add(myNewVariable);

        hierarchyService.setVariables(nodeA.getId(),
                myVariables.stream().map(Variable::getId).collect(Collectors.toSet()));
        assertThat(hierarchyService.getVariables(nodeA.getId())).contains(myNewVariable);

        Hierarchy nodeB = nodeA.toBuilder().description("new-description").name(nodeA.getName() + "_edited")
                .build();

        hierarchyService.update(nodeB);
    }

    @Test
    public void shouldCreateHierarchyAndManageEntities() {
        Hierarchy nodeA = hierarchyService.create(
                Hierarchy.builder().name(getRandomName()).systemSpec(SYSTEM).build());
        Entity myEntityA = getOrCreateEntity(10);
        Entity myEntityB = getOrCreateEntity(11);

        assertThat(hierarchyService.getEntities(nodeA.getId())).isEmpty();

        hierarchyService.setEntities(nodeA.getId(), Collections.singleton(myEntityA.getId()));
        assertThat(hierarchyService.getEntities(nodeA.getId())).contains(myEntityA);

        hierarchyService.addEntities(nodeA.getId(), Collections.singleton(myEntityB.getId()));
        assertThat(hierarchyService.getEntities(nodeA.getId())).containsExactlyInAnyOrder(myEntityA, myEntityB);

        hierarchyService.removeEntities(nodeA.getId(), Collections.singleton(myEntityA.getId()));
        assertThat(hierarchyService.getEntities(nodeA.getId())).containsExactlyInAnyOrder(myEntityB);

        hierarchyService.setEntities(nodeA.getId(), Collections.<Long>emptySet());
        assertThat(hierarchyService.getEntities(nodeA.getId())).isEmpty();
    }

    @Test
    public void shouldGetHierarchiesForVariable() {
        Hierarchy nodeA = hierarchyService
                .create(Hierarchy.builder().name(getRandomName()).systemSpec(SYSTEM).build());
        Hierarchy nodeB = hierarchyService
                .create(Hierarchy.builder().name(getRandomName()).systemSpec(SYSTEM).build());
        Variable myVariable = getOrCreateVariable("my-hierarchy-variable-A-" + getRandomName());

        Set<Long> myVariables = Collections.singleton(myVariable.getId());
        hierarchyService.addVariables(nodeA.getId(), myVariables);
        hierarchyService.addVariables(nodeB.getId(), myVariables);

        Set<Hierarchy> foundHierarchies = hierarchyService.getHierarchiesForVariable(myVariable.getId());
        assertEquals(2, foundHierarchies.size());
        assertTrue(foundHierarchies.contains(nodeA));
        assertTrue(foundHierarchies.contains(nodeB));
    }

    @Test
    public void shouldReturnEmptySetOnGetHierarchiesForVariablesWhenVariableDoesNotExist() {
        Set<VariableHierarchies> hierarchiesByVariableId = hierarchyService
                .getHierarchiesForVariables(Collections.singleton(1234L));
        assertThat(hierarchiesByVariableId).isEmpty();
    }

    @Test
    public void shouldReturnEmptySetOnGetHierarchiesForVariableWhenVariableHasNoAssociationsWithHierarchy() {
        Variable v1 = getOrCreateVariable(getRandomName());
        Variable v2 = getOrCreateVariable(getRandomName());
        Set<VariableHierarchies> hierarchiesByVariableId = hierarchyService
                .getHierarchiesForVariables(Sets.newHashSet(v1.getId(), v2.getId(), 1234L));
        assertThat(hierarchiesByVariableId).isEmpty();
    }

    @Test
    public void shouldGetHierarchiesForVariables() {
        String prefix = getRandomName();
        Hierarchy nodeA = hierarchyService.create(Hierarchy.builder().name(prefix + "_A").systemSpec(SYSTEM).build());
        Hierarchy nodeB = hierarchyService.create(Hierarchy.builder().name(prefix + "_B").systemSpec(SYSTEM).build());

        Variable myVariableA = getOrCreateVariable(prefix + "_my-hierarchy-variable-A");
        Variable myVariableB = getOrCreateVariable(prefix + "_my-hierarchy-variable-B");

        hierarchyService.addVariables(nodeA.getId(), Sets.newHashSet(myVariableA.getId(), myVariableB.getId()));
        hierarchyService.addVariables(nodeB.getId(), Collections.singleton(myVariableA.getId()));

        Set<VariableHierarchies> variableHierarchies = hierarchyService.getHierarchiesForVariables(
                Sets.newHashSet(myVariableA.getId(), myVariableB.getId()));

        assertThat(variableHierarchies).isNotEmpty();
        assertThat(variableHierarchies.size()).isEqualTo(2);

        Map<Long, Set<Hierarchy>> hierarchiesByVariableId = variableHierarchies.stream().collect(Collectors.toMap(
                VariableHierarchies::getVariableId, VariableHierarchies::getHierarchies));
        Set<Hierarchy> hierarchiesForVarA = hierarchiesByVariableId.get(myVariableA.getId());
        assertThat(hierarchiesForVarA).isNotEmpty();
        assertThat(hierarchiesForVarA.size()).isEqualTo(2);
        assertEquals(Sets.newHashSet(nodeA.getId(), nodeB.getId()),
                hierarchiesForVarA.stream().map(Hierarchy::getId).collect(toSet()));

        Set<Hierarchy> hierarchiesForVarB = hierarchiesByVariableId.get(myVariableB.getId());
        assertThat(hierarchiesForVarB).isNotEmpty();
        assertThat(hierarchiesForVarB.size()).isEqualTo(1);
        assertEquals(Collections.singleton(nodeA.getId()),
                hierarchiesForVarB.stream().map(Hierarchy::getId).collect(toSet()));
    }

    @Test
    public void shouldGetHierarchiesForOneVariable() {
        String prefix = getRandomName();
        Hierarchy nodeA = hierarchyService.create(Hierarchy.builder().name(prefix + "_A").systemSpec(SYSTEM).build());
        Hierarchy nodeB = hierarchyService.create(Hierarchy.builder().name(prefix + "_B").systemSpec(SYSTEM).build());

        Variable myVariableA = getOrCreateVariable(prefix + "_my-hierarchy-variable-A");
        Variable myVariableB = getOrCreateVariable(prefix + "_my-hierarchy-variable-B");

        hierarchyService.addVariables(nodeA.getId(), Sets.newHashSet(myVariableA.getId(), myVariableB.getId()));
        hierarchyService.addVariables(nodeB.getId(), Collections.singleton(myVariableA.getId()));

        Set<VariableHierarchies> variableHierarchies = hierarchyService.getHierarchiesForVariables(
                Sets.newHashSet(myVariableA.getId()));

        assertThat(variableHierarchies).isNotEmpty();
        assertThat(variableHierarchies.size()).isEqualTo(1);
        VariableHierarchies variableHierarchy = Iterables.getOnlyElement(variableHierarchies);
        assertEquals(myVariableA.getId(), variableHierarchy.getVariableId());
        Set<Hierarchy> hierarchies = variableHierarchy.getHierarchies();
        assertThat(variableHierarchy.getHierarchies()).isNotEmpty();
        assertThat(hierarchies.size()).isEqualTo(2);
        assertEquals(Sets.newHashSet(nodeA.getId(), nodeB.getId()),
                hierarchies.stream().map(Hierarchy::getId).collect(toSet()));
    }

    @Test
    public void shouldGetHierarchiesForOver1kVariables() {
        int numOfVariables = 1010;
        Set<Long> variableIds = createVariables(numOfVariables, "hierarchy-variable-over1k");

        Hierarchy nodeA = hierarchyService.create(Hierarchy.builder()
                .name(getRandomName()).systemSpec(SYSTEM).build());

        hierarchyService.addVariables(nodeA.getId(), variableIds);

        Set<VariableHierarchies> variableHierarchies = hierarchyService.getHierarchiesForVariables(variableIds);
        assertThat(variableHierarchies).isNotEmpty();
        assertEquals(numOfVariables, variableHierarchies.size());
        variableHierarchies.forEach(e -> {
            assertTrue(variableIds.contains(e.getVariableId()));
            Set<Hierarchy> hierarchies = e.getHierarchies();
            assertThat(hierarchies).isNotEmpty();
            assertEquals(1, hierarchies.size());
            assertEquals(nodeA.getId(), Iterables.getOnlyElement(hierarchies).getId());
        });
    }

    private Set<Long> createVariables(int numOfVariables, String prefix) {
        Set<Variable> variablesToCreate = new HashSet<>();
        String suffix = getRandomName();
        for (int i = 0; i < numOfVariables; i++) {
            Variable variable = getVariableData(
                    String.join("_", prefix, suffix, Integer.toString(i + 1)));
            variablesToCreate.add(variable);
        }
        return variableService.createAll(variablesToCreate).stream()
                .map(Variable::getId).collect(toSet());
    }

    @Test
    public void shouldGetHierarchyIdsForVariables() {
        String prefix = getRandomName();
        Hierarchy nodeA = hierarchyService.create(Hierarchy.builder().name(prefix + "_A").systemSpec(SYSTEM).build());
        Hierarchy nodeB = hierarchyService.create(Hierarchy.builder().name(prefix + "_B").systemSpec(SYSTEM).build());

        Variable variableA = getOrCreateVariable(prefix + "_my-hierarchy-variable-A");
        Variable variableB = getOrCreateVariable(prefix + "_my-hierarchy-variable-B");

        hierarchyService.addVariables(nodeA.getId(), Sets.newHashSet(variableA.getId(), variableB.getId()));
        hierarchyService.addVariables(nodeB.getId(), Collections.singleton(variableA.getId()));

        Set<VariableHierarchyIds> variableHierarchyIds = hierarchyService.getHierarchyIdsForVariables(
                Sets.newHashSet(variableA.getId(), variableB.getId()));

        assertThat(variableHierarchyIds).isNotEmpty();
        assertThat(variableHierarchyIds.size()).isEqualTo(2);

        Map<Long, Set<Long>> hierarchyIdsByVariableId = variableHierarchyIds.stream().collect(Collectors.toMap(
                VariableHierarchyIds::getVariableId, VariableHierarchyIds::getHierarchyIds));
        Set<Long> hierarchiesForVarA = hierarchyIdsByVariableId.get(variableA.getId());
        assertThat(hierarchiesForVarA).isNotEmpty();
        assertThat(hierarchiesForVarA.size()).isEqualTo(2);
        assertThat(hierarchiesForVarA).containsOnly(nodeA.getId(), nodeB.getId());

        Set<Long> hierarchiesForVarB = hierarchyIdsByVariableId.get(variableB.getId());
        assertThat(hierarchiesForVarB).isNotEmpty();
        assertThat(hierarchiesForVarB.size()).isEqualTo(1);
        assertThat(hierarchiesForVarB).containsOnly(nodeA.getId());
    }

    @Test
    public void shouldReturnEmptySetOnGetHierarchyIdsForVariablesWhenVariableDoesNotExist() {
        Set<VariableHierarchyIds> hierarchiesByVariableId = hierarchyService
                .getHierarchyIdsForVariables(Collections.singleton(1234L));
        assertThat(hierarchiesByVariableId).isEmpty();
    }

    @Test
    public void shouldReturnEmptySetOnGetHierarchyIdsForVariablesWhenVariableHasNoAssociationsWithHierarchy() {
        Variable v1 = getOrCreateVariable("my-hierarchy-variable-1");
        Variable v2 = getOrCreateVariable("my-hierarchy-variable-2");
        Set<VariableHierarchyIds> hierarchiesByVariableId = hierarchyService
                .getHierarchyIdsForVariables(Sets.newHashSet(v1.getId(), v2.getId(), 1234L));
        assertThat(hierarchiesByVariableId).isEmpty();
    }

    @Test
    public void shouldGetHierarchyIdsForOneVariable() {
        String prefix = getRandomName();
        Hierarchy nodeA = hierarchyService.create(Hierarchy.builder().name(prefix + "_A").systemSpec(SYSTEM).build());
        Hierarchy nodeB = hierarchyService.create(Hierarchy.builder().name(prefix + "_B").systemSpec(SYSTEM).build());

        Variable variableA = getOrCreateVariable(prefix + "_my-hierarchy-variable-A");
        Variable variableB = getOrCreateVariable(prefix + "_my-hierarchy-variable-B");

        hierarchyService.addVariables(nodeA.getId(), Sets.newHashSet(variableA.getId(), variableB.getId()));
        hierarchyService.addVariables(nodeB.getId(), Collections.singleton(variableA.getId()));

        Set<VariableHierarchyIds> variableHierarchyIds = hierarchyService.getHierarchyIdsForVariables(
                Sets.newHashSet(variableA.getId()));

        assertThat(variableHierarchyIds).isNotEmpty();
        assertThat(variableHierarchyIds.size()).isEqualTo(1);
        VariableHierarchyIds variableHierarchy = Iterables.getOnlyElement(variableHierarchyIds);
        assertEquals(variableA.getId(), variableHierarchy.getVariableId());
        Set<Long> hierarchies = variableHierarchy.getHierarchyIds();
        assertThat(hierarchies).isNotEmpty();
        assertThat(hierarchies.size()).isEqualTo(2);
        assertThat(hierarchies).containsOnly(nodeA.getId(), nodeB.getId());
    }

    @Test
    public void shouldGetHierarchyIdsForOver1kVariables() {
        int numOfVariables = 1010;
        Set<Long> variableIds = createVariables(numOfVariables, "hierarchy-variable-over1k");

        Hierarchy nodeA = hierarchyService.create(Hierarchy.builder()
                .name(getRandomName()).systemSpec(SYSTEM).build());

        hierarchyService.addVariables(nodeA.getId(), variableIds);

        Set<VariableHierarchyIds> variableHierarchyIds = hierarchyService.getHierarchyIdsForVariables(variableIds);
        assertThat(variableHierarchyIds).isNotEmpty();
        assertEquals(numOfVariables, variableHierarchyIds.size());
        variableHierarchyIds.forEach(e -> {
            assertTrue(variableIds.contains(e.getVariableId()));
            Set<Long> hierarchies = e.getHierarchyIds();
            assertThat(hierarchies).isNotEmpty();
            assertEquals(1, hierarchies.size());
            assertThat(hierarchies).containsOnly(nodeA.getId());
        });
    }

    @Test
    public void shouldCreateMoreThan1kHierarchies() {
        Set<Hierarchy> parentsToCreate = new HashSet<>();
        for (int i = 0; i < 10; i++) {
            parentsToCreate.add(Hierarchy.builder().name(getRandomName()).systemSpec(SYSTEM).build());
        }
        Set<Hierarchy> parents = hierarchyService.createAll(parentsToCreate);
        assertEquals(10, parents.size());

        Set<Hierarchy> hierarchiesToCreate = new HashSet<>();
        for (Hierarchy parent : parents) {
            for (int i = 0; i < 101; i++) {
                hierarchiesToCreate.add(Hierarchy.builder().name(getRandomName())
                        .parent(parent).systemSpec(SYSTEM).build());
            }
        }
        assertTrue(hierarchiesToCreate.size() > 1000);
        Set<Hierarchy> hierarchies = hierarchyService.createAll(hierarchiesToCreate);
        assertEquals(hierarchiesToCreate.size(), hierarchies.size());

        Set<Hierarchy> fetchedParents = getByIdIn(
                parents.stream().map(HierarchyView::getId).collect(Collectors.toSet()));
        for (Hierarchy parent : fetchedParents) {
            Hierarchy createdParent = getByPath(parent.getNodePath());
            assertNotNull(createdParent);
            assertThat(createdParent.isLeaf()).isFalse();
            assertThat(createdParent.getLevel()).isEqualTo(1);
            assertEquals(101, createdParent.getChildren().size());

            Map<Long, Hierarchy> fetchedHierarchies = getByIdIn(parent.getChildren().stream()
                    .map(HierarchyView::getId).collect(Collectors.toSet())).stream()
                    .collect(Collectors.toMap(Hierarchy::getId, Function.identity()));
            for (HierarchyView node : parent.getChildren()) {
                Hierarchy createdChild = fetchedHierarchies.get(node.getId());
                assertEquals(createdParent.getName(), createdChild.getParent().getName());
                assertNotNull(createdChild);
                assertThat(createdChild.isLeaf()).isTrue();
                assertThat(createdChild.getLevel()).isEqualTo(2);
            }
        }
    }

    @Test
    public void shouldUpdateMoreThan1kHierarchies() {
        int numOfElements = 1010;
        Set<Hierarchy> hierarchiesToCreate = new HashSet<>();

        String hierarchyNamePrefix = String.format("update1kHierarchies_%s_", getRandomName());
        for (int i = 0; i < numOfElements; i++) {
            hierarchiesToCreate.add(Hierarchy.builder().name(hierarchyNamePrefix + i).description("test-desc")
                    .systemSpec(SYSTEM).build());
        }
        Set<Hierarchy> createdNodes = hierarchyService.createAll(hierarchiesToCreate);
        assertEquals(numOfElements, createdNodes.size());

        Set<Hierarchy> nodesToUpdate = new HashSet<>();
        for (Hierarchy node : createdNodes) {
            nodesToUpdate.add(node.toBuilder().name(node.getName() + "_updated")
                    .description(node.getDescription() + "_updated").build());
        }
        Set<Hierarchy> updatedNodes = hierarchyService.updateAll(nodesToUpdate);
        assertNotNull(updatedNodes);
        assertEquals(numOfElements, updatedNodes.size());

        Map<Long, Hierarchy> fetchedHierarchies = getByIdIn(updatedNodes.stream().map(Hierarchy::getId)
                .collect(Collectors.toSet())).stream()
                .collect(Collectors.toMap(Hierarchy::getId, Function.identity()));
        for (Hierarchy updatedNode : updatedNodes) {
            Hierarchy fetched = fetchedHierarchies.get(updatedNode.getId());
            assertNotNull(fetched);
            assertEquals(updatedNode, fetched);
            assertTrue(fetched.getName().endsWith("_updated"));
            assertTrue(fetched.getDescription().endsWith("_updated"));
        }
    }

    @Test
    public void shouldDeleteMoreThan1kLeafHierarchies() {
        Hierarchy nodeA = hierarchyService
                .create(Hierarchy.builder().name(getRandomName()).systemSpec(SYSTEM).build());
        assertThat(hierarchyService.findById(nodeA.getId())).isPresent();

        Set<Hierarchy> hierarchiesToCreate = new HashSet<>();
        for (int i = 0; i < 200; i++) {
            hierarchiesToCreate.add(Hierarchy.builder().name(getRandomName()).systemSpec(SYSTEM).build());
        }
        for (int i = 0; i < 810; i++) {
            hierarchiesToCreate.add(Hierarchy.builder().name(getRandomName()).description("test-desc")
                    .parent(nodeA).systemSpec(SYSTEM).build());
        }

        Set<Hierarchy> createdNodes = hierarchyService.createAll(hierarchiesToCreate);
        assertNotNull(createdNodes);
        assertEquals(1010, createdNodes.size());

        hierarchyService.deleteAllLeaves(createdNodes);

        assertThat(hierarchyService.findById(nodeA.getId())).isPresent();
        Set<Hierarchy> fetchedHierarchies = getByIdIn(createdNodes.stream().map(Hierarchy::getId).collect(
                Collectors.toSet()));
        assertNotNull(fetchedHierarchies);
        assertTrue(fetchedHierarchies.isEmpty());
    }

    // helper methods

    private Variable getOrCreateVariable(String name) {
        return variableService
                .findOne(Variables.suchThat().systemName().eq(MOCK_SYSTEM_NAME).and().variableName().eq(name))
                .orElseGet(() -> variableService.create(getVariableData(name)));
    }

    private Entity getOrCreateEntity(int id) {
        return internalEntityService.findOrCreateEntityFor(SYSTEM.getId(),
                new KeyValues(id, ImmutableMap.of("device", getRandomName())),
                new KeyValues(10, PARTITION_KEY_VALUES),
                "my_schema", RECORD_TIMESTAMP);
    }

    private Hierarchy getByPath(String path) {
        return hierarchyService.findOne(Hierarchies.suchThat().path().eq(path))
                .orElseThrow(() -> new IllegalArgumentException("No such hierarchy path"));
    }

    private Set<Hierarchy> getByIdIn(Set<Long> ids) {
        return hierarchyService.findAll(Hierarchies.suchThat().id().in(ids));
    }
}
