/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.integrationtests.service;

import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.EntitySchema;
import cern.nxcals.api.domain.KeyValues;
import cern.nxcals.api.extraction.metadata.queries.EntitySchemas;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class EntitySchemaServiceTest extends AbstractTest {
    @Test
    public void shouldFindSchema() {
        KeyValues entityKeyValues = new KeyValues(ENTITY_KEY_VALUES_SCHEMA_TEST.get("device").hashCode(),
                ENTITY_KEY_VALUES_SCHEMA_TEST);
        KeyValues partitionKeyValues = new KeyValues(0, PARTITION_KEY_VALUES);
        Entity entityData = internalEntityService
                .findOrCreateEntityFor(SYSTEM.getId(), entityKeyValues, partitionKeyValues, SCHEMA, RECORD_TIMESTAMP);
        assertNotNull(entityData);
        EntitySchema schemaData =
                internalSchemaService.findOne(EntitySchemas.suchThat().id().eq(entityData.getEntityHistory().first().getEntitySchema().getId()))
                .orElseThrow(() -> new IllegalArgumentException("No such schema id"));
        assertNotNull(schemaData);

        assertEquals(SCHEMA, schemaData.getSchemaJson());
    }
}
