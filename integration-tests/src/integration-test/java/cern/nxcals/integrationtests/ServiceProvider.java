package cern.nxcals.integrationtests;

import cern.nxcals.api.extraction.metadata.EntityChangelogService;
import cern.nxcals.api.extraction.metadata.EntityService;
import cern.nxcals.api.extraction.metadata.HierarchyChangelogService;
import cern.nxcals.api.extraction.metadata.HierarchyService;
import cern.nxcals.api.extraction.metadata.HierarchyVariablesChangelogService;
import cern.nxcals.api.extraction.metadata.InternalServiceClientFactory;
import cern.nxcals.api.extraction.metadata.ServiceClientFactory;
import cern.nxcals.api.extraction.metadata.SystemSpecService;
import cern.nxcals.api.extraction.metadata.VariableChangelogService;
import cern.nxcals.api.extraction.metadata.VariableConfigChangelogService;
import cern.nxcals.api.extraction.metadata.VariableService;
import cern.nxcals.internal.extraction.metadata.InternalEntityResourceService;
import cern.nxcals.internal.extraction.metadata.InternalEntitySchemaService;
import cern.nxcals.internal.extraction.metadata.InternalEntityService;
import cern.nxcals.internal.extraction.metadata.InternalGroupService;
import cern.nxcals.internal.extraction.metadata.InternalPartitionService;
import cern.nxcals.internal.extraction.metadata.InternalSystemSpecService;

public class ServiceProvider {
    static {
        //Please leave this one just in case you want to run those test from IDE for debugging
        //        System.setProperty("service.url",
        //                "https://nxcals-<user>-1.cern.ch:19094".replace("<user>",
        //                        System.getProperty("user.name"))); //Do not use localhost here.
    }

    protected static final SystemSpecService systemService = ServiceClientFactory.createSystemSpecService();
    protected static final InternalSystemSpecService internalSystemService = InternalServiceClientFactory
            .createSystemSpecService();
    protected static final EntityService entityService = ServiceClientFactory.createEntityService();
    protected static final InternalEntityService internalEntityService = InternalServiceClientFactory
            .createEntityService();
    protected static final InternalPartitionService internalPartitionService = InternalServiceClientFactory
            .createPartitionService();
    protected static final VariableService variableService = ServiceClientFactory.createVariableService();
    protected static final InternalEntitySchemaService internalSchemaService = InternalServiceClientFactory
            .createEntitySchemaService();
    protected static final InternalEntityResourceService entityResourceService = InternalServiceClientFactory
            .createEntityResourceService();
    protected static final HierarchyService hierarchyService = ServiceClientFactory.createHierarchyService();
    protected static final HierarchyChangelogService hierarchyChangelogService = ServiceClientFactory
            .createHierarchyChangelogService();
    protected static final HierarchyVariablesChangelogService hierarchyVariablesChangelogService = ServiceClientFactory
            .createHierarchyVariablesChangelogService();
    protected static final VariableChangelogService variableChangelogService = ServiceClientFactory
            .createVariableChangelogService();
    protected static final VariableConfigChangelogService variableConfigChangelogService = ServiceClientFactory
            .createVariableConfigChangelogService();
    protected static final InternalGroupService internalGroupService = InternalServiceClientFactory.createGroupService();
    protected static final EntityChangelogService entityChangelogService = ServiceClientFactory.createEntityChangelogService();
}
