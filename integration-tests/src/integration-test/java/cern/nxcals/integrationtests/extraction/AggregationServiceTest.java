package cern.nxcals.integrationtests.extraction;

import cern.cmw.datax.ImmutableData;
import cern.nxcals.api.config.SparkContext;
import cern.nxcals.api.custom.domain.FundamentalFilter;
import cern.nxcals.api.custom.service.AggregationService;
import cern.nxcals.api.custom.service.Services;
import cern.nxcals.api.custom.service.aggregation.AggregationFunctions;
import cern.nxcals.api.custom.service.aggregation.DatasetAggregationProperties;
import cern.nxcals.api.custom.service.aggregation.WindowAggregationProperties;
import cern.nxcals.api.custom.service.fundamental.FundamentalContext;
import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.domain.VariableConfig;
import cern.nxcals.api.domain.VariableDeclaredType;
import cern.nxcals.api.extraction.data.builders.DataQuery;
import cern.nxcals.api.extraction.metadata.EntityService;
import cern.nxcals.api.extraction.metadata.ServiceClientFactory;
import cern.nxcals.api.extraction.metadata.SystemSpecService;
import cern.nxcals.api.extraction.metadata.VariableService;
import cern.nxcals.api.extraction.metadata.queries.Entities;
import cern.nxcals.api.extraction.metadata.queries.Variables;
import cern.nxcals.api.utils.TimeUtils;
import cern.nxcals.integrationtests.testutils.ExtractionTestHelper;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSortedSet;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.DependsOn;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import static cern.nxcals.api.backport.domain.core.metadata.FundamentalData.XTIM_DESTINATION_FIELD;
import static cern.nxcals.api.backport.domain.core.metadata.FundamentalData.XTIM_LSA_CYCLE_FIELD;
import static cern.nxcals.api.custom.domain.CmwSystemConstants.DEVICE_KEY_NAME;
import static cern.nxcals.api.custom.domain.CmwSystemConstants.PROPERTY_KEY_NAME;
import static cern.nxcals.api.custom.service.aggregation.AggregationConstants.AGGREGATION_TIMESTAMP_FIELD;
import static cern.nxcals.api.custom.service.aggregation.AggregationConstants.AGGREGATION_VALUE_FIELD;
import static cern.nxcals.api.custom.service.fundamental.FundamentalContext.USER_FIELD;
import static cern.nxcals.integrationtests.extraction.FundamentalServiceTest.FUNDAMENTAL_SYSTEM_SPEC;
import static cern.nxcals.integrationtests.testutils.ExtractionTestHelper.CLASS_KEY;
import static cern.nxcals.integrationtests.testutils.ExtractionTestHelper.DEVICE_KEY;
import static cern.nxcals.integrationtests.testutils.ExtractionTestHelper.PROPERTY_KEY;
import static cern.nxcals.integrationtests.testutils.ExtractionTestHelper.RECORD_TIMESTAMP_KEY;
import static cern.nxcals.integrationtests.testutils.ExtractionTestHelper.RECORD_VERSION_KEY;
import static cern.nxcals.integrationtests.testutils.ExtractionTestHelper.SYSTEM;
import static java.lang.String.format;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;

/**
 * Integration tests for {@link AggregationService}
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = SparkContext.class)
@SpringBootApplication
@Slf4j
@DependsOn("kerberos")
public class AggregationServiceTest {
    static {
        //        Please leave this one just in case you want to run those test from IDE for debugging
        //        Don't forget to check if hadoop-dev jar is included in build.gradle!!!

        //        String username = System.getProperty("user.name");
        //        System.setProperty("kerberos.principal", username);
        //        System.setProperty("kerberos.keytab", "/opt/<user>/.keytab".replace("<user>", username));
        //        System.setProperty("service.url",
        //                "https://nxcals-<user>1.cern.ch:19093".replace("<user>", username)); //Do not use localhost here.
        //        System.setProperty("kafka.producer.bootstrap.servers",
        //                "https://nxcals-<user>3.cern.ch:9092,https://nxcals-<user>4.cern.ch:9092,https://nxcals-<user>5.cern.ch:9092"
        //                        .replace("<user>", username));
    }

    private static final VariableService VARIABLE_SERVICE = ServiceClientFactory.createVariableService();
    private static final EntityService ENTITY_SERVICE = ServiceClientFactory.createEntityService();
    private static final SystemSpecService SYSTEM_SERVICE = ServiceClientFactory.createSystemSpecService();

    private static final SystemSpec SYSTEM_SPEC = SYSTEM_SERVICE.findByName(SYSTEM)
            .orElseThrow(() -> new IllegalArgumentException("No such system"));

    private static final int MAX_WAIT_FOR_DATA_MINUTES = 2;
    private static final AtomicBoolean DATA_VISIBLE = new AtomicBoolean(false);
    private static final AtomicBoolean WAITING_FOR_DATA_FAILED = new AtomicBoolean(false);

    private static final String RANDOM_SUFFIX = UUID.randomUUID().toString();

    private static final String AGGREGATION_DEVICE_NAME = "WINDOW.AGGREGATION.DEVICE_" + RANDOM_SUFFIX;
    private static final String AGGREGATION_PROPERTY_NAME = "AGGREGATION_PROPERTY";
    private static final String AGGREGATION_CLASS_NAME = "TEST_CLASS";

    private static final Map<String, Object> AGGREGATION_ENTITY_KEYS = ImmutableMap
            .of(DEVICE_KEY_NAME, AGGREGATION_DEVICE_NAME, PROPERTY_KEY_NAME, AGGREGATION_PROPERTY_NAME);

    private static final String AGGREGATION_FIELD_NAME = "DATA_FIELD";
    private static final String AGGREGATION_VARIABLE_NAME = "WINDOW:AGGREGATION:VARIABLE_" + RANDOM_SUFFIX;

    private static final Instant DATA_PRODUCE_START = Instant.now();
    private static final Instant DATA_PRODUCE_END = DATA_PRODUCE_START.plus(4, ChronoUnit.MINUTES);
    private static final Duration DATA_PRODUCE_DURATION = Duration.between(DATA_PRODUCE_START, DATA_PRODUCE_END);

    private static final List<Integer> INTERVAL_POINT_VALUES = Arrays.asList(0, 1, 2, 3, 4, 5);

    private static final long DATA_PRODUCE_STEP_SECONDS =
            TimeUnit.SECONDS.convert(1, TimeUnit.MINUTES) / INTERVAL_POINT_VALUES.size();
    private static final long TOTAL_NUM_OF_PRODUCED_DATA =
            DATA_PRODUCE_DURATION.toMinutes() * INTERVAL_POINT_VALUES.size() * 2;

    private static final Instant AGGREGATION_QUERY_START = DATA_PRODUCE_START;
    private static final Instant AGGREGATION_QUERY_END = AGGREGATION_QUERY_START.plus(2, ChronoUnit.MINUTES);

    private static final String TIMESTAMP_FIELD = "timestamp";

    private static final String CYCLE_PREFIX = "TEST_CYCLE_";
    private static final String USER_PREFIX = "TEST_USER_";
    private static final String TEST_DESTINATION = "TEST_DESTINATION";

    private static final String FUNDAMENTAL_DEVICE_NAME = "FUNDAMENTAL.DEVICE_" + RANDOM_SUFFIX;
    private static final String FUNDAMENTAL_PROPERTY_NAME = "FUNDAMENTAL_PROPERTY";
    private static final String FUNDAMENTAL_CLASS_NAME = "TEST_CLASS";
    private static final Map<String, Object> FUNDAMENTAL_ENTITY_KEYS = ImmutableMap
            .of(DEVICE_KEY_NAME, FUNDAMENTAL_DEVICE_NAME, PROPERTY_KEY_NAME, FUNDAMENTAL_PROPERTY_NAME);
    private static final String ACCELERATOR = "ACCELERATOR_" + RANDOM_SUFFIX;
    private static final String FUNDAMENTAL_VARIABLE_NAME =
            ACCELERATOR.toUpperCase() + FundamentalContext.VARIABLE_NAME_SUFFIX;

    @Autowired
    private SparkSession sparkSession;
    private AggregationService aggregationService;

    @BeforeAll
    public static void setUpData() {
        List<ImmutableData> aggregationData = generateAggregationData();
        log.info("Sending fills and beam mode data to NXCALS");
        ExtractionTestHelper.publishData(SYSTEM_SPEC.getName(), aggregationData);
        registerVariable();

        List<ImmutableData> fundamentalData = generateFundamentalData();
        log.info("Sending fundamentals data to NXCALS");
        ExtractionTestHelper.publishData(FUNDAMENTAL_SYSTEM_SPEC.getName(), fundamentalData);

        FundamentalServiceTest.registerFundamentalVariable(FUNDAMENTAL_VARIABLE_NAME, FUNDAMENTAL_ENTITY_KEYS);
    }

    private static List<ImmutableData> generateFundamentalData() {
        List<ImmutableData> dataPoints = new ArrayList<>();
        int value = 0;
        int minute = 0;
        for (long i = 0; i < DATA_PRODUCE_DURATION.getSeconds(); i++) {
            if (i % 60 == 0) { // reset the values every minute
                value = 0;
                minute++; // increase the value every minute, starting from 1 for the first record
            }
            if (i % DATA_PRODUCE_STEP_SECONDS == 0) { // produce data based on specified frequency
                ImmutableData dataOfPoint = buildFundamentalData(DATA_PRODUCE_START.plusSeconds(i), minute, value);
                dataPoints.add(dataOfPoint);
                value++;
            }
        }
        return dataPoints;
    }

    // use "minute" as cycle name suffix, use "value" as user name suffix
    private static ImmutableData buildFundamentalData(Instant timestamp, int minute, int value) {
        String cycle = CYCLE_PREFIX + minute;
        String user = USER_PREFIX + value;
        return ImmutableData.builder()
                .add(DEVICE_KEY, FUNDAMENTAL_DEVICE_NAME)
                .add(PROPERTY_KEY, FUNDAMENTAL_PROPERTY_NAME)
                .add(CLASS_KEY, FUNDAMENTAL_CLASS_NAME)
                .add(RECORD_VERSION_KEY, 0L)
                .add(RECORD_TIMESTAMP_KEY, TimeUtils.getNanosFromInstant(timestamp))
                .add(XTIM_LSA_CYCLE_FIELD, cycle)
                .add(USER_FIELD, user)
                .add(XTIM_DESTINATION_FIELD, TEST_DESTINATION)
                .build();
    }

    private static void registerVariable() {
        VARIABLE_SERVICE.findOne(Variables.suchThat().systemName().eq(SYSTEM_SPEC.getName()).and()
                .variableName().eq(AGGREGATION_VARIABLE_NAME)).ifPresent(v -> {
            throw new IllegalArgumentException("Variable with this name already exists");
        });
        Variable variable = VARIABLE_SERVICE.create(buildVariable(findEntityOrThrow(AGGREGATION_ENTITY_KEYS),
                AGGREGATION_VARIABLE_NAME, AGGREGATION_FIELD_NAME, VariableDeclaredType.NUMERIC));
        assertNotNull(variable);
    }

    // example of generated aggregation test data
    //+-----------+----------+--------------------+------------------+----------+--------------------+--------------------+----------------+
    //            |DATA_FIELD|__record_timestamp__|__record_version__|     class|              device|            property|nxcals_entity_id|
    //            +----------+--------------------+------------------+----------+--------------------+--------------------+----------------+
    //            |         0| 1652111917321325000|                 0|TEST_CLASS|WINDOW.AGGREGATIO...|AGGREGATION_PROPERTY|          105070|
    //            |         1| 1652111927321325000|                 0|TEST_CLASS|WINDOW.AGGREGATIO...|AGGREGATION_PROPERTY|          105070|
    //            |         2| 1652111937321325000|                 0|TEST_CLASS|WINDOW.AGGREGATIO...|AGGREGATION_PROPERTY|          105070|
    //            |         3| 1652111947321325000|                 0|TEST_CLASS|WINDOW.AGGREGATIO...|AGGREGATION_PROPERTY|          105070|
    //            |         4| 1652111957321325000|                 0|TEST_CLASS|WINDOW.AGGREGATIO...|AGGREGATION_PROPERTY|          105070|
    //            |         5| 1652111967321325000|                 0|TEST_CLASS|WINDOW.AGGREGATIO...|AGGREGATION_PROPERTY|          105070|
    //            |         0| 1652111977321325000|                 0|TEST_CLASS|WINDOW.AGGREGATIO...|AGGREGATION_PROPERTY|          105070|
    //            |         1| 1652111987321325000|                 0|TEST_CLASS|WINDOW.AGGREGATIO...|AGGREGATION_PROPERTY|          105070|
    //            |         2| 1652111997321325000|                 0|TEST_CLASS|WINDOW.AGGREGATIO...|AGGREGATION_PROPERTY|          105070|
    //            |         3| 1652112007321325000|                 0|TEST_CLASS|WINDOW.AGGREGATIO...|AGGREGATION_PROPERTY|          105070|
    //            |         4| 1652112017321325000|                 0|TEST_CLASS|WINDOW.AGGREGATIO...|AGGREGATION_PROPERTY|          105070|
    //            |         5| 1652112027321325000|                 0|TEST_CLASS|WINDOW.AGGREGATIO...|AGGREGATION_PROPERTY|          105070|
    //            |         0| 1652112037321325000|                 0|TEST_CLASS|WINDOW.AGGREGATIO...|AGGREGATION_PROPERTY|          105070|
    //            |         1| 1652112047321325000|                 0|TEST_CLASS|WINDOW.AGGREGATIO...|AGGREGATION_PROPERTY|          105070|
    //            |         2| 1652112057321325000|                 0|TEST_CLASS|WINDOW.AGGREGATIO...|AGGREGATION_PROPERTY|          105070|
    //            |         3| 1652112067321325000|                 0|TEST_CLASS|WINDOW.AGGREGATIO...|AGGREGATION_PROPERTY|          105070|
    //            |         4| 1652112077321325000|                 0|TEST_CLASS|WINDOW.AGGREGATIO...|AGGREGATION_PROPERTY|          105070|
    //            |         5| 1652112087321325000|                 0|TEST_CLASS|WINDOW.AGGREGATIO...|AGGREGATION_PROPERTY|          105070|
    //            |         0| 1652112097321325000|                 0|TEST_CLASS|WINDOW.AGGREGATIO...|AGGREGATION_PROPERTY|          105070|
    //            |         1| 1652112107321325000|                 0|TEST_CLASS|WINDOW.AGGREGATIO...|AGGREGATION_PROPERTY|          105070|
    //            |         2| 1652112117321325000|                 0|TEST_CLASS|WINDOW.AGGREGATIO...|AGGREGATION_PROPERTY|          105070|
    //            |         3| 1652112127321325000|                 0|TEST_CLASS|WINDOW.AGGREGATIO...|AGGREGATION_PROPERTY|          105070|
    //            |         4| 1652112137321325000|                 0|TEST_CLASS|WINDOW.AGGREGATIO...|AGGREGATION_PROPERTY|          105070|
    //            |         5| 1652112147321325000|                 0|TEST_CLASS|WINDOW.AGGREGATIO...|AGGREGATION_PROPERTY|          105070|
    //            +----------+--------------------+------------------+----------+--------------------+--------------------+----------------+
    private static List<ImmutableData> generateAggregationData() {
        List<ImmutableData> intervalDataPoints = new ArrayList<>();
        int value = 0;
        for (long i = 0; i < DATA_PRODUCE_DURATION.getSeconds(); i++) {
            if (i % 60 == 0) { // reset the values every minute
                value = 0;
            }
            if (i % DATA_PRODUCE_STEP_SECONDS == 0) { // produce data based on specified frequency
                ImmutableData dataOfPoint = buildData(DATA_PRODUCE_START.plusSeconds(i), value);
                intervalDataPoints.add(dataOfPoint);
                value++;
            }
        }
        return intervalDataPoints;
    }

    private static ImmutableData buildData(Instant timestamp, int value) {
        return ImmutableData.builder()
                .add(DEVICE_KEY, AGGREGATION_DEVICE_NAME)
                .add(PROPERTY_KEY, AGGREGATION_PROPERTY_NAME)
                .add(CLASS_KEY, AGGREGATION_CLASS_NAME)
                .add(RECORD_VERSION_KEY, 0L)
                .add(RECORD_TIMESTAMP_KEY, TimeUtils.getNanosFromInstant(timestamp))
                .add(AGGREGATION_FIELD_NAME, value)
                .build();
    }

    private static Entity findEntityOrThrow(Map<String, Object> keyValues) {
        return ENTITY_SERVICE.findOne(Entities.suchThat().keyValues().eq(SYSTEM_SPEC, keyValues)).orElseThrow(
                () -> new IllegalArgumentException(
                        format("Could not find entity [ %s ]! Make sure that data were sent and metadata created for this target",
                                keyValues.get(DEVICE_KEY_NAME))));
    }

    private static Variable buildVariable(Entity entity, String variableName, String fieldName,
            VariableDeclaredType type) {
        VariableConfig config = VariableConfig.builder()
                .entityId(entity.getId())
                .variableName(variableName)
                .fieldName(fieldName).build();
        return Variable.builder().systemSpec(SYSTEM_SPEC).variableName(variableName)
                .declaredType(type).configs(ImmutableSortedSet.of(config)).build();
    }

    @BeforeEach
    public void waitForData() throws InterruptedException {
        this.aggregationService = Services.newInstance(sparkSession).aggregationService();

        if (!DATA_VISIBLE.get()) {
            //Have to wait 30 seconds in order for the data to be processed and visible in NXCALS extraction
            log.info("Waiting max {} minutes for the data to be visible in NXCALS...", MAX_WAIT_FOR_DATA_MINUTES);
            waitForData(TimeUnit.MINUTES.toMillis(MAX_WAIT_FOR_DATA_MINUTES));
            log.info("Ok, data should be processed now, proceeding with tests");
            DATA_VISIBLE.set(true);
        } else {
            log.info("Data already checked");
        }
    }

    private synchronized void waitForData(long maxTime) throws InterruptedException {
        long start = System.currentTimeMillis();
        while (true) {
            if (WAITING_FOR_DATA_FAILED.get() || (System.currentTimeMillis() - start > maxTime)) {
                WAITING_FOR_DATA_FAILED.set(true);
                fail("Data was not processed in the required maxTime=" + maxTime + " ms");
            }
            if (getTotalDataCount() == TOTAL_NUM_OF_PRODUCED_DATA) {
                return;
            }
            log.info("Still no data visible, waiting more...");
            TimeUnit.SECONDS.sleep(4);
        }
    }

    private long getTotalDataCount() {
        return getDataCount(SYSTEM_SPEC.getName(), AGGREGATION_DEVICE_NAME, AGGREGATION_PROPERTY_NAME) +
                getDataCount(FUNDAMENTAL_SYSTEM_SPEC.getName(), FUNDAMENTAL_DEVICE_NAME, FUNDAMENTAL_PROPERTY_NAME);
    }

    private long getDataCount(String systemName, String deviceName, String propertyName) {
        return DataQuery.builder(sparkSession).byEntities().system(systemName)
                .startTime(DATA_PRODUCE_START)
                .endTime(DATA_PRODUCE_END)
                .entity()
                .keyValue(DEVICE_KEY, deviceName)
                .keyValue(PROPERTY_KEY, propertyName)
                .build().orderBy("__record_timestamp__").count();
    }

    // test cases

    @Test
    public void shouldGetAggregatedDataForVariable() {
        Variable variable = VARIABLE_SERVICE.findOne(Variables.suchThat().systemName().eq(SYSTEM_SPEC.getName()).and()
                .variableName().eq(AGGREGATION_VARIABLE_NAME))
                .orElseThrow(() -> new IllegalStateException("Could not find variable!"));

        WindowAggregationProperties properties = WindowAggregationProperties.builder()
                .timeWindow(AGGREGATION_QUERY_START, AGGREGATION_QUERY_END)
                .interval(40, ChronoUnit.SECONDS)
                .function(AggregationFunctions.MAX)
                .build();

        Dataset<Row> result = aggregationService.getData(variable, properties);
        assertResult(result, Arrays.asList(3, 5, 5), AGGREGATION_QUERY_START,
                Duration.ofMillis(properties.getIntervalMillis()));
    }

    @Test
    public void shouldGetRepeatedDataForVariable() {
        Variable variable = VARIABLE_SERVICE.findOne(Variables.suchThat().systemName().eq(SYSTEM_SPEC.getName()).and()
                .variableName().eq(AGGREGATION_VARIABLE_NAME))
                .orElseThrow(() -> new IllegalStateException("Could not find variable!"));

        WindowAggregationProperties properties = WindowAggregationProperties.builder()
                .timeWindow(AGGREGATION_QUERY_START, AGGREGATION_QUERY_END)
                .interval(35, ChronoUnit.SECONDS)
                .function(AggregationFunctions.REPEAT)
                .build();

        Dataset<Row> result = aggregationService.getData(variable, properties);
        assertResult(result, Arrays.asList(0, 3, 1, 4), AGGREGATION_QUERY_START,
                Duration.ofMillis(properties.getIntervalMillis()));
    }

    @Test
    public void shouldGetRepeatedDataForVariableWithIntervalSmallerThanDataProductionRate() {
        Variable variable = VARIABLE_SERVICE.findOne(Variables.suchThat().systemName().eq(SYSTEM_SPEC.getName()).and()
                .variableName().eq(AGGREGATION_VARIABLE_NAME))
                .orElseThrow(() -> new IllegalStateException("Could not find variable!"));

        WindowAggregationProperties properties = WindowAggregationProperties.builder()
                .timeWindow(AGGREGATION_QUERY_START, AGGREGATION_QUERY_START.plusSeconds(11))
                .interval(1, ChronoUnit.SECONDS)
                .function(AggregationFunctions.REPEAT)
                .build();

        Dataset<Row> result = aggregationService.getData(variable, properties);
        //first element (0) is repeated until we reach [t=( t0 + 10s)] where the value (1) was sent
        assertResult(result, Arrays.asList(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1), AGGREGATION_QUERY_START,
                Duration.ofMillis(properties.getIntervalMillis()));
    }

    @Test
    public void shouldGetInterpolatedDataForEntity() {
        Entity entity = findEntityOrThrow(AGGREGATION_ENTITY_KEYS);

        Instant queryStart = AGGREGATION_QUERY_START.plusSeconds(5);
        WindowAggregationProperties properties = WindowAggregationProperties.builder()
                .timeWindow(queryStart, AGGREGATION_QUERY_END.plusSeconds(5))
                .interval(1, ChronoUnit.MINUTES)
                .function(AggregationFunctions.INTERPOLATE.expandTimeWindowBy(1, 1, ChronoUnit.MONTHS))
                .aggregationField(AGGREGATION_FIELD_NAME)
                .build();

        Dataset<Row> result = aggregationService.getData(entity, properties);
        double expectedEntry1 = calculateInterpolation(queryStart, AGGREGATION_QUERY_START,
                0, AGGREGATION_QUERY_START.plusSeconds(10), 1);

        Instant nextExpectedEntryTime = queryStart.plusSeconds(properties.getIntervalMillis());
        Instant previousValueTime = AGGREGATION_QUERY_START.plusSeconds(properties.getIntervalMillis());
        double expectedEntry2 = calculateInterpolation(nextExpectedEntryTime, previousValueTime, 0,
                previousValueTime.plusSeconds(10), 1);
        assertResult(result, Arrays.asList(expectedEntry1, expectedEntry2), queryStart,
                Duration.ofMillis(properties.getIntervalMillis()));
    }

    @Test
    public void shouldGetDataForVariableAlignedOnDataset() {
        Variable variable = VARIABLE_SERVICE.findOne(Variables.suchThat().systemName().eq(SYSTEM_SPEC.getName()).and()
                .variableName().eq(AGGREGATION_VARIABLE_NAME))
                .orElseThrow(() -> new IllegalStateException("Could not find variable!"));

        Dataset<Row> drivingDataset = getDrivingDataset();
        DatasetAggregationProperties properties = DatasetAggregationProperties.builder()
                .drivingDataset(drivingDataset)
                .timestampField(TIMESTAMP_FIELD)
                .build();

        Dataset<Row> result = aggregationService.getData(variable, properties);
        assertResult(result, Arrays.asList(0, 3, 0, 3), drivingDataset, properties.getTimestampField());
    }

    @Test
    public void shouldGetDataForEntityAlignedOnDataset() {
        Entity entity = findEntityOrThrow(AGGREGATION_ENTITY_KEYS);

        Dataset<Row> drivingDataset = getDrivingDataset();
        DatasetAggregationProperties properties = DatasetAggregationProperties.builder()
                .drivingDataset(drivingDataset)
                .timestampField(TIMESTAMP_FIELD)
                .aggregationField(AGGREGATION_FIELD_NAME)
                .build();

        Dataset<Row> result = aggregationService.getData(entity, properties);
        assertResult(result, Arrays.asList(0, 3, 0, 3), drivingDataset, properties.getTimestampField());
    }

    @Test
    public void shouldGetDataForVariableFilteredByFundamentals() {
        Variable variable = VARIABLE_SERVICE.findOne(Variables.suchThat().systemName().eq(SYSTEM_SPEC.getName()).and()
                .variableName().eq(AGGREGATION_VARIABLE_NAME))
                .orElseThrow(() -> new IllegalStateException("Could not find variable!"));

        FundamentalFilter fundamentalFilter1 = FundamentalFilter.builder().accelerator(ACCELERATOR)
                .lsaCycle(CYCLE_PREFIX + 1).timingUser(USER_PREFIX + 4).build(); // minute 1, value 4
        FundamentalFilter fundamentalFilter2 = FundamentalFilter.builder().accelerator(ACCELERATOR)
                .lsaCycle(CYCLE_PREFIX + 2).timingUser(USER_PREFIX + 1).build(); // minute 2, value 1

        WindowAggregationProperties properties = WindowAggregationProperties.builder()
                .timeWindow(AGGREGATION_QUERY_START, AGGREGATION_QUERY_END)
                .interval(30, ChronoUnit.SECONDS)
                .function(AggregationFunctions.MAX)
                .fundamentalFilter(fundamentalFilter1)
                .fundamentalFilter(fundamentalFilter2)
                .build();

        Dataset<Row> result = aggregationService.getData(variable, properties);

        assertResult(result, Arrays.asList(null, 4, 1, null), AGGREGATION_QUERY_START,
                Duration.ofMillis(properties.getIntervalMillis()));
    }

    @Test
    public void shouldGetDataForEntityFilteredByFundamentals() {
        Entity entity = findEntityOrThrow(AGGREGATION_ENTITY_KEYS);

        FundamentalFilter fundamentalFilter1 = FundamentalFilter.builder().accelerator(ACCELERATOR)
                .lsaCycle(CYCLE_PREFIX + 1).timingUser(USER_PREFIX + "%").destination(TEST_DESTINATION)
                .build(); // minute 1, all values
        FundamentalFilter fundamentalFilter2 = FundamentalFilter.builder().accelerator(ACCELERATOR)
                .lsaCycle(CYCLE_PREFIX + 2).timingUser(USER_PREFIX + 1).build(); // minute 2, value 1
        Set<FundamentalFilter> filters = Set.of(fundamentalFilter1, fundamentalFilter2);

        WindowAggregationProperties properties = WindowAggregationProperties.builder()
                .timeWindow(AGGREGATION_QUERY_START, AGGREGATION_QUERY_END)
                .interval(30, ChronoUnit.SECONDS)
                .function(AggregationFunctions.AVG)
                .fundamentalFilters(filters)
                .aggregationField(AGGREGATION_FIELD_NAME)
                .build();

        Dataset<Row> result = aggregationService.getData(entity, properties);

        assertResult(result, Arrays.asList(1.0, 4.0, 1.0, null), AGGREGATION_QUERY_START,
                Duration.ofMillis(properties.getIntervalMillis()));
    }

    // helper methods

    private double calculateInterpolation(Instant generatedPointTime, Instant prevPointTime,
            long prevPointValue, Instant nextPointTime, long nextPointValue) {
        long generatedPointTs = TimeUtils.getNanosFromInstant(generatedPointTime.truncatedTo(ChronoUnit.SECONDS));
        long prevPointTs = TimeUtils.getNanosFromInstant(prevPointTime.truncatedTo(ChronoUnit.SECONDS));
        long nextPointTs = TimeUtils.getNanosFromInstant(nextPointTime.truncatedTo(ChronoUnit.SECONDS));

        double coefficient = (double) (nextPointValue - prevPointValue) / (nextPointTs - prevPointTs);
        return prevPointValue + (coefficient * (generatedPointTs - prevPointTs));
    }

    private <T> void assertResult(Dataset<Row> result, List<T> expectedResults, Instant queryStart,
            Duration intervalDuration) {
        assertNotNull(result);
        List<Pair<Instant, T>> collectedResult = collect(result);
        assertEquals(expectedResults.size(), collectedResult.size());
        for (int i = 0; i < expectedResults.size(); i++) {
            Pair<Instant, T> row = collectedResult.get(i);
            assertNotNull(row.getLeft());
            Instant expectedIntervalStart = queryStart.plus(intervalDuration.multipliedBy(i));
            assertEquals(expectedIntervalStart, row.getLeft());
            assertEquals(expectedResults.get(i), row.getRight());
        }

    }

    private <T> void assertResult(Dataset<Row> result, List<T> expectedResults, Dataset<Row> drivingDataset,
            String timestampField) {
        assertNotNull(result);

        List<Pair<Instant, Long>> collectedResult = collect(result);
        List<Instant> drivingDatasetRows = collectTimestamps(drivingDataset, timestampField);
        assertEquals(drivingDatasetRows.size(), collectedResult.size());
        Iterator<T> expectedResultsIterator = expectedResults.iterator();
        for (int i = 0; i < expectedResults.size(); i++) {
            Pair<Instant, Long> row = collectedResult.get(i);
            if (row.getRight() == null) {
                continue;
            }
            assertNotNull(row.getLeft());
            assertEquals(drivingDatasetRows.get(i), row.getLeft());
            assertEquals(expectedResultsIterator.next(), row.getRight());
        }

    }

    private List<Instant> collectTimestamps(Dataset<Row> dataset, String timestampField) {
        List<Instant> timestamps = new ArrayList<>();
        dataset.select(timestampField).orderBy(timestampField).collectAsList().forEach(row ->
                timestamps.add(TimeUtils.getInstantFromNanos(row.getLong(0))));
        return timestamps;
    }

    private Dataset<Row> getDrivingDataset() {
        Variable variable = VARIABLE_SERVICE.findOne(Variables.suchThat().systemName().eq(SYSTEM_SPEC.getName()).and()
                .variableName().eq(AGGREGATION_VARIABLE_NAME))
                .orElseThrow(() -> new IllegalStateException("Could not find variable!"));

        WindowAggregationProperties properties = WindowAggregationProperties.builder()
                .timeWindow(AGGREGATION_QUERY_START, AGGREGATION_QUERY_START.plus(2, ChronoUnit.MINUTES))
                .interval(30, ChronoUnit.SECONDS)
                .function(AggregationFunctions.REPEAT)
                .build();

        return aggregationService.getData(variable, properties);
    }

    @SuppressWarnings("unchecked")
    private <T> List<Pair<Instant, T>> collect(Dataset<Row> dataset) {
        List<Pair<Instant, T>> datasetRows = new ArrayList<>();
        dataset.orderBy(AGGREGATION_TIMESTAMP_FIELD).collectAsList().forEach(r -> {
            int timestampIndex = r.fieldIndex(AGGREGATION_TIMESTAMP_FIELD);
            int valueIndex = r.fieldIndex(AGGREGATION_VALUE_FIELD);
            datasetRows.add(ImmutablePair.of(TimeUtils.getInstantFromNanos(r.getLong(timestampIndex)),
                    r.isNullAt(valueIndex) ? null : (T) r.getAs(valueIndex)));
        });
        return datasetRows;
    }

}
