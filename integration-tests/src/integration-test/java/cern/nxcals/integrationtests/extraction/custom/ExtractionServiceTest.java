package cern.nxcals.integrationtests.extraction.custom;

import cern.cmw.datax.ImmutableData;
import cern.nxcals.api.config.SparkContext;
import cern.nxcals.api.custom.service.ExtractionService;
import cern.nxcals.api.custom.service.Services;
import cern.nxcals.api.custom.service.extraction.ExtractionProperties;
import cern.nxcals.api.custom.service.extraction.LookupStrategy;
import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.domain.VariableDeclaredType;
import cern.nxcals.api.utils.TimeUtils;
import cern.nxcals.common.utils.Lazy;
import cern.nxcals.integrationtests.utils.CMWEntityDataGenerator;
import cern.nxcals.integrationtests.utils.HBaseDataInitializer;
import lombok.extern.slf4j.Slf4j;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.DependsOn;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.function.BiFunction;
import java.util.stream.Stream;

import static cern.nxcals.common.SystemFields.NXC_EXTR_TIMESTAMP;
import static cern.nxcals.integrationtests.service.AbstractTest.getRandomName;
import static cern.nxcals.integrationtests.testutils.ExtractionTestHelper.RECORD_TIMESTAMP_KEY;
import static cern.nxcals.integrationtests.utils.ServicesHelper.deleteVariable;
import static cern.nxcals.integrationtests.utils.ServicesHelper.registerSimpleVariable;
import static org.apache.spark.sql.functions.min;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Integration tests for {@link ExtractionService}
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = SparkContext.class)
@SpringBootApplication
@Slf4j
@DependsOn("kerberos")
public class ExtractionServiceTest {
    //    static {
    //        String username = System.getProperty("user.name");
    //        System.setProperty("kerberos.principal", username);
    //        System.setProperty("kerberos.keytab", "/opt/<user>/.keytab".replace("<user>", username));
    //        System.setProperty("service.url",
    //                "https://nxcals-<user>1.cern.ch:19093".replace("<user>", username)); //Do not use localhost here.
    //        System.setProperty("kafka.producer.bootstrap.servers",
    //                "https://nxcals-<user>3.cern.ch:9092,https://nxcals-<user>4.cern.ch:9092,https://nxcals-<user>5.cern.ch:9092".replace(
    //                        "<user>", username));
    //    }

    private static final String FIELD_NAME = "FIELD";

    private static final CMWEntityDataGenerator generator = new CMWEntityDataGenerator(
            datapointNumber -> ImmutableData.builder().add(FIELD_NAME, datapointNumber).build()
    );
    private static final Instant dataProduceStart = generator.getDataProduceStart();
    private static final Instant dataProduceEnd = generator.getDataProduceEnd();
    private static final long QUERIED_TIME_WINDOW_DURATION_IN_MINUTES = 2;
    private static final Instant queryStart = dataProduceStart.plus(1, ChronoUnit.MINUTES);
    private static final Instant queryEnd = queryStart.plus(QUERIED_TIME_WINDOW_DURATION_IN_MINUTES, ChronoUnit.MINUTES)
            .minusNanos(1);
    private static final long DATA_PRODUCE_INTERVAL_SECONDS = generator.getDataProduceIntervalSeconds();

    private static final HBaseDataInitializer dataInitializer = new HBaseDataInitializer(generator);

    @Autowired
    private SparkSession sparkSession;

    Lazy<ExtractionService> extractionService = new Lazy<>(
            () -> Services.newInstance(sparkSession).extractionService());

    interface DataProducer extends BiFunction<ExtractionService, ExtractionProperties, Dataset<Row>> {
    }

    static Dataset<Row> searchByVariable(ExtractionService extractionService, ExtractionProperties properties) {
        Variable variable = registerSimpleVariable(getRandomName("VAR_"), generator.getCreatedEntity(), FIELD_NAME,
                VariableDeclaredType.NUMERIC);
        try {
            return extractionService.getData(variable, properties);
        } finally {
            deleteVariable(variable);
        }
    }

    static Dataset<Row> searchByEntity(ExtractionService extractionService, ExtractionProperties properties) {
        Entity entity = generator.getCreatedEntity();
        return extractionService.getData(entity, properties);
    }

    static Stream<Object[]> datasetProducers() {
        return Stream.of(
                new Object[] {
                        (DataProducer) ExtractionServiceTest::searchByEntity,
                        RECORD_TIMESTAMP_KEY,
                        " quering by entity" },
                new Object[] {
                        (DataProducer) ExtractionServiceTest::searchByVariable,
                        NXC_EXTR_TIMESTAMP.getValue(),
                        " quering by variable" });
    }

    @BeforeAll
    static void beforeAll() {
        dataInitializer.setUpData();
    }

    @BeforeEach
    void beforeEach() throws InterruptedException {
        dataInitializer.waitForData(sparkSession);
    }

    @ParameterizedTest(name = "shouldGetOnlyActualDataIfNoPriorDataAvailable - {2}")
    @MethodSource("datasetProducers")
    public void shouldGetOnlyActualDataIfNoPriorDataAvailable(DataProducer datasetProducer, String timestampField,
            String datasetProducerName) {
        ExtractionProperties properties = ExtractionProperties.builder()
                .timeWindow(dataProduceStart,
                        dataProduceStart.plus(1, ChronoUnit.MINUTES).minusNanos(1))
                .lookupStrategy(LookupStrategy.LAST_BEFORE_START).build();

        long expectedDataCount = 30; // 30 messages/min
        Dataset<Row> result = datasetProducer.apply(extractionService.get(), properties);
        assertEquals(expectedDataCount, result.count());

        Instant timestampBeforeStart = getMinTimestamp(result, timestampField);
        assertEquals(dataProduceStart, timestampBeforeStart);
    }

    @ParameterizedTest(name = "shouldGetNoDataWhenLastPointOlderThanLookupDuration - {2}")
    @MethodSource("datasetProducers")
    public void shouldGetNoDataWhenLastPointOlderThanLookupDuration(DataProducer datasetProducer, String timestampField,
            String datasetProducerName) {
        Instant queryOutOfBoundsStart = queryStart
                .plus(LookupStrategy.LAST_BEFORE_START.getDuration().multipliedBy(2));
        Instant queryOutOfBoundsEnd = queryOutOfBoundsStart.plus(1, ChronoUnit.HOURS);

        ExtractionProperties properties = ExtractionProperties.builder()
                .timeWindow(queryOutOfBoundsStart, queryOutOfBoundsEnd)
                .lookupStrategy(LookupStrategy.LAST_BEFORE_START).build();

        Dataset<Row> result = datasetProducer.apply(extractionService.get(), properties);
        assertTrue(result.isEmpty());
    }

    @ParameterizedTest(name = "shouldGetOnlyGetOneRecordBeforeWhenNoCurrentData - {2}")
    @MethodSource("datasetProducers")
    public void shouldGetOnlyGetOneRecordBeforeWhenNoCurrentData(DataProducer datasetProducer, String timestampField,
            String datasetProducerName) {
        Instant queryOutOfBoundsStart = queryStart.plus(20, ChronoUnit.DAYS);
        Instant queryOutOfBoundsEnd = queryOutOfBoundsStart.plus(1, ChronoUnit.HOURS);

        ExtractionProperties properties = ExtractionProperties.builder()
                .timeWindow(queryOutOfBoundsStart, queryOutOfBoundsEnd)
                .lookupStrategy(LookupStrategy.LAST_BEFORE_START).build();

        int expectedDataCount = 1; // just 1 message before the query start
        Dataset<Row> result = datasetProducer.apply(extractionService.get(), properties);
        assertEquals(expectedDataCount, result.count());

        Instant timestampBeforeStart = getMinTimestamp(result, timestampField);
        assertEquals(dataProduceEnd.minusSeconds(2), timestampBeforeStart);
    }

    @ParameterizedTest(name = "shouldGetDataWithLastRecordBefore - {2}")
    @MethodSource("datasetProducers")
    public void shouldGetDataWithLastRecordBefore(DataProducer datasetProducer, String timestampField,
            String datasetProducerName) {
        ExtractionProperties properties = ExtractionProperties.builder()
                .timeWindow(queryStart, queryEnd)
                .lookupStrategy(LookupStrategy.LAST_BEFORE_START).build();

        int expectedActualDataCount = 60; // 2 minutes x 30 messages/min
        int expectedDataBeforeCount = 1; // 1 message before the query start
        long expectedDataCount = expectedDataBeforeCount + expectedActualDataCount;
        Dataset<Row> result = datasetProducer.apply(extractionService.get(), properties);
        assertEquals(expectedDataCount, result.count());

        Instant expectedTimestampBeforeStart = queryStart.minusSeconds(DATA_PRODUCE_INTERVAL_SECONDS);
        Instant timestampBeforeStart = getMinTimestamp(result, timestampField);
        assertEquals(expectedTimestampBeforeStart, timestampBeforeStart);
    }

    @ParameterizedTest(name = "shouldGetOnlyLookupDataWhenNoCurrentData - {2}")
    @MethodSource("datasetProducers")
    public void shouldGetOnlyLookupDataWhenNoCurrentData(DataProducer datasetProducer, String timestampField,
            String datasetProducerName) {
        Instant queryOutOfBoundsStart = queryStart.plus(20, ChronoUnit.DAYS);
        Instant queryOutOfBoundsEnd = queryOutOfBoundsStart.plus(1, ChronoUnit.HOURS);
        ExtractionProperties properties = ExtractionProperties.builder()
                .timeWindow(queryOutOfBoundsStart, queryOutOfBoundsEnd)
                .lookupStrategy(LookupStrategy.LAST_BEFORE_START_IF_EMPTY).build();

        int expectedDataCount = 1; // just 1 message before the query start
        Dataset<Row> result = datasetProducer.apply(extractionService.get(), properties);
        assertEquals(expectedDataCount, result.count());

        Instant timestampBeforeStart = getMinTimestamp(result, timestampField);
        assertEquals(dataProduceEnd.minusSeconds(2), timestampBeforeStart);
    }

    @ParameterizedTest(name = "shouldGetDataWithoutLookupWhenActualDataAvailable - {2}")
    @MethodSource("datasetProducers")
    public void shouldGetDataWithoutLookupWhenActualDataAvailable(DataProducer datasetProducer, String timestampField,
            String datasetProducerName) {
        ExtractionProperties properties = ExtractionProperties.builder()
                .timeWindow(queryStart, queryEnd)
                .lookupStrategy(LookupStrategy.LAST_BEFORE_START_IF_EMPTY).build();

        long expectedDataCount = 60; // 2 minutes x 30 messages/min

        Dataset<Row> result = datasetProducer.apply(extractionService.get(), properties);
        assertEquals(expectedDataCount, result.count());

        Instant timestampBeforeStart = getMinTimestamp(result, timestampField);
        assertEquals(queryStart, timestampBeforeStart);
    }

    private Instant getMinTimestamp(Dataset<Row> dataset, String timestampField) {
        return TimeUtils.getInstantFromNanos(dataset.agg(min(timestampField)).first().getLong(0));
    }

}
