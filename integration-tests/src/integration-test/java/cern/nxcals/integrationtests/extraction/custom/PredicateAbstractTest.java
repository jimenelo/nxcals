package cern.nxcals.integrationtests.extraction.custom;

import cern.nxcals.api.custom.service.ExtractionService;
import cern.nxcals.api.custom.service.extraction.ExtractionProperties;
import cern.nxcals.api.custom.service.extraction.LookupStrategy;
import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.domain.VariableConfig;
import cern.nxcals.api.domain.VariableDeclaredType;
import cern.nxcals.api.extraction.data.builders.DataQuery;
import cern.nxcals.api.extraction.metadata.VariableService;
import cern.nxcals.api.utils.TimeUtils;
import com.google.common.collect.ImmutableSortedSet;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static cern.nxcals.integrationtests.service.AbstractTest.getRandomName;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public interface PredicateAbstractTest {
    ExtractionService getExtractionService();

    VariableService getVariableService();

    Entity getEntity();

    Instant getStartTime();

    Instant getEndTime();

    String getFieldName();

    String getAdditionalFieldNameForPredicate();

    String getTimeFieldName();

    SparkSession getSparkSession();

    String predicate1Template = "%s > 100";
    String predicate2Template = "%s > 0 and %s < 90";
    String predicate3Template = "%s > 100 and %s == 'CPS'";

    default String getPredicate1() {
        return String.format(predicate1Template, getFieldName());
    }

    default String getPredicate2() {
        return String.format(predicate2Template, getFieldName(), getFieldName());
    }

    default String getPredicate3() {
        return String.format(predicate3Template, getFieldName(), getAdditionalFieldNameForPredicate());
    }

    default Variable registerVariableWithPredicate(String predicate) {
        return registerVariableWithPredicate(predicate, TimeWindow.infinite());
    }

    default Variable registerVariableWithPredicate(String predicate, TimeWindow validity) {
        return registerVariableWithPredicates(Map.of(predicate, validity));
    }

    default Variable registerVariableWithPredicates(Map<String, TimeWindow> predicates) {
        Entity entity = getEntity();
        Variable variable = getVariableService().create(
                buildVariableWithMultiplePredicates(entity.getSystemSpec(),
                        entity, getRandomName("VARIABLE_WITH_PREDICATE_"), getFieldName(), VariableDeclaredType.NUMERIC,
                        predicates));
        assertNotNull(variable);
        return variable;
    }

    private static Variable buildVariableWithMultiplePredicates(SystemSpec systemSpec, Entity entity,
            String variableName, String fieldName, VariableDeclaredType type, Map<String, TimeWindow> predicates) {
        List<VariableConfig> configs = new LinkedList<>();
        for (Map.Entry<String, TimeWindow> entry : predicates.entrySet()) {
            configs.add(getVariableConfig(entity.getId(), variableName, fieldName, entry.getKey(), entry.getValue()));
        }
        return Variable.builder().systemSpec(systemSpec).variableName(variableName)
                .declaredType(type).configs(ImmutableSortedSet.copyOf(configs)).build();
    }

    private static VariableConfig getVariableConfig(long entityId, String variableName, String fieldName,
            String predicate, TimeWindow validity) {
        return VariableConfig.builder()
                .entityId(entityId)
                .variableName(variableName)
                .fieldName(fieldName)
                .predicate(predicate)
                .validity(validity)
                .build();
    }

    default ExtractionProperties getDefaultExtractionProperties() {
        return ExtractionProperties.builder()
                .timeWindow(getStartTime(), getEndTime())
                .lookupStrategy(LookupStrategy.LAST_BEFORE_START_IF_EMPTY).build();
    }

    default Dataset<Row> getReferenceDataset(ExtractionProperties properties) {
        Entity entity = getEntity();

        Dataset<Row> referenceDataset = getExtractionService().getData(entity, properties);
        assertTrue(referenceDataset.count() > 0);
        return referenceDataset;
    }

    @Test
    default void shouldGetOnlyActualDataForVariableWithPredicate() {
        ExtractionProperties properties = getDefaultExtractionProperties();
        Dataset<Row> referenceDataset = getReferenceDataset(properties);

        String predicate = getPredicate1();
        Variable variable = registerVariableWithPredicate(predicate);
        try {
            Dataset<Row> result = getExtractionService().getData(variable, properties);
            assertEquals(referenceDataset.filter(predicate).count(), result.count());
        } finally {
            getVariableService().delete(variable.getId());
        }

    }

    @Test
    default void shouldGetOnlyActualDataIfThereAreTwoVariablesToOneEntityWithAndWithoutPredicate() {
        ExtractionProperties properties = getDefaultExtractionProperties();
        Dataset<Row> referenceDataset = getReferenceDataset(properties);

        String predicate1 = getPredicate1();
        Variable variable1 = registerVariableWithPredicate("");
        Variable variable2 = registerVariableWithPredicate(predicate1);
        try {
            Dataset<Row> result = DataQuery.builder(getSparkSession()).variables()
                    .system(variable2.getSystemSpec().getName())
                    .nameIn(variable1.getVariableName(), variable2.getVariableName())
                    .timeWindow(getStartTime(), getEndTime()).build();

            assertEquals(referenceDataset.count(), result.count());
        } finally {
            getVariableService().delete(variable1.getId());
            getVariableService().delete(variable2.getId());
        }
    }

    @Test
    default void shouldGetOnlyActualDataIfThereAreTwoVariablesToOneEntityWithPredicate() {
        ExtractionProperties properties = getDefaultExtractionProperties();
        Dataset<Row> referenceDataset = getReferenceDataset(properties);

        String predicate1 = getPredicate1();
        String predicate2 = getPredicate2();
        Variable variable1 = registerVariableWithPredicate(predicate1);
        Variable variable2 = registerVariableWithPredicate(predicate2);
        try {
            Dataset<Row> result = DataQuery.builder(getSparkSession()).variables()
                    .system(variable2.getSystemSpec().getName())
                    .nameIn(variable1.getVariableName(), variable2.getVariableName())
                    .timeWindow(getStartTime(), getEndTime()).build();

            assertTrue(referenceDataset.count() > 0); // check if test has any sense

            assertEquals(referenceDataset.filter(predicate1 + " or " + predicate2).count(), result.count());
        } finally {
            getVariableService().delete(variable1.getId());
            getVariableService().delete(variable2.getId());
        }
    }

    @Test
    default void shouldGetOnlyActualDataIfThereAreTwoVariablesToOneEntityWithPredicateWithDifferentValidity() {
        ExtractionProperties properties = getDefaultExtractionProperties();
        Dataset<Row> referenceDataset = getReferenceDataset(properties);

        String predicate1 = getPredicate1();
        String predicate2 = getPredicate2();
        long middleOfQueryTimeWindowAsNanos =
                (TimeUtils.getNanosFromInstant(getEndTime()) - TimeUtils.getNanosFromInstant(getStartTime())) / 2
                        + TimeUtils.getNanosFromInstant(getStartTime());
        Instant middleOfQueryTimeWindow = TimeUtils.getInstantFromNanos(middleOfQueryTimeWindowAsNanos);
        Variable variable1 = registerVariableWithPredicate(predicate1,
                TimeWindow.before(middleOfQueryTimeWindowAsNanos));
        Variable variable2 = registerVariableWithPredicate(predicate2,
                TimeWindow.after(middleOfQueryTimeWindowAsNanos));
        try {
            Dataset<Row> result = DataQuery.builder(getSparkSession()).variables()
                    .system(variable2.getSystemSpec().getName())
                    .nameIn(variable1.getVariableName(), variable2.getVariableName())
                    .timeWindow(getStartTime(), getEndTime()).build();

            String predicate1WithTimeWindow = getPredicateInTimeWindow(predicate1, getStartTime(),
                    middleOfQueryTimeWindow);
            String predicate2WithTimeWindow = getPredicateInTimeWindow(predicate2, middleOfQueryTimeWindow,
                    getEndTime());
            assertEquals(referenceDataset.filter(predicate1WithTimeWindow + " or " + predicate2WithTimeWindow).count(),
                    result.count());
        } finally {
            getVariableService().delete(variable1.getId());
            getVariableService().delete(variable2.getId());
        }
    }

    @Test
    default void shouldGetOnlyActualDataIfThereIsOneVariableWithMultipleConfigs() {
        ExtractionProperties properties = getDefaultExtractionProperties();
        Dataset<Row> referenceDataset = getReferenceDataset(properties);

        String predicate1 = getPredicate1();
        String predicate2 = getPredicate2();
        long middleOfQueryTimeWindowAsNanos =
                (TimeUtils.getNanosFromInstant(getEndTime()) - TimeUtils.getNanosFromInstant(getStartTime())) / 2
                        + TimeUtils.getNanosFromInstant(getStartTime());
        Instant middleOfQueryTimeWindow = TimeUtils.getInstantFromNanos(middleOfQueryTimeWindowAsNanos);
        Variable variable = registerVariableWithPredicates(Map.of(predicate1,
                TimeWindow.before(middleOfQueryTimeWindowAsNanos), predicate2,
                TimeWindow.after(middleOfQueryTimeWindowAsNanos)));
        try {
            Dataset<Row> result = getExtractionService().getData(variable, properties);

            String predicate1WithTimeWindow = getPredicateInTimeWindow(predicate1, getStartTime(),
                    middleOfQueryTimeWindow);
            String predicate2WithTimeWindow = getPredicateInTimeWindow(predicate2, middleOfQueryTimeWindow,
                    getEndTime());
            assertEquals(referenceDataset.filter(predicate1WithTimeWindow + " or " + predicate2WithTimeWindow).count(),
                    result.count());
        } finally {
            getVariableService().delete(variable.getId());
        }
    }

    default String getPredicateInTimeWindow(String predicate, Instant startTime, Instant endTime) {
        String pattern = "%s and %s >= %s and %s < %s";
        return String.format(pattern, predicate,
                getTimeFieldName(), TimeUtils.getNanosFromInstant(startTime), getTimeFieldName(),
                TimeUtils.getNanosFromInstant(endTime));
    }

    @Test
    default void shouldGetOnlyActualDataForVariableWithPredicateWhichUsesColumnsWhichAreNotInQuery() {
        ExtractionProperties properties = getDefaultExtractionProperties();
        Dataset<Row> referenceDataset = getReferenceDataset(properties);

        String predicate = getPredicate3();
        Variable variable = registerVariableWithPredicate(predicate);
        try {
            Dataset<Row> result = getExtractionService().getData(variable, properties);
            assertEquals(referenceDataset.filter(predicate).count(), result.count());
        } finally {
            getVariableService().delete(variable.getId());
        }
    }
}
