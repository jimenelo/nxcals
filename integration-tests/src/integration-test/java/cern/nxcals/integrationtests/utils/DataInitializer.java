package cern.nxcals.integrationtests.utils;

import java.util.concurrent.atomic.AtomicBoolean;

public abstract class DataInitializer {
    private final AtomicBoolean executed = new AtomicBoolean(false);


    /*
    Do action only once
     */

    protected void doOnce(Runnable action) {
        if (executed.compareAndSet(false, true)) {
            action.run();
        }
    }
}
