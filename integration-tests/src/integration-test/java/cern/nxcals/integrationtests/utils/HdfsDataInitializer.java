package cern.nxcals.integrationtests.utils;

import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.EntityHistory;
import cern.nxcals.api.domain.KeyValues;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.utils.TimeUtils;
import cern.nxcals.internal.extraction.metadata.InternalEntityService;
import lombok.RequiredArgsConstructor;
import org.apache.hadoop.fs.Path;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.avro.SchemaConverters;

import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.Map;
import java.util.function.Function;

@RequiredArgsConstructor
public class HdfsDataInitializer extends DataInitializer {
    private final CMWDatasetGenerator datasetGenerator;

    public void setUpData(SparkSession spark, String hdfsDataDir) {
        doOnce(() -> insertData(spark, hdfsDataDir));
    }

    protected void insertData(SparkSession sparkSession, String hdfsDataDir) {
        Entity entity = initEntity(datasetGenerator.getDataGenerationStart());
        Dataset<Row> dataset = datasetGenerator.createDataset(entity, sparkSession);
        putData(entity, dataset, datasetGenerator.getDataGenerationStart(), hdfsDataDir);
    }

    private Entity initEntity(Instant validFrom) {
        InternalEntityService service = (InternalEntityService) ServicesHelper.ENTITY_SERVICE;
        SystemSpec system = ServicesHelper.findSystemOrThrow(datasetGenerator.getSystemName());
        KeyValues partitionKeyValues = fromMap(datasetGenerator.getPartitionKeyValues());
        KeyValues entityKeyValues = fromMap(datasetGenerator.getEntityKeyValues());
        return service.findOrCreateEntityFor(system.getId(), entityKeyValues, partitionKeyValues,
                getAvroSchema(), TimeUtils.getNanosFromInstant(validFrom));
    }

    private String getAvroSchema() {
        return SchemaConverters.toAvroType(datasetGenerator.getSchema(), false, "data0", "cern.nxcals")
                .toString();
    }

    private void putData(Entity entity, Dataset<Row> dataset, Instant date, String hdfsDataDir) {
        String folderPath = getFolderToStoreData(entity.getFirstEntityHistory(), date, hdfsDataDir);
        String filePath = String.join(Path.SEPARATOR, folderPath, "dummyPath.parquet");
        dataset.write().parquet(filePath);
    }

    private String getFolderToStoreData(EntityHistory entityHistory, Instant date, String hdfsDataDir) {
        OffsetDateTime dateTime = date.atOffset(ZoneOffset.UTC);
        return String.join(Path.SEPARATOR,
                hdfsPathPrefixProvider(hdfsDataDir).apply(entityHistory),
                Integer.toString(dateTime.getYear()),
                Integer.toString(dateTime.getMonth().getValue()),
                Integer.toString(dateTime.getDayOfMonth()));
    }

    private Function<EntityHistory, String> hdfsPathPrefixProvider(String hdfsDataDir) {
        return entityHistory -> String.join(Path.SEPARATOR,
                hdfsDataDir,
                String.valueOf(entityHistory.getPartition().getSystemSpec().getId()),
                String.valueOf(entityHistory.getPartition().getId()),
                String.valueOf(entityHistory.getEntitySchema().getId()));
    }

    private static KeyValues fromMap(Map<String, Object> map) {
        return new KeyValues(map.hashCode(), map);
    }
}
