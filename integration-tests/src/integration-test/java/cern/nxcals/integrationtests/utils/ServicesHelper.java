package cern.nxcals.integrationtests.utils;

import cern.nxcals.api.custom.extraction.metadata.SnapshotService;
import cern.nxcals.api.custom.extraction.metadata.SnapshotServiceFactory;
import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.domain.VariableConfig;
import cern.nxcals.api.domain.VariableDeclaredType;
import cern.nxcals.api.extraction.metadata.EntityService;
import cern.nxcals.api.extraction.metadata.ServiceClientFactory;
import cern.nxcals.api.extraction.metadata.SystemSpecService;
import cern.nxcals.api.extraction.metadata.VariableService;
import cern.nxcals.api.extraction.metadata.queries.Entities;
import com.google.common.collect.ImmutableSortedSet;
import lombok.experimental.UtilityClass;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@UtilityClass
public class ServicesHelper {
    //    static {
    //        String username = System.getProperty("user.name");
    //        System.setProperty("kerberos.principal", username);
    //        System.setProperty("kerberos.keytab", "/opt/<user>/.keytab".replace("<user>", username));
    //        System.setProperty("service.url",
    //                "https://nxcals-<user>1.cern.ch:19093".replace("<user>", username)); //Do not use localhost here.
    //        System.setProperty("kafka.producer.bootstrap.servers",
    //                "https://nxcals-<user>3.cern.ch:9092,https://nxcals-<user>4.cern.ch:9092,https://nxcals-<user>5.cern.ch:9092".replace(
    //                        "<user>", username));
    //    }

    public static final VariableService VARIABLE_SERVICE = ServiceClientFactory.createVariableService();
    public static final EntityService ENTITY_SERVICE = ServiceClientFactory.createEntityService();
    public static final SystemSpecService SYSTEM_SERVICE = ServiceClientFactory.createSystemSpecService();
    public static final SnapshotService SNAPSHOT_SERVICE = SnapshotServiceFactory.createSnapshotService();

    public static Entity findEntityOrThrow(String systemName, Map<String, Object> keyValues) {
        SystemSpec systemSpec = findSystemOrThrow(systemName);
        return ENTITY_SERVICE.findOne(
                        Entities.suchThat().keyValues().eq(systemSpec, keyValues))
                .orElseThrow(() -> new IllegalArgumentException(
                        String.format("Could not find entity [ %s ]!", keyValues)));
    }

    public static SystemSpec findSystemOrThrow(String systemName) {
        return SYSTEM_SERVICE.findByName(systemName)
                .orElseThrow(() -> new IllegalArgumentException("Cannot find system: " + systemName));
    }

    public static Variable registerSimpleVariable(String variableName, Entity entity, String fieldName,
            VariableDeclaredType fieldType) {
        Variable variable = VARIABLE_SERVICE.create(
                buildVariable(entity, variableName, fieldName, fieldType));
        assertNotNull(variable);
        return variable;
    }

    private static Variable buildVariable(Entity entity, String variableName, String fieldName,
            VariableDeclaredType type) {
        VariableConfig config = VariableConfig.builder()
                .entityId(entity.getId())
                .variableName(variableName)
                .fieldName(fieldName).build();
        return Variable.builder().systemSpec(entity.getSystemSpec()).variableName(variableName)
                .declaredType(type).configs(ImmutableSortedSet.of(config)).build();
    }

    public static Variable registerVariableWithPredicate(String variableName, Entity entity, String fieldName,
            VariableDeclaredType fieldType, String predicate) {
        Variable variable = VARIABLE_SERVICE.create(
                buildVariableWithPredicate(entity, variableName, fieldName, fieldType, predicate));
        assertNotNull(variable);
        return variable;
    }

    private static Variable buildVariableWithPredicate(Entity entity, String variableName, String fieldName,
            VariableDeclaredType type, String predicate) {
        VariableConfig config = VariableConfig.builder()
                .entityId(entity.getId())
                .variableName(variableName)
                .predicate(predicate)
                .fieldName(fieldName).build();
        return Variable.builder().systemSpec(entity.getSystemSpec()).variableName(variableName)
                .declaredType(type).configs(ImmutableSortedSet.of(config)).build();
    }

    public static Variable registerVariableToEntity(String variableName, Entity entity) {
        return VARIABLE_SERVICE.create(buildVariableToEntity(variableName, entity));
    }

    private static Variable buildVariableToEntity(String variableName, Entity entity) {
        VariableConfig config = VariableConfig.builder()
                .entityId(entity.getId())
                .variableName(variableName)
                .build();
        return Variable.builder().systemSpec(entity.getSystemSpec()).variableName(variableName)
                .configs(ImmutableSortedSet.of(config)).build();

    }

    public static void deleteVariable(Variable variable) {
        VARIABLE_SERVICE.delete(variable.getId());
    }
}
