package cern.nxcals.integrationtests.utils;

import cern.cmw.datax.DataBuilder;
import cern.cmw.datax.ImmutableData;
import cern.cmw.datax.ImmutableEntry;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.extraction.data.builders.DataQuery;
import cern.nxcals.api.utils.TimeUtils;
import com.google.common.collect.ImmutableMap;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.apache.spark.sql.SparkSession;

import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import static cern.nxcals.api.custom.domain.CmwSystemConstants.DEVICE_KEY_NAME;
import static cern.nxcals.api.custom.domain.CmwSystemConstants.PROPERTY_KEY_NAME;
import static cern.nxcals.integrationtests.service.AbstractTest.getRandomName;
import static cern.nxcals.integrationtests.testutils.ExtractionTestHelper.CLASS_KEY;
import static cern.nxcals.integrationtests.testutils.ExtractionTestHelper.DEVICE_KEY;
import static cern.nxcals.integrationtests.testutils.ExtractionTestHelper.PROPERTY_KEY;
import static cern.nxcals.integrationtests.testutils.ExtractionTestHelper.RECORD_TIMESTAMP_KEY;
import static cern.nxcals.integrationtests.testutils.ExtractionTestHelper.RECORD_VERSION_KEY;
import static cern.nxcals.integrationtests.testutils.ExtractionTestHelper.SYSTEM;

@RequiredArgsConstructor
public class CMWEntityDataGenerator extends CMWDataGenerator implements CMWTestDataGenerator {
    /**
     * Generator which takes datapoint number and return values of given row.
     * Fields in ImmutableData should not change, otherwise schema will change also.
     */
    private final Function<Integer, ImmutableData> rowValuesGenerator;

    private final String deviceName = getRandomName("EXTRACTION.DEVICE_");
    private final String propertyName = getRandomName("EXTRACTION_PROPERTY");
    @Getter
    private final Map<String, Object> entityKeyValues = ImmutableMap
            .of(DEVICE_KEY_NAME, deviceName, PROPERTY_KEY_NAME, propertyName);
    private final String className = getRandomName("TEST_CLASS");
    @Getter
    private final Instant dataProduceStart = Instant.now().truncatedTo(ChronoUnit.MINUTES);
    @Getter
    private final Instant dataProduceEnd = dataProduceStart.plus(5, ChronoUnit.MINUTES);
    @Getter
    private final long dataProduceDurationSeconds = Duration.between(dataProduceStart, dataProduceEnd)
            .getSeconds();
    @Getter
    private final int dataProduceIntervalSeconds = 2; // produce data every N seconds
    @Getter
    private final long numberOfPoints = dataProduceDurationSeconds / dataProduceIntervalSeconds;


    @Override
    public String getSystemName() {
        return SYSTEM;
    }

    @Override
    public List<ImmutableData> generateData() {
        List<ImmutableData> intervalDataPoints = new ArrayList<>();
        for (int i = 0; i < dataProduceDurationSeconds; i += dataProduceIntervalSeconds) {
            ImmutableData dataOfPoint = buildData(dataProduceStart.plusSeconds(i), i);
            intervalDataPoints.add(dataOfPoint);
        }
        return intervalDataPoints;
    }

    private ImmutableData buildData(Instant timestamp, int rowNumber) {
        DataBuilder builder = ImmutableData.builder()
                .add(DEVICE_KEY, deviceName)
                .add(PROPERTY_KEY, propertyName)
                .add(CLASS_KEY, className)
                .add(RECORD_VERSION_KEY, 0L)
                .add(RECORD_TIMESTAMP_KEY, TimeUtils.getNanosFromInstant(timestamp));
        for (ImmutableEntry value : rowValuesGenerator.apply(rowNumber).getEntries()) {
            builder.add(value.getName(), value.get());
        }
        return builder.build();
    }

    @Override
    public boolean isDatasetReady(SparkSession spark) {
        return getDataCount(spark) == numberOfPoints;
    }

    private long getDataCount(SparkSession sparkSession) {
        return DataQuery.builder(sparkSession).byEntities().system(this.getSystemName())
                .startTime(dataProduceStart)
                .endTime(dataProduceEnd)
                .entity()
                .keyValue(DEVICE_KEY, deviceName)
                .keyValue(PROPERTY_KEY, propertyName)
                .build().count();
    }

    @Override
    public TimeWindow getQueryTimeWindow() {
        return TimeWindow.between(dataProduceStart, dataProduceEnd);
    }
}
