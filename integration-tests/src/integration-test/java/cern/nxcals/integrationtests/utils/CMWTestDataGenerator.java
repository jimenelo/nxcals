package cern.nxcals.integrationtests.utils;

import cern.cmw.datax.ImmutableData;
import org.apache.spark.sql.SparkSession;

import java.util.List;

public interface CMWTestDataGenerator {
    String getSystemName();

    List<ImmutableData> generateData();

    boolean isDatasetReady(SparkSession spark);
}
