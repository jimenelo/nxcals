from py4jgw.cern.nxcals.api.domain import Hierarchy, Variable
from py4jgw.cern.nxcals.api.extraction.metadata import ServiceClientFactory
from py4jgw.cern.nxcals.api.extraction.metadata.queries import (
    Variables,
    Hierarchies,
    Entities,
)

from python.nxcals.integrationtests import (
    get_random_name,
    MONITORING_SYSTEM_NAME,
    MONITORING_DEVICE_KEY,
    MONITORING_PARTITION_KEY,
)
from python.nxcals.integrationtests.type_stubs import (
    NAME_PREFIX,
)


system_service = ServiceClientFactory.createSystemSpecService()
variable_service = ServiceClientFactory.createVariableService()
hierarchy_service = ServiceClientFactory.createHierarchyService()
entity_service = ServiceClientFactory.createEntityService()


def test_call_variable_query():
    query = Variables.suchThat().variableName().like("not_existing_variable")
    variables = variable_service.findAll(query)
    assert len(variables) == 0


def test_creating_hierarchy():
    name = get_random_name(NAME_PREFIX)

    hierarchy = register_hierarchy(name)

    assert hierarchy.getName() == name

    hierarchy_service.deleteLeaf(hierarchy)


def register_hierarchy(name: str) -> Hierarchy:
    system = system_service.findByName(MONITORING_SYSTEM_NAME).get()

    return hierarchy_service.create(
        Hierarchy.builder().name(name).systemSpec(system).build()
    )


def test_finding_hierarchy():
    name = get_random_name(NAME_PREFIX)
    hierarchy = register_hierarchy(name)
    query = (
        Hierarchies.suchThat()
        .name()
        .like(NAME_PREFIX + "%")
        .and_()
        .systemName()
        .eq(MONITORING_SYSTEM_NAME)
    )

    found_hierarchies = hierarchy_service.findAll(query)

    assert hierarchy in found_hierarchies

    hierarchy_service.deleteLeaf(hierarchy)


def test_creating_and_finding_variable():
    system = system_service.findByName(MONITORING_SYSTEM_NAME).get()

    name = get_random_name(NAME_PREFIX)
    variable = register_variable(name)
    assert variable is not None

    condition = (
        Variables.suchThat()
        .variableName()
        .eq(name)
        .and_()
        .systemName()
        .eq(system.getName())
    )
    assert variable_service.findOne(condition).isPresent()

    variable_service.delete(variable.getId())


def register_variable(name: str):
    system = system_service.findByName(MONITORING_SYSTEM_NAME).get()
    variable = Variable.builder().variableName(name).systemSpec(system).build()

    return variable_service.create(variable)


def test_creating_and_finding_variables(spark_session):
    name1 = get_random_name(NAME_PREFIX)
    variable1 = register_variable(name1)
    name2 = get_random_name(NAME_PREFIX)
    variable2 = register_variable(name2)
    assert variable1 is not None and variable2 is not None

    condition = Variables.suchThat().variableName().in_([name1, name2])
    assert variable_service.findAll(condition).size() > 0

    variable_service.delete(variable1.getId())
    variable_service.delete(variable2.getId())


def test_creating_entity():
    name = get_random_name(NAME_PREFIX)
    system = system_service.findByName(MONITORING_SYSTEM_NAME).get()
    entity = entity_service.createEntity(
        system.getId(),
        {MONITORING_DEVICE_KEY: name},
        {MONITORING_PARTITION_KEY: name},
    )
    assert entity is not None


def test_creating_and_finding_entity():
    name = get_random_name(NAME_PREFIX)
    system = system_service.findByName(MONITORING_SYSTEM_NAME).get()
    entity = entity_service.createEntity(
        system.getId(),
        {MONITORING_DEVICE_KEY: name},
        {MONITORING_PARTITION_KEY: name},
    )
    maybe_found_entity = entity_service.findOne(
        Entities.suchThat().keyValues().eq(system, entity.getEntityKeyValues())
    )
    assert maybe_found_entity.isPresent()


def test_creating_and_finding_entities_by_id(spark_session):
    name = get_random_name(NAME_PREFIX)
    system = system_service.findByName(MONITORING_SYSTEM_NAME).get()
    entity1 = entity_service.createEntity(
        system.getId(),
        {MONITORING_DEVICE_KEY: name},
        {MONITORING_PARTITION_KEY: name},
    )
    entity2 = entity_service.createEntity(
        system.getId(),
        {MONITORING_DEVICE_KEY: name + "-1"},
        {MONITORING_PARTITION_KEY: name},
    )
    entities = entity_service.findAll(
        Entities.suchThat().id().in_([entity1.getId(), entity2.getId()])
    )
    assert entities.size() == 2
