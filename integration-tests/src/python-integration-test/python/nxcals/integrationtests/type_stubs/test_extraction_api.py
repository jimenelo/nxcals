from py4jgw.cern.nxcals.api.domain import TimeWindow
from py4jgw.cern.nxcals.api.extraction.data.builders import DataQuery
from py4jgw.cern.nxcals.common import SystemFields
from pyspark.sql import SparkSession

from python.nxcals.integrationtests import (
    CMW_SYSTEM_NAME,
    CMW_PROPERTY_KEY,
    CMW_DEVICE_KEY,
)
from python.nxcals.integrationtests.type_stubs.conftest import (
    GeneratedData,
    NUMBER_OF_POINTS,
)

start_time = "2022-04-02 00:00:00.000"
end_time = "2022-04-02 00:01:00.000"


def test_simple_extraction_query(query_builder: DataQuery):
    query_to_build = (
        query_builder.byEntities()
        .system(CMW_SYSTEM_NAME)
        .startTime(start_time)
        .endTime(end_time)
        .entity()
        .keyValue(CMW_DEVICE_KEY, "dev")
        .keyValueLike(CMW_PROPERTY_KEY, "prop")
    )
    dataset = query_to_build.build()
    assert dataset.isEmpty()


def test_simple_extraction_query_new_syntax(query_builder: DataQuery):
    query_to_build = (
        query_builder.entities()
        .system(CMW_SYSTEM_NAME)
        .keyValuesLike({CMW_DEVICE_KEY: "dev", CMW_PROPERTY_KEY: "prop"})
        .timeWindow(start_time, end_time)
    )
    dataset = query_to_build.build()
    assert dataset.isEmpty()


def test_simple_extraction_query_new_syntax_with_time_window(query_builder: DataQuery):
    time_window = TimeWindow.fromStrings(start_time, end_time)
    query_to_build = (
        query_builder.entities()
        .system(CMW_SYSTEM_NAME)
        .keyValuesLike({CMW_DEVICE_KEY: "dev", CMW_PROPERTY_KEY: "prop"})
        .timeWindow(time_window)
    )
    dataset = query_to_build.build()
    assert dataset.isEmpty()


def test_pivot(spark_session: SparkSession, data_generated: GeneratedData):
    variables = data_generated.variables.values()
    variable_names = {
        variable.getVariableName() for variable in data_generated.variables.values()
    }

    dataset = DataQuery.getAsPivot(
        spark_session, data_generated.time_window, set(variables)
    )

    real_columns = dataset.columns()
    expected_columns = variable_names.union(
        {SystemFields.NXC_EXTR_TIMESTAMP.getValue()}
    )
    assert expected_columns == set(real_columns)
    assert NUMBER_OF_POINTS == dataset.count()
