import uuid


def get_random_name(prefix):
    return f"{prefix}_{uuid.uuid4()}"


MONITORING_SYSTEM_NAME = "MOCK-SYSTEM"
MONITORING_DEVICE_KEY = "device"
MONITORING_PARTITION_KEY = "specification"

CMW_SYSTEM_NAME = "CMW"
CMW_DEVICE_KEY = "device"
CMW_PROPERTY_KEY = "property"
CMW_CLASS_KEY = "class"
CMW_TIMESTAMP_KEY = "__record_timestamp__"
CMW_VERSION_KEY = "__record_version__"
