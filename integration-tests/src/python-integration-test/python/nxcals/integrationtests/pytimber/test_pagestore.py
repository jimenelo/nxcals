from typing import Any, Optional

import numpy as np
import pytest
from python.nxcals.integrationtests.pytimber import END_TIME, START_TIME
from pytimber.pagestore import PageStore


@pytest.mark.parametrize(
    "data",
    [
        {"v1": ([1, 2, 3], [1, 2, 3]), "v2": ([4, 5, 6], [4, 5, 6])},
    ],
)
def test_get(db: PageStore, data: Any) -> None:
    db.store(data)
    data1 = db.get(["v1", "v2"])
    data2 = db.get("v%")

    assert len(data) == len(data1) == len(data2)
    assert set(data.keys()) == set(data1.keys()) == set(data2.keys())

    assert np.array_equal(data["v1"][0], data1["v1"][0])
    assert np.array_equal(data["v2"][1], data2["v2"][1])


@pytest.mark.parametrize(
    "data, idxa, idxb",
    [
        (
            {"v": ([START_TIME + 1, START_TIME + 2, START_TIME + 3], [1, 2, 3])},
            None,
            None,
        ),
        ({"v": ([START_TIME, END_TIME], [1, 2])}, START_TIME, END_TIME),
    ],
)
def test_get_idx(
    db: PageStore, data: Any, idxa: Optional[int], idxb: Optional[int]
) -> None:
    db.store(data)
    result = db.get_idx("v", idxa, idxb)

    assert np.array_equal(data["v"][0], result)


def test_count(db: PageStore) -> None:
    data = {"v": ([1, 2, 3], [1, 2, 3])}
    db.store(data)
    result = db.count("v")

    assert result == len(data["v"][0])


def test_pages(db: PageStore) -> None:
    data = {"v": ([0, 1, 2, 3, 4, 5, 6, 7, 8, 9], [0, 1, 2, 3, 4, 5, 6, 7, 8, 9])}
    db.store(data)

    db.split_pages("v", 20)
    db.delete_page(db.get_page(db.get_last_pageid()))

    assert len(db.get("v")["v"][0]) == 8

    page = db.get_page(3)
    db.delete_page(page)

    assert np.array_equal(db.get("v")["v"][0], [0, 1, 4, 5, 6, 7])
