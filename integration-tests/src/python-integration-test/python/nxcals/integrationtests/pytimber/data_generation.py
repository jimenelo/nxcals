from collections import namedtuple
from typing import Any, Dict, List, Optional, Tuple, TypeVar

from py4j.java_gateway import JavaObject
from pyspark.sql import SparkSession
from python.nxcals.integrationtests import (
    data_access,
    CMW_DEVICE_KEY,
    CMW_PROPERTY_KEY,
    CMW_CLASS_KEY,
    CMW_VERSION_KEY,
    CMW_TIMESTAMP_KEY,
)
from python.nxcals.integrationtests.hierarchy_details import HierarchyDetails
from python.nxcals.integrationtests.pytimber import (
    ALIGNMENT_VARIABLE_NAME,
    BEAM_MODE_FIELD,
    BEAM_MODES,
    FILL_NR_FIELD,
    FILLS_NUMBER,
    FIRST_FILL,
    INTERVALS_NUMBER,
    PYTIMBER_TEST_BMODE_DEVICE,
    PYTIMBER_TEST_BMODE_PROPERTY,
    PYTIMBER_TEST_CLASS,
    PYTIMBER_TEST_DEVICE,
    PYTIMBER_TEST_DEVICE_2,
    PYTIMBER_TEST_FILL_DEVICE,
    PYTIMBER_TEST_FILL_PROPERTY,
    PYTIMBER_TEST_FUND_DEVICE,
    PYTIMBER_TEST_FUND_PROPERTY,
    PYTIMBER_TEST_PROPERTY,
    PYTIMBER_TEST_PROPERTY_2,
    RANDOM_SUFFIX,
    RECORDS_NUMBER,
    SECOND,
    START_TIME,
    TIME_STEP,
    VAR_UNIT,
    DOUBLE_VECTOR_NUMERIC_VARIABLE_NAME,
    TEXT_VARIABLE_NAME,
    FLOAT_VARIABLE_NAME,
)
from python.nxcals.integrationtests.pytimber.test_utils import TestUtils
from nxcals.common import to_java_array

DEVICE_KEY = CMW_DEVICE_KEY
PROPERTY_KEY = CMW_PROPERTY_KEY
CLASS_KEY = CMW_CLASS_KEY
RECORD_VERSION_KEY = CMW_VERSION_KEY
RECORD_TIMESTAMP_KEY = CMW_TIMESTAMP_KEY

Field = namedtuple("Field", ["name", "formula", "dims", "declared_type"])


class DataGeneration:
    def __init__(
        self,
        spark: SparkSession,
        test_system: str,
        var_description: str,
        hierarchy_details: HierarchyDetails,
        snapshot_system: str,
        snapshots_list: List[Tuple[str, Dict[str, str]]],
    ):
        self.spark = spark
        self.test_system = test_system
        self.var_description = var_description
        self.hierarchy_details = hierarchy_details
        self.snapshot_system = snapshot_system
        self.snapshots_list = snapshots_list

        self.hierarchy_system = hierarchy_details.system
        self.hierarchy_base = hierarchy_details.base
        self.hierarchy_nodes_list = hierarchy_details.nodes_list

        self._metadata_utils = data_access.metadata_utils

        self._lang = spark._jvm.java.lang
        self._function = spark._jvm.java.util.function.Function
        self._immutable_data = spark._jvm.cern.cmw.datax.ImmutableData
        self._immutable_entry = spark._jvm.cern.cmw.datax.ImmutableEntryImpl
        self._nxcals_api = spark._jvm.cern.nxcals.api
        self._custom = self._nxcals_api.custom
        self._domain = self._nxcals_api.domain
        self._test_system_publisher = (
            self._nxcals_api.ingestion.PublisherFactory.newInstance().createPublisher(
                self.test_system, self._function.identity()
            )
        )
        self._fundamental_context = self._custom.service.fundamental.FundamentalContext

        self._data_builder = spark._jvm.cern.cmw.datax.DataBuilder

        self.fields = [
            Field(
                "FIELD0",
                lambda v: v,
                None,
                self._domain.VariableDeclaredType.NUMERIC,
            ),
            Field(
                "FIELD1",
                lambda v: f"str_{v}",
                None,
                self._domain.VariableDeclaredType.TEXT,
            ),
            Field(
                "FIELD2",
                lambda v: float(v + 0.2),
                None,
                None,
            ),
            Field(
                "FIELD3",
                lambda v: chr(ord("A") + v),
                None,
                self._domain.VariableDeclaredType.TEXT,
            ),
            Field(
                "FIELD4",
                lambda v: self._to_java_array(
                    self._lang.String, [f"A_{v}", f"B_{v}", f"C_{v}"]
                ),
                None,
                None,
            ),
            Field(
                "FIELD5",
                lambda v: self._to_java_array(
                    self.spark._jvm.int, [1 + v, 2 + v, 3 + v]
                ),
                None,
                self._domain.VariableDeclaredType.VECTOR_NUMERIC,
            ),
            Field(
                "FIELD6",
                lambda v: self._to_java_array(
                    self.spark._jvm.double, [1.0 + v / 10, 2.0 + v / 10, 3.0 + v / 10]
                ),
                None,
                self._domain.VariableDeclaredType.VECTOR_NUMERIC,
            ),
            Field(
                "FIELD7",
                lambda v: self._to_java_array(
                    self.spark._jvm.int, [1 + v, 2 + v, 3 + v, 4 + v, 5 + v, 6 + v]
                ),
                self._to_java_array(self.spark._jvm.int, [2, 3]),
                self._domain.VariableDeclaredType.MATRIX_NUMERIC,
            ),
        ]

    def generate_data(self) -> None:
        self._generate_variables_data()
        self._generate_fills_data()

    def register_variables(self) -> None:
        entity1 = self._create_test_entity(PYTIMBER_TEST_DEVICE, PYTIMBER_TEST_PROPERTY)
        entity2 = self._create_test_entity(
            PYTIMBER_TEST_DEVICE_2, PYTIMBER_TEST_PROPERTY_2
        )
        entity3 = self._create_test_entity(
            PYTIMBER_TEST_FUND_DEVICE, PYTIMBER_TEST_FUND_PROPERTY
        )
        entity4 = self._create_test_entity(
            PYTIMBER_TEST_FILL_DEVICE, PYTIMBER_TEST_FILL_PROPERTY
        )
        entity5 = self._create_test_entity(
            PYTIMBER_TEST_BMODE_DEVICE, PYTIMBER_TEST_BMODE_PROPERTY
        )

        for field in self.fields:
            test_variable_name = TestUtils.construct_var_name(RANDOM_SUFFIX, field.name)

            maybe_variable = self._metadata_utils.find_variable(
                system=self.test_system, name=test_variable_name
            )
            if not maybe_variable.isPresent():
                self._metadata_utils.create_variable(
                    name=test_variable_name,
                    entity=entity1,
                    field=field.name,
                    description=self.var_description,
                    unit=VAR_UNIT,
                    declared_type=field.declared_type,
                )

        # create a context variable
        vnum_var = self._metadata_utils.find_variable(
            system=self.test_system, name=DOUBLE_VECTOR_NUMERIC_VARIABLE_NAME
        )
        self._metadata_utils.create_variable(
            name=f"NXCALS_CONTEXT:{vnum_var.get().getId()}",
            entity=entity1,
            field="FIELD4",
            description=self.var_description,
        )

        # create variable for alignment
        self._metadata_utils.create_variable(
            name=ALIGNMENT_VARIABLE_NAME,
            entity=entity2,
            field="INTERVAL_FIELD",
            description=self.var_description,
        )

        # create fundamental variable
        self._metadata_utils.create_variable(
            name=f"ACC_{RANDOM_SUFFIX}{self._fundamental_context.VARIABLE_NAME_SUFFIX}",
            entity=entity3,
            field=None,
            description=self.var_description,
        )

        # create variable used by fills

        self._metadata_utils.recreate_variable(
            name="HX:FILLN",
            entity=entity4,
            field=FILL_NR_FIELD,
            description="Fill number",
        )
        self._metadata_utils.recreate_variable(
            name="HX:BMODE",
            entity=entity5,
            field=BEAM_MODE_FIELD,
            description="LHC Beam Mode",
        )

    def _generate_variables_data(self) -> None:
        data1 = []  # Scalar and  vector Numeric
        data2 = []  # Fundamental data
        data3 = []  # For alignment / scaling

        for interval_nr in range(INTERVALS_NUMBER):
            for rec_nr in range(RECORDS_NUMBER):
                field_values_list = []
                for field in self.fields:
                    field_values_list.append(
                        (field.name, field.formula(rec_nr), field.dims)
                    )

                data1.append(
                    self._build_data(
                        START_TIME + interval_nr * SECOND + rec_nr * TIME_STEP,
                        PYTIMBER_TEST_DEVICE,
                        PYTIMBER_TEST_PROPERTY,
                        field_values_list,
                    ),
                )

                if not rec_nr % 2:
                    field_values_list = [
                        ("__LSA_CYCLE__", f"CYCLE{rec_nr}", None),
                        (self._fundamental_context.USER_FIELD, f"USER{rec_nr}", None),
                        (
                            self._fundamental_context.DESTINATION_FIELD,
                            f"DEST{rec_nr}",
                            None,
                        ),
                    ]

                    data2.append(
                        self._build_data(
                            START_TIME + interval_nr * SECOND + rec_nr * TIME_STEP,
                            PYTIMBER_TEST_FUND_DEVICE,
                            PYTIMBER_TEST_FUND_PROPERTY,
                            field_values_list,
                        ),
                    )

            data3.append(
                self._build_data(
                    START_TIME + interval_nr * SECOND,
                    PYTIMBER_TEST_DEVICE_2,
                    PYTIMBER_TEST_PROPERTY_2,
                    [("INTERVAL_FIELD", interval_nr, None)],
                ),
            )

        self._publish_data(data1)
        self._publish_data(data2)
        self._publish_data(data3)

    def _generate_fills_data(self) -> None:
        data1 = []  # Fills
        data2 = []  # Beam modes
        for fill_nr in range(FILLS_NUMBER):
            data1.append(
                self._build_data(
                    START_TIME + fill_nr * SECOND,
                    PYTIMBER_TEST_FILL_DEVICE,
                    PYTIMBER_TEST_FILL_PROPERTY,
                    [(FILL_NR_FIELD, FIRST_FILL + fill_nr, None)],
                ),
            )
            for beam_mode in BEAM_MODES:
                data2.append(
                    self._build_data(
                        START_TIME
                        + fill_nr * SECOND
                        + BEAM_MODES.index(beam_mode) * TIME_STEP,
                        PYTIMBER_TEST_BMODE_DEVICE,
                        PYTIMBER_TEST_BMODE_PROPERTY,
                        [(BEAM_MODE_FIELD, beam_mode, None)],
                    ),
                )

        self._publish_data(data1)
        self._publish_data(data2)

    T = TypeVar("T")

    def _to_java_array(self, java_class: JavaObject, arr: List[T]) -> JavaObject:
        return to_java_array(self.spark, arr, java_class)

    def _create_test_entity(self, device_name: str, property_name: str) -> Any:
        key_values_pattern = f'%"{device_name}"%'

        maybe_entity = self._metadata_utils.find_entity(
            system=self.test_system,
            key_value_pattern=key_values_pattern,
        )
        if not maybe_entity.isPresent():
            entity = self._metadata_utils.create_cmw_entity(
                self.test_system, device_name, property_name
            )
        else:
            entity = maybe_entity.get()
        return entity

    def _build_data(
        self,
        timestamp_nanos: int,
        device_name: str,
        property_name: str,
        field_values_list: List[Tuple[str, Any, Any]],
    ) -> Any:
        builder = self._immutable_data.builder()
        builder.add(DEVICE_KEY, device_name, None)
        builder.add(PROPERTY_KEY, property_name, None)
        builder.add(CLASS_KEY, PYTIMBER_TEST_CLASS, None)
        builder.add(RECORD_TIMESTAMP_KEY, timestamp_nanos, None)
        builder.add(RECORD_VERSION_KEY, 1000000000000000000, None)

        for f in field_values_list:
            name, value, dims = f
            builder.add(name, value, dims)

        return builder.build()

    def _publish_data(self, records: Optional[List[Any]] = None) -> None:
        if records is None:
            records = []

        print("publishing a total of {} records".format(len(records)))

        for record in records:
            try:
                self._test_system_publisher.publish(record)
            except Exception as e:
                print("Got exception while publishing record", e)
        print("done!")

    def delete_variables(self) -> None:
        self._metadata_utils.delete_variables_by_description(
            self.test_system, self.var_description
        )

    def generate_hierarchies(self) -> None:
        for order, parent_path, node_name in self.hierarchy_nodes_list:
            hierarchy = self._metadata_utils.create_hierarchy(
                self.hierarchy_system,
                self.hierarchy_base,
                parent_path,
                node_name,
            )

            if hierarchy:
                self._metadata_utils.attach_variables_to_hierarchy(
                    hierarchy.getId(),
                    self.test_system,
                    [
                        TEXT_VARIABLE_NAME,
                        FLOAT_VARIABLE_NAME,
                    ],
                )

    def delete_hierarchies(self) -> None:
        for _, _, node_name in sorted(self.hierarchy_nodes_list, reverse=True):
            if node_name != self.hierarchy_base:
                self._metadata_utils.delete_hierarchy(self.hierarchy_system, node_name)

    def generate_snapshots(self) -> None:
        for snapshot_name, snapshot_properties in self.snapshots_list:
            snapshot = self._metadata_utils.create_snapshot(
                self.snapshot_system, snapshot_name, snapshot_properties
            )
            self._metadata_utils.attach_variables_to_snapshot(
                snapshot.getId(),
                self.test_system,
                [
                    TEXT_VARIABLE_NAME,
                    FLOAT_VARIABLE_NAME,
                ],
            )

    def delete_snapshots(self) -> None:
        for snapshot_name, _ in self.snapshots_list:
            self._metadata_utils.delete_snapshot(self.snapshot_system, snapshot_name)
