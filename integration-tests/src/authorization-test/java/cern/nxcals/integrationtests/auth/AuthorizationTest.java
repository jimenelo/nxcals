package cern.nxcals.integrationtests.auth;

import cern.nxcals.api.extraction.data.builders.DataQuery;
import cern.nxcals.api.utils.TimeUtils;
import cern.rbac.client.authentication.AuthenticationException;
import cern.rbac.common.authentication.LoginPolicy;
import cern.rbac.util.authentication.LoginServiceBuilder;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import java.io.IOException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import static org.assertj.core.api.Assertions.assertThat;

/*
 * We need to separate AuthorizationTestLocal and AuthorizationTestYarn to test authorization with delegation tokens
 * via RBAC independently.
 * It is also required to set forkEvery(1) property for gradle authorizationIntegrationTest task
 */
public class AuthorizationTest {

    private static final Instant NO_LOSSES_DAY = TimeUtils.getInstantFromString("2022-04-04 00:00:00.000");

    private static final String SYSTEM = "MOCK-SYSTEM";
    private static final String DEVICE_KEY = "device";
    private static final String DEVICE_VALUE = "NXCALS_MONITORING_DEV1";

    static void loginRbac() throws IOException, AuthenticationException {
        Properties properties = new Properties();
        properties.load(AuthorizationTest.class.getResourceAsStream("/application.properties"));
        String username = properties.getProperty("username");
        String password = properties.getProperty("password");
        LoginServiceBuilder.newInstance().userName(username).userPassword(password.toCharArray()).autoRefresh(true)
                .loginPolicy(LoginPolicy.EXPLICIT).build();
    }

    private List<String> extractDataAndVerify(Instant startTime, SparkSession session) {
        Instant endTime = startTime.plus(1, ChronoUnit.MINUTES);


        Dataset<Row> dataSet = DataQuery.builder(session).byEntities().system(SYSTEM).startTime(startTime)
                .endTime(endTime).entity().keyValue(DEVICE_KEY, DEVICE_VALUE).build();
        List<Row> rows = dataSet.collectAsList();

        assertThat(rows.size()).isPositive();
        Row row = rows.iterator().next();
        assertThat(row.get(0)).isNotNull();

        return Arrays.asList(dataSet.inputFiles());
    }

    void extractHBaseDataAndVerify(SparkSession session) {
        List<String> files = extractDataAndVerify(Instant.now().minus(3, ChronoUnit.MINUTES), session);
        // should not access HDFS files
        assertThat(files).isEmpty();
    }

    void extractHdfsDataAndVerify(SparkSession session) {
        List<String> files = extractDataAndVerify(NO_LOSSES_DAY, session);
        // should access HDFS files
        assertThat(files).anyMatch(file -> file.startsWith("hdfs://"));
    }
}
