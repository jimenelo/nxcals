package cern.nxcals.integrationtests.prometheus.prometheus;

import cern.nxcals.integrationtests.prometheus.config.PrometheusConfig;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.SpringBootDependencyInjectionTestExecutionListener;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

/**
 * Created by jwozniak on 25/03/17.
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = { PrometheusConfig.class })
@TestPropertySource("classpath:application.yml")
//jwozniak - due to a bug (mockito in spring boot, https://github.com/spring-projects/spring-boot/issues/6520) this is needed.
//Otherwise there is an exception when spring boot tries to reset mocks that actually are not used here.
@TestExecutionListeners(listeners = { SpringBootDependencyInjectionTestExecutionListener.class })
public class CheckPrometheus {

    @Autowired
    private Prometheus prometheus;

    @Test
    public void shouldHaveNoFiringAlertsInPrometheus() {

        JsonObject result = prometheus.alertsFiring();
        JsonArray alerts = result.getAsJsonObject("data").getAsJsonArray("result");
        Assertions.assertEquals(0, alerts.size(), "Should have 0 alerts firing in prometheus");
    }

    @Test
    public void shouldHaveNoNXCALSMonitoringParametersMetricsErrors() {
        JsonObject result = prometheus.metrics("nxcals_monitoring_dataloss");
        JsonArray metrics = result.getAsJsonObject("data").getAsJsonArray("result");
        Assertions.assertTrue(metrics.size() > 0, "Should have metrics nxcals_monitoring name");
        metrics.forEach(elt -> {
            JsonObject metric = (JsonObject) elt;
            Assertions.assertEquals(
                    "0",
                    metric.getAsJsonArray("value").get(1).getAsString(), "Metric " + metric.getAsJsonObject("metric").get("__name__").getAsString() + " should be 0");
        });
    }

}
