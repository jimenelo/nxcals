/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.api.ingestion;

import cern.cmw.datax.ImmutableData;
import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.EntitySchema;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.NonNull;

import java.util.Map;

/**
 * Represents the internal record meta information and the data needed to process it in the NXCALS system.
 * Assumptions (jwozniak): EntityData is here with only one history record that corresponds to the timestamp.
 * This history record must be taken as source of information for the schema & partition as the Entity may travel
 * in time between schemas and partitions.
 * We MUST NOT take this information from the Entity itself as it might not be the same as in the history if the timestamp
 * of the data is from the history that is older than the most recent.
 *
 *
 */
@Data
class RecordData {
    @NonNull
    //Hiden for safety, we should take on the info from this record and not from entityData.
    @Getter(AccessLevel.NONE)
    private final Entity entityData;
    @NonNull
    private final ImmutableData data;

    private final long timestamp;

    EntitySchema getSchemaData() {
        return entityData.getFirstEntityHistory().getEntitySchema();
    }

    long getSystemId() {
        return entityData.getSystemSpec().getId();
    }

    long getEntityId() {
        return entityData.getId();
    }

    long getPartitionId() {
        return entityData.getFirstEntityHistory().getPartition().getId();
    }

    long getSchemaId() {
        return getSchemaData().getId();
    }

    Map<String, Object> getEntityKeyValues() {
        return entityData.getEntityKeyValues();
    }

    Integer getKafkaPartition() {
        return entityData.getKafkaPartition();
    }
}
