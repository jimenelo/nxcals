/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.api.ingestion;

import java.io.Closeable;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.ForkJoinPool;

import static cern.nxcals.api.ingestion.Publisher.Constants.CURRENT_THREAD_EXECUTOR;

/**
 * NXCALS Ingestion API main interface. It defines methods to send data to the underlying system returning confirmation
 *
 * @param <V> Type of value to be published.
 *            in a form of {@link Result}.
 */
public interface Publisher<V> extends Closeable {
    enum Constants {
        ;
        static final Executor CURRENT_THREAD_EXECUTOR = Runnable::run;
        static final int MAX_FIELDS = 1000;
    }

    /**
     * Publishes synchronously the given value to the NXCALS system. The invocation is performed using the
     * caller thread.
     *
     * @param value the value to be published
     * @return result of the publication
     */
    default Result publish(V value) {
        return this.publishAsync(value, CURRENT_THREAD_EXECUTOR).join();
    }

    /**
     * Publishes asynchronously the given value to the NXCALS system. The invocation is performed using the
     * {@link ForkJoinPool#commonPool()} (unless it does not support a parallelism level of at least two, in which case,
     * a new Thread is created to run each task).
     *
     * @param value the value to be published
     * @return result of the publication
     */
    default CompletableFuture<Result> publishAsync(V value) {
        return publishAsync(value, ForkJoinPool.commonPool());
    }

    /**
     * Publishes asynchronously the given value to the NXCALS system. The invocation is performed using the
     * provided executor.
     *
     * @param value    the value to be published
     * @param executor the executor to perform the publication within.
     * @return result of the publication
     */
    CompletableFuture<Result> publishAsync(V value, Executor executor);
}
