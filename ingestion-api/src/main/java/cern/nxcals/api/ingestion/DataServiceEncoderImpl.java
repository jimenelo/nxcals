/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.api.ingestion;

import cern.cmw.datax.ImmutableData;
import cern.nxcals.api.domain.KeyValues;
import cern.nxcals.common.avro.DataEncoderImpl;
import cern.nxcals.common.converters.TimeConverter;
import org.apache.avro.Schema;

import java.util.function.Function;

/**
 * @deprecated - please use {@link DataEncoderImpl}
 */
@Deprecated
public class DataServiceEncoderImpl implements DataServiceEncoder<KeyValues, KeyValues, String, Long>,
        Function<RecordData, byte[]> {

    private final DataEncoderImpl delegate;
    private final RecordDataToBytesConverter serializer = new RecordDataToBytesConverter();

    /**
     * Constructor that builds encoder based on supplied schemas.
     *
     * @param entityKeyDefs          Entity schema definition.
     * @param partitionKeyDefs       Partition schema definition.
     * @param timeKeyDefs            Time schema definition.
     * @param recordVersionKeySchema Record version schema definition.
     * @param timeConverter          Timestamp converter from generic record.
     */
    @SuppressWarnings("WeakerAccess")
    public DataServiceEncoderImpl(Schema entityKeyDefs, Schema partitionKeyDefs, Schema timeKeyDefs,
            Schema recordVersionKeySchema, TimeConverter timeConverter) {
        this.delegate = new DataEncoderImpl(entityKeyDefs, partitionKeyDefs, timeKeyDefs,
                recordVersionKeySchema, timeConverter);
    }

    @Override
    public Long encodeTimeKeyValues(ImmutableData record) {
        try {
            return this.delegate.encodeTimeKeyValues(record);
        } catch (IllegalArgumentException e) {
            throw new IllegalRecordRuntimeException(e);
        }
    }

    @Override
    public KeyValues encodeEntityKeyValues(ImmutableData data) {
        try {
            return delegate.encodeEntityKeyValues(data);
        } catch (IllegalArgumentException e) {
            throw new IllegalRecordRuntimeException(e);
        }
    }

    @Override
    public KeyValues encodePartitionKeyValues(ImmutableData data) {
        try {
            return delegate.encodePartitionKeyValues(data);
        } catch (IllegalArgumentException e) {
            throw new IllegalRecordRuntimeException(e);
        }
    }

    /**
     * Creates a Schema from a given CMW Data record.
     */
    @Override
    public String encodeRecordFieldDefinitions(ImmutableData record) {
        try {
            return delegate.encodeRecordFieldDefinitions(record);
        } catch (IllegalArgumentException e) {
            throw new IllegalRecordRuntimeException(e);
        }
    }

    @Override
    public byte[] apply(RecordData record) {
        return this.serializer.apply(record);
    }

}
