package cern.nxcals.api.ingestion.retry;

import cern.nxcals.api.ingestion.retry.BackupStrategies.BackupConfig;
import cern.nxcals.api.ingestion.retry.BackupStrategies.BackupEventListener;
import com.google.common.annotations.VisibleForTesting;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Stream;

import static java.util.concurrent.TimeUnit.SECONDS;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.collections.CollectionUtils.isEmpty;

@Slf4j
@AllArgsConstructor
class LocalFileBackupStrategy<T> implements BackupStrategy<T> {
    @VisibleForTesting
    static final String DEFAULT_FILE_PREFIX = "nxcals-backup@";
    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd_HH:mm:ss.SSS");
    private static final AtomicLong SUFFIX_ID_GEN = new AtomicLong();
    private final ConcurrentMap<String, DataPartition<T>> partitions = new ConcurrentHashMap<>();
    private final BlockingQueue<Pair<CompletableFuture<Void>, T>> buffer;
    private final ScheduledExecutorService executor;
    private final BackupConfig config;
    private final Serializer<T> serializer;
    private final BackupEventListener eventListener;

    LocalFileBackupStrategy(ScheduledExecutorService executor, BackupConfig config, Serializer<T> serializer,
            BackupEventListener eventListener) {
        this(new ArrayBlockingQueue<>(config.getBufferSize()), executor, config, serializer, eventListener);
    }

    void init() {
        log.info("Starting local backup service with config: [{}]", this.config);
        long seconds = config.getFlushBufferFrequency().getSeconds();
        this.executor.scheduleAtFixedRate(this::flushBuffer, seconds, seconds, SECONDS);
    }

    @Override
    public CompletableFuture<Void> backup(@NonNull T msg) {
        CompletableFuture<Void> handle = new CompletableFuture<>();
        boolean accepted = this.buffer.offer(Pair.of(handle, msg));
        if (!accepted) {
            log.debug("Buffer full [{}], flushing to the disk", buffer.size());
            CompletableFuture.runAsync(this::flushBuffer, this.executor);
            try {
                this.buffer.put(Pair.of(handle, msg));
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                log.error("Error while trying to put {} to the internal buffer {}", msg, e);
                handle.completeExceptionally(new BackupException(e.getMessage(), e));
            }
        }
        return handle;
    }

    @VisibleForTesting
    void flushBuffer() {
        Collection<Pair<CompletableFuture<Void>, T>> pairs = new ArrayList<>(buffer.size());
        this.buffer.drainTo(pairs);
        if (isEmpty(pairs)) {
            log.trace("No buffered messages to be flushed, skipping");
            return;
        }
        Path path = config.getBackupPath();
        List<T> messages = pairs.stream().map(Pair::getValue).collect(toList());
        try {
            log.debug("Backing up {} messages in {}", messages.size(), path);
            this.serialize(messages, path);
            pairs.forEach(p -> p.getKey().complete(null));
        } catch (Exception e) {
            log.error("Error while serializing {} messages to {}", messages.size(), path, e);
            pairs.forEach(p -> p.getKey().completeExceptionally(new BackupException(e.getMessage(), e)));
        }
    }

    private void serialize(List<T> messages, Path path) {
        String fileName = createFileName();
        File file = Paths.get(path.toString(), fileName).toFile();
        this.serializer.writeTo(messages, file);
        log.debug("Successfully serialized {} records to {}", messages.size(), file);
    }

    @Override
    public List<DataPartition<T>> restore() {
        List<File> files = this.getFiles(config.getBackupPath(), config.getNbOfFilesToRestoreInOneGo());
        List<DataPartition<T>> ret = new LinkedList<>();
        for (File file : files) {
            List<T> read = this.serializer.readFrom(file);
            log.debug("Read {} records from {}, creating partition out of them", read.size(), file);
            DataPartition<T> partition = DataPartition.<T>builder().id(file.getAbsolutePath()).messages(read).build();
            ret.add(partition);
            this.partitions.put(partition.getId(), partition);
        }
        return ret;
    }

    @Override
    public void markAsRestored(DataPartition<T> partition) {
        log.debug("Partition {} marked as processed", partition);
        DataPartition<T> cached = this.partitions.get(partition.getId());
        if (cached == null) {
            log.warn("Partition {} not found ", partition);
            return;
        }
        log.trace("Found {} partition, trying to delete ", cached);
        delete(new File(cached.getId()));
        log.trace("Partition {} deleted", cached);
        this.partitions.remove(cached.getId());
    }

    @VisibleForTesting
    String createFileName() {
        LocalDateTime now = LocalDateTime.now();
        String filePrefix = getFilePrefix();
        return String.join("", filePrefix, DATE_TIME_FORMATTER.format(now), "_",
                String.valueOf(SUFFIX_ID_GEN.incrementAndGet()), this.serializer.getBackupFileSuffix());
    }

    @VisibleForTesting
    String getFilePrefix() {
        return !StringUtils.isEmpty(this.config.getFilePrefix()) ? this.config.getFilePrefix() : DEFAULT_FILE_PREFIX;
    }

    private List<File> getFiles(Path path, int nbOfFiles) {
        long filesCount = 0;
        try (Stream<Path> files = Files.list(path)) {
            List<File> ret = files.filter(this::isWithFilePrefix).sorted().limit(nbOfFiles).map(Path::toFile)
                    .collect(toList());
            if (!isEmpty(ret)) {
                filesCount = this.getFilesCountIn(path);
                log.debug("Got {}/{} files to read backup data from", ret.size(), filesCount);
            }
            this.eventListener.onRestore(filesCount);
            return ret;
        } catch (IOException e) {
            log.error("Error while reading backup files", e);
            throw new UncheckedIOException(e.getMessage(), e);
        }
    }

    private boolean isWithFilePrefix(Path path) {
        return path.getFileName().toString().startsWith(getFilePrefix());
    }

    private long getFilesCountIn(Path path) {
        try (Stream<Path> files = Files.list(path)) {
            return files.filter(this::isWithFilePrefix).count();
        } catch (IOException e) {
            log.error("Error while getting number of backup files", e);
            throw new UncheckedIOException(e.getMessage(), e);
        }
    }

    private void delete(File file) {
        try {
            Files.delete(file.toPath());
        } catch (IOException e) {
            log.warn("Cannot delete {} file due to {}", file, e);
            throw new UncheckedIOException(e.getMessage(), e);
        }
    }

}
