package cern.nxcals.api.ingestion;

import java.io.Closeable;

/**
 * Defines a sink object to send data to. Some validation required by the given implementation may be performed in
 * the caller thread. The send operation itself together with confirmation delivery may be executed in asynchronous
 * manner.
 * </p>
 * The result of performed operation should be returned through a provided callback instance.
 * </p>
 */
interface DataSink<D extends RecordData, C extends Callback> extends Closeable {

    /**
     * Interface responsible for sending data to the remote sink.
     *
     * @param data     Actual message that should be sent into the sink.
     * @param callback Callback with result of send operation.
     */
    void send(D data, C callback);
}
