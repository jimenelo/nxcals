package cern.nxcals.api.ingestion;

/**
 * Thrown when record cannot be accepted due to contract violation (size, nb or fields, etc)
 * Created by jwozniak on 26/10/16.
 */
public class IllegalRecordRuntimeException extends RuntimeException {

    /**
     * Exception constructor.
     *
     * @param message Exception message.
     */
    public IllegalRecordRuntimeException(String message) {
        super(message);
    }

    public IllegalRecordRuntimeException(IllegalArgumentException e) {
        super(e);
    }
}
