package cern.nxcals.api.ingestion.retry;

public class BackupException extends RuntimeException {
    public BackupException() {
    }

    public BackupException(String message) {
        super(message);
    }

    public BackupException(String message, Exception e) {
        super(message, e);
    }
}
