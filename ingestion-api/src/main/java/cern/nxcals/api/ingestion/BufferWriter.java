package cern.nxcals.api.ingestion;

import cern.cmw.datax.ImmutableData;
import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.KeyValues;
import cern.nxcals.api.ingestion.BufferedPublisher.DelayedMessage;
import cern.nxcals.api.ingestion.BufferedPublisher.EntityData;
import lombok.AllArgsConstructor;
import lombok.NonNull;

import java.time.Clock;
import java.time.Duration;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.function.Supplier;

@AllArgsConstructor
class BufferWriter<V> {
    private final Buffer<DelayedMessage> buffer;
    private final PublisherHelper<V> helper;
    private final Duration bufferTime;
    private final Clock clock;

    void put(@NonNull V value, @NonNull CompletableFuture<Result> result, @NonNull Executor executor) {
        ImmutableData data = this.helper.getConverter().apply(value);
        if (data.size() > Publisher.Constants.MAX_FIELDS) {
            result.completeExceptionally(new IllegalRecordRuntimeException(
                    "Record has " + data.size() + " fields with limit of " + Publisher.Constants.MAX_FIELDS
                            + ". This large number of fields cannot be processes with Spark at extraction. Please "
                            + "re-model your data."));
            return;
        }
        Long timestamp = this.helper.getEncoder().encodeTimeKeyValues(data);
        if (timestamp == null || timestamp <= 0) {
            result.completeExceptionally(new IllegalRecordRuntimeException(
                    "Illegal record timestamp for record=" + data.toString(10) + " system id=" + this.helper
                            .getSystemId() + " timestamp=" + timestamp));
            return;
        }

        EntityData entityData = entityDataOf(data, timestamp);
        DelayedMessage msg = DelayedMessage.builder().result(result).clock(this.clock).data(data)
                .expiredAt(this.clock.millis() + this.bufferTime.toMillis()).executor(executor).entityData(entityData)
                .build();
        this.buffer.push(msg);
    }

    private EntityData entityDataOf(ImmutableData record, Long timestamp) {
        String schemaKey = helper.getEncoder().encodeRecordFieldDefinitions(record);
        KeyValues entityKey = helper.getEncoder().encodeEntityKeyValues(record);
        KeyValues partitionKey = helper.getEncoder().encodePartitionKeyValues(record);
        Supplier<Entity> entitySupplier = () -> helper.findEntity(record, entityKey, timestamp);
        return new EntityData(entityKey.toString(), partitionKey.toString(), schemaKey, timestamp, entitySupplier);
    }

}
