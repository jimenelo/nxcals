package cern.nxcals.api.ingestion.retry;

import cern.nxcals.api.ingestion.retry.BackupStrategies.BackupConfig;
import cern.nxcals.api.ingestion.retry.BackupStrategy.DataPartition;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.mockito.ArgumentCaptor;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.IntStream;

import static cern.nxcals.api.ingestion.retry.LocalFileBackupStrategy.DEFAULT_FILE_PREFIX;
import static java.util.Collections.singletonList;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class LocalFileBackupStrategyTest {
    @TempDir
    public Path tmpFolder;
    private BackupConfig config;
    private Serializer<Integer> serializerMock;
    private ScheduledExecutorService executorMock;
    private LocalFileBackupStrategy<Integer> instance;

    @BeforeEach
    public void setUp() {
        this.serializerMock = mock(MockSerializer.class);
        when(serializerMock.getBackupFileSuffix()).thenReturn(".serial");
        this.executorMock = mock(ScheduledExecutorService.class);
        this.config = BackupConfig.builder().backupPath(tmpFolder)
                .flushBufferFrequency(Duration.ofMinutes(2)).bufferSize(2).nbOfFilesToRestoreInOneGo(2).build();
        this.instance = new LocalFileBackupStrategy<>(executorMock, config, serializerMock, filesCount -> {});
    }

    @Test
    public void shouldInit() {
        this.instance.init();
        long seconds = this.config.getFlushBufferFrequency().getSeconds();
        verify(this.executorMock, times(1))
                .scheduleAtFixedRate(any(Runnable.class), eq(seconds), eq(seconds), eq(SECONDS));
    }

    @Test
    public void shouldBackupSingleMessageInEmptyBuffer() {
        int value = ThreadLocalRandom.current().nextInt();
        BackupStrategy<Integer> strategy = BackupStrategies.asyncJavaSerializationToLocalFile(config, executorMock);
        CompletableFuture<Void> handle = strategy.backup(value);
        assertNotNull(handle);
        assertFalse(handle.isDone());
    }

    @Test
    public void shouldNotBackupNullMessage() {
        BackupStrategy<Integer> strategy = BackupStrategies.asyncJavaSerializationToLocalFile(config, executorMock);
        assertThrows(NullPointerException.class, () -> strategy.backup(null));
    }

    @Test
    public void shouldNotSerializeEmptyBuffer() {
        this.instance.flushBuffer();
        verify(serializerMock, times(0)).writeTo(anyList(), any());
    }

    @Test
    public void shouldSuccessfullyFlushSingleMessageBuffer() {
        // given
        int value = ThreadLocalRandom.current().nextInt();
        ArgumentCaptor<List> messagesCaptor = ArgumentCaptor.forClass(List.class);
        ArgumentCaptor<File> pathCaptor = ArgumentCaptor.forClass(File.class);

        // when
        CompletableFuture<Void> handle = this.instance.backup(value);
        this.instance.flushBuffer();

        // then
        verify(serializerMock, times(1)).writeTo(messagesCaptor.capture(), pathCaptor.capture());
        List<Integer> messages = messagesCaptor.getValue();
        assertEquals(1, messages.size());
        assertEquals(value, (int) messages.get(0));

        assertTrue(handle.isDone());
        assertFalse(handle.isCompletedExceptionally());

        File file = pathCaptor.getValue();
        assertTrue(file.getName().startsWith(DEFAULT_FILE_PREFIX));
        assertEquals(this.config.getBackupPath().toString(), file.getParent());
    }

    @Test
    public void shouldCompleteExceptionallyOnFlushingException() {
        // given
        doThrow(new IllegalStateException("Serializer exception")).when(serializerMock)
                .writeTo(anyList(), any(File.class));
        // when
        CompletableFuture<Void> handle = this.instance.backup(1);
        this.instance.flushBuffer();

        // then
        verify(serializerMock, times(1)).writeTo(anyList(), any(File.class));
        assertTrue(handle.isDone());
        assertTrue(handle.isCompletedExceptionally());
    }

    @Test
    public void shouldRestoreData() throws IOException {
        // given
        List<File> files = new ArrayList<>();
        AtomicLong restoredFilesCounter = new AtomicLong();
        this.instance = new LocalFileBackupStrategy<>(executorMock, config, serializerMock,
                restoredFilesCounter::addAndGet);

        int nbOfFiles = this.config.getNbOfFilesToRestoreInOneGo() + 1;
        for (int i = 0; i < nbOfFiles; i++) {
            File file = tmpFolder.resolve(this.instance.createFileName()).toFile();
            when(this.serializerMock.readFrom(eq(file))).thenReturn(singletonList(i));
            files.add(file);
            Files.createFile(file.toPath());
        }

        // when
        List<DataPartition<Integer>> partitions = this.instance.restore();

        // then
        assertEquals(config.getNbOfFilesToRestoreInOneGo(), partitions.size());
        for (DataPartition<Integer> partition : partitions) {
            assertTrue(files.stream().map(File::getAbsolutePath).anyMatch(name -> partition.getId().equals(name)));
            assertTrue(IntStream.rangeClosed(0, nbOfFiles).anyMatch(i -> partition.getMessages().contains(i)));
        }
        assertEquals(nbOfFiles, restoredFilesCounter.get());
    }

    @Test
    public void shouldNotRestoreDataIfNoFiles() {
        File[] files = tmpFolder.toFile().listFiles();
        assertNotNull(files);
        assertEquals(0, files.length);
        BackupStrategy<Integer> strategy = BackupStrategies.asyncJavaSerializationToLocalFile(config, executorMock);
        List<DataPartition<Integer>> partitions = strategy.restore();
        assertNotNull(partitions);
        assertEquals(0, partitions.size());
    }

    @Test
    public void shouldRestoreAndMarkData() throws IOException {
        // given
        int nbOfFiles = this.config.getNbOfFilesToRestoreInOneGo() + 1;
        for (int i = 0; i < nbOfFiles; i++) {
            File file = tmpFolder.resolve(this.instance.createFileName()).toFile();
            when(this.serializerMock.readFrom(eq(file))).thenReturn(singletonList(i));
            Files.createFile(file.toPath());
        }
        File[] files = tmpFolder.toFile().listFiles();
        assertNotNull(files);
        assertEquals(nbOfFiles, files.length);

        // when
        List<DataPartition<Integer>> partitions = this.instance.restore();
        for (DataPartition<Integer> partition : partitions) {
            this.instance.markAsRestored(partition);
        }

        // then
        files = tmpFolder.toFile().listFiles();
        assertNotNull(files);
        assertEquals(nbOfFiles - this.config.getNbOfFilesToRestoreInOneGo(), files.length);
    }

    @Test
    public void shouldUseDefaultFilePrefix() {
        assertNull(this.config.getFilePrefix());
        String filePrefix = this.instance.getFilePrefix();
        assertEquals(DEFAULT_FILE_PREFIX, filePrefix);
    }

    private interface MockSerializer extends Serializer<Integer> {
    }

}
