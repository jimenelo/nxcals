package cern.nxcals.api.ingestion;

import cern.cmw.datax.ImmutableData;
import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.EntityHistory;
import cern.nxcals.api.domain.EntitySchema;
import cern.nxcals.api.domain.Partition;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.common.utils.ReflectionUtils;
import com.google.common.collect.ImmutableSortedSet;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Created by jwozniak on 20/12/16.
 */
@ExtendWith(MockitoExtension.class)
public class RecordDataTest {
    @Mock
    private ImmutableData data1;

    @Test
    public void shouldCorrectlyCreateRecordData() {
        final EntitySchema schema = ReflectionUtils.builderInstance(EntitySchema.InnerBuilder.class).id(20L).schemaJson("test").build();
        final SystemSpec system = SystemSpec.builder().name("system").entityKeyDefinitions("")
                .partitionKeyDefinitions("").timeKeyDefinitions("").build();
        final Partition partition = ReflectionUtils.builderInstance(Partition.InnerBuilder.class)
                .id(10L).systemSpec(system).keyValues(new HashMap<>()).build();
        Entity entity = ReflectionUtils.builderInstance(Entity.InnerBuilder.class).id(1)
                .entityKeyValues(Collections.emptyMap()).systemSpec(system).partition(partition).build();
        ImmutableSortedSet<EntityHistory> history = ImmutableSortedSet.of(ReflectionUtils.builderInstance(EntityHistory.InnerBuilder.class)
                .id(10L).validity(TimeWindow.infinite()).partition(partition).entitySchema(schema).entity(entity).build());

        RecordData record = new RecordData(entity.toBuilder().entityHistory(history).build(), data1, 100);
        assertEquals(entity.getId(), record.getEntityId());
        assertEquals(data1, record.getData());
        assertEquals(100, record.getTimestamp());
        assertEquals(schema.getId(), record.getSchemaId());
        assertEquals(partition.getId(), record.getPartitionId());
    }
}