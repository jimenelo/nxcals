package cern.nxcals.api.ingestion.retry;

import cern.nxcals.api.exceptions.FatalDataConflictRuntimeException;
import cern.nxcals.api.ingestion.IllegalRecordRuntimeException;
import cern.nxcals.api.ingestion.RecordTooBigRuntimeException;
import com.google.common.collect.ImmutableList;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Collections;
import java.util.Set;
import java.util.concurrent.TimeoutException;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class NoDataErrorsAcceptStrategyTest {

    @ParameterizedTest
    @MethodSource("params")
    public void name(Pair<Throwable, Boolean> pair) {
        if (pair.getValue()) {
            assertTrue(AcceptStrategies.noDataErrors().test(pair.getKey()));
        } else {
            assertFalse(AcceptStrategies.noDataErrors().test(pair.getKey()));
        }
    }

    private static Object[][] params() {
        // @formatter:off
        return ImmutableList.of(
                Pair.of(null, true),
                Pair.of(new IllegalArgumentException(), false),
                Pair.of(new IllegalStateException(), false),
                Pair.of(new FatalDataConflictRuntimeException(), false),
                Pair.of(new IllegalRecordRuntimeException(""), false),
                Pair.of(new RecordTooBigRuntimeException(""), false),
                Pair.of(new TimeoutException(), true),
                Pair.of(new RuntimeException(new RecordTooBigRuntimeException("")), false)
        ).stream().map(Collections::singleton).map(Set::toArray).toArray(Object[][]::new);
        // @formatter:on
    }

}
