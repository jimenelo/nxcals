package cern.nxcals.api.ingestion.retry;

import cern.nxcals.api.ingestion.retry.BackupStrategies.BackupConfig;
import cern.nxcals.api.ingestion.retry.BackupStrategies.JavaSerializer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.IntStream;

import static cern.accsoft.commons.util.Assert.notNull;
import static java.time.Duration.ZERO;
import static java.util.stream.Collectors.toList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class BackupStrategiesTest {
    @TempDir
    public Path tmpFolder;
    private final BackupConfig configMock = mock(BackupConfig.class);
    private final ScheduledExecutorService executorMock = mock(ScheduledExecutorService.class);

    @Test
    public void shouldSerializeWithJava() throws IOException {
        this.shouldSerializeWith(new JavaSerializer<>());
    }

    // to make sonar happy
    @Test
    public void shouldNotSerializeNullListWithJava() throws IOException {
        JavaSerializer<Integer> serializer = new JavaSerializer<>();
        assertThrows(NullPointerException.class, () ->
                serializer.writeTo(null, tmpFolder.resolve("null").toFile()));
    }

    @Test
    public void shouldNotSerializeListToNullFileWithJava() {
        JavaSerializer<Integer> serializer = new JavaSerializer<>();
        assertThrows(NullPointerException.class, () -> serializer.writeTo(new ArrayList<>(), null));
    }

    @Test
    public void shouldNotSerializeNullListToNullFileWithJava() {
        JavaSerializer<Integer> serializer = new JavaSerializer<>();
        assertThrows(NullPointerException.class, () -> serializer.writeTo(null, null));
    }

    @Test
    public void shouldNotSerializeNotSerializableListToNullFileWithJava() throws IOException {
        JavaSerializer<Integer> serializer = new JavaSerializer<>();
        assertThrows(IllegalArgumentException.class, () -> serializer.writeTo(new NotSerializableList<>(), new File(tmpFolder.getParent().toFile().getAbsolutePath())));
    }

    @Test
    public void shouldNotCreateJavaSerializerWithNullConfig() {
        assertThrows(NullPointerException.class, () -> BackupStrategies.asyncJavaSerializationToLocalFile(null, executorMock));
    }

    @Test
    public void shouldNotCreateJavaSerializerWithNullExecutor() {
        assertThrows(NullPointerException.class, () -> BackupStrategies.asyncJavaSerializationToLocalFile(configMock, null));
    }

    @Test
    public void shouldNotCreateJavaSerializerWithNullExecutorAndNullConfig() {
        assertThrows(NullPointerException.class, () -> BackupStrategies.asyncJavaSerializationToLocalFile(null, null));
    }

    @Test
    public void shoulCreateJavaSerializerWithNotNullExecutorAndNotNullConfig() {
        when(configMock.getBufferSize()).thenReturn(1);
        when(configMock.getFlushBufferFrequency()).thenReturn(ZERO);
        BackupStrategy<Object> strategy = BackupStrategies.asyncJavaSerializationToLocalFile(configMock, executorMock);
        notNull(strategy);
    }

    private void shouldSerializeWith(Serializer<Integer> serializer) throws IOException {
        File file = tmpFolder.resolve("file.serial").toFile();
        List<Integer> messages = getMessages();
        serializer.writeTo(messages, file);
        List<Integer> read = serializer.readFrom(file);
        assertEquals(messages, read);
    }

    private List<Integer> getMessages() {
        int count = ThreadLocalRandom.current().nextInt(500) + 100;
        return IntStream.rangeClosed(1, count).boxed().collect(toList());
    }

    private static class NotSerializableList<T> extends AbstractList<T> implements List<T> {
        @Override
        public T get(int index) {
            return null;
        }

        @Override
        public int size() {
            return 0;
        }
    }
}
