package cern.nxcals.api.ingestion;

import cern.cmw.datax.ImmutableData;
import cern.nxcals.api.domain.KeyValues;
import cern.nxcals.api.ingestion.BufferedPublisher.DelayedMessage;
import cern.nxcals.api.ingestion.BufferedPublisher.EntityData;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneOffset;
import java.util.concurrent.CompletableFuture;

import static cern.nxcals.api.ingestion.Publisher.Constants.CURRENT_THREAD_EXECUTOR;
import static cern.nxcals.api.ingestion.Publisher.Constants.MAX_FIELDS;
import static java.time.Duration.ofMillis;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class BufferWriterTest {
    private Clock clock = Clock.fixed(Instant.ofEpochMilli(1L), ZoneOffset.systemDefault());
    private Buffer<DelayedMessage> bufferMock;
    private PublisherHelper<Integer> helperMock;
    private BufferWriter<Integer> instance;

    @BeforeEach
    public void setUp() {
        this.bufferMock = mock(Buffer.class);
        this.helperMock = mock(PublisherHelper.class, RETURNS_DEEP_STUBS);
        this.instance = new BufferWriter<>(bufferMock, helperMock, ofMillis(1L), clock);
    }

    @Test
    public void shouldNotPublishValueWithTooManyFields() {
        // given
        int value = 1;
        ImmutableData dataMock = mock(ImmutableData.class);
        when(dataMock.getEntryCount()).thenReturn(MAX_FIELDS + 1);
        when(helperMock.getConverter().apply(eq(value))).thenReturn(dataMock);
        // when
        CompletableFuture<Result> result = new CompletableFuture<>();
        this.instance.put(value, result, CURRENT_THREAD_EXECUTOR);
        // then
        assertTrue(result.isCompletedExceptionally());
        result.whenComplete((o, ee) -> assertNotNull(ee));
        result.whenComplete((o, ee) -> assertTrue(ee instanceof IllegalRecordRuntimeException));
    }

    @Test
    public void shouldNotPublishValueWithNullStamp() {
        // given
        int value = 1;
        when(helperMock.getConverter().apply(eq(value))).thenReturn(ImmutableData.builder().build());
        when(helperMock.getEncoder().encodeTimeKeyValues(any())).thenReturn(null);
        // when
        CompletableFuture<Result> result = new CompletableFuture<>();
        this.instance.put(value, result, CURRENT_THREAD_EXECUTOR);
        // then
        assertTrue(result.isCompletedExceptionally());
        result.whenComplete((o, ee) -> assertNotNull(ee));
        result.whenComplete((o, ee) -> assertTrue(ee instanceof IllegalRecordRuntimeException));
    }

    @Test
    public void andFinallyShouldPublish() {
        // given
        int value = 1;
        Long time = 1L;
        String schema = "schema";
        ImmutableData data = ImmutableData.builder().build();
        KeyValues keyValuesMock = mock(KeyValues.class);
        when(helperMock.getConverter().apply(eq(value))).thenReturn(data);
        when(helperMock.getEncoder().encodeTimeKeyValues(any())).thenReturn(time);
        when(helperMock.getEncoder().encodeRecordFieldDefinitions(eq(data))).thenReturn(schema);
        when(helperMock.getEncoder().encodeEntityKeyValues(eq(data))).thenReturn(keyValuesMock);
        when(helperMock.getEncoder().encodePartitionKeyValues(eq(data))).thenReturn(keyValuesMock);

        // when
        CompletableFuture<Result> result = new CompletableFuture<>();
        this.instance.put(value, result, CURRENT_THREAD_EXECUTOR);

        // then
        ArgumentCaptor<DelayedMessage> msgCaptor = ArgumentCaptor.forClass(DelayedMessage.class);
        verify(bufferMock, times(1)).push(msgCaptor.capture());

        DelayedMessage msg = msgCaptor.getValue();
        assertNotNull(msg);
        assertEquals(CURRENT_THREAD_EXECUTOR, msg.getExecutor());
        assertEquals(this.clock, msg.getClock());
        assertEquals(data, msg.getData());
        assertEquals(result, msg.getResult());
        assertEquals(1 + 1, msg.getExpiredAt());

        EntityData entityData = msg.getEntityData();
        assertEquals(schema, entityData.getSchemaKey());
        assertEquals(keyValuesMock.toString(), entityData.getEntityKey());
        assertEquals(keyValuesMock.toString(), entityData.getPartitionKey());
        assertEquals(time, entityData.getTime());

    }
}
