package cern.nxcals.api.ingestion.retry;

import cern.nxcals.api.ingestion.Result;
import cern.nxcals.api.ingestion.retry.BackupStrategies.BackupConfig;
import cern.nxcals.api.ingestion.retry.PublicationRetryer.PublicationRetryerBuilder;
import cern.nxcals.api.ingestion.retry.RecoveryStrategies.RecoveryConfig;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Predicate;
import java.util.stream.IntStream;

import static cern.nxcals.api.ingestion.retry.AcceptStrategies.noDataErrors;
import static cern.nxcals.api.ingestion.retry.BackupStrategies.asyncJavaSerializationToLocalFile;
import static cern.nxcals.api.ingestion.retry.RecoveryStrategies.basicStrategy;
import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static java.time.Duration.ofMillis;
import static java.time.Duration.ofSeconds;
import static java.util.concurrent.Executors.newScheduledThreadPool;
import static java.util.stream.Collectors.toList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.mock;

@Slf4j
public class IngestionRetryerIntegrationTest {
    @TempDir
    public Path tmpFolder;

    @Test
    public void shouldRetrySeveralTimesAndFinallySucceed() {
        // given
        PublicationRetryerBuilder<Integer> builder = PublicationRetryer.<Integer>builder();
        builder.retryConfig(createRetryerConfig(100, a -> true, false));
        builder.acceptStrategy(noDataErrors());
        // we don't backup in this test
        builder.backupStrategy(mock(MockBackupStrategy.class));
        AtomicInteger sentMsgCounter = new AtomicInteger();
        builder.publisher(msg -> {
            CompletableFuture<Result> ret = new CompletableFuture<>();
            int random = ThreadLocalRandom.current().nextInt(10);
            if (random < 6) {
                log.debug("Rejecting {}", msg);
                ret.completeExceptionally(new RuntimeException("Don't call logging support yet"));
            } else {
                ret.complete(null);
                log.debug("Completing {}", msg);
                sentMsgCounter.incrementAndGet();
            }
            return ret;
        });
        ScheduledExecutorService executor = createExecutor("retryer-%d");
        builder.retryExecutor(executor);
        PublicationRetryer<Integer> retryer = builder.build();
        List<Integer> messages = getMessages();

        // when
        retryWith(retryer, messages).join();

        // then
        assertEquals(messages.size(), sentMsgCounter.get());
        executor.shutdown();
    }

    @Test
    public void shouldRetryThenFailThenRestoreAndThenSucceed() {
        // given
        ScheduledExecutorService retryExecutor = createExecutor("retryer-%d");
        ScheduledExecutorService backupExecutor = createExecutor("backup-%d");

        List<Integer> messages = getMessages();
        PublicationRetryerBuilder<Integer> builder = PublicationRetryer.<Integer>builder();
        builder.retryExecutor(retryExecutor);
        builder.retryConfig(createRetryerConfig(100, a -> a < 2, true));
        builder.acceptStrategy(noDataErrors());
        BackupStrategy<Integer> backupStrategy = javaSerializationStrategy(backupExecutor, messages);
        builder.backupStrategy(backupStrategy);

        AtomicInteger sentMsgCounter = new AtomicInteger();
        Map<Integer, Boolean> msgRejected = new ConcurrentHashMap<>();
        CountDownLatch latch = new CountDownLatch(1);
        builder.publisher(msg -> {
            CompletableFuture<Result> ret = new CompletableFuture<>();
            boolean rejected = msgRejected.computeIfAbsent(msg, k -> FALSE);
            if (rejected) {
                log.debug("Completing {}", msg);
                ret.complete(null);
                int completed = sentMsgCounter.incrementAndGet();
                if (completed == messages.size()) {
                    latch.countDown();
                }
            } else {
                log.debug("Rejecting {}", msg);
                ret.completeExceptionally(new RuntimeException("Don't call logging support yet"));
                msgRejected.put(msg, TRUE);
            }
            return ret;
        });

        PublicationRetryer<Integer> retryer = builder.build();

        RecoveryStrategy recoveryStrategy = basicStrategy(retryExecutor, backupStrategy,
                new RecoveryConfig(ofSeconds(5), ofSeconds(1)), retryer);

        // when
        recoveryStrategy.start();
        retryWith(retryer, messages).join();

        // then
        try {
            latch.await();
        } catch (InterruptedException e) {
            fail();
        }
        assertEquals(messages.size(), sentMsgCounter.get());

        retryExecutor.shutdown();
        backupExecutor.shutdown();
    }

    private CompletableFuture<Void> retryWith(PublicationRetryer<Integer> retryer, List<Integer> messages) {
        CompletableFuture[] futures = messages.stream().map(retryer::retry).toArray(CompletableFuture[]::new);
        return CompletableFuture.allOf(futures);
    }

    private BackupStrategy<Integer> javaSerializationStrategy(ScheduledExecutorService executor, List<Integer> data) {
        return asyncJavaSerializationToLocalFile(
                BackupConfig.builder().bufferSize(data.size() / 10).nbOfFilesToRestoreInOneGo(2)
                        .flushBufferFrequency(ofSeconds(5)).backupPath(tmpFolder).build(), executor);
    }

    private RetryConfig createRetryerConfig(long delayMilis, Predicate<Integer> accept, boolean backup) {
        return RetryConfig.builder().delayAttempt(i -> ofMillis(delayMilis)).acceptAttempt(accept).shouldBackup(backup)
                .build();
    }

    private List<Integer> getMessages() {
        int count = (ThreadLocalRandom.current().nextInt(50) + 1) * 10;
        return IntStream.rangeClosed(1, count).boxed().collect(toList());
    }

    private ScheduledExecutorService createExecutor(String s) {
        return newScheduledThreadPool(1, threadFactory(s));
    }

    private ThreadFactory threadFactory(String pattern) {
        return new ThreadFactoryBuilder().setNameFormat(pattern).build();
    }

    private interface MockBackupStrategy extends BackupStrategy<Integer> {

    }
}
