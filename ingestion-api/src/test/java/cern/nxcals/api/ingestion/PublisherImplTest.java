/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.api.ingestion;

import cern.cmw.datax.EntryType;
import cern.cmw.datax.ImmutableData;
import cern.cmw.datax.ImmutableEntry;
import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.KeyValues;
import cern.nxcals.api.domain.Partition;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.common.SystemFields;
import cern.nxcals.common.avro.DataEncoderImpl;
import cern.nxcals.common.utils.ReflectionUtils;
import cern.nxcals.internal.extraction.metadata.InternalEntityService;
import com.google.common.collect.ImmutableMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.function.Function;

import static java.util.Collections.emptyMap;
import static java.util.Collections.emptySortedSet;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyMap;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by jwozniak on 20/12/16.
 */
public class PublisherImplTest {
    private static final Executor CURRENT_THREAD_EXECUTOR = Runnable::run;
    private static final Map<String, Object> ENTITY_KEY_VALUES = ImmutableMap.of("entity", "value1");
    private static final Map<String, Object> PARTITION_KEY_VALUES = ImmutableMap.of("partition", "value");
    private static final String SCHEMA = "SCHEMA";
    private static KeyValues entityKeyValues;
    private static KeyValues partitionKeyValues;
    private Function<Map<String, Object>, ImmutableData> converter;
    private InternalEntityService entityService;
    private DataEncoderImpl encoder;
    private DataSink<RecordData, DefaultCallback> dataSink;
    private Publisher<Map<String, Object>> publisher;
    private ImmutableData cmwData;
    private Entity entityData;
    private ImmutableEntry entityIdEntry;
    private ImmutableEntry partitionIdEntry;
    private Map<String, Object> data;

    private static final long SYSTEM_ID = 1L;
    private static final long ENTITY_ID = 1L;
    private static final long PARTITION_ID = 2L;
    private static final long RECORD_TIMESTAMP = 100L;

    @BeforeEach
    public void setUp() {
        entityKeyValues = mock(KeyValues.class);
        when(entityKeyValues.getId()).thenReturn(1);
        when(entityKeyValues.getKeyValues()).thenReturn(ENTITY_KEY_VALUES);

        partitionKeyValues = mock(KeyValues.class);
        when(partitionKeyValues.getId()).thenReturn(0);
        when(partitionKeyValues.getKeyValues()).thenReturn(PARTITION_KEY_VALUES);

        data = new HashMap<>();
        data.put("field1", 1L);
        data.put("field2", 2L);

        converter = mock(Function.class);
        entityService = mock(InternalEntityService.class);
        encoder = mock(DataEncoderImpl.class);
        dataSink = mock(DataSink.class);
        cmwData = mock(ImmutableData.class);
        SystemSpec system = SystemSpec.builder().name("system").entityKeyDefinitions("").partitionKeyDefinitions("")
                .timeKeyDefinitions("").build();
        entityData = ReflectionUtils.builderInstance(Entity.InnerBuilder.class).id(-1).entityKeyValues(emptyMap()).systemSpec(system).partition(
                ReflectionUtils.builderInstance(Partition.InnerBuilder.class).id(-1).systemSpec(system).keyValues(emptyMap()).build())
                .entityHistory(emptySortedSet()).lockedUntilStamp(null).recVersion(0).build();

        entityIdEntry = mock(ImmutableEntry.class);
        partitionIdEntry = mock(ImmutableEntry.class);
        when(converter.apply(anyMap())).thenReturn(cmwData);
        when(cmwData.getEntryCount()).thenReturn(10);

        when(encoder.encodeEntityKeyValues(cmwData)).thenReturn(entityKeyValues);
        when(encoder.encodePartitionKeyValues(cmwData)).thenReturn(partitionKeyValues);
        when(encoder.encodeRecordFieldDefinitions(cmwData)).thenReturn(SCHEMA);
        when(encoder.encodeTimeKeyValues(cmwData)).thenReturn(100L);
        when(entityService.findOrCreateEntityFor(1L, entityKeyValues, partitionKeyValues, SCHEMA, 100L))
                .thenReturn(entityData);
        PublisherHelper<Map<String, Object>> helper = PublisherHelper.<Map<String, Object>>builder()
                .converter(converter).dataSink(dataSink).encoder(encoder).systemId(1L).entityService(entityService)
                .build();
        publisher = new PublisherImpl<>(helper);
    }

    @Test
    public void shouldPublish() {
        Map<String, Object> data = new HashMap<>();
        data.put("field1", 1L);
        data.put("field2", 2L);
        CompletableFuture<Result> future = publisher.publishAsync(data, CURRENT_THREAD_EXECUTOR);

        assertNotNull(future);
        future.handle((o, e) -> {
            assertNotNull(o);
            assertNull(e);
            return o;
        });
        verify(dataSink, times(1)).send(any(RecordData.class), any(DefaultCallback.class));

    }

    @Test
    public void shouldPublishWithEntityIdAndPartitionId() {
        data.put(SystemFields.NXC_ENTITY_ID.getValue(), ENTITY_ID);
        data.put(SystemFields.NXC_PARTITION_ID.getValue(), PARTITION_ID);
        when(cmwData.getEntry(SystemFields.NXC_ENTITY_ID.getValue())).thenReturn(entityIdEntry);
        when(cmwData.getEntry(SystemFields.NXC_PARTITION_ID.getValue())).thenReturn(partitionIdEntry);
        when(entityIdEntry.getAs(EntryType.INT64)).thenReturn(ENTITY_ID);
        when(partitionIdEntry.getAs(EntryType.INT64)).thenReturn(PARTITION_ID);
        when(entityService.findOrCreateEntityFor(SYSTEM_ID, ENTITY_ID, PARTITION_ID, SCHEMA, RECORD_TIMESTAMP))
                .thenReturn(entityData);

        CompletableFuture<Result> future = publisher.publishAsync(data, CURRENT_THREAD_EXECUTOR);

        assertNotNull(future);
        future.exceptionally((exception) -> {
            exception.printStackTrace();
            return null;
        });
        assertFalse(future.isCompletedExceptionally());

        verify(dataSink, times(1)).send(any(RecordData.class), any(DefaultCallback.class));

    }

    @Test
    public void shouldFailToPublishWithEntityIdAndMissingPartitionId() {
        data.put(SystemFields.NXC_ENTITY_ID.getValue(), ENTITY_ID);
        when(cmwData.getEntry(SystemFields.NXC_ENTITY_ID.getValue())).thenReturn(entityIdEntry);
        when(entityIdEntry.getAs(EntryType.INT64)).thenReturn(ENTITY_ID);
        when(entityService.findOrCreateEntityFor(SYSTEM_ID, ENTITY_ID, PARTITION_ID, SCHEMA, RECORD_TIMESTAMP))
                .thenReturn(entityData);

        CompletableFuture<Result> future = publisher.publishAsync(data, CURRENT_THREAD_EXECUTOR);

        assertNotNull(future);
        future.exceptionally((exception) -> {
            assertEquals(IllegalRecordRuntimeException.class, exception.getClass());
            return null;
        });
        assertTrue(future.isCompletedExceptionally());
    }

    @Test
    public void shouldThrowOnPublishNull() {
        Map<String, Object> data = null;
        assertThrows(NullPointerException.class, () -> publisher.publish(data));
    }

    @Test
    public void shouldReturnExceptionFutureOnInvalidRecordTimestamp() {

        when(encoder.encodeTimeKeyValues(cmwData)).thenReturn(Long.valueOf(0));

        CompletableFuture<Result> future = publisher.publishAsync(data, CURRENT_THREAD_EXECUTOR);
        assertNotNull(future);

        assertTrue(future.isCompletedExceptionally());
        future.handle((val, ex) -> {
            assertEquals(ex.getClass(), IllegalRecordRuntimeException.class);
            return val;
        });

        verify(dataSink, times(0)).send(any(RecordData.class), any(DefaultCallback.class));
    }

    @Test
    public void shouldReturnExceptionFutureOnRecordFieldsMaxSizeExceeded() {
        when(cmwData.size()).thenReturn(10000);

        CompletableFuture<Result> future = publisher.publishAsync(data, CURRENT_THREAD_EXECUTOR);
        assertNotNull(future);

        assertTrue(future.isCompletedExceptionally());
        future.handle((val, ex) -> {
            assertEquals(ex.getClass(), IllegalRecordRuntimeException.class);
            return val;
        });

        verify(dataSink, times(0)).send(any(RecordData.class), any(DefaultCallback.class));
    }

    @Test
    public void shouldReturnExceptionFutureOnInternalConverterException() {

        when(converter.apply(anyMap())).thenThrow(new NullPointerException());

        CompletableFuture<Result> future = publisher.publishAsync(data, CURRENT_THREAD_EXECUTOR);
        assertNotNull(future);

        assertTrue(future.isCompletedExceptionally());
        verify(dataSink, times(0)).send(any(RecordData.class), any(DefaultCallback.class));
    }

    @Test
    public void shouldReturnExceptionFutureOnInternalEncoderException() {

        when(encoder.encodeEntityKeyValues(cmwData)).thenThrow(new NullPointerException());

        CompletableFuture<Result> future = publisher.publishAsync(data, CURRENT_THREAD_EXECUTOR);
        assertNotNull(future);

        assertTrue(future.isCompletedExceptionally());
        verify(dataSink, times(0)).send(any(RecordData.class), any(DefaultCallback.class));
    }

    @Test
    public void shouldReturnExceptionFutureOnInternalEntityServiceException() {

        when(entityService.findOrCreateEntityFor(SYSTEM_ID, entityKeyValues, partitionKeyValues, SCHEMA, 100L))
                .thenThrow(new NullPointerException());
        CompletableFuture<Result> future = publisher.publishAsync(data, CURRENT_THREAD_EXECUTOR);
        assertNotNull(future);

        assertTrue(future.isCompletedExceptionally());
        verify(dataSink, times(0)).send(any(RecordData.class), any(DefaultCallback.class));
    }

    @Test
    public void shouldReturnExceptionFutureOnInternalSinkException() {

        doThrow(new NullPointerException()).when(dataSink).send(any(RecordData.class), any(DefaultCallback.class));

        CompletableFuture<Result> future = publisher.publishAsync(data, CURRENT_THREAD_EXECUTOR);
        assertNotNull(future);

        assertTrue(future.isCompletedExceptionally());
        verify(dataSink, times(1)).send(any(RecordData.class), any(DefaultCallback.class));
    }
}
