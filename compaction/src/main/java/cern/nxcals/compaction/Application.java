/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.compaction;

import cern.nxcals.compaction.service.Compactor;
import cern.nxcals.compaction.service.LeaderElector;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.ApplicationPidFileWriter;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
@Slf4j
public class Application {

    private static final Logger LOGGER = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {
        try {

            SpringApplication app = new SpringApplication(Application.class);
            app.addListeners(new ApplicationPidFileWriter());
            ApplicationContext ctx = app.run();

            LeaderElector leaderElector = ctx.getBean(LeaderElector.class);
            leaderElector.waitForLeader();

            Compactor compactor = ctx.getBean(Compactor.class);
            compactor.run();

        } catch (Exception e) {
            LOGGER.error("Exception while running compaction app", e);
            System.exit(1);
        }
    }
}
