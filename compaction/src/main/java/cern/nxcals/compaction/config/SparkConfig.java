/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.compaction.config;

import cern.nxcals.common.config.SparkSessionModifier;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

@EnableConfigurationProperties(CompactorProperties.class)
@Configuration
@DependsOn({"kerberos"})
public class SparkConfig {
    @Bean
    public SparkSessionModifier compactionSparkSessionModifier(CompactorProperties properties) {
        return sparkSession -> {
            sparkSession.sparkContext().hadoopConfiguration()
                    .setInt("parquet.block.size",
                            properties.getParquet().getBlockSize()); //32 MB instead of the default 128MB
            sparkSession.sparkContext().hadoopConfiguration()
                    .setInt("parquet.dictionary.page.size",
                            properties.getParquet().getDictSize()); //3MB for bigger dictionaries
        };
    }
}