package cern.nxcals.compaction.config;

import cern.nxcals.common.metrics.MetricsRegistry;
import cern.nxcals.compaction.metrics.FileCountMetrics;
import lombok.Setter;
import org.apache.hadoop.fs.FileSystem;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.Map;

@Configuration
@ConfigurationProperties("metrics.file-count")
@Import(SpringConfig.class)
@Setter
@EnableScheduling
public class FileCountMetricsConfig {
    private Map<String, String> roots;
    private String prefix;

    @Bean
    public FileCountMetrics fileCountMetrics(MetricsRegistry metricsRegistry, FileSystem fileSystem) {
        return new FileCountMetrics(metricsRegistry, fileSystem, roots, prefix);
    }
}


