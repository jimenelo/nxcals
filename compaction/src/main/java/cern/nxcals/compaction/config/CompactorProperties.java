package cern.nxcals.compaction.config;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.time.Duration;

@ConfigurationProperties("compactor")
@Data
@NoArgsConstructor
public class CompactorProperties {
    @Data
    public static class ParquetProperties {
        int blockSize = 33554432;
        int dictSize = 3145728;
    }

    private ParquetProperties parquet;
    private int threads = 5;
    private int maxJobs = 1000;
    private Duration executeEvery = Duration.ofMinutes(10);
    private Duration noJobsSleep = Duration.ofMinutes(30);
    private String workDir;
    private boolean adaptiveEnabled;

}
