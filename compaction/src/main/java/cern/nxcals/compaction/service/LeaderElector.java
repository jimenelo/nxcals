package cern.nxcals.compaction.service;


import com.google.common.annotations.VisibleForTesting;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.recipes.leader.LeaderLatch;
import org.apache.curator.framework.recipes.leader.LeaderLatchListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.Closeable;
import java.io.IOException;

@Slf4j
@Service
public class LeaderElector implements Closeable {

    private final LeaderLatch leaderLatch;

    @Autowired
    public LeaderElector(@NonNull CuratorFramework cf, @Value("${zookeeper.leaderPath}") @NonNull String electionPath) {
        leaderLatch = new LeaderLatch(cf, electionPath);
        addListener();
    }

    private void addListener() {
        leaderLatch.addListener(new LeaderLatchListener() {

            @Override
            public void isLeader() {
                /*
                 * Empty method implementation here, since the information about
                 * acquired leadership is provided outside of the listener's scope.
                 */
            }

            @Override
            public void notLeader() {
                log.warn("**** LOST LEADERSHIP...****");
                log.info("Exiting the process to keep safe, will be restarted");
                //System.exit() is not reliable as it calls the hooks which can block (in case of no Zookeeper they will).
                Runtime.getRuntime().halt(-1);
            }
        });
    }

    @VisibleForTesting
    LeaderElector(LeaderLatch latch) {
        leaderLatch = latch;
        addListener();
    }

    public void waitForLeader() {
        try {
            log.info("**** WAITING FOR LEADERSHIP... ****");
            leaderLatch.start();
            leaderLatch.await();
            log.info("**** GOT LEADERSHIP! ****");
        } catch (InterruptedException e) {
            log.warn("Thread interrupted in waiting for leadership", e);
            Thread.currentThread().interrupt();
            throw new IllegalStateException(e.getMessage(), e);
        } catch (Exception e) {
            log.warn("Exception in waiting for leadership", e);
            throw new IllegalStateException(e);
        }
    }


    @Override
    public void close() throws IOException {
        leaderLatch.close();
    }
}
