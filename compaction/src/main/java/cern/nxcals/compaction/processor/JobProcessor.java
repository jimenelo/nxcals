package cern.nxcals.compaction.processor;

import cern.nxcals.common.domain.DataProcessingJob;
import cern.nxcals.common.domain.DataProcessingJob.JobType;

/**
 * Created by jwozniak on 21/10/16.
 */
public interface JobProcessor<T extends DataProcessingJob> {
    ProcessingStatus process(T job);

    JobType getJobType();
}
