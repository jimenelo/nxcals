package cern.nxcals.compaction.processor.writer;

import cern.nxcals.common.domain.DataProcessingJob;
import lombok.Getter;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder(toBuilder = true)
public class DatasetWriterJob {
    private boolean sortEnabled;
    private final long systemId;
    private final int partitionCount;

    public static DatasetWriterJob from(DataProcessingJob job) {
        return DatasetWriterJob.builder().sortEnabled(job.isSortEnabled()).systemId(job.getSystemId())
                .partitionCount(job.getPartitionCount()).build();
    }

}
