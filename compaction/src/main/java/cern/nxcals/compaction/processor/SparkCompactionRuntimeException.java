package cern.nxcals.compaction.processor;

/**
 * Created by jwozniak on 25/10/16.
 */
public class SparkCompactionRuntimeException extends RuntimeException {

    public SparkCompactionRuntimeException() {
    }

    public SparkCompactionRuntimeException(String message) {
        super(message);
    }

    public SparkCompactionRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }

    public SparkCompactionRuntimeException(Throwable cause) {
        super(cause);
    }

    public SparkCompactionRuntimeException(String message, Throwable cause, boolean enableSuppression,
            boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
