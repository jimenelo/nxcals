package cern.nxcals.compaction.processor.handler;

import org.apache.hadoop.fs.Path;

import java.io.IOException;

public interface HdfsFileReader<T> {
    T read(Path path) throws IOException;
}
