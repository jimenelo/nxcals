package cern.nxcals.compaction.processor;

import cern.nxcals.common.domain.AdaptiveMergeCompactionJob;
import cern.nxcals.common.domain.DataProcessingJob;
import cern.nxcals.common.utils.Lazy;
import cern.nxcals.compaction.config.CompactorProperties;
import cern.nxcals.compaction.processor.handler.HdfsFileReader;
import cern.nxcals.compaction.processor.writer.AdaptiveDatasetWriter;
import cern.nxcals.compaction.processor.writer.AdaptiveDatasetWriterJob;
import cern.nxcals.internal.extraction.metadata.InternalSystemSpecService;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.List;

@Component
@Slf4j
public class AdaptiveMergeCompactProcessor extends MergeCompactProcessor<AdaptiveMergeCompactionJob> {

    @Autowired
    public AdaptiveMergeCompactProcessor(@NonNull FileSystem fileSystem,
            @NonNull Lazy<SparkSession> ctx,
            @NonNull InternalSystemSpecService systemService,
            @NotNull CompactorProperties properties,
            @NonNull HdfsFileReader<List<Path>> reportFileReader) {
        super(fileSystem, ctx, systemService, properties, reportFileReader);
    }

    @Override
    public DataProcessingJob.JobType getJobType() {
        return DataProcessingJob.JobType.ADAPTIVE_MERGE_COMPACT;
    }

    @Override
    protected void saveDataset(Path operationPath, Dataset<Row> dataset, AdaptiveMergeCompactionJob job)
            throws IOException {
        AdaptiveDatasetWriter writer = new AdaptiveDatasetWriter(getDeduplicateCache(), getTimestampFieldsCache(),
                getFs());
        writer.saveDataset(operationPath, dataset, AdaptiveDatasetWriterJob.from(job));
    }
}
