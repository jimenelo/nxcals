package cern.nxcals.compaction.processor.writer;

import cern.nxcals.api.utils.TimeUtils;
import cern.nxcals.common.SystemFields;
import cern.nxcals.common.utils.HdfsFileUtils;
import com.github.benmanes.caffeine.cache.LoadingCache;
import com.google.common.annotations.VisibleForTesting;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.functions;
import org.apache.spark.sql.types.DataTypes;

import java.io.IOException;
import java.util.List;
import java.util.Set;

import static cern.nxcals.common.Constants.PART_FIELDS_SEPARATOR;
import static cern.nxcals.common.SystemFields.NXC_ENTITY_BUCKET_COLUMN;
import static cern.nxcals.common.SystemFields.NXC_TIME_PARTITION_COLUMN;

/**
 * Repartition by time and entity id, and save to specified directory
 */
@Slf4j
public class AdaptiveDatasetWriter extends DatasetWriter<AdaptiveDatasetWriterJob> {
    @VisibleForTesting
    static final String TIME_PARTITION_COLUMN = NXC_TIME_PARTITION_COLUMN.getValue();
    @VisibleForTesting
    static final String ENTITY_BUCKET_COLUMN = NXC_ENTITY_BUCKET_COLUMN.getValue();
    private final LoadingCache<Long, String> timestampFieldsCache;
    @Getter
    private final FileSystem fs;

    public AdaptiveDatasetWriter(LoadingCache<Long, Set<String>> deduplicateCache,
            LoadingCache<Long, String> timestampFieldsCache, FileSystem fs) {
        super(deduplicateCache);
        this.timestampFieldsCache = timestampFieldsCache;
        this.fs = fs;
    }

    @Override
    public void saveDataset(Path operationPath, Dataset<Row> dataset, AdaptiveDatasetWriterJob job) throws IOException {
        Dataset<Row> deduplicatedDataset = deduplicateDataset(job.getSystemId(), dataset);
        Dataset<Row> repartitionedDataset = repartitionByTimeAndEntityBucket(job, deduplicatedDataset);
        Dataset<Row> sortedDataset = sortIfNeeded(job, repartitionedDataset);
        log.debug("Writing to path {} with parquet row group size={}, hdfs block size={}, sorting={}", operationPath,
                job.getParquetRowGroupSize(),
                job.getHdfsBlockSize(),
                job.isSortEnabled());
        writeParquet(operationPath.toString(), sortedDataset, job);
        movePartitionsToFileNames(operationPath);
    }

    @VisibleForTesting
    Dataset<Row> repartitionByTimeAndEntityBucket(AdaptiveDatasetWriterJob job, Dataset<Row> dataset) {
        Dataset<Row> withTimePartitionColumn = addTimePartitionColumn(job, dataset);
        Dataset<Row> withPartitionColumns = addEntityPartitionColumn(job, withTimePartitionColumn);
        return withPartitionColumns.repartitionByRange(job.getPartitionCount(),
                withPartitionColumns.col(TIME_PARTITION_COLUMN),
                withPartitionColumns.col(ENTITY_BUCKET_COLUMN));
    }

    private Dataset<Row> addTimePartitionColumn(AdaptiveDatasetWriterJob job, Dataset<Row> dataset) {
        long intervalInSeconds = TimeUtils.SECONDS_IN_DAY / job.getPartitionType().getTimePartitionCount();
        String timestampField = timestampFieldsCache.get(job.getSystemId());

        Column timePartitionColumn = dataset.col(timestampField)
                .divide(functions.lit(TimeUtils.CONVERT_EPOCH_NANOS_TO_SECONDS_VALUE)) //to seconds only
                .mod(functions.lit(TimeUtils.SECONDS_IN_DAY)) //will have seconds since the beginning of the day
                .divide(functions.lit(intervalInSeconds)) //dividing by interval length will get the partition number
                .cast(DataTypes.LongType); //interested in integrals only

        return dataset.withColumn(TIME_PARTITION_COLUMN, timePartitionColumn);
    }

    private Dataset<Row> addEntityPartitionColumn(AdaptiveDatasetWriterJob job, Dataset<Row> dataset) {
        //entity bucketing
        //Standard way of calculating buckets: https://towardsdatascience.com/best-practices-for-bucketing-in-spark-sql-ea9f23f7dd53
        Column numberOfBuckets = functions.lit(job.getPartitionType().getEntityBucketCount());
        return dataset.withColumn(ENTITY_BUCKET_COLUMN,
                functions.pmod(functions.hash(dataset.col(SystemFields.NXC_ENTITY_ID.getValue())), numberOfBuckets));
    }

    private Dataset<Row> sortIfNeeded(AdaptiveDatasetWriterJob job, Dataset<Row> dataset) {
        if (job.isSortEnabled()) {
            String timestampField = timestampFieldsCache.get(job.getSystemId());
            //https://stackoverflow.com/questions/52159938/cant-write-ordered-data-to-parquet-in-spark
            return dataset.sortWithinPartitions(TIME_PARTITION_COLUMN, ENTITY_BUCKET_COLUMN,
                    timestampField); //must add sorting by partition, otherwise unsorted order when saving by partition happens
        }
        return dataset;
    }

    private void writeParquet(String destPathStr, Dataset<Row> dataset, AdaptiveDatasetWriterJob job) {
        dataset.write()
                .option("parquet.block.size", job.getParquetRowGroupSize())
                .option("dfs.block.size", job.getHdfsBlockSize())
                .partitionBy(TIME_PARTITION_COLUMN, ENTITY_BUCKET_COLUMN)
                .mode(SaveMode.Append)
                .parquet(destPathStr);
    }

    /**
     * This method is renaming files by incorporating the Spark created partitions into the file names like:
     * partColumn=1/entityBucket=2/file.parquet -> partColumn=1__entityBucket=2__file.parquet
     * and moving them directly into operationPath
     *
     * @param operationPath - the path for this operation
     */
    private void movePartitionsToFileNames(Path operationPath) throws IOException {
        log.info("Moving partitions into files names in path {}", operationPath.toString());
        //recursive as the files are with partitions as directories
        List<FileStatus> dataFilesIn = HdfsFileUtils.getParquetFilesIn(getFs(), operationPath,
                HdfsFileUtils.SearchType.RECURSIVE);
        for (FileStatus fileStatus : dataFilesIn) {
            Path sourceFilePath = fileStatus.getPath();
            String fileName = sourceFilePath.getName();
            String entityBucketName = sourceFilePath.getParent().getName();
            String timePartName = sourceFilePath.getParent().getParent().getName();
            org.apache.hadoop.fs.Path destinationFilePath = new org.apache.hadoop.fs.Path(
                    operationPath.toString() + org.apache.hadoop.fs.Path.SEPARATOR + timePartName
                            + PART_FIELDS_SEPARATOR + entityBucketName + PART_FIELDS_SEPARATOR + fileName);
            HdfsFileUtils.rename(getFs(), sourceFilePath, destinationFilePath);
        }
    }
}
