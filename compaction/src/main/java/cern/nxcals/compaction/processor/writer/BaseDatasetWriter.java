package cern.nxcals.compaction.processor.writer;

import com.github.benmanes.caffeine.cache.LoadingCache;
import com.google.common.annotations.VisibleForTesting;
import lombok.extern.slf4j.Slf4j;
import org.apache.hadoop.fs.Path;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;

import java.io.IOException;
import java.util.Set;

/**
 * Simple repartition and save file to selected directory
 */
@Slf4j
public class BaseDatasetWriter extends DatasetWriter<DatasetWriterJob> {
    private final LoadingCache<Long, String> timestampFieldsCache;

    public BaseDatasetWriter(LoadingCache<Long, Set<String>> deduplicateCache,
            LoadingCache<Long, String> timestampFieldsCache) {
        super(deduplicateCache);
        this.timestampFieldsCache = timestampFieldsCache;
    }

    @Override
    public void saveDataset(Path operationPath, Dataset<Row> dataset, DatasetWriterJob job) throws IOException {
        Dataset<Row> deduplicatedDataset = deduplicateDataset(job.getSystemId(), dataset);
        Dataset<Row> repartitionedDataset = repartitionDataSet(job, deduplicatedDataset);

        log.debug("Writing to path {}", operationPath);
        appendDataToFile(operationPath.toString(), repartitionedDataset);
    }

    @VisibleForTesting
    protected Dataset<Row> repartitionDataSet(DatasetWriterJob job, Dataset<Row> dataset) {
        log.debug("Repartitioning into {} partitions", job.getPartitionCount());
        return sortIfNeeded(job, dataset.repartition(job.getPartitionCount()));
    }

    private Dataset<Row> sortIfNeeded(DatasetWriterJob job, Dataset<Row> dataset) {
        //Enabled global sorting by timestamp, when needed (kkrynick)
        if (job.isSortEnabled()) {
            String timestampField = timestampFieldsCache.get(job.getSystemId());
            log.debug("Sorting by {}", timestampField);
            return dataset.sortWithinPartitions(timestampField);
        }
        return dataset;
    }

    public static void appendDataToFile(String path, Dataset<Row> df) {
        df.write().mode(SaveMode.Append).parquet(path);
    }
}
