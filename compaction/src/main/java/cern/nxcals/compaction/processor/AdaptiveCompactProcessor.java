/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.compaction.processor;

import cern.nxcals.api.domain.PartitionResource;
import cern.nxcals.api.domain.PartitionResourceHistory;
import cern.nxcals.api.domain.TimeEntityPartitionType;
import cern.nxcals.api.extraction.metadata.queries.PartitionResourceHistories;
import cern.nxcals.api.extraction.metadata.queries.PartitionResources;
import cern.nxcals.common.domain.AdaptiveCompactionJob;
import cern.nxcals.common.domain.DataProcessingJob.JobType;
import cern.nxcals.common.paths.StagingPath;
import cern.nxcals.common.utils.HdfsFileUtils;
import cern.nxcals.common.utils.Lazy;
import cern.nxcals.compaction.config.CompactorProperties;
import cern.nxcals.compaction.processor.writer.AdaptiveDatasetWriter;
import cern.nxcals.compaction.processor.writer.AdaptiveDatasetWriterJob;
import cern.nxcals.internal.extraction.metadata.InternalPartitionResourceHistoryService;
import cern.nxcals.internal.extraction.metadata.InternalPartitionResourceService;
import cern.nxcals.internal.extraction.metadata.InternalSystemSpecService;
import com.google.common.annotations.VisibleForTesting;
import lombok.AccessLevel;
import lombok.NonNull;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URI;
import java.time.Instant;
import java.util.Collection;
import java.util.List;

/**
 * Compacts the avro and parquet files in the directory.
 * If the process fails in the middle of operation there will be next compaction that actually will fix the duplicated files.
 * Using Adaptive Partitioning that partitions by time and buckets by entity id.
 * Also using adaptive row group sizes & hdfs block sizes.
 * Should only be included if enabled
 */
@Component
@Slf4j
public class AdaptiveCompactProcessor extends AbstractCompactProcessor<AdaptiveCompactionJob> {
    @VisibleForTesting
    @Setter(AccessLevel.PACKAGE)
    private AdaptiveDatasetWriter writer;
    private final InternalPartitionResourceService resourceService;
    private final InternalPartitionResourceHistoryService resourceHistoryService;

    private static class PartitionResourceAbsentException extends RuntimeException {

    }

    @Autowired
    public AdaptiveCompactProcessor(@NonNull FileSystem fileSystem, @NonNull Lazy<SparkSession> ctx,
            @NonNull InternalSystemSpecService systemService,
            @NonNull InternalPartitionResourceService resourceService,
            @NonNull InternalPartitionResourceHistoryService historyService,
            @NonNull CompactorProperties properties) {
        super(fileSystem, ctx, systemService, properties);
        this.resourceHistoryService = historyService;
        this.resourceService = resourceService;
        writer = new AdaptiveDatasetWriter(getDeduplicateCache(), getTimestampFieldsCache(), fileSystem);
    }

    @Override
    public JobType getJobType() {
        return JobType.ADAPTIVE_COMPACT;
    }

    protected Collection<Dataset<Row>> loadDatasets(List<Path> paths) {
        return convertToDataFrameByType(paths).values();
    }

    @Override
    protected void saveDataset(Path operationalPath, Dataset<Row> dataset, AdaptiveCompactionJob job)
            throws IOException {
        writer.saveDataset(operationalPath, dataset, AdaptiveDatasetWriterJob.from(job));
    }


    @Override
    protected boolean handleException(Exception e, AdaptiveCompactionJob job) {
        try {
            //Need to check if there is no data in the output folder, we cannot increase partitions for
            //existing data as it breaks extraction.
            //This assumes that there is only one job per output dir (day) running at the same time.
            if(HdfsFileUtils.hasDataFiles(getFs(), new Path(job.getDestinationDir()))) {
               log.warn("Directory {} has data, cannot change time/entity partitions for job {}", job.getDestinationDir(), job);
               return false;
            }

            //Try to increase the number of partitions
            TimeEntityPartitionType newType = TimeEntityPartitionType.next(job.getPartitionType(), 2);
            if (newType.equals(job.getPartitionType())) {
                log.error(
                        "Cannot further increase time/entity partitions for job {}, please see the logs to handle job error manually",
                        job);
                return false;
            }
            PartitionResource partitionResource = resourceService.findOne(
                            PartitionResources.suchThat().partitionId().eq(job.getPartitionId()).and().systemId()
                                    .eq(job.getSystemId()).and().schemaId().eq(job.getSchemaId()))
                    .orElseThrow(PartitionResourceAbsentException::new);

            Instant from = StagingPath.toInstant(job.getSourceDir());
            PartitionResourceHistory partitionResourceHistory = resourceHistoryService.findOne(
                            PartitionResourceHistories.suchThat()
                                    .partitionResourceId().eq(partitionResource.getId())
                                    .and()
                                    .validFromStamp().eq(from)
                                    .and()
                                    .validToStamp().eq(PartitionResourceHistory.to(from)))
                    .orElseThrow(PartitionResourceAbsentException::new);

            PartitionResourceHistory newPartitionResourceHistory = partitionResourceHistory.toBuilder()
                    .partitionInformation(newType.toString()).build();
            resourceHistoryService.update(newPartitionResourceHistory);
            log.warn("Updated partition resource history to {} for job {} due to a problem", newType, job);
            return true;
        } catch (PartitionResourceAbsentException exception) {
            log.error(
                    "No partition resource or partition resource history found for job {}, cannot handle job processing exception",
                    job);
            return false;
        }
    }


}
