package cern.nxcals.compaction.processor.handler;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static java.lang.String.format;

@Slf4j
@Component
public class ReportFileHandler implements HdfsFileReader<List<Path>>, HdfsFileWriter {
    private final FileSystem fileSystem;

    @Autowired
    public ReportFileHandler(FileSystem fileSystem) {
        this.fileSystem = Objects.requireNonNull(fileSystem);
    }

    @Override
    public List<Path> read(Path filePath) throws IOException {
        try (DataInputStream inputStream = fileSystem.open(filePath)) {
            return IOUtils.readLines(inputStream, Charset.defaultCharset()).stream()
                    .map(Path::new).collect(Collectors.toList());
        } catch (IOException e) {
            log.error("Could not read re-stage report content from path {}!", filePath, e);
            throw e;
        }
    }

    @Override
    public void write(Path filePath, String content) throws IOException {
        if (content == null) {
            throw new IllegalArgumentException(
                    format("Provided content to write on path [%s] is null!", filePath));
        }
        try (OutputStream outputStream = fileSystem.create(filePath)) {
            outputStream.write(content.getBytes());
        } catch (IOException e) {
            log.error("Could not write re-stage report content to path {}!", filePath, e);
            throw e;
        }
    }

}
