/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.compaction.metrics;

import cern.nxcals.common.metrics.MetricsRegistry;
import cern.nxcals.compaction.processor.ProcessingStatus;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.Objects;

@Slf4j
@RequiredArgsConstructor
public final class CompactionMetrics {
    static final String COMPACTION_HEARTBEAT_COUNT_METRIC_NAME = "nxcals.compaction.heartbeat";
    static final String COMPACTION_FILE_COUNT_METRIC_NAME = "nxcals.compaction.fileCount";
    static final String COMPACTION_BYTE_COUNT_METRIC_NAME = "nxcals.compaction.byteCount";
    static final String COMPACTION_TASK_COUNT_METRIC_NAME = "nxcals.compaction.taskCount";
    static final String COMPACTION_ERROR_COUNT_METRIC_NAME = "nxcals.compaction.errorCount";

    @NonNull
    private final MetricsRegistry metricsRegistry;

    public void heartbeat() {
        metricsRegistry.incrementCounterBy(COMPACTION_HEARTBEAT_COUNT_METRIC_NAME, 1);
    }

    public void export(ProcessingStatus status) {
        Objects.requireNonNull(status, "Process status cannot be null!");
        metricsRegistry.updateGaugeTo(COMPACTION_FILE_COUNT_METRIC_NAME, status.getFileCount());
        metricsRegistry.updateGaugeTo(COMPACTION_BYTE_COUNT_METRIC_NAME, status.getByteCount());
        metricsRegistry.updateGaugeTo(COMPACTION_TASK_COUNT_METRIC_NAME, status.getTaskCount());
        metricsRegistry.updateGaugeTo(COMPACTION_ERROR_COUNT_METRIC_NAME, status.getErrorCount());
    }
}
