package cern.nxcals.compaction.processor;

import cern.nxcals.api.domain.TimeEntityPartitionType;
import cern.nxcals.common.domain.AdaptiveMergeCompactionJob;
import cern.nxcals.common.utils.Lazy;
import cern.nxcals.compaction.config.CompactorProperties;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.spark.sql.DataFrameWriter;
import org.apache.spark.sql.SaveMode;
import org.junit.jupiter.api.Test;
import org.mockito.Answers;

import java.io.IOException;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class AdaptiveMergeCompactProcessorTest extends MergeCompactProcessorTest {

    AdaptiveMergeCompactionJob job = AdaptiveMergeCompactionJob.builder()
            .destinationDir(new Path("/test").toUri())
            .filePrefix("")
            .files(List.of(new Path("/file").toUri()))
            .partitionType(TimeEntityPartitionType.of(2, 2))
            .sortEnabled(false)
            .sourceDir(new Path("/source").toUri())
            .systemId(SYSTEM_ID)
            .totalSize(100)
            .restagingReportFile(new Path("/tst").toUri())
            .build();

    @Test
    void testSaveFile() throws IOException {
        // given
        initMockDataframeForAdaptive();
        initMockSystemService();
        FileSystem fs = mock(FileSystem.class);
        CompactorProperties properties = new CompactorProperties();
        properties.setWorkDir("c");
        AdaptiveMergeCompactProcessor processor = new AdaptiveMergeCompactProcessor(fs,
                new Lazy<>(() -> sparkSession), systemService, properties, reportFileReader);

        // when
        processor.saveDataset(new Path("/"), dataFrame, job);

        // then, it is fine, if mocks doesn't complain about unnecessary stubbing
    }

    protected void initMockDataframeForAdaptive() {
        DataFrameWriter dfWriter = mock(DataFrameWriter.class, Answers.RETURNS_DEEP_STUBS);
        when(dataFrame.union(any())).thenReturn(dataFrame);
        when(dataFrame.dropDuplicates(any(String[].class))).thenReturn(dataFrame);
        when(dataFrame.repartition(anyInt())).thenReturn(dataFrame);
        when(dataFrame.sortWithinPartitions(any(String.class), any(String.class), any(String.class))).thenReturn(
                dataFrame);
        when(dataFrame.withColumn(any(), any())).thenReturn(dataFrame);
        when(dataFrame.repartitionByRange(anyInt(), any(), any())).thenReturn(dataFrame);
        when(dataFrame.write()).thenReturn(dfWriter);
        when(dfWriter.mode(any(SaveMode.class))).thenReturn(dfWriter);
        when(dfWriter.option(anyString(), anyLong())).thenReturn(dfWriter);
        when(dfWriter.partitionBy(anyString(), anyString())).thenReturn(dfWriter);
    }
}
