/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.compaction.processor;

import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.extraction.metadata.queries.SystemSpecs;
import cern.nxcals.common.domain.RestageJob;
import cern.nxcals.common.utils.Lazy;
import cern.nxcals.common.utils.ReflectionUtils;
import cern.nxcals.compaction.SparkContextStoppedRuntimeException;
import cern.nxcals.compaction.config.CompactorProperties;
import cern.nxcals.compaction.processor.handler.HdfsFileWriter;
import com.github.rutledgepaulv.qbuilders.conditions.Condition;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.Path;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static cern.nxcals.common.utils.RSQLUtils.toRSQL;
import static cern.nxcals.compaction.processor.RestageProcessor.PARTITION_BY_HOUR_COLUMN;
import static org.apache.spark.sql.functions.col;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class RestageProcessorTest extends BaseJobProcessorTest {

    private final String basePath1 = System.getProperty("user.dir") + "/staging/CMW-1/1/1/1/2016-3-21";
    private final String basePath2 = System.getProperty("user.dir") + "/staging/CMW-2/1/1/1/2016-3-21";

    private final String file1 = basePath1 + "/f1.avro";
    private final String file2 = basePath1 + "/f2.avro";
    private final String file3 = basePath1 + "/f3.parquet";

    private final String file4 = basePath2 + "/f4.avro";
    private final String file5 = basePath2 + "/f5.avro";
    private final String file6 = basePath2 + "/f6.parquet";

    private final String outFile1 = basePath1 + "/out1.parquet";
    private final String outFile2 = basePath2 + "/out2.parquet";

    @Mock
    private FileStatus fileStatus1, fileStatus2, fileStatus3, fileStatus4, fileStatus5, fileStatus6;

    @Mock
    private HdfsFileWriter reportFileWriter;

    private RestageJob jobData;
    private RestageProcessor jobProcessor;
    @AfterEach
    public void tearDown() {
        reset(fileSystem, dataFrame);
    }

    @BeforeEach
    public void setup() throws IOException {
        jobData = RestageJob.builder()
                .sourceDir(URI.create(RESTAGING_DIR))
                .destinationDir(URI.create(DESTINATION_DIR))
                .files(createFiles())
                .dataFiles(createDataFiles())
                .restagingReportFile(RESTAGING_REPORT_FILE)
                .systemId(SYSTEM_ID)
                .build();

        doReturn(fileStatus1).when(fileSystem).getFileStatus(argThat(path -> path.toString().equals(file1)));
        doReturn(fileStatus2).when(fileSystem).getFileStatus(argThat(path -> path.toString().equals(file2)));
        doReturn(fileStatus3).when(fileSystem).getFileStatus(argThat(path -> path.toString().equals(file3)));
        doReturn(fileStatus4).when(fileSystem).getFileStatus(argThat(path -> path.toString().equals(file4)));
        doReturn(fileStatus5).when(fileSystem).getFileStatus(argThat(path -> path.toString().equals(file5)));
        doReturn(fileStatus6).when(fileSystem).getFileStatus(argThat(path -> path.toString().equals(file6)));

        when(fileSystem.exists(any(Path.class))).thenReturn(true);
        //this is important for batches as it should return one file for each batch.

        doReturn(iteratorForOutput).when(fileSystem).listFiles(any(Path.class), anyBoolean());
        properties = new CompactorProperties();
        properties.setWorkDir(COMPACTION_DIR);
        jobProcessor = spy(new RestageProcessor(fileSystem, new Lazy<>(() -> sparkSession), systemService, properties,
                reportFileWriter));
    }

    private void initMockSparkSession() {
        when(sparkSession.sparkContext()).thenReturn(sparkContext);
        when(sparkContext.isStopped()).thenReturn(false);
    }

    private void initMockDataframe() {
        when(dataFrame.union(any())).thenReturn(dataFrame);
        when(dataFrame.join(any(Dataset.class), any(Column.class), anyString())).thenReturn(dataFrame);
        when(dataFrame.columns()).thenReturn(new String[] { TIMESTAMP_FIELD });
        when(dataFrame.drop(anyString())).thenReturn(dataFrame);

        when(dataFrame.withColumn(eq(PARTITION_BY_HOUR_COLUMN), any(Column.class))).thenReturn(dataFrame);
        when(dataFrame.dropDuplicates(any(String[].class))).thenReturn(dataFrame);
        when(dataFrame.repartition(col(PARTITION_BY_HOUR_COLUMN))).thenReturn(dataFrame);
        when(dataFrame.sortWithinPartitions(anyString())).thenReturn(dataFrame);
        when(dataFrame.where(any(Column.class))).thenReturn(dataFrame);

        when(dataFrame.col(TIMESTAMP_FIELD)).thenReturn(col(TIMESTAMP_FIELD));
    }

    private void initMockFileReader() {
        when(sparkSession.read()).thenReturn(reader);
        when(reader.format(anyString())).thenReturn(reader);
        when(reader.load((String[]) any())).thenReturn(dataFrame);
    }

    private void initMockSystemService() {
        Condition<SystemSpecs> condition = SystemSpecs.suchThat().id().eq(SYSTEM_ID);
        when(systemService.findOne(argThat((Condition<SystemSpecs> cond) -> toRSQL(cond).equals(toRSQL(condition)))))
                .thenReturn(
                        Optional.of(
                                ReflectionUtils.builderInstance(SystemSpec.InnerBuilder.class).id(SYSTEM_ID)
                                        .name("CMW").entityKeyDefinitions(entityKeyDefSchema)
                                        .partitionKeyDefinitions(partitionKeyDefSchema)
                                        .timeKeyDefinitions(timeKeyDefSchema)
                                        .recordVersionKeyDefinitions(recordVersionKeyDefSchemaNullable).build()));
    }

    private List<URI> createFiles() {
        List<URI> files = new ArrayList<>();
        files.add(URI.create(file1));
        files.add(URI.create(file2));
        files.add(URI.create(file3));
        files.add(URI.create(file4));
        files.add(URI.create(file5));
        files.add(URI.create(file6));
        return files;
    }

    private List<URI> createDataFiles() {
        List<URI> files = new ArrayList<>();
        files.add(URI.create(outFile1));
        files.add(URI.create(outFile2));
        return files;
    }


    @Test
    public void testSparkContextClosed() {
        when(sparkSession.sparkContext()).thenReturn(sparkContext);
        when(sparkContext.isStopped()).thenReturn(true);
        assertThrows(SparkContextStoppedRuntimeException.class, () ->
                new RestageProcessor(fileSystem, new Lazy<>(() -> sparkSession), systemService, properties,
                        reportFileWriter)
                .process(jobData));
    }

    @Test
    public void shouldRethrowOnException() {
        initMockSparkSession();

        when(dataFrame.repartition(any(Column.class))).thenThrow(new RuntimeException());
        assertThrows(SparkCompactionRuntimeException.class, () -> jobProcessor.process(jobData));
    }

    @Test
    public void shouldThrowIfJobHasNoDestinationDir() throws IOException {
        RestageJob restageJob = RestageJob.builder()
                .sourceDir(URI.create(RESTAGING_DIR))
                .destinationDir(null)
                .files(createFiles())
                .restagingReportFile(RESTAGING_REPORT_FILE)
                .systemId(SYSTEM_ID)
                .build();
        assertThrows(IllegalArgumentException.class, () -> jobProcessor.process(restageJob));
    }

    @Test
    public void shouldNotAcceptEmptyJob() {
        jobData = RestageJob.builder()
                .systemId(SYSTEM_ID)
                .files(Collections.emptyList())
                .restagingReportFile(RESTAGING_REPORT_FILE)
                .build();
        assertThrows(IllegalArgumentException.class, () -> jobProcessor.process(jobData));
    }

    @Test
    public void shouldOnlyProduceEmptyReportWhenNoDataFiles() throws IOException {
        initMockSparkSession();

        Path restagePath = new Path(RESTAGING_REPORT_FILE);
        String expectedReportContent = "";

        jobData = RestageJob.builder()
                .systemId(SYSTEM_ID)
                .sourceDir(URI.create(RESTAGING_DIR))
                .destinationDir(URI.create(DESTINATION_DIR))
                .files(createFiles())
                .dataFiles(Collections.emptyList())
                .restagingReportFile(RESTAGING_REPORT_FILE)
                .build();

        doNothing().when(reportFileWriter).write(restagePath, expectedReportContent);

        ProcessingStatus status = jobProcessor.process(jobData);
        assertEquals(0, status.getErrorCount());

        verify(reportFileWriter, times(1)).write(new Path(jobData.getRestagingReportFile()),
                expectedReportContent);
    }

    @Test
    public void shouldRestageDataFiles() throws Exception {
        initMockSparkSession();
        initMockFileReader();
        initMockDataframe();
        initMockSystemService();

        Path restagePath = new Path(RESTAGING_REPORT_FILE);
        String content = jobData.getDataFiles().stream().map(Path::new).map(Path::toString)
                .collect(Collectors.joining(System.lineSeparator()));
        doNothing().when(reportFileWriter).write(restagePath, content);

        ProcessingStatus process = jobProcessor.process(jobData);
        assertEquals(0, process.getErrorCount());

        verify(reportFileWriter, times(1)).write(new Path(jobData.getRestagingReportFile()), content);
    }
}
