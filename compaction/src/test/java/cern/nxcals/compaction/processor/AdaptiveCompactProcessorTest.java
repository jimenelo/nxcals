/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.compaction.processor;

import cern.nxcals.api.domain.PartitionResource;
import cern.nxcals.api.domain.PartitionResourceHistory;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.TimeEntityPartitionType;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.extraction.metadata.queries.SystemSpecs;
import cern.nxcals.api.utils.TimeUtils;
import cern.nxcals.common.Constants;
import cern.nxcals.common.domain.AdaptiveCompactionJob;
import cern.nxcals.common.utils.HdfsFileUtils;
import cern.nxcals.common.utils.Lazy;
import cern.nxcals.common.utils.MathUtils;
import cern.nxcals.common.utils.ReflectionUtils;
import cern.nxcals.compaction.config.CompactorProperties;
import cern.nxcals.compaction.processor.writer.AdaptiveDatasetWriter;
import com.github.rutledgepaulv.qbuilders.conditions.Condition;
import com.google.common.collect.Comparators;
import lombok.SneakyThrows;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.apache.spark.unsafe.hash.Murmur3_x86_32;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.ArgumentCaptor;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;

import static cern.nxcals.common.SystemFields.NXC_ENTITY_BUCKET_COLUMN;
import static cern.nxcals.common.SystemFields.NXC_ENTITY_ID;
import static cern.nxcals.common.SystemFields.NXC_EXTR_VALUE;
import static cern.nxcals.common.SystemFields.NXC_TIME_PARTITION_COLUMN;
import static cern.nxcals.common.utils.RSQLUtils.toRSQL;
import static org.apache.spark.sql.types.DataTypes.DoubleType;
import static org.apache.spark.sql.types.DataTypes.IntegerType;
import static org.apache.spark.sql.types.DataTypes.LongType;
import static org.apache.spark.sql.types.DataTypes.StringType;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AdaptiveCompactProcessorTest extends BaseJobProcessorTest {

    private static final long SYSTEM_ID2 = 2L;
    private FileSystem fs;
    private final SparkSession sparkSession = SparkSession.builder()
            .master("local")
            .appName("adaptiveCompactionTest")
            .getOrCreate();
    private String tmpDir;
    private String outputDir;
    private String sourceDir;

    private List<FileStatus> inputFiles;
    private static final String ENTITY_ID = NXC_ENTITY_ID.getValue();
    private static final String VALUE = NXC_EXTR_VALUE.getValue();
    private static final StructType ROW_SCHEMA = rowSchema();

    private Dataset<Row> dataset;
    private Instant time;
    private AdaptiveCompactProcessor processor;

    private Dataset<Row> ds(SparkSession session, StructType schema, Object[]... data) {
        List<Row> rows = new ArrayList<>(data.length);
        for (Object[] datum : data) {
            rows.add(RowFactory.create(datum));
        }
        return session.createDataFrame(rows, schema);
    }

    private static StructType rowSchema() {
        StructField entityId = new StructField(ENTITY_ID, LongType, true, Metadata.empty());
        StructField timestamp = new StructField(TIMESTAMP_FIELD, LongType, true, Metadata.empty());
        StructField value = new StructField(VALUE, DoubleType, true, Metadata.empty());
        StructField recordVersion1 = new StructField(REC_VER1, StringType, true, Metadata.empty());
        StructField recordVersion2 = new StructField(REC_VER2, IntegerType, true, Metadata.empty());

        return new StructType(new StructField[] { entityId, timestamp, value, recordVersion1, recordVersion2 });
    }

    @BeforeEach
    public void setup() throws IOException {
        time = Instant.now().truncatedTo(ChronoUnit.DAYS);
        fs = FileSystem.getLocal(new Configuration(false));
        tmpDir = Files.createTempDirectory("tmpCompactionTest").toFile().getAbsolutePath();

        java.nio.file.Path path = Files.createDirectories(
                java.nio.file.Path.of(tmpDir + "/staging/ETL-1/2/2/2/2022-09-01"));
        outputDir = tmpDir + "/output";
        sourceDir = path.toString();

        dataset = getDataset();

        saveToAvro(dataset, "/1");
        saveToAvro(dataset, "/2"); //doubled to check if deduplication works

        inputFiles = HdfsFileUtils.findFilesInPath(fs, new Path(path.toUri()), HdfsFileUtils.SearchType.RECURSIVE,
                HdfsFileUtils::isAvroDataFile);
        assertEquals(2, inputFiles.size());
        CompactorProperties properties = new CompactorProperties();
        properties.setWorkDir(tmpDir);
        processor = new AdaptiveCompactProcessor(fs, new Lazy<>(() -> sparkSession), systemService,
                partitionResourceService, partitionResourceHistoryService, properties);

    }

    @AfterEach
    public void tearDown() throws IOException {
        fs.delete(new Path(tmpDir), true);
    }

    private void saveToAvro(Dataset<Row> dataset, String s) {
        dataset.repartition(1).write().format("avro").mode(SaveMode.Overwrite).save(sourceDir + s);
    }

    private void initMockSystemService() {
        Condition<SystemSpecs> condition = SystemSpecs.suchThat().id().eq(SYSTEM_ID2);
        when(systemService.findOne(argThat((Condition<SystemSpecs> cond) -> toRSQL(cond).equals(toRSQL(condition)))))
                .thenReturn(
                        Optional.of(
                                ReflectionUtils.builderInstance(SystemSpec.InnerBuilder.class).id(SYSTEM_ID2)
                                        .name("CMW")
                                        .entityKeyDefinitions(entityKeyDefSchema)
                                        .partitionKeyDefinitions(partitionKeyDefSchema)
                                        .timeKeyDefinitions(timeKeyDefSchema)
                                        .recordVersionKeyDefinitions(recordVersionKeyDefSchemaNullable).build()));
    }

    private Dataset<Row> getDataset() {
        Random rand = new Random();

        return ds(sparkSession, ROW_SCHEMA,
                new Object[] { 1L, TimeUtils.getNanosFromInstant(time.plus(1, ChronoUnit.SECONDS)), 10.0, "v1", 0 },
                new Object[] { 1L, TimeUtils.getNanosFromInstant(time.plus(17, ChronoUnit.HOURS)), 10.0, "v1", 0 },
                new Object[] { 1L, TimeUtils.getNanosFromInstant(time.plus(1, ChronoUnit.HOURS)), 10.0, "v1", 0 },
                new Object[] { 1L, TimeUtils.getNanosFromInstant(time.plus(6, ChronoUnit.HOURS)), 10.0, "v1", 0 },
                new Object[] { 1L, TimeUtils.getNanosFromInstant(time.plus(23, ChronoUnit.HOURS)), 10.0, "v1", 0 },
                new Object[] { 1L, TimeUtils.getNanosFromInstant(time.plus(13, ChronoUnit.HOURS)), 10.0, "v1", 0 },

                new Object[] { 100L, TimeUtils.getNanosFromInstant(time.plus(17, ChronoUnit.HOURS)), 10.0, "v1", 0 },
                new Object[] { 100L, TimeUtils.getNanosFromInstant(time.plus(6, ChronoUnit.HOURS)), 10.0, "v1", 0 },
                new Object[] { 100L, TimeUtils.getNanosFromInstant(time.plus(23, ChronoUnit.HOURS)), 10.0, "v1", 0 },
                new Object[] { 100L, TimeUtils.getNanosFromInstant(time.plus(1, ChronoUnit.HOURS)), 10.0, "v1", 0 },
                new Object[] { 100L, TimeUtils.getNanosFromInstant(
                        time.plus(1, ChronoUnit.HOURS).plus(rand.nextInt(100), ChronoUnit.SECONDS)), 11.0, "v1", 1 },
                new Object[] { 100L, TimeUtils.getNanosFromInstant(
                        time.plus(1, ChronoUnit.HOURS).plus(rand.nextInt(100), ChronoUnit.SECONDS)), 12.0, "v1", 2 },
                new Object[] { 100L, TimeUtils.getNanosFromInstant(
                        time.plus(1, ChronoUnit.HOURS).plus(rand.nextInt(100), ChronoUnit.SECONDS)), 13.0, "v1", 3 },
                new Object[] { 100L, TimeUtils.getNanosFromInstant(
                        time.plus(1, ChronoUnit.HOURS).plus(rand.nextInt(100), ChronoUnit.SECONDS)), 14.0, "v1", 4 },
                new Object[] { 100L, TimeUtils.getNanosFromInstant(
                        time.plus(1, ChronoUnit.HOURS).plus(rand.nextInt(100), ChronoUnit.SECONDS)), 15.0, "v1", 5 },
                new Object[] { 100L, TimeUtils.getNanosFromInstant(
                        time.plus(1, ChronoUnit.HOURS).plus(rand.nextInt(100), ChronoUnit.SECONDS)), 16.0, "v1", 6 },
                new Object[] { 100L, TimeUtils.getNanosFromInstant(
                        time.plus(1, ChronoUnit.HOURS).plus(rand.nextInt(100), ChronoUnit.SECONDS)), 17.0, "v1", 7 },
                new Object[] { 100L, TimeUtils.getNanosFromInstant(
                        time.plus(1, ChronoUnit.HOURS).plus(rand.nextInt(100), ChronoUnit.SECONDS)), 18.0, "v1", 8 },
                new Object[] { 100L, TimeUtils.getNanosFromInstant(
                        time.plus(1, ChronoUnit.HOURS).plus(rand.nextInt(100), ChronoUnit.SECONDS)), 19.0, "v1", 9 },
                new Object[] { 100L, TimeUtils.getNanosFromInstant(
                        time.plus(1, ChronoUnit.HOURS).plus(rand.nextInt(100), ChronoUnit.SECONDS)), 20.0, "v1", 10 },
                new Object[] { 100L, TimeUtils.getNanosFromInstant(
                        time.plus(1, ChronoUnit.HOURS).plus(rand.nextInt(100), ChronoUnit.SECONDS)), 21.0, "v1", 11 },
                new Object[] { 100L, TimeUtils.getNanosFromInstant(
                        time.plus(1, ChronoUnit.HOURS).plus(rand.nextInt(100), ChronoUnit.SECONDS)), 22.0, "v1", 12 },
                new Object[] { 100L, TimeUtils.getNanosFromInstant(time.plus(13, ChronoUnit.HOURS)), 10.0, "v1", 0 },

                new Object[] { 1009L, TimeUtils.getNanosFromInstant(time.plus(23, ChronoUnit.HOURS)), 10.0, "v1", 0 },
                new Object[] { 1009L, TimeUtils.getNanosFromInstant(time.plus(6, ChronoUnit.HOURS)), 10.0, "v1", 0 },
                new Object[] { 1009L, TimeUtils.getNanosFromInstant(time.plus(13, ChronoUnit.HOURS)), 10.0, "v1", 0 },
                new Object[] { 1009L, TimeUtils.getNanosFromInstant(time.plus(17, ChronoUnit.HOURS)), 10.0, "v1", 0 },
                new Object[] { 1009L, TimeUtils.getNanosFromInstant(time.plus(1, ChronoUnit.HOURS)), 10.0, "v1", 0 },

                new Object[] { 2005L, TimeUtils.getNanosFromInstant(time.plus(17, ChronoUnit.HOURS)), 10.0, "v1", 0 },
                new Object[] { 2005L, TimeUtils.getNanosFromInstant(time.plus(1, ChronoUnit.HOURS)), 10.0, "v1", 0 },
                new Object[] { 2005L, TimeUtils.getNanosFromInstant(time.plus(13, ChronoUnit.HOURS)), 10.0, "v1", 0 },
                new Object[] { 2005L, TimeUtils.getNanosFromInstant(time.plus(23, ChronoUnit.HOURS)), 10.0, "v1", 0 },
                new Object[] { 2005L, TimeUtils.getNanosFromInstant(time.plus(6, ChronoUnit.HOURS)), 10.0, "v1", 0 });

    }

    @Test
    void shouldCompactData() {
        //given
        initMockSystemService();
        AdaptiveCompactionJob job = getJob(2, 2, false);

        //when
        ProcessingStatus status = processor.process(job);

        //then
        assertEquals(0, status.getErrorCount());

        //are output files created?
        Set<String> filesInPath = HdfsFileUtils
                .findFilesInPath(fs, new Path(outputDir), HdfsFileUtils.SearchType.NONRECURSIVE,
                        HdfsFileUtils::isParquetDataFile)
                .stream().map(f -> f.getPath().getName())
                .collect(Collectors.toSet());

        assertEquals(4, filesInPath.size()); //2x2 partition x bucket
        assertTrue(filesInPath.stream().anyMatch(s -> s.startsWith(
                NXC_TIME_PARTITION_COLUMN.getValue() + "=0" + Constants.PART_FIELDS_SEPARATOR
                        + NXC_ENTITY_BUCKET_COLUMN.getValue() + "=0" + Constants.PART_FIELDS_SEPARATOR)));
        assertTrue(filesInPath.stream().anyMatch(s -> s.startsWith(
                NXC_TIME_PARTITION_COLUMN.getValue() + "=0" + Constants.PART_FIELDS_SEPARATOR
                        + NXC_ENTITY_BUCKET_COLUMN.getValue() + "=1" + Constants.PART_FIELDS_SEPARATOR)));
        assertTrue(filesInPath.stream().anyMatch(s -> s.startsWith(
                NXC_TIME_PARTITION_COLUMN.getValue() + "=1" + Constants.PART_FIELDS_SEPARATOR
                        + NXC_ENTITY_BUCKET_COLUMN.getValue() + "=0" + Constants.PART_FIELDS_SEPARATOR)));
        assertTrue(filesInPath.stream().anyMatch(s -> s.startsWith(
                NXC_TIME_PARTITION_COLUMN.getValue() + "=1" + Constants.PART_FIELDS_SEPARATOR
                        + NXC_ENTITY_BUCKET_COLUMN.getValue() + "=1" + Constants.PART_FIELDS_SEPARATOR)));

        //check if it is deduplicated?
        assertEquals(dataset.count(), sparkSession.read().parquet(outputDir).count());

        //are input files deleted?
        assertEquals(0, HdfsFileUtils.findFilesInPath(fs, new Path(sourceDir), HdfsFileUtils.SearchType.RECURSIVE,
                HdfsFileUtils::isAvroDataFile).size());

    }

    private AdaptiveCompactionJob getJob(int timeParts, int entBuckets, boolean sorted) {
        return AdaptiveCompactionJob.builder()
                .destinationDir(URI.create(outputDir))
                .files(inputFiles.stream().map(f -> f.getPath().toUri()).collect(Collectors.toList()))
                .systemId(SYSTEM_ID2)
                .filePrefix("")
                .sourceDir(URI.create(sourceDir))
                .partitionType(TimeEntityPartitionType.of(timeParts, entBuckets))
                .sortEnabled(sorted)
                .build();
    }

    @Test
    void shouldSplitCorrectlyByTime24Buckets() {
        initMockSystemService();
        AdaptiveCompactionJob job = getJob(24, 1, false);

        //when
        ProcessingStatus status = processor.process(job);

        //then
        assertEquals(0, status.getErrorCount());

        //testing if the data is located in the correct files.
        assertEquals(1, readTimePartition(0).where(
                ENTITY_ID + "=1 and " + TIMESTAMP_FIELD + "=" + TimeUtils.getNanosFromInstant(
                        time.plus(1, ChronoUnit.SECONDS))).count());
        assertEquals(1, readTimePartition(1).where(ENTITY_ID + "=1 and " + TIMESTAMP_FIELD + "=" + TimeUtils
                .getNanosFromInstant(time.plus(1, ChronoUnit.HOURS))).count());
        assertEquals(1, readTimePartition(23).where(ENTITY_ID + "=100 and " + TIMESTAMP_FIELD + "=" + TimeUtils
                .getNanosFromInstant(time.plus(23, ChronoUnit.HOURS))).count());

        assertEquals(1, readTimePartition(17).where(ENTITY_ID + "=2005 and " + TIMESTAMP_FIELD + "=" + TimeUtils
                .getNanosFromInstant(time.plus(17, ChronoUnit.HOURS))).count());
        assertEquals(1, readTimePartition(1).where(ENTITY_ID + "=2005 and " + TIMESTAMP_FIELD + "=" + TimeUtils
                .getNanosFromInstant(time.plus(1, ChronoUnit.HOURS))).count());
        assertEquals(1, readTimePartition(13).where(ENTITY_ID + "=2005 and " + TIMESTAMP_FIELD + "=" + TimeUtils
                .getNanosFromInstant(time.plus(13, ChronoUnit.HOURS))).count());
        assertEquals(1, readTimePartition(23).where(ENTITY_ID + "=2005 and " + TIMESTAMP_FIELD + "=" + TimeUtils
                .getNanosFromInstant(time.plus(23, ChronoUnit.HOURS))).count());
        assertEquals(1, readTimePartition(6).where(ENTITY_ID + "=2005 and " + TIMESTAMP_FIELD + "=" + TimeUtils
                .getNanosFromInstant(time.plus(6, ChronoUnit.HOURS))).count());

    }

    @Test
    void shouldNotExecuteJobWithNonExistentFiles() {
        AdaptiveCompactionJob job = getJob(48, 1, false);
        job.getFiles().add(URI.create("/file/not/there.avro"));

        //when
        ProcessingStatus status = processor.process(job);

        assertEquals(0, status.getErrorCount());
        assertEquals(0, status.getByteCount());
        assertEquals(0, status.getFileCount());
    }

    @Test
    void shouldSplitCorrectlyByTime48Buckets() {
        initMockSystemService();
        AdaptiveCompactionJob job = getJob(48, 1, false);

        //when
        ProcessingStatus status = processor.process(job);

        //then
        assertEquals(0, status.getErrorCount());

        //testing if the data is located in the correct files.
        assertEquals(1, readTimePartition(0).where(ENTITY_ID + "=1 and " + TIMESTAMP_FIELD + "=" + TimeUtils
                .getNanosFromInstant(time.plus(1, ChronoUnit.SECONDS))).count());
        assertEquals(1, readTimePartition(2).where(ENTITY_ID + "=1 and " + TIMESTAMP_FIELD + "=" + TimeUtils
                .getNanosFromInstant(time.plus(1, ChronoUnit.HOURS))).count());
        assertEquals(1, readTimePartition(46).where(ENTITY_ID + "=100 and " + TIMESTAMP_FIELD + "=" + TimeUtils
                .getNanosFromInstant(time.plus(23, ChronoUnit.HOURS))).count());

        assertEquals(1, readTimePartition(34).where(ENTITY_ID + "=2005 and " + TIMESTAMP_FIELD + "=" + TimeUtils
                .getNanosFromInstant(time.plus(17, ChronoUnit.HOURS))).count());
        assertEquals(1, readTimePartition(2).where(ENTITY_ID + "=2005 and " + TIMESTAMP_FIELD + "=" + TimeUtils
                .getNanosFromInstant(time.plus(1, ChronoUnit.HOURS))).count());
        assertEquals(1, readTimePartition(26).where(ENTITY_ID + "=2005 and " + TIMESTAMP_FIELD + "=" + TimeUtils
                .getNanosFromInstant(time.plus(13, ChronoUnit.HOURS))).count());
        assertEquals(1, readTimePartition(46).where(ENTITY_ID + "=2005 and " + TIMESTAMP_FIELD + "=" + TimeUtils
                .getNanosFromInstant(time.plus(23, ChronoUnit.HOURS))).count());
        assertEquals(1, readTimePartition(12).where(ENTITY_ID + "=2005 and " + TIMESTAMP_FIELD + "=" + TimeUtils
                .getNanosFromInstant(time.plus(6, ChronoUnit.HOURS))).count());

    }

    @Test
    void shouldSplitCorrectlyByTime2Buckets() {
        initMockSystemService();
        AdaptiveCompactionJob job = getJob(2, 1, false);

        //when
        ProcessingStatus status = processor.process(job);

        //then
        assertEquals(0, status.getErrorCount());

        //testing if the data is located in the correct files.
        assertEquals(1, readTimePartition(0).where(ENTITY_ID + "=1 and " + TIMESTAMP_FIELD + "=" + TimeUtils
                .getNanosFromInstant(time.plus(1, ChronoUnit.SECONDS))).count());
        assertEquals(1, readTimePartition(0).where(ENTITY_ID + "=1 and " + TIMESTAMP_FIELD + "=" + TimeUtils
                .getNanosFromInstant(time.plus(1, ChronoUnit.HOURS))).count());
        assertEquals(1, readTimePartition(1).where(ENTITY_ID + "=100 and " + TIMESTAMP_FIELD + "=" + TimeUtils
                .getNanosFromInstant(time.plus(23, ChronoUnit.HOURS))).count());

        assertEquals(1, readTimePartition(1).where(ENTITY_ID + "=2005 and " + TIMESTAMP_FIELD + "=" + TimeUtils
                .getNanosFromInstant(time.plus(17, ChronoUnit.HOURS))).count());
        assertEquals(1, readTimePartition(0).where(ENTITY_ID + "=2005 and " + TIMESTAMP_FIELD + "=" + TimeUtils
                .getNanosFromInstant(time.plus(1, ChronoUnit.HOURS))).count());
        assertEquals(1, readTimePartition(1).where(ENTITY_ID + "=2005 and " + TIMESTAMP_FIELD + "=" + TimeUtils
                .getNanosFromInstant(time.plus(13, ChronoUnit.HOURS))).count());
        assertEquals(1, readTimePartition(1).where(ENTITY_ID + "=2005 and " + TIMESTAMP_FIELD + "=" + TimeUtils
                .getNanosFromInstant(time.plus(23, ChronoUnit.HOURS))).count());
        assertEquals(1, readTimePartition(0).where(ENTITY_ID + "=2005 and " + TIMESTAMP_FIELD + "=" + TimeUtils
                .getNanosFromInstant(time.plus(6, ChronoUnit.HOURS))).count());

    }

    Dataset<Row> readTimePartition(int part) {
        return sparkSession.read().parquet(
                outputDir + "/" + NXC_TIME_PARTITION_COLUMN.getValue() + "=" + part + Constants.PART_FIELDS_SEPARATOR
                        + NXC_ENTITY_BUCKET_COLUMN.getValue() + "=0*");
    }

    Dataset<Row> readEntityBucket(int bucket) {
        return sparkSession.read().parquet(
                outputDir + "/" + NXC_TIME_PARTITION_COLUMN.getValue() + "=0" + Constants.PART_FIELDS_SEPARATOR
                        + NXC_ENTITY_BUCKET_COLUMN.getValue() + "=" + bucket + Constants.PART_FIELDS_SEPARATOR + "*");
    }

    private static int getBucket(long val, long nbOfBuckets) {
        //42 is used by Spark as seed...
        return (int) MathUtils.pmod(Murmur3_x86_32.hashLong(val, 42), nbOfBuckets);
    }

    @ParameterizedTest
    @ValueSource(ints = { 1, 2, 3, 6, 10 })
    void shouldSplitCorrectlyByEntityBucket(int buckets) {
        initMockSystemService();

        AdaptiveCompactionJob job = getJob(1, buckets, false);

        //when
        ProcessingStatus status = processor.process(job);

        //then
        assertEquals(0, status.getErrorCount());

        //testing if the data is located in the correct files.
        assertEntityBucket(1L, buckets);
        assertEntityBucket(100L, buckets);
        assertEntityBucket(1009L, buckets);
        assertEntityBucket(2005L, buckets);

    }

    private void assertEntityBucket(long entityId, int buckets) {
        String cond = ENTITY_ID + "=" + entityId;
        assertEquals(dataset.where(cond).count(), readEntityBucket(getBucket(entityId, buckets)).where(cond).count());
    }

    @ParameterizedTest
    @ValueSource(booleans = { true, false })
    void shouldDataBeSorted(boolean sorted) {
        //given
        initMockSystemService();
        AdaptiveCompactionJob job = getJob(2, 2, sorted);

        //when
        ProcessingStatus status = processor.process(job);

        //then
        assertEquals(0, status.getErrorCount());

        //is the file sorted
        List<Long> longs = sparkSession.read().parquet(
                        outputDir + "/" + NXC_TIME_PARTITION_COLUMN.getValue() + "=0" + Constants.PART_FIELDS_SEPARATOR
                                + NXC_ENTITY_BUCKET_COLUMN.getValue() + "=0*")
                .select(TIMESTAMP_FIELD)
                .collectAsList()
                .stream()
                .map(r -> r.getLong(0))
                .collect(Collectors.toList());

        assertEquals(sorted, Comparators.isInOrder(longs, Comparator.<Long>naturalOrder()));

    }

    @Test
    void shouldNotAcceptEmptyJob() {

        AdaptiveCompactionJob job = AdaptiveCompactionJob.builder()
                .files(Collections.emptyList())
                .destinationDir(URI.create(DESTINATION_DIR))
                .systemId(SYSTEM_ID)
                .filePrefix("")
                .sourceDir(new Path("/source").toUri())
                .partitionType(TimeEntityPartitionType.of(1, 1))
                .sortEnabled(true)
                .build();

        assertThrows(IllegalArgumentException.class, () -> processor.process(job));
    }

    @SneakyThrows
    @Test
    void shouldHandleException() {
        AdaptiveCompactionJob job = getJob(2, 2, false);
        AdaptiveDatasetWriter mockWriter = mock(AdaptiveDatasetWriter.class);
        processor.setWriter(mockWriter);
        doThrow(new IOException("Test")).when(mockWriter).saveDataset(any(), any(), any());
        PartitionResource partitionResource = mock(PartitionResource.class);

        PartitionResourceHistory partitionResourceHistory = PartitionResourceHistory.builder()
                .partitionResource(partitionResource)
                .validity(TimeWindow.empty())
                .partitionInformation(job.getPartitionType().toString())
                .compactionType("daily").storageType("hdfs")
                .build();
        when(partitionResourceService.findOne(any())).thenReturn(Optional.of(partitionResource));
        when(partitionResourceHistoryService.findOne(any())).thenReturn(Optional.of(partitionResourceHistory));

        //when
        ProcessingStatus status = processor.process(job);

        //then
        assertTrue(status.isSuccess());

        ArgumentCaptor<PartitionResourceHistory> valueCapture = ArgumentCaptor.forClass(PartitionResourceHistory.class);

        verify(partitionResourceHistoryService).update(valueCapture.capture());
        assertEquals(
                TimeEntityPartitionType.next(job.getPartitionType(), 2).toString(),
                valueCapture.getValue().getPartitionInformation());

    }

}
