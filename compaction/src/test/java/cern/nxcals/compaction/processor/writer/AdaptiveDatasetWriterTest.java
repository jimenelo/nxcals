package cern.nxcals.compaction.processor.writer;

import cern.nxcals.api.custom.domain.CmwSystemConstants;
import cern.nxcals.api.domain.TimeEntityPartitionType;
import org.apache.hadoop.fs.Path;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.DataFrameWriter;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

import static cern.nxcals.compaction.processor.writer.AdaptiveDatasetWriter.ENTITY_BUCKET_COLUMN;
import static cern.nxcals.compaction.processor.writer.AdaptiveDatasetWriter.TIME_PARTITION_COLUMN;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class AdaptiveDatasetWriterTest extends DatasetWriterTest {
    private AdaptiveDatasetWriter writer;
    DataFrameWriter dfWriter = mock(DataFrameWriter.class, Answers.RETURNS_DEEP_STUBS);

    private static AdaptiveDatasetWriterJob getJob(boolean withSorting) {
        return AdaptiveDatasetWriterJob.builder()
                .systemId(SYSTEM_ID)
                .sortEnabled(withSorting)
                .partitionType(TimeEntityPartitionType.of(1, 2))
                .build();
    }

    @AfterEach
    public void tearDown() {
        reset(fileSystem, dataFrame);
    }

    @BeforeEach
    public void setup() {
        initMockDataframe();
        writer = spy(new AdaptiveDatasetWriter(deduplicateCache, timestampFieldCache, fileSystem));
    }

    private void initMockDataframe() {
        when(dataFrame.union(any())).thenReturn(dataFrame);
        when(dataFrame.dropDuplicates(any(String[].class))).thenReturn(dataFrame);
        when(dataFrame.repartition(anyInt())).thenReturn(dataFrame);
        when(dataFrame.sortWithinPartitions(any(String.class), any(String.class), any(String.class))).thenReturn(
                dataFrame);
        when(dataFrame.withColumn(any(), any())).thenReturn(dataFrame);
        when(dataFrame.repartitionByRange(anyInt(), any(), any())).thenReturn(dataFrame);
        when(dataFrame.write()).thenReturn(dfWriter);
        when(dfWriter.mode(any(SaveMode.class))).thenReturn(dfWriter);
        when(dfWriter.option(anyString(), anyLong())).thenReturn(dfWriter);
        when(dfWriter.partitionBy(anyString(), anyString())).thenReturn(dfWriter);
    }

    @Test
    public void shouldSortWhenNeeded() throws Exception {
        AdaptiveDatasetWriterJob job = getJob(true);
        writer.saveDataset(mock(Path.class), dataFrame, job);
        verify(dataFrame, times(1)).sortWithinPartitions(TIME_PARTITION_COLUMN, ENTITY_BUCKET_COLUMN,
                CmwSystemConstants.RECORD_TIMESTAMP);
    }

    @Test
    public void shouldNotSortWhenNotNeeded() throws Exception {
        writer.saveDataset(mock(Path.class), dataFrame, getJob(false));
        verify(dataFrame, times(0)).sortWithinPartitions(anyString(), anyString(), anyString());
    }

    @Test
    public void shouldRepartition() throws Exception {
        writer.saveDataset(mock(Path.class), dataFrame, getJob(false));
        verify(dataFrame, times(1)).repartitionByRange(anyInt(), any(Column.class), any(Column.class));
    }

    @Test
    public void shouldWriteParquet() throws Exception {
        writer.saveDataset(mock(Path.class), dataFrame, getJob(false));
        verify(dataFrame, times(1)).write();
        verify(dfWriter, times(1)).parquet(anyString());
    }

    @Test
    void shouldPerformMergeCompactionOnProperEntityAndAppendNewRows() throws IOException {
        // given
        Dataset<Row> dataDs = createOriginalDataset();
        Dataset<Row> expectedDs = createExpectedMergedDataset();
        // when
        Dataset<Row> deduplicateDataset = writer.deduplicateDataset(SYSTEM_ID, dataDs);
        Dataset<Row> repartitionedDataSet = writer.repartitionByTimeAndEntityBucket(getJob(true), deduplicateDataset);

        // then
        assertAll(
                () -> assertTrue(toSet(repartitionedDataSet.columns()).containsAll(toSet(expectedDs.columns()))),
                () -> assertEquals(6, repartitionedDataSet.columns().length),
                () -> assertEquals(expectedDs.count(), repartitionedDataSet.count()),
                () -> assertEquals(Set.of(0, 1),
                        repartitionedDataSet.select(ENTITY_BUCKET_COLUMN).collectAsList().stream()
                                .map(x -> x.getInt(0))
                                .collect(Collectors.toSet())), // 2 entities buckets
                () -> assertEquals(Set.of(0L),
                        repartitionedDataSet.select(TIME_PARTITION_COLUMN).collectAsList().stream()
                                .map(x -> x.getLong(0))
                                .collect(Collectors.toSet())) // 1 time bucket
        );
    }

    private <T> Set<T> toSet(T[] array) {
        return Arrays.stream(array).collect(Collectors.toSet());
    }

}
