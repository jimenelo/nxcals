package cern.nxcals.compaction.processor.writer;

import cern.nxcals.api.custom.domain.CmwSystemConstants;
import cern.nxcals.common.SystemFields;
import cern.nxcals.compaction.processor.BaseJobProcessorTest;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import org.apache.hadoop.fs.Path;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.apache.spark.sql.types.DataTypes.DoubleType;
import static org.apache.spark.sql.types.DataTypes.LongType;
import static org.apache.spark.sql.types.DataTypes.StringType;
import static org.apache.spark.sql.types.DataTypes.createStructField;
import static org.apache.spark.sql.types.DataTypes.createStructType;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class DatasetWriterTest extends BaseJobProcessorTest {
    protected static final SparkSession spark = SparkSession.builder()
            .master("local")
            .appName("merge_compact_test")
            .getOrCreate();

    protected static final LoadingCache<Long, Set<String>> deduplicateCache = Caffeine.newBuilder()
            .maximumSize(1)
            .build((x) -> Set.of(CmwSystemConstants.RECORD_TIMESTAMP, SystemFields.NXC_ENTITY_ID.getValue()));
    protected static final LoadingCache<Long, String> timestampFieldCache = Caffeine.newBuilder()
            .maximumSize(1)
            .build((x) -> CmwSystemConstants.RECORD_TIMESTAMP);

    static class TestDatasetWriter extends DatasetWriter<DatasetWriterJob> {

        public TestDatasetWriter(LoadingCache<Long, Set<String>> deduplicateCache) {
            super(deduplicateCache);
        }

        @Override
        public void saveDataset(Path operationPath, Dataset<Row> dataset, DatasetWriterJob job) throws IOException {
            // pass
        }
    }

    @Test
    void shouldDeduplicateDataset() {
        // given
        TestDatasetWriter writer = new TestDatasetWriter(deduplicateCache);
        Dataset<Row> dataDs = createOriginalDataset();

        // when
        Dataset<Row> deduplicatedDs = writer.deduplicateDataset(SYSTEM_ID, dataDs);

        // then
        Dataset<Row> expectedDs = createExpectedMergedDataset();

        Dataset<Row> nonMatchingRows = expectedDs.except(deduplicatedDs);
        assertTrue(nonMatchingRows.isEmpty());
        assertEquals(expectedDs.count(), deduplicatedDs.count());
    }

    // helper methods

    protected Dataset<Row> createOriginalDataset() {
        List<List<Object>> rows = Arrays.asList(
                Arrays.asList(1L, 1L, "value_old", null),   // record_1_1
                Arrays.asList(1L, 2L, "value_new", 1.5D),   // record_2_1
                Arrays.asList(1L, 2L, null, 1.5D),          // duplicate of record_2_1
                Arrays.asList(2L, 2L, null, 5.0D),          // record_2_4
                Arrays.asList(3L, 3L, "value_a", 2.0D),     // record_3_1
                Arrays.asList(3L, 5L, "value", 2.5D),
                Arrays.asList(5L, 1L, "value_old", null),   // record_1_2
                Arrays.asList(15L, 6L, "value", 2.5D),
                Arrays.asList(5L, 1L, "value_old", null),   // duplicate
                Arrays.asList(10L, 2L, "value_old", 2.0D),
                Arrays.asList(10L, 2L, "value_old", null),  // record_2_2
                Arrays.asList(10L, 4L, "value_b", 2.0D),     // record_4_1
                Arrays.asList(5L, 2L, "value_new", 2.0D)
        );
        return createDataset(rows);
    }

    protected Dataset<Row> createExpectedMergedDataset() {
        List<List<Object>> rows = Arrays.asList(
                Arrays.asList(1L, 1L, "value_old", null),     // record_1_1
                Arrays.asList(1L, 2L, "value_new", 1.5D),     // updated record_2_1
                Arrays.asList(2L, 2L, null, 5.0D),            // new record_2_4
                Arrays.asList(3L, 3L, "value_a", 2.0D),       // record_3_1
                Arrays.asList(3L, 5L, "value", 2.5D),         // new record_5_1
                Arrays.asList(5L, 1L, "value_old", null),     // record_1_2
                Arrays.asList(5L, 2L, "value_new", 2.0D),     // new record_2_3
                Arrays.asList(10L, 2L, "value_old", 2.0D),    // updated record_2_2
                Arrays.asList(10L, 4L, "value_b", 2.0D),      // record_4_1
                Arrays.asList(15L, 6L, "value", 2.5D)         // new record_6_1
        );
        return createDataset(rows);
    }

    protected Dataset<Row> createDataset(List<List<Object>> rows) {
        List<Row> dsRows = rows.stream().map(r -> RowFactory.create(r.toArray(new Object[0])))
                .collect(Collectors.toList());
        StructType schema = createStructType(new StructField[] {
                createStructField(CmwSystemConstants.RECORD_TIMESTAMP, LongType, false),
                createStructField(SystemFields.NXC_ENTITY_ID.getValue(), LongType, false),
                createStructField("fieldA", StringType, true),
                createStructField("fieldB", DoubleType, true)
        });
        return spark.sqlContext().createDataFrame(dsRows, schema);
    }
}
