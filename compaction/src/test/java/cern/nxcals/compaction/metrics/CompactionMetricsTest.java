package cern.nxcals.compaction.metrics;

import cern.nxcals.common.metrics.MetricsRegistry;
import cern.nxcals.common.metrics.MicrometerMetricsRegistry;
import cern.nxcals.compaction.processor.ProcessingStatus;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.simple.SimpleMeterRegistry;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static cern.nxcals.compaction.metrics.CompactionMetrics.COMPACTION_BYTE_COUNT_METRIC_NAME;
import static cern.nxcals.compaction.metrics.CompactionMetrics.COMPACTION_ERROR_COUNT_METRIC_NAME;
import static cern.nxcals.compaction.metrics.CompactionMetrics.COMPACTION_FILE_COUNT_METRIC_NAME;
import static cern.nxcals.compaction.metrics.CompactionMetrics.COMPACTION_HEARTBEAT_COUNT_METRIC_NAME;
import static cern.nxcals.compaction.metrics.CompactionMetrics.COMPACTION_TASK_COUNT_METRIC_NAME;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class CompactionMetricsTest {

    private MeterRegistry meterRegistry;
    private MetricsRegistry metricsRegistry;

    @BeforeEach
    public void setUp() {
        meterRegistry = new SimpleMeterRegistry();
        metricsRegistry = new MicrometerMetricsRegistry(meterRegistry);
    }

    @Test
    public void testThrowExceptionOnExportWhenMeterRegistryIsNull() {
        assertThrows(NullPointerException.class,
                () -> new CompactionMetrics(null).export(ProcessingStatus.nothing()),
                "CompactionMetrics should deny creation of instance with null MetricsRegistry!"
        );
    }

    @Test
    public void testThrowExceptionOnExportWhenProcessingStatusIsNull() {
        assertThrows(NullPointerException.class,
                () -> new CompactionMetrics(metricsRegistry).export(null),
                "CompactionMetrics should not allow exporting metrics from null compaction status!"
        );
    }

    @Test
    public void testInitExportMetricsDefaultAsZero() {
        new CompactionMetrics(metricsRegistry).export(ProcessingStatus.nothing());

        assertEquals(0.0, meterRegistry.get(COMPACTION_FILE_COUNT_METRIC_NAME).gauge().value(),0);
        assertEquals(0.0, meterRegistry.get(COMPACTION_BYTE_COUNT_METRIC_NAME).gauge().value(),0);
        assertEquals(0.0, meterRegistry.get(COMPACTION_TASK_COUNT_METRIC_NAME).gauge().value(),0);
        assertEquals(0.0, meterRegistry.get(COMPACTION_ERROR_COUNT_METRIC_NAME).gauge().value(),0);
    }

    @Test
    public void testExportMetrics() {
        long processedFiles = 100;
        long processedBytes = Long.MAX_VALUE / 2;

        new CompactionMetrics(metricsRegistry).export(ProcessingStatus.success(processedFiles, processedBytes));

        assertEquals(processedFiles , meterRegistry.get(COMPACTION_FILE_COUNT_METRIC_NAME).gauge().value(),0);
        assertEquals(processedBytes, meterRegistry.get(COMPACTION_BYTE_COUNT_METRIC_NAME).gauge().value(),0);
        assertEquals(1.0, meterRegistry.get(COMPACTION_TASK_COUNT_METRIC_NAME).gauge().value(),0);
        assertEquals(0.0, meterRegistry.get(COMPACTION_ERROR_COUNT_METRIC_NAME).gauge().value(),0);
    }

    @Test
    public void testExportRunAttempt() {
        new CompactionMetrics(metricsRegistry).heartbeat();
        assertEquals(1,meterRegistry.get(COMPACTION_HEARTBEAT_COUNT_METRIC_NAME).counter().count(), 0.001d);

        new CompactionMetrics(metricsRegistry).heartbeat();
        assertEquals(2,meterRegistry.get(COMPACTION_HEARTBEAT_COUNT_METRIC_NAME).counter().count(), 0.001d);
    }
}
