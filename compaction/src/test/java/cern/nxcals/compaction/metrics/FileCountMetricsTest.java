package cern.nxcals.compaction.metrics;

import cern.nxcals.common.metrics.MetricsRegistry;
import cern.nxcals.common.metrics.MicrometerMetricsRegistry;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.simple.SimpleMeterRegistry;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Arrays;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FileCountMetricsTest {

    private static final String HDFS_ROOT = "../fake_hdfs/file-count-metrics/";
    private static final String METRIC_PREFIX = "prefix";
    private static FileSystem fs;

    private MeterRegistry meterRegistry;
    private MetricsRegistry metricsRegistry;

    @BeforeAll
    public static void preInit() throws IOException {
        fs = FileSystem.getLocal(new Configuration(false));
        fs.setWorkingDirectory(new Path(HDFS_ROOT));
    }

    @BeforeEach
    public void setUp() throws Exception {
        meterRegistry = new SimpleMeterRegistry();
        metricsRegistry = new MicrometerMetricsRegistry(meterRegistry);
    }

    @Test
    public void shouldConsumeIOException() {
        createFileCountMetrics("absent").publish();
    }

    @Test
    public void shouldSanitizePath() {
        createFileCountMetrics("DIR2/DIR2-2").publish();

        assertEquals(1.0, meterRegistry.get(METRIC_PREFIX + "_DIR2/DIR2-2").gauge().value(), 0.1);
    }

    @Test
    public void shouldCountFiles() {
        createFileCountMetrics("DIR1").publish();

        assertEquals(2.0, meterRegistry.get(METRIC_PREFIX + "_DIR1").gauge().value(), 0.1);
    }

    @Test
    public void shouldCountFilesInMultipleDirs() {
        createFileCountMetrics("DIR1", "DIR2").publish();

        assertEquals(2.0, meterRegistry.get(METRIC_PREFIX + "_DIR1").gauge().value(), 0.1);
        assertEquals(2.0, meterRegistry.get(METRIC_PREFIX + "_DIR2").gauge().value(), 0.1);
    }

    private FileCountMetrics createFileCountMetrics(String... directory) {
        return new FileCountMetrics(metricsRegistry, fs, Arrays.stream(directory).collect(Collectors.toMap(Function.identity(), Function.identity())), METRIC_PREFIX);
    }
}