package cern.nxcals.compaction;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by jwozniak on 16/11/16.
 */
public class ScrapMain {
    public static void main(String[] args) {
        //String value = "metrics<name=application_1478903457920_7052.driver.CodeGenerator.sourceCodeSize><>Mean";
        //String pattern = "metrics<name=application_([0-9]+)_([0-9]+)\\.(.*)><>(.*)";
        String value = "oracle.ucp.admin.UniversalConnectionPoolMBean<name=UniversalConnectionPoolManager(528012309)-5770486603498074109-2-cs-ccr-nxcalsdev1.cern.ch><>availableConnectionsCount";
        String pattern = "oracle(.*)<name=(.*)\\(([0-9]+)\\)(.*)><>(.*)";
        //"    name: oracle_$1_$2_$5\n";

        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(value);
        System.out.println("Matches: " + m.matches());
        System.out.println(m.group(1));
        System.out.println(m.group(2));
        System.out.println(m.group(5));
    }
}
