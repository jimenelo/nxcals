package cern.nxcals.compaction.service;

import cern.nxcals.common.domain.CompactionJob;
import cern.nxcals.common.domain.DataProcessingJob;
import cern.nxcals.common.domain.DataProcessingJob.JobType;
import cern.nxcals.common.domain.MergeCompactionJob;
import cern.nxcals.common.domain.RestageJob;
import cern.nxcals.common.metrics.MetricsRegistry;
import cern.nxcals.common.metrics.MicrometerMetricsRegistry;
import cern.nxcals.common.utils.Lazy;
import cern.nxcals.compaction.config.CompactorProperties;
import cern.nxcals.compaction.processor.JobProcessor;
import cern.nxcals.compaction.processor.ProcessingStatus;
import cern.nxcals.internal.extraction.metadata.InternalCompactionService;
import io.micrometer.core.instrument.simple.SimpleMeterRegistry;
import org.apache.spark.SparkContext;
import org.apache.spark.sql.SparkSession;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.net.URI;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Supplier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by jwozniak on 21/10/16.
 */
@ExtendWith(MockitoExtension.class)
public class CompactorTest {

    @Mock
    private JobProcessor<CompactionJob> compactionJobProcessor;

    @Mock
    private JobProcessor<RestageJob> restageJobProcessor;

    @Mock
    private JobProcessor<MergeCompactionJob> mergeCompactionJobProcessor;

    @Mock
    private SparkSession sparkSession;

    @Mock
    private SparkContext sparkContext;


    @Mock
    private InternalCompactionService compactionService;

    @Mock
    private Runnable exitHandler;

    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
    private final Executor executor = Runnable::run;
    private final MetricsRegistry metricsRegistry = new MicrometerMetricsRegistry(new SimpleMeterRegistry());
    private final LinkedBlockingQueue<Runnable> queue = new LinkedBlockingQueue<>();

    private final AtomicInteger count = new AtomicInteger(1);
    private CompactorProperties properties = new CompactorProperties();

    private void initMockCompactionJobs() {
        List<DataProcessingJob> compactionJobs = new ArrayList<>();
        compactionJobs.add(CompactionJob.builder().sourceDir(URI.create("/dir1/1/2/3/2024-01-01")).build());
        compactionJobs.add(CompactionJob.builder().sourceDir(URI.create("/dir2/1/2/4/2024-01-01")).build());
        compactionJobs.add(CompactionJob.builder().sourceDir(URI.create("/dir3/1/2/5/2024-01-01")).build());
        when(compactionService.getDataProcessJobs(anyInt(), eq(JobType.COMPACT))).thenReturn(compactionJobs);
    }

    @BeforeEach
    public void setup() {
        properties.setMaxJobs(1000);
    }
    @Test
    public void shouldRunExitHandlerWhenSparkContextIsStopped() {
        //given
        when(compactionJobProcessor.getJobType()).thenReturn(JobType.COMPACT);
        Compactor compactor = new Compactor(Collections.singletonList(compactionJobProcessor), executor, queue,
                scheduler,
                metricsRegistry, new Lazy<>(() -> sparkSession), compactionService, properties);

        compactor.setExitHandler(exitHandler);

        when(sparkSession.sparkContext()).thenReturn(sparkContext);
        when(sparkContext.isStopped()).thenReturn(true);

        //We just throw exception here, meaning that this handler was run.
        doThrow(new UnknownError("Test exception")).when(exitHandler).run();

        //when: should throw UnknownError from exitHandler
        assertThrows(UnknownError.class, compactor::run);
    }

    @Test
    public void shouldRunExitHandlerWhenExceptionOccurred() {
        //given
        when(compactionJobProcessor.getJobType()).thenReturn(JobType.COMPACT);
        Compactor compactor = new Compactor(Collections.singletonList(compactionJobProcessor), executor, queue,
                scheduler,
                metricsRegistry, new Lazy<>(() -> sparkSession), compactionService, properties);

        compactor.setExitHandler(exitHandler);

        when(sparkSession.sparkContext()).thenReturn(sparkContext);
        when(sparkContext.isStopped()).thenThrow(new NullPointerException("Test"));

        //We just throw exception here, meaning that this handler was run.
        doThrow(new UnknownError("Test exception")).when(exitHandler).run();

        //when: should throw UnknownError from exitHandler
        assertThrows(UnknownError.class, compactor::run);

    }

    @Test
    public void runSuccessfulCompaction() {
        //given
        initMockCompactionJobs();
        when(compactionJobProcessor.getJobType()).thenReturn(JobType.COMPACT);
        when(restageJobProcessor.getJobType()).thenReturn(JobType.RESTAGE);

        when(compactionJobProcessor.process(any())).thenReturn(ProcessingStatus.success(1, 100),
                ProcessingStatus.success(2, 200),
                ProcessingStatus.success(3, 300));

        Compactor compactor = new Compactor(Arrays.asList(compactionJobProcessor, restageJobProcessor), executor, queue,
                scheduler, metricsRegistry, new Lazy<>(() -> sparkSession), compactionService, properties);

        //when
        int result = compactor.addJobs(3, compactionJobProcessor);
        ProcessingStatus status = compactor.getTotalStatus();

        //then
        assertEquals(3, result);
        assertEquals(3, status.getTaskCount());
        assertEquals(6, status.getFileCount());
        assertEquals(600, status.getByteCount());

        verify(compactionJobProcessor, times(3)).process(any(CompactionJob.class));
        verify(restageJobProcessor, never()).process(any(RestageJob.class));
    }

    @Test
    public void runWithTaskFailure() {
        //given
        initMockCompactionJobs();
        when(compactionJobProcessor.getJobType()).thenReturn(JobType.COMPACT);
        when(compactionJobProcessor.process(any())).thenReturn(ProcessingStatus.success(1, 100),
                ProcessingStatus.success(2, 200))
                .thenThrow(new RuntimeException());

        Compactor compactor = new Compactor(Collections.singletonList(compactionJobProcessor), executor, queue,
                scheduler,
                metricsRegistry, new Lazy<>(() -> sparkSession), compactionService, properties);

        //when
        int result = compactor.addJobs(3, compactionJobProcessor);
        ProcessingStatus status = compactor.getTotalStatus();
        //then
        assertEquals(3, result);
        assertEquals(3, status.getTaskCount());
        assertEquals(3, status.getFileCount());
        assertEquals(300, status.getByteCount());
        assertEquals(1, status.getErrorCount());
    }

    @Test
    public void shouldThrowWhenCompactorInitializedWithNoProcessors() {
        //given
        assertThrows(IllegalArgumentException.class,
                () -> new Compactor(Collections.emptyList(), executor, queue, scheduler, metricsRegistry,
                        new Lazy<>(() -> sparkSession),
                        compactionService, properties));

    }

    @Test
    public void shouldScheduleExecution() throws InterruptedException {
        // given
        when(compactionJobProcessor.getJobType()).thenReturn(JobType.COMPACT);
        when(restageJobProcessor.getJobType()).thenReturn(JobType.RESTAGE);
        when(mergeCompactionJobProcessor.getJobType()).thenReturn(JobType.MERGE_COMPACT);
        initMockCompactionJobs();

        List<JobProcessor<?>> jobProcessors = Arrays.asList(
                compactionJobProcessor, restageJobProcessor, mergeCompactionJobProcessor);
        //Has to have two queries, should use logStatus()
        CountDownLatch latch = new CountDownLatch(2);
        //latches all the jobs in the executor, permits to run logStatus() as there are jobs executing.
        CountDownLatch jobsLatch = new CountDownLatch(1);

        properties.setExecuteEvery(Duration.of(500, ChronoUnit.MILLIS));
        properties.setNoJobsSleep(Duration.of(500, ChronoUnit.MILLIS));
        properties.setMaxJobs(10);

        // when
        when(sparkContext.isStopped()).thenReturn(false);
        when(sparkSession.sparkContext()).thenReturn(sparkContext);
        when(compactionService.getDataProcessJobs(properties.getMaxJobs(), JobType.COMPACT)).thenAnswer(i -> {
            latch.countDown();
            if (latch.getCount() == 0) {
                jobsLatch.countDown();
            }
            return generateJobs(i.getArgument(0), i.getArgument(1));
        });

        when(compactionJobProcessor.process(any())).thenAnswer(job -> {
            jobsLatch.await();
            return ProcessingStatus.success(1, 100);
        });

        Executor exec = new ThreadPoolExecutor(20, 20, 100, TimeUnit.MINUTES, queue);
        Compactor compactor = new Compactor(jobProcessors, exec, queue, scheduler, metricsRegistry,
                new Lazy<>(() -> sparkSession), compactionService, properties);
        compactor.setExitHandler(() -> {
        }); //avoid System.exit() in shutdown of the test, we don't care about errors there.
        compactor.run();

        latch.await();
        scheduler.shutdownNow();

        verify(compactionService, atLeast(2)).getDataProcessJobs(properties.getMaxJobs(), JobType.COMPACT);

    }


    @Test
    public void shouldNotAskForMoreJobsIfCompactionReturnsMaxJobs() throws InterruptedException {
        // given
        when(compactionJobProcessor.getJobType()).thenReturn(JobType.COMPACT);
        when(restageJobProcessor.getJobType()).thenReturn(JobType.RESTAGE);
        when(mergeCompactionJobProcessor.getJobType()).thenReturn(JobType.MERGE_COMPACT);
        initMockCompactionJobs();


        List<JobProcessor<?>> jobProcessors = Arrays.asList(
                compactionJobProcessor, restageJobProcessor, mergeCompactionJobProcessor);
        CountDownLatch latch = new CountDownLatch(1);

        // when
        when(sparkContext.isStopped()).thenReturn(false);
        when(sparkSession.sparkContext()).thenReturn(sparkContext);
        when(compactionService.getDataProcessJobs(properties.getMaxJobs(), JobType.COMPACT)).thenAnswer(i -> {
            latch.countDown();
            return generateJobs(i.getArgument(0), i.getArgument(1));
        });

        when(compactionJobProcessor.process(any())).thenReturn(ProcessingStatus.success(1, 100));

        Compactor compactor = new Compactor(jobProcessors, executor, queue, scheduler, metricsRegistry,
                new Lazy<>(() -> sparkSession), compactionService, properties);
        compactor.run();

        latch.await();
        scheduler.shutdownNow();

        verify(compactionService, only()).getDataProcessJobs(properties.getMaxJobs(), JobType.COMPACT);
        verify(compactionService, never()).getDataProcessJobs(anyInt(), eq(JobType.RESTAGE));
        verify(compactionService, never()).getDataProcessJobs(anyInt(), eq(JobType.MERGE_COMPACT));

        verify(compactionJobProcessor, times(properties.getMaxJobs())).process(any());
        verify(restageJobProcessor, never()).process(any());
        verify(mergeCompactionJobProcessor, never()).process(any());
    }

    @Test
    public void shouldDistributeJobsBasedOnJobsReturnedFromService() throws InterruptedException {
        // given
        when(compactionJobProcessor.getJobType()).thenReturn(JobType.COMPACT);
        when(restageJobProcessor.getJobType()).thenReturn(JobType.RESTAGE);
        when(mergeCompactionJobProcessor.getJobType()).thenReturn(JobType.MERGE_COMPACT);
        initMockCompactionJobs();


        List<JobProcessor<?>> jobProcessors = Arrays.asList(
                compactionJobProcessor, restageJobProcessor, mergeCompactionJobProcessor);
        CountDownLatch latch = new CountDownLatch(jobProcessors.size());

        // when
        when(sparkContext.isStopped()).thenReturn(false);
        when(sparkSession.sparkContext()).thenReturn(sparkContext);

        when(compactionService.getDataProcessJobs(properties.getMaxJobs(), JobType.COMPACT)).thenAnswer(i -> {
            latch.countDown();
            int requestedJobs = i.getArgument(0);
            return generateJobs(requestedJobs / 2, i.getArgument(1));
        });
        when(compactionService.getDataProcessJobs(properties.getMaxJobs() / 2, JobType.RESTAGE)).thenAnswer(i -> {
            latch.countDown();
            int requestedJobs = i.getArgument(0);
            return generateJobs(requestedJobs / 2, i.getArgument(1));
        });

        when(compactionService.getDataProcessJobs(properties.getMaxJobs() / 4, JobType.MERGE_COMPACT)).thenAnswer(i -> {
            latch.countDown();
            return generateJobs((i.getArgument(0)), i.getArgument(1));
        });

        when(compactionJobProcessor.process(any())).thenReturn(ProcessingStatus.success(1, 100));
        when(restageJobProcessor.process(any())).thenReturn(ProcessingStatus.success(1, 100));
        when(mergeCompactionJobProcessor.process(any())).thenReturn(ProcessingStatus.success(1, 100));

        Compactor compactor = new Compactor(jobProcessors, executor, queue, scheduler, metricsRegistry,
                new Lazy<>(() -> sparkSession), compactionService, properties);
        compactor.run();

        latch.await();
        scheduler.shutdownNow();

        // then
        // ex. for 1000 jobs we must request:
        // 1000 compaction | 500 restaging | 250 merge-compaction
        // but we will actually get from the service the following:
        // 500 compaction | 250 restaging | 250 merge-compaction
        // verifying scenario:
        verify(compactionService, times(3)).getDataProcessJobs(anyInt(), any(JobType.class));
        verify(compactionService, times(1)).getDataProcessJobs(properties.getMaxJobs(), JobType.COMPACT);
        verify(compactionService, times(1)).getDataProcessJobs(properties.getMaxJobs() / 2, JobType.RESTAGE);
        verify(compactionService, times(1)).getDataProcessJobs(properties.getMaxJobs() / 4, JobType.MERGE_COMPACT);

        int actualCompactionJobs = properties.getMaxJobs() / 2;
        int actualOtherJobs = properties.getMaxJobs() / 4;
        verify(compactionJobProcessor, times(actualCompactionJobs)).process(any());
        verify(restageJobProcessor, times(actualOtherJobs)).process(any());
        verify(mergeCompactionJobProcessor, times(actualOtherJobs)).process(any());
    }


    @SuppressWarnings("unchecked")
    private <T extends DataProcessingJob> List<T> generateJobs(int size, JobType jobType) {
        Supplier<DataProcessingJob> supplier = getJobSupplier(jobType);
        List<DataProcessingJob> jobs = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            jobs.add(supplier.get());
        }
        return (List<T>) jobs;
    }

    private Supplier<DataProcessingJob> getJobSupplier(JobType jobType) {
        switch (jobType) {
        case RESTAGE:
            return () -> RestageJob.builder()
                    .sourceDir(URI.create("/dir1/1/2/" + count.getAndIncrement() + "/2024-01-01")).build();
        case MERGE_COMPACT:
            return () -> MergeCompactionJob.builder()
                    .sourceDir(URI.create("/dir1/1/2/" + count.getAndIncrement() + "/2024-01-01")).build();
        case COMPACT:
        default:
            return () -> CompactionJob.builder()
                    .sourceDir(URI.create("/dir1/1/2/" + count.getAndIncrement() + "/2024-01-01")).build();
        }
    }
}
