package cern.nxcals.compaction.service;

import org.apache.curator.framework.recipes.leader.LeaderLatch;
import org.apache.curator.framework.recipes.leader.LeaderLatchListener;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class LeaderElectorTest {
    @Mock
    private LeaderLatch latch;
    private LeaderElector leaderElector;

    @BeforeEach
    void setup() {
        leaderElector = new LeaderElector(latch);
    }


    @Test
    void shouldWaitForLeader() throws Exception {
        //given


        //when
        leaderElector.waitForLeader();

        //then
        verify(latch).addListener(ArgumentMatchers.any(LeaderLatchListener.class));
        verify(latch).start();
        verify(latch).await();
    }


    @Test
    void shouldCloseLatch() throws IOException {
        //given
        //when
        leaderElector.close();

        //then
        verify(latch).close();
    }


    @Test
    void shouldThrow() throws Exception {
        //given
        doThrow(new Exception("test")).when(latch).start();

        //when
        assertThrows(IllegalStateException.class, () -> leaderElector.waitForLeader());
        //then nothing


    }

    @Test
    void shouldInterrupt() throws Exception {
        //given
        doThrow(new InterruptedException("test")).when(latch).start();

        //when
        assertThrows(IllegalStateException.class, () -> leaderElector.waitForLeader());
        //then
        assertTrue(Thread.currentThread().isInterrupted());

    }
}