package cern.nxcals.api.extraction.data.demo;

import cern.nxcals.api.extraction.data.builders.DataQuery;
import cern.nxcals.api.extraction.data.builders.DevicePropertyDataQuery;
import com.google.common.collect.ImmutableMap;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

/**
 * This class provide some examples and helps to test functionality of the builders from data access package
 */
class BuildersDemo {

    static void runAll(SparkSession sparkSession) {
        testKeyValuesQuery(sparkSession);
        testDevicePropertyQuery(sparkSession);
        testVariableQuery(sparkSession);
    }

    static void testKeyValuesQuery(SparkSession sparkSession) {

        System.out.println("--------------------- START KEY_VALUES_QUERY TEST ------------------------");

        Dataset<Row> dataset = DataQuery.builder(sparkSession).byEntities().system("CMW-MIG-DEV-VHYHYNIA")
                .startTime("2017-04-26 00:00:00.000").endTime("2018-11-28 01:00:15.000")
                .entity()
                .keyValue("device", "device_test1_vhyhynia1")
                .keyValueLike("property", "property\\_test1_vh%nia1")
                .entity()
                .keyValuesLike(ImmutableMap.of(
                        "device", "device_t%t2\\_vhyhynia2",
                        "property", "property_test2_vhyhynia2"
                ))
                .entity()
                .keyValues(ImmutableMap.of(
                        "device", "device_test3_vhyhynia3",
                        "property", "property_test3_vhyhynia3"
                ))
                .build();

        long beforeCount = System.currentTimeMillis();
        long count = dataset.count();
        System.out.println("Count=" + count + " took " + (System.currentTimeMillis() - beforeCount) + " ms");
        System.out.println("--------------------------------");
        System.out.println();
        dataset.show();
        System.out.println();
        System.out.println();
        System.out.println("nxcals_entity_id ::::::::::::::::::::");
        Dataset<Row> dsEntity = dataset.select("nxcals_entity_id");
        System.out.println("Count: " + dsEntity.count());
        Dataset<Row> dsEntityDistinct = dsEntity.distinct();
        System.out.println("Count distinct: " + dsEntityDistinct.count());
        System.out.println("SHOW :::: ");
        dsEntityDistinct.show(10, false);

        System.out.println("device ::::::::::::::::::::");
        Dataset<Row> dsDevice = dataset.select("device");
        System.out.println("Count: " + dsDevice.count());
        Dataset<Row> dsDeviceDistinct = dsDevice.distinct();
        System.out.println("Count distinct: " + dsDeviceDistinct.count());
        System.out.println("SHOW :::: ");
        dsDeviceDistinct.show(10, false);

        System.out.println("--------------------- DONE KEY_VALUES_QUERY TEST ------------------------");

    }

    static void testDevicePropertyQuery(SparkSession sparkSession) {
        System.out.println("--------------------- START DEVICE_PROPERTY_QUERY TEST ------------------------");

        Dataset<Row> dataset = DevicePropertyDataQuery.builder(sparkSession).system("CMW-MIG-DEV-VHYHYNIA")
                .startTime("2017-04-26 00:00:00.000").endTime("2018-11-28 01:00:15.000")
                .entity()
                .parameterLike("dev_ce_t%t3_vhyhynia3/property_t%3_vhyhynia3")
                .entity()
                .deviceLike("%ce%").property("property_test2_vhyhynia2")
                .entity()
                .device("device_test1_vhyhynia1").propertyLike("property\\_test1_vhy%nia1")
                .build();

        System.out.println("--------------------------------");
        System.out.println();
        dataset.show();
        System.out.println();
        System.out.println("Count: " + dataset.count());
        System.out.println("nxcals_entity_id ::::::::::::::::::::");
        Dataset<Row> dsEntityDistinct = dataset.select("nxcals_entity_id").distinct();
        System.out.println("Count distinct: " + dsEntityDistinct.count());
        System.out.println("SHOW :::: ");
        dsEntityDistinct.show(10, false);

        System.out.println("device ::::::::::::::::::::");
        Dataset<Row> dsDeviceDistinct = dataset.select("device").distinct();
        System.out.println("Count distinct: " + dsDeviceDistinct.count());
        System.out.println("SHOW :::: ");
        dsDeviceDistinct.show(10, false);

        System.out.println("--------------------- DONE DEVICE_PROPERTY_QUERY TEST------------------------");
    }

    static void testVariableQuery(SparkSession sparkSession) {

        System.out.println("--------------------- START VARIABLE_QUERY TEST ------------------------");

        Dataset<Row> dataset = getRowDatasetForMyLocalVars(sparkSession);
        //        Dataset<Row> dataset = getRowDatasetFromPro(sparkSession);

        System.out.println();
        System.out.println();

        dataset.show(false);
        dataset.select("nxcals_variable_name").distinct().show(false);

        System.out.println("--------------------------------");
        System.out.println();
        System.out.println("nxcals_entity_id ::::::::::::::::::::");
        Dataset<Row> dsEntity = dataset.select("nxcals_entity_id");
        Dataset<Row> dsEntityDistinct = dsEntity.distinct();
        System.out.println("Count distinct: " + dsEntityDistinct.count());
        System.out.println("SHOW :::: ");
        dsEntityDistinct.show(10, false);

        System.out.println("variable ::::::::::::::::::::");
        Dataset<Row> dsVariable = dataset.select("nxcals_value");
        System.out.println("Count: " + dsVariable.count());
        Dataset<Row> dsVariableDistinct = dsVariable.distinct();
        System.out.println("Count distinct: " + dsVariableDistinct.count());
        System.out.println("SHOW :::: ");
        dsVariableDistinct.show(10, false);

        System.out.println("--------------------- DONE VARIABLE_QUERY TEST ------------------------");

    }

    private static Dataset<Row> getRowDatasetForMyLocalVars(SparkSession sparkSession) {
        //        Dataset<Row> dataset = VariableQuery.builder(sparkSession).system("CMW")

        return DataQuery.builder(sparkSession).byVariables().system("CMW-MIG-DEV-VHYHYNIA")
                //                .startTime("2017-04-26 00:00:00.000").endTime("2018-11-28 01:00:15.000")
                .startTime("2018-06-26 00:00:00.000").endTime("2018-11-28 01:00:15.000")
                //                .startTime("2016-06-26 00:00:00.000").endTime("2017-11-28 01:00:15.000")
                //                .variable("variable_test2_vhyhynia2")
                //                    .variableLike("%vhyhynia_")
                //                    .variableLike("v%able_t_st1_vhyhynia1")
                //                    .variable("variable_test3_vhyhynia3")
                .variableLike("%vhyhynia_")
                //                .variable("variable_test3_vhyhynia3")
                //                .variableLike("variable_test3_vhyhynia3")
                //                For dedicated exception with expanded field name
                //                .variable("TCP.6R3.B2:FUNC_WARN_IN_GU")
                .build();
    }

    private static Dataset<Row> getRowDatasetFromPro(SparkSession sparkSession) {
        //        Dataset<Row> dataset = VariableQuery.builder(sparkSession).system("CMW")

        return DataQuery.builder(sparkSession).byVariables().system("CMW")
                .startTime("2018-04-29 00:00:00.000").endTime("2018-04-30 00:00:00.000")
                .variableLike("LTB.BCT%:INTENSITY")
                //                For dedicated exception with expanded field name
                //                .variable("TCP.6R3.B2:FUNC_WARN_IN_GU")
                .build();
    }

}