/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.api.extraction.performance;

import cern.nxcals.api.config.SparkContext;
import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.extraction.data.builders.DataQuery;
import cern.nxcals.api.extraction.metadata.EntityService;
import cern.nxcals.api.extraction.metadata.ServiceClientFactory;
import cern.nxcals.api.extraction.metadata.SystemSpecService;
import cern.nxcals.common.SystemFields;
import com.google.common.collect.Lists;
import lombok.Data;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.avro.Schema;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocatedFileStatus;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.RemoteIterator;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.StructField;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Import;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;

@Import(SparkContext.class)
@SpringBootApplication
@Slf4j
public class ApplicationPerformanceAnalysisCompareHDFSvsHbaseUsingNXCALS {


    public static final Random rand = new Random();
    private static final FileSystem fileSystem = createFileSystem();
    private static final int INTERVAL_IN_MINUTES = 1 * 60; //our split interval
    private static final long PARQUET_BLOCK_SIZE_MB = 128;
    private static final long PARQUET_BLOCK_SIZE = PARQUET_BLOCK_SIZE_MB * 1024 * 1024L;
    private static final boolean SHOULD_USE_PARTITION_IN_QUERY = true;
    private static final boolean SHOULD_USE_ADAPTIVE_ROW_GROUP = true;


    static {
//        System.setProperty("logging.config", "classpath:log4j2.yml");
//
//        //         Uncomment in order to overwrite the default security settings.
//        //        System.setProperty("javax.net.ssl.trustStore", "/opt/nxcalsuser/nxcals_cacerts");
//        //        System.setProperty("javax.net.ssl.trustStorePassword", "nxcals");
//
//        //        Kerberos credentials (or comment them out to allow them be automatically discovered via krb ticket cache)
//        //        String user = System.getProperty("user.name");
        System.setProperty("kerberos.principal", "jwozniak");
        System.setProperty("kerberos.keytab", "/opt/jwozniak/.keytab");
//
//        //         TEST Env
//        //        System.setProperty("service.url", "https://nxcals-test4.cern.ch:19093,https://nxcals-test5.cern.ch:19093,https://nxcals-test6.cern.ch:19093");
//        //         TESTBED
//        //        System.setProperty("service.url",
//        //                "https://cs-ccr-testbed2.cern.ch:19093,https://cs-ccr-testbed3.cern.ch:19093,https://cs-ccr-nxcalstbs1.cern.ch:19093");
//        //         PRO
        System.setProperty("service.url",
                "https://cs-ccr-nxcals6.cern.ch:19093,https://cs-ccr-nxcals7.cern.ch:19093,https://cs-ccr-nxcals8.cern.ch:19093");
//        //Hadoop 3
//        System.setProperty("service.url",
//                "https://cs-ccr-nxcalstst2.cern.ch:19093,https://cs-ccr-nxcalstst3.cern.ch:19093,https://cs-ccr-nxcalstst4.cern.ch:19093");
    }

    private static SystemSpecService systemSpecService = ServiceClientFactory.createSystemSpecService();
    private static EntityService entityService = ServiceClientFactory.createEntityService();

    public static void main(String[] args) {

        ConfigurableApplicationContext context = SpringApplication.run(ApplicationPerformanceAnalysisCompareHDFSvsHbaseUsingNXCALS.class, args);
        SparkSession sparkSession = context.getBean(SparkSession.class);

        warmUpSpark(sparkSession);

        List<String> inputPaths = getInputPaths();

        for (String path : inputPaths) {
            log.info("Start processing {}", path);
            for (int i = 0; i < 1; ++i) {
                log.info("************ Run {}", i);
                List<Query> queries = getQueries(sparkSession, path);
                log.info("Start querying for HDFS");
                List<QueryResult> hdfs = queryInPath(sparkSession, path, queries, false);
                log.info("Start querying for HBase");
                List<QueryResult> hbase = queryInPath(sparkSession, path, queries, true);
                compareResults(path, hdfs, hbase);
            }
        }
    }

    private static FileSystem createFileSystem() {
        org.apache.hadoop.conf.Configuration conf = new org.apache.hadoop.conf.Configuration();
        try {
            return FileSystem.get(conf);
        } catch (IOException e) {
            throw new UncheckedIOException("Cannot access filesystem", e);
        }
    }

    private static void warmUpSpark(SparkSession sparkSession) {
        log.info("Warming up the spark session and java");
        for (int i = 0; i < 10; i++) {
            sparkSession.read().parquet("/project/nxcals/nxcals_pro/data/2/14074/15804/2021/9/20").sample(0.1).collectAsList();
            sparkSession.read().parquet("/project/nxcals/nxcals_pro/data/2/14074/15804/2021/9/20").select(SystemFields.NXC_ENTITY_ID.getValue()).distinct().sample(0.1).collectAsList();
        }
        log.info("Warm up finished");
    }

    private static void compareResults(String path, List<QueryResult> hdfs, List<QueryResult> hbase) {
        log.info("Comparing results for {} and hbase", path);
        log.info("Query times for original: {}", hdfs);
        log.info("Query times for repartioned: {}", hbase);
        long better = 0;
        long worse = 0;
        long betterSum = 0;
        long worseSum = 0;
        long cannotCompare = 0;
        for (int i = 0; i < hdfs.size(); i++) {
            QueryResult hdfsQueryResult = hdfs.get(i);
            QueryResult hbaseQueryResult = hbase.get(i);
            if (!check(hdfsQueryResult, hbaseQueryResult, 30)) {
                log.warn("Queries differ too much in size to compare {} {}", hdfsQueryResult, hbaseQueryResult);
                cannotCompare++;
                continue;
            }
            long orgAvgTime = average(hdfsQueryResult);
            long repAvgTime = average(hbaseQueryResult);
            long diff = Math.abs(orgAvgTime - repAvgTime);
            long percent = (long) (((double) diff / (double) orgAvgTime) * 100);
            if (orgAvgTime > repAvgTime) {
                log.info("For HDSF query {} and Hbase query {} BETTER for Hbase by {}% diff={} hdfsQueryResult={} hbaseQueryResult={}", hdfsQueryResult, hbaseQueryResult, percent, diff, orgAvgTime, repAvgTime);
                better++;
                betterSum += percent;
            } else {
                log.warn("**** For HDSF query {} and Hbase query {} WORSE for Hbase by {}% diff={} hdfsQueryResult={} hbaseQueryResult={}", hdfsQueryResult, hbaseQueryResult, percent, diff, orgAvgTime, repAvgTime);
                worse++;
                worseSum += percent;
            }

        }

        long betterAvg = 0;
        long worseAvg = 0;
        if (better > 0) {
            betterAvg = betterSum / better;
        }
        if (worse > 0) {
            worseAvg = worseSum / worse;
        }

        Directory orgDir = analysePath(path);


        log.info("Finished analysis for {} and Hbase, better {} with avg {}% worse {} with avg {}% (org dir count: {} {} MB), cannot compare={}",
                path,
                better, betterAvg, worse, worseAvg,
                orgDir.fileCount,
                String.format("%,.3f", orgDir.totalSize / (1024 * 1024.0)),
                cannotCompare
        );

    }

    @SneakyThrows
    private static Directory analysePath(String path) {
        RemoteIterator<LocatedFileStatus> iterator = fileSystem.listFiles(new Path(path), true);
        long count = 0;
        long size = 0;
        while (iterator.hasNext()) {
            LocatedFileStatus file = iterator.next();
            if (file.isFile() && file.getPath().getName().endsWith(".parquet")) {
                size += file.getLen();
                count++;
            }
        }
        return new Directory(count, size);
    }

    @Data
    public static class Directory {
        private final long fileCount;
        private final long totalSize;
    }

    private static boolean check(QueryResult org, QueryResult rep, long diff) {
        //compare only results for count diff < diff%
        return (org.count == 0 && rep.count == 0) || (Math.abs(org.count - rep.count) / (double) org.count) * 100.0 <= diff;
    }

    private static long average(QueryResult org) {
        return (long) org.queryTimes.stream().mapToLong(r -> r).average().orElseThrow(() -> new RuntimeException("Cannot calculate average"));
    }

    private static List<QueryResult> queryInPath(SparkSession sparkSession, String path, List<Query> queries, boolean useHbase) {
        log.info("Start querying in path {}", path);
        String timestampColumn = getTimestampColumn(path);

        //Cannot parallelize as this introduces a very big random factor to query times as they affect each other.
        return queries.stream().map(query -> query(sparkSession, query, useHbase)).collect(Collectors.toList());
    }


    private static QueryResult query(SparkSession sparkSession, Query query, boolean useHbase) {
        long count = -1;
        long entityId = query.entity;
        Entity entity = entityService.findById(entityId).orElseThrow(() -> new RuntimeException("No such entity " + entityId));
        List<Long> queryTimes = new ArrayList<>();
        for (int i = 0; i < 3; ++i) { //query multiple times for the same dataset to take average.
            long start = System.currentTimeMillis();
            List<Row> rows = null;
            if (useHbase) {
                Dataset<Row> dataset = DataQuery.getFor(sparkSession, query.timeWindow, entity);
                Column[] columns = getColumns(dataset, query);
                rows = dataset.select(columns).collectAsList();
            } else {
                TimeWindow newTime = TimeWindow.between(query.timeWindow.getStartTime().minus(3, ChronoUnit.DAYS)
                        , query.timeWindow.getEndTime().minus(3, ChronoUnit.DAYS));
                query = new Query(entity.getId(), newTime, query.fields);
                Dataset<Row> dataset = DataQuery.getFor(sparkSession, newTime, entity);
                Column[] columns = getColumns(dataset, query);
                rows = dataset.select(columns).collectAsList();
            }

            long queryTime = System.currentTimeMillis() - start;

            log.info("Run a query for {}, took {} ms, count={} useHbase={}", query, queryTime, rows.size(), useHbase);
            queryTimes.add(queryTime);
            count = rows.size();
        }

        return new QueryResult(query, queryTimes, count);
    }


    private static Column[] getColumns(Dataset<Row> dataset, Query query) {


        return query.getFields().stream().map(dataset::col).toArray(Column[]::new);
    }

    private static List<Query> getQueries(SparkSession sparkSession, String path) {
        List<Long> entities = getEntities(sparkSession, path, 3);
        List<TimeWindow> timeWindows = getTimewindows(10);
        Set<String> fields = getFields(sparkSession, path);
        log.info("For path {} using entities {} and timewindows {}", path, entities, timeWindows);
        List<Query> queries = new ArrayList<>();

        for (Long entity : entities) {
            for (TimeWindow tw : timeWindows) {
                queries.add(new Query(entity, tw, fields));
            }
        }
        return queries;
    }

    private static Set<String> getFields(SparkSession sparkSession, String path) {
        String timestampColumn = getTimestampColumn(path);
        Dataset<Row> dataset = sparkSession.read().parquet(path);
        StructField[] fields = dataset.schema().fields();
        Set<String> queryFields = new HashSet<>();
        queryFields.add(timestampColumn);
        queryFields.add("device");
        queryFields.add("property");
        return queryFields;
    }


    private static String getTimestampColumn(String path) {
        ///project/nxcals/nxcals_pro/data/17/29222/201119/2021/9/20
        long systemId = Long.parseLong(path.split("/")[5]);
        SystemSpec systemSpec = systemSpecService.findById(systemId).orElseThrow(() -> new RuntimeException("No such system id " + systemId));
        return new Schema.Parser().parse(systemSpec.getTimeKeyDefinitions()).getFields().stream().map(Schema.Field::name)
                .collect(Collectors.toList()).get(0);


    }


    private static List<TimeWindow> getTimewindows(int number) {
        Instant end = Instant.now();

        List<TimeWindow> windows = new ArrayList<>();
        for (int i = 0; i < number / 2; i++) {
            Instant t1 = end.minus(10 + rand.nextInt(32), ChronoUnit.HOURS);
            Instant t2 = end.minus(rand.nextInt(300), ChronoUnit.MINUTES);
//            windows.add(TimeWindow.between(t1, t2));

            //small window
            t1 = end.minus(rand.nextInt(36), ChronoUnit.HOURS);
            t2 = t1.plus(rand.nextInt(50), ChronoUnit.MINUTES);
            windows.add(TimeWindow.between(t1, t2));

        }
        return windows;
    }


    private static List<Long> getEntities(SparkSession sparkSession, String path, int limit) {
        Dataset<Row> dataset = sparkSession.read().parquet(path);

        Dataset<Row> distinct = dataset.select(SystemFields.NXC_ENTITY_ID.getValue()).distinct();

        long total = distinct.count();
        log.info("Total number {} of entities in path {}", total, path);
        List<Long> entityIds =
                distinct.sample(0.1).limit(limit).collectAsList()
                        .stream().map(r -> (Long) r.getAs(SystemFields.NXC_ENTITY_ID.getValue())).collect(Collectors.toList());
        if (entityIds.isEmpty()) {
            //try without sampling
            return distinct.limit(limit)
                    .collectAsList().stream().map(r -> (Long) r.getAs(SystemFields.NXC_ENTITY_ID.getValue())).collect(Collectors.toList());
        } else {
            return entityIds;
        }

    }


    private static List<String> getInputPathsForTest() {
        return Lists.newArrayList(
                //1GB

                "/project/nxcals/nxcals_pro/data/2/17491/210991/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/21222/186115/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/17412/15804/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/26724/196613/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/135221/222995/2021/9/20"

        );

    }

    private static List<String> getInputPaths() {
        return Lists.newArrayList(
//very small files << 10KB
//
                "/project/nxcals/nxcals_pro/data/2/11961/13972/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/12260/14499/2021/9/20",
//~100KB files
                "/project/nxcals/nxcals_pro/data/2/20731/182496/2021/9/20",

//~900KB files
                "/project/nxcals/nxcals_pro/data/2/14640/186611/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/15831/15091/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/11751/14054/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/75221/175425/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/137222/337494/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/15833/15091/2021/9/20",
                //~10MB-20MB files
                "/project/nxcals/nxcals_pro/data/2/15391/177561/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/135230/175346/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/51221/276495/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/11534/13933/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/103221/309492/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/14074/15804/2021/9/20",

                //100MB files
                "/project/nxcals/nxcals_pro/data/2/11661/14032/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/11850/244493/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/15531/13982/2021/9/20",
//500MB files
                "/project/nxcals/nxcals_pro/data/2/15865/15514/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/20222/186137/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/20244/186123/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/13629/175483/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/11513/13922/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/12661/186136/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/106221/14073/2021/9/20", //derived, 0.5GB

//                //Big files > 1GB
//

                "/project/nxcals/nxcals_pro/data/2/17491/210991/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/30722/222492/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/12866/268492/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/112222/315498/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/13649/211991/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/12839/266495/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/20224/186112/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/14673/186131/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/16861/177429/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/12869/175115/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/21222/186115/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/17412/15804/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/26724/196613/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/135221/222995/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/11863/13943/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/20246/186122/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/33234/213004/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/11768/14123/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/21241/186120/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/103223/335506/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/11860/14189/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/88221/177302/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/21242/186618/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/15372/177302/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/87222/177302/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/20248/186122/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/20250/186120/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/11734/14072/2021/9/20",


                "/project/nxcals/nxcals_pro/data/2/17313/326502/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/13638/175479/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/15715/177145/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/36721/222995/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/20247/186141/2021/9/20",

                "/project/nxcals/nxcals_pro/data/2/20251/15804/2021/9/20",

                "/project/nxcals/nxcals_pro/data/2/20744/186115/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/20754/186618/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/20245/186616/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/11468/13865/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/11745/186611/2021/9/20", //10GB
                "/project/nxcals/nxcals_pro/data/2/71221/176908/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/13306/15804/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/130223/332493/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/26728/196616/2021/9/20",

                //very big files >>20 GB
//
                "/project/nxcals/nxcals_pro/data/2/17424/15804/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/15382/175351/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/13438/224491/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/49223/278494/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/11784/14118/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/11936/177661/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/11785/14119/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/90224/298500/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/11792/14135/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/11490/177370/2021/9/20"
        );
    }

    @Data
    public static class Query {
        private final Long entity;
        private final TimeWindow timeWindow;
        private final Set<String> fields;
    }

    @Data
    public static class QueryResult {
        private final Query query;
        private final List<Long> queryTimes;
        private final long count;
    }

}
