package cern.nxcals.api.extraction.data.demo;

import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.domain.VariableConfig;
import cern.nxcals.api.domain.VariableDeclaredType;
import cern.nxcals.api.extraction.metadata.EntityService;
import cern.nxcals.api.extraction.metadata.ServiceClientFactory;
import cern.nxcals.api.extraction.metadata.SystemSpecService;
import cern.nxcals.api.extraction.metadata.VariableService;
import cern.nxcals.api.extraction.metadata.queries.Entities;
import cern.nxcals.api.extraction.metadata.queries.Variables;
import cern.nxcals.api.utils.TimeUtils;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSortedSet;

import java.util.Optional;

public class VariableCreator {
    static {
        System.setProperty("logging.config", "classpath:log4j2.yml");

        //         Uncomment in order to overwrite the default security settings.
        //        System.setProperty("javax.net.ssl.trustStore", "/opt/nxcalsuser/nxcals_cacerts");
        //        System.setProperty("javax.net.ssl.trustStorePassword", "nxcals");

        //        Kerberos credentials (or comment them out to allow them be automatically discovered via krb ticket cache)
        //        String user = System.getProperty("user.name");
        //        System.setProperty("kerberos.principal", "acclog");
        //        System.setProperty("kerberos.keytab", "/opt/" + user + "/.keytab-acclog");

        //         TEST Env
        //        System.setProperty("service.url", "http://nxcals-test4.cern.ch:19093,http://nxcals-test5.cern.ch:19093,http://nxcals-test6.cern.ch:19093");
        //         TESTBED
        //        System.setProperty("service.url",
        //                "http://cs-ccr-testbed2.cern.ch:19093,http://cs-ccr-testbed3.cern.ch:19093,http://cs-ccr-nxcalstbs1.cern.ch:19093");
        //         PRO
        //        System.setProperty("service.url",
        //                "http://cs-ccr-nxcals6.cern.ch:19093,http://cs-ccr-nxcals7.cern.ch:19093,http://cs-ccr-nxcals8.cern.ch:19093");
    }

    public static void main(String[] args) {
        VariableService variableService = ServiceClientFactory.createVariableService();
        EntityService entityService = ServiceClientFactory.createEntityService();
        SystemSpecService systemSpecService = ServiceClientFactory.createSystemSpecService();

        SystemSpec systemSpec = systemSpecService.findByName("CMW")
                .orElseThrow(() -> new IllegalArgumentException("No such system"));

        //        {"device": "CPS.TGM", "property": "FULL-TELEGRAM.STRC"}
        //        {"device": "XTIM.PX.SCY-CT", "property": "Acquisition"}

        Entity tgm = entityService.findOne(Entities.suchThat().systemName().eq(systemSpec.getName()).and().keyValues()
                .eq(systemSpec, ImmutableMap.of("device", "SPS.TGM", "property", "FULL-TELEGRAM.STRC")))
                .orElseThrow(() -> new IllegalArgumentException("No such entity"));

        Entity xtim = entityService.findOne(Entities.suchThat().systemName().eq(systemSpec.getName()).and().keyValues()
                .eq(systemSpec, ImmutableMap.of("device", "XTIM.SX.SCY-CT", "property", "Acquisition")))
                .orElseThrow(() -> new IllegalArgumentException("No such entity"));

        System.out.println(tgm);
        System.out.println(xtim);

        System.out.println(tgm.getFirstEntityHistory().getValidity().getStartTime());

        Optional<Variable> fundamentalVariable = variableService.findOne(
                Variables.suchThat().systemId().eq(systemSpec.getId()).and().variableName()
                        .eq("SPS" + ":NXCALS_FUNDAMENTAL"));
        if (!fundamentalVariable.isPresent()) {
            VariableConfig cpsTgmData = VariableConfig.builder().entityId(tgm.getId())
                    .validity(TimeWindow.between(null, TimeUtils.getInstantFromString("2019-03-19 00:00:00.000")))
                    .build();
            VariableConfig xtimData = VariableConfig.builder().entityId(xtim.getId())
                    .validity(TimeWindow.between(TimeUtils.getInstantFromString("2019-03-19 00:00:00.000"), null))
                    .build();

            Variable variable = Variable.builder().variableName("SPS:NXCALS_FUNDAMENTAL").systemSpec(systemSpec)
                    .description("Fundamental linking TGM and XTIM data under one record")
                    .declaredType(VariableDeclaredType.FUNDAMENTAL).configs(ImmutableSortedSet.of(cpsTgmData, xtimData))
                    .build();

            Variable registered = variableService.create(variable);
            System.out.println("Registered: " + registered);
        } else {
            Variable modifiedVariable = fundamentalVariable.get().toBuilder().description("new description").build();
            Variable updated = variableService.update(modifiedVariable);
            System.out.println("Updated: " + updated);
        }

    }
}
