package cern.nxcals.api.extraction.data.demo;

import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.IOException;

public class HdfsCheck {


    public static void main(String[] args) throws IOException {

        org.apache.hadoop.conf.Configuration conf = new org.apache.hadoop.conf.Configuration();
        FileSystem fileSystem = FileSystem.get(conf);

//        RemoteIterator<LocatedFileStatus> iterator = fileSystem
//                .listFiles(new Path("/project/nxcals/nxcals_pro/staging"), true);
//
//        while(iterator.hasNext()){
//            LocatedFileStatus fileStatus = iterator.next();
//            System.out.println(fileStatus.getPath());
//        }
        long time = System.currentTimeMillis();
        FileStatus[] fileStatuses = fileSystem.globStatus(new Path("/project/nxcals/nxcals_pro/staging/*/*/*/*"));

        for(FileStatus stat : fileStatuses) {
            fileSystem.listStatus(stat.getPath());
        }

        System.out.println(fileStatuses[0].getPath() + " size: " + fileStatuses.length + " took " + (System.currentTimeMillis()-time));


    }

}
