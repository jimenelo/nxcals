pipeline {
    agent { label 'performance-test' }
    options {
        timeout(time: 360, unit: 'MINUTES')
    }

    environment {
        GITLAB_MR_USER_NAME = "${env.gitlabMergedByUser}"
        spring_profiles_active = "gitlab-ci"
        JAVA_HOME="${tool 'jdk1.11'}"
    }

    stages {
        stage('Build') {
            when {
                expression { params.PUBLISH == true }
            }

            environment {
                // set ci profile for service junit tests database resolution to nxcals_junit
                spring_profiles_active = "ci"
            }

            steps {
                timestamps {
                    retry(2) {
                        script {
                            def server = Artifactory.server 'artifactory'
                            def rtGradle = Artifactory.newGradleBuild()
                            def buildInfo = Artifactory.newBuildInfo()
                            rtGradle.tool = 'gradle' // Tool name from Jenkins configuration
                            rtGradle.useWrapper = true
                            rtGradle.deployer repo: 'ds-development-local', server: server
                            rtGradle.resolver repo: 'ds-release', server: server
                            rtGradle.deployer.artifactDeploymentPatterns.addExclude("*.tar")
                            //Trying to exclude ghost artefact from the main project dir
                            rtGradle.deployer.artifactDeploymentPatterns.addExclude("*nxcals-perf*")
                            rtGradle.deployer.deployMavenDescriptors = true
                            rtGradle.deployer.deployIvyDescriptors = false
                            rtGradle.run buildInfo: buildInfo, rootDir: ".", buildFile: 'build.gradle', tasks: 'clean install --no-daemon --parallel'
                            rtGradle.run buildInfo: buildInfo, rootDir: ".", buildFile: 'build.gradle', tasks: 'artifactoryPublish --no-daemon'
                            // Moved to above build (jwozniak)
                            // def publishInfo = rtGradle.run rootDir: ".", buildFile: 'build.gradle', tasks: 'distWheel artifactoryPublish --no-daemon'
                            // buildInfo.append(publishInfo)
                            buildInfo.retention maxBuilds: 20, maxDays: 30, deleteBuildArtifacts: true

                            def props = readProperties file: 'gradle.properties'
                            env.VERSION = props['version']

                            server.publishBuildInfo buildInfo
                        }
                    }
                }

            }
        }

        stage ("Deploy to perf test env") {
            when {
                expression { params.DEPLOY == true }
            }
            steps {
                withCredentials([file(credentialsId: 'NXCALS-ANSIBLE-VAULT-PASS', variable: 'VAULT_PASS')]) {
                    script {
                        def props = readProperties file: 'gradle.properties'
                        env.VERSION = props['version']
                        env.PYTIMBER_MAJOR_VERSION = props['pytimberMajorVersion']
                        dir('ansible') {
                            try {
                                sh '''
                                  source /opt/acc-py/venv/bin/activate
                                  ../gradlew install
                                  cat $VAULT_PASS > .nxcals-build-temp.txt
                                  chmod og-rwx .nxcals-build-temp.txt
                                  ansible-vault decrypt files/keytab-acclog-encrypted --vault-password-file=.nxcals-build-temp.txt
                                  kinit -f -r 5d -kt files/keytab-acclog-encrypted acclog && aklog
                                  ansible-playbook -i inventory/perf-test nxcals-full-perf-install-with-sudo.yml -e "application_version=$VERSION" -e pytimber_major_version="$PYTIMBER_MAJOR_VERSION" --vault-password-file=.nxcals-build-temp.txt
                               '''
                            } finally {
                                sh 'rm .nxcals-build-temp.txt || true'
                            }
                        }
                    }
                }
            }
        }
        stage ("Performance Test") {
            when {
                expression { params.RUN_TESTS == true }
            }
            steps {
                script {
                    env.GRADLE = tool 'gradle'
                    env.JENKINS_ENV = 'perf-test'
                    env.HOME = '/opt/nxcals/'
                    env.REPORT_FILE = 'jmh/reports/result.json'
                }
                withCredentials([file(credentialsId: 'NXCALS-ANSIBLE-VAULT-PASS', variable: 'VAULT_PASS')]) {
                    script {
                        sh script: "source /opt/acc-py/venv/bin/activate; ./performance-tests.sh -r $REPORT_FILE", returnStatus: true
                    }
                }
                jmhReport "performance-tests/build/$REPORT_FILE"
            }
        }
    }
}
