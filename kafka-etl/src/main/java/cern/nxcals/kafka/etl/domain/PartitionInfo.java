package cern.nxcals.kafka.etl.domain;

import cern.nxcals.api.domain.RecordKey;
import cern.nxcals.api.utils.TimeUtils;
import lombok.Value;

import java.time.Instant;

/**
 * Represents the data partition in the storage.
 */
@Value
public class PartitionInfo {

    //Used to mark that the partition cannot be created, usually due to invalid kafka record key (jwozniak)
    public static final PartitionInfo INVALID_PARTITION = new PartitionInfo(-1, -1, -1, -1, -1, false);

    long systemId;
    long partitionId;
    long schemaId;
    long timestampMillis; //in hours, must have some timestamp here to identify the correct partition between 2 different days.
    long timePartition; //with respect to the 24h day
    boolean updatable;

    public static PartitionInfo from(RecordKey key, TimeUtils.TimeSplits maxPartitions, boolean isUpdatable) {
        return from(key.getSystemId(), key.getPartitionId(), key.getSchemaId(), key.getTimestamp(), maxPartitions,
                isUpdatable);
    }

    //Used for testing only
    public static PartitionInfo from(long systemId, long partitionId, long schemaId, long timestamp,
            boolean isUpdatable) {
        return from(systemId, partitionId, schemaId, timestamp, TimeUtils.TimeSplits.FIVE_MINUTE_SPLIT, isUpdatable);
    }

    public static PartitionInfo from(long systemId, long partitionId, long schemaId, long timestamp,
            TimeUtils.TimeSplits maxPartitions, boolean isUpdatable) {
        long timePartition = TimeUtils.getTimePartition(timestamp, maxPartitions);
        return new PartitionInfo(systemId, partitionId, schemaId, TimeUtils.truncateToHours(timestamp), timePartition,
                isUpdatable);
    }

    public String getDate() {
        return dateAsString(Instant.ofEpochMilli(timestampMillis));
    }

    private String dateAsString(Instant instant) {
        return TimeUtils.getStringDateFromInstant(instant);
    }

}
