package cern.nxcals.kafka.etl.hdfs;

import cern.nxcals.kafka.etl.domain.PartitionInfo;
import lombok.RequiredArgsConstructor;
import org.apache.avro.Schema;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

@RequiredArgsConstructor
public class HdfsFileWriterProvider {
    private final Path basePath;
    private final FileSystem fileSystem;
    private final String compressionCodec;
    private final int syncIntervalSize;
    private final int fileBufferSize;

    public HdfsFileWriter createFor(PartitionInfo partition, Schema schema) {
        Path path = convertToPath(partition);
        return new HdfsFileWriter(
                fileSystem,
                new Path(basePath.toString() + Path.SEPARATOR + path.toString()),
                schema, compressionCodec, syncIntervalSize,fileBufferSize
        );
    }

    private Path convertToPath(PartitionInfo partitionInfo) {
        return new Path(partitionInfo.getSystemId() + Path.SEPARATOR
                + partitionInfo.getPartitionId() + Path.SEPARATOR
                + partitionInfo.getSchemaId() + Path.SEPARATOR
                + partitionInfo.getDate() + Path.SEPARATOR
                + partitionInfo.getTimePartition()
        );
    }
}
