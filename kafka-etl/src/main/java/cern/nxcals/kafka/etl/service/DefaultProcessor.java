package cern.nxcals.kafka.etl.service;

import cern.nxcals.common.metrics.MetricsRegistry;
import cern.nxcals.kafka.etl.domain.PartitionInfo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.common.TopicPartition;
import org.springframework.retry.RetryContext;
import org.springframework.retry.support.RetryTemplate;

import javax.annotation.concurrent.NotThreadSafe;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Stream;

import static java.util.stream.Collectors.groupingBy;

/**
 * The message processor that is instantiated per KafkaConsumerRunner.
 *
 * @param <K>
 * @param <V>
 */
@Slf4j
@RequiredArgsConstructor
@NotThreadSafe
public class DefaultProcessor<K, V> implements Processor<K, V> {
    public static final String PROCESS_ERRORS_COUNT = "process_errors";

    private static final AtomicLong processorCount = new AtomicLong(1);
    private final String processorId = "processor-" + processorCount.getAndIncrement();

    private final Partitioner<K, V> partitioner;
    private final List<Store<K, V>> stores;
    private final ExecutorService storeExecutor;
    private final MetricsRegistry metricsRegistry;
    private final RetryTemplate retryTemplate;
    private Set<PartitionInfo> usedPartitions = new HashSet<>();


    private void processTerminated(RetryContext retryContext, WriterTerminatedException ex) {
        log.warn("Looks like we are stopping the app, not repeating", ex);
        retryContext.setExhaustedOnly();
    }

    @Override
    public void flush() {
        retryTemplate.execute(retryContext -> {
            try {
                flushImpl();
            } catch(WriterTerminatedException ex) {
                processTerminated(retryContext, ex);
                throw ex;
            } catch (Exception ex) {
                processException(retryContext, ex, "Flush failed with exception, will be retried, retry count {}");
                throw ex;
            }
            return null;
        });
    }

    @Override
    public void process(ConsumerRecords<K, V> data) {
        retryTemplate.execute(retryContext -> {
            try {
                processImpl(data);
            } catch(WriterTerminatedException ex) {
                processTerminated(retryContext, ex);
                throw ex;
            } catch (Exception ex) {
                processException(retryContext, ex, "Processing records failed with exception, will be retried, retry count {}");
                throw ex;
            }
            return null;
        });
    }

    private void processException(RetryContext retryContext, Exception ex, String text)  {
        metricsRegistry.incrementCounterBy(PROCESS_ERRORS_COUNT, 1);
        log.warn(text, retryContext.getRetryCount(), ex);
    }

    private void flushImpl() {
        try {
            long time = System.currentTimeMillis();
            log.debug("Processor={}, flushing stores with {} used NXCALS partitions", processorId, usedPartitions.size());
            for (Store<K, V> store : stores) {
                store.flush(usedPartitions);
            }
            usedPartitions.clear();
            log.debug("Processor={}, flushing stores finished, took {} ms", processorId,
                    System.currentTimeMillis() - time);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    private void processImpl(ConsumerRecords<K, V> data) {
        long batchSize = data.count();

        long start = System.currentTimeMillis();
        log.trace("Processor={}, start processing records={}", processorId, batchSize);

        Stream<CompletableFuture<Void>[]> processPartitionRecordsTasks = data.partitions().stream()
                .map(partition -> getProcessPartitionRecordsTasks(partition, data.records(partition)));

        CompletableFuture[] tasks = processPartitionRecordsTasks.flatMap(Arrays::stream) // flatten
                .toArray(CompletableFuture[]::new);

        executeTasks(tasks);

        log.trace("Processor={}, finish processing records={} took={} ms", processorId,
                batchSize, System.currentTimeMillis() - start);
    }

    private CompletableFuture<Void>[] getProcessPartitionRecordsTasks(TopicPartition partition,
            List<ConsumerRecord<K, V>> data) {
        if (!data.isEmpty()) {
            long time = System.currentTimeMillis();

            Map<PartitionInfo, List<ConsumerRecord<K, V>>> dataPerStorePartition = groupByNxcalsPartition(data);

            log.trace(
                    "Processor={}, found {} NXCALS partitions in the Kafka TopicPartition {}, partitioning took {} ms, now storing data...",
                    processorId,
                    dataPerStorePartition.size(), partition, (System.currentTimeMillis() - time));

            usedPartitions.addAll(dataPerStorePartition.keySet());

            return createTasks(dataPerStorePartition);
        } else {
            return new CompletableFuture[0];
        }
    }

    private Map<PartitionInfo, List<ConsumerRecord<K, V>>> groupByNxcalsPartition(List<ConsumerRecord<K, V>> data) {
        Map<PartitionInfo, List<ConsumerRecord<K, V>>> partitionInfoListMap = data.parallelStream()
                .collect(groupingBy(partitioner::findPartition));

        if(partitionInfoListMap.containsKey(PartitionInfo.INVALID_PARTITION)) {
            List<ConsumerRecord<K,V>> invalid = partitionInfoListMap.get(PartitionInfo.INVALID_PARTITION);
            log.error("Invalid {} records in the data are removed, please check previous errors for more information", invalid.size());
            partitionInfoListMap.remove(PartitionInfo.INVALID_PARTITION);
        }
        return partitionInfoListMap;
    }

    private CompletableFuture[] createTasks(Map<PartitionInfo, List<ConsumerRecord<K, V>>> dataPerPartition) {
        CompletableFuture[] tasks = new CompletableFuture[dataPerPartition.size() * stores.size()];
        int i = 0;
        for (Store<K, V> store : stores) {
            for (Map.Entry<PartitionInfo, List<ConsumerRecord<K, V>>> entry : dataPerPartition.entrySet()) {
                tasks[i++] = CompletableFuture
                        .runAsync(() -> processStorePartition(store, entry.getKey(), entry.getValue()), storeExecutor);

            }
        }
        return tasks;
    }

    private void processStorePartition(Store<K, V> store, PartitionInfo partition, List<ConsumerRecord<K, V>> records) {
        try {
            long start = System.currentTimeMillis();
            store.save(partition, records);
            if (log.isTraceEnabled()) {
                log.trace("Processor={}, processing store partition {} with {} records took {} ms for store {}",
                        processorId, partition, records.size(), (System.currentTimeMillis() - start), store);
            }
        } catch(WriterTerminatedException ex) {
            throw ex;
        } catch (Exception e) {
            log.error("Cannot process records {} for partition {} and store {}", records.size(), partition, store);
            throw new IllegalStateException("Processor=" + processorId + ", cannot process partition " + partition, e);
        }
    }

    private void executeTasks(CompletableFuture[] tasks) {
        try {
            CompletableFuture.allOf(tasks).join();
        } catch (CompletionException e) {
            Throwable cause = e.getCause();
            if (cause instanceof WriterTerminatedException) {
                throw (WriterTerminatedException) cause;
            }
            throw new IllegalStateException(cause);
        }
    }
}
