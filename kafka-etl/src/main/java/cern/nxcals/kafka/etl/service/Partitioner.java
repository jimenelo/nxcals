package cern.nxcals.kafka.etl.service;

import cern.nxcals.kafka.etl.domain.PartitionInfo;
import org.apache.kafka.clients.consumer.ConsumerRecord;

/**
 * Gets a proper partition for a given record.
 *
 * @param <K>
 * @param <V>
 */
public interface Partitioner<K, V> {
    /**
     *
     * @param record
     * @return PartitionInfo for valid record and or PartitionInfo.INVALID_PARTITION for invalid key
     */
    PartitionInfo findPartition(ConsumerRecord<K, V> record);
}
