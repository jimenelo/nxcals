package cern.nxcals.kafka.etl.hbase;

import cern.nxcals.api.domain.RecordKey;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.extraction.metadata.queries.SystemSpecs;
import cern.nxcals.api.utils.TimeUtils;
import cern.nxcals.internal.extraction.metadata.InternalSystemSpecService;
import cern.nxcals.kafka.etl.config.HbaseProperties;
import cern.nxcals.kafka.etl.domain.PartitionInfo;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import com.google.common.annotations.VisibleForTesting;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericDatumReader;
import org.apache.avro.generic.GenericDatumWriter;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.io.BinaryDecoder;
import org.apache.avro.io.BinaryEncoder;
import org.apache.avro.io.DecoderFactory;
import org.apache.avro.io.EncoderFactory;
import org.apache.avro.util.Utf8;
import org.apache.commons.codec.digest.MurmurHash3;
import org.apache.commons.lang3.StringUtils;
import org.apache.hadoop.hbase.client.Put;
import org.apache.kafka.clients.consumer.ConsumerRecord;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;

import static cern.nxcals.common.Constants.HBASE_ROW_KEY_DELIMITER;
import static cern.nxcals.common.Schemas.ENTITY_ID;
import static cern.nxcals.common.utils.AvroUtils.deserializeFromAvro;
import static cern.nxcals.common.utils.AvroUtils.getSchemaTypeFor;
import static cern.nxcals.common.utils.AvroUtils.serializeToAvro;
import static java.util.Objects.requireNonNull;
import static org.apache.hadoop.hbase.util.Bytes.toBytes;

@Slf4j
public class RecordConverter implements Closeable {
    private static final int WRITER_CACHE_MAX_SIZE = 500;
    private static final String RECORD_VERSION_DEFAULT = "0";

    private final InternalSystemSpecService systemService;
    private final byte[] columnFamily;
    private final int saltBuckets;
    private final int oldSaltBuckets;
    private final long newSaltingSinceNanos;

    private final LoadingCache<Long, Optional<String>> recordVersionFieldCache;
    private final LoadingCache<Schema, GenericDatumWriter<Object>> writerCache;
    private final LoadingCache<Long, String> timestampCache;

    public RecordConverter(byte[] columnFamily, @NonNull InternalSystemSpecService systemService, int saltBuckets,
            int oldSaltBuckets, String newSaltingSince) {
        this.systemService = systemService;
        this.columnFamily = columnFamily;
        this.saltBuckets = saltBuckets;
        this.oldSaltBuckets = oldSaltBuckets;
        this.newSaltingSinceNanos = TimeUtils.getNanosFromString(newSaltingSince);

        writerCache = Caffeine.newBuilder()
                .maximumSize(WRITER_CACHE_MAX_SIZE)
                .build(GenericDatumWriter::new);
        timestampCache = Caffeine.newBuilder()
                .build(this::getTimestamp);
        recordVersionFieldCache = Caffeine.newBuilder()
                .build(this::getSystemRecordVersion);
    }

    private void logRecord(ConsumerRecord<byte[], byte[]> record, PartitionInfo partition, UncheckedIOException orgException) {
        Object key = record.key();
        try {
            key = RecordKey.deserialize(record.key());
        } catch (Exception ex) {
            log.error("Cannot deserialize record key", ex);
        }
        log.error("Cannot deserialize record for partition={}, record key={}, value={}", partition, key, record.value(),
                orgException);
    }

    List<Put> toPuts(List<ConsumerRecord<byte[], byte[]>> records, Schema schema, PartitionInfo partition,
            AtomicLong deserializeTime, AtomicLong serializeTime) {
        BinaryDecoder decoder = emptyDecoder();
        BinaryEncoder encoder = emptyEncoder();

        GenericDatumReader<GenericRecord> reader = new GenericDatumReader<>(schema);
        String timestamp = timestampCache.get(partition.getSystemId());

        List<Put> result = new ArrayList<>();

        GenericRecord genericRecord = null;
        for (ConsumerRecord<byte[], byte[]> consumerRecord : records) {
            try {
                long start = System.currentTimeMillis();
                genericRecord = deserializeFromAvro(consumerRecord.value(), reader, decoder, genericRecord);
                long end = System.currentTimeMillis();
                deserializeTime.addAndGet(end - start);
            } catch (UncheckedIOException ex) {
                logRecord(consumerRecord, partition, ex);
                //skip the record as it is broken, later on we could think what to do with such records, like dead letter box.
                continue;
            }
            String recordVersionField = getRecordVersionField(genericRecord, partition.getSystemId());
            long start = System.currentTimeMillis();
            Put put = toPut(genericRecord, schema, recordVersionField, timestamp, encoder);
            long end = System.currentTimeMillis();
            serializeTime.addAndGet(end - start);
            result.add(put);
        }

        return result;
    }

    private BinaryDecoder emptyDecoder() {
        return DecoderFactory.get().binaryDecoder(new ByteArrayInputStream(new byte[0]), null);
    }

    private BinaryEncoder emptyEncoder() {
        return EncoderFactory.get().directBinaryEncoder(new ByteArrayOutputStream(0), null);
    }

    private Put toPut(GenericRecord record, Schema recordSchema, String version, String timestampField,
            BinaryEncoder encoder) {
        long timestampNanos = (long) record.get(timestampField);
        long timestampMillis = timestampNanos / 1_000_000L;
        Put put = new Put(rowKey(record, version, timestampNanos), timestampMillis);
        for (Schema.Field field : recordSchema.getFields()) {
            Schema schema = field.schema();
            log.trace("Encoding field: {}, value: {}, schema: {}, schema type: {}", field.name(), field, schema,
                    schema.getType());
            byte[] serialized = serialize(record.get(field.name()), schema, encoder, schema.getType());
            put.addColumn(columnFamily, toBytes(field.name()), serialized);
        }
        return put;
    }

    private byte[] rowKey(GenericRecord record, String version, long timestampNanos) {
        String salt = getSaltingBasedOnTimestamp(timestampNanos);
        long key = Long.MAX_VALUE - timestampNanos;
        Object entityId = record.get(ENTITY_ID.getFieldName());
        return toBytes(salt + entityId + HBASE_ROW_KEY_DELIMITER + key + HBASE_ROW_KEY_DELIMITER + version);
    }

    // old and new salting buckets are used for a smooth transition between different salt buckets
    // and ensures that the duplicated records are always sent to the same bucket (as they have the same timestamp)
    @VisibleForTesting
    String getSaltingBasedOnTimestamp(long timestampNanos) {
        if (saltBuckets != HbaseProperties.NO_SALT_BUCKETS) {
            int hash = MurmurHash3.hash32(timestampNanos);

            if (timestampNanos < newSaltingSinceNanos) {
                return Math.abs(hash % oldSaltBuckets) + HBASE_ROW_KEY_DELIMITER;
            } else {
                return Math.abs(hash % saltBuckets) + HBASE_ROW_KEY_DELIMITER;
            }
        } else {
            return "";
        }
    }

    private byte[] serialize(Object fieldValue, Schema schema, BinaryEncoder encoder, Schema.Type type) {
        if (fieldValue == null) {
            return new byte[0]; // null and byte[0] are equivalent in hbase
        }

        switch (type) {
            case RECORD:
            case ENUM:
            case ARRAY:
            case MAP:
            case FIXED:
                return serializeToAvro(fieldValue, encoder, requireNonNull(writerCache.get(schema)));
            case UNION:
                // Note that original union schema will be used. Only the schema type changes.
                // This means we still encode [type, null] unions as such
                return serialize(fieldValue, schema, encoder, getSchemaTypeFor(schema));
            case STRING:
                return toBytes(((Utf8) fieldValue).toString());
            case BYTES:
                return toBytes((ByteBuffer) fieldValue);
            case INT:
                return toBytes((int) fieldValue);
            case LONG:
                return toBytes((long) fieldValue);
            case FLOAT:
                return toBytes((float) fieldValue);
            case DOUBLE:
                return toBytes((double) fieldValue);
            case BOOLEAN:
                return toBytes((boolean) fieldValue);
            case NULL:
                return new byte[0]; // null and byte[0] are equivalent in hbase
            default:
                throw new UncheckedIOException(new IOException(String.format("Unable to serialize avro data (%s). Unknown type: %s", fieldValue, type)));
        }
    }

    private String getRecordVersionField(GenericRecord genericRecord, long systemId) {
        return requireNonNull(recordVersionFieldCache.get(systemId))
                .map(genericRecord::get)
                .map(Object::toString)
                .orElse(RECORD_VERSION_DEFAULT);
    }

    private Optional<String> getSystemRecordVersion(long systemId) {
        SystemSpec systemSpec = systemService.findOne(SystemSpecs.suchThat().id().eq(systemId)).orElseThrow(() -> new IllegalArgumentException("No such system id " + systemId));
        String recordVersionKeyDefinitions = systemSpec.getRecordVersionKeyDefinitions();
        if (StringUtils.isEmpty(recordVersionKeyDefinitions)) {
            return Optional.empty();
        }
        Schema recordVersionKeyDefSchema = new Schema.Parser().parse(recordVersionKeyDefinitions);
        return recordVersionKeyDefSchema.getFields().stream().map(Schema.Field::name).findFirst();
    }

    private String getTimestamp(long systemId) {
        SystemSpec systemSpec = systemService.findOne(SystemSpecs.suchThat().id().eq(systemId)).orElseThrow(() -> new IllegalArgumentException("No such system id " + systemId));
        Schema timeKeySchema = new Schema.Parser().parse(systemSpec.getTimeKeyDefinitions());
        return timeKeySchema.getFields().stream()
                .map(Schema.Field::name)
                .findFirst()
                .orElseThrow(() -> new RuntimeException("Cannot obtain time schema for system id" + systemId));
    }

    @Override
    public void close() {
        writerCache.invalidateAll();
        writerCache.cleanUp();
        timestampCache.invalidateAll();
        timestampCache.cleanUp();
    }
}
