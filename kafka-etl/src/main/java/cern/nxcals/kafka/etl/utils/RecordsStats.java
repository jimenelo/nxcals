package cern.nxcals.kafka.etl.utils;

import cern.nxcals.api.domain.RecordKey;
import cern.nxcals.api.utils.TimeUtils;
import lombok.Value;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;

import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.LongSummaryStatistics;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.summarizingInt;
import static java.util.stream.Collectors.summarizingLong;

/**
 * Used to generate records statistics per entityId from Kafka records.
 */
@UtilityClass
@Slf4j
public class RecordsStats {

    public Stream<EntityStats> from(List<ConsumerRecord<byte[], byte[]>> records) {
        List<RecordInfo> recordInfos = records.stream()
                .map(RecordInfo::from).collect(Collectors.toList());

        Map<Long, IntSummaryStatistics> sizeStats = recordInfos.stream()
                .collect(groupingBy(RecordInfo::getEntityId,
                        summarizingInt(RecordInfo::getRecordSize)));

        Map<Long, LongSummaryStatistics> timeStats = recordInfos.stream()
                .collect(groupingBy(RecordInfo::getEntityId,
                        summarizingLong(RecordInfo::getTimestamp)));

        return sizeStats.entrySet().stream()
                .map(e -> EntityStats.of(e.getKey(), e.getValue(), timeStats.get(e.getKey())));
    }

    public String asString(Stream<EntityStats> stats) {
        return stats.map(RecordsStats.EntityStats::toString)
                .collect(Collectors.joining("," + System.lineSeparator()));
    }

    @Value(staticConstructor = "of")
    private static class RecordInfo {
        long entityId;
        int recordSize;
        long timestamp;

        public static RecordInfo from(ConsumerRecord<byte[], byte[]> record) {
            RecordKey recordKey = RecordKey.deserialize(record.key());
            return RecordInfo.of(recordKey.getEntityId(), record.value().length, recordKey.getTimestamp());
        }
    }

    @Value(staticConstructor = "of")
    public static class EntityStats {
        long entityId;
        long recCount;
        long totalSize;
        long minSize;
        long maxSize;
        long avgSize;

        long minTs;
        long maxTs;

        private static EntityStats of(long entityId, IntSummaryStatistics sizeStats, LongSummaryStatistics timeStats) {
            return EntityStats
                    .of(entityId, sizeStats.getCount(), sizeStats.getSum(), sizeStats.getMin(), sizeStats.getMax(),
                            Math.round(sizeStats.getAverage()), timeStats.getMin(), timeStats.getMax());
        }

        /**
         * Proportion used to calculate rec/sec.
         **/
        public double getRecPerSecond() {
            long timeSpanInNanos = maxTs - minTs;
            if (timeSpanInNanos > 0) {
                return ((recCount - 1) * 1_000_000_000.0) / timeSpanInNanos;
            } else {
                return 0; //cannot calculate
            }
        }

        public double getBytesPerSecond() {
            return getRecPerSecond() * avgSize;
        }

        public String toString() {
            return String.format(
                    "EntityStats[ entityId=%s, recCount=%s, totalSize=%s, minSize=%s, maxSize=%s, avgSize=%s, minTs=%s, maxTS=%s, rec/sec=%.2f, bytes/sec=%.2f ]",
                    entityId, recCount, totalSize, minSize, maxSize, avgSize, TimeUtils.getInstantFromNanos(minTs),
                    TimeUtils.getInstantFromNanos(maxTs), getRecPerSecond(), getBytesPerSecond());

        }

    }

}
