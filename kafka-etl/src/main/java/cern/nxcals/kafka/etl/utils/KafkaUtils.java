package cern.nxcals.kafka.etl.utils;

import cern.nxcals.common.utils.CollectionUtils;
import lombok.experimental.UtilityClass;
import org.apache.kafka.clients.consumer.ConsumerRecord;

import java.util.List;

@UtilityClass
public class KafkaUtils {

    public static <K, V> List<List<ConsumerRecord<K, V>>> splitIntoParts(
            List<ConsumerRecord<K, V>> records, long maxPartSizeInBytes, long maxPartSize) {
        return CollectionUtils.splitIntoParts(records, maxPartSizeInBytes, maxPartSize,
                cr -> (long) cr.serializedValueSize());
    }
}
