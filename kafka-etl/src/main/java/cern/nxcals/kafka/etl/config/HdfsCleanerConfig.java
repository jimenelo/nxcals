package cern.nxcals.kafka.etl.config;

import cern.nxcals.common.utils.DirectoryIterator;
import cern.nxcals.common.utils.HdfsCleaner;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.time.DurationFormatUtils;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;

import java.io.IOException;
import java.util.function.Predicate;

@Configuration
@Profile("hdfs")
@Slf4j
public class HdfsCleanerConfig {
    private static final Predicate<FileStatus> DO_NOT_REMOVE_IF = child -> child.isDirectory() || hasExtension(child, ".avro") || hasExtension(child, ".parquet");

    @Bean(name = "cleaner")
    @Order(Ordered.HIGHEST_PRECEDENCE)
    public HdfsCleaner cleaner(HdfsProperties props, FileSystem fs) {
        String root = props.getBasePath();

        log.info("Directory Cleaner | Directory to clean: {}", root);

        try {
            HdfsCleaner cleaner = new HdfsCleaner(new DirectoryIterator(fs, new Path(root), DO_NOT_REMOVE_IF));
            cleaner.run();
            log.info("Directory Cleaner | cleanup took: {}, directories removed: {}",
                    DurationFormatUtils.formatDurationHMS(cleaner.timeUsed()), cleaner.removedCount());
            return cleaner;
        } catch (IOException ex) {
            log.error("Directory Cleaner | Error accessing {}. Skipping cleaning", root, ex);
        }
        return null;
    }

    private static boolean hasExtension(FileStatus child, String s) {
        return child.getPath().getName().endsWith(s);
    }
}
