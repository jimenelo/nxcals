package cern.nxcals.kafka.etl.service;

import cern.nxcals.api.domain.RecordKey;
import cern.nxcals.api.utils.TimeUtils;
import cern.nxcals.internal.extraction.metadata.InternalPartitionService;
import cern.nxcals.kafka.etl.domain.PartitionInfo;
import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;

import java.util.concurrent.TimeUnit;

@Slf4j
@RequiredArgsConstructor
public class DefaultPartitioner implements Partitioner<byte[], byte[]> {
    private final TimeUtils.TimeSplits dailyPartitions;
    //A date from which we will have the configured new time partitioning
    private final long newTimeSplitsFrom;
    private final InternalPartitionService partitionService;
    private static final Cache<Long, Boolean> partitionsCache = Caffeine.newBuilder()
            .expireAfterAccess(6, TimeUnit.HOURS).build();
    @Override
    public PartitionInfo findPartition(ConsumerRecord<byte[], byte[]> record) {
        RecordKey key;
        try {
            key = RecordKey.deserialize(record.key());
        } catch (Exception e) {
            log.error("Cannot deserialize record key {} with value {}", record.key(), record.value(), e);
            //Has to return this to allow the process of partitioning to continue in the case of broken records (jwozniak)
            return PartitionInfo.INVALID_PARTITION;
        }
        long partitionId = key.getPartitionId();
        boolean isUpdatable = partitionsCache.get(partitionId, p -> partitionService.findById(partitionId)
                .orElseThrow(() -> new IllegalArgumentException("No partition with id " + partitionId + " found"))
                .getProperties().isUpdatable());

        if (key.getTimestamp() >= newTimeSplitsFrom) {
            return PartitionInfo.from(key, dailyPartitions, isUpdatable);
        } else {
            //old schema with 24 hours
            return PartitionInfo.from(key, TimeUtils.TimeSplits.ONE_HOUR_SPLIT, isUpdatable);
        }
    }
}
