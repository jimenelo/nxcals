package cern.nxcals.kafka.etl.config;

import org.apache.commons.lang3.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import static cern.nxcals.kafka.etl.config.HbaseProperties.DEFAULT_WRITER_NAME;

public class HbasePropertiesValidator implements Validator {

    private static final String GENERAL_CONFIGURATION_ERROR = "general configuration error";
    private static final String WRITER_CONFIGURATION_ERROR = "writer configuration error";
    private static final String THREAD_POOL_CONFIGURATION_ERROR = "thread pool configuration error";
    private static final String CONNECTION_POOL_CONFIGURATION_ERROR = "connection pool configuration error";

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz == HbaseProperties.class;
    }

    @Override
    public void validate(Object target, Errors errors) {
        HbaseProperties properties = (HbaseProperties) target;

        validate(properties, errors);

        for (HbaseProperties.WriterProperties p : properties.getWriters().values()) {
            validate(p, errors);

            if (!properties.getExecutors().containsKey(p.getExecutorName())) {
                errors.reject(WRITER_CONFIGURATION_ERROR, "unknown executor name: " + p.getExecutorName());
            }

            if (!properties.getConnectionPools().containsKey(p.getConnectionPoolName())) {
                errors.reject(WRITER_CONFIGURATION_ERROR, "unknown connection pool name: " + p.getConnectionPoolName());
            }
        }

        for (HbaseProperties.ExecutorProperties p : properties.getExecutors().values()) {
            validate(p, errors);
        }

        for (HbaseProperties.ConnectionProperties p : properties.getConnectionPools().values()) {
            validate(p, errors);
        }
    }

    private void validate(HbaseProperties properties, Errors errors) {
        if (!properties.getWriters().containsKey(DEFAULT_WRITER_NAME)) {
            errors.reject(GENERAL_CONFIGURATION_ERROR, "missing default writer configuration");
        }

        if (StringUtils.isEmpty(properties.getColumnFamily())) {
            errors.reject(GENERAL_CONFIGURATION_ERROR, "empty column family");
        }

        if (StringUtils.isEmpty(properties.getNamespace())) {
            errors.reject(GENERAL_CONFIGURATION_ERROR, "empty namespace");
        }

        if (properties.getTtlHours() < 1) {
            errors.reject(GENERAL_CONFIGURATION_ERROR, "ttl hours negative");
        }

        if (properties.getSaltBuckets() < -1 || properties.getSaltBuckets() == 0) {
            errors.reject(GENERAL_CONFIGURATION_ERROR, "number of salt buckets less than -1 or equals to 0");
        }

        if (properties.isMobEnabled() && properties.getMobThreshold() < 0) {
            errors.reject(GENERAL_CONFIGURATION_ERROR, "MOB threshold is negative");
        }
    }

    private void validate(HbaseProperties.WriterProperties properties, Errors errors) {
        if (StringUtils.isEmpty(properties.getExecutorName())) {
            errors.reject(WRITER_CONFIGURATION_ERROR, "empty thread pool name");
        }

        if (StringUtils.isEmpty(properties.getConnectionPoolName())) {
            errors.reject(WRITER_CONFIGURATION_ERROR, "empty connection pool name");
        }
    }

    private void validate(HbaseProperties.ExecutorProperties properties, Errors errors) {
        if (properties.getThreadCount() < 1) {
            errors.reject(THREAD_POOL_CONFIGURATION_ERROR, "thread count negative");
        }
    }

    private void validate(HbaseProperties.ConnectionProperties properties, Errors errors) {
        if (properties.getMaxConnections() < 1) {
            errors.reject(CONNECTION_POOL_CONFIGURATION_ERROR, "max connection count negative");
        }
    }
}
