package cern.nxcals.kafka.etl.config;

import cern.nxcals.common.config.FileSystemConfig;
import cern.nxcals.common.metrics.MetricsPulse;
import cern.nxcals.internal.extraction.metadata.InternalEntitySchemaService;
import cern.nxcals.internal.extraction.metadata.InternalSystemSpecService;
import cern.nxcals.kafka.etl.hdfs.HdfsFileWriterProvider;
import cern.nxcals.kafka.etl.hdfs.HdfsStore;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.Scope;

@Configuration
@EnableConfigurationProperties(HdfsProperties.class)
@Profile("hdfs")
@Import(FileSystemConfig.class)
@DependsOn({ "kerberos" })
public class HdfsStoreConfig {
    private final InternalSystemSpecService systemService;
    private final InternalEntitySchemaService schemaService;

    @Autowired
    public HdfsStoreConfig(InternalSystemSpecService systemService, InternalEntitySchemaService schemaService) {
        this.systemService = systemService;
        this.schemaService = schemaService;
    }

    @Bean
    public HdfsFileWriterProvider provider(HdfsProperties props, FileSystem fileSystem) {
        return new HdfsFileWriterProvider(new Path(props.getBasePath()), fileSystem, props.getCompressionCodec(), props.getSyncIntervalBytes(), props.getFileBufferSize());
    }

    @Bean
    @Scope("prototype")
    @Profile("multi-store")
    public HdfsStore storeMulti(HdfsFileWriterProvider provider, MetricsPulse metricsPulse, HdfsProperties props) {
        return store(provider, metricsPulse, props);
    }

    @Bean
    @Profile("!multi-store")
    public HdfsStore storeSingle( HdfsFileWriterProvider provider, MetricsPulse metricsPulse, HdfsProperties props) {
        return store(provider, metricsPulse, props);
    }

    private HdfsStore store(HdfsFileWriterProvider provider, MetricsPulse metricsPulse, HdfsProperties props) {
        return new HdfsStore(systemService,schemaService, props, provider, metricsPulse);
    }
}
