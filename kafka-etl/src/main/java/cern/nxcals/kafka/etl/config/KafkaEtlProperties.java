package cern.nxcals.kafka.etl.config;

import cern.nxcals.api.utils.TimeUtils;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

/**
 * Created by msobiesz on 29/10/18.
 */
@ConfigurationProperties("nxcals.kafka.etl")
@Data
@NoArgsConstructor
public class KafkaEtlProperties {
    @NonNull
    private Properties consumerProperties;
    @NonNull
    private String topicPattern;
    private long pollTimeout;

    private int concurrency;

    private RetryProperties retry;

    //executor
    private int executorThreadPoolSize;
    private int executorQueueSize;

    //flush
    private int flushInterval;
    private TimeUnit flushIntervalUnit;

    //orchestrator
    private int rescheduleDelay;
    private TimeUnit rescheduleDelayUnit;

    //debugging
    private int maxRecordsToDisplay;
    //sensible default
    private Duration longWriteThreshold = Duration.of(30, ChronoUnit.SECONDS);

    //How many time partitions we will have per day (previously it was hours, now we changed to more fine-grained)
    private TimeUtils.TimeSplits dailyPartitions;

    //The new time bucketing will be since that date, to be set in the config.
    private String timeBucketSince;

    @Data
    @NoArgsConstructor
    public static class RetryProperties {
        private Duration backoffInitialDelay;
        private Duration backoffMaxDelay;
        private Duration maxExecutionTime;
    }
}
