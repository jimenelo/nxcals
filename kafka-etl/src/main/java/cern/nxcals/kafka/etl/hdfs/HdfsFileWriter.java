package cern.nxcals.kafka.etl.hdfs;

import cern.nxcals.kafka.etl.service.WriterTerminatedException;
import com.google.common.annotations.VisibleForTesting;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.avro.Schema;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.Closeable;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.UUID;

/**
 * Writer that can closes file after specified time.
 */
@Slf4j
@RequiredArgsConstructor
public class HdfsFileWriter implements Closeable {
    private static final String SEP = "-";
    private static final int BUFFER_SIZE = 128 * 1024;

    private final FileSystem fileSystem;
    private final Path basePath;
    private final Schema schema;
    private final String compressionCodec;
    private final int syncIntervalBytes;
    private final int fileBufferSize;

    private AvroOutputStreamWriter streamWriter;
    private FSDataOutputStream file;
    private String fileName;

    @Getter
    private Path fullPath;
    @Getter
    private boolean terminated = false;

    public synchronized void write(byte[] record) {
        verifyNonTerminated();

        if (isClosed()) {
            open();
        }

        try {
            streamWriter.write(record);
        } catch (IOException ex) {
            log.error("Cannot write data to {}", fileName, ex);
            throw new UncheckedIOException("Cannot write data to " + fileName, ex);
        }
    }

    public synchronized void terminate() {
        close();
        terminated = true;
    }

    private synchronized void open() {
        verifyNonTerminated();

        fileName = generateFileName();
        fullPath = new Path(basePath.toString() + Path.SEPARATOR + fileName);
        try {
            fileSystem.mkdirs(new Path(basePath.toString()));
            file = fileSystem.create(fullPath, true, this.fileBufferSize);
            log.debug("Created new file {}", fullPath);
            streamWriter = newStreamWriter();
        } catch (IOException e) {
            throw new UncheckedIOException("Cannot open file " + fullPath, e);
        }
    }

    @VisibleForTesting
    AvroOutputStreamWriter newStreamWriter() throws IOException {
        return new AvroOutputStreamWriter(basePath.toString(), schema, file, compressionCodec, syncIntervalBytes);
    }

    @Override
    public synchronized void close() {
        if (!isClosed() && !isTerminated()) {
            log.debug("Closing file {}/{}", basePath, fileName);
            try {
                streamWriter.close();
                file.close();
            } catch (IOException e) {
                log.error("Cannot close file {}/{}", basePath, fileName, e);
                throw new IllegalStateException(e);
            } finally {
                streamWriter = null;
                file = null;
            }
        }
    }

    private String generateFileName() {
        long currentTime = System.currentTimeMillis();
        return "data" + SEP + currentTime + SEP + UUID.randomUUID() + SEP + compressionCodec + ".avro";
    }

    private synchronized boolean isClosed() {
        return this.streamWriter == null;
    }

    private void verifyNonTerminated() {
        if (isTerminated()) {
            String message = String.format("Using terminated writer to %s/%s", basePath, fileName);
            log.error(message);
            throw new WriterTerminatedException(message);
        }
    }
}
