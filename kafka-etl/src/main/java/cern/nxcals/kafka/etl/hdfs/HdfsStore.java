package cern.nxcals.kafka.etl.hdfs;

import cern.nxcals.common.metrics.MetricsPulse;
import cern.nxcals.internal.extraction.metadata.InternalEntitySchemaService;
import cern.nxcals.internal.extraction.metadata.InternalSystemSpecService;
import cern.nxcals.kafka.etl.config.HdfsProperties;
import cern.nxcals.kafka.etl.domain.PartitionInfo;
import cern.nxcals.kafka.etl.service.AbstractStore;
import cern.nxcals.kafka.etl.service.Store;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import com.github.benmanes.caffeine.cache.RemovalCause;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;

import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

@Slf4j
public class HdfsStore extends AbstractStore implements Store<byte[], byte[]> {

    private static final String OPEN_FILES_METRIC_NAME = "hdfs_open_files";
    private final HdfsFileWriterProvider provider;
    private LoadingCache<PartitionInfo, HdfsFileWriter> writers;

    public HdfsStore(InternalSystemSpecService systemService, InternalEntitySchemaService schemaService, HdfsProperties props,
                     HdfsFileWriterProvider provider, MetricsPulse metricsPulse) {
        super(schemaService, systemService);
        this.provider = Objects.requireNonNull(provider);

        this.writers = createCache(props);

        if (metricsPulse != null) {
            metricsPulse.register(OPEN_FILES_METRIC_NAME, writers::estimatedSize, 5, TimeUnit.SECONDS);
        }
        log.debug("Created HdfsStore {}", this);
    }

    private LoadingCache<PartitionInfo, HdfsFileWriter> createCache(HdfsProperties props) {
        Caffeine<Object, Object> builder = Caffeine.newBuilder();

        if (props.getMaxFiles() > 0) {
            builder = builder.maximumSize(props.getMaxFiles());
        }

        if (props.getFileExpireAfter() > 0) {
            builder = builder.expireAfterAccess(props.getFileExpireAfter(), props.getFileExpireAfterUnit());
        }
        return builder.removalListener(this::onRemoval)
                .build(p -> provider.createFor(p, getSchema(p)));
    }

    @SuppressWarnings({"squid:S2445", "squid:S1172"}) //sync on writer and unused parameter
    private void onRemoval(PartitionInfo key, HdfsFileWriter writer, RemovalCause removalCause) {
        synchronized (writer) {
            log.debug("Cache eviction - removing writer ({}) with cause {}", writer.getFullPath(), removalCause);
            writer.terminate();
        }
    }

    @Override
    @SuppressWarnings("squid:S2445") // sync on parameter
    public void save(PartitionInfo partition, List<ConsumerRecord<byte[], byte[]>> consumerRecords)
            throws IOException {
        HdfsFileWriter writer = writers.get(partition);
        synchronized (writer) {
            for (ConsumerRecord<byte[], byte[]> record : consumerRecords) {
                writer.write(record.value());
            }
        }
    }

    @Override
    public void flush(Set<PartitionInfo> usedPartitions) throws IOException {
        try {
            int errorCount = getForkJoinPool().submit(() -> usedPartitions.parallelStream().mapToInt(partitionInfo -> {
                HdfsFileWriter writer = writers.getIfPresent(partitionInfo);
                if (writer != null) {
                    try {
                        writer.close();
                    } catch (Exception e) {
                        log.error("Error during flushing writer for partition {}", partitionInfo, e);
                        return 1;
                    }
                }
                //It can happen that the writer for this partition is closed and removed from the cache.
                //This is fine, we don't have to flush it.
                return 0;
            }).sum()).get();

            if (errorCount > 0) {
                throw new IOException("Errors (" + errorCount + ") in flushing the writers, check log for exceptions");
            }
        } catch (InterruptedException e) {
            log.error("Interrupted!", e);
            // Restore interrupted state...
            Thread.currentThread().interrupt();
            throw new IllegalStateException(e);
        } catch (ExecutionException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void close() {
        writers.invalidateAll();
        writers.cleanUp();
    }
}
