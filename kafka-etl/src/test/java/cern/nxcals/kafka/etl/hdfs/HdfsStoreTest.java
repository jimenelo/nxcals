package cern.nxcals.kafka.etl.hdfs;

import cern.nxcals.api.domain.EntitySchema;
import cern.nxcals.common.metrics.MetricsPulse;
import cern.nxcals.internal.extraction.metadata.InternalEntitySchemaService;
import cern.nxcals.internal.extraction.metadata.InternalSystemSpecService;
import cern.nxcals.kafka.etl.TestDataGenerator;
import cern.nxcals.kafka.etl.config.HdfsProperties;
import cern.nxcals.kafka.etl.domain.PartitionInfo;
import org.apache.avro.Schema;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.util.Optional;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static cern.nxcals.common.Schemas.ENTITY_ID;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class HdfsStoreTest {
    private final String schemaString = "{"
            + "\"type\": \"record\", "
            + "\"name\": \"test_pair\","
            + "\"fields\":"
            + "["
            + "{\"name\": \"timestamp\", \"type\": \"long\"},"
            + "{\"name\": \"" + ENTITY_ID.getFieldName() + "\", \"type\": \"string\"}"
            + "]"
            + "}";

    private final Schema schema = new Schema.Parser().parse(schemaString);

    private TestDataGenerator generator = new TestDataGenerator();

    @Mock
    private HdfsFileWriterProvider provider;

    @Mock
    private HdfsFileWriter writer;

    @Mock
    private InternalEntitySchemaService schemaService;
    @Mock
    private InternalSystemSpecService systemService;

    @Mock
    private EntitySchema schemaData;


    @Test
    public void shouldFailOnNullProvider() {
        HdfsProperties props = getProps();
        assertThrows(NullPointerException.class, () ->
                new HdfsStore(systemService, schemaService, props, null, mock(MetricsPulse.class)));
    }

    @Test
    public void shouldAcceptNullMetrics() {
        new HdfsStore(systemService,schemaService, getProps(), provider, null);
    }

    @Test
    public void shouldRegisterWithMetricsPulse() {
        MetricsPulse metricsPulse = mock(MetricsPulse.class);
        new HdfsStore(systemService,schemaService, getProps(), provider, metricsPulse);
        verify(metricsPulse, times(1)).register(any(), any(), anyInt(), any());
    }

    @Nested
    class HdfsStoreTestNested {

        @BeforeEach
        public void initMocks() {
            when(provider.createFor(any(), any())).thenReturn(writer);
            when(schemaService.findOne(any())).thenReturn(Optional.of(schemaData));
            when(schemaData.getSchemaJson()).thenReturn(schemaString);
        }

        @Test
        public void shouldDeserializeAndStoreNewData() throws IOException {
            HdfsStore hdfsStore = new HdfsStore(systemService, schemaService, getProps(), provider, null);

            PartitionInfo partitionInfo = PartitionInfo.from(1, 2, 3, generator.nowNanos(), false);

            int size = 100;
            hdfsStore.save(partitionInfo, generator.randomData(size, 0, schema));

            verify(writer, times(size)).write(any());
        }

        @Test
        public void shouldTerminateOnWriterCachePurge() throws IOException, InterruptedException {
            HdfsStore hdfsStore = new HdfsStore(systemService, schemaService, getProps(), provider, null);

            PartitionInfo partitionInfo = PartitionInfo.from(1, 2, 3, generator.nowNanos(), false);

            int size = 100;
            hdfsStore.save(partitionInfo, generator.randomData(size, 0, schema));

            verify(writer, times(size)).write(any());

            CountDownLatch terminateLatch = new CountDownLatch(1);
            doAnswer(invocationOnMock -> {
                terminateLatch.countDown();
                return null;
            }).when(writer).terminate();

            hdfsStore.close();
            terminateLatch.await(10, TimeUnit.SECONDS);

            verify(writer, times(1)).terminate();
        }
    }

    private HdfsProperties getProps() {
        HdfsProperties props = new HdfsProperties();
        props.setFileExpireAfter(1);
        props.setFileExpireAfterUnit(TimeUnit.DAYS);
        props.setMaxFiles(100);
        return props;
    }

}