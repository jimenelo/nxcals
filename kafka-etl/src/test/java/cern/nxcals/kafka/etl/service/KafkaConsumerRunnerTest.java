package cern.nxcals.kafka.etl.service;

import cern.nxcals.kafka.etl.config.KafkaEtlProperties;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.errors.WakeupException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Pattern;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class KafkaConsumerRunnerTest {

    private static final Long OFFSET1 = 1L;
    private static final Long OFFSET2 = 2L;

    @Mock
    private Consumer<byte[], byte[]> kafkaConsumer;
    @Mock
    private Processor<byte[], byte[]> messageProcessor;
    @Mock
    private ScheduledExecutorService executor;
    @Mock
    private ConsumerRecords<byte[], byte[]> records;
    @Mock
    private ConsumerRecord<byte[], byte[]> record1, record2;

    private TopicPartition tp1, tp2;

    @Nested
    class KafkaConsumerRunnerTestNested {
        @BeforeEach
        public void setUp() {
            tp1 = new TopicPartition("tp1", 1);
            tp2 = new TopicPartition("tp2", 2);
            Set<TopicPartition> tps = new HashSet<>();
            tps.add(tp1);
            tps.add(tp2);
            when(records.partitions()).thenReturn(tps);

            //records
            List<ConsumerRecord<byte[], byte[]>> consumerRecordsTp1 = new ArrayList<>();
            List<ConsumerRecord<byte[], byte[]>> consumerRecordsTp2 = new ArrayList<>();

            consumerRecordsTp1.add(record1);
            consumerRecordsTp2.add(record2);

            when(records.records(tp1)).thenReturn(consumerRecordsTp1);
            when(records.records(tp2)).thenReturn(consumerRecordsTp2);

            when(record1.offset()).thenReturn(OFFSET1);
            when(record2.offset()).thenReturn(OFFSET2);
        }

        @Test
        public void shouldRunAndCommitOffsets() throws InterruptedException {
            //given
            when(kafkaConsumer.poll(argThat(a->true))).thenReturn(records);
            when(records.isEmpty()).thenReturn(false);

            KafkaConsumerRunner<byte[], byte[]> runner = createRunner(0, TimeUnit.NANOSECONDS);
            CountDownLatch latch = getMessageProcessedLatch(false);

            //when
            run(runner);
            latch.await(10, TimeUnit.SECONDS);

            //then
            verify(kafkaConsumer, times(1)).subscribe((Pattern) argThat(p -> true), argThat(r -> r == runner));
            verify(messageProcessor, atLeastOnce()).process(records);
            verify(messageProcessor, atLeastOnce()).flush();
            verify(kafkaConsumer, atLeastOnce()).commitSync(anyMap());
            verify(record1, atLeastOnce()).offset();
            verify(record2, atLeastOnce()).offset();

        }

        @Test
        public void shouldStoreAndCommitOffsets() {
            //given
            KafkaConsumerRunner.OffsetHolder<byte[],byte[]> holder = new KafkaConsumerRunner.OffsetHolder<>();

            //argument captor somehow does not always work here.
            doAnswer(invocationOnMock -> {
                Map<TopicPartition, OffsetAndMetadata> args = invocationOnMock.getArgument(0);
                assertEquals(OFFSET1 + 1, args.get(tp1).offset());
                assertEquals(OFFSET2 + 1, args.get(tp2).offset());
                return null;
            }).when(kafkaConsumer).commitSync((Map)argThat(r -> true));

            //when
            holder.storeOffsets(records);
            holder.commitOffsets(kafkaConsumer);

            //then
            verify(kafkaConsumer,times(1)).commitSync(anyMap());

        }

        @Test
        @Disabled("fix in NXCALS-2438")
        public void shouldCommitOffsetsOnPartitionsRevoked() throws InterruptedException {
            //given
            //we are never flushing, thus big argument to flushInterval
            KafkaConsumerRunner<byte[], byte[]> runner = createRunner(Integer.MAX_VALUE, TimeUnit.MINUTES);

            CountDownLatch messageProcessingLatch = getMessageProcessedLatch(false);

            doAnswer(invocationOnMock -> {
                Map<TopicPartition, OffsetAndMetadata> args = invocationOnMock.getArgument(0);
                assertEquals(OFFSET1 + 1, args.get(tp1).offset());
                assertEquals(OFFSET2 + 1, args.get(tp2).offset());
                return null;
            }).when(kafkaConsumer).commitSync((Map)argThat(r -> true));

            //when
            run(runner);
            messageProcessingLatch.await(10, TimeUnit.SECONDS);
            //messages processed, we can now trigger the method, argument does not matter
            runner.onPartitionsRevoked(Collections.emptyList());

            //then
            verify(kafkaConsumer, atLeastOnce()).commitSync((Map)argThat(arg -> true));
        }


        @Test
        public void shouldCloseOnWakeupException() throws InterruptedException {
            //given
            when(kafkaConsumer.poll(argThat(a->true))).thenReturn(records);
            when(records.isEmpty()).thenReturn(false);

            KafkaConsumerRunner<byte[], byte[]> runner = createRunner(Integer.MAX_VALUE, TimeUnit.MINUTES);
            CountDownLatch messageProcessingLatch = getMessageProcessedLatch(true);

            CountDownLatch closeLatch = new CountDownLatch(1);
            doAnswer(invocationOnMock -> {
                closeLatch.countDown();
                return null;
            }).when(kafkaConsumer).close();

            //when
            run(runner);
            messageProcessingLatch.await(10, TimeUnit.SECONDS);
            closeLatch.await(10, TimeUnit.SECONDS);

            //then
            verify(kafkaConsumer, times(0)).commitSync((Map)argThat(v -> true)); //should not try to commit in shutdown mode
            verify(kafkaConsumer, times(1)).close();
        }

    }

    @Test
    public void shouldCallWakeupOnClose() throws IOException {
        //given
        KafkaConsumerRunner<byte[], byte[]> runner = createRunner(Integer.MAX_VALUE, TimeUnit.MINUTES);

        //when
        runner.close();

        //then
        verify(kafkaConsumer, times(1)).wakeup();

    }

    private void run(KafkaConsumerRunner<byte[], byte[]> runner) {
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        executorService.submit(runner);
    }

    private KafkaConsumerRunner<byte[], byte[]> createRunner(int flushInterval, TimeUnit unit) {
        KafkaEtlProperties props = new KafkaEtlProperties();
        props.setTopicPattern(".*");
        props.setFlushInterval(flushInterval);
        props.setFlushIntervalUnit(unit);
        return new KafkaConsumerRunner<>(this.kafkaConsumer, this.messageProcessor, this.executor, props);
    }

    private CountDownLatch getMessageProcessedLatch(boolean shouldThrowWakeup) {
        AtomicInteger count = new AtomicInteger();
        CountDownLatch messageProcessingLatch = new CountDownLatch(1);
        doAnswer(invocationOnMock -> {
            if (count.getAndIncrement() == 2) {
                //we need 2 iterations as the offsets are set after process method
                messageProcessingLatch.countDown();
                if (shouldThrowWakeup) {
                    throw new WakeupException();
                }
            }
            return null;
        }).when(messageProcessor).process(argThat(r -> true));
        return messageProcessingLatch;
    }
}