package cern.nxcals.kafka.etl.hbase;

import cern.nxcals.kafka.etl.config.HbaseProperties;
import cern.nxcals.kafka.etl.domain.PartitionInfo;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableExistsException;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.Connection;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.function.Supplier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class HbaseTableProviderTest {

    @Mock
    private Admin admin;

    @Mock
    private Supplier<Connection> mockConnectionSupplier;

    @Mock
    private Connection connection;

    @Nested
    class HbaseTableProviderTestNested {

        @BeforeEach
        public void initMocks() throws IOException {
            when(mockConnectionSupplier.get()).thenReturn(connection);
            when(connection.getAdmin()).thenReturn(admin);
        }

        @Test
        public void shouldCreateUsingSimpleNameFunction() {
            HbaseTableProvider provider = new HbaseTableProvider(p -> "my_table_name", getHbaseProps(4, "data",
                    "columnFamily"), mockConnectionSupplier);

            PartitionInfo partitionInfo = PartitionInfo.from(1, 2, 3, 4, false);
            TableName actual = provider.tableFor(partitionInfo);
            TableName expected = TableName.valueOf("data", "my_table_name");

            assertEquals(expected.getNamespaceAsString(), actual.getNamespaceAsString());
            assertEquals(expected.getQualifierAsString(), actual.getQualifierAsString());
        }

        @Test
        public void shouldCreateUsingComplexNameFunction() {
            HbaseTableProvider provider = new HbaseTableProvider(
                    p -> p.getSchemaId() + "_val_" + p.getPartitionId(), getHbaseProps(4, "data", "columnFamily"),
                    mockConnectionSupplier);

            PartitionInfo partitionInfo = PartitionInfo.from(1, 2, 3, 4, false);
            TableName actual = provider.tableFor(partitionInfo);
            TableName expected = TableName.valueOf("data", "3_val_2");

            assertEquals(expected.getNamespaceAsString(), actual.getNamespaceAsString());
            assertEquals(expected.getQualifierAsString(), actual.getQualifierAsString());
        }

        @Test
        public void shouldCache() {
            HbaseTableProvider provider = new HbaseTableProvider(p -> "my_table_name", getHbaseProps(4, "data",
                    "columnFamily"), mockConnectionSupplier);

            PartitionInfo partitionInfo = PartitionInfo.from(1, 2, 3, 4, false);
            provider.tableFor(partitionInfo);

            assertEquals(1, provider.estimatedSize());
        }

        @Test
        public void shouldCleanCache() {
            HbaseTableProvider provider = new HbaseTableProvider(p -> "my_table_name", getHbaseProps(4, "data",
                    "columnFamily"), mockConnectionSupplier);

            PartitionInfo partitionInfo = PartitionInfo.from(1, 2, 3, 4, false);

            provider.tableFor(partitionInfo);
            provider.destroy();

            assertEquals(0, provider.estimatedSize());
        }

        @Test
        public void shouldCreateTableIfAbsent() throws IOException {
            HbaseTableProvider provider = new HbaseTableProvider(p -> "my_table_name", getHbaseProps(4, "data",
                    "columnFamily"), mockConnectionSupplier);

            PartitionInfo partitionInfo = PartitionInfo.from(1, 2, 3, 4, false);
            TableName tableName = TableName.valueOf("data", "my_table_name");
            when(admin.tableExists(tableName)).thenReturn(false);

            provider.tableFor(partitionInfo);

            verify(admin, times(1)).createTable(any());
        }

        @Test
        public void shouldReuseTableIfPresent() throws IOException {
            HbaseTableProvider provider = new HbaseTableProvider(p -> "my_table_name", getHbaseProps(4, "data",
                    "columnFamily"), mockConnectionSupplier);

            PartitionInfo partitionInfo = PartitionInfo.from(1, 2, 3, 4, false);
            TableName tableName = TableName.valueOf("data", "my_table_name");
            HTableDescriptor table = new HTableDescriptor(tableName);
            HColumnDescriptor cDesc = new HColumnDescriptor("family");
            table.addFamily(cDesc);

            when(admin.tableExists(tableName)).thenReturn(true);
            when(admin.getTableDescriptor(tableName)).thenReturn(table);
            provider.tableFor(partitionInfo);

            verify(admin, never()).createTable(any());
        }

        @Test
        public void shouldFailOnTableCreationError() throws IOException {
            HbaseTableProvider provider = new HbaseTableProvider(p -> "my_table_name", getHbaseProps(4, "data",
                    "columnFamily"), mockConnectionSupplier);

            PartitionInfo partitionInfo = PartitionInfo.from(1, 2, 3, 4, false);

            when(admin.tableExists(any(TableName.class))).thenReturn(false);
            doThrow(new IOException()).when(admin).createTable(any());

            assertThrows(UncheckedIOException.class, () -> provider.tableFor(partitionInfo));
        }

        @Test
        public void shouldNotFailOnTableExists() throws IOException {
            String myTableName = "my_table_name";
            String namespace = "data";
            HbaseTableProvider provider = new HbaseTableProvider(p -> myTableName, getHbaseProps(4, namespace,
                    "columnFamily"), mockConnectionSupplier);

            PartitionInfo partitionInfo = PartitionInfo.from(1, 2, 3, 4, false);

            when(admin.tableExists(any(TableName.class))).thenReturn(false);
            doThrow(new TableExistsException()).when(admin).createTable(any());

            TableName tableName = provider.tableFor(partitionInfo);
            assertEquals(TableName.valueOf(namespace, myTableName), tableName);
        }
    }

    @Test
    public void shouldFailOnNullConnection() {
        assertThrows(NullPointerException.class, () ->
                new HbaseTableProvider(p -> "", getHbaseProps(4, "namespace", "columnFamily"), null));
    }

    @Test
    public void shouldFailOnNullNameGenerator() {
        assertThrows(NullPointerException.class, () ->
                new HbaseTableProvider(null, getHbaseProps(4, "namespace", "columnFamily"), mockConnectionSupplier));
    }

    private HbaseProperties getHbaseProps(int ttlHours, String namespace, String columnFamily) {
        HbaseProperties props = new HbaseProperties();
        props.setTtlHours(ttlHours);
        props.setNamespace(namespace);
        props.setColumnFamily(columnFamily);
        props.setSaltBuckets(10);
        return props;
    }

}