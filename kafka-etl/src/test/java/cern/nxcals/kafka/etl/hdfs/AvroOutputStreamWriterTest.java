package cern.nxcals.kafka.etl.hdfs;

import org.apache.avro.file.DataFileWriter;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class AvroOutputStreamWriterTest {

    @Test
    public void shouldWrite() throws IOException {
        DataFileWriter<Object> dataFileWriter = mock(DataFileWriter.class);

        AvroOutputStreamWriter writer = new AvroOutputStreamWriter("A", dataFileWriter);

        writer.write(new byte[0]);

        verify(dataFileWriter, times(1)).appendEncoded(any());
    }

    @Test
    public void shouldFailOnFailedClose() throws IOException {
        DataFileWriter<Object> dataFileWriter = mock(DataFileWriter.class);
        doThrow(new IOException()).when(dataFileWriter).close();
        AvroOutputStreamWriter writer = new AvroOutputStreamWriter("A", dataFileWriter);

        assertThrows(IOException.class, () -> writer.close());
    }

    @Test
    public void shouldFlush() throws IOException {
        DataFileWriter<Object> dataFileWriter = mock(DataFileWriter.class);

        AvroOutputStreamWriter writer = new AvroOutputStreamWriter("A", dataFileWriter);

        writer.flush();

        verify(dataFileWriter, times(1)).flush();
    }

    @Test
    public void shouldClose() throws IOException {
        DataFileWriter<Object> dataFileWriter = mock(DataFileWriter.class);

        AvroOutputStreamWriter writer = new AvroOutputStreamWriter("A", dataFileWriter);

        writer.close();

        verify(dataFileWriter, times(1)).close();
    }
}
