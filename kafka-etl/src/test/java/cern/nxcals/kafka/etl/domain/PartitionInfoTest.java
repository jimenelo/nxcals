package cern.nxcals.kafka.etl.domain;

import cern.nxcals.api.utils.TimeUtils;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import static java.util.stream.Collectors.groupingBy;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class PartitionInfoTest {

    @Test
    public void shouldBeEqualIgnoringTimestamp() {
        PartitionInfo one = PartitionInfo.from(1, 1, 1, 100, false);
        PartitionInfo two = PartitionInfo.from(1, 1, 1, 200, false);

        assertEquals(one, two);
    }

    @Test
    public void shouldTruncateTimestamp() {
        OffsetDateTime in = OffsetDateTime
                .ofInstant(Instant.ofEpochMilli(new Random().nextInt(1550585106)), ZoneOffset.UTC);

        PartitionInfo partition = PartitionInfo.from(1, 1, 1, TimeUnit.MILLISECONDS.toNanos(in.toInstant().toEpochMilli()), false);

        OffsetDateTime out = Instant.ofEpochMilli(partition.getTimestampMillis()).atOffset(ZoneOffset.UTC);

        assertEquals(in.getYear(), out.getYear());
        assertEquals(in.getMonth(), out.getMonth());
        assertEquals(in.getDayOfMonth(), out.getDayOfMonth());
        assertEquals(in.getHour(), out.getHour());
        assertEquals(0, out.getMinute());
        assertEquals(0, out.getSecond());
    }

    @Test
    public void shouldBucketByHour() {
        List<PartitionInfo> partitions = new ArrayList<>();
        int hourCount = 10;
        int minuteCount = 36;

        for (int hour = 0; hour < hourCount; hour++) {
            for (int minute = 0; minute < minuteCount; minute++) {
                OffsetDateTime dateTime = OffsetDateTime.of(2000, 1, 1, hour, minute, 0, 0, ZoneOffset.UTC);
                PartitionInfo partition = PartitionInfo.from(1, 1, 1,
                        TimeUnit.MILLISECONDS.toNanos(dateTime.toInstant().toEpochMilli()),
                        TimeUtils.TimeSplits.ONE_HOUR_SPLIT, false);
                partitions.add(partition);
            }
        }

        Map<PartitionInfo, List<PartitionInfo>> collect = partitions.stream().collect(groupingBy(Function.identity()));

        assertEquals(hourCount, collect.keySet().size());

        collect.values().stream().mapToInt(List::size).forEach(t -> assertEquals(t, minuteCount));
    }

}