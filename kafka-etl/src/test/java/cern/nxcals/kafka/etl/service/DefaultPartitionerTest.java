package cern.nxcals.kafka.etl.service;

import cern.nxcals.api.utils.TimeUtils;
import cern.nxcals.internal.extraction.metadata.InternalPartitionService;
import cern.nxcals.kafka.etl.domain.PartitionInfo;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class DefaultPartitionerTest {
    @Mock
    ConsumerRecord<byte[],byte[]> record;

    @Mock
    private InternalPartitionService partitionService;

    @Test
    public void shouldReturnInvalidPartitionForBrokenKeys() {
        DefaultPartitioner partitioner = new DefaultPartitioner(TimeUtils.TimeSplits.FIVE_MINUTE_SPLIT,
                TimeUtils.getNanosFromString("2023-01-01 00:00:00.000"),
                partitionService);

        when(record.key()).thenReturn(new byte[] {1,2,3});
        PartitionInfo partition = partitioner.findPartition(record);

        assertEquals(PartitionInfo.INVALID_PARTITION,partition);
    }
}