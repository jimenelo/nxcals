import getopt
import os
import re
import sys
import textwrap


def main(argv):

    def save_snippet(lines, tag, filename):
        mkdocs_indent = '    '
        text = ""

        os.makedirs(os.path.dirname(filename), exist_ok=True)
        out_file = open(filename, 'w')

        for num, line in enumerate(lines):
            match_end = re.search('(//|#)(.*)@snippet:' + tag + ':off', line, re.IGNORECASE)
            if match_end:
                out_file.write(textwrap.indent(textwrap.dedent(text), mkdocs_indent).replace(mkdocs_indent,'',1))
                out_file.close()
                return

            # skip other snippets bookmarks
            match_skip_line = re.search('(//|#)(.*)@snippet:(.*):(on|off)', line, re.IGNORECASE)
            if not match_skip_line:
                text = text + line

    def process_code_from_file(lines, inp_path, out_path):

        for num, line in enumerate(lines):

            match_start = re.search('(//|#)(.*)@snippet:(.*):on', line, re.IGNORECASE)
            if match_start:
                tag = match_start.group(3)

                filename = path.replace(inp_path, out_path + inp_path) + "/" + file + "." + tag

                save_snippet(lines[num+1:], tag, filename)

    inp_path = 'src/'
    out_path = 'pages/generated/'

    try:
        opts, args = getopt.getopt(argv, "hi:", ["inp-path"])
    except getopt.GetoptError:
        print('scan_for_snippets.py -i <input_path>')
        sys.exit(2)

    for opt, arg in opts:
        if opt == '-h':
            print('scan_for_snippets.py -i <input_path>')
            sys.exit()
        elif opt in ("-i", "--inp-path"):
            inp_path = arg

    for path, dirs, files in os.walk(inp_path):
        if "__pycache__" in path:
            continue
        for file in files:

            with open(path + "/" + file, "r") as inp_file:
                lines = inp_file.readlines()
                process_code_from_file(lines, inp_path, out_path)


if __name__ == "__main__":
    main(sys.argv[1:])
