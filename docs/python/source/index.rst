NXCALS Python APIs
==================

Introduction
------------

NXCALS Data Extraction API (part of nxcals package) contains DataQuery builders - extraction methods
written directly on top of the Java API as a thin set of native python units that are internally using Py4J


Installation
------------

Please refer to our NXCALS package installation documentation.


.. toctree::
    :maxdepth: 1
    :hidden:

    self

.. toctree::
    :caption: Reference docs
    :maxdepth: 1

    api
    genindex
