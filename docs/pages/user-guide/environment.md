Prior to running any code related to desired actions (such as data extraction, data ingestion or thin API extraction) one must establish NXCALS environment by setting up certain properties as follows:


|Action/Property|service.url|kafka.producer.bootstrap.servers|spark.servers.url|
|-|:-:|:-:|:-:|
|**Data extraction**|X||
|**Data ingestion**|X|X||
|**Thin API extraction**|||X|


Examples of code snippets for setting these values can be found in our "Quick guide" for Java examples project
under [this link](../examples-project/examples-project/#setting-environment-required).

Depending on the environment **PRO** or **TESTBED** the following values must be used:


=== "PRO environment"

    |Property|Value|
    |-|-|
    |service.url|{{public_services_pro}}|
    |kafka.producer.bootstrap.servers|{{kafkas_pro}}|
    |spark.servers.url|{{spark_servers_pro}}|


=== "TESTBED environment"

    |Property|Value|
    |-|-|
    |service.url|{{public_services_testbed}}|
    |kafka.producer.bootstrap.servers|{{kafkas_testbed}}|
    |spark.servers.url|{{spark_servers_testbed}}|

Please note that in case of Python (using *pip install nxcals* or *Spark bundle*) the service.url setting will come from the
bundle itself (it is set in spark-defaults.conf).
In order to connect to other environments (like TESTBED) you have to set it in the method *spark_session_builder.get_or_create(service_url=...)*.

## Required ports to open in firewall on the local machine
In certain cases NXCALS requires that **firewall is open for some number of ports**. That applies to 
running Spark in YARN client mode.
By default NXCALS is using the following ports:

```properties
spark.driver.port                       5001
spark.blockManager.port                 5101
spark.ui.port                           5201
```

!!! important
    In certain situations proposed values may conflict with ports which are arleady in use.
    In that case they can be changed to the new ones, selected from the following ranges:
    
    ```text
    5001-5100  ->  Spark driver
    5101-5200  ->  Block manager
    5201-5300  ->  Driver UI
    ```

!!! hint
    Issuing the telnet command:
    ```bash 
     telnet [domainname or ip] [port] 
    ```
    
    allows to test connectivity to a remote host on the given port, for example:
    
    ```bash
    telnet yourmachine.cern.ch 5001
    ```
    A failed connection, accompanied by an error message, most likely indicates a firewall issue.