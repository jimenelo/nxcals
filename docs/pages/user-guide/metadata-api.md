# Metadata API 

NXCALS meta-data can be retrieved using Java Client API from `cern.nxcals.api.metadata` package. 

**NXCALS Service Client API** itself consist of four end points:

- [Metadata API](#metadata-api)
    - [System Service](#system-service)
        - [Examples](#examples)
            - [Find information about system using its name](#find-information-about-system-using-its-name)
    - [Entity Service](#entity-service)
        - [`withOptions`](#withoptions)
        - [Examples](#examples-1)
            - [Search for one entity.](#search-for-one-entity)
            - [Search for an entity with its history which is present in the provided time range.](#search-for-an-entity-with-its-history-which-is-present-in-the-provided-time-range)
            - [Update entity (change its key values)](#update-entity-change-its-key-values)
            - [Search for multiple entities `withOptions`: `limit`, `orderBy` and `withHistory`](#search-for-multiple-entities-withoptions-limit-orderby-and-withhistory)
    - [Variable Service](#variable-service)
        - [`withOptions`](#withoptions-1)
        - [Examples](#examples-2)
            - [Find variables using pattern on name](#find-variables-using-pattern-on-name)
            - [Find variables using pattern on description](#find-variables-using-pattern-on-description)
            - [Get list of variables sorted by name without configs](#get-list-of-variables-sorted-by-name-without-configs)
    - [Hierarchy Service](#hierarchy-service)
        - [Examples](#examples-3)
            - [Find hierarchies using pattern on path](#find-hierarchies-using-pattern-on-path)
            - [Attach a "new node" to "/example" hierarchy. Once created, modify its description](#attach-a-new-node-to-example-hierarchy-once-created-modify-its-description)
            - [Delete new node](#delete-new-node)
            - [Attach a variable to a hierarchy node. Demonstrates usage of `findOne`, `addVariables` and `getVariables` methods](#attach-a-variable-to-a-hierarchy-node-demonstrates-usage-of-findone-addvariables-and-getvariables-methods)
            - [Replace entities attached to a hierarchy node with a single entity. Demonstrates usage of `findAll`, `setEntities` and `getEntities` methods](#replace-entities-attached-to-a-hierarchy-node-with-a-single-entity-demonstrates-usage-of-findall-setentities-and-getentities-methods)
    - [Tag Service](#tag-service) (Experimental)
        - [Examples](#examples-4)
            - [Search for one tag](#search-for-one-tag)
            - [Create tag](#create-tag)
            - [Update tag](#update-tag) (change its key values)
    - [Snapshot Service](#snapshot-service) (Experimental)
        - [Examples](#examples-5)
            - [Search for one snapshot](#search-for-one-snapshot)
            - [Extract data using snapshot](#extract-data-using-snapshot)
    - [Cache Configurations](#cache-configurations)
    - [Additional options (for `Entities` and `Variables`) - `limit`, `offset`, and `orderBy`](#additional-options-for-entities-and-variables---limit-offset-and-orderby)


Full Java documentation is available [here.](../../java-docs/index.html)

In contrary to versions prior to the 0.2.0 the API of each endpoint contains similar search methods that accept arbitrary `Condition`
built with a query builder allowing to search for one or many objects in a very flexible way. The method finding one object
(`findOne` and `findById`) are cached. 

```java
Set<X> findAll(Condition<Xs>)
Optional<X> findOne(Condition<Xs>)
Optional<X> findById(long id)
```

Please note that you will find corresponding `Condition` builders with classes that names are made plural from the domain 
object. So `Entity` -> `Entities`, `Variable` -> `Variables`, etc.

In order to build a `Condition` please use the following code: 
```java
{% include 'generated/src/main/java/cern/nxcals/docs/metadataapi/EntityServiceSnippets.java.3' %}
```

Note that conditions are by default in case-sensitive mode. Meaning:

```java
{% include 'generated/src/main/java/cern/nxcals/docs/metadataapi/EntityServiceSnippets.java.4' %}
```

Will only match group named `"myName"`.

If you wish to enable case-insensitive search please use `caseInsensitive()` call prior to any string operation:

```java
{% include 'generated/src/main/java/cern/nxcals/docs/metadataapi/EntityServiceSnippets.java.5' %}
```

This will match every possible combination of casing of `"myName"`, eg. `"MYNAME"`, `"MyName"`, `"MyNaMe"` etc.

Case insensitive search is also available for key-value searches:
```java
{% include 'generated/src/main/java/cern/nxcals/docs/metadataapi/EntityServiceSnippets.java.6' %}
```

Conditions must return a correct number of records according to the method used. 
If a `Condition` that returns multiple rows is passed to the `findOne` method an exception will be thrown (`IllegalArgumentException`). 

!!!important 
    Using case insensitive search might cause a query to return more than one value in unexpected circumstances. 
    Be sure to call `queryAll` method for safety, whenever opting for case-insensitive search.
    
!!!warning
    Running Python code for this API requires a specific setup as described [here](./data-access/py4j-jpype-installaton.md). The code snippets provided on this page are using JPype.
## System Service
Service responsible for retrieving data related to systems.

Additionally available methods (on top of `findAll`, `findOne` and `findById()`):

```java
Optional<System> findByName(String name)
Set<System> findAll()
```

More info can be found in [Javadoc for SystemSpecService interface](../../java-docs/cern/nxcals/api/extraction/metadata/SystemSpecService.html).

### Examples

#### Find information about system using its name


=== "Java"
    ```java
    {% include 'generated/src/main/java/cern/nxcals/docs/metadataapi/SystemServiceSnippets.java.1' %}
    ```
=== "Python (Py4J/SWAN)"
    ```Python
    {% include 'generated/src/main/python/cern/nxcals/docs/metadataapipy4j/SystemServiceSnippets.py.1' %}
    ```
=== "Python (JPype)"
    ```Python
    {% include 'generated/src/main/python/cern/nxcals/docs/metadataapijpype/SystemServiceSnippets.py.1' %}
    ```

## Entity Service
Service responsible for retrieving data related to Entities.

!!!note
    For update of entities a role with `WRITE` permission for a given system is required.
    Please follow procedure described [here](../data-access/nxcals-access-request/) to obtain required authorization. 

In addition to `findAll`, `findOne` and `findById()`, the EntityService exposes two additional methods for obtaining
the Entity history(ies) for a given time window: 

```java
Set<Entity> findAllWithHistory(Condition<Entities> condition, long historyStartTime, long historyEndTime)
Optional<Entity> findOneWithHistory(Condition<Entities> condition, long historyStartTime, long historyEndTime)
``` 

There are some other methods for data persistence / manipulation: 

```java
Entity createEntity(long systemId, Map<String, Object> entityKey, Map<String, Object> partitionKey)
Set<Entity> updateEntities(Set<Entity> entityDataList)
```

More info can be found in [Javadoc for EntityService interface](../../java-docs/cern/nxcals/api/extraction/metadata/EntityService.html).

### `withOptions`

`withOptions` allows to specify additional options to query. Options common with `Variables` are:

- `limit(int size)` - allows to limit number of returned `Entity` objects;
- `orderBy` - allows to sort objects by specified field in `Entity`. Order might be ascending (`asc`) or descending (`desc`);
- `offset(int offset)` - enables skipping first N result, what can be used in combination with above.


`Entities` contains following specific methods:

- `noHistory` - allows to query just for plain `Entity` objects, without `EntityHistory`;
- `withHistory(long/String/Instant start, long/String/Instant end)` - allows to query for `Entity` objects with history entries, which were valid in specified time range.


By default, query will return `Entity` objects with the latest `EntityHistory`.


### Examples
 
#### Search for one entity.
Please note that extracted entity history contains only a current history:

=== "Java"
    ```java
    {% include 'generated/src/main/java/cern/nxcals/docs/metadataapi/EntityServiceSnippets.java.1' %}
    ```
=== "Python (Py4J/SWAN)"
    ```Python
    {% include 'generated/src/main/python/cern/nxcals/docs/metadataapipy4j/EntityServiceSnippets.py.1' %}
    ```
=== "Python (JPype)"
    ```Python
    {% include 'generated/src/main/python/cern/nxcals/docs/metadataapijpype/EntityServiceSnippets.py.1' %}
    ``` 

#### Search for an entity with its history which is present in the provided time range.
For the example below, two entity histories will be shown:
first with the validity from `1970-01-01T00:00:00Z` until `2019-01-01T00:00:00Z`,
and second with the validity from `2019-01-01T00:00:00Z` onwards:

=== "Java"
    ```java
    {% include 'generated/src/main/java/cern/nxcals/docs/metadataapi/EntityServiceSnippets.java.7' %}
    ```
=== "Python (Py4J/SWAN)"
    ```Python
    {% include 'generated/src/main/python/cern/nxcals/docs/metadataapipy4j/EntityServiceSnippets.py.2' %}
    ```
=== "Python (JPype)"
    ```Python
    {% include 'generated/src/main/python/cern/nxcals/docs/metadataapijpype/EntityServiceSnippets.py.2' %}
    ```

#### Update entity (change its key values)

=== "Java"
    ```java
    {% include 'generated/src/main/java/cern/nxcals/docs/metadataapi/EntityServiceSnippets.java.2' %}
    ```
=== "Python (Py4J/SWAN)"
    ```Python
    {% include 'generated/src/main/python/cern/nxcals/docs/metadataapipy4j/EntityServiceSnippets.py.3' %}
    ```
=== "Python (JPype)"
    ```Python
    {% include 'generated/src/main/python/cern/nxcals/docs/metadataapijpype/EntityServiceSnippets.py.3' %}
    ``` 

#### Search for multiple entities `withOptions`: `limit`, `orderBy` and `withHistory`

=== "Java"
    ```java
    {% include 'generated/src/main/java/cern/nxcals/docs/metadataapi/EntityServiceSnippets2.java.4' %}
    ```
=== "Python (Py4J/SWAN)"
    ```Python
    {% include 'generated/src/main/python/cern/nxcals/docs/metadataapipy4j/EntityServiceSnippets.py.11' %}
    ```
=== "Python (JPype)"
    ```Python
    {% include 'generated/src/main/python/cern/nxcals/docs/metadataapijpype/EntityServiceSnippets.py.11' %}
    ```


## Variable Service
Service responsible for retrieving data related to variables.

!!!note
    For registration or update of variables a role with `VARIABLE:WRITE` permission is required.
    Please follow procedure described [here](../data-access/nxcals-access-request/) to obtain required authorization. 

Additionally available methods (on top of `findAll`, `findOne` and `findById()`):

```java
Variable create(Variable variableData)
Variable update(Variable variableData)
List<Variable> findAll(VariableQueryWithOptions query)
Optional<Variable> findOne(VariableQueryWithOptions query)
```

More info can be found in [Javadoc for VariableService interface](../../java-docs/cern/nxcals/api/extraction/metadata/VariableService.html).  

### `withOptions`

`withOptions` allows to specify additional options to query. Options common with `Entities` are:

- `limit(int size)` - allows to limit number of returned `Variable` objects;
- `orderBy()` - allows to sort objects by specified field in `Variable`. Order might be ascending (`asc`) or descending (`desc`);
- `offset(int offset)` - enables skipping first N result, what can be used in combination with above.


Currently `Variables` contains one specific method - `noConfigs` which allows to query just for plain `Variable` objects, without `VariableConfig`. By default, query will return `Variable` objects with `VariableConfig`.

### Examples

#### Find variables using pattern on name

=== "Java"
    ```java
    {% include 'generated/src/main/java/cern/nxcals/docs/metadataapi/VariableServiceSnippets.java.1' %}
    ```
=== "Python (Py4J/SWAN)"
    ```Python
    {% include 'generated/src/main/python/cern/nxcals/docs/metadataapipy4j/VariableServiceSnippets.py.1' %}
    ```
=== "Python (JPype)"
    ```Python
    {% include 'generated/src/main/python/cern/nxcals/docs/metadataapijpype/VariableServiceSnippets.py.1' %}
    ```
    
#### Find variables using pattern on description

=== "Java"
    ```java
    {% include 'generated/src/main/java/cern/nxcals/docs/metadataapi/VariableServiceSnippets.java.2' %}
    ```
=== "Python (Py4J/SWAN)"
    ```Python
    {% include 'generated/src/main/python/cern/nxcals/docs/metadataapipy4j/VariableServiceSnippets.py.2' %}
    ```
=== "Python (JPype)"
    ```Python
    {% include 'generated/src/main/python/cern/nxcals/docs/metadataapijpype/VariableServiceSnippets.py.2' %}
    ```

Create a new variable pointing to a given entity and field for an open time window. In case of existing variable
update its description.
Demonstrates usage of `create` and `update` methods:

=== "Java"
    ```java
    {% include 'generated/src/main/java/cern/nxcals/docs/metadataapi/VariableServiceSnippets.java.3' %}
    ```
=== "Python (Py4J/SWAN)"
    ```Python
    {% include 'generated/src/main/python/cern/nxcals/docs/metadataapipy4j/VariableServiceSnippets.py.3' %}
    ```
=== "Python (JPype)"
    ```Python
    {% include 'generated/src/main/python/cern/nxcals/docs/metadataapijpype/VariableServiceSnippets.py.3' %}
    ```

#### Get list of variables sorted by name without configs

=== "Java"
    ```java
    {% include 'generated/src/main/java/cern/nxcals/docs/metadataapi/VariableServiceSnippets2.java.1' %}
    ```
=== "Python (Py4J/SWAN)"
    ```Python
    {% include 'generated/src/main/python/cern/nxcals/docs/metadataapipy4j/VariableServiceSnippets.py.4' %}
    ```
=== "Python (JPype)"
    ```Python
    {% include 'generated/src/main/python/cern/nxcals/docs/metadataapijpype/VariableServiceSnippets.py.4' %}
    ```

## Hierarchy Service
Service responsible for retrieving data related to hierarchies.

!!!note
    For registration or update of hierarchies a role with `HIERARCHY:WRITE` permission is required.
    Please follow procedure described [here](../data-access/nxcals-access-request/) to obtain required authorization. 


Additional available methods (on top of `findAll`, `findOne` and `findAll()` ):

```java
Hierarchy create(Hierarchy hierarchy)
Hierarchy update(Hierarchy hierarchy)
void deleteLeaf(long hierarchyId)

Set<Hierarchy> getTopLevel(SystemSpec system)
List<Hierarchy> getChainTo(Hierarchy hierarchy)

void setVariables(long hierarchyId, Set<Long> variableIds)
void addVariables(long hierarchyId, Set<Long> variableIds)
void removeVariables(long hierarchyId, Set<Long> variableIds)
Set<Variable> getVariables(long hierarchyId)

void setEntities(long hierarchyId, Set<Long> entityIds)
void addEntities(long hierarchyId, Set<Long> entityIds)
void removeEntities(long hierarchyId, Set<Long> entityIds)
Set<Entity> getEntities(long hierarchyId)
```

More info can be found in [Javadoc for HierarchyService interface](../../java-docs/cern/nxcals/api/extraction/metadata/HierarchyService.html).  

### Examples

#### Find hierarchies using pattern on path

=== "Java"
    ```java
    {% include 'generated/src/main/java/cern/nxcals/docs/metadataapi/HierarchyServiceSnippets.java.1' %}
    ```
=== "Python (Py4J/SWAN)"
    ```python
    {% include 'generated/src/main/python/cern/nxcals/docs/metadataapipy4j/HierarchyServiceSnippets.py.1' %}
    ```
=== "Python (JPype)"
    ```python
    {% include 'generated/src/main/python/cern/nxcals/docs/metadataapijpype/HierarchyServiceSnippets.py.1' %}
    ```

#### Attach a "new node" to "/example" hierarchy. Once created, modify its description

=== "Java"
    ```java
    {% include 'generated/src/main/java/cern/nxcals/docs/metadataapi/HierarchyServiceSnippets.java.4' %}
    ```
=== "Python (Py4J/SWAN)"
    ```python
    {% include 'generated/src/main/python/cern/nxcals/docs/metadataapipy4j/HierarchyServiceSnippets.py.2' %}
    ```
=== "Python (JPype)"
    ```python
    {% include 'generated/src/main/python/cern/nxcals/docs/metadataapijpype/HierarchyServiceSnippets.py.2' %}
    ```

#### Delete new node

=== "Java"
    ```java
    {% include 'generated/src/main/java/cern/nxcals/docs/metadataapi/HierarchyServiceSnippets.java.5' %}
    ```
=== "Python (Py4J/SWAN)"
    ```python
    {% include 'generated/src/main/python/cern/nxcals/docs/metadataapipy4j/HierarchyServiceSnippets.py.3' %}
    ```
=== "Python (JPype)"
    ```python
    {% include 'generated/src/main/python/cern/nxcals/docs/metadataapijpype/HierarchyServiceSnippets.py.3' %}
    ```

#### Attach a variable to a hierarchy node. Demonstrates usage of `findOne`, `addVariables` and `getVariables` methods

=== "Java"
    ```java
    {% include 'generated/src/main/java/cern/nxcals/docs/metadataapi/HierarchyServiceSnippets.java.2' %}
    ```
=== "Python (Py4J/SWAN)"
    ```python
    {% include 'generated/src/main/python/cern/nxcals/docs/metadataapipy4j/HierarchyServiceSnippets.py.4' %}
    ```
=== "Python (JPype)"
    ```python
    {% include 'generated/src/main/python/cern/nxcals/docs/metadataapijpype/HierarchyServiceSnippets.py.4' %}
    ```

#### Replace entities attached to a hierarchy node with a single entity. Demonstrates usage of `findAll`, `setEntities` and `getEntities` methods

=== "Java"
    ```java
    {% include 'generated/src/main/java/cern/nxcals/docs/metadataapi/HierarchyServiceSnippets.java.3' %}
    ```
=== "Python (Py4J/SWAN)"
    ```python
    {% include 'generated/src/main/python/cern/nxcals/docs/metadataapipy4j/HierarchyServiceSnippets.py.5' %}
    ```
=== "Python (JPype)"
    ```python
    {% include 'generated/src/main/python/cern/nxcals/docs/metadataapijpype/HierarchyServiceSnippets.py.5' %}
    ```

Please not that the methods `setVariables` and `setEntities` work as normal setters - they replace the current state. Be sure not to overwrite anything you don't intend to.

## Tag Service
Service responsible for retrieving data using Tags.

!!!note
    For update of tags a role with `WRITE` permission for a given system is required.
    Please follow procedure described [here](../data-access/nxcals-access-request/) to obtain required authorization.

In addition to `findAll`, `findOne` and `findById()`, the `TagService` exposes two additional methods for obtaining
attached entities and variables:

```java
Map<String, Set<Entity>> getEntities(long id)
Map<String, Set<Variable>> getVariables(long id)
``` 
returning the complete map of variables/entities grouped by their association to a given group.

There are some other methods for data persistence / manipulation:

```java
Tag create(String name, String description, Visibility visibility, String owner, String system, Instant timestamp, @NonNull Collection<Entity> entities, @NonNull Collection<Variable> variables)
Tag update(Tag tag)
void setVariables(long id, Map<String, Set<Long>> variableIdsPerAssociation)
void addVariables(long id, Map<String, Set<Long>> variableIdsPerAssociation)
void removeVariables(long id, Map<String, Set<Long>> variableIdsPerAssociation)
void setEntities(long id, Map<String, Set<Long>> entityIdsPerAssociation)
void addEntities(long id, Map<String, Set<Long>> entityIdsPerAssociation)
void removeEntities(long id, Map<String, Set<Long>> entityIdsPerAssociation)
```

More info can be found in [Javadoc for TagService interface](../../java-docs/cern/nxcals/api/custom/extraction/metadata/TagService.html).

### Examples

#### Search for one tag

=== "Java"
    ```java
    import static cern.nxcals.api.custom.extraction.metadata.TagService.DEFAULT_RELATION;
    {% include 'generated/src/main/java/cern/nxcals/docs/metadataapi/TagServiceSnippets.java.1' %}
    ``` 
=== "Python (Py4J/SWAN)"
    ```Python
    {% include 'generated/src/main/python/cern/nxcals/docs/metadataapipy4j/TagServiceSnippets.py.1' %}
    ```

#### Create tag

=== "Java"
    ```java
    {% include 'generated/src/main/java/cern/nxcals/docs/metadataapi/TagServiceSnippets.java.2' %}
    ```
=== "Python (Py4J/SWAN)"
    ```Python
    {% include 'generated/src/main/python/cern/nxcals/docs/metadataapipy4j/TagServiceSnippets.py.2' %}
    ```

#### Update tag

=== "Java"
    ```java
    {% include 'generated/src/main/java/cern/nxcals/docs/metadataapi/TagServiceSnippets.java.3' %}
    ```
=== "Python (Py4J/SWAN)"
    ```Python
    {% include 'generated/src/main/python/cern/nxcals/docs/metadataapipy4j/TagServiceSnippets.py.3' %}
    ```

## Snapshot Service
Service responsible for retrieving data using Snapshots.

!!!note
    For update of tags a role with `WRITE` permission for a given system is required.
    Please follow procedure described [here](../data-access/nxcals-access-request/) to obtain required authorization.

In addition to `findAll`, `findOne` and `findById()`, the SnapshotService exposes two additional methods for obtaining
attached entities and variables:

```java
Map<String, Set<Entity>> getEntities(long id)
Map<String, Set<Variable>> getVariables(long id)
``` 
returning the complete map of variables/entities grouped by their association to a given group

There are some other methods for data persistence / manipulation:

```java
Snapshot create(Snapshot snapshot, Collection<Entity> entities, Collection<Variable> variables, Collection<Variable> xAxisVariables, Collection<Variable> fundamentalFiltersVariables, Collection<Variable> drivingVariables)
Snapshot update(Snapshot tag)
void setVariables(long id, Map<String, Set<Long>> variableIdsPerAssociation)
void addVariables(long id, Map<String, Set<Long>> variableIdsPerAssociation)
void removeVariables(long id, Map<String, Set<Long>> variableIdsPerAssociation)
void setEntities(long id, Map<String, Set<Long>> entityIdsPerAssociation)
void addEntities(long id, Map<String, Set<Long>> entityIdsPerAssociation)
void removeEntities(long id, Map<String, Set<Long>> entityIdsPerAssociation)
```

More info can be found in [Javadoc for TagService interface](../../java-docs/cern/nxcals/api/custom/extraction/metadata/TagService.html).

### Examples

#### Search for one snapshot

=== "Java"
    ```java
    {% include 'generated/src/main/java/cern/nxcals/docs/metadataapi/SnapshotServiceSnippets.java.1' %}
    ```
=== "Python (Py4J/SWAN)"
    ```Python
    {% include 'generated/src/main/python/cern/nxcals/docs/metadataapipy4j/SnapshotServiceSnippets.py.1' %}
    ```

#### Extract data using snapshot

=== "Java"
    ```java
    {% include 'generated/src/main/java/cern/nxcals/docs/metadataapi/SnapshotServiceSnippets.java.2' %}
    ```
=== "Python (Py4J/SWAN)"
    ```Python
    {% include 'generated/src/main/python/cern/nxcals/docs/metadataapipy4j/SnapshotServiceSnippets.py.2' %}
    ```

## Cache Configurations
NXCALS is using [Caffeine](https://www.javadoc.io/doc/com.github.ben-manes.caffeine/caffeine/{% include 'generated/caffeine_version.txt'  %}/com/github/benmanes/caffeine/cache/CaffeineSpec.html) as its library to handle in-memory caching. 
Below, we can find a list of all the user-configurable properties of the cache with their default value.
```conf
cache.spec.initial.capacity = 500
cache.spec.maximum.size = Integer.MAX_VALUE 
cache.spec.expire.after.access.seconds = Integer.MAX_VALUE
cache.spec.expire.after.write.seconds = Integer.MAX_VALUE
```
Internally, we are using [Typesafe](https://github.com/lightbend/config) and the following properties are set in a typesafe config way. 
In case you need it, you can configure any properties in two ways.
Either you overwrite those properties in your application.conf file, for example by adding:
```conf
cache.spec {
  initial.capacity = 250,
  maximum.size = 1000,
  expire.after.access.seconds = 600,
  expire.after.write.seconds = 600
}
```
or by specifying those properties as JVM properties, for example:
```bash
-Dcache.spec.initial.capacity=250 -Dcache.spec.maximum.size=1000
```
For the temporal settings, by default we are considering seconds, so the unit of measure is not needed after the specified value.

!!! important
    There are some constraints that need to be taken into account for the cache not to slow down too much NXCALS.
    Minimum cache.spec.initial.capacity = 100  
    Minimum cache.spec.maximum.size = 500    
    Minimum cache.spec.expire.after.access.seconds = 600  
    Minimum cache.spec.expire.after.write.seconds = 600  

Broking one of these constraints will make NXCALS throw an exception. If you are not sure what to set for a certain property, you can omit it to use the default one.  

## Additional options (for `Entities` and `Variables`) - `limit`, `offset`, and `orderBy`

There is possibility to limit number of returned objects. You can do this using at first `withOptions()`, and then `limit()`. Example:
```java
{% include 'generated/src/main/java/cern/nxcals/docs/metadataapi/EntityServiceSnippets2.java.1' %}
```

Apart from `limit`, there is also way to specify `offset()`. It causes, that first N records are skipped. With combination with limit, it can be used to provide pagination.
```java
{% include 'generated/src/main/java/cern/nxcals/docs/metadataapi/EntityServiceSnippets2.java.2' %}
```

Another option is sorting records by chosen field:
```java
{% include 'generated/src/main/java/cern/nxcals/docs/metadataapi/EntityServiceSnippets2.java.3' %}
```
