Here is a quick guide to get started with NXCALS.

## Registration

Registration process is described in [Getting access](./nxcals-access-request.md).

## Timber (extraction web interface)

You can explore data from NXCALS using [timber](https://timber.cern.ch).

## Java API

Consult our Java [NXCALS examples project](../../examples-project/examples-project/). Supported versions you can check on [dedicated page](../supported-software-versions.md).

!!! important
    Running NXCALS bundle and executing NXCALS examples require additional [configuration steps](../../data-access/authentication/).

## Python API

To set up you python environment, please check [Setting up Python environments](../python-environments.md) page.
Currently, NXCALS provides a Python API only for [**Data Extraction API**](../../extraction-api/). There is a possibility of accessing from Python other NXCALS Java APIs such as: 

- [CERN Extraction API](../../cern-extraction-api/)
- [Metadata API](../../extraction-api/)
- [Ingestion API](../../ingestion-api/)
- [Backport API](../../backport-api/)

by [**using Py4J or JPype**](py4j-jpype-installaton.md).

### SWAN

Using SWAN is the easiest and recommended way for simple scripting. Everything is configured and up to date. For more details check [Using SWAN](../swan/swan-session.md) page.

### Instalation on LXPLUS

For instuction how to install and run NXCALS Python package on LXPLUS please consult [Using LXPLUS](../using-lxplus.md) page.

### Instalation nxcals package from acc-py repository

#### Installation
Install Python NXCALS Extraction APIs (pySpark based).
To be able to install package, you need Python3, in version at least 3.7. (currently supported version is {% include 'generated/python_version.txt' %})
Acc-Py is available on machines from the CERN accelerator sector (ACC).

=== "Acc-Py"
    ```bash
    source /acc/local/share/python/acc-py/base/pro/setup.sh
    acc-py venv ./venv
    source ./venv/bin/activate
    python -m pip install nxcals
    ```

=== "Pure Python"
    ```bash
    python3 -m venv ./venv
    source ./venv/bin/activate
    python -m pip install -U --index-url https://acc-py-repo.cern.ch/repository/vr-py-releases/simple --trusted-host acc-py-repo.cern.ch nxcals
    ```

#### Running

You need a valid kerberos ticket. To init kerberos:
```bash
kinit
```

Activate virtual environment:
```bash
source ./venv/bin/activate
```

And later start python and create spark object:
    ```python
    from nxcals.spark_session_builder import get_or_create
    from nxcals.api.extraction.data.builders import DataQuery
    spark = get_or_create("My_APP")

    {% include 'generated/src/main/python/cern/nxcals/docs/DataAccessApi.py.121' %}
    ```

You can find more examples in [Extraction API](../extraction-api.md) chapter.
Common problems are described in [Setting up Python environments](../python-environments.md).