To access **data** (create Spark session, use NXCALS's SWAN software stacks) you need to be a member of the [**it-hadoop-nxcals-pro-analytics**](https://e-groups.cern.ch/e-groups/Egroup.do?egroupId=10249154)* e-group.

For **metadata** modification, for example, the creation of snapshots, hierarchies in Timber, or using directly our interfaces such as Group API etc., you need to be a member of:

- [**nxcals-pro-metadata-auth**](https://e-groups.cern.ch/e-groups/Egroup.do?egroupId=10527329)* for PRO environment
- [**nxcals-testbed-metadata-auth**](https://e-groups.cern.ch/e-groups/Egroup.do?egroupId=10527330)* for TESTBED environment
    
      
\* - you can self-subscribe  by selecting "members" tab and clicking on "Add me" button.

!!!note
    If you require any further assistance please do not hesitate to contact NXCALS support via email to [**acc-logging-support@cern.ch**](mailto:acc-logging-support@cern.ch)
