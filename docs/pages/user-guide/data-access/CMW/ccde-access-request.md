The [Controls Configuration Data Editor (CCDE)](https://ccde.cern.ch/dashboard) is a web-based application to interact with CCDB data.

Please visit [https://ccde.cern.ch/dashboard/loggingAccessRequest](https://ccde.cern.ch/dashboard/loggingAccessRequest) to request editor access to subscriptions from chosen accelerator.

After acceptance access to [search screen](https://ccde.cern.ch/nxcals/search) and subscription editor will be enabled.

!!! Important
    Depending on the environment, you should use the appropriate address for CCDE editor:
    
    - [https://ccde.cern.ch/dashboard](https://ccde.cern.ch/dashboard) for **PRO**
    - [https://ccde-testbed.cern.ch/dashboard](https://ccde-testbed.cern.ch/dashboard) for **TESTBED**

From that moment individual subscriptions for CMW data can be handled as described in [CMW subscriptions registration](../../logging-subscription/cmw-subscription-registration.md)
