#Ingestion API

In order to publish you need to create a publisher object and and use it to sends records as on the following snippet.
In this case the publishing is done in the caller thread.

=== "Java"
    ```java
    Publisher<ImmutableData> publisher = PublisherFactory.newInstance().createPublisher("MY_SYSTEM", Function.identity());
    ImmutableData record = ...;
    publisher.publish(record);
    ```

The publishing can be done asynchronous methods with provided executor & thread-pool.
Here ForkJoinPool is used implicitly.

=== "Java"
    ```java
    Publisher<ImmutableData> publisher = PublisherFactory.newInstance().createPublisher("MY_SYSTEM", Function.identity());
    ImmutableData record = ...;
    CompletableFuture<Result> output = publisher.publishAsync(record);
    ```

In this snippet the external executor can also be used.

=== "Java"
    ```java
    Executor executor = ...
    Publisher<ImmutableData> publisher = PublisherFactory.newInstance().createPublisher("MY_SYSTEM", Function.identity());
    ImmutableData record = ...;
    CompletableFuture<Result> output = publisher.publishAsync(record, executor);
    ```

There exists also a *BufferedPublisher* that can buffer and order messages before sending.
It can be used to avoid problems with schema changes for *fast data streams done from multiple threads*.

##Other examples

For the full working example (with error handling) of how to use Ingestion API please look into the example project: [Ingestion API Example](../examples-project/examples-ingestion).
All you need to do is to checkout the [example project](https://gitlab.cern.ch/acc-logging-team/nxcals-examples), navigate to the module ```ingestion-api-examples``` and explore.