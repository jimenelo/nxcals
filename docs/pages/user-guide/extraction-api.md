# Data Extraction API

In order to facilitate NXCALS data retrieval a Data Access API has been implemented in **Java**.
It should be considered as a **principal method of accessing logging data** and it serves as a base
for implementation of other data retrieval mechanisms which are proposed to users.

Those include **Python 3 library** written directly on top of the Java API as a thin set of native python units that are internally using Py4J - a bridge between Python and Java.
Py4J enables Python programs running in a Python interpreter to dynamically access Java objects in a JVM.

Python language is considered as a first-class citizen in the Spark world because of
the availability of multitude of libraries including the ones for visualizing data.
Being more analytical oriented it is a great choice for building data science applications.
Our python interface has been made available directly from python,
via PySpark shell (through NXCALS bundle) and SWAN web interface.

There is yet another possibility of accessing NXCALS API and it could be done through **Scala**.
Since Scala operates on the same JVM and provides language interoperability with the Java, the API
becomes automatically available in that language as well (unfortunately NXCALS team does not support Scala as a language).

It is worth to underline that thanks to our approach (reusing the object from the same shared JVM) we have achieved
homogeneous functionality across the **Java**, **Python** and **Scala** APIs.

**NXCALS Data Access API** itself consist of two query builders: **DataQuery** and **ParameterDataQuery** (**DevicePropertyDataQuery** is deprecated)
returning result dataset as an output.
It always expects specification of time window as a input altogether with information which allows identifying data signal such as:
system, generic key values pairs, device/property (in case of CMW system) or variable name (for backward compatibility).
More details about exact syntax with some examples can be found below.

!!!note
    The reference presented below is language independent. Concrete examples are given in Python, Java and Scala for clarification.

## DataQuery for key-values
Builder responsible for querying generic data using key/value pairs.
=== "entities()"
    ```python
    DataQuery.builder(spark).entities()
        .system("CMW")                                      # str / String
        .keyValuesEq({"key":"value"})                       # Dict[str, Any] / Map<String, Object>
        .keyValuesIn([{"key":"value"}, {"key":"value2"}])   # List[Dict[str, Any]] / List<Map<String, Object>>
        .keyValuesLike({"key":"value_pattern%"})            # Dict[str, Any] / Map<String, Object> pattern works only if is string and should be in Oracle SQL pattern style (% and _ as wildcards)
        .idEq(id)                                           # int / Long
        .idIn({id})                                         # Set[int] / Set<Long>
        .timeWindow(start_time, end_time)                   # Time may be in multiple formats (str "YYYY-MM-DD HH24:MI:SS.SSS", nanos, datetime etc.) - check type in code
        .atTime(time)                                       # Only one time method invoke is possible
        .fieldAliases({"alias": ("field1", "field2)})       # Dict[str, Set[str]] / Map<String, Set<String>> optional - allow to specify aliases
        .fieldAliases("alias", "field1")                    # String, ...String - not accessible in Python
        .build()
    ```
=== "byEntities() (deprecated)"
    ```python
    DataQuery.builder(spark).byEntities()
        .system(systemString)                                  # "SYSTEM"
            # Obligatory time range block
            .atTime(timeUtcString)                             # "YYYY-MM-DD HH24:MI:SS.SSS"
            .startTime(startTimeUtcString)                     # "YYYY-MM-DD HH24:MI:SS.SSS"
                .duration(duration)                            # NUMBER
                .endTime(endTimeUtcString)                     # "YYYY-MM-DD HH24:MI:SS.SSS"
            # Optional data context block
            .fieldAliases(fieldAliasesMap)                     # {"FIELD1": ["FIELD-ALIAS1"], "FIELD2": ["FIELD-ALIAS2"]}
            # Obligatory entity block which can be repeated
            .entity()
                .keyValue(keyString, valueString)              # "KEY", "VALUE"
                .keyValueLike(keyString, valueString)          # "KEY", "VALUE-WITH-WILDCARDS"
                .keyValues(keyValuesMap)                       # {"KEY1": "VALUE1", "KEY2": "VALUE2"}
                .keyValuesLike(keyString, valueString)         # {"KEY1", "VALUE1-WITH-WILDCARDS", "KEY2", "VALUE2-WITH-WILDCARDS"}
                    .entity()
                    .build()
    ```
#### Examples

=== "Python"
    ```python
    from nxcals.api.extraction.data.builders import DataQuery
    
    {% include 'generated/src/main/python/cern/nxcals/docs/DataAccessApi.py.21' %}
    ```
=== "Python (deprecated)"
    ```python
    from nxcals.api.extraction.data.builders import DataQuery
    
    {% include 'generated/src/main/python/cern/nxcals/docs/DataAccessApi.py.1' %}
    ```
=== "Java"
    ```java
    import cern.nxcals.api.extraction.data.builder.DataQuery;
    
    import org.apache.spark.sql.Dataset;
    import org.apache.spark.sql.Row;
    import java.util.HashMap;
    import java.util.Map;
    
    {% include 'generated/src/main/java/cern/nxcals/docs/DataAccessApi.java.21' %}
    ```
=== "Java (deprecated)"
    ```java
    import cern.nxcals.api.extraction.data.builder.DataQuery;
    
    import org.apache.spark.sql.Dataset;
    import org.apache.spark.sql.Row;
    import java.util.HashMap;
    import java.util.Map;
    
    {% include 'generated/src/main/java/cern/nxcals/docs/DataAccessApi.java.1' %}
    ```
=== "Scala"
    ```scala
    import cern.nxcals.api.extraction.data.builders._
    import scala.collection.JavaConversions._
    
    val df1 = DataQuery.builder(spark).entities().system("CMW").
        keyValuesEq(mapAsJavaMap(Map("device" -> "LHC.LUMISERVER", "property" -> "CrossingAngleIP1"))).
        timeWindow("2018-04-29 00:00:00.000", "2018-04-30 00:00:00.000").
        build()    
    
    val df2 = DataQuery.builder(spark).entities().system("CMW").
        keyValuesLike(mapAsJavaMap(Map("device" -> "LHC.LUMISERVER", "property" -> "CrossingAngleIP%"))).
        timeWindow("2018-04-29 00:00:00.000", "2018-04-30 00:00:00.000").
        build()

    val df3 = DataQuery.builder(spark).entities().system("CMW").
        idEq(57336).
        timeWindow("2018-04-29 00:00:00.000", "2018-04-30 00:00:00.000").
        build()
    ```
=== "Scala (deprecated)"
    ```scala
    import cern.nxcals.api.extraction.data.builders._
    import scala.collection.JavaConversions._
    
    val df1 = DataQuery.builder(spark).byEntities().system("WINCCOA").
        startTime("2018-06-15 00:00:00.000").endTime("2018-06-17 00:00:00.000").
        entity().keyValue("variable_name", "MB.C16L2:U_HDS_3").
        build()
        
    val df2 = DataQuery.builder(spark).byEntities().system("CMW").
        startTime("2018-04-29 00:00:00.000").endTime("2018-04-30 00:00:00.000").
        entity().keyValue("device", "LHC.LUMISERVER").keyValue("property", "CrossingAngleIP1").
        build()
    
    val df3 = DataQuery.builder(spark).byEntities().system("CMW").
        startTime("2018-04-29 00:00:00.000").endTime("2018-04-30 00:00:00.000").
        entity().keyValues(mapAsJavaMap(Map("device" -> "LHC.LUMISERVER", "property" -> "CrossingAngleIP1"))).
        build()    
    
    val df4 = DataQuery.builder(spark).byEntities().system("CMW").
        startTime("2018-04-29 00:00:00.000").endTime("2018-04-30 00:00:00.000").
        entity().keyValuesLike(mapAsJavaMap(Map("device" -> "LHC.LUMISERVER", "property" -> "CrossingAngleIP%"))).
        build()
    ```
 
## DataQuery for variables
Builder responsible for querying using variable names

=== "variables()"
    ```python
    DataQuery.builder(spark).variables()
        .system("CMW")                                      # str / String
        .nameEq("var")                                      # str / String
        .nameIn(["var"])                                    # List[str] / List<String>
        .nameLike("var_name_pattern%")                      # str / String, pattern should be in Oracle SQL style (% and _ as wildcards)
        .idEq(id)                                           # int / Long
        .idIn({id})                                         # Set[int] / Set<Long>
        .timeWindow(start_time, end_time)                   # Time may be in multiple formats (str "YYYY-MM-DD HH24:MI:SS.SSS", nanos, datetime etc.) - check type in code
        .atTime(time)                                       # Only one time method invoke is possible
        .fieldAliases({"alias": ["field1", "field2"]})      # Dict[str, Set[str]] / Map<String, Set<String>> optional - allow to specify aliases
        .fieldAliases("alias", "field1")                    # String, ...String - not accessible in Python
        .build()
    ```
=== "byVariables() (deprecated)"
    ```python
    DataQuery.builder(spark).byVariables()
        .system(systemString)                                  # "SYSTEM"
                # Obligatory time range block
                .atTime(timeUtcString)                         # "YYYY-MM-DD HH24:MI:SS.SSS"
                .startTime(startTimeUtcString)                 # "YYYY-MM-DD HH24:MI:SS.SSS"
                    .duration(Duration)                        # NUMBER
                    .endTime(endTimeUtcString)                 # "YYYY-MM-DD HH24:MI:SS.SSS"
                .variable(variableNameString)                  # "VARIABLE-NAME"
                .variableLike(variableNameString)              # "VARIABLE-NAME-WITH-WILDCARDS"          
                # Obligatory variable block
                .build()
    ```
#### Examples

=== "Python"
    ```python
    from nxcals.api.extraction.data.builders import DataQuery
    
    {% include 'generated/src/main/python/cern/nxcals/docs/DataAccessApi.py.22' %}
    ```
=== "Python (deprecated)"
    ```python
    from nxcals.api.extraction.data.builders import *
    
    {% include 'generated/src/main/python/cern/nxcals/docs/DataAccessApi.py.2' %}
    ```

=== "Java"
    ```java
    import cern.nxcals.api.extraction.data.builder.DataQuery;
    import org.apache.spark.sql.Dataset;
    import org.apache.spark.sql.Row;
    import com.google.common.collect.Sets;
    
    {% include 'generated/src/main/java/cern/nxcals/docs/DataAccessApi.java.22' %}
    ```
=== "Java (deprecated)"
    ```java
    import cern.nxcals.api.extraction.data.builder.DataQuery;
    import org.apache.spark.sql.Dataset;
    import org.apache.spark.sql.Row;
    
    {% include 'generated/src/main/java/cern/nxcals/docs/DataAccessApi.java.2' %}
    ```
=== "Scala"
    ```scala
    import cern.nxcals.api.extraction.data.builders._
    
    val df1 = DataQuery.builder(spark).variables().
        system("CMW").
        nameEq("LTB.BCT60:INTENSITY").
        timeWindow("2018-04-29 00:00:00.000", "2018-04-30 00:00:00.000").
        build()
        
    val df2 = DataQuery.builder(spark).variables().
        system("CMW").
        nameLike("LTB.BCT%:INTENSITY").
        timeWindow("2018-04-29 00:00:00.000", "2018-04-30 00:00:00.000").
        build()
        
    val df3 = DataQuery.builder(spark).variables().
        system("CMW").
        nameLike("LTB.BCT%:INTENSITY").
        nameEq("LTB.BCT60:INTENSITY").
        timeWindow("2018-04-29 00:00:00.000", "2018-04-30 00:00:00.000").
        build()

    val df4 = DataQuery.builder(spark).variables().
        system("CMW").
        idEq(1005050).
        idIn(Set(1011562)).
        timeWindow("2018-04-29 00:00:00.000", "2018-04-30 00:00:00.000").
        build()
    ```
=== "Scala (deprecated)"
    ```scala
    import cern.nxcals.api.extraction.data.builders._
    
    val df1 = DataQuery.builder(spark).byVariables().
        system("CMW").
        startTime("2018-04-29 00:00:00.000").endTime("2018-04-30 00:00:00.000").
        variable("LTB.BCT60:INTENSITY").
        build()
        
    val df2 = DataQuery.builder(spark).byVariables().
        system("CMW").
        startTime("2018-04-29 00:00:00.000").endTime("2018-04-30 00:00:00.000").
        variableLike("LTB.BCT%:INTENSITY").
        build()
        
    val df3 = DataQuery.builder(spark).byVariables().
        system("CMW").
        startTime("2018-04-29 00:00:00.000").endTime("2018-04-30 00:00:00.000").
        variableLike("LTB.BCT%:INTENSITY").
        variable("LTB.BCT60:INTENSITY").
        build()    
    ```
## CMW data query builders
Builder responsible for querying using device/property pairs. 

=== "ParameterDataQuery"
    ```python
    ParameterDataQuery.builder(spark)
        .system("CMW")                                      # str / String
        .deviceEq("dev")                                    # str / String
            .propertyEq("property")                         # str / String
        .deviceLike("dev_pattern%")                         # str / String - pattern should be in Oracle SQL style (% and _ as wildcards)
            .propertyLike("property_pattern%")              # str / String - pattern
        .parameterEq("device/property")                     # str / String - parameter in form "device/property"
        .parameterIn(["device/property"])                   # List[str] / List<String> - list of parameters
        .parameterLike("devi_e/proper%")                    # str / String - pattern should be in Oracle SQL style (% and _ as wildcards)
        .timeWindow(start_time, end_time)                   # Time may be in multiple formats (str "YYYY-MM-DD HH24:MI:SS.SSS", nanos, datetime etc.) - check type in code
        .atTime(time)                                       # Only one time method invoke is possible
        .fieldAliases({"alias": ["field1", "field2]})       # Dict[str, Set[str]] / Map<String, Set<String>> optional - allow to specify aliases
        .fieldAliases("alias", "field1")                    # String, ...String - not accessible in Python
        .build()
    ```
=== "DevicePropertyDataQuery (deprecated)"
    ```python
    DevicePropertyDataQuery.builder(spark)
        .system(systemString)                                  # "SYSTEM"
            # Obligatory time range block
            .atTime(timeUtcString)                             # "YYYY-MM-DD HH24:MI:SS.SSS"
            .startTime(startTimeUtcString)                     # "YYYY-MM-DD HH24:MI:SS.SSS"          
                .duration(duration)                            # NUMBER
                .endTime(endTimeUtcString)                     # "YYYY-MM-DD HH24:MI:SS.SSS"  
            # Optional data context block  
            .fieldAliases(fieldAliasesMap)                     # {"FIELD1": ["FIELD-ALIAS1"], "FIELD2": ["FIELD-ALIAS2"]}
            # Obligatory entity block which can be repeated
            .entity()                             
                .device(deviceString)                          # "DEVICE-NAME"
                .deviceLike(deviceString)                      # "DEVICE-NAME-WITH-WILDCARDS"
                    .property(propertyString)                  # "PROPERTY-NAME"
                    .propertyLike(propertyString)              # "PROPERTY-NAME-WITH-WILDCARDS"
                        .entity()
                        .build()
                .parameter(parameterString)                    # "VARIABLE-NAME/PROPERTY-NAME"
                .parameterLike(parameterString)                # "VARIABLE-NAME/PROPERTY-NAME-WITH-WILDCARDS"
                    .entity()
                    .build()
    ```
#### Examples

=== "Python"
    ```python
    from nxcals.api.extraction.data.builders import *
    
    {% include 'generated/src/main/python/cern/nxcals/docs/DataAccessApi.py.23' %}
    ```
=== "Python (deprecated)"
    ```python
    from nxcals.api.extraction.data.builders import *
    
    {% include 'generated/src/main/python/cern/nxcals/docs/DataAccessApi.py.3' %}
    ```
=== "Java"
    ```java
    import cern.nxcals.api.extraction.data.builder.ParameterDataQuery;
    
    import org.apache.spark.sql.Dataset;
    import org.apache.spark.sql.Row;
    import java.util.ArrayList;
    import java.util.HashMap;
    import java.util.List;
    import java.util.Map;
    
    {% include 'generated/src/main/java/cern/nxcals/docs/DataAccessApi.java.23' %}
    ```
=== "Java (deprecated)"
    ```java
    import cern.nxcals.api.extraction.data.builder.DevicePropertyDataQuery;
    
    import org.apache.spark.sql.Dataset;
    import org.apache.spark.sql.Row;
    import java.util.ArrayList;
    import java.util.HashMap;
    import java.util.List;
    import java.util.Map;
    
    {% include 'generated/src/main/java/cern/nxcals/docs/DataAccessApi.java.3' %}
    ```
=== "Scala"
    ```scala
    import cern.nxcals.api.extraction.data.builders._
    import scala.collection.JavaConverters._
    
    val df1 = ParameterDataQuery.builder(spark).system("CMW")
        parameterEq("RADMON.PS-10/ExpertMonitoringAcquisition").
        timeWindow("2017-08-29 00:00:00.000", "2017-08-29 00:00:10.000").
        build()
        
    val df2 = ParameterDataQuery.builder(spark).system("CMW").
        parameterIn("RADMON.PS-1/ExpertMonitoringAcquisition", "RADMON.PS-10/ExpertMonitoringAcquisition").
        timeWindow("2018-04-29 00:00:00.000", "2018-04-30 00:00:00.000").
        fieldsAliases(mapAsJavaMap(Map("CURRENT 18V" -> seqAsJavaList(Seq("current_18V", "voltage_18V"))))).
        build()
        
    val df3 = ParameterDataQuery.builder(spark).system("CMW").
        parameterLike("RADMON.PS-%/ExpertMonitoringAcquisition").
        timeWindow("2017-08-29 00:00:00.000", "2017-08-29 00:00:10.000").
        build()
    ```
=== "Scala (deprecated)"
    ```scala
    import cern.nxcals.api.extraction.data.builders._
    import scala.collection.JavaConverters._
    
    val df1 = DevicePropertyDataQuery.builder(spark).
        system("CMW").startTime("2017-08-29 00:00:00.000").duration(10000000000l).
        entity().parameter("RADMON.PS-10/ExpertMonitoringAcquisition").
        build()
        
    val df2 = DevicePropertyDataQuery.builder(spark).
        system("CMW").startTime("2018-04-29 00:00:00.000").endTime("2018-04-30 00:00:00.000").
        fieldAliases(mapAsJavaMap(Map("CURRENT 18V" -> seqAsJavaList(Seq("current_18V", "voltage_18V"))))).
        entity().device("RADMON.PS-1").property("ExpertMonitoringAcquisition").
        entity().parameter("RADMON.PS-10/ExpertMonitoringAcquisition").
        build()
        
    val df3 = DevicePropertyDataQuery.builder(spark).
        system("CMW").startTime("2017-08-29 00:00:00.000").duration(10000000000l).
        entity().parameterLike("RADMON.PS-%/ExpertMonitoringAcquisition").
        build()
    ```

## Alternative to builder static methods

There are another API methods, which are built on the top of builder. They can be used to query by entities and
variables.

### Variables

Syntax description for Python:

```python
DataQuery.getForVariables( 
    spark=spark,                                    # Spark session
    system=systemString,                            # "SYSTEM"
    start_time=startTimeUtcString,                  # "YYYY-MM-DD HH24:MI:SS.SSS"
    end_time=endTimeUtcString,                      # "YYYY-MM-DD HH24:MI:SS.SSS"
    variables= \                                    # Specify either variables, or variables_like, or both
        variable_names_list,                        # ["VARIABLE-NAME1", "VARIABLE-NAME2"]
    variables_like=variables_like_list              # ["VARIABLE-NAME1-WITH-WILDCARDS", "VARIABLE-NAME2-WITH-WILDCARDS"]
    field_aliases=field_aliases_map                 # {"FIELD1": ["FIELD-ALIAS1"], "FIELD2": ["FIELD-ALIAS2"]}
)
```
Snippets contain previous examples and queries rewritten to the new API.

Simple query for variable:

=== "Python"
    ```python
    from nxcals.api.extraction.data.builders import *

    {%include'generated/src/main/python/cern/nxcals/docs/DataAccessApi.py.9'%}
    ```
=== "Java"
    ```java
    import cern.nxcals.api.domain.TimeWindow;
    import cern.nxcals.api.extraction.data.builder.DataQuery;
    import org.apache.spark.sql.Dataset;
    import org.apache.spark.sql.Row;

    {%include'generated/src/main/java/cern/nxcals/docs/DataAccessApi.java.9'%}
    ```

Query for variable by pattern of variable name:

=== "Python"
    ```python
    from nxcals.api.extraction.data.builders import *

    {%include'generated/src/main/python/cern/nxcals/docs/DataAccessApi.py.10'%}
    ```
=== "Java"
    ```java
    import cern.nxcals.api.domain.TimeWindow;
    import cern.nxcals.api.extraction.data.builder.DataQuery;
    import org.apache.spark.sql.Dataset;
    import org.apache.spark.sql.Row;
    import com.google.common.collect.ImmutableList;
    import java.util.List;

    {%include'generated/src/main/java/cern/nxcals/docs/DataAccessApi.java.10'%}
    ```

You can also query for variables by multiple names and patterns:

=== "Python"
    ```python
    from nxcals.api.extraction.data.builders import *
    
    {%include'generated/src/main/python/cern/nxcals/docs/DataAccessApi.py.11'%}
    ```
=== "Java"
    ```java
    import cern.nxcals.api.domain.TimeWindow;
    import cern.nxcals.api.extraction.data.builder.DataQuery;
    import org.apache.spark.sql.Dataset;
    import org.apache.spark.sql.Row;
    import com.google.common.collect.ImmutableList;
    import java.util.List;

    {%include'generated/src/main/java/cern/nxcals/docs/DataAccessApi.java.11'%}
    ```

### Entities

Syntax description for Python:

```python
DataQuery.getForEntities( 
    spark=spark,                                    # Spark session
    system=systemString,                            # "SYSTEM"
    start_time=startTimeUtcString,                  # "YYYY-MM-DD HH24:MI:SS.SSS"
    end_time=endTimeUtcString,                      # "YYYY-MM-DD HH24:MI:SS.SSS"
    entity_queries=[                                # list of EntityQuery
        EntityQuery(                                # Specify either key values
            key_values=key_values_map,              # {"KEY1": "VALUE1", "KEY2": "VALUE2"}
        ),
        EntityQuery(                                # or key values like
            key_values_like=key_values_like_map     # {"KEY1", "VALUE1-WITH-WILDCARDS", "KEY2", "VALUE2-WITH-WILDCARDS"}
        ),
        EntityQuery(                                # or both
            key_values=key_values_map,              # {"KEY1": "VALUE1", "KEY2": "VALUE2"}
            key_values_like=key_values_like_map     # {"KEY1", "VALUE1-WITH-WILDCARDS", "KEY2", "VALUE2-WITH-WILDCARDS"}
        )   
    ],
    field_aliases=field_aliases_map                 # {"FIELD1": ["FIELD-ALIAS1"], "FIELD2": ["FIELD-ALIAS2"]}
)
```

Like above, snippets contain previous examples and queries rewritten to the new API below.

Search for entity by multiple key-values using Map:

=== "Python"
    ```python
    from nxcals.api.extraction.data.builders import *

    {%include'generated/src/main/python/cern/nxcals/docs/DataAccessApi.py.6'%}
    ```
=== "Java"
    ```java
    import cern.nxcals.api.domain.TimeWindow;
    import cern.nxcals.api.extraction.data.builder.DataQuery;
    import org.apache.spark.sql.Dataset;
    import org.apache.spark.sql.Row;
    import com.google.common.collect.ImmutableMap;
    import java.util.Map;

    {%include'generated/src/main/java/cern/nxcals/docs/DataAccessApi.java.6'%}
    ```

Using patterns in search for entities:

=== "Python"
    ```python
    from nxcals.api.extraction.data.builders import *

    {%include'generated/src/main/python/cern/nxcals/docs/DataAccessApi.py.7'%}
    ```
=== "Java"
    ```java
    import cern.nxcals.api.domain.EntityQuery;
    import cern.nxcals.api.domain.TimeWindow;
    import cern.nxcals.api.extraction.data.builder.DataQuery;
    import org.apache.spark.sql.Dataset;
    import org.apache.spark.sql.Row;
    import com.google.common.collect.ImmutableMap;
    import java.util.Collections;
    import java.util.Map;

    {%include'generated/src/main/java/cern/nxcals/docs/DataAccessApi.java.7'%}
    ```

It is also possible to run multiple queries for entities, using both simple key-values or patterns:

=== "Python"
    ```python
    from nxcals.api.extraction.data.builders import *

    {%include'generated/src/main/python/cern/nxcals/docs/DataAccessApi.py.8'%}
    ```
=== "Java"
    ```java
    import cern.nxcals.api.domain.EntityQuery;
    import cern.nxcals.api.domain.TimeWindow;
    import cern.nxcals.api.extraction.data.builder.DataQuery;
    import org.apache.spark.sql.Dataset;
    import org.apache.spark.sql.Row;
    import java.util.HashMap;
    import java.util.Map;

    {%include'generated/src/main/java/cern/nxcals/docs/DataAccessApi.java.8'%}
    ```

Simple search for entity by key-value (deprecated):

=== "Python"
    ```python
    from nxcals.api.extraction.data.builders import *

    {%include'generated/src/main/python/cern/nxcals/docs/DataAccessApi.py.4'%}
    ```
=== "Java"
    ```java
    import cern.nxcals.api.domain.TimeWindow;
    import cern.nxcals.api.extraction.data.builder.DataQuery;
    import org.apache.spark.sql.Dataset;
    import org.apache.spark.sql.Row;
    import com.google.common.collect.ImmutableMap;

    {%include'generated/src/main/java/cern/nxcals/docs/DataAccessApi.java.4'%}
    ```

Search for entity by multiple key-values (deprecated):

=== "Python"
    ```python
    from nxcals.api.extraction.data.builders import *

    {%include'generated/src/main/python/cern/nxcals/docs/DataAccessApi.py.5'%}
    ```
=== "Java"
    ```java
    import cern.nxcals.api.domain.TimeWindow;
    import cern.nxcals.api.extraction.data.builder.DataQuery;
    import org.apache.spark.sql.Dataset;
    import org.apache.spark.sql.Row;
    import com.google.common.collect.ImmutableMap;

    {%include'generated/src/main/java/cern/nxcals/docs/DataAccessApi.java.5'%}
    ```

## Other helper methods

### Pivot for variables

Often the problem when extracting variables is that they can have different types. Also separating values from different variables requires additional grouping or filtering. In such cases pivot on variables might be helpful. In general, it returns `Dataset`, which contains one column with timestamps (`"nxcals_timestamps"`) and other columns named after variable names, with values coming from these variables. It doesn't require variables to have the same type, neither to have aligned timestamps. In the latter case, if a variable doesn't have values for a given timestamp, there will be a `null` in output row. Since it requires a series of full-outer join operations, the performance of the operation is in general very low when compared to normal extraction. Then, we strongly recommend using it wisely, on reasonable small datasets.
Example of usage (please notice that it is marked as experimental, and may be subject to change in the future):

=== "Python"
    ```python
    from nxcals.api.extraction.data.builders import *

    {%include'generated/src/main/python/cern/nxcals/docs/DataAccessApi.py.12'%}
    ```
=== "Java"
    ```java
    import cern.nxcals.api.domain.TimeWindow;
    import cern.nxcals.api.extraction.data.builder.DataQuery;
    import cern.nxcals.api.extraction.metadata.ServiceClientFactory;
    import cern.nxcals.api.extraction.metadata.VariableService;
    import cern.nxcals.api.metadata.queries.Variables;
    import org.apache.spark.sql.Dataset;
    import org.apache.spark.sql.Row;

    {%include'generated/src/main/java/cern/nxcals/docs/DataAccessApi.java.24'%}
    ```

#### Pivot by variable names

Apart from above methods, they are overloaded to accept also variable names with system name. Examples:

=== "Python"
    ```python
    from nxcals.api.extraction.data.builders import *

    {%include'generated/src/main/python/cern/nxcals/docs/DataAccessApi.py.13'%}
    ```
=== "Java"
    ```java
    import cern.nxcals.api.domain.TimeWindow;
    import cern.nxcals.api.extraction.data.builder.DataQuery;
    import org.apache.spark.sql.Dataset;
    import org.apache.spark.sql.Row;

    {%include'generated/src/main/java/cern/nxcals/docs/DataAccessApi.java.25'%}
    ```