# Extraction Thin API
This API allows to send scripts or perform specific queries (currently related to Fills data) to the existing Spark Server which are executed on the server side and results are 
returned to the client.

!!!important
    The purpose of this API is to limit the number of jars (and potential clashes) when using Spark and simplify usage for small queries.
    It is not meant to be called from SWAN (where one can use provided pySpark) or be used for extensive data analysis acquiring large amount of data.

When working with scripts, results are in the form of Avro bytes that need to be converted to the desired row format. 
Currently we support converting to:
 
* [GenericRecord](https://avro.apache.org/docs/{% include 'generated/avro_version.txt' %}/api/java/index.html) from [Avro](https://svn.apache.org/repos/asf/avro/site/publish/docs/{% include 'generated/avro_version.txt' %}/spec.html)
* ImmutableData from CMW Datax 

For the queries related to fills results can be converted to native CERN Extraction API domain objects using provided methods:

* `#!java Fill toFill(FillData fillData)`
* `#!java List<Fill> toFillList(List<FillData> fillDataStream)`

and back to Thin API domain objects using: 

* `#!java FillData toFillData(Fill fill)`
* `#!java List<FillData> toFillsData(List<Fill> fills)`

The Thin API is currently only supported from Java, in order to use it from Python please resort to JPype. 

!!!important 
    Please make sure you don't mix together the jars from normal Extraction API together with the Thin client jars.
    Such a configuration will not work due to jar clashes between those two APIs. In order to use the Thin API please import the **nxcals-extraction-api-thin** product only. 

# Input script
The Thin API greatly limits the number of jars required to access NXCALS data. On the limitations side it requires that the passed script returns the 
Dataset<Row> type. Other return types are not supported for the moment.  
The script can be provided in the form of a string that contains references to our normal API calls. The syntax of the script is Javascript but you can 
use Java objects there. It has to be of the form: 
=== "Java"
    ```java 
    String script = "DataQuery.builder(sparkSession)" + 
    ".entities().system('CMW')" +  
    ".keyValuesEq({'property': 'Acquisition', 'device': 'SPSBQMSPSv1'})"
    ".timeWindow('2018-08-01 00:00:00.0', '2018-08-01 01:00:00.0').build()";
    ```
=== "Java (deprecated)"
    ```java 
    String script = "DataQuery.builder(sparkSession)" + 
    ".byEntities().system('CMW')" + 
    ".startTime('2018-08-01 00:00:00.0').endTime('2018-08-01 01:00:00.0')" + 
    ".entity().keyValue('property', 'Acquisition').keyValue('device', 'SPSBQMSPSv1').build()";
    ```

There are appropriate Query builders provided in the Thin API that will generate some typical basic query strings.


## Java 11
This is currently the only API that supports Java11. Please refer to the [compatibility page](supported-software-versions.md) for more information.

## Authentication

The API uses RBAC authentication so no Kerberos token is required. On the other hand one must provide
a valid RBAC token present in the environment. So some method of RBAC login is required to be executed 
before the Thin API is used. 

One example of the explicit RBAC login is here: 

=== "Java"
    ```java
    {% include 'generated/src/main/java/cern/nxcals/docs/extractionthin/ExtractionThinSnippets.java.1' %}
    ```

For other methods of login please refer to the RBAC documentation on the [wikis](https://confluence.cern.ch/display/MW/RBAC+User+Guide).

## Example of use

Please find below an example of the typical Thin API client calls for extracting some CMW data: 

=== "Java"
    ```java
    {% include 'generated/src/main/java/cern/nxcals/docs/extractionthin/ExtractionThinSnippets.java.5' %}
    ```
=== "Python"
    ```python
    #WORK IN PROGRESS, please use JPype
    ```

Old, deprecated API:
=== "Java"
    ```java
    {% include 'generated/src/main/java/cern/nxcals/docs/extractionthin/ExtractionThinSnippets.java.2' %}
    ```


and an example for extraction of data related to fills:

=== "Java"
    ```java
    {% include 'generated/src/main/java/cern/nxcals/docs/extractionthin/ExtractionThinSnippets.java.3' %}
    ```
    including helper method for printing Fill data:
    ```java
    {% include 'generated/src/main/java/cern/nxcals/docs/extractionthin/ExtractionThinSnippets.java.4' %}
    ```
=== "Python"
    ```python
    #WORK IN PROGRESS, please use JPype
    ```

??? note "Click to see expected application output..."
    ```
    Fill nr: 7245 Beam modes: INJPHYS,INJPROB,INJPHYS,PRERAMP,RAMP,FLATTOP,SQUEEZE,ADJUST,STABLE,BEAMDUMP,BEAMDUMP,RAMPDOWN
    Fill nr: 7246 Beam modes: RAMPDOWN,SETUP,INJPROB,INJPHYS
    Fill nr: 7247 Beam modes: INJPHYS,SETUP,INJPROB,SETUP,INJPROB,INJPHYS,INJPROB,INJPHYS
    Fill nr: 7248 Beam modes: INJPHYS,SETUP,INJPROB,INJPHYS
    Fill nr: 7249 Beam modes: INJPHYS,INJPROB,INJPHYS
    Fill nr: 7250 Beam modes: INJPHYS,INJPROB,INJPHYS,BEAMDUMP,CYCLING
    Fill nr: 7251 Beam modes: CYCLING,SETUP,INJPROB,INJPHYS
    Fill nr: 7252 Beam modes: INJPHYS,INJPROB,INJPHYS,PRERAMP,RAMP,FLATTOP,SQUEEZE,ADJUST,STABLE,BEAMDUMP,RAMPDOWN
    Fill nr: 7253 Beam modes: RAMPDOWN,SETUP,INJPROB,INJPHYS,PRERAMP,RAMP,FLATTOP,SQUEEZE,ADJUST,STABLE,BEAMDUMP,RAMPDOWN
    Fill nr: 7254 Beam modes: RAMPDOWN,SETUP,CYCLING
    Fill nr: 7255 Beam modes: CYCLING,SETUP,INJPROB,INJPHYS,INJPROB,INJPHYS
    ```

For the Spark Server addresses please use: 

* PRO: `{{spark_servers_pro}}`
* TESTBED: `{{spark_servers_testbed}}`

A full working example of the Thin API usage can be seen in our [Java examples project](https://gitlab.cern.ch/acc-logging-team/nxcals-examples/-/tree/master/extraction-api-thin-examples) 

Java doc can be found [here.](../../java-docs/index.html)
