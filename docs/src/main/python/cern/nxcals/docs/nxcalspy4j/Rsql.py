class RsqlSnippets:

    def __init__(self):
        pass

    @staticmethod
    def find_variable():
        # @snippet:1:on
        _metadata = spark._jvm.cern.nxcals.api.extraction.metadata
        variableService = _metadata.ServiceClientFactory.createVariableService()

        Variables = _metadata.queries.Variables

        variable = variableService \
            .findOne(getattr(Variables.suchThat().systemName().eq("CMW"), 'and')().variableName().eq("SPS:NXCALS_FUNDAMENTAL")) \
            .get()

        variable.getDescription()
        # @snippet:1:off

    @staticmethod
    def find_variables():
        # @snippet:2:on
        _metadata = spark._jvm.cern.nxcals.api.extraction.metadata
        variableService = _metadata.ServiceClientFactory.createVariableService()

        Variables = _metadata.queries.Variables

        variables = variableService \
            .findAll(getattr( \
                getattr(Variables.suchThat().variableName().eq("SPS:NXCALS_FUNDAMENTAL"), 'or')().variableName().eq("CPS:NXCALS_FUNDAMENTAL"), \
                'and')().systemName().eq("CMW"))

        len(variables)
        # @snippet:2:off