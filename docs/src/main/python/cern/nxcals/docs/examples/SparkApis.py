from nxcals.api.extraction.data.builders import DataQuery, DevicePropertyDataQuery

class SparkApis:

    def __init__(self):
            pass

    @staticmethod
    def queries(spark):

        # @snippet:1:on
        df = DevicePropertyDataQuery.builder(spark).system('CMW') \
            .startTime('2018-05-21 00:00:00.000').endTime('2018-05-23 13:30:00.000') \
            .entity().device('ZT10.QFO03').property('Acquisition').build()

        df.select('acqStamp', 'current').where("selector = 'CPS.USER.TOF'").show(10)
        # @snippet:1:off

        # @snippet:2:on
        df.createOrReplaceTempView('temp_table')
        df2 = spark.sql("SELECT selector, avg(t.current) avg_curr, min(t.current) min_curr, max(t.current) max_curr\
                         FROM temp_table t\
                         GROUP BY selector")
        df2.show()
        # @snippet:2:off

        # @snippet:3:on
        df_bpm = DataQuery.builder(spark).byVariables().system('CMW') \
            .startTime('2017-05-22 00:00:00.000').endTime('2017-05-23 13:30:00.000') \
            .variable('BPM_LHC_TEST') \
            .build()

        df_bpm.createOrReplaceTempView('bpm_temp_table')
        df2 = spark.sql( "SELECT t1.boolField result, count(t1.boolField) cnt\
                          FROM temp_table t1\
                          JOIN bpm_temp_table t2 ON t2.cycleStamp = t1.cycleStamp\
                          WHERE t2.floatField < 0.1\
                          GROUP BY t1.boolField")
        df2.show()
        # @snippet:3:off

        # @snippet:4:on
        from numpy import datetime64

        start = datetime64('2018-05-23T00:05:53.500000000').astype(datetime)
        end = datetime64('2018-05-23T01:37:39.100000000').astype(datetime)

        df2 = df.filter(df.acqStamp.between(start, end)).groupBy('selector').avg('current')
        df2.show()
        # @snippet:4:off

        # @snippet:5:on
        import pyspark.sql.functions as func

        df2 = df.filter(df.acqStamp.between(1527033953500000000, 1527039459100000000)) \
            .groupBy('selector').avg('current').select('selector', func.col('avg(current)').alias('avg_current'))
        df2.show()
        # @snippet:5:off

        # @snippet:6:on
        df2.where("selector like 'CPS.USER.S%'").show()
        # @snippet:6:off

        # @snippet:7:on
        df2.orderBy(df2.selector.desc()).show()
        # @snippet:7:off

        # @snippet:8:on
        df.select('selector').distinct.show()
        # @snippet:8:off

        # @snippet:9:on
        df.limit(50).crosstab('current','selector').show()
        # @snippet:9:off

        # @snippet:10:on
        df2.select('selector').dropDuplicates().show()
        # @snippet:10:off

        # @snippet:11:on
        df.select('selector','current_max').dropna().count()
        # @snippet:11:off

        # @snippet:12:on
        df2.select('selector','avg_current').withColumn('avg_exp',func.exp('avg_current')).show()
        # @snippet:12:off

        # @snippet:13:on
        df3 = df2.drop('selector')
        # @snippet:13:off
