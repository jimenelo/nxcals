# @snippet:1:on
from nxcals.api.extraction.data.builders import DevicePropertyDataQuery
# @snippet:1:off

class MultipleSchemas:

    def __init__(self):
        pass


    @staticmethod
    def query(spark):
        # @snippet:2:on
        df = DevicePropertyDataQuery \
            .builder(spark) \
            .system("CMW") \
            .startTime("2019-08-22 11:00:00.000") \
            .endTime("2019-08-22 12:00:00.000") \
            .entity() \
            .parameter("IGBT_EE_1/PM") \
            .build()
        # @snippet:2:off
