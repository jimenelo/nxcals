# @snippet:1:on
# source the nxcals query builders
from nxcals.api.extraction.data.builders import DevicePropertyDataQuery
from pyspark.sql.functions import max


# @snippet:1:off

class CmwQueries:

    def __init__(self):
        pass

    @staticmethod
    def query(spark):
        # @snippet:2:on
        # build the query (note: instead of parameter you can specify device/property separetly:
        # .device('PR.BCT').property('HotspotIntensity')
        intensity = DevicePropertyDataQuery.builder(spark).system('CMW') \
            .startTime('2018-06-17 00:00:00.000').endTime('2018-06-20 00:00:00.000') \
            .entity().parameter('PR.BCT/HotspotIntensity').build()

        # print the schema
        intensity.printSchema()
        # @snippet:2:off

        # @snippet:3:on
        # count the presence of recoreds that contain 'acqStamp' and 'dcBefInj1' fields
        intensity.select('acqStamp', 'dcBefInj1').count()
        # @snippet:3:off

        # @snippet:4:on
        tgmData = DevicePropertyDataQuery.builder(spark).system('CMW') \
            .startTime('2018-06-17 00:00:00.000').endTime('2018-06-20 00:00:00.000') \
            .entity().device('CPS.TGM').property('FULL-TELEGRAM.STRC').build()
        # @snippet:4:off

        # @snippet:5:on
        tgmData.groupBy('USER').count().show()
        # @snippet:5:off

        # @snippet:6:on
        userTOF = tgmData.select('cyclestamp').where("USER == 'TOF'")
        userTOF.show()
        # @snippet:6:off

        # @snippet:7:on
        result = intensity.join(userTOF, intensity.cyclestamp == userTOF.cyclestamp) \
            .select(intensity.cyclestamp, intensity.dcAftInj1)
        # @snippet:7:off

        # @snippet:8:on
        result.show(4)
        # @snippet:8:off

        # @snippet:9:on
        # Get intensity data
        intData = DevicePropertyDataQuery.builder(spark).system('CMW') \
            .startTime('2018-09-27 00:00:00.000').endTime('2018-09-28 01:00:00.000') \
            .entity().device('PR.BCT').property('HotspotIntensity').build()
        # @snippet:9:off

        # @snippet:10:on
        intData.where("selector = 'CPS.USER.SFTPRO1'").select(max(intData.dcAftInj1)).show()
        # @snippet:10:off

        # @snippet:11:on
        intData.describe('dcAftInj1').show()
        # @snippet:11:off

        # @snippet:12:on
        tgmDataReduced = DevicePropertyDataQuery.builder(spark).system('CMW') \
            .startTime('2018-06-15 00:00:00.000').endTime('2018-06-17 00:00:00.000') \
            .entity().parameter('CPS.TGM/FULL-TELEGRAM.STRC').build() \
            .select('BATCH','DEST','USER')
        # @snippet:12:off

        # @snippet:13:on
        tgmDataReduced.printSchema()
        # @snippet:13:off


