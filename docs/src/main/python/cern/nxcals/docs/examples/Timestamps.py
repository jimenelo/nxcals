# @snippet:1:on
import pandas as pd
from nxcals.api.extraction.data.builders import DataQuery
from pyspark.sql.functions import pandas_udf, PandasUDFType


# @snippet:1:off

class Timestamps:

    def __init__(self):
        pass

    @staticmethod
    def query(spark):
        # @snippet:2:on
        # Defining UDF function converting timestamp stored as long to Human readable version
        @pandas_udf('timestamp', PandasUDFType.SCALAR)
        def to_stamp(stamps):
            return pd.to_datetime(stamps, unit='ns')

        # Getting some sample data
        df = DataQuery.builder(spark).byEntities().system('CMW') \
            .startTime('2018-05-03 00:00:00.000').endTime('2018-05-03 01:00:00.000') \
            .entity().keyValue('device', 'LHC.LUMISERVER').keyValue('property', 'CrossingAngleIP1') \
            .build()

        # Add formated timestamp column and output result
        df.withColumn("stamp", to_stamp(df.acqStamp) ).select('stamp', 'acqStamp').toPandas()
        # @snippet:2:off
