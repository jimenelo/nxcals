from nxcals.api.extraction.data.builders import DataQuery, DevicePropertyDataQuery
from pyspark.sql import *


class NxcalsDemo:

    def __init__(self):
        pass

    @staticmethod
    def last_data_prior_to_timestamp_within_user_interval(spark):
        # @snippet:1:on
        # Necessary libraries.
        # The most convenient way for handling of timestamps with nanoseconds is Numpy:

        import numpy

        ref_time = numpy.datetime64('2018-05-23T00:05:54.500')
        interval_start = ref_time  - numpy.timedelta64(1,'D')

        df = DevicePropertyDataQuery.builder(spark).system('CMW') \
            .startTime(interval_start).endTime(ref_time) \
            .entity().device('ZT10.QFO03').property('Acquisition').build() \
            .select('cyclestamp', 'current').orderBy('cyclestamp', ascending=False).limit(1)

        df.show()
        # @snippet:1:off


    @staticmethod
    def data_in_time_window_and_last_data_prior_to_time_window_within_default_interval(spark):
        # @snippet:2:on
        # Necessary libraries:
        import numpy

        start_time = numpy.datetime64('2018-05-21 00:00:00.000')
        interval_start = start_time - numpy.timedelta64(30,'D')

        df= DevicePropertyDataQuery.builder(spark).system('CMW') \
            .startTime(start_time).endTime('2018-05-23 13:30:00.000') \
            .entity().device('ZT10.QFO03').property('Acquisition').build() \
            .select('cyclestamp', 'current').orderBy('cyclestamp', ascending=False) \
            .unionAll ( \
            DevicePropertyDataQuery.builder(spark).system('CMW') \
                .startTime(interval_start).endTime(start_time) \
                .entity().device('ZT10.QFO03').property('Acquisition').build() \
                .select('cyclestamp', 'current').orderBy('cyclestamp', ascending=False).limit(1))

        df.sort('cyclestamp').show(5)
        # @snippet:2:off

    @staticmethod
    def data_filtered_by_timestamps(spark):
        # @snippet:3:on
        # Necessary libraries:
        from datetime import datetime
        import numpy

        df = DevicePropertyDataQuery.builder(spark).system('CMW') \
            .startTime('2018-05-21 00:00:00.000').endTime('2018-05-23 13:30:00.000') \
            .entity().device('ZT10.QFO03').property('Acquisition').build()

        df.select('cyclestamp','current').show()
        # @snippet:3:off

        # @snippet:4:on
        df2 = spark.createDataFrame([
            Row(times=numpy.datetime64('2018-05-23T00:05:53.500000000').astype(datetime)),
            Row(times=numpy.datetime64('2018-05-23T01:15:53.500000000').astype(datetime)),
            Row(times=numpy.datetime64('2018-05-23T02:21:19.900000000').astype(datetime))])
        # @snippet:4:off


        # @snippet:5:on
        df.join(df2, df.cyclestamp == df2.times ) \
            .select('cyclestamp', 'current').show()
        # @snippet:5:off

        # @snippet:6:on
        print(numpy.datetime64(1527038153500000000,'ns'))
        # @snippet:6:off

    @staticmethod
    def data_distribution_in_timewindow_for_bins_number(spark):
        # @snippet:7:on
        from pyspark.ml.feature import Bucketizer
        from pyspark.sql.functions import max, min
        # @snippet:7:off

        # @snippet:8:on
        data = DevicePropertyDataQuery.builder(spark).system('CMW') \
            .startTime('2018-05-21 00:00:00.000').endTime('2018-05-23 13:30:00.000') \
            .entity().device('ZT10.QFO03').property('Acquisition').build() \
            .select('current')
        # @snippet:8:off

        # @snippet:9:on
        minmax=data.select(min('current').alias('minval'), max('current') \
                           .alias('maxval')).collect()[0]
        print(minmax)
        # @snippet:9:off

        # @snippet:10:on
        nr_buckets = 10
        width = (minmax.maxval - minmax.minval) / nr_buckets

        print(width)

        splits = [ minmax.minval + width * x for x in range(1, nr_buckets - 1) ]
        splits = [-float("inf")]+[minmax.minval]+splits+[minmax.maxval]+[float("inf")]
        print(splits)

        bucketizer = Bucketizer(splits=splits, inputCol="current", outputCol="bucketedFeatures")
        bucketed_data = bucketizer.transform(data)
        # @snippet:10:off

        # @snippet:11:on
        bucketed_data.select('bucketedFeatures').sort('bucketedFeatures') \
            .groupBy('bucketedFeatures').count().show()
        # @snippet:11:off

    @staticmethod
    def vectornumeric_data_in_time_window_filtered_by_vector_indices(spark):
        # @snippet:12:on
        df = DataQuery.builder(spark).byVariables().system('CMW') \
            .startTime('2018-05-21 00:00:00.000').endTime('2018-05-21 00:05:00.000') \
            .variable('SPS.BCTDC.51895:TOTAL_INTENSITY') \
            .build()
        # @snippet:12:off

        # @snippet:13:on
        inplist=df.select('nxcals_timestamp','nxcals_value').collect()
        # @snippet:13:off

        # @snippet:14:on
        indlist=[1,3]
        outlist=[]

        for element in inplist:
            retarray=[]
            if element.nxcals_value is not None:
                for ind in indlist:
                    retarray.append(element.nxcals_value.elements[ind])
                outlist.append(Row(nxcals_timestamp=element.nxcals_timestamp, nxcals_value=Row(elements=retarray, dimensions=[len(retarray)])))
            else:
                outlist.append(Row(nxcals_timestamp=element.nxcals_timestamp, nxcals_value=None))

        outdf=spark.createDataFrame(outlist)
        # @snippet:14:off

        # @snippet:15:on
        df.sort('nxcals_timestamp').select('nxcals_timestamp','nxcals_value').limit(5).collect()
        # @snippet:15:off

        # @snippet:16:on
        outdf.sort('nxcals_timestamp').limit(5).collect()
        # @snippet:16:off

    @staticmethod
    def multi_column_data_in_time_window(spark):
        # @snippet:17:on
        vdf1 = DataQuery.builder(spark).byVariables().system('CMW') \
            .startTime('2018-05-20 00:00:00.000').endTime('2018-05-20 00:12:00.000') \
            .variable('SPS.BCTDC.41435:INT_FLATTOP') \
            .build() \
            .withColumnRenamed('nxcals_value', 'value1')
        vdf2 = DataQuery.builder(spark).byVariables().system('CMW') \
            .startTime('2018-05-20 00:00:00.000').endTime('2018-05-20 00:12:00.000') \
            .variable('SPS.BCTDC.51895:SBF_INTENSITY') \
            .build() \
            .withColumnRenamed('nxcals_value', 'value2')
        # @snippet:17:off

        # @snippet:18:on
        tsdf = vdf1.select('nxcals_timestamp') \
            .union( vdf2.select('nxcals_timestamp')).distinct()
        # @snippet:18:off

        # @snippet:19:on
        vdf1=vdf1.withColumnRenamed('nxcals_timestamp', 'ts1').fillna("N/A")
        vdf2=vdf2.withColumnRenamed('nxcals_timestamp', 'ts2')
        # @snippet:19:off

        # @snippet:20:on
        outdf=tsdf.join(vdf1, tsdf.nxcals_timestamp == vdf1.ts1, how='leftouter') \
            .join(vdf2, tsdf.nxcals_timestamp == vdf2.ts2, how='leftouter')

        outdf.select('nxcals_timestamp','value1','value2') \
            .sort('nxcals_timestamp', ascending=False).show(2)
        # @snippet:20:off