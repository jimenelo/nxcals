# @snippet:1:on
import jpype

# Enable Java imports
import jpype.imports

# Import all standard Java types into the global scope
from jpype.types import *

# @snippet:1:off

class JPype:

    def __init__(self):
        pass

    @staticmethod
    def demo(spark):
        # @snippet:2:on
        # Launch the JVM, convertStrings=True is used for the proper conversion of java.lang.String to Python string literals
        jpype.startJVM(classpath=['/your_nxcals_package_location/venv/nxcals-bundle/jars/*',
                                  '/your_nxcals_package_location/venv/nxcals-bundle/nxcals_jars/*'], convertStrings=True)

        # point metadata client to NXCALS PRO services
        from java.lang import System
        System.setProperty("service.url", "{{public_services_pro}}");

        from cern.nxcals.api.extraction.metadata import ServiceClientFactory
        vs = ServiceClientFactory.createVariableService()

        from cern.nxcals.api.extraction.metadata.queries import Variables
        var = vs.findOne(Variables.suchThat().variableName().eq("HX:BMODE")).get()

        var.getVariableName()
        var.getId()
        # @snippet:2:off

    @staticmethod
    def demo2(spark):
        # @snippet:3:on
        # actual packages: cern.nxcals
        # expose as alias: nxcals_meta
        jpype.imports.registerDomain('nxcals_meta', alias='cern.nxcals')

        # now we can normally import and use exposed units under package alias
        from nxcals_meta.api.extraction.metadata import ServiceClientFactory
        vs = ServiceClientFactory.createVariableService()

        from nxcals_meta.api.extraction.metadata.queries import Variables
        var = vs.findOne(Variables.suchThat().variableName().eq("HX:BMODE")).get()

        var.getVariableName()
        # @snippet:3:off
