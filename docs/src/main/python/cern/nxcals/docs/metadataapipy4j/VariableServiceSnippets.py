class VariableServiceSnippets:

    def __init__(self):
        pass

    @staticmethod
    def find_variables_using_pattern_on_name():
        # @snippet:1:on
        _nxcals_api = spark._jvm.cern.nxcals.api
        ServiceClientFactory = _nxcals_api.extraction.metadata.ServiceClientFactory
        Variables = _nxcals_api.extraction.metadata.queries.Variables

        variableService = ServiceClientFactory.createVariableService()

        variables = variableService.findAll(
            getattr(Variables.suchThat().systemName().eq("CMW"), 'and')().variableName().like("SPS%"))
        # @snippet:1:off

    @staticmethod
    def find_variables_using_pattern_on_description():
        # @snippet:2:on
        _nxcals_api = spark._jvm.cern.nxcals.api
        ServiceClientFactory = _nxcals_api.extraction.metadata.ServiceClientFactory
        Variables = _nxcals_api.extraction.metadata.queries.Variables

        variableService = ServiceClientFactory.createVariableService()

        variables = variableService.findAll(
            getattr(Variables.suchThat().systemName().eq("CMW"), 'and')().description().like("%Current Beam Energy%"))
        # @snippet:2:off

    @staticmethod
    def create_new_variable():
        # @snippet:3:on
        _nxcals_api = spark._jvm.cern.nxcals.api
        ServiceClientFactory = _nxcals_api.extraction.metadata.ServiceClientFactory
        Variables = _nxcals_api.extraction.metadata.queries.Variables
        Entities = _nxcals_api.extraction.metadata.queries.Entities
        VariableConfig = _nxcals_api.domain.VariableConfig
        TimeWindow = _nxcals_api.domain.TimeWindow
        Variable = _nxcals_api.domain.Variable
        ImmutableSortedSet = spark._jvm.com.google.common.collect.ImmutableSortedSet

        SYSTEM = "CMW"
        DEMO_VARIABLE = "DEMO:VARIABLE"

        variableService = ServiceClientFactory.createVariableService()

        variable = variableService.findOne(getattr(Variables.suchThat().systemName().eq(SYSTEM), 'and')().variableName().eq(DEMO_VARIABLE))

        if variable.isPresent():

            updatedVariable = variable.get().toBuilder().description("updated description").build()
            variableService.update(updatedVariable)

        else:
            entityService = ServiceClientFactory.createEntityService()

            entity = entityService.findOne(getattr(Entities.suchThat().systemName().eq(SYSTEM), 'and')().keyValues().like("%myUniqueDeviceProperty%"))

            if entity.isEmpty():
                raise ValueError("No such entity")

            config = VariableConfig.builder().entityId(entity.get().getId()).fieldName("myFieldName").validity(
                TimeWindow.between(None, None)).build()

            variableData = Variable.builder().variableName(DEMO_VARIABLE).configs(
                ImmutableSortedSet.of(config)).systemSpec(entity.get().getSystemSpec()).build()

            variableService.create(variableData)
        # @snippet:3:off

    @staticmethod
    def get_sorted_list_of_variables():
        # @snippet:4:on
        _nxcals_api = spark._jvm.cern.nxcals.api
        ServiceClientFactory = _nxcals_api.extraction.metadata.ServiceClientFactory
        Variables = _nxcals_api.metadata.queries.Variables  # New package!

        variable_service = ServiceClientFactory.createVariableService()

        variables = variable_service.findAll(Variables.suchThat()
                                             .variableName().like("SPS%")
                                             .withOptions()
                                             .noConfigs()
                                             .orderBy().variableName().asc()
                                             .limit(10))
        # @snippet:4:off
