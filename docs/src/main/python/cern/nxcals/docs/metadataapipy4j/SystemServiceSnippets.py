class SystemServiceSnippets:

    def __init__(self):
       pass

    @staticmethod
    def find_system_info():
        # @snippet:1:on
        systemService = spark._jvm.cern.nxcals.api.extraction.metadata.ServiceClientFactory.createSystemSpecService()

        systemData = systemService.findByName("CMW")

        if systemData.isEmpty():
            raise ValueError("No such system")
        # @snippet:1:off
