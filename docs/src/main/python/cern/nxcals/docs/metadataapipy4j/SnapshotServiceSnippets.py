class SnapshotServiceSnippets:

    def __init__(self):
        pass

    @staticmethod
    def find_one_snapshot():
        # @snippet:1:on
        _nxcals_api = spark._jvm.cern.nxcals.api

        SnapshotServiceFactory = _nxcals_api.custom.extraction.metadata.SnapshotServiceFactory
        ServiceClientFactory = _nxcals_api.extraction.metadata.ServiceClientFactory

        Snapshots = _nxcals_api.custom.extraction.metadata.queries.Snapshots

        snapshotService = SnapshotServiceFactory.createSnapshotService()
        systemName = "CERN"

        systemSpec = ServiceClientFactory.createSystemSpecService().findByName(systemName)

        if systemSpec.isEmpty():
            raise ValueError("System not found")

        snapshot = snapshotService.findOne(getattr(getattr(Snapshots.suchThat().systemName().eq(systemName), 'and')().name().eq("FILLS"), 'and')().variableName().like("%"))

        if snapshot.isEmpty():
            raise ValueError("Snapshot not found")
        snapshot = snapshot.get()

        # Retrieve attached variables per association
        associationVariableMap = snapshotService.getVariables(snapshot.getId())
        # @snippet:1:off

    @staticmethod
    def extract_data_using_snapshot():
        # @snippet:2:on
        _nxcals_api = spark._jvm.cern.nxcals.api
        TimeDefinition = _nxcals_api.custom.domain.snapshot.TimeDefinition
        Snapshot = _nxcals_api.custom.domain.Snapshot
        Instant = spark._jvm.java.time.Instant
        ImmutableSet = spark._jvm.com.google.common.collect.ImmutableSet

        SnapshotServiceFactory = _nxcals_api.custom.extraction.metadata.SnapshotServiceFactory
        ServiceClientFactory = _nxcals_api.extraction.metadata.ServiceClientFactory

        Entities = _nxcals_api.extraction.metadata.queries.Entities
        Variables = _nxcals_api.extraction.metadata.queries.Variables

        snapshotService = SnapshotServiceFactory.createSnapshotService()
        entityService = ServiceClientFactory.createEntityService()
        variableService = ServiceClientFactory.createVariableService()

        system = ServiceClientFactory.createSystemSpecService().findByName("CMW")
        if system.isEmpty():
            raise ValueError("System not found")

        timeDefinition = TimeDefinition.fixedDates(
            Instant.parse("2023-05-15T20:17:30.151238525Z"),
            Instant.parse("2023-05-16T05:15:03.533738525Z"))

        snapshot = Snapshot.builder().timeDefinition(timeDefinition).build()

        keyValues = {"device": "LHC.LUMISERVER", "property": "CrossingAngleIP1"}

        entity = entityService.findOne(
            getattr(Entities.suchThat().systemId().eq(system.getId()), 'and')().keyValues().eq(system, keyValues))

        if entity.isEmpty():
            raise ValueError("Entity not found")
        entity = entity.get()

        variable = variableService.findOne(
            getattr(Variables.suchThat().systemId().eq(system.getId()), 'and')().variableName().eq("DEMO:VARIABLE"))

        if variable.isEmpty():
            raise ValueError("Variable not found")
        variable = variable.get()

        created = snapshotService.create(
            snapshot, ImmutableSet.of(entity), ImmutableSet.of(variable),
            ImmutableSet.of(),ImmutableSet.of(),ImmutableSet.of())
        # @snippet:2:off