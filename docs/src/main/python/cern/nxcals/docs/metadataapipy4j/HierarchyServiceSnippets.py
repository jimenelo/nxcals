class HierarchyServiceSnippets:

    def __init__(self):
        pass

    @staticmethod
    def find_hierarchies_using_pattern_on_path():
        # @snippet:1:on
        _nxcals_api = spark._jvm.cern.nxcals.api
        ServiceClientFactory = _nxcals_api.extraction.metadata.ServiceClientFactory
        Hierarchies = _nxcals_api.extraction.metadata.queries.Hierarchies

        hierarchyService = ServiceClientFactory.createHierarchyService()

        hierarchies = hierarchyService.findAll(Hierarchies.suchThat().path().like("%example%"))
        # @snippet:1:off

    @staticmethod
    def attach_new_node():
        # @snippet:2:on
        _nxcals_api = spark._jvm.cern.nxcals.api
        ServiceClientFactory = _nxcals_api.extraction.metadata.ServiceClientFactory
        Hierarchies = _nxcals_api.extraction.metadata.queries.Hierarchies
        Hierarchy = _nxcals_api.domain.Hierarchy

        hierarchyService = ServiceClientFactory.createHierarchyService()
        systemName = "CMW"

        systemSpec = ServiceClientFactory.createSystemSpecService().findByName(systemName)

        if systemSpec.isEmpty():
            raise ValueError("No such system specification")

        parent = hierarchyService.findOne(getattr(Hierarchies.suchThat().systemName().eq(systemName), 'and')().path().eq("/example"))

        if parent.isEmpty():
            raise ValueError("No such hierarchy")

        newHierarchy = Hierarchy.builder().name("new node").description("New node description").systemSpec(systemSpec.get()).parent(parent.get()).build()

        modified = hierarchyService.create(newHierarchy)

        modified.toBuilder().description("Modified node description").build()

        hierarchyService.update(modified)
        # @snippet:2:off

    @staticmethod
    def delete_node():
        _nxcals_api = spark._jvm.cern.nxcals.api
        ServiceClientFactory = _nxcals_api.extraction.metadata.ServiceClientFactory
        Hierarchies = _nxcals_api.extraction.metadata.queries.Hierarchies

        hierarchyService = ServiceClientFactory.createHierarchyService()
        systemName = "CMW"

        # @snippet:3:on
        leaf = hierarchyService.findOne(getattr(Hierarchies.suchThat().systemName().eq(systemName), 'and')().path().eq("/example/new node"))
        if leaf.isEmpty():
            raise ValueError("No such hierarchy")

        hierarchyService.deleteLeaf(leaf.get())
        # @snippet:3:off

    @staticmethod
    def attach_variable_to_node():
        # @snippet:4:on
        _nxcals_api = spark._jvm.cern.nxcals.api
        ServiceClientFactory = _nxcals_api.extraction.metadata.ServiceClientFactory
        Hierarchies = _nxcals_api.extraction.metadata.queries.Hierarchies
        variables = _nxcals_api.extraction.metadata.queries.Variables
        Collections = spark._jvm.java.util.Collections

        hierarchyService = ServiceClientFactory.createHierarchyService()
        variableService = ServiceClientFactory.createVariableService()

        systemName = "CMW"

        hierarchy = hierarchyService.findOne(getattr(Hierarchies.suchThat().systemName().eq(systemName), 'and')().path().eq("/example"))

        if hierarchy.isEmpty():
            raise ValueError("No such hierarchy")

        # Prepare variable to be attached to the hierarchy node
        variable = variableService.findOne(getattr(Variables.suchThat().systemName().eq(systemName), 'and')().variableName().eq("DEMO:VARIABLE"))

        if variable.isEmpty():
            raise ValueError("No such variable")

        # The variable will be added to the set of already attached variables
        hierarchyService.addVariables(hierarchy.get().getId(), Collections.singleton(variable.get().getId()))

        variables = hierarchyService.getVariables(hierarchy.get().getId())
        # @snippet:4:off

    @staticmethod
    def replace_entities_with_single_entity():
        # @snippet:5:on
        _nxcals_api = spark._jvm.cern.nxcals.api
        ServiceClientFactory = _nxcals_api.extraction.metadata.ServiceClientFactory
        Entities = _nxcals_api.extraction.metadata.queries.Entities
        Hierarchies = _nxcals_api.extraction.metadata.queries.Hierarchies
        Collections = spark._jvm.java.util.Collections
        Iterables = spark._jvm.com.google.common.collect.Iterables

        hierarchyService = ServiceClientFactory.createHierarchyService()
        entityService = ServiceClientFactory.createEntityService()
        hierarchySystemName = "CERN"
        systemName = "CMW"

        hierarchy = hierarchyService.findOne(
            getattr(Hierarchies.suchThat().systemName().eq(hierarchySystemName), 'and')().path().eq("/example"))

        if hierarchy.isEmpty():
            raise ValueError("No such hierarchy")

        # Obtain an entity of your interest
        entity = Iterables.getLast(entityService
                                   .findAll(Entities.suchThat().systemName().eq(systemName)))
        print(entity)

        # The entity will replace a set of already attached entities
        hierarchyService.setEntities(hierarchy.get().getId(), Collections.singleton(entity.getId()))

        entities = hierarchyService.getEntities(hierarchy.get().getId())
        print(entities)
        # @snippet:5:off