from cern.nxcals.api.extraction.metadata import ServiceClientFactory
from cern.nxcals.api.extraction.metadata.queries import Variables

class RsqlSnippets:

    def __init__(self):
        pass

    @staticmethod
    def find_variable():
        variableService = ServiceClientFactory.createVariableService()

        # @snippet:1:on
        variable = variableService \
            .findOne(Variables.suchThat().systemName().eq("CMW").and_().variableName().eq("SPS:NXCALS_FUNDAMENTAL")) \
            .get()
        variable.getDescription()
        # @snippet:1:off

    @staticmethod
    def find_variables():
        variableService = ServiceClientFactory.createVariableService()

        # @snippet:2:on
        variables = variableService.findOne(Variables.suchThat() \
            .systemName().eq("CMW").and_(Variables.suchThat.variableName().eq("SPS:NXCALS_FUNDAMENTAL").or_().variableName().eq("CPS:NXCALS_FUNDAMENTAL")))
        # @snippet:2:off