class AggregationServiceSnippets:

    def __init__(self):
        pass

    @staticmethod
    def getService():
        # @snippet:1:on
        from cern.nxcals.api.custom.service import Services
        from cern.nxcals.api.config import SparkProperties

        aggregationService = Services.newInstance(SparkProperties.defaults("MY_APP")) \
            .aggregationService()
        # @snippet:1:off
        return aggregationService


    @staticmethod
    def getServicePy4j():
        # @snippet:11:on
        from nxcals.spark_session_builder import get_or_create  # on swan create spark session from UI (star icon)
        spark = get_or_create()

        Services = spark._jvm.cern.nxcals.api.custom.service.Services

        aggregationService = Services.newInstance(spark._jsparkSession).aggregationService()
        # @snippet:11:off
        return spark, aggregationService


    @staticmethod
    def getAggregatedVariableDataAvg():

        aggregationService = AggregationServiceSnippets.getService()
        # @snippet:2:on
        from cern.nxcals.api.extraction.metadata import ServiceClientFactory
        from cern.nxcals.api.extraction.metadata.queries import Variables
        from cern.nxcals.api.utils import TimeUtils
        from java.time.temporal import ChronoUnit

        from cern.nxcals.api.custom.service.aggregation import AggregationFunctions
        from cern.nxcals.api.custom.service.aggregation import WindowAggregationProperties

        variableService = ServiceClientFactory.createVariableService()

        myVariable = variableService.findOne(Variables.suchThat().variableName().eq("CPS.TGM:CYCLE"))

        if myVariable.isEmpty():
            raise ValueError("Could not obtain variable from service")

        startTime = TimeUtils.getInstantFromString("2020-04-25 00:00:00.000000000")
        endTime = TimeUtils.getInstantFromString("2020-04-27 00:00:00.000000000")

        properties = WindowAggregationProperties.builder().timeWindow(startTime, endTime) \
            .interval(8, ChronoUnit.HOURS).function((AggregationFunctions.AVG)).build()

        print(properties)

        dataset = aggregationService.getData(myVariable.get(), properties)
        dataset.show()
        # @snippet:2:off

    @staticmethod
    def getAggregatedVariableDataAvgPy4j():

        spark, aggregationService = AggregationServiceSnippets.getServicePy4j()
        # @snippet:12:on
        ServiceClientFactory = spark._jvm.cern.nxcals.api.extraction.metadata.ServiceClientFactory
        Variables = spark._jvm.cern.nxcals.api.extraction.metadata.queries.Variables
        TimeUtils = spark._jvm.cern.nxcals.api.utils.TimeUtils
        ChronoUnit = spark._jvm.java.time.temporal.ChronoUnit

        AggregationFunctions = spark._jvm.cern.nxcals.api.custom.service.aggregation.AggregationFunctions
        WindowAggregationProperties = spark._jvm.cern.nxcals.api.custom.service.aggregation.WindowAggregationProperties

        variableService = ServiceClientFactory.createVariableService()

        myVariable = variableService.findOne(Variables.suchThat().variableName().eq("CPS.TGM:CYCLE"))

        if myVariable.isEmpty():
            raise ValueError("Could not obtain variable from service")

        startTime = TimeUtils.getInstantFromString("2020-04-25 00:00:00.000000000")
        endTime = TimeUtils.getInstantFromString("2020-04-27 00:00:00.000000000")

        properties = WindowAggregationProperties.builder().timeWindow(startTime, endTime) \
            .interval(8, ChronoUnit.HOURS).function((AggregationFunctions.AVG)).build()

        print(properties)

        dataset = aggregationService.getData(myVariable.get(), properties)
        dataset.show()
        # @snippet:12:off

    @staticmethod
    def getAggregatedEntityDataMax():
        aggregationService = AggregationServiceSnippets.getService()
        # @snippet:3:on
        from cern.nxcals.api.extraction.metadata import ServiceClientFactory
        from cern.nxcals.api.extraction.metadata.queries import Entities

        from cern.nxcals.api.utils import TimeUtils
        from java.time.temporal import ChronoUnit

        from cern.nxcals.api.custom.service.aggregation import AggregationFunctions
        from cern.nxcals.api.custom.service.aggregation import WindowAggregationProperties

        systemService = ServiceClientFactory.createSystemSpecService()
        entityService = ServiceClientFactory.createEntityService()

        cmwSystemSpec = systemService.findByName("CMW")

        if cmwSystemSpec.isEmpty():
            raise ValueError("No such system")

        from cern.nxcals.api.custom.domain.CmwSystemConstants import DEVICE_KEY_NAME
        from cern.nxcals.api.custom.domain.CmwSystemConstants import PROPERTY_KEY_NAME
        from com.google.common.collect import ImmutableMap

        keyValues = ImmutableMap.of(DEVICE_KEY_NAME, "CPS.TGM", PROPERTY_KEY_NAME, "FULL-TELEGRAM.STRC")

        myEntity = entityService.findOne(Entities.suchThat().keyValues().eq(cmwSystemSpec.get(), keyValues))

        if myEntity.isEmpty():
            raise ValueError("Could not obtain entity from service")

        startTime = TimeUtils.getInstantFromString("2020-04-10 00:00:00.000000000")
        endTime = TimeUtils.getInstantFromString("2020-04-14 00:00:10.000000000")

        properties = WindowAggregationProperties.builder().timeWindow(startTime, endTime) \
            .interval(1, ChronoUnit.DAYS).function((AggregationFunctions.MAX)).aggregationField("CYCLE").build()

        dataset = aggregationService.getData(myEntity.get(), properties)
        dataset.show()
        # @snippet:3:off

    @staticmethod
    def getAggregatedEntityDataMaxPy4j():
        spark, aggregationService = AggregationServiceSnippets.getServicePy4j()
        # @snippet:13:on
        ServiceClientFactory = spark._jvm.cern.nxcals.api.extraction.metadata.ServiceClientFactory
        Entities = spark._jvm.cern.nxcals.api.extraction.metadata.queries.Entities
        TimeUtils = spark._jvm.cern.nxcals.api.utils.TimeUtils
        ChronoUnit = spark._jvm.java.time.temporal.ChronoUnit

        AggregationFunctions = spark._jvm.cern.nxcals.api.custom.service.aggregation.AggregationFunctions
        WindowAggregationProperties = spark._jvm.cern.nxcals.api.custom.service.aggregation.WindowAggregationProperties

        DEVICE_KEY_NAME = spark._jvm.cern.nxcals.api.custom.domain.CmwSystemConstants.DEVICE_KEY_NAME
        PROPERTY_KEY_NAME = spark._jvm.cern.nxcals.api.custom.domain.CmwSystemConstants.PROPERTY_KEY_NAME
        ImmutableMap = spark._jvm.com.google.common.collect.ImmutableMap

        systemService = ServiceClientFactory.createSystemSpecService()
        entityService = ServiceClientFactory.createEntityService()

        cmwSystemSpec = systemService.findByName("CMW")

        if cmwSystemSpec.isEmpty():
            raise ValueError("No such system")

        keyValues = ImmutableMap.of(DEVICE_KEY_NAME, "CPS.TGM", PROPERTY_KEY_NAME, "FULL-TELEGRAM.STRC")

        myEntity = entityService.findOne(Entities.suchThat().keyValues().eq(cmwSystemSpec.get(), keyValues))

        if myEntity.isEmpty():
            raise ValueError("Could not obtain entity from service")

        startTime = TimeUtils.getInstantFromString("2020-04-10 00:00:00.000000000")
        endTime = TimeUtils.getInstantFromString("2020-04-14 00:00:10.000000000")

        properties = WindowAggregationProperties.builder().timeWindow(startTime, endTime) \
            .interval(1, ChronoUnit.DAYS).function((AggregationFunctions.MAX)).aggregationField("CYCLE").build()

        dataset = aggregationService.getData(myEntity.get(), properties)
        dataset.show()
        # @snippet:13:off

    @staticmethod
    def getAggregatedVariableDataRepeat():
        aggregationService = AggregationServiceSnippets.getService()
        # @snippet:4:on
        from cern.nxcals.api.extraction.metadata import ServiceClientFactory
        from cern.nxcals.api.extraction.metadata.queries import Variables
        from cern.nxcals.api.utils import TimeUtils
        from java.time.temporal import ChronoUnit

        from cern.nxcals.api.custom.service.aggregation import AggregationFunctions
        from cern.nxcals.api.custom.service.aggregation import WindowAggregationProperties

        variableService = ServiceClientFactory.createVariableService()

        myVariable = variableService.findOne(Variables.suchThat().variableName().eq("CPS.TGM:CYCLE"))

        if myVariable.isEmpty():
            raise ValueError("Could not obtain variable from service")

        startTime = TimeUtils.getInstantFromString("2020-04-25 00:00:00.000000000")
        endTime = TimeUtils.getInstantFromString("2020-04-25 00:05:00.000000000")

        properties = WindowAggregationProperties.builder().timeWindow(startTime, endTime) \
            .interval(20, ChronoUnit.SECONDS).function((AggregationFunctions.REPEAT)).build()
        dataset = aggregationService.getData(myVariable.get(), properties)
        dataset.show()
        # @snippet:4:off

    @staticmethod
    def getAggregatedVariableDataRepeatPy4j():
        spark, aggregationService = AggregationServiceSnippets.getServicePy4j()
        # @snippet:14:on
        ServiceClientFactory = spark._jvm.cern.nxcals.api.extraction.metadata.ServiceClientFactory
        Variables = spark._jvm.cern.nxcals.api.extraction.metadata.queries.Variables
        TimeUtils = spark._jvm.cern.nxcals.api.utils.TimeUtils
        ChronoUnit = spark._jvm.java.time.temporal.ChronoUnit

        AggregationFunctions = spark._jvm.cern.nxcals.api.custom.service.aggregation.AggregationFunctions
        WindowAggregationProperties = spark._jvm.cern.nxcals.api.custom.service.aggregation.WindowAggregationProperties

        variableService = ServiceClientFactory.createVariableService()

        myVariable = variableService.findOne(Variables.suchThat().variableName().eq("CPS.TGM:CYCLE"))

        if myVariable.isEmpty():
            raise ValueError("Could not obtain variable from service")

        startTime = TimeUtils.getInstantFromString("2020-04-25 00:00:00.000000000")
        endTime = TimeUtils.getInstantFromString("2020-04-25 00:05:00.000000000")

        properties = WindowAggregationProperties.builder().timeWindow(startTime, endTime) \
            .interval(20, ChronoUnit.SECONDS).function((AggregationFunctions.REPEAT)).build()
        dataset = aggregationService.getData(myVariable.get(), properties)
        dataset.show()
        # @snippet:14:off

    @staticmethod
    def getAggregatedEntityDataInterpolate():
        aggregationService = AggregationServiceSnippets.getService()
        # @snippet:5:on
        from cern.nxcals.api.extraction.metadata import ServiceClientFactory
        from cern.nxcals.api.extraction.metadata.queries import Entities
        from cern.nxcals.api.utils import TimeUtils
        from java.time.temporal import ChronoUnit

        from cern.nxcals.api.custom.service.aggregation import AggregationFunctions
        from cern.nxcals.api.custom.service.aggregation import WindowAggregationProperties

        entityService = ServiceClientFactory.createEntityService()
        systemService = ServiceClientFactory.createSystemSpecService()

        cmwSystemSpec = systemService.findByName("CMW")

        if cmwSystemSpec.isEmpty():
            raise ValueError("No such system")

        from cern.nxcals.api.custom.domain.CmwSystemConstants import DEVICE_KEY_NAME
        from cern.nxcals.api.custom.domain.CmwSystemConstants import PROPERTY_KEY_NAME
        from com.google.common.collect import ImmutableMap

        keyValues = ImmutableMap.of(DEVICE_KEY_NAME, "CPS.TGM", PROPERTY_KEY_NAME, "FULL-TELEGRAM.STRC")

        myEntity = entityService.findOne(Entities.suchThat().keyValues().eq(cmwSystemSpec.get(), keyValues))
        if myEntity.isEmpty():
            raise ValueError("Could not obtain entity from service")


        startTime = TimeUtils.getInstantFromString("2020-02-10 00:00:00.000000000")
        endTime = TimeUtils.getInstantFromString("2020-08-10 00:00:00.000000000")

        customInterpolateFunc = AggregationFunctions.INTERPOLATE.expandTimeWindowBy(1, 0, ChronoUnit.MONTHS)

        properties = WindowAggregationProperties.builder().timeWindow(startTime, endTime) \
            .interval(2, ChronoUnit.MONTHS).function(customInterpolateFunc).aggregationField("CYCLE").build()
        dataset = aggregationService.getData(myEntity.get(), properties)
        dataset.show()
        # @snippet:5:off

    @staticmethod
    def getAggregatedEntityDataInterpolatePy4j():
        spark, aggregationService = AggregationServiceSnippets.getServicePy4j()
        # @snippet:15:on
        ServiceClientFactory = spark._jvm.cern.nxcals.api.extraction.metadata.ServiceClientFactory
        Entities = spark._jvm.cern.nxcals.api.extraction.metadata.queries.Entities
        TimeUtils = spark._jvm.cern.nxcals.api.utils.TimeUtils
        ChronoUnit = spark._jvm.java.time.temporal.ChronoUnit

        AggregationFunctions = spark._jvm.cern.nxcals.api.custom.service.aggregation.AggregationFunctions
        WindowAggregationProperties = spark._jvm.cern.nxcals.api.custom.service.aggregation.WindowAggregationProperties
        
        DEVICE_KEY_NAME = spark._jvm.cern.nxcals.api.custom.domain.CmwSystemConstants.DEVICE_KEY_NAME
        PROPERTY_KEY_NAME = spark._jvm.cern.nxcals.api.custom.domain.CmwSystemConstants.PROPERTY_KEY_NAME
        ImmutableMap = spark._jvm.com.google.common.collect.ImmutableMap

        entityService = ServiceClientFactory.createEntityService()
        systemService = ServiceClientFactory.createSystemSpecService()

        cmwSystemSpec = systemService.findByName("CMW")

        if cmwSystemSpec.isEmpty():
            raise ValueError("No such system")

        keyValues = ImmutableMap.of(DEVICE_KEY_NAME, "CPS.TGM", PROPERTY_KEY_NAME, "FULL-TELEGRAM.STRC")

        myEntity = entityService.findOne(Entities.suchThat().keyValues().eq(cmwSystemSpec.get(), keyValues))
        if myEntity.isEmpty():
            raise ValueError("Could not obtain entity from service")


        startTime = TimeUtils.getInstantFromString("2020-02-10 00:00:00.000000000")
        endTime = TimeUtils.getInstantFromString("2020-08-10 00:00:00.000000000")

        customInterpolateFunc = AggregationFunctions.INTERPOLATE.expandTimeWindowBy(1, 0, ChronoUnit.MONTHS)

        properties = WindowAggregationProperties.builder().timeWindow(startTime, endTime) \
            .interval(2, ChronoUnit.MONTHS).function(customInterpolateFunc).aggregationField("CYCLE").build()
        dataset = aggregationService.getData(myEntity.get(), properties)
        dataset.show()
        # @snippet:15:off

    @staticmethod
    def getVariableDataAlignedToDataset():
        aggregationService = AggregationServiceSnippets.getService()
        # @snippet:6:on
        from cern.nxcals.api.extraction.metadata import ServiceClientFactory
        from cern.nxcals.api.extraction.metadata.queries import Variables
        from cern.nxcals.api.utils import TimeUtils
        from cern.nxcals.api.domain import TimeWindow

        from cern.nxcals.api.custom.service.aggregation import DatasetAggregationProperties
        from cern.nxcals.api.extraction.data.builders import DataQuery
        from org.apache.spark.sql import SparkSession

        variableService = ServiceClientFactory.createVariableService()

        sparkSession = SparkSession.active()

        drivingVariable = variableService.findOne(Variables.suchThat().variableName().eq("HX:FILLN"))
        if drivingVariable.isEmpty():
            raise ValueError("Could not obtain variable from service")


        startTime = TimeUtils.getInstantFromString("2016-03-01 00:00:00.000000000")
        endTime = TimeUtils.getInstantFromString("2016-05-01 00:00:00.000000000")

        drivingDataset = DataQuery.getFor(sparkSession, TimeWindow.between(startTime, endTime), drivingVariable.get())

        myVariable = variableService.findOne(Variables.suchThat().variableName().eq("CPS.TGM:CYCLE"))

        if myVariable.isEmpty():
            raise ValueError("Could not obtain variable from service")

        properties = DatasetAggregationProperties.builder().drivingDataset(drivingDataset).build()
        dataset = aggregationService.getData(myVariable.get(), properties)
        dataset.show()
        # @snippet:6:off

    @staticmethod
    def getVariableDataAlignedToDatasetPy4j():
        spark, aggregationService = AggregationServiceSnippets.getServicePy4j()
        # @snippet:16:on
        ServiceClientFactory = spark._jvm.cern.nxcals.api.extraction.metadata.ServiceClientFactory
        Variables = spark._jvm.cern.nxcals.api.extraction.metadata.queries.Variables
        TimeUtils = spark._jvm.cern.nxcals.api.utils.TimeUtils
        TimeWindow = spark._jvm.cern.nxcals.api.domain.TimeWindow

        DatasetAggregationProperties = spark._jvm.cern.nxcals.api.custom.service.aggregation.DatasetAggregationProperties
        DataQuery = spark._jvm.cern.nxcals.api.extraction.data.builders.DataQuery

        variableService = ServiceClientFactory.createVariableService()

        drivingVariable = variableService.findOne(Variables.suchThat().variableName().eq("HX:FILLN"))
        if drivingVariable.isEmpty():
            raise ValueError("Could not obtain variable from service")

        startTime = TimeUtils.getInstantFromString("2016-03-01 00:00:00.000000000")
        endTime = TimeUtils.getInstantFromString("2016-05-01 00:00:00.000000000")

        drivingDataset = DataQuery.getFor(spark._jsparkSession, TimeWindow.between(startTime, endTime), drivingVariable.get())

        myVariable = variableService.findOne(Variables.suchThat().variableName().eq("CPS.TGM:CYCLE"))

        if myVariable.isEmpty():
            raise ValueError("Could not obtain variable from service")

        properties = DatasetAggregationProperties.builder().drivingDataset(drivingDataset).build()
        dataset = aggregationService.getData(myVariable.get(), properties)
        dataset.show()
        # @snippet:16:off

    @staticmethod
    def getEntityDataAlignedToDataset():
        from cern.nxcals.api.extraction.metadata import ServiceClientFactory
        from cern.nxcals.api.extraction.metadata.queries import Variables
        from cern.nxcals.api.extraction.data.builders import DataQuery
        from cern.nxcals.api.utils import TimeUtils
        from cern.nxcals.api.domain import TimeWindow
        from org.apache.spark.sql import SparkSession

        entityService = ServiceClientFactory.createEntityService()
        variableService = ServiceClientFactory.createVariableService()
        systemService = ServiceClientFactory.createSystemSpecService()
        aggregationService = AggregationServiceSnippets.getService()

        sparkSession = SparkSession.active()

        cmwSystemSpec = systemService.findByName("CMW")

        if cmwSystemSpec.isEmpty():
            raise ValueError("No such system")

        drivingVariable = variableService.findOne(Variables.suchThat().variableName().eq("HX:FILLN"))
        if drivingVariable.isEmpty():
            raise ValueError("Could not obtain variable from service")


        startTime = TimeUtils.getInstantFromString("2016-03-01 00:00:00.000000000")
        endTime = TimeUtils.getInstantFromString("2016-05-01 00:00:00.000000000")

        drivingDataset = DataQuery.getFor(sparkSession, TimeWindow.between(startTime, endTime), drivingVariable.get())

        # @snippet:7:on
        from cern.nxcals.api.custom.service.aggregation import DatasetAggregationProperties
        from cern.nxcals.api.extraction.metadata.queries import Entities
        from cern.nxcals.api.custom.domain.CmwSystemConstants import DEVICE_KEY_NAME
        from cern.nxcals.api.custom.domain.CmwSystemConstants import PROPERTY_KEY_NAME
        from com.google.common.collect import ImmutableMap

        keyValues = ImmutableMap.of(DEVICE_KEY_NAME, "CPS.TGM", PROPERTY_KEY_NAME, "FULL-TELEGRAM.STRC")

        myEntity = entityService.findOne(Entities.suchThat().keyValues().eq(cmwSystemSpec.get(), keyValues))

        if myEntity.isEmpty():
            raise ValueError("Could not obtain entity from service")

        properties = DatasetAggregationProperties.builder().drivingDataset(drivingDataset).aggregationField("CYCLE").build()
        dataset = aggregationService.getData(myEntity.get(), properties)
        dataset.show()
        # @snippet:7:off

    @staticmethod
    def getEntityDataAlignedToDatasetPy4j():
        from cern.nxcals.api.extraction.data.builders import DataQuery

        spark, aggregationService = AggregationServiceSnippets.getServicePy4j()
        ServiceClientFactory = spark._jvm.cern.nxcals.api.extraction.metadata.ServiceClientFactory
        Variables = spark._jvm.cern.nxcals.api.extraction.metadata.queries.Variables
        TimeUtils = spark._jvm.cern.nxcals.api.utils.TimeUtils
        TimeWindow = spark._jvm.cern.nxcals.api.domain.TimeWindow
        DataQuery = spark._jvm.cern.nxcals.api.extraction.data.builders.DataQuery

        entityService = ServiceClientFactory.createEntityService()
        variableService = ServiceClientFactory.createVariableService()
        systemService = ServiceClientFactory.createSystemSpecService()

        cmwSystemSpec = systemService.findByName("CMW")

        if cmwSystemSpec.isEmpty():
            raise ValueError("No such system")

        drivingVariable = variableService.findOne(Variables.suchThat().variableName().eq("HX:FILLN"))
        if drivingVariable.isEmpty():
            raise ValueError("Could not obtain variable from service")


        startTime = TimeUtils.getInstantFromString("2016-03-01 00:00:00.000000000")
        endTime = TimeUtils.getInstantFromString("2016-05-01 00:00:00.000000000")

        drivingDataset = DataQuery.getFor(spark._jsparkSession, TimeWindow.between(startTime, endTime), drivingVariable.get())

        # @snippet:17:on
        DatasetAggregationProperties = spark._jvm.cern.nxcals.api.custom.service.aggregation.DatasetAggregationProperties
        Entities = spark._jvm.cern.nxcals.api.extraction.metadata.queries.Entities
        DEVICE_KEY_NAME = spark._jvm.cern.nxcals.api.custom.domain.CmwSystemConstants.DEVICE_KEY_NAME
        PROPERTY_KEY_NAME = spark._jvm.cern.nxcals.api.custom.domain.CmwSystemConstants.PROPERTY_KEY_NAME
        ImmutableMap = spark._jvm.com.google.common.collect.ImmutableMap

        keyValues = ImmutableMap.of(DEVICE_KEY_NAME, "CPS.TGM", PROPERTY_KEY_NAME, "FULL-TELEGRAM.STRC")

        myEntity = entityService.findOne(Entities.suchThat().keyValues().eq(cmwSystemSpec.get(), keyValues))

        if myEntity.isEmpty():
            raise ValueError("Could not obtain entity from service")

        properties = DatasetAggregationProperties.builder().drivingDataset(drivingDataset).aggregationField("CYCLE").build()
        dataset = aggregationService.getData(myEntity.get(), properties)
        dataset.show()
        # @snippet:17:off

    @staticmethod
    def test_py4j():
        AggregationServiceSnippets.getAggregatedEntityDataInterpolatePy4j()
        AggregationServiceSnippets.getAggregatedEntityDataMaxPy4j()
        AggregationServiceSnippets.getAggregatedVariableDataAvgPy4j()
        AggregationServiceSnippets.getEntityDataAlignedToDatasetPy4j()
        AggregationServiceSnippets.getAggregatedVariableDataRepeatPy4j()
        AggregationServiceSnippets.getServicePy4j()
        AggregationServiceSnippets.getVariableDataAlignedToDatasetPy4j()