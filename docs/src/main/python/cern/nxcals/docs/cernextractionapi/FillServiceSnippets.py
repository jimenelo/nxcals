class FillServiceSnippets:

    def __init__(self):
        pass

    @staticmethod
    def getFillService():
        # @snippet:1:on
        from cern.nxcals.api.custom.service import Services
        from org.apache.spark import SparkConf
        from org.apache.spark.sql import SparkSession

        sparkConf = SparkConf().setMaster("local[*]").setAppName("MY_APP")
        session = SparkSession.builder().config(sparkConf).getOrCreate()

        fillService = Services.newInstance(session).fillService()
        # @snippet:1:off

        return fillService

    @staticmethod
    def getFillServicePy4j():
        # @snippet:11:on
        from nxcals.spark_session_builder import get_or_create # on swan create spark session from UI (star icon)
        spark = get_or_create()

        Services = spark._jvm.cern.nxcals.api.custom.service.Services

        fillService = Services.newInstance(spark._jsparkSession).fillService()
        # @snippet:11:off

        return spark, fillService

    @staticmethod
    def getFillWithNumber():
        fillService = FillServiceSnippets.getFillService()
        # @snippet:2:on
        fill = fillService.findFill(3000)

        if fill.isEmpty():
            raise ValueError("No such a fill")

        Utilities.printFillInfo(fill.get())
        # @snippet:2:off

    @staticmethod
    def getFillWithNumberPy4j():
        spark, fillService = FillServiceSnippets.getFillServicePy4j()
        # @snippet:12:on
        fill = fillService.findFill(3000)

        if fill.isEmpty():
            raise ValueError("No such a fill")

        Utilities.printFillInfo(fill.get())
        # @snippet:12:off

    @staticmethod
    def getFillsInTimeWindow():

        fillService = FillServiceSnippets.getFillService()
        # @snippet:3:on
        from cern.nxcals.api.utils import TimeUtils

        fills = fillService.findFills(TimeUtils.getInstantFromString("2018-04-25 00:00:00.000000000"),
            TimeUtils.getInstantFromString("2018-04-28 00:00:00.000000000"))

        for fill in fills:
            Utilities.printFillInfo(fill)
        # @snippet:3:off

    @staticmethod
    def getFillsInTimeWindowPy4j():
        spark, fillService = FillServiceSnippets.getFillServicePy4j()
        # @snippet:13:on
        TimeUtils = spark._jvm.cern.nxcals.api.utils.TimeUtils

        fills = fillService.findFills(TimeUtils.getInstantFromString("2018-04-25 00:00:00.000000000"),
            TimeUtils.getInstantFromString("2018-04-28 00:00:00.000000000"))

        for fill in fills:
            Utilities.printFillInfo(fill)
        # @snippet:13:off

    @staticmethod
    def test_py4j():
        FillServiceSnippets.getFillServicePy4j()
        FillServiceSnippets.getFillsInTimeWindowPy4j()
        FillServiceSnippets.getFillWithNumberPy4j()


# @snippet:4:on
class Utilities:
    @staticmethod
    def printFillInfo(fill):
        outputMsg = "Fill: " + str(fill.getNumber())
        outputMsg += ", Validity: " + Utilities.__validityToString(fill.getValidity()) + "\n"

        beamModes = fill.getBeamModes()
        for beamMode in beamModes:
            outputMsg += Utilities.__addBeamModeInfo(beamMode)

        print(outputMsg)

    @staticmethod
    def __addBeamModeInfo(beamMode):
        return "\tBeam mode name: " + beamMode.getBeamModeValue() + ", Validity: " + Utilities.__validityToString(beamMode.getValidity()) + "\n"

    @staticmethod
    def __validityToString(validity):
        return validity.getStartTime().toString() + " " + validity.getEndTime().toString()
# @snippet:4:off
