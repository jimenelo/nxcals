package cern.nxcals.docs.metadataapi;

import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.domain.VariableConfig;
import cern.nxcals.api.extraction.metadata.EntityService;
import cern.nxcals.api.extraction.metadata.ServiceClientFactory;
import cern.nxcals.api.extraction.metadata.VariableService;
import cern.nxcals.api.extraction.metadata.queries.Entities;
import cern.nxcals.api.extraction.metadata.queries.Variables;
import com.google.common.collect.ImmutableSortedSet;

import java.util.Optional;
import java.util.Set;

public class VariableServiceSnippets {
    public void variableSearchPatternOnName() {
        // @snippet:1:on
        VariableService variableService = ServiceClientFactory.createVariableService();

        Set<Variable> variablesDataSet = variableService.findAll(
                Variables.suchThat().systemName().eq("CMW").and().variableName().like("SPS%"));
        // @snippet:1:off
    }

    public void variableSearchPatternOnDescription() {
        // @snippet:2:on
        VariableService variableService = ServiceClientFactory.createVariableService();

        Set<Variable> variablesDataSet = variableService
                .findAll(Variables.suchThat().systemName().eq("CMW").and().description().like("%Current Beam Energy%"));
        // @snippet:2:off
    }

    public void variableRegisterOrUpdate() {
        // @snippet:3:on
        final String SYSTEM = "CMW";
        final String DEMO_VARIABLE = "DEMO:VARIABLE";

        VariableService variableService = ServiceClientFactory.createVariableService();

        Optional<Variable> variable = variableService.findOne(Variables.suchThat()
                .systemName().eq(SYSTEM).and().variableName().eq(DEMO_VARIABLE));

        if (variable.isPresent()) {
            Variable updatedVariable = variable.get().toBuilder().description("updated description").build();
            variableService.update(updatedVariable);
        }
        else {
            EntityService entityService = ServiceClientFactory.createEntityService();

            Entity entity = entityService.findOne(
                    Entities.suchThat().systemName().eq(SYSTEM).and().keyValues().like("%myUniqueDeviceProperty%"))
                    .orElseThrow(() -> new IllegalArgumentException("No such entity"));

            VariableConfig config = VariableConfig.builder().entityId(entity.getId()).fieldName("myFieldName")
                    .validity(TimeWindow.between(null, null)).build();

            Variable variableData = Variable.builder().variableName(DEMO_VARIABLE)
                    .configs(ImmutableSortedSet.of(config))
                    .systemSpec(entity.getSystemSpec())
                    .build();

            variableService.create(variableData);
        }
        // @snippet:3:off
    }
}
