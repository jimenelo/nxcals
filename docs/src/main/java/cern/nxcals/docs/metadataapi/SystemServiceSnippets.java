package cern.nxcals.docs.metadataapi;

import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.extraction.metadata.ServiceClientFactory;
import cern.nxcals.api.extraction.metadata.SystemSpecService;

public class SystemServiceSnippets {
    public void systemSearch() {
        // @snippet:1:on
        SystemSpecService systemService = ServiceClientFactory.createSystemSpecService();

        SystemSpec systemData = systemService.findByName("CMW")
                .orElseThrow(() -> new IllegalArgumentException("No such system"));
        // @snippet:1:off
    }
}
