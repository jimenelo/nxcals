package cern.nxcals.docs.metadataapi;

import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.Hierarchy;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.extraction.metadata.EntityService;
import cern.nxcals.api.extraction.metadata.HierarchyService;
import cern.nxcals.api.extraction.metadata.ServiceClientFactory;
import cern.nxcals.api.extraction.metadata.VariableService;
import cern.nxcals.api.extraction.metadata.queries.Entities;
import cern.nxcals.api.extraction.metadata.queries.Hierarchies;
import cern.nxcals.api.extraction.metadata.queries.Variables;
import com.google.common.collect.Iterables;

import java.util.Collections;
import java.util.Set;

public class HierarchyServiceSnippets {
    public void hierarchySearch() {
        // @snippet:1:on
        HierarchyService hierarchyService = ServiceClientFactory.createHierarchyService();

        Set<Hierarchy> hierarchies = hierarchyService.findAll(Hierarchies.suchThat().path().like("%example%"));
        // @snippet:1:off
    }

    public void hierarchyUpdateVariables() {
        // @snippet:2:on
        HierarchyService hierarchyService = ServiceClientFactory.createHierarchyService();
        VariableService variableService = ServiceClientFactory.createVariableService();
        String systemName = "CMW";

        Hierarchy hierarchy = hierarchyService.findOne(Hierarchies.suchThat().systemName().eq(systemName).and().path().eq("/example"))
                .orElseThrow(() -> new IllegalArgumentException("No such hierarchy"));

        // Prepare variable to be attached to the hierarchy node
        Variable variable = variableService
                .findOne(Variables.suchThat().systemName().eq(systemName).and().variableName().eq("DEMO:VARIABLE"))
                .orElseThrow(() -> new IllegalArgumentException("No such variable"));

        // The variable will be added to the set of already attached variables
        hierarchyService.addVariables(hierarchy.getId(), Collections.singleton(variable.getId()));

        Set<Variable> variables = hierarchyService.getVariables(hierarchy.getId());

        // @snippet:2:off
    }

    public void hierarchyUpdateEntities() {
        // @snippet:3:on
        HierarchyService hierarchyService = ServiceClientFactory.createHierarchyService();
        EntityService entityService = ServiceClientFactory.createEntityService();
        String hierarchySystemName = "CERN";
        String systemName = "CMW";

        Hierarchy hierarchy = hierarchyService.findOne(Hierarchies.suchThat().systemName().eq(hierarchySystemName).and().path().eq("/example"))
                .orElseThrow(() -> new IllegalArgumentException("No such hierarchy"));

        // Obtain an entity of your interest
        Entity entity = Iterables.getLast(entityService
                .findAll(Entities.suchThat().systemName().eq(systemName)));

        // The entity will replace a set of already attached entities
        hierarchyService.setEntities(hierarchy.getId(), Collections.singleton(entity.getId()));

        Set<Entity> entities = hierarchyService.getEntities(hierarchy.getId());

        // @snippet:3:off
    }

    public void hierarchyNodeSave() {
        // @snippet:4:on
        HierarchyService hierarchyService = ServiceClientFactory.createHierarchyService();
        String systemName = "CMW";

        SystemSpec systemSpec = ServiceClientFactory.createSystemSpecService().findByName(systemName)
                .orElseThrow(() -> new IllegalArgumentException("No such system specification"));

        Hierarchy parent = hierarchyService.findOne(Hierarchies.suchThat().systemName().eq(systemName).and().path().eq("/example"))
                .orElseThrow(() -> new IllegalArgumentException("No such hierarchy"));


        Hierarchy newHierarchy = Hierarchy.builder().name("new node").description("New node description")
                .systemSpec(systemSpec).parent(parent).build();

        Hierarchy modified = hierarchyService.create(newHierarchy);

        modified.toBuilder().description("Modified node description").build();

        hierarchyService.update(modified);
        // @snippet:4:off
        // @snippet:5:on
        hierarchyService.deleteLeaf(hierarchyService.findOne(Hierarchies.suchThat()
                .systemName().eq(systemName).and().path().eq("/example/new node"))
                .orElseThrow(() -> new IllegalArgumentException("No such hierarchy")));
        // @snippet:5:off
    }
}
