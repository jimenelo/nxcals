package cern.nxcals.docs.metadataapi;

import avro.shaded.com.google.common.collect.ImmutableMap;
import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.extraction.metadata.EntityService;
import cern.nxcals.api.extraction.metadata.ServiceClientFactory;
import cern.nxcals.api.extraction.metadata.SystemSpecService;
import cern.nxcals.api.extraction.metadata.queries.Entities;
import cern.nxcals.api.extraction.metadata.queries.Groups;
import cern.nxcals.api.extraction.metadata.queries.SystemSpecs;
import cern.nxcals.api.utils.TimeUtils;
import com.google.common.collect.Sets;

import java.util.Map;
import java.util.Set;

public class EntityServiceSnippets {

    public void entitySearch() {
        // @snippet:1:on
        EntityService entityService = ServiceClientFactory.createEntityService();
        SystemSpecService systemService = ServiceClientFactory.createSystemSpecService();

        SystemSpec systemData = systemService.findOne(SystemSpecs.suchThat().name().eq("CMW"))
                .orElseThrow(() -> new IllegalArgumentException("System not found"));

        Map<String, Object> keyValues = ImmutableMap.of("device", "LHC.LUMISERVER", "property", "CrossingAngleIP1");
        Entity entityData = entityService.findOne(
                Entities.suchThat().systemId().eq(systemData.getId()).and().keyValues().eq(systemData, keyValues))
                .orElseThrow(() -> new IllegalArgumentException("Entity not found"));
        // @snippet:1:off
    }

    public void entityUpdate() {
        // @snippet:2:on
        SystemSpecService systemService = ServiceClientFactory.createSystemSpecService();
        EntityService entityService = ServiceClientFactory.createEntityService();

        SystemSpec systemData = systemService.findByName("CMW")
                .orElseThrow(() -> new IllegalArgumentException("No such system CMW"));

        Map<String, Object> keyValues = ImmutableMap.of("device", "MY.DEVICE", "property", "MyProperty");
        Entity entityData = entityService.findOne(Entities.suchThat()
                .systemId().eq(systemData.getId())
                .and()
                .keyValues().eq(systemData, keyValues))
                .orElseThrow(() -> new IllegalArgumentException("Entity not found"));

        // Change entity to set new keyValues
        Map<String, Object> newKeyValues = ImmutableMap
                .of("device", "Example.Device", "property", "ExampleNewProperty");
        Entity newEntityData = entityData.toBuilder().entityKeyValues(newKeyValues).build();

        // Perform the update
        Set<Entity> updatedEntities = entityService.updateEntities(Sets.newHashSet(newEntityData));
        // @snippet:2:off
    }

    public void entitiesSearchWithHistory() {
        // @snippet:7:on
        SystemSpec systemData = ServiceClientFactory.createSystemSpecService()
                .findOne(SystemSpecs.suchThat().name().eq("CMW"))
                .orElseThrow(() -> new IllegalArgumentException("System not found"));

        Map<String, Object> keyValues = ImmutableMap.of("device", "LHC.LUMISERVER", "property", "CrossingAngleIP1");

        EntityService entityService = ServiceClientFactory.createEntityService();
        Entity entities = entityService.findOneWithHistory(
                Entities.suchThat().systemName().eq(systemData.getName()).and().keyValues().eq(systemData, keyValues),
                TimeUtils.getNanosFromString("2017-10-10 14:15:00.000000000"),
                TimeUtils.getNanosFromString("2020-10-26 14:15:00.000000000")
        ).orElseThrow(() -> new IllegalArgumentException("Entity not found"));

        entities.getEntityHistory().forEach(System.out::println);
        // @snippet:7:off
    }

    public void snippets() {
        // @snippet:3:on
        Entities.suchThat().systemId().eq(1L).and().keyValues().like("%myDevice%");
        // @snippet:3:off

        // @snippet:4:on
        Groups.suchThat().name().eq("myName");
        // @snippet:4:off

        // @snippet:5:on
        Groups.suchThat().name().caseInsensitive().eq("myName");
        // @snippet:5:off

        // @snippet:6:on
        Entities.suchThat().keyValues().caseInsensitive().like("%myDevice%myProperty%");
        // @snippet:6:off
    }
}
