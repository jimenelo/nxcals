package cern.nxcals.docs.cernextractionapi;

import cern.nxcals.api.custom.domain.BeamMode;
import cern.nxcals.api.custom.domain.Fill;
import cern.nxcals.api.custom.service.FillService;
import cern.nxcals.api.custom.service.Services;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.utils.TimeUtils;
import lombok.experimental.UtilityClass;
import org.apache.spark.SparkConf;
import org.apache.spark.sql.SparkSession;

import java.util.List;
import java.util.StringJoiner;

public class FillServiceSnippets {

    private static FillService getFillService() {
        // @snippet:1:on
        SparkConf sparkConf = new SparkConf().setMaster("local[*]").setAppName("MY_APP");
        SparkSession session = SparkSession.builder().config(sparkConf).getOrCreate();

        FillService fillService = Services.newInstance(session).fillService();
        // @snippet:1:off
        return fillService;
    }

    public static void getFillWithNumber() {
        FillService fillService = getFillService();
        // @snippet:2:on
        Fill fill = fillService.findFill(3000)
                .orElseThrow(() -> new IllegalArgumentException("No such a fill"));

        Utilities.printFillInfo(fill);
        // @snippet:2:off
    }


    public static void getFillsInTimeWindow() {
        FillService fillService = getFillService();
        // @snippet:3:on
        List<Fill> fills = fillService.findFills(
                TimeUtils.getInstantFromString("2018-04-25 00:00:00.000000000"),
                TimeUtils.getInstantFromString("2018-04-28 00:00:00.000000000")
        );

        fills.forEach(Utilities::printFillInfo);
        // @snippet:3:off
    }


    @UtilityClass
    // @snippet:4:on
    class Utilities {

        private static void printFillInfo(Fill fill) {
            StringBuilder outputMsg = new StringBuilder();
            StringJoiner joiner = new StringJoiner("\n");

            outputMsg.append("Fill: ").append(fill.getNumber());
            outputMsg.append(", Validity: ").append(validityToString(fill.getValidity())).append("\n");

            List<BeamMode> beamModes = fill.getBeamModes();
            beamModes.forEach(b -> outputMsg.append(addBeamModeInfo(b)));

            System.out.println(outputMsg);
        }

        private static String addBeamModeInfo(BeamMode beamMode) {
            return "\tBeam mode name: " + beamMode.getBeamModeValue() + ", Validity: " + validityToString(beamMode.getValidity()) + "\n";
        }

        private static String validityToString(TimeWindow validity) {
            return validity.getStartTime() + " " + validity.getEndTime();
        }
    }
    // @snippet:4:off
}
