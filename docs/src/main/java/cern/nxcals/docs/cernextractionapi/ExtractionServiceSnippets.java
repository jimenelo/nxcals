package cern.nxcals.docs.cernextractionapi;

import cern.nxcals.api.config.SparkProperties;
import cern.nxcals.api.custom.service.ExtractionService;
import cern.nxcals.api.custom.service.Services;
import cern.nxcals.api.custom.service.extraction.ExtractionProperties;
import cern.nxcals.api.custom.service.extraction.LookupStrategy;
import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.extraction.metadata.EntityService;
import cern.nxcals.api.extraction.metadata.ServiceClientFactory;
import cern.nxcals.api.extraction.metadata.SystemSpecService;
import cern.nxcals.api.extraction.metadata.VariableService;
import cern.nxcals.api.extraction.metadata.queries.Entities;
import cern.nxcals.api.extraction.metadata.queries.Variables;
import cern.nxcals.api.utils.TimeUtils;
import com.google.common.collect.ImmutableMap;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Map;

import static cern.nxcals.api.custom.domain.CmwSystemConstants.DEVICE_KEY_NAME;
import static cern.nxcals.api.custom.domain.CmwSystemConstants.PROPERTY_KEY_NAME;

public class ExtractionServiceSnippets {
    private static final VariableService variableService = ServiceClientFactory.createVariableService();
    private static final EntityService entityService = ServiceClientFactory.createEntityService();
    private static final SystemSpecService systemService = ServiceClientFactory.createSystemSpecService();

    private static final ExtractionService extractionService = getService();

    public static ExtractionService getService() {
        // @snippet:1:on
        ExtractionService extractionService = Services.newInstance(SparkProperties.defaults("MY_APP"))
                .extractionService();
        // @snippet:1:off
        return extractionService;
    }

    public static void getVariableDataWhenDataAvailableNoLookup() {
        // @snippet:2:on
        Variable myVariable = variableService.findOne(Variables.suchThat()
                .variableName().eq("CPS.TGM:CYCLE"))
                .orElseThrow(() -> new IllegalArgumentException("Could not obtain variable from service"));
        Instant startTime = TimeUtils.getInstantFromString("2020-04-25 00:00:00.000000000");
        Instant endTime = TimeUtils.getInstantFromString("2020-04-26 00:00:00.000000000");

        ExtractionProperties properties = ExtractionProperties.builder()
                .timeWindow(startTime, endTime)
                .lookupStrategy(LookupStrategy.LAST_BEFORE_START_IF_EMPTY).build();
        Dataset<Row> dataset = extractionService.getData(myVariable, properties);

        System.out.println("Dataset count: " + dataset.count());
        dataset.show();
        // @snippet:2:off
    }

    public static void getVariableDataAlwaysDoLookup() {
        // @snippet:3:on
        Variable myVariable = variableService.findOne(Variables.suchThat()
                .variableName().eq("CPS.TGM:CYCLE"))
                .orElseThrow(() -> new IllegalArgumentException("Could not obtain variable from service"));
        Instant startTime = TimeUtils.getInstantFromString("2020-04-25 00:00:00.000000000");
        Instant endTime = TimeUtils.getInstantFromString("2020-04-26 00:00:00.000000000");

        ExtractionProperties properties = ExtractionProperties.builder()
                .timeWindow(startTime, endTime)
                .lookupStrategy(LookupStrategy.LAST_BEFORE_START).build();
        Dataset<Row> dataset = extractionService.getData(myVariable, properties);

        System.out.println("Dataset count: " + dataset.count());
        dataset.show();
        // @snippet:3:off
    }

    public static void getEntityDataWhenDataAvailableNoLookup() {
        SystemSpec cmwSystemSpec = systemService.findByName("CMW")
                .orElseThrow(() -> new IllegalArgumentException("No such system"));
        // @snippet:4:on
        Map<String, Object> keyValues = ImmutableMap
                .of(DEVICE_KEY_NAME, "CPS.TGM", PROPERTY_KEY_NAME, "FULL-TELEGRAM.STRC");
        Entity myEntity = entityService.findOne(Entities.suchThat()
                .keyValues().eq(cmwSystemSpec, keyValues))
                .orElseThrow(() -> new IllegalArgumentException("Could not obtain entity from service"));
        Instant startTime = TimeUtils.getInstantFromString("2020-04-10 00:00:00.000000000");
        Instant endTime = TimeUtils.getInstantFromString("2020-04-11 00:00:10.000000000");

        ExtractionProperties properties = ExtractionProperties.builder()
                .timeWindow(startTime, endTime)
                .lookupStrategy(LookupStrategy.LAST_BEFORE_START_IF_EMPTY).build();
        Dataset<Row> dataset = extractionService.getData(myEntity, properties);

        System.out.println("Dataset count: " + dataset.count());
        dataset.show(2);
        // @snippet:4:off
    }

    public static void getEntityDataAlwaysDoLookup() {
        SystemSpec cmwSystemSpec = systemService.findByName("CMW")
                .orElseThrow(() -> new IllegalArgumentException("No such system"));
        // @snippet:5:on
        Map<String, Object> keyValues = ImmutableMap
                .of(DEVICE_KEY_NAME, "CPS.TGM", PROPERTY_KEY_NAME, "FULL-TELEGRAM.STRC");
        Entity myEntity = entityService.findOne(Entities.suchThat()
                .keyValues().eq(cmwSystemSpec, keyValues))
                .orElseThrow(() -> new IllegalArgumentException("Could not obtain entity from service"));
        Instant startTime = TimeUtils.getInstantFromString("2020-04-10 00:00:00.000000000");
        Instant endTime = TimeUtils.getInstantFromString("2020-04-11 00:00:10.000000000");

        ExtractionProperties properties = ExtractionProperties.builder()
                .timeWindow(startTime, endTime)
                .lookupStrategy(LookupStrategy.LAST_BEFORE_START).build();
        Dataset<Row> dataset = extractionService.getData(myEntity, properties);

        System.out.println("Dataset count: " + dataset.count());
        dataset.show(2);
        // @snippet:5:off
    }

    public static void getEntityDataWithCustomLookup() {
        SystemSpec cmwSystemSpec = systemService.findByName("CMW")
                .orElseThrow(() -> new IllegalArgumentException("No such system"));
        // @snippet:6:on
        Map<String, Object> keyValues = ImmutableMap
                .of(DEVICE_KEY_NAME, "CPS.TGM", PROPERTY_KEY_NAME, "FULL-TELEGRAM.STRC");
        Entity myEntity = entityService.findOne(Entities.suchThat()
                .keyValues().eq(cmwSystemSpec, keyValues))
                .orElseThrow(() -> new IllegalArgumentException("Could not obtain entity from service"));
        Instant startTime = TimeUtils.getInstantFromString("2020-04-10 00:00:00.000000000");
        Instant endTime = TimeUtils.getInstantFromString("2020-04-11 00:00:10.000000000");

        LookupStrategy customLookupStrategy = LookupStrategy.LAST_BEFORE_START
                .withLookupDuration(8, ChronoUnit.HOURS);
        ExtractionProperties properties = ExtractionProperties.builder()
                .timeWindow(startTime, endTime)
                .lookupStrategy(customLookupStrategy).build();
        Dataset<Row> dataset = extractionService.getData(myEntity, properties);

        System.out.println("Dataset count: " + dataset.count());
        dataset.show(2);
        // @snippet:6:off
    }

}
