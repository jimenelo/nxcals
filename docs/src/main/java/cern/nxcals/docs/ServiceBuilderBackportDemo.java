package cern.nxcals.docs;

import cern.nxcals.api.backport.client.service.ServiceBuilder;
import cern.nxcals.api.backport.client.service.TimeseriesDataService;
import cern.nxcals.api.backport.domain.core.metadata.Variable;
import cern.nxcals.api.backport.domain.core.timeseriesdata.TimeseriesDataSet;
import cern.nxcals.api.backport.domain.util.TimestampFactory;
import cern.nxcals.api.config.SparkProperties;
import cern.nxcals.api.custom.config.SparkSessionFlavor;
import cern.nxcals.api.utils.SparkUtils;
import com.google.common.collect.ImmutableList;
import org.apache.spark.SparkConf;
import org.apache.spark.sql.SparkSession;

import java.time.Duration;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

public class ServiceBuilderBackportDemo {

    public void runDemos() {
        getDataInTimeWindow(getDefaultServiceBuilder());
        getDataInTimeWindow(getDefaultServiceBuilderExplicitly());
        getDataInTimeWindow(getYarnServiceBuilderFromProperties());
        getDataInTimeWindow(getServiceBuilderFromSession());
        getDataInTimeWindow(getServiceBuilderUsingSparkUtility());
        getDataInTimeWindow(getYarnServiceBuilderWithCustomizedProperties());
    }

    private static void getDataInTimeWindow(ServiceBuilder serviceBuilder) {

        TimeseriesDataService timeseriesDataService = serviceBuilder.createTimeseriesService();

        Variable variable = getVariable("PR.DCAFTINJ_1:INTENSITY");

        Instant start = Instant.now();
        TimeseriesDataSet data = timeseriesDataService.getDataInTimeWindow(variable,
                TimestampFactory.parseUTCTimestamp("2018-06-19 00:00:00.000"),
                TimestampFactory.parseUTCTimestamp("2018-07-19 00:05:00.000"));
        Instant end = Instant.now();

        System.out.println("Values for variable : " + variable.getVariableName() + " size: " + data.size());
        System.out.println(Duration.between(start, end));
    }

    private static ServiceBuilder getDefaultServiceBuilder() {
        // @snippet:1:on
        ServiceBuilder defaultServiceBuilder = ServiceBuilder.getInstance();
        return defaultServiceBuilder;
        // @snippet:1:off
    }

    private static ServiceBuilder getDefaultServiceBuilderExplicitly() {
        // @snippet:2:on
        SparkProperties sparkProperties = SparkProperties.defaults("MY_APP");
        ServiceBuilder defaultServiceBuilder = ServiceBuilder.getInstance(sparkProperties);
        // @snippet:2:off
        return defaultServiceBuilder;
    }

    private static ServiceBuilder getYarnServiceBuilderFromProperties() {
        // @snippet:3:on
        SparkProperties sparkProperties = new SparkProperties();
        sparkProperties.setAppName("MY_APP");
        sparkProperties.setMasterType("yarn");

        Map<String, String> properties = new HashMap<>();
        properties.put("spark.executor.memory", "8G");
        properties.put("spark.executor.cores", "10");

        // yarn
        properties.put("spark.yarn.appMasterEnv.JAVA_HOME", "/var/nxcals/jdk1.11");
        properties.put("spark.executorEnv.JAVA_HOME", "/var/nxcals/jdk1.11");
        properties.put("spark.yarn.jars", "hdfs:////project/nxcals/lib/spark-3.3.1/*.jar,hdfs:////project/nxcals/nxcals_lib/nxcals_pro/*.jar");
        properties.put("spark.yarn.am.extraLibraryPath", "/usr/lib/hadoop/lib/native");
        properties.put("spark.executor.extraLibraryPath", "/usr/lib/hadoop/lib/native");
        properties.put("spark.yarn.historyServer.address", "ithdp1001.cern.ch:18080");
        properties.put("spark.yarn.access.hadoopFileSystems", "nxcals");
        properties.put("spark.sql.caseSensitive", "true");

        sparkProperties.setProperties(properties);

        ServiceBuilder yarnServiceBuilder = ServiceBuilder.getInstance(sparkProperties);
        // @snippet:3:off
        return yarnServiceBuilder;
    }

    private static ServiceBuilder getServiceBuilderFromSession() {
        // @snippet:4:on
        SparkConf sparkConf = new SparkConf().setMaster("local[4]").setAppName("My_APP");
        org.apache.spark.SparkContext sc = new org.apache.spark.SparkContext(sparkConf);
        SparkSession session = new SparkSession(sc);

        ServiceBuilder serviceBuilderFromSession = ServiceBuilder.getInstance(session);

        // @snippet:4:off
        return serviceBuilderFromSession;
    }

    private static ServiceBuilder getServiceBuilderUsingSparkUtility() {
        // @snippet:5:on
        SparkConf sparkConf = SparkUtils.createSparkConf(SparkProperties.defaults("MY_APP"));
        ServiceBuilder serviceBuilderFromSparkUtility =
                ServiceBuilder.getInstance(SparkUtils.createSparkSession(sparkConf));

        // @snippet:5:off
        return serviceBuilderFromSparkUtility;
    }

    private static ServiceBuilder getYarnServiceBuilderFromPredefinedProperties() {
        // @snippet:6:on
        SparkSessionFlavor initialProperties = SparkSessionFlavor.MEDIUM;

        // Please replace "pro" with "testbed" when accessing NXCALS TESTBED
        ServiceBuilder yarnServiceBuilderFromPredefinedProperties
                = ServiceBuilder.getInstance(SparkProperties.remoteSessionProperties("MY_APP", initialProperties, "pro"));
        // @snippet:6:off
        return yarnServiceBuilderFromPredefinedProperties;
    }

    private static ServiceBuilder getYarnServiceBuilderWithCustomizedProperties() {
        // @snippet:7:on
        SparkSessionFlavor initialProperties = SparkSessionFlavor.LARGE;
        SparkProperties sparkProperties = SparkProperties.remoteSessionProperties("MY_APP", initialProperties, "pro");

        Map<String, String> props = new HashMap<>();

        // Customize retrieved properties, for example:
        props.put("spark.executor.memory", "10G");
        props.put("spark.executor.cores", "10");

        sparkProperties.setProperties(props);

        SparkSession sparkSession = SparkUtils.createSparkSession(sparkProperties);
        ServiceBuilder serviceBuilder = ServiceBuilder.getInstance(sparkSession);
        // @snippet:7:off
        return serviceBuilder;
    }

    private static Variable getVariable(String variableName) {
        return ServiceBuilder.getInstance().createMetaService()
                .getVariablesWithNameInListofStrings(ImmutableList.of(variableName)).iterator().next();
    }
}
