package cern.nxcals.docs.sparksession;

import cern.nxcals.api.config.SparkProperties;
import cern.nxcals.api.custom.config.SparkSessionFlavor;
import cern.nxcals.api.utils.SparkUtils;
import org.apache.spark.SparkConf;
import org.apache.spark.sql.SparkSession;

public class SparkSessionDemo {
    public static void simpleSessionSpark() {
        // @snippet:1:on
        SparkSession sparkSession = SparkSession.builder().getOrCreate();
        // @snippet:1:off
    }

    public static void simpleSessionSparkWithProps() {
        // @snippet:2:on
        SparkConf sparkConf =  new SparkConf()
                .setAppName("MY_APP")
                .setMaster("yarn")
                .set("spark.submit.deployMode", "client")
                .set("spark.yarn.appMasterEnv.JAVA_HOME",  "/var/nxcals/jdk1.11")
                .set("spark.executorEnv.JAVA_HOME", "/var/nxcals/jdk1.11")
                .set("spark.yarn.jars", "hdfs:////project/nxcals/lib/spark-3.3.1/*.jar,hdfs:////project/nxcals/nxcals_lib/nxcals_pro/*.jar\"")
                .set("spark.executor.instances", "4")
                .set("spark.executor.cores", "1")
                .set("spark.executor.memory", "1g")
                .set("sql.caseSensitive", "true")
                .set("spark.kerberos.access.hadoopFileSystems", "nxcals");

        SparkSession sparkSession = SparkSession.builder().config(sparkConf).getOrCreate();
       // @snippet:2:off
    }

    public static void simpleSessionSparkWithNXCALSLocal() {
        // @snippet:3:on
        //Defaults to local[*] mode
        SparkProperties sparkProperties = SparkProperties.defaults("MY_APP");
        SparkSession sparkSession = SparkUtils.createSparkSession(sparkProperties);
        // @snippet:3:off
    }


    public static void simpleSessionSparkWithNXCALSYarnFlavor() {
        // @snippet:4:on
        //Using SparkSessionFlavor.SMALL, all properties pre-set, running on Yarn
        SparkProperties sparkProperties = SparkProperties.remoteSessionProperties("MY_APP", SparkSessionFlavor.SMALL, "pro");
        //Can overwrite (or set) any property
        sparkProperties.setProperty("spark.executor.instances", "6");
        SparkSession sparkSession = SparkUtils.createSparkSession(sparkProperties);
        // @snippet:4:off
    }

}
