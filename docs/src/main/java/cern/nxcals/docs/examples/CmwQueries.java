package cern.nxcals.docs.examples;

// @snippet:1:on
// source the nxcals query builders
import cern.nxcals.api.extraction.data.builders.DevicePropertyDataQuery;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.functions;

// @snippet:1:off

public class CmwQueries {
    private SparkSession spark;

    public CmwQueries(SparkSession spark) {
        this.spark = spark;
    }

    public void queries() {

        // @snippet:2:on
        // build the query (note: instead of parameter you can specify device/property separetly:
        // .device("PR.BCT").property("HotspotIntensity")
        Dataset<Row> intensity = DevicePropertyDataQuery.builder(spark).system("CMW")
                .startTime("2018-06-17 00:00:00.000").endTime("2018-06-20 00:00:00.000")
                .entity().parameter("PR.BCT/HotspotIntensity").build();

        // print the schema
        intensity.printSchema();
        // @snippet:2:off

        // @snippet:3:on
        // count the presence of recoreds that contain 'acqStamp' and 'dcBefInj1' fields
        intensity.select("acqStamp", "dcBefInj1").count();
        // @snippet:3:off

        // @snippet:4:on
        Dataset<Row> tgmData = DevicePropertyDataQuery.builder(spark).system("CMW")
                .startTime("2018-06-17 00:00:00.000").endTime("2018-06-20 00:00:00.000")
                .entity().device("CPS.TGM").property("FULL-TELEGRAM.STRC").build();
        // @snippet:4:off

        // @snippet:5:on
        tgmData.groupBy("USER").count().show();
        // @snippet:5:off

        // @snippet:6:on
        Dataset<Row> userTOF = tgmData.select("cyclestamp").where("USER == 'TOF'");
        userTOF.show();
        // @snippet:6:off

        // @snippet:7:on
        Dataset<Row> result = intensity.join(userTOF, "cyclestamp")
                .select(intensity.col("cyclestamp"), intensity.col("dcAftInj1"));
        // @snippet:7:off

        // @snippet:8:on
        result.show(4);
        // @snippet:8:off

        // @snippet:9:on
        // Get intensity data
        Dataset<Row> intData = DevicePropertyDataQuery.builder(spark).system("CMW")
                .startTime("2018-09-27 00:00:00.000").endTime("2018-09-28 01:00:00.000")
                .entity().device("PR.BCT").property("HotspotIntensity").build();
        // @snippet:9:off

        // @snippet:10:on
        intData.where("selector = 'CPS.USER.SFTPRO1'").select(functions.max(functions.col("dcAftInj1"))).show();
        // @snippet:10:off

        // @snippet:11:on
        intData.describe("dcAftInj1").show();
        // @snippet:11:off

        // @snippet:12:on
        Dataset<Row> tgmDataReduced = DevicePropertyDataQuery.builder(spark).system("CMW")
                .startTime("2018-06-15 00:00:00.000").endTime("2018-06-17 00:00:00.000")
                .entity().parameter("CPS.TGM/FULL-TELEGRAM.STRC").build()
                .select("BATCH", "DEST", "USER");
        // @snippet:12:off

        // @snippet:13:on
        tgmDataReduced.printSchema();
        // @snippet:13:off
    }
}