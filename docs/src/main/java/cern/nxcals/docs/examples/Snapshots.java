package cern.nxcals.docs.examples;

import cern.nxcals.api.backport.client.service.MetaDataService;
import cern.nxcals.api.backport.client.service.QuerySnapshotDataService;
import cern.nxcals.api.backport.client.service.ServiceBuilder;
import cern.nxcals.api.backport.domain.core.constants.VariableDataType;
import cern.nxcals.api.backport.domain.core.metadata.VariableList;
import cern.nxcals.api.backport.domain.core.metadata.VariableListSet;
import cern.nxcals.api.backport.domain.core.metadata.VariableSet;
import cern.nxcals.api.backport.domain.core.snapshot.Snapshot;
import cern.nxcals.api.backport.domain.core.snapshot.SnapshotCriteria;
import cern.nxcals.api.backport.domain.core.snapshot.SnapshotProperties;
import cern.nxcals.api.backport.domain.core.snapshot.SnapshotPropertyName;
import cern.nxcals.api.custom.domain.GroupPropertyName;
import cern.nxcals.api.custom.domain.GroupType;
import cern.nxcals.api.domain.Group;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.domain.Visibility;
import cern.nxcals.api.extraction.metadata.GroupService;
import cern.nxcals.api.extraction.metadata.ServiceClientFactory;
import cern.nxcals.api.extraction.metadata.SystemSpecService;
import cern.nxcals.api.extraction.metadata.VariableService;
import cern.nxcals.api.extraction.metadata.queries.Groups;
import cern.nxcals.api.extraction.metadata.queries.Variables;
import com.github.rutledgepaulv.qbuilders.conditions.Condition;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class Snapshots {

    public static void main(String[] args) {
        snapshotsBackportApiDemo();
        snapshotCreateUpdate();
    }

    private static void snapshotsBackportApiDemo() {
        // @snippet:1:on
        QuerySnapshotDataService querySnapshotDataService = ServiceBuilder.getInstance().createQuerySnapshotDataService();
        MetaDataService metaDataService = ServiceBuilder.getInstance().createMetaService();
        GroupService groupService = ServiceClientFactory.createGroupService();
        // @snippet:1:off

        // @snippet:2:on
        /**
         * Get user snapshots with the given criteria.
         *
         * @param criteria - set of criteria to match snapshots against
         * @return returns the set of Snapshots
         *
         * List<Snapshot> getSnapshotsFor(SnapshotCriteria criteria);
         */

        SnapshotCriteria criteria = new SnapshotCriteria.SnapshotCriteriaBuilder("BI_BWS%", "jjgras")
                .withPublic().build();
        List<Snapshot> snapshots = querySnapshotDataService.getSnapshotsFor(criteria);

        snapshots.forEach(x -> {
            System.out.println("owner: " + x.getOwner() + " name: " + x.getName());

            SnapshotProperties snapshotProperties = x.getProperties();
            // Get selected variables if true
            Map<SnapshotPropertyName, String> propertiesMap = snapshotProperties.getAttributes(true);

            propertiesMap.forEach(
                    (key, values) -> System.out.println("\tKey: " + key.getDisplayName() + ", Value: " + values));
        });
        // @snippet:2:off

        // @snippet:3:on
        Condition<Groups> snapCondition = Groups.suchThat().label().eq(GroupType.SNAPSHOT.toString()).and()
                .visibility().eq(Visibility.PUBLIC).and().name().like("BI_BWS").and().ownerName().eq("jjgras");

        List<Group> snapshots2 = new ArrayList<>(groupService.findAll(snapCondition));
        // @snippet:3:off

        // @snippet:4:on
        /**
         * Gets a List of VariableList objects belonging to the given user, and with the list name and description matching
         * the given patterns. % = wildcard.
         *
         * @param userName the user name of the user
         * @param listNamePattern the list name pattern
         * @param listDescriptionPattern the list description pattern
         * @return the user variable lists
         *
         * VariableListSet getVariableListsOfUserWithNameLikeAndDescLike(String userName, String listNamePattern,
         *                 String listDescriptionPattern);
         */

        VariableListSet variableListSet = querySnapshotDataService
                .getVariableListsOfUserWithNameLikeAndDescLike("cdroderi", "MDB%", "%");

        variableListSet.getVariableLists()
                .forEach(x -> {
                    System.out.println("Onwer " + x.getUserName() + ", List name: " + x.getVariableListName());
                });
        // @snippet:4:off

        // @snippet:5:on
        Condition<Groups> groupCondition = Groups.suchThat().label().in(GroupType.GROUP.toString(), GroupType.SNAPSHOT.toString()).and()
                .ownerName().eq("cdroderi").and().name().like("MDB%").and().description().like("%");

        VariableListSet variableListSet2 = groupService.findAll(groupCondition).stream().map(VariableList::from)
                .collect(VariableListSet.collector());

        // @snippet:5:off


        // @snippet:6:on
        /**
         * Get a set of variables with a given datatype which can found in a variableList
         *
         * @param variableList - VariableList object which can be extracted using
         *            cern.accsoft.backport.extr.domain.client.QuerySnapshotDataService
         * @param dataType - VariableDataType to match the variables to. This is represented by an enum which can be found
         *            at VariableDataType
         * @return VariableSet - Collection of variables returned
         *
         * VariableSet getVariablesOfDataTypeInVariableList(
         *                 VariableList variableList, VariableDataType dataType);
         */

        VariableList variableList = querySnapshotDataService.getVariableListWithName("BI_BCT_LHCDC2");

        if (variableList != null) {
            VariableSet variableSet = metaDataService.getVariablesOfDataTypeInVariableList(variableList, VariableDataType.ALL);

            variableSet.forEach(x -> {
                System.out.println(x.getVariableName());
            });
        }
        // @snippet:6:off

        // @snippet:7:on
        Condition<Groups> condition = Groups.suchThat().label().eq(GroupType.GROUP.toString())
                .and().name().eq("BI_BCT_LHCDC2");
        VariableList variableList2 = groupService.findOne(condition).map(VariableList::from).orElse(null);
        // @snippet:7:off
    }

    private static void snapshotCreateUpdate() {
        // @snippet:10:on
        GroupService groupService = ServiceClientFactory.createGroupService();
        VariableService variableService = ServiceClientFactory.createVariableService();
        SystemSpecService systemSpecService = ServiceClientFactory.createSystemSpecService();

        String systemName = "MOCK-SYSTEM";
        SystemSpec systemSpec = systemSpecService.findByName(systemName)
                .orElseThrow(() -> new IllegalArgumentException("Could not find: " + systemName));

        String variableListName = "Test snapshot";
        Condition<Groups> snapCondition = Groups.suchThat().label().in(GroupType.SNAPSHOT.toString()).and()
                .name().eq(variableListName);

        groupService.findOne(snapCondition).ifPresent(group -> groupService.delete(group.getId()));

        Group variableListFromGroup = Group.builder().systemSpec(systemSpec).name(variableListName).description("Snapshot description")
                .label(GroupType.SNAPSHOT.toString()).visibility(Visibility.PUBLIC).build();
        Group newlyCreatedVariableList = groupService.create(variableListFromGroup);

        Set<Long> variableIds = variableService.findAll(Variables.suchThat()
                        .variableName().like("LHCB:LUMI%")).stream().map(Variable::getId)
                .collect(Collectors.toSet());
        Map<String, Set<Long>> variableAssociations = new HashMap<>();
        variableAssociations.put(GroupPropertyName.getSelectedVariables.name(), variableIds);

        groupService.setVariables(newlyCreatedVariableList.getId(), variableAssociations);
        // @snippet:10:off

        // @snippet:12:on
        groupService.findAll(snapCondition).forEach(snapshot -> {
            Map<String, Set<Variable>> map = groupService.getVariables(snapshot.getId());

            map.forEach((k, variableSet) -> {

                variableSet.forEach(variable -> {
                    System.out.println("Variable name: " + variable.getVariableName() + " Variable id: " + variable.getId());
                });
            });
        });
        // @snippet:12:off

        // @snippet:11:on
        groupService.findOne(Groups.suchThat().label().in(GroupType.SNAPSHOT.toString()).and()
                .name().eq(variableListName)).ifPresent(variableListToUpdate -> {
            variableListToUpdate.toBuilder().description("New Snapshot description").build();
            groupService.update(variableListToUpdate);
        });
        // @snippet:11:off
    }
}
