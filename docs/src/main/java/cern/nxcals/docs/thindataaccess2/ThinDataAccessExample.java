package cern.nxcals.docs.thindataaccess2;

/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */


import cern.cmw.datax.ImmutableData;
import cern.nxcals.api.custom.converters.FillConverter;
import cern.nxcals.api.custom.domain.Fill;
import cern.nxcals.api.domain.Hierarchy;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.extraction.data.Avro;
import cern.nxcals.api.extraction.data.Datax;
import cern.nxcals.api.extraction.metadata.HierarchyService;
import cern.nxcals.api.extraction.metadata.ServiceClientFactory;
import cern.nxcals.api.extraction.metadata.queries.Hierarchies;
import cern.nxcals.api.extraction.thin.AvroData;
import cern.nxcals.api.extraction.thin.AvroQuery;
import cern.nxcals.api.extraction.thin.ExtractionServiceGrpc.ExtractionServiceBlockingStub;
import cern.nxcals.api.extraction.thin.FillData;
import cern.nxcals.api.extraction.thin.FillQueryByNumber;
import cern.nxcals.api.extraction.thin.FillQueryByWindow;
import cern.nxcals.api.extraction.thin.FillServiceGrpc;
import cern.nxcals.api.extraction.thin.ServiceFactory;
import cern.nxcals.api.extraction.thin.TimeWindow;
import cern.nxcals.api.extraction.thin.data.builders.DataQuery;
import cern.nxcals.api.extraction.thin.data.builders.DevicePropertyDataQuery;
import cern.nxcals.api.utils.TimeUtils;
import cern.rbac.client.authentication.AuthenticationClient;
import cern.rbac.client.authentication.AuthenticationException;
import cern.rbac.common.RbaToken;
import cern.rbac.util.holder.ClientTierTokenHolder;
import com.google.protobuf.Empty;
import io.grpc.ManagedChannel;
import org.apache.avro.generic.GenericRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.StringJoiner;

/**
 * This is the minimal NXCALS Spark data access application using the Thin Spark Server API.
 * It requires to have an RBAC token present, it does not require Kerberos token.
 * In order to run it please set the system property user.password to your password (or any other service user password).
 * Please note that you need first to ask for access to the data by sending e-mail to acc-logging-support@cern.ch.
 * NOTE: THIS IS WORK IN PROGRESS, API MIGHT CHANGE!
 */
public class ThinDataAccessExample {

    static {
        System.setProperty("logging.config", "classpath:log4j2.yml");
        // Logging debug if necessary
        // System.setProperty("org.apache.logging.log4j.simplelog.StatusLogger.level", "TRACE");

        // Uncomment in order to overwrite the default security settings.
        //System.setProperty("javax.net.ssl.trustStore", "/opt/nxcalsuser/nxcals_cacerts");
        //System.setProperty("javax.net.ssl.trustStorePassword", "nxcals");


        // NXCALS PRO
        System.setProperty("service.url",
                "https://cs-ccr-nxcals6.cern.ch:19093,https://cs-ccr-nxcals7.cern.ch:19093,https://cs-ccr-nxcals8.cern.ch:19093");

    }
    private static final Logger log = LoggerFactory.getLogger(ThinDataAccessExample.class);
    private static final String SYSTEM_NAME = "CMW";
    //NXCALS PRO, Please use all or subset of Spark Servers addresses
    private static String SPARK_SERVERS_URL = "nxcals-spark-thin-api-lb:14500,cs-ccr-nxcals5.cern.ch:15000,cs-ccr-nxcals6.cern.ch:15000,cs-ccr-nxcals7.cern.ch:15000,cs-ccr-nxcals8.cern.ch:15000";

    // @snippet:3:on
    public static void main(String[] args) {

        login();

        ThinDataAccessExample thinDataAccessExample = new ThinDataAccessExample();

        log.info("############## Lets get some cmw data ############");
        thinDataAccessExample.getCmwData();

        log.info("############## Lets get some variable data ############");
        thinDataAccessExample.getVariableDataForHierarchy("/EXAMPLE");

        log.info("############## Lets get some fill data ############");
        thinDataAccessExample.getFillData();

        log.info("THE END, Happy Exploring Spark Thin API!");
    }
    // @snippet:3:off

    // @snippet:1:on
    public static void login() {

        //Login with RBAC, must set user.password or give it explicitly here, don't forget to remove before commit!
        login(System.getProperty("user.name"), System.getProperty("user.password"));

    }

    private static void login(String user, String password) {

        try {
            AuthenticationClient authenticationClient = AuthenticationClient.create();
            //Don't commit your password here!!!
            RbaToken token = authenticationClient.loginExplicit(user, password);
            ClientTierTokenHolder.setRbaToken(token);
        } catch (AuthenticationException e) {
            throw new IllegalArgumentException("Cannot login", e);
        }

    }
    // @snippet:1:off

    // @snippet:2:on
    List<ImmutableData> getCmwData() {

        //Create the service stub
        ExtractionServiceBlockingStub extractionService = ServiceFactory.createExtractionService(SPARK_SERVERS_URL);


        String startTime = "2018-08-01 00:00:00.00";
        String endTime = "2018-08-01 01:00:00.00";

        //This is the meta-data query script (you can write your own, this is just a helper class).
        String script = DevicePropertyDataQuery.builder().system(SYSTEM_NAME)
                .startTime(startTime).endTime(endTime).entity()
                .parameter("SPSBQMSPSv1/Acquisition").build();

        log.info("Generated Script: {}", script);


        script += ".select('acqStamp',\"bunchIntensities\")";
        log.info("You can change the script or create your own, both quote types work (\", '): {}", script);

        //Build a query
        AvroQuery avroQuery = AvroQuery.newBuilder()
                .setScript(script)
                .setCompression(AvroQuery.Codec.BZIP2) //optional bytes compression
                .build();

        //Query for data (return type is just bytes that need to be converted to desired record type, currently Avro or CMW ImmutableData)
        AvroData avroData = extractionService.query(avroQuery);

        log.info("What are the fields available?");
        log.info("Avro Schema: {}",avroData.getAvroSchema());


        long nrOfRecordsinExampleDataset = avroData.getRecordCount();
        log.info("Number of records: {} ", nrOfRecordsinExampleDataset);

        log.info("Converting data bytes into Avro Generic Records");
        List<GenericRecord> records = Avro.records(avroData);

        GenericRecord record = records.get(0);

        log.info("Record content: {}", record);

        log.info("Accessing a record field {}", record.get("bunchLengths") );


        log.info("Converting data into CMW ImmutableData objects");
        List<ImmutableData> cmwRecords = Datax.records(avroData);
        log.info("Immutable record: {}", cmwRecords.get(0));
        return cmwRecords;
    }

    List<Long> getVariableDataForHierarchy( String hierarchyPath) {
        //Create the service stub
        ExtractionServiceBlockingStub extractionService = ServiceFactory.createExtractionService(SPARK_SERVERS_URL);

        //Create the meta-data service stub
        HierarchyService service = ServiceClientFactory.createHierarchyService();
        List<Long> datasetSizes = new ArrayList<>();

        log.info("Getting hierarchy for {}", hierarchyPath);
        Hierarchy node = service.findOne(Hierarchies.suchThat().path().eq(hierarchyPath))
                .orElseThrow(()->new IllegalArgumentException("No such hierarchy path " + hierarchyPath));

        log.info("Found hierarchy: {}", node.getNodePath());

        String startTime = "2018-06-19 00:00:00.000"; //UTC
        String endTime = "2018-06-19 00:10:00.000"; //UTC

        Set<Variable> variables = service.getVariables(node.getId());
        for (Variable variableData: variables) {
            String script = DataQuery.builder().byVariables().system("CMW").startTime(startTime)
                    .endTime(endTime).variable(variableData.getVariableName()).build();

            log.info("Querying for {} variable between {} and {}, script: {}", variableData.getVariableName(), startTime, endTime, script);

            List<GenericRecord> records = Avro
                    .records(extractionService.query(AvroQuery.newBuilder().setScript(script).build()));

            long datasetSize = records.size();
            datasetSizes.add(datasetSize);
            log.info("Got {} rows for {}", datasetSize, variableData.getVariableName());
            records.subList(0,10).forEach(System.out::println);
        }

        return datasetSizes;
    }

    void getFillData() {

        //Create the service stub
        FillServiceGrpc.FillServiceBlockingStub fillService = ServiceFactory.createFillService(SPARK_SERVERS_URL);

        int fillNumber = 7252;

        log.info("Quering for fill nr {}", fillNumber);

        // Query for data
        FillData fillData = fillService.findFill(FillQueryByNumber.newBuilder().setFillNr(fillNumber).build());

        TimeWindow timeWindow = fillData.getValidity();

        StringJoiner beamJoiner = new StringJoiner(", ");
        fillData.getBeamModesList().forEach(b -> beamJoiner.add(b.getBeamModeValue()));

        log.info("Extracted fill {} starting at {} and finishing at {} having the following beam modes: {}",
                fillData.getNumber(),
                TimeUtils.getInstantFromNanos(timeWindow.getStartTime()),
                TimeUtils.getInstantFromNanos(timeWindow.getEndTime()),
                beamJoiner.toString());

        log.info("Quering for fills in time range");

        long start = TimeUtils.getNanosFromString("2018-10-02 00:00:00.378000000");
        long stop = TimeUtils.getNanosFromString("2018-10-10 00:00:00.56000000");

        Iterator<FillData> fillsData = fillService.findFills(FillQueryByWindow.newBuilder()
                .setTimeWindow(TimeWindow.newBuilder().setStartTime(start).setEndTime(stop).build()).build());

        // Optionally convert to list
        List<FillData> fillsDataList = new ArrayList<>();
        fillsData.forEachRemaining(fillsDataList::add);

        StringJoiner fillJoiner = new StringJoiner(",");
        fillsDataList.forEach(f -> fillJoiner.add(String.valueOf(f.getNumber())));

        log.info("Extracted {} fills with the time range: {}", fillsDataList.size(), fillJoiner.toString());


        log.info("Quering for the last completed fill");
        FillData lastCompleted = fillService.getLastCompleted(Empty.newBuilder().build());

        TimeWindow lastCompletedWindow = fillData.getValidity();

        log.info("Last completed fill: {}, started at {} and finished at {}",
                lastCompleted.getNumber(),
                TimeUtils.getInstantFromNanos(lastCompletedWindow.getStartTime()),
                TimeUtils.getInstantFromNanos(lastCompletedWindow.getEndTime())
        );

        // Convert from Thin API Extraction FillData object to standard extraction API Fill object
        Fill fill = FillConverter.toFill(lastCompleted);

        log.info("Fill number originating from Fill object:  {}", fill.getNumber());

        // Cleanup channel created during service initialization
        ((ManagedChannel)fillService.getChannel()).shutdown();
    }
    // @snippet:2:off
}
