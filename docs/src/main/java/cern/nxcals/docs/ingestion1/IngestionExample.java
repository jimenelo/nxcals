package cern.nxcals.docs.ingestion1;

import cern.cmw.datax.DataBuilder;
import cern.cmw.datax.EntryType;
import cern.cmw.datax.ImmutableData;
import cern.nxcals.api.ingestion.Publisher;
import cern.nxcals.api.ingestion.PublisherFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Function;

// @snippet:1:on
public class IngestionExample {

    private static final Logger LOGGER = LoggerFactory.getLogger(IngestionExample.class);
    private static final String SYSTEM_NAME = "MOCK-SYSTEM";

    private static final long MAX_NUMBER_OF_MSG_TO_BE_SENT = 10L;

    static {
        System.setProperty("logging.config", "classpath:log4j2.yml");


        // NXCALS Testbed (for PRO access please contact Logging team!)
        System.setProperty("service.url",
                "https://cs-ccr-testbed2.cern.ch:19093,https://cs-ccr-testbed3.cern.ch:19093,https://cs-ccr-nxcalstbs1.cern.ch:19093");
        System.setProperty("kafka.producer.bootstrap.servers",
                "cs-ccr-nxcalstbs1.cern.ch:9092,cs-ccr-nxcalstbs2.cern.ch:9092,cs-ccr-nxcalstbs3.cern.ch:9092,cs-ccr-nxcalstbs4.cern.ch:9092");
    }

    public static void main(String[] args) {
        IngestionExample ingestionExample = new IngestionExample();
    }
// @snippet:1:off

    // @snippet:2:on
    private static ImmutableData getExampleData() {
        final long stampNanos = TimeUnit.NANOSECONDS.convert(System.currentTimeMillis(), TimeUnit.MILLISECONDS);

        DataBuilder builder = ImmutableData.builder();

        // entity key field as specified in the schema of the system definition
        builder.add("device", "device_test");

        // partition field based on the system declaration (used for data partitioning/indexing on the storage)
        builder.add("specification", "specification_test");

        // timestamp accordingly to the system (as pointed by the SYSTEM_NAME property) definition
        builder.add("timestamp", stampNanos);

        // user data (could be anything based on the actual use-case, since it's not enforced by the system)
        builder.add("array_field", new int[]{1, 2, 3, 3}, new int[]{1});
        builder.add("double_field", 123.6257D);
        builder.add("extra_time_field", stampNanos);
        builder.add("description", "This record produced by NXCALS examples");

        // some fields may be set to null, in such case we have to specify their type
        builder.addNull("selector", EntryType.STRING);

        return builder.build();
    }
    // @snippet:2:off


    // @snippet:3:on
    long runExample(long nrOfMessagesToSent) {
        AtomicLong nrOfMessagesSent = new AtomicLong(0);

        try (Publisher<ImmutableData> publisher = PublisherFactory.newInstance()
                .createPublisher(SYSTEM_NAME, Function.identity())) {

            LOGGER.info("Will try to publish {} messages with data records, via the NXCALS client publisher",
                    nrOfMessagesToSent);

            for (int i = 0; i < nrOfMessagesToSent; i++) {
                int msgNum = i + 1;

                // get example data to be sent
                ImmutableData exampleData = getExampleData();
                publisher.publish(exampleData);

                LOGGER.info("Published record for message #{} with timestamp: {}",
                        msgNum, exampleData.getEntry("timestamp").get());
                nrOfMessagesSent.incrementAndGet();
            }
        } catch (Exception ex) {
            LOGGER.error("We cannot send data to NXCALS for some reason:", ex);
        }
        LOGGER.info("Finished!");
        return nrOfMessagesSent.get();
    }
    // @snippet:3:off

}


