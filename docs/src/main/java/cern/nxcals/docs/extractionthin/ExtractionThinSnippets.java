package cern.nxcals.docs.extractionthin;

import cern.cmw.datax.ImmutableData;
import cern.nxcals.api.extraction.data.Avro;
import cern.nxcals.api.extraction.data.Datax;
import cern.nxcals.api.extraction.thin.AvroData;
import cern.nxcals.api.extraction.thin.AvroQuery;
import cern.nxcals.api.extraction.thin.BeamMode;
import cern.nxcals.api.extraction.thin.ExtractionServiceGrpc;
import cern.nxcals.api.extraction.thin.FillData;
import cern.nxcals.api.extraction.thin.FillQueryByWindow;
import cern.nxcals.api.extraction.thin.FillServiceGrpc;
import cern.nxcals.api.extraction.thin.ServiceFactory;
import cern.nxcals.api.extraction.thin.TimeWindow;
import cern.nxcals.api.extraction.thin.data.builders.DevicePropertyDataQuery;
import cern.nxcals.api.extraction.thin.data.builders.ParameterDataQuery;
import cern.nxcals.api.utils.TimeUtils;
import cern.rbac.client.authentication.AuthenticationClient;
import cern.rbac.client.authentication.AuthenticationException;
import cern.rbac.common.RbaToken;
import cern.rbac.util.holder.ClientTierTokenHolder;
import io.grpc.ManagedChannel;
import org.apache.avro.generic.GenericRecord;

import java.util.Iterator;
import java.util.List;
import java.util.StringJoiner;

/**
 * This class containing the Thin API example will be problematic if run together with the normal Extraction API due
 * to the current jar clashes between proto2 & proto3 for Hbase.
 */
public class ExtractionThinSnippets {

    public static void login() {
        // @snippet:1:on
        try {
            String user = ""; //obtain the user-name from somewhere
            String password = ""; //obtain the password from somewhere
            AuthenticationClient authenticationClient = AuthenticationClient.create();
            RbaToken token = authenticationClient.loginExplicit(user, password);
            ClientTierTokenHolder.setRbaToken(token);
        } catch (AuthenticationException e) {
            throw new IllegalArgumentException("Cannot login", e);
        }
        // @snippet:1:off

    }

    public static void example1() {
        // @snippet:2:on
        final String SPARK_SERVERS_URL = "nxcals-spark-thin-api-lb:14500,cs-ccr-nxcals5.cern.ch:15000,cs-ccr-nxcals6.cern.ch:15000,cs-ccr-nxcals7.cern.ch:15000,cs-ccr-nxcals8.cern.ch:15000";
        //Create the service stub
        ExtractionServiceGrpc.ExtractionServiceBlockingStub extractionService = ServiceFactory
                .createExtractionService(SPARK_SERVERS_URL);

        String startTime = "2018-08-01 00:00:00.00";
        String endTime = "2018-08-01 01:00:00.00";

        //This is the meta-data query script (you can write your own, this is just a helper class).
        String script = DevicePropertyDataQuery.builder().system("CMW")
                .startTime(startTime).endTime(endTime).entity()
                .parameter("SPSBQMSPSv1/Acquisition").build();

        //Adding some custom operations to the generated script
        script += ".select('acqStamp',\"bunchIntensities\")";

        //Build a query
        AvroQuery avroQuery = AvroQuery.newBuilder()
                .setScript(script)
                .setCompression(AvroQuery.Codec.BZIP2) //optional bytes compression
                .build();

        //Query for data (return type is just bytes that need to be converted to desired record type, currently Avro or CMW ImmutableData)
        AvroData avroData = extractionService.query(avroQuery);
        long nrOfRecordsInExampleDataset = avroData.getRecordCount();
        //Convert to Avro GenericRecord
        List<GenericRecord> records = Avro.records(avroData);
        //Do something with the list of GenericRecord objects...

        //Convert to ImmutableData
        List<ImmutableData> cmwRecords = Datax.records(avroData);
        //Do something with the list of ImmutableData objects...

        // Cleanup channel created during service initialization
        ((ManagedChannel) extractionService.getChannel()).shutdown();
        // @snippet:2:off
    }

    public static void example2() {
        // @snippet:3:on
        final String SPARK_SERVERS_URL = "nxcals-spark-thin-api-lb:14500,cs-ccr-nxcals5.cern.ch:15000,cs-ccr-nxcals6.cern.ch:15000,cs-ccr-nxcals7.cern.ch:15000,cs-ccr-nxcals8.cern.ch:15000";

        //Create the service stub
        FillServiceGrpc.FillServiceBlockingStub fillService = ServiceFactory.createFillService(SPARK_SERVERS_URL);

        long start = TimeUtils.getNanosFromString("2018-10-02 00:00:00.000000000");
        long stop = TimeUtils.getNanosFromString("2018-10-04 00:00:00.00000000");

        Iterator<FillData> fillsData = fillService.findFills(FillQueryByWindow.newBuilder()
                .setTimeWindow(TimeWindow.newBuilder().setStartTime(start).setEndTime(stop).build()).build());

        fillsData.forEachRemaining(fillData -> printFillInfo(fillData));

        // Cleanup channel created during service initialization
        ((ManagedChannel) fillService.getChannel()).shutdown();
        // @snippet:3:off
    }

    // @snippet:4:on
    private static void printFillInfo(FillData fillData) {
        StringBuilder fillInfo = new StringBuilder();
        StringJoiner beamModes = new StringJoiner(",");

        List<BeamMode> beamModesList = fillData.getBeamModesList();
        beamModesList.forEach(b -> beamModes.add(b.getBeamModeValue()));

        fillInfo.append("Fill nr: ").append(String.valueOf(fillData.getNumber()))
                .append(" Beam modes: ").append(beamModes);

        System.out.println(fillInfo.toString());
    }
    // @snippet:4:off

    public static void example1_v2() {
        // @snippet:5:on
        final String SPARK_SERVERS_URL = "nxcals-spark-thin-api-lb:14500,cs-ccr-nxcals5.cern.ch:15000,cs-ccr-nxcals6.cern.ch:15000,cs-ccr-nxcals7.cern.ch:15000,cs-ccr-nxcals8.cern.ch:15000";
        //Create the service stub
        ExtractionServiceGrpc.ExtractionServiceBlockingStub extractionService = ServiceFactory
                .createExtractionService(SPARK_SERVERS_URL);

        String startTime = "2018-08-01 00:00:00.00";
        String endTime = "2018-08-01 01:00:00.00";

        //This is the meta-data query script (you can write your own, this is just a helper class).
        String script = ParameterDataQuery.builder().system("CMW")
                .parameterEq("SPSBQMSPSv1/Acquisition")
                .timeWindow(startTime, endTime).build();

        //Adding some custom operations to the generated script
        script += ".select('acqStamp',\"bunchIntensities\")";

        //Build a query
        AvroQuery avroQuery = AvroQuery.newBuilder()
                .setScript(script)
                .setCompression(AvroQuery.Codec.BZIP2) //optional bytes compression
                .build();

        //Query for data (return type is just bytes that need to be converted to desired record type, currently Avro or CMW ImmutableData)
        AvroData avroData = extractionService.query(avroQuery);
        long nrOfRecordsInExampleDataset = avroData.getRecordCount();
        //Convert to Avro GenericRecord
        List<GenericRecord> records = Avro.records(avroData);
        //Do something with the list of GenericRecord objects...

        //Convert to ImmutableData
        List<ImmutableData> cmwRecords = Datax.records(avroData);
        //Do something with the list of ImmutableData objects...

        // Cleanup channel created during service initialization
        ((ManagedChannel) extractionService.getChannel()).shutdown();
        // @snippet:5:off
    }
}
