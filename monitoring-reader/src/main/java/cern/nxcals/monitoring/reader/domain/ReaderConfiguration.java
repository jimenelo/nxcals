package cern.nxcals.monitoring.reader.domain;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Created by jwozniak on 14/01/17.
 */
@ConfigurationProperties("nxcals.reader")
@Data
@NoArgsConstructor
public class ReaderConfiguration {
    private List<EntityConfig> entities;
    private Map<String, CheckConfig> checks;
    private TimeUnit periodTimeUnit;
    private long period;
}
