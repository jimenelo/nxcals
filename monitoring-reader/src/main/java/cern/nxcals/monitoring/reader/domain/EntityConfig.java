package cern.nxcals.monitoring.reader.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by jwozniak on 14/01/17.
 */
@Data
@NoArgsConstructor
public class EntityConfig {
    private String system;
    private Map<String, Object> entityKeys;
    private List<String> checkRefs;

    public String getName() {
        return (getSystem() + "_" + String.join("_", getValues())).toLowerCase();
    }

    private List<String> getValues() {
        return getEntityKeys().values().stream().map(Object::toString).collect(Collectors.toList());
    }
}
