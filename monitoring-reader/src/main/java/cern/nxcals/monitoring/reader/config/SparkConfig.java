/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.monitoring.reader.config;

import cern.nxcals.api.config.SparkContext;
import cern.nxcals.common.config.SparkSessionModifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Import;

@Configuration
@Import(SparkContext.class)
public class SparkConfig {
    @Bean
    @DependsOn("kerberos")
    public SparkSessionModifier dummyModifier() {
        // dummy modifier only to add dependency between spark bean and kerberos, so that kerberos will be initialized before spark
        return sparkSession -> {
        };
    }
}
