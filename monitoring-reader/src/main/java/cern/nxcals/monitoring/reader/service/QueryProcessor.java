package cern.nxcals.monitoring.reader.service;

import cern.nxcals.common.metrics.MetricsRegistry;
import cern.nxcals.monitoring.reader.domain.CheckConfig;
import cern.nxcals.monitoring.reader.domain.EntityConfig;
import cern.nxcals.monitoring.reader.domain.Query;
import cern.nxcals.monitoring.reader.domain.ReaderConfiguration;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.time.DurationFormatUtils;
import org.springframework.stereotype.Service;

import javax.script.Bindings;
import javax.script.ScriptEngine;
import java.util.List;
import java.util.concurrent.ScheduledExecutorService;
import java.util.function.Function;

/**
 * Processor executing queries.
 */
@Service
@Slf4j
@RequiredArgsConstructor
@SuppressWarnings("squid:S1181") // catching throwable
public class QueryProcessor {

    private final ReaderConfiguration config;
    private final QueryBuilder queryBuilder;
    private final MetricsRegistry metricsRegistry;
    private final ScriptEngine engine;
    private final ScheduledExecutorService executor;

    public void process() {
        executor.scheduleAtFixedRate(this::runInThread, 0, config.getPeriod(), config.getPeriodTimeUnit());
    }

    private void runInThread() {
        try {
            long start = System.currentTimeMillis();
            log.info("Start processing entities {} ", config.getEntities());
            config.getEntities().parallelStream().forEach(this::processEntity);
            log.info("Finished processing entities in {}", duration(System.currentTimeMillis() - start));
        } catch (Throwable ex) {
            log.error("Exception while processing entities", ex);
        }
    }

    private void processEntity(EntityConfig entityConfig) {
        for (String checkRef : entityConfig.getCheckRefs()) {
            CheckConfig checkConfig = config.getChecks().get(checkRef);
            String metric = (entityConfig.getName() + "_" + checkConfig.getName()).toLowerCase();

            try {
                List<Query> queries = queryBuilder.build(entityConfig, checkConfig);
                long start = System.currentTimeMillis();
                long errorCount = countErrors(queries, query -> executeQuery(entityConfig.getName(), checkConfig, query));
                long executionTime = System.currentTimeMillis() - start;

                if (errorCount > 0) {
                    log.error("Errors ({}) for {} | executed in {}", errorCount, metric, duration(executionTime));
                } else {
                    log.info("All checks OK for {} | executed in {}", metric, duration(executionTime));
                }

                publishStatistics(metric, errorCount, 0, executionTime);
            } catch (Exception ex) {
                log.error("Exception while processing {}", metric, ex);
                publishStatistics(metric, 0, 1, 0);
            }
        }
    }

    private String duration(long executionTime) {
        return DurationFormatUtils.formatDurationHMS(executionTime);
    }

    private long countErrors(List<Query> results, Function<Query, Boolean> evaluation) {
        return results.parallelStream().map(evaluation).filter(val -> !val).count();
    }

    private void publishStatistics(String metric, long errors, int exceptions, long executionTime) {
        metricsRegistry.updateGaugeTo(metric + "_dataloss", errors);
        metricsRegistry.updateGaugeTo(metric + "_exceptions", exceptions);
        metricsRegistry.updateGaugeTo(metric + "_execution_time", executionTime);
    }

    private boolean executeQuery(String entityName, CheckConfig checkConfig, Query result) {
        try {
            Bindings bindings = engine.createBindings();
            bindings.put("dataSet", result.getDataset());
            log.debug("Start executing query for entity {} and check {} for start {} end {}", entityName, checkConfig, result.getStartTime(), result.getEndTime());
            Boolean eval = (Boolean) engine.eval(checkConfig.getCondition(), bindings);

            if (eval) {
                log.info("OK: Entity={} check={} condition={} from={} to={}",
                        entityName, checkConfig.getName(), checkConfig.getCondition(), result.getStartTime(),
                        result.getEndTime());
            } else {
                log.error(
                        "ERROR: Entity={} check={}  condition={} from={} to={} failed with message={}",
                        entityName, checkConfig.getName(), checkConfig.getCondition(), result.getStartTime(),
                        result.getEndTime(), engine.eval(checkConfig.getDebug(), bindings));
            }
            return eval;
        } catch (Throwable e) {
            log.error("Failed to execute entity={} check={} condition={}", entityName,
                    checkConfig.getName(), checkConfig.getCondition(), e);
            throw new IllegalStateException(e);
        }
    }
}
