package cern.nxcals.monitoring.reader.service;

import cern.nxcals.api.extraction.data.builders.DataQuery;
import cern.nxcals.monitoring.reader.domain.CheckConfig;
import cern.nxcals.monitoring.reader.domain.EntityConfig;
import cern.nxcals.monitoring.reader.domain.Query;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAmount;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

import static java.time.format.DateTimeFormatter.ofPattern;

/**
 * Builder for queries based on the entity configs.
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class QueryBuilder {
    static final DateTimeFormatter FORMATTER = ofPattern("yyyy-MM-dd HH:mm:ss");
    private Instant extractSince = Instant.EPOCH;

    @NonNull
    private final SparkSession session;

    @NonNull
    private final Clock clock;

    @Autowired
    QueryBuilder(SparkSession sparkSession) {
        this(sparkSession, Clock.systemDefaultZone());
    }

    public List<Query> build(EntityConfig entityConfig, CheckConfig checkConfig) {
        Instant startTime = clock.instant()
                .minus(checkConfig.getStartTime(), checkConfig.getTimeUnit())
                .truncatedTo(checkConfig.getTimeUnit());
        Instant endTime = startTime.plus(checkConfig.getTimeSlots(), checkConfig.getTimeUnit());
        TemporalAmount step = Duration.of(1, checkConfig.getTimeUnit());

        Instant currTime = startTime;
        while (currTime.isBefore(extractSince)) {
            log.info("Data checking/extraction of [{}] skipped due to start time {} older than configuration extraction time: {}, ",
                    (entityConfig.getName() + "_" + checkConfig.getName()).toLowerCase(), currTime, extractSince);
            currTime = currTime.plus(step);
        }

        List<Query> ret = new ArrayList<>();
        while (currTime.isBefore(endTime)) {
            Instant end = currTime.plus(step).minusNanos(1);
            ret.add(new Query(currTime, end, getDatasetSupplier(entityConfig, currTime, end)));
            currTime = currTime.plus(step);
        }
        return ret;
    }

    Supplier<Dataset<Row>> getDatasetSupplier(EntityConfig entityConfig, Instant start, Instant end) {
        return () -> DataQuery.builder(session)
                .byEntities()
                .system(entityConfig.getSystem())
                .startTime(start).endTime(end)
                .entity()
                .keyValues(entityConfig.getEntityKeys())
                .build();
    }

    /**
     * Sets the date to start the extraction getStartTime
     *
     * @param date - in format yyyy-mm-dd hh:mm:ss
     */
    @Value(value = "${nxcals.reader.extract.since:1970-01-01 00:00:00}")
    public void setExtractSince(String date) {
        this.extractSince = FORMATTER.withZone(this.clock.getZone()).parse(date, Instant::from);
        if (log.isInfoEnabled()) {
            log.info("Set extract since to {}", FORMATTER.withZone(this.clock.getZone()).format(this.extractSince));
        }
    }

    LocalDateTime getExtractSince() {
        return LocalDateTime.ofInstant(this.extractSince, this.clock.getZone());
    }
}
