package cern.nxcals.api.backport.client.service;

import cern.nxcals.api.backport.domain.core.constants.FilterOperand;
import cern.nxcals.api.backport.domain.core.constants.VariableDataType;
import cern.nxcals.api.backport.domain.core.metadata.Variable;
import cern.nxcals.api.backport.domain.core.timeseriesdata.filter.value.Filter;
import cern.nxcals.api.backport.domain.core.timeseriesdata.filter.value.FilterFactory;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class VariableExtractorFilterVisitorTest {
    private Variable dVar = build("dVar", VariableDataType.NUMERIC);
    private  Variable v1 = build(".TEST:TEST/TEST-V1", VariableDataType.NUMERIC);

    private static Variable build(String name, VariableDataType type) {
        return Variable.builder().variableName(name).variableDataType(type).build();
    }

    @Test
    public void shouldChangeNameIntoValidColumn() {
        VariableExtractorFilterVisitor visitor = new VariableExtractorFilterVisitor(dVar);
        Filter filter = FilterFactory.getInstanceForNumerics().withSingleValuePredicate(v1, 1.0, FilterOperand.EQUALS);
        filter.accept(visitor);

        ScalarFilterVariable filterVariable = visitor.getScalarFilterVariables().iterator().next();

        assertTrue("_TEST_TEST_TEST_V1".equals(filterVariable.getLegalColumnName()));

    }
}
