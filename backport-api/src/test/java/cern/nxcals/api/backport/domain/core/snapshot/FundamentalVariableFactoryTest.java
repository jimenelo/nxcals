package cern.nxcals.api.backport.domain.core.snapshot;

import cern.nxcals.api.backport.domain.core.metadata.FundamentalData;
import cern.nxcals.api.custom.domain.FundamentalFilter;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.Variable;
import com.google.common.collect.Sets;
import org.apache.commons.lang.StringUtils;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class FundamentalVariableFactoryTest {
    private static final String FUNDAMENTAL_VARIABLE_NAME_SEPARATOR = FundamentalData.FUNDAMENTAL_SEPARATOR;

    private static final SystemSpec SYSTEM_SPEC = SystemSpec.builder().name("TEST").entityKeyDefinitions("{}")
            .partitionKeyDefinitions("{}").timeKeyDefinitions("{}").build();

    @Test
    public void shouldThrowOnNullFundamentalFilters() {
        assertThrows(NullPointerException.class, () -> FundamentalVariableFactory.buildFrom(null, SYSTEM_SPEC));
    }

    @Test
    public void shouldThrowOnNullSystem() {
        assertThrows(NullPointerException.class, () -> FundamentalVariableFactory.buildFrom(Collections.emptySet(), null));
    }

    @Test
    public void shouldThrowOnFundamentalFiltersSetContainingNull() {
        assertThrows(NullPointerException.class, () -> FundamentalVariableFactory.buildFrom(Collections.singleton(null), SYSTEM_SPEC));
    }

    @Test
    public void shouldReturnEmptySetOnEmptyFundamentalFilters() {
        Set<Variable> variables = FundamentalVariableFactory
                .buildFrom(Collections.emptySet(), SYSTEM_SPEC);
        assertNotNull(variables);
        assertTrue(variables.isEmpty());
    }

    @Test
    public void shouldReturnGeneratedEmptyVariableWhenFundamentalFilterIsEmpty() {
        FundamentalFilter emptyFilter = FundamentalFilter.builder().build();
        Set<Variable> variables = FundamentalVariableFactory.buildFrom(Collections.singleton(emptyFilter), SYSTEM_SPEC);
        assertNotNull(variables);
        assertEquals(1, variables.size());

        String expectedVariableName = buildFundamentalVariableName("", "", "");
        Variable generatedVariable = variables.iterator().next();
        assertEquals(expectedVariableName, generatedVariable.getVariableName());
        assertEquals(SYSTEM_SPEC, generatedVariable.getSystemSpec());
    }

    @Test
    public void shouldReturnGeneratedPartiallyNamedVariableWhenFundamentalFilterHasMissingProperties() {
        FundamentalFilter filter1 = FundamentalFilter.builder().accelerator("accelerator1").timingUser("user1").build();
        FundamentalFilter filter2 = FundamentalFilter.builder().accelerator("accelerator2").lsaCycle("cycle2").build();
        FundamentalFilter filter3 = FundamentalFilter.builder().lsaCycle("cycle2").timingUser("user3").build();

        Set<FundamentalFilter> fundamentalFilters = Sets.newHashSet(filter1, filter2, filter3);
        Set<Variable> variables = FundamentalVariableFactory.buildFrom(fundamentalFilters, SYSTEM_SPEC);
        assertNotNull(variables);

        Set<String> expectedVariableNames = getExpectedVariableNamesFrom(fundamentalFilters);
        Set<String> actualVariableNames = getVariableNamesFrom(variables);

        assertEquals(expectedVariableNames.size(), actualVariableNames.size());
        assertTrue(actualVariableNames.containsAll(expectedVariableNames));
    }

    @Test
    public void shouldReturnGeneratedVariablesBasedOnFundamentalFilters() {
        FundamentalFilter filter1 = FundamentalFilter.builder().accelerator("accelerator1").lsaCycle("cycle1")
                .timingUser("user1").build();
        FundamentalFilter filter2 = FundamentalFilter.builder().accelerator("accelerator2").lsaCycle("cycle2")
                .timingUser("user2").build();

        Set<FundamentalFilter> fundamentalFilters = Sets.newHashSet(filter1, filter2);
        Set<Variable> variables = FundamentalVariableFactory.buildFrom(fundamentalFilters, SYSTEM_SPEC);
        assertNotNull(variables);

        Set<String> expectedVariableNames = getExpectedVariableNamesFrom(fundamentalFilters);
        Set<String> actualVariableNames = getVariableNamesFrom(variables);

        assertEquals(expectedVariableNames.size(), actualVariableNames.size());
        assertTrue(actualVariableNames.containsAll(expectedVariableNames));
    }

    private Set<String> getVariableNamesFrom(Set<Variable> fundamentalVariables) {
        return fundamentalVariables.stream().map(Variable::getVariableName).collect(Collectors.toSet());
    }

    private Set<String> getExpectedVariableNamesFrom(Set<FundamentalFilter> filters) {
        return filters.stream()
                .map(f -> buildFundamentalVariableName(f.getAccelerator(), f.getLsaCycle(), f.getTimingUser()))
                .collect(Collectors.toSet());
    }

    private String buildFundamentalVariableName(String accelerator, String lsaCycle, String timingUser) {
        return String.join(FUNDAMENTAL_VARIABLE_NAME_SEPARATOR, StringUtils.defaultString(accelerator, ""),
                StringUtils.defaultString(lsaCycle, ""), StringUtils.defaultString(timingUser, ""));
    }

}
