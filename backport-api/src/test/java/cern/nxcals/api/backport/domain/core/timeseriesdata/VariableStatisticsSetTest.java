package cern.nxcals.api.backport.domain.core.timeseriesdata;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class VariableStatisticsSetTest {

    private VariableStatistics stats_A = stats("stats_A");
    private VariableStatistics stats_B = stats("stats_B");
    private VariableStatistics stats_C = stats("stats_C");
    private VariableStatistics stats_D = stats("stats_D");

    @Test
    public void shouldAddDifferentStats() {
        VariableStatisticsSet set = new VariableStatisticsSet();

        set.addVariableStatistics(stats_A);
        set.addVariableStatistics(stats_B);
        set.addVariableStatistics(stats_C);
        set.addVariableStatistics(stats_D);

        assertEquals(4, set.size());
    }

    @Test
    public void shouldMergeSameStats() {
        VariableStatisticsSet set = new VariableStatisticsSet();

        set.addVariableStatistics(stats_A);
        set.addVariableStatistics(stats_A);
        set.addVariableStatistics(stats_B);
        set.addVariableStatistics(stats_C);
        set.addVariableStatistics(stats_D);

        assertEquals(4, set.size());
        verify(stats_A, times(1)).mergeWith(any(VariableStatistics.class));
    }

    @Test
    public void shouldRemoveDifferentStats() {
        VariableStatisticsSet set = new VariableStatisticsSet();

        set.addVariableStatistics(stats_A);
        set.addVariableStatistics(stats_B);
        set.addVariableStatistics(stats_C);
        set.addVariableStatistics(stats_D);

        assertEquals(4, set.size());
        assertNotNull(set.getVariableStatistics("stats_A"));

        set.removeVariableStatistics(stats_A);

        assertEquals(3, set.size());
        assertNull(set.getVariableStatistics("stats_A"));
    }

    @Test
    public void shouldSortByName() {
        VariableStatisticsSet set = new VariableStatisticsSet();

        set.addVariableStatistics(stats_D);
        set.addVariableStatistics(stats_C);
        set.addVariableStatistics(stats_B);
        set.addVariableStatistics(stats_A);

        assertEquals(4, set.size());
        List<VariableStatistics> list = set.getStatisticsList();
        assertEquals(stats_A, list.get(0));
        assertEquals(stats_B, list.get(1));
        assertEquals(stats_C, list.get(2));
        assertEquals(stats_D, list.get(3));
    }
    @Test
    public void shouldGetSorted() {
        VariableStatisticsSet set = new VariableStatisticsSet();

        set.addVariableStatistics(stats_D);
        set.addVariableStatistics(stats_C);
        set.addVariableStatistics(stats_B);
        set.addVariableStatistics(stats_A);

        assertEquals(4, set.size());
        assertEquals(stats_A, set.getVariableStatistics(0));
        assertEquals(stats_B, set.getVariableStatistics(1));
        assertEquals(stats_C, set.getVariableStatistics(2));
        assertEquals(stats_D, set.getVariableStatistics(3));
    }

    @Test
    public void shouldFailOnGetOutOfRange() {
        VariableStatisticsSet set = new VariableStatisticsSet();

        set.addVariableStatistics(stats_D);
        set.addVariableStatistics(stats_C);
        set.addVariableStatistics(stats_B);
        set.addVariableStatistics(stats_A);

        assertThrows(IndexOutOfBoundsException.class, () -> set.getVariableStatistics(4));
    }


    private VariableStatistics stats(String name) {
        VariableStatistics mock = mock(VariableStatistics.class);
        when(mock.getVariableName()).thenReturn(name);
        when(mock.mergeWith(any(VariableStatistics.class))).thenReturn(mock);
        return mock;
    }
}