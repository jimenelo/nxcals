package cern.nxcals.api.backport.client.service;

import cern.cmw.datax.ImmutableEntry;
import cern.nxcals.api.backport.domain.core.constants.VariableDataType;
import cern.nxcals.api.backport.domain.core.metadata.Variable;
import cern.nxcals.api.backport.domain.core.snapshot.Snapshot;
import cern.nxcals.api.backport.domain.core.snapshot.SnapshotPropertyName;
import cern.nxcals.api.backport.domain.core.timeseriesdata.SparkTimeseriesData;
import cern.nxcals.api.backport.domain.core.timeseriesdata.SparkTimeseriesDataSet;
import cern.nxcals.api.backport.domain.core.timeseriesdata.TimeseriesData;
import cern.nxcals.api.backport.domain.core.timeseriesdata.TimeseriesDataSet;
import cern.nxcals.api.backport.domain.core.timeseriesdata.filter.value.Filter;
import cern.nxcals.api.backport.domain.core.timeseriesdata.filter.value.Filters;
import cern.nxcals.api.custom.domain.GroupType;
import cern.nxcals.api.custom.service.AggregationService;
import cern.nxcals.api.domain.Group;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.domain.VariableConfig;
import cern.nxcals.api.domain.VariableDeclaredType;
import cern.nxcals.api.domain.Visibility;
import cern.nxcals.api.extraction.data.builders.DataQuery;
import cern.nxcals.api.extraction.data.builders.fluent.VariableAliasStage;
import cern.nxcals.api.extraction.data.builders.fluent.VariableStageLoop;
import cern.nxcals.api.extraction.metadata.GroupService;
import cern.nxcals.api.extraction.metadata.VariableService;
import cern.nxcals.api.utils.TimeUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataType;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import static cern.nxcals.api.backport.domain.core.constants.FilterOperand.BETWEEN;
import static cern.nxcals.api.backport.domain.core.constants.FilterOperand.EQUALS;
import static cern.nxcals.api.backport.domain.core.constants.FilterOperand.GREATER;
import static cern.nxcals.api.backport.domain.core.constants.FilterOperand.GREATER_OR_EQUALS;
import static cern.nxcals.api.backport.domain.core.constants.FilterOperand.IN;
import static cern.nxcals.api.backport.domain.core.constants.FilterOperand.LESS_OR_EQUALS;
import static cern.nxcals.api.backport.domain.core.constants.FilterOperand.NOT_IN;
import static cern.nxcals.api.backport.domain.core.constants.VectorRestriction.ALL_ELEMENTS;
import static cern.nxcals.api.backport.domain.core.constants.VectorRestriction.MATCHED_ELEMENTS;
import static cern.nxcals.api.backport.domain.core.constants.VectorRestriction.SINGLE_ELEMENT;
import static cern.nxcals.api.backport.domain.core.timeseriesdata.filter.value.FilterFactory.getInstanceForNumerics;
import static cern.nxcals.api.backport.domain.core.timeseriesdata.filter.value.FilterFactory.getInstanceForTextuals;
import static cern.nxcals.common.SystemFields.NXC_EXTR_TIMESTAMP;
import static cern.nxcals.common.SystemFields.NXC_EXTR_VALUE;
import static cern.nxcals.common.avro.SchemaConstants.ARRAY_DIMENSIONS_FIELD_NAME;
import static cern.nxcals.common.avro.SchemaConstants.ARRAY_ELEMENTS_FIELD_NAME;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.TestInstance.Lifecycle.PER_CLASS;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@TestInstance(PER_CLASS)
public class TimeseriesDataServiceImplTest {

    private TimeseriesDataServiceImpl timeseriesDataService;

    private DataQuery dataBuilderMock;

    private GroupService groupService;

    private VariableService variableService;

    private LHCFillDataService fillService;

    private MetaDataService metaDataService;

    private AggregationService aggregationService;

    private final Variable dVar = build("TEST:TEST/TEST-dVar", VariableDataType.NUMERIC);
    private final Variable v1 = build("TEST:TEST/TEST-V1", VariableDataType.NUMERIC);
    private final Variable v2 = build("TEST:TEST/TEST-V2", VariableDataType.NUMERIC);
    private final Variable v3 = build("TEST:TEST/TEST-V3", VariableDataType.NUMERIC);
    private final Variable v4 = build("TEST:TEST/TEST-V4", VariableDataType.NUMERIC);
    private final Variable v5 = build("TEST:TEST/TEST-V5", VariableDataType.TEXT);

    private final cern.nxcals.api.domain.Variable nxVar1 = nxVar("var1", VariableDeclaredType.NUMERIC);

    private final Variable vectorNumericVariable = build("vectorNumericVar", VariableDataType.VECTOR_NUMERIC);
    private final Variable vectorStringVariable = build("vectorStringVar", VariableDataType.VECTOR_STRING);

    private final SparkSession sparkSession = SparkSession.builder()
            .master("local")
            .appName("TimeseriesDataServiceImplTest")
            .getOrCreate();

    private static Variable build(String name, VariableDataType type) {
        return Variable.builder().variableName(name).variableDataType(type).build();
    }

    @BeforeEach
    public void setup() throws Exception {
        dataBuilderMock = mock(DataQuery.class, RETURNS_DEEP_STUBS);
        groupService = mock(GroupService.class, RETURNS_DEEP_STUBS);
        variableService = mock(VariableService.class, RETURNS_DEEP_STUBS);
        fillService = mock(LHCFillDataService.class, RETURNS_DEEP_STUBS);
        metaDataService = mock(MetaDataService.class, RETURNS_DEEP_STUBS);
        aggregationService = mock(AggregationService.class, RETURNS_DEEP_STUBS);
        timeseriesDataService = new TimeseriesDataServiceImpl(sparkSession, () -> dataBuilderMock, groupService,
                fillService, metaDataService, variableService, aggregationService);
    }

    private static SystemSpec systemSpec(String name) {
        return SystemSpec.builder().name(name).partitionKeyDefinitions("").timeKeyDefinitions("")
                .entityKeyDefinitions("").build();
    }

    private Group group(String name) {
        return Group.builder().name(name).systemSpec(systemSpec("CMW"))
                .visibility(Visibility.PROTECTED)
                .label(GroupType.SNAPSHOT.toString())
                .build();
    }

    private static cern.nxcals.api.domain.Variable nxVar(String name, VariableDeclaredType type) {
        VariableConfig config = VariableConfig.builder().fieldName("f1").build();
        return cern.nxcals.api.domain.Variable.builder().variableName(name)
                .declaredType(type).configs(Sets.newTreeSet(Sets.newHashSet(config)))
                .systemSpec(systemSpec("CMW"))
                .build();
    }

    public Object[][] dataForSnapshotTest() {
        Group g1 = group("s1");
        Map<String, Set<cern.nxcals.api.domain.Variable>> variables = new HashMap<>();
        variables.put(SnapshotPropertyName.getSelectedVariables.name(), Collections.singleton(nxVar1));
        //@formatter:off
        return new Object[][]{
                new Object[]{
                        g1, Snapshot.from(g1, variables),
                        new cern.nxcals.api.domain.Variable[]{nxVar1},
                        new Dataset[]{
                                ds(DataTypes.DoubleType, new Object[][]{{1L, 2d}, {2L, 2d}})
                        },
                        new TimeseriesDataSet[]{
                                ts(Variable.from(nxVar1), new Object[][]{{1L, 2d}, {2L, 2d}})
                        }
                }
        };
        //@formatter:on
    }

    @ParameterizedTest
    @MethodSource("dataForSnapshotTest")
    public void shouldGetDataForSnapshot(Group group, Snapshot snapshot, cern.nxcals.api.domain.Variable[] variables,
                                         Dataset<Row>[] datasets, TimeseriesDataSet[] result) {
        //give
        when(groupService
                .findOne(argThat(cond -> true))).thenReturn(Optional.of(group));

        Map<String, Set<cern.nxcals.api.domain.Variable>> associatedVariables = new HashMap<>();
        associatedVariables.put(SnapshotPropertyName.getSelectedVariables.name(), Sets.newHashSet(variables));
        when(groupService
                .getVariables(group.getId())).thenReturn(associatedVariables);

        mockDataSetsForVariables(variables, datasets);

        //when
        List<TimeseriesDataSet> data = timeseriesDataService
                .getDataForSnapshot(snapshot.getName(), snapshot.getOwner());

        //then
        assertEquals(result.length, data.size());
        assertThat(data.get(0)).containsExactlyElementsOf(result[0]);
    }

    private void mockDataSetsForVariables(cern.nxcals.api.domain.Variable[] variables, Dataset<Row>[] datasets) {
        for (int i = 0; i < variables.length; ++i) {
            cern.nxcals.api.domain.Variable var = variables[i];

            VariableAliasStage<Dataset<Row>> variableAliasStage = mock(VariableAliasStage.class, RETURNS_DEEP_STUBS);

            when(dataBuilderMock
                    .byVariables()
                    .system(argThat(v -> true))
                    .startTime((Instant) argThat(v -> true))
                    .endTime((Instant) argThat(v -> true))).thenReturn(variableAliasStage);

            when(variableAliasStage
                    .variable(argThat(v -> var.getVariableName().equals(v)))
                    .build())
                    .thenReturn(datasets[i]);
        }
    }

    public Object[][] dataForFilteringTest() {
        //@formatter:off
        return new Object[][]{
                new Object[]{
                        new Variable[]{dVar, dVar},
                        new Dataset[]{
                                ds(arrayType(DataTypes.IntegerType), new Object[][]{{1L, new Object[]{-1, 2, 3}},
                                        {2L, new Object[]{-1, -2, -3}}, {3L, new Object[]{7}}}),
                                ds(arrayType(DataTypes.IntegerType), new Object[][]{{1L, new Object[]{-1, 2, 3}},
                                        {2L, new Object[]{-1, -2, -3}}, {3L, new Object[]{7}}}),
                        },
                        getInstanceForNumerics().withSingleValuePredicate(dVar, 3d, GREATER_OR_EQUALS,
                                MATCHED_ELEMENTS),
                        2,
                        ts(dVar, new Object[][]{{1L, new Integer[]{3}}, {3L, new Integer[]{7}}})},

                new Object[]{
                        new Variable[]{dVar, dVar},
                        new Dataset[]{
                                ds(arrayType(DataTypes.IntegerType), new Object[][]{{1L, new Object[]{-1, 2, 3}},
                                        {2L, new Object[]{-1, -2, -3, 5}}, {3L, new Object[]{7}}}),
                                ds(arrayType(DataTypes.IntegerType), new Object[][]{{1L, new Object[]{-1, 2, 3}},
                                        {2L, new Object[]{-1, -2, -3, 5}}, {3L, new Object[]{7}}}),
                        },
                        getInstanceForNumerics().withMultipleValuesPredicate(dVar, new Double[]{3d, 5d}, IN,
                                MATCHED_ELEMENTS),
                        2,
                        ts(dVar, new Object[][]{{1L, new Integer[]{3}}, {2L, new Integer[]{5}}})},

                new Object[]{
                        new Variable[]{dVar, v1},
                        new Dataset[]{
                                ds(DataTypes.DoubleType, new Object[][]{{1L, 2d}, {2L, 2d}}),
                                ds(DataTypes.DoubleType, new Object[][]{{2L, 2d}})
                        },
                        getInstanceForNumerics().withTimePredicate(v1),
                        1, null},
                new Object[]{
                        new Variable[]{dVar, v5},
                        new Dataset[]{
                                ds(DataTypes.StringType, new Object[][]{{1L, "a"}, {2L, "b"}}),
                                ds(DataTypes.StringType, new Object[][]{{1L, "a"}, {2L, "b"}, {3L, "c"}}),
                        },
                        getInstanceForTextuals().withTimePredicate(v5),
                        2, null},
                new Object[]{
                        new Variable[]{dVar, v5},
                        new Dataset[]{
                                ds(DataTypes.StringType, new Object[][]{{1L, "a"}, {2L, "b"}}),
                                ds(DataTypes.StringType, new Object[][]{{1L, "aa"}, {2L, "bb"}, {3L, "cc"}}),
                        },
                        getInstanceForTextuals().withSingleValuePredicate(v5, "cc", EQUALS),
                        0, null},
                new Object[]{
                        new Variable[]{dVar},
                        new Dataset[]{
                                ds(DataTypes.DoubleType,
                                        new Object[][]{{1L, 2d}, {2L, 2d}, {3L, 4d}, {4L, 4d},
                                                {5L, null}}),
                        },
                        getInstanceForNumerics().withSingleValuePredicate(dVar, 2d, GREATER),
                        2, null},
                new Object[]{
                        new Variable[]{dVar, v1},
                        new Dataset[]{
                                ds(DataTypes.DoubleType, new Object[][]{{1L, 2d}, {2L, 2d}}),
                                ds(DataTypes.DoubleType, new Object[][]{{3L, 2d}})
                        },
                        getInstanceForNumerics().withTimePredicate(v1),
                        0, null},
                new Object[]{
                        new Variable[]{dVar, v1, v1},
                        new Dataset[]{
                                ds(DataTypes.DoubleType, new Object[][]{{1L, 2d}, {2L, 2d}, {3L, 2d}}),
                                ds(DataTypes.DoubleType, new Object[][]{{1L, 2d}, {2L, 3d}, {3L, 4d}}),
                                ds(DataTypes.DoubleType, new Object[][]{{1L, 2d}, {2L, 3d}, {3L, 4d}}),
                        },
                        Filters.and(
                                getInstanceForNumerics().withSingleValuePredicate(v1, 3d, GREATER_OR_EQUALS),
                                getInstanceForNumerics().withSingleValuePredicate(v1, 4d, IN)),
                        1, null},
                new Object[]{
                        new Variable[]{dVar, v1, v2},
                        new Dataset[]{
                                ds(DataTypes.DoubleType,
                                        new Object[][]{{1L, 2d}, {2L, 2d}, {3L, 2d}, {4L, 2d}}),
                                ds(DataTypes.DoubleType, new Object[][]{{1L, 4d}, {2L, 4.5d}, {3L, 7d}}),
                                ds(DataTypes.DoubleType, new Object[][]{{1L, 4d}, {2L, 5d}, {3L, 6d}})
                        },
                        Filters.or(
                                getInstanceForNumerics().withSingleValuePredicate(v1, 4d, EQUALS),
                                getInstanceForNumerics().withSingleValuePredicate(v2, 5d, EQUALS)
                        ),
                        2, null},
                new Object[]{
                        new Variable[]{dVar, v1, v2},
                        new Dataset[]{
                                ds(DataTypes.DoubleType,
                                        new Object[][]{{1L, 2d}, {2L, 2d}, {3L, 2d}, {4L, 2d}}),
                                ds(DataTypes.DoubleType, new Object[][]{{1L, 4d}, {2L, 4.5d}, {3L, 7d}}),
                                ds(DataTypes.DoubleType, new Object[][]{{1L, 4d}, {2L, 5d}, {3L, 6d}})
                        },
                        Filters.and(
                                getInstanceForNumerics().withSingleValuePredicate(v1, 4d, GREATER),
                                getInstanceForNumerics().withSingleValuePredicate(v2, 5d, EQUALS)
                        ),
                        1, null},
                new Object[]{
                        new Variable[]{dVar, v1, v2, v3},
                        new Dataset[]{
                                ds(DataTypes.DoubleType,
                                        new Object[][]{{1L, 2d}, {2L, 2d}, {3L, 2d}, {4L, 2d}}),
                                ds(DataTypes.DoubleType, new Object[][]{{1L, 4d}, {2L, 4.5d}, {3L, 7d}}),
                                ds(DataTypes.DoubleType, new Object[][]{{1L, 4d}, {2L, 5d}, {3L, 6d}}),
                                ds(DataTypes.DoubleType, new Object[][]{{1L, 3d}, {2L, -1d}, {3L, 7d}}),
                        },
                        Filters.and(Filters.and(
                                getInstanceForNumerics().withSingleValuePredicate(v1, 4d, GREATER),
                                getInstanceForNumerics().withSingleValuePredicate(v2, 5d, LESS_OR_EQUALS)
                        ), getInstanceForNumerics().withSingleValuePredicate(v3, -1d, LESS_OR_EQUALS)),
                        1, null},
                new Object[]{
                        new Variable[]{dVar, v1, v2, v3, v4},
                        new Dataset[]{
                                ds(DataTypes.DoubleType,
                                        new Object[][]{{1L, 2d}, {2L, 2d}, {3L, 2d}, {4L, 2d}}),
                                ds(DataTypes.DoubleType, new Object[][]{{1L, 4d}, {2L, 4.5d}, {3L, 7d}}),
                                ds(DataTypes.DoubleType, new Object[][]{{1L, 4d}, {2L, 5d}, {3L, 6d}}),
                                ds(DataTypes.DoubleType, new Object[][]{{1L, 3d}, {2L, -1d}, {3L, 7d}}),
                                ds(DataTypes.DoubleType,
                                        new Object[][]{{1L, 2d}, {2L, 2d}, {3L, 2d}, {4L, 2d}}),
                        },
                        Filters.or( //effectively all
                                Filters.and(Filters.and(
                                        getInstanceForNumerics().withSingleValuePredicate(v1, 4d, GREATER),
                                        getInstanceForNumerics().withSingleValuePredicate(v2, 5d, LESS_OR_EQUALS)
                                ), getInstanceForNumerics().withSingleValuePredicate(v3, -1d, LESS_OR_EQUALS)),
                                getInstanceForNumerics().withTimePredicate(v4)),
                        4, null},
                new Object[]{
                        new Variable[]{dVar, v1, v2, v3},
                        new Dataset[]{
                                ds(DataTypes.DoubleType,
                                        new Object[][]{{1L, 2d}, {2L, 2d}, {3L, 2d}, {4L, 2d}}),
                                ds(DataTypes.DoubleType, new Object[][]{{1L, 4d}, {2L, 4.5d}, {3L, 7d}}),
                                ds(DataTypes.DoubleType, new Object[][]{{1L, 4d}, {2L, 5d}, {3L, 6d}}),
                                ds(arrayType(DataTypes.DoubleType),
                                        new Object[][]{{1L, new Object[]{1d, 2d, 3d}},
                                                {2L, new Object[]{-1d, -2d, -3d}}, {3L, new Object[]{7d}}}),
                        },
                        Filters.and(Filters.and(
                                getInstanceForNumerics().withSingleValuePredicate(v1, 4d, GREATER),
                                getInstanceForNumerics().withSingleValuePredicate(v2, 5d, LESS_OR_EQUALS)
                        ), getInstanceForNumerics().withMultipleValuesPredicate(v3, new Double[]{-1d}, EQUALS,
                                SINGLE_ELEMENT)),
                        1, null},
                new Object[]{
                        new Variable[]{dVar, v1, v2, v3},
                        new Dataset[]{
                                ds(DataTypes.DoubleType,
                                        new Object[][]{{1L, 2d}, {2L, 2d}, {3L, 2d}, {4L, 2d}}),
                                ds(DataTypes.DoubleType, new Object[][]{{1L, 4d}, {2L, 4.5d}, {3L, 7d}}),
                                ds(DataTypes.DoubleType, new Object[][]{{1L, 4d}, {2L, 5d}, {3L, 6d}}),
                                ds(arrayType(DataTypes.DoubleType),
                                        new Object[][]{{1L, new Object[]{-1d, 2d, 3d}},
                                                {2L, new Object[]{-1d, -2d, -3d}}, {3L, new Object[]{7d}}}),
                        },
                        Filters.and(
                                Filters.and(
                                        getInstanceForNumerics().withSingleValuePredicate(v1, 4d, NOT_IN),
                                        getInstanceForNumerics().withSingleValuePredicate(v2, 5d, IN)
                                ), getInstanceForNumerics().withMultipleValuesPredicate(v3, new Double[]{-3d, 3d},
                                        BETWEEN, ALL_ELEMENTS)),
                        1, null},
        };
        //@formatter:on
    }

    public Object[][] dataForMaxValueTest() {
        //@formatter:off
        return new Object[][]{
                new Object[]{
                        dVar, TimeWindow.between(0L, 10L),
                        ds(DataTypes.IntegerType, new Object[][]{{1L, 7}, {2L, 10}, {3L, 2}}),
                        getTimeSeriesData(2L, 10)
                },
                new Object[]{
                        dVar, TimeWindow.between(0L, 10L),
                        ds(DataTypes.DoubleType, new Object[][]{{1L, 7d}, {2L, 10d}, {3L, 2d}}),
                        getTimeSeriesData(2L, 10d)
                },
                new Object[]{
                        dVar, TimeWindow.between(0L, 10L),
                        ds(DataTypes.LongType, new Object[][]{{1L, 7L}, {2L, 10L}, {3L, 2L}}),
                        getTimeSeriesData(2L, 10L)
                },
                new Object[]{
                        dVar, TimeWindow.between(10L, 20L),
                        ds(DataTypes.IntegerType),
                        null
                },
        };
        //@formatter:on
    }

    public Object[][] dataForVectornumFilteredByIndicesTest() {
        //@formatter:off
        return new Object[][]{
                new Object[]{
                        vectorNumericVariable,
                        TimeUtils.getTimestampFromNanos(0L),
                        TimeUtils.getTimestampFromNanos(10L),
                        new int[]{1, 3},
                        ds(arrayType(DataTypes.IntegerType),
                                new Object[][]{{1L, new Object[]{1, 2, 3}}, {2L, new Object[]{4, 5, 6}},
                                        {3L, new Object[]{7, 8, 9}}}),
                        ts(vectorNumericVariable,
                                new Object[][]{{1L, new Integer[]{1, 3}}, {2L, new Integer[]{4, 6}},
                                        {3L, new Integer[]{7, 9}}})
                },
                new Object[]{
                        vectorNumericVariable,
                        TimeUtils.getTimestampFromNanos(0L),
                        TimeUtils.getTimestampFromNanos(10L),
                        new int[]{1, 3},
                        ds(arrayType(DataTypes.LongType), new Object[][]{{1L, new Object[]{1L, 2L, 3L}},
                                {2L, new Object[]{4L, 5L, 6L}}, {3L, new Object[]{7L, 8L, 9L}}}),
                        ts(vectorNumericVariable,
                                new Object[][]{{1L, new Long[]{1L, 3L}}, {2L, new Long[]{4L, 6L}},
                                        {3L, new Long[]{7L, 9L}}})
                },
                new Object[]{
                        vectorNumericVariable,
                        TimeUtils.getTimestampFromNanos(0L),
                        TimeUtils.getTimestampFromNanos(10L),
                        new int[]{1, 3},
                        ds(arrayType(DataTypes.FloatType), new Object[][]{{1L, new Object[]{1f, 2f, 3f}},
                                {2L, new Object[]{4f, 5f, 6f}}, {3L, new Object[]{7f, 8f, 9f}}}),
                        ts(vectorNumericVariable,
                                new Object[][]{{1L, new Float[]{1f, 3f}}, {2L, new Float[]{4f, 6f}},
                                        {3L, new Float[]{7f, 9f}}})
                },
                new Object[]{
                        vectorNumericVariable,
                        TimeUtils.getTimestampFromNanos(0L),
                        TimeUtils.getTimestampFromNanos(10L),
                        new int[]{1, 3},
                        ds(arrayType(DataTypes.DoubleType), new Object[][]{{1L, new Object[]{1d, 2d, 3d}},
                                {2L, new Object[]{4d, 5d, 6d}}, {3L, new Object[]{7d, 8d, 9d}}}),
                        ts(vectorNumericVariable,
                                new Object[][]{{1L, new Double[]{1d, 3d}}, {2L, new Double[]{4d, 6d}},
                                        {3L, new Double[]{7d, 9d}}})
                },
                new Object[]{
                        vectorStringVariable,
                        TimeUtils.getTimestampFromNanos(0L),
                        TimeUtils.getTimestampFromNanos(10L),
                        new int[]{1, 3, 5},
                        ds(arrayType(DataTypes.StringType), new Object[][]{{1L, new Object[]{"a", "b"}},
                                {2L, new Object[]{"d", "e", "f"}}, {3L, new Object[]{}}}),
                        ts(vectorStringVariable,
                                new Object[][]{{1L, new String[]{"a"}}, {2L, new String[]{"d", "f"}},
                                        {3L, new String[]{}}})
                },
                new Object[]{
                        vectorStringVariable,
                        TimeUtils.getTimestampFromNanos(0L),
                        TimeUtils.getTimestampFromNanos(10L),
                        new int[]{},
                        ds(arrayType(DataTypes.StringType)),
                        ts(vectorStringVariable, new Object[][]{})
                }
        };
        //@formatter:on
    }

    private TimeseriesData getTimeSeriesData(long time, Object value) {
        Timestamp timestamp = TimeUtils.getTimestampFromNanos(time);
        return SparkTimeseriesData.of(timestamp, ImmutableEntry.of("", value, 1));
    }

    private TimeseriesDataSet ts(Variable var, Object[][] values) {
        List<TimeseriesData> dataList = new ArrayList<>(values.length);
        for (Object[] row : values) {
            Timestamp timestamp = TimeUtils.getTimestampFromNanos((long) row[0]);
            ImmutableEntry entry = ImmutableEntry.of("", row[1], 1);
            TimeseriesData timeseriesData = SparkTimeseriesData.of(timestamp, entry);
            dataList.add(timeseriesData);
        }
        return SparkTimeseriesDataSet.of(var, dataList);
    }

    @ParameterizedTest
    @MethodSource("dataForFilteringTest")
    public void shouldRunDataInTimeWindowFilteredByValues(Variable[] variables, Dataset<Row>[] datasets, Filter filter,
                                                          int resultSize, TimeseriesDataSet result) {
        //@formatter:off
        for (int i = 0; i < variables.length; ++i) {
            Variable var = variables[i];
            VariableStageLoop<Dataset<Row>> variableStageLoop = mock(VariableStageLoop.class, RETURNS_DEEP_STUBS);
            when(dataBuilderMock
                    .byVariables()
                    .system(argThat(v -> true))
                    .startTime((Instant) argThat(v -> true))
                    .endTime((Instant) argThat(v -> true))
                    .variable(argThat(v -> var.getVariableName().equals(v)))).thenReturn(variableStageLoop);
            //Again here mockito does not work with such deep mocks.
            when(variableStageLoop
                    .build())
                    .thenReturn(datasets[i]);
        }

        TimeseriesDataSet values = timeseriesDataService.getDataInTimeWindowFilteredByValues(
                variables[0],
                TimeUtils.getTimestampFromNanos(0), //not important
                TimeUtils.getTimestampFromNanos(100),
                filter
        );

        assertEquals(resultSize, values.size());

        if (result != null) {
            assertEquals(result, values);
        }

        //@formatter:on
    }

    @Test
    public void shouldReturnEmptyFundamentalSet() {

        assertEquals(0, timeseriesDataService.getFundamentalDataInTimeWindowWithFundamentalNamesLikePattern(
                "PATTERN_WITH_NO_FUNDAMENTALS",
                TimeUtils.getTimestampFromNanos(0),
                TimeUtils.getTimestampFromNanos(100)).size());
    }

    private StructType arrayType(DataType eltType) {
        StructField elts = new StructField(ARRAY_ELEMENTS_FIELD_NAME, DataTypes.createArrayType(eltType), true,
                Metadata.empty());
        StructField dims = new StructField(ARRAY_DIMENSIONS_FIELD_NAME,
                DataTypes.createArrayType(DataTypes.IntegerType), true, Metadata.empty());

        return DataTypes.createStructType(Lists.newArrayList(elts, dims));
    }

    private Dataset<Row> ds(DataType valueDataType, Object[]... data) {
        List<Row> rows = new ArrayList<>(data.length);

        for (Object[] datum : data) {
            assertEquals(2, datum.length);
            Row row;
            if (valueDataType.simpleString().contains("array")) {
                Row array = RowFactory.create(datum[1], new Integer[]{1});
                row = RowFactory.create(datum[0], array);
            } else {
                row = RowFactory.create(datum);
            }
            rows.add(row);
        }

        StructField aLong = new StructField(NXC_EXTR_TIMESTAMP.getValue(), DataTypes.LongType, true, Metadata.empty());
        StructField aType = new StructField(NXC_EXTR_VALUE.getValue(), valueDataType, true, Metadata.empty());
        StructType schema = new StructType(new StructField[]{aLong, aType});

        return sparkSession.createDataFrame(rows, schema);
    }

    @ParameterizedTest
    @MethodSource("dataForMaxValueTest")
    public void getDataHavingMaxValueInTimeWindow(Variable variable, TimeWindow timeWindow,
                                                  Dataset<Row> dataset, TimeseriesData expectedResult) {

        VariableAliasStage<Dataset<Row>> variableAliasStage = mock(VariableAliasStage.class, RETURNS_DEEP_STUBS);

        when(dataBuilderMock
                .byVariables()
                .system(argThat(v -> true))
                .startTime((Instant) argThat(v -> true))
                .endTime((Instant) argThat(v -> true))).thenReturn(variableAliasStage);

        when(variableAliasStage
                .variable(argThat(v -> variable.getVariableName().equals(v)))
                .build())
                .thenReturn(dataset);

        TimeseriesData value = timeseriesDataService
                .getDataHavingMaxValueInTimeWindow(variable, Timestamp.from(timeWindow.getStartTime()),
                        Timestamp.from(timeWindow.getEndTime()));
        assertEquals(expectedResult, value);
    }

    @ParameterizedTest
    @MethodSource("dataForVectornumFilteredByIndicesTest")
    public void getVectornumericDataInTimeWindowFilteredByVectorIndices(
            Variable variable, Timestamp startTime, Timestamp endTime, int[] indexes,
            Dataset<Row> dataset, TimeseriesDataSet expectedResult) {

        VariableAliasStage<Dataset<Row>> variableAliasStage = mock(VariableAliasStage.class, RETURNS_DEEP_STUBS);

        when(dataBuilderMock
                .byVariables()
                .system(argThat(v -> true))
                .startTime((Instant) argThat(v -> true))
                .endTime((Instant) argThat(v -> true))).thenReturn(variableAliasStage);

        when(variableAliasStage
                .variable(argThat(v -> variable.getVariableName().equals(v)))
                .build())
                .thenReturn(dataset);

        assertEquals(expectedResult,
                timeseriesDataService.getVectornumericDataInTimeWindowFilteredByVectorIndices(variable, startTime,
                        endTime, indexes));
    }

    public Object[][] dataForVectorElementCount() {
        //@formatter:off
        return new Object[][]{
                new Object[]{
                        vectorNumericVariable,
                        TimeUtils.getTimestampFromNanos(0L),
                        TimeUtils.getTimestampFromNanos(10L),
                        ds(arrayType(DataTypes.IntegerType),
                                new Object[][]{{1L, new Object[]{1, 2, 3}}, {2L, new Object[]{1, 2, 3, 4}},
                                        {3L, new Object[]{1, 2}}}),
                        4},
                new Object[]{
                        vectorStringVariable,
                        TimeUtils.getTimestampFromNanos(0L),
                        TimeUtils.getTimestampFromNanos(10L),
                        ds(arrayType(DataTypes.StringType), new Object[][]{{1L, new Object[]{"a", "b"}},
                                {2L, new Object[]{"a", "b", "c"}}, {3L, new Object[]{"a"}}}),
                        3},
                new Object[]{
                        vectorStringVariable,
                        TimeUtils.getTimestampFromNanos(0L),
                        TimeUtils.getTimestampFromNanos(10L),
                        ds(arrayType(DataTypes.StringType)),
                        0}
        };
        //@formatter:on
    }

    @ParameterizedTest
    @MethodSource("dataForVectorElementCount")
    public void shouldGetMaxVectornumericElementCountInTimeWindow(Variable variable, Timestamp startTime,
                                                                  Timestamp endTime, Dataset<Row> dataset, int expectedResult) {

        VariableAliasStage<Dataset<Row>> variableAliasStage = mock(VariableAliasStage.class, RETURNS_DEEP_STUBS);

        when(dataBuilderMock
                .byVariables()
                .system(argThat(v -> true))
                .startTime((Instant) argThat(v -> true))
                .endTime((Instant) argThat(v -> true))).thenReturn(variableAliasStage);

        when(variableAliasStage
                .variable(argThat(v -> variable.getVariableName().equals(v)))
                .build())
                .thenReturn(dataset);

        int maxCount = timeseriesDataService.getMaxVectornumericElementCountInTimeWindow(variable, startTime, endTime);
        assertEquals(expectedResult, maxCount);
    }

    public Object[][] dataForDataDistribution() {
        //@formatter:off
        return new Object[][]{
                new Object[]{
                        ds(arrayType(DataTypes.IntegerType),
                                new Object[][]{{1L, new Object[]{1, 2, 3}}, {2L, new Object[]{1, 2, 3, 4}},
                                        {3L, new Object[]{1, 2}}}),
                        ts(v1, new Object[][]{{1L, new Integer[]{0}}, {2L, new Integer[]{0}},
                                {3L, new Integer[]{0}}})},
                new Object[]{
                        ds(arrayType(DataTypes.StringType), new Object[][]{{1L, new Object[]{"a", "b"}},
                                {2L, new Object[]{"a", "b", "c"}}, {3L, new Object[]{"a"}}}),
                        ts(v1, new Object[][]{{1L, new String[]{null}}, {2L, new String[]{null}},
                                {3L, new String[]{null}}})},
                new Object[]{
                        ds(DataTypes.DoubleType, new Object[][]{{1L, 7d}, {2L, 10d}, {3L, 2d}}),
                        ts(v1, new Object[][]{{1L, 0d}, {2L, 0d}, {3L, 0d}})}
        };
        //@formatter:on
    }

    @ParameterizedTest
    @MethodSource("dataForDataDistribution")
    public void shouldGetDataDistribution(Dataset<Row> inputDataset, TimeseriesDataSet expectedDataset) {
        Timestamp startTime = TimeUtils.getTimestampFromNanos(0L);
        Timestamp endTime = TimeUtils.getTimestampFromNanos(10L);

        VariableAliasStage variableAliasStage = mock(VariableAliasStage.class, RETURNS_DEEP_STUBS);

        when(dataBuilderMock
                .byVariables()
                .system(argThat(v -> true))
                .startTime((Instant) argThat(v -> true))
                .endTime((Instant) argThat(v -> true))).thenReturn(variableAliasStage);

        when(variableAliasStage
                .variable(any())
                .build())
                .thenReturn(inputDataset);
        TimeseriesDataSet result = timeseriesDataService.getDataDistribution(v1, startTime, endTime);
        for (int i = 0; i < expectedDataset.size(); i++) {
            assertEquals(expectedDataset.getTimeseriesData(i), result.getTimeseriesData(i));
        }
    }
}
