package cern.nxcals.api.backport.client.service;

import cern.japc.core.AcquiredParameterValue;
import cern.japc.core.Parameters;
import cern.japc.core.Selectors;
import cern.japc.value.MapParameterValue;
import cern.nxcals.api.backport.domain.core.metadata.FundamentalData;
import cern.nxcals.api.backport.domain.core.metadata.JapcParameterDefinition;
import cern.nxcals.api.backport.domain.core.metadata.VariableSet;
import cern.nxcals.api.extraction.data.builders.DataQuery;
import cern.nxcals.api.extraction.data.builders.fluent.DeviceStage;
import cern.nxcals.api.extraction.data.builders.fluent.EntityAliasStage;
import cern.nxcals.api.extraction.data.builders.fluent.SystemStage;
import cern.nxcals.api.extraction.data.builders.fluent.TimeEndStage;
import cern.nxcals.api.extraction.data.builders.fluent.TimeStartStage;
import cern.nxcals.api.extraction.data.builders.fluent.VariableAliasStage;
import cern.nxcals.api.utils.TimeUtils;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static cern.nxcals.api.backport.domain.core.metadata.FundamentalData.DESTINATION_FIELD;
import static cern.nxcals.api.backport.domain.core.metadata.FundamentalData.FUNDAMENTAL_SUFFIX;
import static cern.nxcals.api.backport.domain.core.metadata.FundamentalData.TGM_LSA_CYCLE_FIELD;
import static cern.nxcals.api.backport.domain.core.metadata.FundamentalData.USER_FIELD;
import static cern.nxcals.api.backport.domain.core.metadata.FundamentalData.VIRTUAL_LSA_CYCLE_FIELD;
import static cern.nxcals.api.backport.domain.core.metadata.FundamentalData.XTIM_DESTINATION_FIELD;
import static cern.nxcals.api.backport.domain.core.metadata.FundamentalData.XTIM_LSA_CYCLE_FIELD;
import static cern.nxcals.api.custom.domain.CmwSystemConstants.ACQSTAMP;
import static cern.nxcals.api.custom.domain.CmwSystemConstants.CMW_SYSTEM;
import static cern.nxcals.api.custom.domain.CmwSystemConstants.CYCLESTAMP;
import static cern.nxcals.api.custom.domain.CmwSystemConstants.DEVICE_KEY_NAME;
import static cern.nxcals.api.custom.domain.CmwSystemConstants.PROPERTY_KEY_NAME;
import static cern.nxcals.api.custom.domain.CmwSystemConstants.RECORD_TIMESTAMP;
import static cern.nxcals.api.custom.domain.CmwSystemConstants.SELECTOR;
import static cern.nxcals.api.utils.TimeUtils.getTimestampFromNanos;
import static cern.nxcals.common.SystemFields.NXC_EXTR_TIMESTAMP;
import static cern.nxcals.common.SystemFields.NXC_EXTR_VARIABLE_NAME;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class AcquiredParameterValueServiceImplTest {
    private static final String DEV_1 = "dev1";
    private static final String PROP_1 = "prop1";
    public static final String PARAMETER_NAME = Parameters.buildParameterName(DEV_1, PROP_1);
    public static final JapcParameterDefinition JAPC_PARAMETER_DEFINITION = JapcParameterDefinition.builder()
            .deviceName(DEV_1).propertyName(PROP_1)
            .build();
    private static final String SEL = "sel";
    private static final String FIELD_1 = "field1";
    private static final long STOP_TIME = 200L;
    public static final Timestamp STOP_TIMESTAMP = getTimestampFromNanos(STOP_TIME);
    private static final long START_TIME = 100L;
    public static final Timestamp START_TIMESTAMP = getTimestampFromNanos(START_TIME);
    private static final StructType SCHEMA = createSchema();
    private static final StructType FUND_SCHEMA = createFundamentalDSSchema();

    private static final SparkSession sparkSession = SparkSession.builder()
            .master("local")
            .appName("AcquiredParameterValueServiceImplTest")
            .getOrCreate();

    public static final Dataset<Row> DATASET = ds(SCHEMA, new Object[][] {
            { DEV_1, PROP_1, 1L, 1L, 1L, SEL, 10L },
            { DEV_1, PROP_1, 7L, 2L, 2L, SEL, 20L },
            { DEV_1, PROP_1, 5L, 3L, 3L, SEL, 30L },
            { DEV_1, PROP_1, 2L, 4L, 4L, SEL, 40L },
    });

    public static final Dataset<Row> DATASET_WITH_NULL_SELECTOR = ds(SCHEMA, new Object[][] {
            { DEV_1, PROP_1, 1L, 1L, 1L, null, 10L },
            { DEV_1, PROP_1, 7L, 2L, 2L, null, 20L },
            { DEV_1, PROP_1, 5L, 3L, 3L, null, 30L },
            { DEV_1, PROP_1, 2L, 4L, 4L, null, 40L },
    });

    public static final Dataset<Row> FUND_DATASET = ds(FUND_SCHEMA, new Object[][] {
            { "LHC:" + FUNDAMENTAL_SUFFIX, 1L, "CYCLE1", "LHC1", "DEST" },
            { "LHC:" + FUNDAMENTAL_SUFFIX, 2L, "CYCLE1", "LHC2", "DEST" },
            { "LHC:" + FUNDAMENTAL_SUFFIX, 5L, "CYCLE1", "LHC2", "DEST" },
            { "SPS:" + FUNDAMENTAL_SUFFIX, 7L, "CYCLE1", "LHC2", "DEST" }, //not matching
            { "LHC:" + FUNDAMENTAL_SUFFIX, 9L, "CYCLE2", "LHC1", "DEST" }, //not matching
    });

    public static final Dataset<Row> EMPTY_DATASET = ds(SCHEMA);
    public static final Dataset<Row> EMPTY_FUNDAMENTAL_DATASET = ds(FUND_SCHEMA);

    private SparkSession sparkSessionMock = mock(SparkSession.class);
    private SystemStage<TimeStartStage<EntityAliasStage<DeviceStage<Dataset<Row>>, Dataset<Row>>, Dataset<Row>>, Dataset<Row>> parameterQueryMock;
    private AcquiredParameterValuesService acquiredParameterValuesService;
    private DataQuery dataQueryMock;

    private static Dataset<Row> ds(StructType schema, Object[]... data) {
        List<Row> rows = new ArrayList<>(data.length);

        for (Object[] datum : data) {
            rows.add(RowFactory.create(datum));
        }

        return sparkSession.createDataFrame(rows, schema);
    }

    private static StructType createSchema() {

        StructField deviceField = new StructField(DEVICE_KEY_NAME, DataTypes.StringType, true, Metadata.empty());
        StructField properyField = new StructField(PROPERTY_KEY_NAME, DataTypes.StringType, true, Metadata.empty());
        StructField recordTimestamp = new StructField(RECORD_TIMESTAMP, DataTypes.LongType, true, Metadata.empty());
        StructField acqStampField = new StructField(ACQSTAMP, DataTypes.LongType, true, Metadata.empty());
        StructField cyclestampField = new StructField(CYCLESTAMP, DataTypes.LongType, true, Metadata.empty());
        StructField selectorField = new StructField(SELECTOR, DataTypes.StringType, true, Metadata.empty());
        StructField dataField = new StructField(FIELD_1, DataTypes.LongType, true, Metadata.empty());

        return new StructType(
                new StructField[] { deviceField, properyField, recordTimestamp, acqStampField, cyclestampField,
                        selectorField,
                        dataField });
    }

    //This is just a dummy fundamental ds
    private static StructType createFundamentalDSSchema() {

        StructField varName = new StructField(NXC_EXTR_VARIABLE_NAME.getValue(), DataTypes.StringType, true,
                Metadata.empty());
        StructField timestamp = new StructField(NXC_EXTR_TIMESTAMP.getValue(), DataTypes.LongType, true,
                Metadata.empty());
        StructField lsaCycle = new StructField(VIRTUAL_LSA_CYCLE_FIELD, DataTypes.StringType, true, Metadata.empty());
        StructField timingUser = new StructField(USER_FIELD, DataTypes.StringType, true, Metadata.empty());
        StructField destination = new StructField(DESTINATION_FIELD, DataTypes.StringType, true, Metadata.empty());

        return new StructType(
                new StructField[] { varName, timestamp, lsaCycle, timingUser, destination });
    }

    @BeforeEach
    public void setup() {
        parameterQueryMock = mock(SystemStage.class);
        dataQueryMock = mock(DataQuery.class, RETURNS_DEEP_STUBS);
        sparkSessionMock = mock(SparkSession.class);

        acquiredParameterValuesService = new AcquiredParameterValueServiceImpl(() -> dataQueryMock,
                () -> parameterQueryMock);
    }

    @Test
    public void shouldGetParameterValuesInTimeWindowFilteredByFundamentals() {
        //given
        mockQuery(DATASET, STOP_TIME);
        mockFundamental(FUND_DATASET);

        VariableSet fundamentals = VariableSet.fromCalsVars(ImmutableSet.of(
                cern.nxcals.api.backport.domain.core.metadata.Variable.builder()
                        .variableName("LHC:CYCLE1:LHC1").build(),
                cern.nxcals.api.backport.domain.core.metadata.Variable.builder()
                        .variableName("LHC:CYCLE1:LHC2").build()
        ));

        //when

        List<AcquiredParameterValue> values = acquiredParameterValuesService
                .getParameterValuesInTimeWindowFilteredByFundamentals(JAPC_PARAMETER_DEFINITION, START_TIMESTAMP,
                        STOP_TIMESTAMP, fundamentals);

        //then
        assertEquals(3, values.size());
    }

    @Test
    public void shouldGetParameterValuesInTimeWindowFilteredByFundamentalsEmpty() {
        //given
        mockQuery(DATASET, STOP_TIME);
        mockFundamental(EMPTY_FUNDAMENTAL_DATASET);

        VariableSet fundamentals = VariableSet.fromCalsVars(ImmutableSet.of(
                cern.nxcals.api.backport.domain.core.metadata.Variable.builder()
                        .variableName("LHC:CYCLE1:LHC1").build(),
                cern.nxcals.api.backport.domain.core.metadata.Variable.builder()
                        .variableName("LHC:CYCLE1:LHC2").build()
        ));

        //when

        List<AcquiredParameterValue> values = acquiredParameterValuesService
                .getParameterValuesInTimeWindowFilteredByFundamentals(JAPC_PARAMETER_DEFINITION, START_TIMESTAMP,
                        STOP_TIMESTAMP, fundamentals);

        //then
        assertEquals(0, values.size());
    }

    @Test
    public void shouldGetDataDistribution() {
        //given
        mockQuery(DATASET, STOP_TIME);

        //when
        List<Timestamp> values = acquiredParameterValuesService
                .getDataDistribution(JAPC_PARAMETER_DEFINITION, START_TIMESTAMP,
                        STOP_TIMESTAMP);

        assertThat(values)
                .containsExactly(getTimestampFromNanos(1L), getTimestampFromNanos(2L), getTimestampFromNanos(5L),
                        getTimestampFromNanos(7L));
    }

    @Test
    public void shouldGetDataDistributionEmpty() {
        //given
        mockQuery(EMPTY_DATASET, STOP_TIME);

        //when
        List<Timestamp> values = acquiredParameterValuesService
                .getDataDistribution(JAPC_PARAMETER_DEFINITION, START_TIMESTAMP,
                        STOP_TIMESTAMP);

        assertEquals(0, values.size());
    }

    @Test
    public void shouldGetParameterValuesInTimeWindow() {
        //given
        mockQuery(DATASET, STOP_TIME);

        //when
        List<AcquiredParameterValue> values = acquiredParameterValuesService
                .getParameterValuesInTimeWindow(JAPC_PARAMETER_DEFINITION, START_TIMESTAMP,
                        STOP_TIMESTAMP);

        assertEquals(4, values.size());

        AcquiredParameterValue acquiredParameterValue = values.get(0);
        assertEquals(PARAMETER_NAME, acquiredParameterValue.getParameterName());
        assertEquals(SEL, acquiredParameterValue.getHeader().getSelector().toString());
        assertEquals(1L, acquiredParameterValue.getHeader().getAcqStamp());
        assertEquals(1L, acquiredParameterValue.getHeader().getCycleStamp());
        assertEquals(10L, ((MapParameterValue) acquiredParameterValue.getValue()).getLong(FIELD_1));
    }

    @Test
    public void shouldGetParameterValuesInTimeWindowEmpty() {
        //given
        mockQuery(EMPTY_DATASET, STOP_TIME);

        //when
        List<AcquiredParameterValue> values = acquiredParameterValuesService
                .getParameterValuesInTimeWindow(JAPC_PARAMETER_DEFINITION, START_TIMESTAMP,
                        STOP_TIMESTAMP);

        assertEquals(0, values.size());
    }

    @Test
    public void shouldGetParameterValueAtTimestampWhenSelectorIsNull() {
        //given
        mockQuery(DATASET_WITH_NULL_SELECTOR, START_TIME);

        //when
        AcquiredParameterValue acquiredParameterValue = acquiredParameterValuesService
                .getParameterValueAtTimestamp(JAPC_PARAMETER_DEFINITION, START_TIMESTAMP);

        assertEquals(PARAMETER_NAME, acquiredParameterValue.getParameterName());
        assertEquals(Selectors.NO_SELECTOR.getId(), acquiredParameterValue.getHeader().getSelector().getId());
        assertEquals(1L, acquiredParameterValue.getHeader().getAcqStamp());
        assertEquals(1L, acquiredParameterValue.getHeader().getCycleStamp());
        assertEquals(10L, ((MapParameterValue) acquiredParameterValue.getValue()).getLong(FIELD_1));
    }

    @Test
    public void shouldGetParameterValueAtTimestamp() {
        //given
        mockQuery(DATASET, START_TIME);

        //when
        AcquiredParameterValue acquiredParameterValue = acquiredParameterValuesService
                .getParameterValueAtTimestamp(JAPC_PARAMETER_DEFINITION, START_TIMESTAMP);

        assertEquals(PARAMETER_NAME, acquiredParameterValue.getParameterName());
        assertEquals(SEL, acquiredParameterValue.getHeader().getSelector().toString());
        assertEquals(1L, acquiredParameterValue.getHeader().getAcqStamp());
        assertEquals(1L, acquiredParameterValue.getHeader().getCycleStamp());
        assertEquals(10L, ((MapParameterValue) acquiredParameterValue.getValue()).getLong(FIELD_1));
    }

    @Test
    public void shouldGetParameterValueAtTimestampAsNullForEmpty() {
        //given
        mockQuery(EMPTY_DATASET, START_TIME);

        //when
        AcquiredParameterValue acquiredParameterValue = acquiredParameterValuesService
                .getParameterValueAtTimestamp(JAPC_PARAMETER_DEFINITION, START_TIMESTAMP);

        assertNull(acquiredParameterValue);
    }

    //Unfortunately deep mocking fails on some sub-calls, we have to split it into multiple when() calls.
    private void mockQuery(Dataset<Row> dataset, long stop) {
        TimeStartStage<EntityAliasStage<DeviceStage<Dataset<Row>>, Dataset<Row>>, Dataset<Row>> timeStartStage = mock(
                TimeStartStage.class);

        when(parameterQueryMock
                .system(CMW_SYSTEM)).thenReturn(timeStartStage);

        EntityAliasStage<DeviceStage<Dataset<Row>>, Dataset<Row>> entityAliasStage = mock(EntityAliasStage.class);
        TimeEndStage<EntityAliasStage<DeviceStage<Dataset<Row>>, Dataset<Row>>, Dataset<Row>> entityAliasStageDatasetTimeEndStage = mock(
                TimeEndStage.class);

        when(timeStartStage.startTime(TimeUtils.getInstantFromNanos(START_TIME))).thenReturn(
                entityAliasStageDatasetTimeEndStage);
        when(entityAliasStageDatasetTimeEndStage.endTime(TimeUtils.getInstantFromNanos(stop))).thenReturn(
                entityAliasStage);
        DeviceStage<Dataset<Row>> deviceStage = mock(DeviceStage.class, RETURNS_DEEP_STUBS);

        when(entityAliasStage
                .entity()).thenReturn(deviceStage);

        when(deviceStage
                .device(DEV_1)
                .property(PROP_1)
                .build())
                .thenReturn(dataset);

        // This does not work with deep stubs...
        //        when(parameterQueryMock
        //                .system(CMW_SYSTEM)
        //                .startTime(TimeUtils.getInstantFromNanos(START_TIME))
        //                .endTime(TimeUtils.getInstantFromNanos(stop))
        //                .entity()
        //                .device(DEV_1)
        //                .property(PROP_1)
        //                .build())
        //                .thenReturn(dataset);

    }

    //This split of deep mocks is needed, mockito somehow cannot deal with deep stubs that deep (jwozniak)
    private void mockFundamental(Dataset<Row> dataset) {
        VariableAliasStage<Dataset<Row>> variableAliasStage = mock(VariableAliasStage.class, RETURNS_DEEP_STUBS);

        when(dataQueryMock
                .byVariables()
                .system(FundamentalData.FUNDAMENTAL_SYSTEM)
                .startTime(TimeUtils.getInstantFromNanos(START_TIME))
                .endTime(TimeUtils.getInstantFromNanos(STOP_TIME))).thenReturn(variableAliasStage);

        //The XTIM & TGM lsa cycle field name is different, need an alias
        when(variableAliasStage
                .fieldAliases(VIRTUAL_LSA_CYCLE_FIELD, ImmutableList.of(TGM_LSA_CYCLE_FIELD, XTIM_LSA_CYCLE_FIELD))
                .fieldAliases(DESTINATION_FIELD, ImmutableList.of(DESTINATION_FIELD, XTIM_DESTINATION_FIELD))
                .variables(ImmutableList.of("LHC:" + FUNDAMENTAL_SUFFIX))
                .build()).thenReturn(dataset);
    }
}
