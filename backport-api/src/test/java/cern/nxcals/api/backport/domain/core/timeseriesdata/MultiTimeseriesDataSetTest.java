package cern.nxcals.api.backport.domain.core.timeseriesdata;

import cern.cmw.datax.ImmutableEntry;
import cern.nxcals.api.backport.domain.core.metadata.Variable;
import org.junit.jupiter.api.Test;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class MultiTimeseriesDataSetTest {
    @Test
    public void shouldNotAllowNulls() {
        assertThrows(NullPointerException.class, () -> MultiTimeseriesDataSet.builder().series((TimeseriesDataSet) null).build());
    }

    @Test
    public void shouldAllowEmptySeries() {
        MultiColumnTimeseriesDataSet result = MultiTimeseriesDataSet.builder().build();

        assertThat(result.getTimestamps()).isEmpty();
        assertThat(result.getColumnHeaders()).containsExactly("UTC_STAMP");
        assertThat(result.size()).isEqualTo(0);
    }

    @Test
    public void shouldCreateSingleSet() {
        TimeseriesDataSet data = mockDataSet("data", new long[]{1, 2}, new Object[]{1d, 2d});

        MultiColumnTimeseriesDataSet result = MultiTimeseriesDataSet.builder()
                .series(data)
                .build();

        assertThat(result.getTimestamps()).containsExactly(toTimestamp(1), toTimestamp(2));
        assertThat(result.getColumnHeaders()).containsExactly("UTC_STAMP", "data");
        assertThat(result.getRowOfData(toTimestamp(1))).containsExactly(String.valueOf(1d));
        assertThat(result.getRowOfData(toTimestamp(2))).containsExactly(String.valueOf(2d));
        assertThat(result.size()).isEqualTo(2);

        assertThat(result.getRowOfData(toTimestamp(10))).isNotNull();
        assertThat(result.getRowOfData(toTimestamp(10))).containsNull();
    }

    @Test
    public void shouldCreateDoubleSet() {
        TimeseriesDataSet data1 = mockDataSet("dataA", new long[]{1, 2}, new Object[]{1d, 2d});
        TimeseriesDataSet data2 = mockDataSet("dataB", new long[]{2, 3}, new Object[]{"A", "B"});

        MultiColumnTimeseriesDataSet result = MultiTimeseriesDataSet.builder()
                .series(data1)
                .series(data2)
                .build();

        assertThat(result.getTimestamps()).containsExactly(toTimestamp(1), toTimestamp(2), toTimestamp(3));
        assertThat(result.getColumnHeaders()).containsExactly("UTC_STAMP", "dataA", "dataB");
        assertThat(result.getRowOfData(toTimestamp(1))).containsExactly(String.valueOf(1d), null);
        assertThat(result.getRowOfData(toTimestamp(2))).containsExactly(String.valueOf(2d), "A");
        assertThat(result.getRowOfData(toTimestamp(3))).containsExactly(null, "B");
        assertThat(result.size()).isEqualTo(3);

        assertThat(result.getRowOfData(toTimestamp(10))).isNotNull();
        assertThat(result.getRowOfData(toTimestamp(10))).containsNull();
    }

    @Test
    public void shouldWorkForRandomData() throws NoSuchMethodException {
        TimeseriesDataSet data1 = randomDataSet("A", 4000);
        TimeseriesDataSet data2 = randomDataSet("B", 4000);
        TimeseriesDataSet data3 = randomDataSet("C", 4000);

        MultiColumnTimeseriesDataSet result = MultiTimeseriesDataSet.builder()
                .series(data1)
                .series(data2)
                .series(data3)
                .build();

        assertThat(result.getColumnHeaders()).containsExactly("UTC_STAMP", "A", "B", "C");

        for (Timestamp timestamp : result.getTimestamps()) {
            List<String> row = result.getRowOfData(timestamp);
            eq(row.get(0), data1.getTimeseriesData(timestamp));
            eq(row.get(1), data2.getTimeseriesData(timestamp));
            eq(row.get(2), data3.getTimeseriesData(timestamp));
        }
    }

    @Test
    public void shouldWorkForSingleSet() throws NoSuchMethodException {
        TimeseriesDataSet data = randomDataSet("A", 4000);

        MultiColumnTimeseriesDataSet result = MultiTimeseriesDataSet.builder()
                .series(data)
                .series(data)
                .series(data)
                .build();

        for (Timestamp timestamp : result.getTimestamps()) {
            //assertNull(result.getRowOfData(timestamp).get(0));
            assertEquals(result.getRowOfData(timestamp).get(0), data.getTimeseriesData(timestamp).toStringValue());
            assertEquals(result.getRowOfData(timestamp).get(1), data.getTimeseriesData(timestamp).toStringValue());
            assertEquals(result.getRowOfData(timestamp).get(2), data.getTimeseriesData(timestamp).toStringValue());
        }
    }

    private void eq(String expected, TimeseriesData given) throws NoSuchMethodException {
        if (given != null) {
            assertEquals(expected, given.toStringValue());
        } else {
            assertNull(expected);
        }
    }

    private TimeseriesDataSet randomDataSet(String name, int size) {
        final Random random = new Random(System.currentTimeMillis());
        long[] timestamps = random.longs(size, 0, size * 20).sorted().toArray();
        Object[] values = random.doubles(size, 0, 100).boxed().toArray();

        return mockDataSet(name, timestamps, values);
    }

    private TimeseriesDataSet mockDataSet(String name, long[] timestamps, Object[] values) {
        List<TimeseriesData> dataPoints = new ArrayList<>();
        for (int i = 0; i < timestamps.length; i ++) {

            Timestamp timestamp = toTimestamp(timestamps[i]);
            Object value = values[i];
            dataPoints.add(SparkTimeseriesData.of(timestamp, ImmutableEntry.of(name, value)));
        }

        Variable variable = mock(Variable.class);
        when(variable.getVariableName()).thenReturn(name);

        return SparkTimeseriesDataSet.ofSorted(variable, dataPoints);
    }

    private Timestamp toTimestamp(long timestamp) {
        return Timestamp.from(Instant.ofEpochSecond(timestamp));
    }
}