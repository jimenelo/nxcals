package cern.nxcals.api.backport.domain.core.metadata;

import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class VariableSetTest {
    @Test
    public void shouldCollectEmptySet() {
        //given
        Set<Variable> vars = createVariableSet(0);

        //when
        VariableSet variableSet = vars.stream().collect(VariableSet.collector());

        //then
        assertEquals(variableSet.size(), vars.size());

    }

    @Test
    public void shouldCollectNotEmptyVars() {
        //given
        Set<Variable> vars = createVariableSet(100);

        //when
        //parallel for a reason
        VariableSet variableSet = vars.parallelStream().collect(VariableSet.collector());

        //then
        assertEquals(variableSet.size(), vars.size());
    }

    private Set<Variable> createVariableSet(int size) {
        Set<Variable> ret = new HashSet<>();
        for (int i = 0; i < size; ++i) {
            ret.add(Variable.builder().variableName("var" + i).build());
        }
        return ret;
    }
}