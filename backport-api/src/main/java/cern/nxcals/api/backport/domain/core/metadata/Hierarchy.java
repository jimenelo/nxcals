package cern.nxcals.api.backport.domain.core.metadata;

import cern.nxcals.api.backport.domain.util.BackportUtils;
import cern.nxcals.api.domain.HierarchyView;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Optional;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString(onlyExplicitlyIncluded = true)
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Hierarchy implements Serializable, Comparable<Hierarchy> {
    private static final long serialVersionUID = -3189661961765755673L;

    private String nodePath;
    private String description;
    @ToString.Include
    private String hierarchyName;
    @EqualsAndHashCode.Include
    private long hierarchyID;
    private long parentID;
    private int nodeLevel;

    public static Hierarchy from(@NonNull cern.nxcals.api.domain.Hierarchy hierarchy) {
        Hierarchy result = new Hierarchy();
        result.description = hierarchy.getDescription();
        result.hierarchyID = hierarchy.getId();
        result.hierarchyName  = hierarchy.getName();
        result.nodeLevel = hierarchy.getLevel();
        result.parentID = Optional.ofNullable(hierarchy.getParent()).map(HierarchyView::getId).orElse(-1L);
        result.nodePath = BackportUtils.toCalsHierarchyNodePath(hierarchy.getNodePath());
        return result;
    }

    @Override
    public int compareTo(Hierarchy hier) {
        return (nodePath.compareTo(hier.getNodePath()));
    }

    /**
     * @deprecated please use {@link Hierarchy#Hierarchy()} instead
     */
    @Deprecated
    public Hierarchy(String nodePath, String nodeDelimiter) {
        this.nodePath = BackportUtils.toCalsHierarchyNodePath(nodePath);
    }

}
