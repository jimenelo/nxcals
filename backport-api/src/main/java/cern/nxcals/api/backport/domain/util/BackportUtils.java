package cern.nxcals.api.backport.domain.util;

import lombok.experimental.UtilityClass;
import org.apache.commons.lang3.StringUtils;

@UtilityClass
public final class BackportUtils {
    private static final String NXCALS_NODE_PATH_SEPARATOR = cern.nxcals.common.Constants.HIERARCHY_SEPARATOR;
    private static final String CALS_NODE_PATH_SEPARATOR = "->";

    public static String toNxcalsHierarchyNodePath(String nodePath) {
        if (StringUtils.isBlank(nodePath) || nodePath.contains(NXCALS_NODE_PATH_SEPARATOR)) {
            return nodePath;
        }
        return NXCALS_NODE_PATH_SEPARATOR + nodePath.replace(CALS_NODE_PATH_SEPARATOR, NXCALS_NODE_PATH_SEPARATOR);
    }

    public static String toCalsHierarchyNodePath(String nodePath) {
        if (StringUtils.isBlank(nodePath) || nodePath.contains(CALS_NODE_PATH_SEPARATOR)) {
            return nodePath;
        }
        if (nodePath.startsWith(NXCALS_NODE_PATH_SEPARATOR)) {
            nodePath = nodePath.substring(NXCALS_NODE_PATH_SEPARATOR.length());
        }
        return nodePath.replace(NXCALS_NODE_PATH_SEPARATOR, CALS_NODE_PATH_SEPARATOR);
    }

}
