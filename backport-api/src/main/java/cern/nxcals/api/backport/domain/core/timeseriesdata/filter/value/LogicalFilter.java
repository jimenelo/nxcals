package cern.nxcals.api.backport.domain.core.timeseriesdata.filter.value;

import cern.nxcals.api.backport.domain.core.constants.FilterOperand;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;

import java.io.Serializable;

@Getter
@AllArgsConstructor
public class LogicalFilter implements Filter, Serializable {
    private static final long serialVersionUID = 5776511414313536352L;
    @NonNull
    private final FilterOperand operand;

    @Override
    public void accept(FilterVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public String describe() {
        return operand.toString();
    }

    @Override
    public String toString() {
        return describe();
    }
}
