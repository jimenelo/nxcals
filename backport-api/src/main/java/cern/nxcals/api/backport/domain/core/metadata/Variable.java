package cern.nxcals.api.backport.domain.core.metadata;

import cern.nxcals.api.backport.domain.core.constants.VariableDataType;
import cern.nxcals.api.backport.domain.core.constants.VariableType;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.sql.Timestamp;
import java.time.Instant;

@Getter
@Setter
@Builder
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@SuppressWarnings("squid:S00116") // noncompliant field naming
public class Variable implements Serializable, Comparable<Variable> {
    private static final long serialVersionUID = 8867271402092451906L;
    private final VariableDataType variableDataType;
    private final String unit;
    private final String description;
    @EqualsAndHashCode.Include
    private final String system;
    @EqualsAndHashCode.Include
    private final String variableName;

    private final long variableInternalID;
    private Integer vectornumericSize;
    private Long vectornumericContextID;
    private VectornumericElementsSet elementsSet;

    private Long derivationContextId;
    private boolean virtual;
    private String drivingVariable ;

    private VariableIdentity LDBdatasource;
    private VariableIdentity MDBdatasource;
    private Double maxRecsDayLDB;
    private Double maxRecsDayMDB;
    private final Timestamp obsoleteSince;
    //TODO: set when variable history is added
    private final Timestamp validSince;
    private VariableType variableType;
    private int daysInShortTermStorage;

    public static Variable from(cern.nxcals.api.domain.Variable variable) {
        return builder()
                .description(variable.getDescription())
                .unit(variable.getUnit())
                .variableName(variable.getVariableName())
                .variableInternalID(variable.getId())
                .variableDataType(variableDataTypeFrom(variable))
                .variableType(VariableType.ONLINE)
                .system(variable.getSystemSpec().getName())
                .obsoleteSince(obsoleteSinceFrom(variable))
                .build();

    }

    private static Timestamp obsoleteSinceFrom(cern.nxcals.api.domain.Variable variableData) {
        Instant validToStamp = variableData.getConfigs().isEmpty() ?
                null :
                variableData.getConfigs().first().getValidity().getEndTime();
        return validToStamp != null ? Timestamp.from(validToStamp) : null;
    }

    private static VariableDataType variableDataTypeFrom(cern.nxcals.api.domain.Variable variableData) {
        return VariableDataType.from(variableData.getDeclaredType());
    }

    @Override
    public int compareTo(final Variable variable) {
        return variableName.compareTo(variable.getVariableName());
    }

    @Override
    public String toString() {
        return variableName;
    }

    public boolean hasVectornumericElementNames() {
        return vectornumericContextID != null;
    }

    public VectornumericElementsSet getVectornumericElements() {
        return elementsSet;
    }

    public Long getVectorNumericSize() {
        return vectornumericSize == null ? null : (long) vectornumericSize;
    }

    public void setVectornumericElements(final VectornumericElementsSet elementsSet) {
        if (getVariableDataType().equals(VariableDataType.VECTOR_NUMERIC)) {
            this.elementsSet = elementsSet;
        }
    }

    public boolean hasDerivationContext() {
        return derivationContextId != null;
    }

    public String getMetaDataInfo() {
        StringBuilder content = new StringBuilder();

        content.append("DataType: ").append(variableDataType).append("\n");
        content.append("Unit: ").append(getUnit()).append("\n");
        content.append("Description: ").append(getDescription()).append("\n");
        if (isVirtual()) {
            content.append("Virtual variable drived by: ").append(getDrivingVariable()).append("\n");
        }

        return content.toString();
    }

    public boolean isValidBetween(final Timestamp startTime, final Timestamp endTime) {
        if (startTime == null || endTime == null) {
            return false;
        }

        if (startTime.after(endTime)) {
            return false;
        } else if (isObsolete()) {
            return !startTime.after(obsoleteSince);
        }

        return true;
    }

    public Timestamp getValidityAdjustedEndTime(final Timestamp endTime) {
        if (isObsolete() && endTime.after(obsoleteSince)) {
            return obsoleteSince;
        } else {
            return endTime;
        }
    }

    public Timestamp getValidityAdjustedStartTime(final Timestamp startTime) {
        return startTime.before(validSince) ? validSince : startTime;
    }

    public boolean isObsolete() {
        return obsoleteSince != null;
    }







}
