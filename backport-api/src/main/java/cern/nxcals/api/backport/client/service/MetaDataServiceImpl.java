package cern.nxcals.api.backport.client.service;

import cern.cmw.datax.EntryType;
import cern.cmw.datax.ImmutableData;
import cern.nxcals.api.backport.domain.core.constants.VariableDataType;
import cern.nxcals.api.backport.domain.core.metadata.FundamentalData;
import cern.nxcals.api.backport.domain.core.metadata.Hierarchy;
import cern.nxcals.api.backport.domain.core.metadata.HierarchySet;
import cern.nxcals.api.backport.domain.core.metadata.JapcParameterDefinition;
import cern.nxcals.api.backport.domain.core.metadata.SimpleJapcParameter;
import cern.nxcals.api.backport.domain.core.metadata.Variable;
import cern.nxcals.api.backport.domain.core.metadata.VariableList;
import cern.nxcals.api.backport.domain.core.metadata.VariableSet;
import cern.nxcals.api.backport.domain.core.metadata.VectornumericElements;
import cern.nxcals.api.backport.domain.core.metadata.VectornumericElementsSet;
import cern.nxcals.api.backport.domain.util.BackportUtils;
import cern.nxcals.api.converters.SparkRowToImmutableDataConverter;
import cern.nxcals.api.custom.domain.GroupType;
import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.VariableConfig;
import cern.nxcals.api.domain.VariableDeclaredType;
import cern.nxcals.api.extraction.data.builders.DataQuery;
import cern.nxcals.api.extraction.metadata.EntityService;
import cern.nxcals.api.extraction.metadata.GroupService;
import cern.nxcals.api.extraction.metadata.HierarchyService;
import cern.nxcals.api.extraction.metadata.SystemSpecService;
import cern.nxcals.api.extraction.metadata.VariableService;
import cern.nxcals.api.extraction.metadata.queries.Entities;
import cern.nxcals.api.extraction.metadata.queries.Groups;
import cern.nxcals.api.extraction.metadata.queries.Hierarchies;
import cern.nxcals.api.extraction.metadata.queries.Variables;
import cern.nxcals.api.utils.TimeUtils;
import com.github.rutledgepaulv.qbuilders.conditions.Condition;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.functions;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.SortedSet;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static cern.nxcals.api.backport.domain.core.metadata.FundamentalData.DESTINATION_FIELD;
import static cern.nxcals.api.backport.domain.core.metadata.FundamentalData.FUNDAMENTAL_SYSTEM;
import static cern.nxcals.api.backport.domain.core.metadata.FundamentalData.TGM_LSA_CYCLE_FIELD;
import static cern.nxcals.api.backport.domain.core.metadata.FundamentalData.USER_FIELD;
import static cern.nxcals.api.backport.domain.core.metadata.FundamentalData.VIRTUAL_LSA_CYCLE_FIELD;
import static cern.nxcals.api.backport.domain.core.metadata.FundamentalData.XTIM_DESTINATION_FIELD;
import static cern.nxcals.api.backport.domain.core.metadata.FundamentalData.XTIM_LSA_CYCLE_FIELD;
import static cern.nxcals.api.backport.domain.util.FundamentalUtils.concatColumnsToFundamentalVariableName;
import static cern.nxcals.api.custom.domain.CmwSystemConstants.CMW_SYSTEM;
import static cern.nxcals.api.custom.domain.CmwSystemConstants.DEVICE_KEY_NAME;
import static cern.nxcals.api.custom.domain.CmwSystemConstants.PROPERTY_KEY_NAME;
import static cern.nxcals.common.SystemFields.NXC_EXTR_TIMESTAMP;
import static cern.nxcals.common.SystemFields.NXC_EXTR_VALUE;
import static org.apache.commons.collections.CollectionUtils.isEmpty;
import static org.apache.spark.sql.functions.col;

@Slf4j
@RequiredArgsConstructor
class MetaDataServiceImpl implements MetaDataService {

    /**
     * Timestamp for the oldest context present in the database
     */
    private static final String OLDEST_CONTEXT = "2009-01-01T00:00:00.000Z";
    private static final Instant OLDEST_CONTEXT_INSTANT = Instant.parse(OLDEST_CONTEXT);
    private static final Timestamp OLDEST_CONTEXT_TIMESTAMP = Timestamp.from(OLDEST_CONTEXT_INSTANT);

    @NonNull
    private final SystemSpecService systemSpecService;
    @NonNull
    private final EntityService entityService;
    @NonNull
    private final VariableService variableService;
    @NonNull
    private final HierarchyService hierarchyService;
    @NonNull
    private final GroupService groupService;
    @NonNull
    private final SparkSession session;

    @Override
    public VariableSet getVariablesOfDataTypeAttachedToHierarchy(@NonNull Hierarchy hier,
            @NonNull VariableDataType dataType) {
        return hierarchyService.getVariables(hier.getHierarchyID()).stream()
                .map(Variable::from)
                .filter(byDataType(dataType))
                .collect(VariableSet.collector());
    }

    @Override
    public VariableSet getVariablesOfDataTypeWithNameLikePatternAttachedToHierarchy(@NonNull Hierarchy hier,
            @NonNull String pattern,
            @NonNull VariableDataType dataType) {
        String regex = pattern.replace("_", ".").replace("%", ".*?");
        Pattern compiledPattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE | Pattern.DOTALL);

        return hierarchyService.getVariables(hier.getHierarchyID()).stream()
                .filter(byNamePattern(compiledPattern))
                .map(Variable::from)
                .filter(byDataType(dataType))
                .collect(VariableSet.collector());
    }

    private Predicate<Variable> byDataType(VariableDataType type) {
        return t -> VariableDataType.ALL.equals(type) || type.equals(t.getVariableDataType());
    }

    private Predicate<cern.nxcals.api.domain.Variable> byNamePattern(Pattern pattern) {
        return t -> pattern.matcher(t.getVariableName()).matches();
    }

    @Override
    public VariableSet getVariablesOfDataTypeInVariableList(VariableList variableList,
            @NonNull VariableDataType dataType) {
        return variablesFor(variableList).stream()
                .map(Variable::from)
                .filter(byDataType(dataType))
                .collect(VariableSet.collector());
    }

    /**
     * This implementation is limited to the pattern matching ACC:LSA_CYCLE:TIMING_USER only.
     * We can implement any pattern but it will be quite inefficient for selection as in this case
     * we would have to do the concatenation on all data and apply the pattern afterwards (to be tested with all
     * fundamentals to see the performance penalty)
     *
     * @param startTime - Start of the required time window represented by a Java Timestamp object which can be created
     *                  by Timestamp startTime = Timestamp.valueOf("2012-01-01 12:00:00"); Note this should represent the
     *                  local time.
     * @param endTime   - End of the required time window represented by a Java Timestamp object. NOTE the format must be
     *                  the JDBC escape format: yyyy-mm-dd hh:mm:ss
     * @param pattern   - String pattern to match the variable name against % = wildcard.
     * @return
     */
    @Override
    public VariableSet getFundamentalsInTimeWindowWithNameLikePattern(Timestamp startTime, Timestamp endTime,
            String pattern) {

        FundamentalData fundamentalData = FundamentalData.from(pattern);
        Set<cern.nxcals.api.domain.Variable> fundamentalVariables = variableService
                .findAll(Variables.suchThat().variableName().like(fundamentalData.getRealFundamentalName()));
        if (isEmpty(fundamentalVariables)) {
            throw new IllegalArgumentException("Fundamental variables not found for pattern: " + pattern);
        }
        //In NXCALS we have real FUNDAMENTALS based on TGM & XTIM with the name of <ACC>:NXCALS_FUNDAMENTAL
        //First selecting data for all variables matching the accelerator from the pattern.
        //@formatter:off
        return DataQuery.builder(this.session)
                .byVariables()
                .system(FUNDAMENTAL_SYSTEM)
                .startTime(startTime.toInstant())
                .endTime(endTime.toInstant())
                //The XTIM & TGM lsa cycle field & destination field names are different, need an alias
                .fieldAliases(VIRTUAL_LSA_CYCLE_FIELD, ImmutableList.of(TGM_LSA_CYCLE_FIELD, XTIM_LSA_CYCLE_FIELD))
                .fieldAliases(DESTINATION_FIELD, ImmutableList.of(DESTINATION_FIELD, XTIM_DESTINATION_FIELD))
                //Should have fundamental variables created in this for in NXCALS
                .variableLike(fundamentalData.getRealFundamentalName())
                .build()
                .where(conditionFrom(fundamentalData))
                .select(concatColumnsToFundamentalVariableName())
                //We want only distinct variable names here
                .distinct()
                .as(Encoders.STRING())
                //Matching only selected rows and concatenating to obtain the virtual FUNDAMENTAL variable name like in CALS
                .collectAsList()
                .stream()
                .map(this::fromNameToVariable)
                .collect(VariableSet.collector());
        //@formatter:on
    }

    private Column conditionFrom(FundamentalData fundamentalData) {
        return col(VIRTUAL_LSA_CYCLE_FIELD).like(fundamentalData.getLsaCycle())
                .and(col(USER_FIELD).like(fundamentalData.getTimingUser()));
    }

    private Variable fromNameToVariable(String name) {
        return Variable.builder()
                .variableName(name)
                .variableDataType(VariableDataType.FUNDAMENTAL)
                .description("Virtual Fundamental Variable " + name)
                .system(FUNDAMENTAL_SYSTEM)
                .build();
    }

    @Override
    public VariableSet getVariablesOfDataTypeWithNameLikePattern(String pattern, VariableDataType dataType) {
        Condition<Variables> condition = Variables.suchThat().variableName().like(pattern);
        if (dataType.equals(VariableDataType.UNDETERMINED)) {
            condition = condition.and().declaredType().doesNotExist();
        } else if (!dataType.equals(VariableDataType.ALL)) {
            condition = condition.and().declaredType().eq(VariableDeclaredType.valueOf(dataType.name()));
        }
        return findBy(condition);
    }

    @Override
    public VariableSet getVariablesWithNameInListofStrings(List<String> variableNames) {
        return findBy(Variables.suchThat().variableName().in(variableNames));
    }

    private VariableSet findBy(Condition<Variables> condition) {
        return VariableSet.from(variableService.findAll(condition));
    }

    @Override
    public VectornumericElementsSet getVectorElements(Variable variable) {
        return this.getVectorElementsInTimeWindow(variable, OLDEST_CONTEXT_TIMESTAMP, Timestamp.from(Instant.now()));
    }

    @Override
    public VectornumericElementsSet getVectorElementsInTimeWindow(@NonNull Variable variable,
            @NonNull Timestamp minStamp,
            @NonNull Timestamp maxStamp) {
        String contextVariableName = "NXCALS_CONTEXT:" + variable.getVariableInternalID();

        Optional<cern.nxcals.api.domain.Variable> contextVariable = variableService.findOne(Variables.suchThat()
                .systemName().eq(CMW_SYSTEM).and().variableName().eq(contextVariableName));

        if (contextVariable.isPresent()) {
            String nxcTimestamp = NXC_EXTR_TIMESTAMP.getValue();
            String nxcValue = NXC_EXTR_VALUE.getValue();

            List<Row> elementsRowsInTimeWindow = DataQuery.builder(this.session)
                    .byVariables()
                    .system(CMW_SYSTEM)
                    .startTime(minStamp.toInstant())
                    .endTime(maxStamp.toInstant())
                    .variable(contextVariableName)
                    .build()
                    .select(nxcTimestamp, nxcValue)
                    .collectAsList();

            List<Row> latestElementsRowBeforeMinStamp = getLatestElementsRowBeforeTimestamp(minStamp,
                    contextVariableName);
            return rowsToVectornumericElements(
                    Stream.of(elementsRowsInTimeWindow, latestElementsRowBeforeMinStamp).flatMap(Collection::stream)
                            .collect(Collectors.toList()));
        }

        return null;
    }

    private List<Row> getLatestElementsRowBeforeTimestamp(@NonNull Timestamp minTimestamp, String contextVariableName) {
        String nxcTimestamp = NXC_EXTR_TIMESTAMP.getValue();
        String nxcValue = NXC_EXTR_VALUE.getValue();

        if (!minTimestamp.after(OLDEST_CONTEXT_TIMESTAMP)) {
            return Collections.emptyList();
        }

        return DataQuery.builder(this.session)
                .byVariables()
                .system(CMW_SYSTEM)
                .startTime(OLDEST_CONTEXT_INSTANT)
                .endTime(minTimestamp.toInstant().minusNanos(1))
                .variable(contextVariableName)
                .build()
                .select(nxcTimestamp, nxcValue)
                .orderBy(functions.desc(nxcTimestamp))
                .limit(1)
                .collectAsList();
    }

    private VectornumericElementsSet rowsToVectornumericElements(List<Row> rows) {
        VectornumericElementsSet elementsSet = new VectornumericElementsSet();

        for (Row row : rows) {
            VectornumericElements elements = new VectornumericElements();

            ImmutableData data = SparkRowToImmutableDataConverter.convert(row);

            String[] names = data.getEntry(NXC_EXTR_VALUE.getValue()).getAs(EntryType.STRING_ARRAY);

            if (names != null) {
                int index = 0;
                for (String name : names) {
                    elements.addElement(index++, name);
                }
            }

            elementsSet.addVectornumericElements(
                    TimeUtils.getTimestampFromNanos(
                            data.getEntry(NXC_EXTR_TIMESTAMP.getValue()).getAs(EntryType.INT64)),
                    elements);
        }

        return elementsSet;
    }

    @Override
    public HierarchySet getAllHierarchies() {
        return hierarchyService.findAll(Hierarchies.suchThat().id().exists()).stream()
                .map(Hierarchy::from)
                .collect(HierarchySet.collector());
    }

    @Override
    public HierarchySet getHierarchyChildNodes(@NonNull Hierarchy parentHier) {
        return hierarchyService.findAll(Hierarchies.suchThat().parentId().eq(parentHier.getHierarchyID())).stream()
                .map(Hierarchy::from)
                .collect(HierarchySet.collector());
    }

    @Override
    public Hierarchy getHierarchyForNodePath(@NonNull String nodePath) {
        String nxcalsNodePath = BackportUtils.toNxcalsHierarchyNodePath(nodePath);
        return hierarchyService.findOne(Hierarchies.suchThat().path().eq(nxcalsNodePath))
                .map(Hierarchy::from)
                .orElse(null);
    }

    @Override
    public HierarchySet getTopLevelHierarchies() {
        return hierarchyService.findAll(Hierarchies.suchThat().areTopLevel()).stream()
                .map(Hierarchy::from)
                .collect(HierarchySet.collector());
    }

    @Override
    public HierarchySet getHierarchiesForVariable(Variable variable) {
        return hierarchyService.getHierarchiesForVariable(variable.getVariableInternalID()).stream()
                .map(Hierarchy::from)
                .collect(HierarchySet.collector());
    }

    @Override
    public JapcParameterDefinition getJapcParameterDefinitionForDeviceAndPropertyName(String deviceName,
            String propertyName) {
        //Those calls are to make sure that such entity exists in our system.
        SystemSpec systemSpec = this.systemSpecService.findByName(CMW_SYSTEM)
                .orElseThrow(() -> new IllegalArgumentException("No such system: " + CMW_SYSTEM));

        Entity entity = this.entityService
                .findOne(Entities.suchThat().systemName().eq(CMW_SYSTEM).and().keyValues()
                        .eq(systemSpec, ImmutableMap.of(DEVICE_KEY_NAME, deviceName, PROPERTY_KEY_NAME, propertyName)))
                .orElseThrow(() -> new IllegalArgumentException(
                        "No such entity: " + deviceName + "/" + propertyName + " in system: " + CMW_SYSTEM));

        return JapcParameterDefinition.builder().deviceName(deviceName).propertyName(propertyName)
                .fieldMap(getVariablesForFieldsMap(entity)).build();
    }

    private Map<String, Set<Variable>> getVariablesForFieldsMap(Entity entity) {
        Set<cern.nxcals.api.domain.Variable> variables = this.variableService
                .findAll(Variables.suchThat().configsEntityId().eq(entity.getId()));
        Map<String, Set<Variable>> ret = new HashMap<>();

        for (cern.nxcals.api.domain.Variable var : variables) {
            for (VariableConfig config : var.getConfigs()) {
                if (config.getFieldName() != null) {
                    Set<Variable> set = ret.computeIfAbsent(config.getFieldName(), key -> new HashSet<>());
                    set.add(Variable.from(var));
                }
            }
        }

        return ret;
    }

    @Override
    public VariableSet getVariablesOfDataTypeWithNameLikePatternInVariableList(VariableList variableList,
            String pattern, @NonNull VariableDataType dataType) {
        String regex = pattern.replace("_", ".").replace("%", ".*?");
        Pattern compiledPattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
        return variablesFor(variableList).stream()
                .filter(byNamePattern(compiledPattern))
                .map(Variable::from)
                .filter(byDataType(dataType))
                .collect(VariableSet.collector());
    }

    @Override
    public List<String> getAllDeviceNames() {
        String query = String.format("%%\"%s\": %%", DEVICE_KEY_NAME);
        return entityService.findAll(Entities.suchThat().systemName().eq(CMW_SYSTEM).and().keyValues().like(query))
                .stream()
                .map(Entity::getEntityKeyValues)
                .map(t -> t.get(DEVICE_KEY_NAME))
                .filter(Objects::nonNull)
                .map(Object::toString)
                .collect(Collectors.toList());
    }

    @Override
    public List<String> getPropertyNamesForDeviceName(@NonNull String deviceName) {
        String query = String.format("%%\"%s\": \"%s\"%%\"%s\"%%", DEVICE_KEY_NAME, deviceName, PROPERTY_KEY_NAME);
        return entityService.findAll(Entities.suchThat().systemName().eq(CMW_SYSTEM).and().keyValues().like(query))
                .stream()
                .map(Entity::getEntityKeyValues)
                .map(t -> t.get(PROPERTY_KEY_NAME))
                .filter(Objects::nonNull)
                .map(Object::toString)
                .collect(Collectors.toList());
    }

    @Override
    public Collection<SimpleJapcParameter> getJapcDefinitionFor(@NonNull VariableList variableList) {
        List<VariableConfig> configs = currentConfigsFor(variablesFor(variableList));

        if (configs.isEmpty()) {
            return Collections.emptySet();
        }

        Condition<Entities> condition = Entities.suchThat().systemName().eq(CMW_SYSTEM).and()
                .id().in(configs.stream().map(VariableConfig::getEntityId).collect(Collectors.toSet()));

        Map<Long, Entity> entitiesById = entityService.findAll(condition).stream()
                .collect(Collectors.toMap(Entity::getId, Function.identity()));

        List<Pair<VariableConfig, Entity>> configsWithEntities = configs.stream()
                .filter(e -> entitiesById.containsKey(e.getEntityId()))
                .map(e -> Pair.of(e, entitiesById.get(e.getEntityId())))
                .collect(Collectors.toList());

        return toJapcParameters(configsWithEntities);
    }

    private List<VariableConfig> currentConfigsFor(Set<cern.nxcals.api.domain.Variable> variables) {
        return variables.stream()
                .map(cern.nxcals.api.domain.Variable::getConfigs)
                .filter(CollectionUtils::isNotEmpty)
                .map(SortedSet::first)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    private Collection<SimpleJapcParameter> toJapcParameters(Collection<Pair<VariableConfig, Entity>> pairs) {
        return pairs.stream().map(pair -> SimpleJapcParameter.from(pair.getLeft(), pair.getRight()))
                .collect(Collectors.toSet());
    }

    private Set<cern.nxcals.api.domain.Variable> variablesFor(VariableList variableList) {
        Condition<Groups> condition = Groups.suchThat().label()
                .in(GroupType.GROUP.toString(), GroupType.SNAPSHOT.toString()).and()
                .id().eq(variableList.getVariableListId());
        Map<String, Set<cern.nxcals.api.domain.Variable>> varsByAssociation = groupService.findOne(condition)
                .map(g -> groupService.getVariables(g.getId()))
                .orElse(Collections.emptyMap());
        return Collections.unmodifiableSet(varsByAssociation.values().stream().flatMap(Collection::stream)
                .collect(Collectors.toSet()));
    }
}
