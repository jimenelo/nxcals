package cern.nxcals.api.backport.domain.core.snapshot;

import com.google.common.annotations.VisibleForTesting;
import lombok.AllArgsConstructor;
import org.joda.time.DateTime;

import java.time.DayOfWeek;
import java.util.function.Supplier;

@AllArgsConstructor
public enum SimplePriorTime implements PriorTime {

    //@formatter:off
    NOW("Now", DateTime::now),
    START_OF_HOUR("Start of hour", () -> DateTime.now().hourOfDay().roundFloorCopy()),
    START_OF_DAY("Start of day", () -> DateTime.now().withTimeAtStartOfDay()),
    START_OF_MONTH("Start of month", () -> DateTime.now().monthOfYear().roundFloorCopy()),
    START_OF_YEAR("Start of year", () -> DateTime.now().year().roundFloorCopy()),

    PREVIOUS_MONDAY("Previous Monday", () -> previousWeekDay(DateTime.now(), DayOfWeek.MONDAY)),
    PREVIOUS_TUESDAY("Previous Tuesday", () -> previousWeekDay(DateTime.now(), DayOfWeek.TUESDAY)),
    PREVIOUS_WEDNESDAY("Previous Wednesday", () -> previousWeekDay(DateTime.now(), DayOfWeek.WEDNESDAY)),
    PREVIOUS_THURSDAY("Previous Thursday", () -> previousWeekDay(DateTime.now(), DayOfWeek.THURSDAY)),
    PREVIOUS_FRIDAY("Previous Friday", () -> previousWeekDay(DateTime.now(), DayOfWeek.FRIDAY)),
    PREVIOUS_SATURDAY("Previous Saturday", () -> previousWeekDay(DateTime.now(), DayOfWeek.SATURDAY)),
    PREVIOUS_SUNDAY("Previous Sunday", () -> previousWeekDay(DateTime.now(), DayOfWeek.SUNDAY));
    //@formatter:on

    private final String description;
    private final Supplier<DateTime> time;

    @Override
    public String description() {
        return this.description;
    }

    @Override
    public DateTime time() {
        return this.time.get();
    }

    public static PriorTime from(String desc) {
        for (PriorTime pT : values()) {
            if (pT.description().equals(desc)) {
                return pT;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return this.description();
    }

    @VisibleForTesting
    static DateTime previousWeekDay(DateTime refDateTime, DayOfWeek dayOfWeek) {
        if (dayOfWeek.getValue() > refDateTime.getDayOfWeek()) {
            return refDateTime.withDayOfWeek(dayOfWeek.getValue()).minusDays(7).withTimeAtStartOfDay();
        }
        return refDateTime.withDayOfWeek(dayOfWeek.getValue()).withTimeAtStartOfDay();
    }
}
