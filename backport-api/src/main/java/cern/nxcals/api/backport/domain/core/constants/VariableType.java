package cern.nxcals.api.backport.domain.core.constants;

import cern.nxcals.api.backport.domain.core.metadata.Variable;

/**
 * Holds available {@link Variable} types.
 */
public enum VariableType {
    /**
     * Defines a variable for which data are captured as a direct result of live measurements.
     */
    ONLINE,

    /**
     * Defines an online variable which may have versioned data.
     */
    ONLINE_VERSIONED,

    /**
     * Defines a variable which comes from an offline data source like analysis or statistics, and not real time
     * measurements.
     */
    OFFLINE
}
