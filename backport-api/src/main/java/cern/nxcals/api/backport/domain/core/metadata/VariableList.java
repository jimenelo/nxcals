package cern.nxcals.api.backport.domain.core.metadata;

import cern.nxcals.api.domain.Group;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class VariableList implements Serializable {
    private static final long serialVersionUID = -5105673288688739024L;

    @Getter(AccessLevel.NONE)
    private final Map<String, Boolean> groupPrivileges = new HashMap<>();
    private long variableListId;
    private String description;
    private String variableListName;
    private String userName;
    private boolean visibleToPublic;
    private String system;

    public static VariableList from(Group group) {
        VariableList result = new VariableList();
        result.setVariableListName(group.getName());
        result.setDescription(group.getDescription());
        result.setUserName(group.getOwner());
        result.setVisibleToPublic(group.isVisible());
        result.setVariableListId(group.getId());
        result.setSystem(group.getSystemSpec().getName());
        return result;
    }

    public VariableList(int listId, String description, String listName) {
        this(listId, description, listName, null, false, null);
    }

    /**
     * @deprecated this functionality has been fully removed
     */
    @Deprecated
    public String getStamp() {
        throw new UnsupportedOperationException("Timestamp on VariableList deprecated");
    }

    /**
     * @deprecated this functionality has been fully removed
     */
    @Deprecated
    public void setStamp(String stamp) {
        throw new UnsupportedOperationException("Timestamp on VariableList deprecated");
    }

    @Override
    public String toString() {
        return variableListName;
    }

    public boolean hasGroupWritingPermissions(String groupName) {
        return groupPrivileges.getOrDefault(groupName, false);
    }

    public String[] getGroups() {
        return groupPrivileges.keySet().toArray(new String[0]);
    }

    public List<String> getReadingGroups() {
        return groupPrivileges.keySet().stream().filter(t -> !hasGroupWritingPermissions(t)).collect(Collectors.toList());
    }

    public List<String> getWritingGroups() {
        return groupPrivileges.keySet().stream().filter(this::hasGroupWritingPermissions).collect(Collectors.toList());
    }

    public void addGroup(String groupName, boolean writePermissions) {
        groupPrivileges.put(groupName, writePermissions);
    }

    public void removeGroup(String groupName) {
        groupPrivileges.remove(groupName);
    }
}
