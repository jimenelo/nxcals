package cern.nxcals.api.backport.domain.core.timeseriesdata.spi;

import cern.nxcals.api.backport.domain.core.constants.LoggingTimeZone;
import cern.nxcals.api.backport.domain.core.constants.VariableDataType;
import cern.nxcals.api.backport.domain.core.metadata.TimeseriesDataSetMetaData;
import cern.nxcals.api.backport.domain.core.metadata.Variable;
import lombok.EqualsAndHashCode;
import org.apache.spark.ml.feature.Bucketizer;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static cern.nxcals.common.SystemFields.NXC_EXTR_VALUE;
import static org.apache.spark.sql.functions.max;
import static org.apache.spark.sql.functions.min;

@EqualsAndHashCode(callSuper = true)
public class DataDistributionBinSet extends ArrayList<DataDistributionBin> implements Serializable {
    private static final String BUCKETS_COLUMN = "BUCKETS";
    private static final String MIN_VALUE_ALIAS = "minValue";
    private static final String MAX_VALUE_ALIAS = "maxValue";
    private static final long serialVersionUID = -4189138449388207065L;
    private final TimeseriesDataSetMetaData metaData = new TimeseriesDataSetMetaData();
    private double minValue;
    private double width;
    private int binsNumber;

    public DataDistributionBinSet(Variable variable, Dataset<Row> dataset, int binsNumber) {
        validateBinsNumber(binsNumber);
        this.metaData.setVariable(variable);
        this.binsNumber = binsNumber;
        if (dataset.isEmpty() || !VariableDataType.NUMERIC.equals(variable.getVariableDataType())) {
            createEmptyBins();
        } else {
            Row minMax = (dataset.select(min(NXC_EXTR_VALUE.getValue())
                    .alias(MIN_VALUE_ALIAS), max(NXC_EXTR_VALUE.getValue()).alias(MAX_VALUE_ALIAS))).collectAsList().get(0);
            this.minValue = Double.parseDouble(minMax.getAs(MIN_VALUE_ALIAS).toString());
            double maxValue = Double.parseDouble(minMax.getAs(MAX_VALUE_ALIAS).toString());
            this.width = (maxValue - minValue) / binsNumber;
            Dataset<Row> binsDataset = divideDataToBins(dataset);
            createBins(binsDataset);
        }
    }

    private void createBins(Dataset<Row> datasetWithBuckets) {
        List<Row> rows = datasetWithBuckets.collectAsList();
        for (int i = 0; i < rows.size(); i++) {
            double bottomLimit = minValue + i * width;
            double topLimit = bottomLimit + width;
            long count = rows.get(i).getLong(1);
            addDataDistributionBin(new DataDistributionBin(count, bottomLimit, topLimit));
        }
    }

    private void createEmptyBins() {
        for (int i = 0; i < binsNumber; i++) {
            add(new DataDistributionBin(0, 0d, 0d));
        }
    }

    private Dataset<Row> divideDataToBins(Dataset<Row> inputDataset) {
        double inf = Double.MAX_VALUE;
        double[] splits = new double[binsNumber + 2];
        splits[0] = -inf;
        splits[binsNumber + 1] = inf;
        for (int i = 0; i <= binsNumber; i++) {
            splits[i + 1] = minValue + width * i;
        }

        Bucketizer bucketizer = new Bucketizer()
                .setSplits(splits).setInputCol(NXC_EXTR_VALUE.getValue()).setOutputCol(BUCKETS_COLUMN);
        Dataset<Row> dataset = bucketizer.transform(inputDataset);
        return dataset.select(BUCKETS_COLUMN).sort(BUCKETS_COLUMN)
                .groupBy(BUCKETS_COLUMN).count();
    }

    public int getBinsCount() {
        return size();
    }

    public DataDistributionBin getBin(int i) {
        return get(i);
    }

    public void addDataDistributionBin(DataDistributionBin dataDistributionBin) {
        add(dataDistributionBin);
    }

    public Variable getVariable() {
        return metaData.getVariable();
    }

    public String getVariableName() {
        return metaData.getVariableName();
    }

    public String getMetaDataInfo(LoggingTimeZone timeZone) {
        return metaData.getMetaDataInfo(timeZone);
    }

    public TimeseriesDataSetMetaData getMetaData() {
        return metaData;
    }

    public void setVariable(Variable variable) {
        metaData.setVariable(variable);
    }

    public void setVariableName(String variableName) {
        metaData.setVariableName(variableName);
    }

    public void setDataType(VariableDataType dataType) {
        metaData.setDataType(dataType);
    }

    private void validateBinsNumber(int binsNumber) {
        if (binsNumber <= 0) {
            throw new IllegalArgumentException("Number of bins must be greater than 0!");
        }
    }
}
