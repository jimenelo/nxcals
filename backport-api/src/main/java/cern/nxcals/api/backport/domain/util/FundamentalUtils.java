package cern.nxcals.api.backport.domain.util;

import cern.nxcals.api.backport.domain.core.metadata.FundamentalData;
import cern.nxcals.api.backport.domain.core.metadata.VariableSet;
import cern.nxcals.api.extraction.data.builders.DataQuery;
import com.google.common.collect.ImmutableList;
import lombok.NonNull;
import lombok.experimental.UtilityClass;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.StructField;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static cern.nxcals.api.backport.domain.core.metadata.FundamentalData.DESTINATION_FIELD;
import static cern.nxcals.api.backport.domain.core.metadata.FundamentalData.FUNDAMENTAL_NAME_FIELD;
import static cern.nxcals.api.backport.domain.core.metadata.FundamentalData.FUNDAMENTAL_SEPARATOR;
import static cern.nxcals.api.backport.domain.core.metadata.FundamentalData.FUNDAMENTAL_SUFFIX;
import static cern.nxcals.api.backport.domain.core.metadata.FundamentalData.TGM_LSA_CYCLE_FIELD;
import static cern.nxcals.api.backport.domain.core.metadata.FundamentalData.USER_FIELD;
import static cern.nxcals.api.backport.domain.core.metadata.FundamentalData.VIRTUAL_LSA_CYCLE_FIELD;
import static cern.nxcals.api.backport.domain.core.metadata.FundamentalData.XTIM_DESTINATION_FIELD;
import static cern.nxcals.api.backport.domain.core.metadata.FundamentalData.XTIM_LSA_CYCLE_FIELD;
import static cern.nxcals.common.SystemFields.NXC_EXTR_TIMESTAMP;
import static cern.nxcals.common.SystemFields.NXC_EXTR_VARIABLE_NAME;
import static org.apache.spark.sql.functions.col;
import static org.apache.spark.sql.functions.concat_ws;
import static org.apache.spark.sql.functions.regexp_replace;
import static org.apache.spark.sql.types.DataTypes.LongType;
import static org.apache.spark.sql.types.DataTypes.StringType;
import static org.apache.spark.sql.types.DataTypes.createStructField;
import static org.apache.spark.sql.types.DataTypes.createStructType;

@UtilityClass
public class FundamentalUtils {

    /**
     * The idea here is to convert a virtual fundamental(s) (created by the MetaDataService) that has a name of ACC:LSA_CYCLE:TIMING_USER
     * to the where condition that is used on the real fundamental.
     * Each virtual fundamental creates such a where condition and all of them are OR-ed for the final query.
     *
     * @param startTime
     * @param endTime
     * @param virtualFundamentals
     * @return
     */

    public static Dataset<Row> createDatasetFromFundamentals(
            @NonNull Timestamp startTime,
            @NonNull Timestamp endTime,
            @NonNull VariableSet virtualFundamentals,
            @NonNull SparkSession sparkSession) {

        if (virtualFundamentals.size() == 0) {

            return sparkSession.createDataFrame(new ArrayList<>(), createStructType(
                    new StructField[] {
                            createStructField(FundamentalData.NXCALS_FUNDAMENTAL_TIMESTAMP, LongType, false),
                            createStructField(DESTINATION_FIELD, StringType, false),
                            createStructField(FUNDAMENTAL_NAME_FIELD, StringType, false) }));
        }
        return processDataSetCreation(startTime, endTime, virtualFundamentals, DataQuery.builder(sparkSession));
    }

    /**
     * @deprecated (always pass SparkSession parameter instead of DataQuery builder, additional validation
     *can be performed)
     */
    @Deprecated
    public static Dataset<Row> createDatasetFromFundamentals(
            @NonNull Timestamp startTime,
            @NonNull Timestamp endTime,
            @NonNull VariableSet virtualFundamentals,
            @NonNull DataQuery builder) {

        return processDataSetCreation(startTime, endTime, virtualFundamentals, builder);
    }

    public static Column concatColumnsToFundamentalVariableName() {
        return concat_ws(FUNDAMENTAL_SEPARATOR, regexp_replace(
                        col(NXC_EXTR_VARIABLE_NAME.getValue()),
                        FUNDAMENTAL_SEPARATOR + FUNDAMENTAL_SUFFIX, ""),
                col(VIRTUAL_LSA_CYCLE_FIELD),
                col(USER_FIELD));
    }

    private static Dataset<Row> processDataSetCreation(
            @NonNull Timestamp startTime,
            @NonNull Timestamp endTime,
            @NonNull VariableSet virtualFundamentals,
            @NonNull DataQuery builder) {
        //Set used for uniqueness, used in DataQuery to pre-select variables
        Set<String> allRealFundNames = new HashSet<>();
        Optional<Column> condition = virtualFundamentals.getVariableNames().stream().map(virtualName -> {
            FundamentalData fundData = FundamentalData.from(virtualName);
            String realFundName = fundData.getRealFundamentalName();
            //store real name
            allRealFundNames.add(realFundName);
            return col(NXC_EXTR_VARIABLE_NAME.getValue()).equalTo(realFundName)
                    .and(col(VIRTUAL_LSA_CYCLE_FIELD).equalTo(fundData.getLsaCycle())
                            .and(col(USER_FIELD).equalTo(fundData.getTimingUser())));
        }).reduce(Column::or); //OR all conditions into one by reducing (no other way in Java Spark?)
        //@formatter:off
        Dataset<Row> dataset = builder.byVariables()
                .system(FundamentalData.FUNDAMENTAL_SYSTEM)
                .startTime(startTime.toInstant())
                .endTime(endTime.toInstant())
                //The XTIM & TGM lsa cycle field name is different, need an alias
                .fieldAliases(VIRTUAL_LSA_CYCLE_FIELD, ImmutableList.of(TGM_LSA_CYCLE_FIELD, XTIM_LSA_CYCLE_FIELD))
                .fieldAliases(DESTINATION_FIELD, ImmutableList.of(DESTINATION_FIELD, XTIM_DESTINATION_FIELD))
                .variables(ImmutableList.copyOf(allRealFundNames))
                .build();

        return dataset.where(condition.orElseThrow(() -> new IllegalArgumentException("Empty fundamental data set")))
                .withColumn(FUNDAMENTAL_NAME_FIELD, concatColumnsToFundamentalVariableName())
                .select(col(NXC_EXTR_TIMESTAMP.getValue()).alias(FundamentalData.NXCALS_FUNDAMENTAL_TIMESTAMP),
                        col(DESTINATION_FIELD), col(FUNDAMENTAL_NAME_FIELD));
        //@formatter:on

    }
}
