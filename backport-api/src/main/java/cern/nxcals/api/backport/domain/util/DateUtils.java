package cern.nxcals.api.backport.domain.util;

import cern.nxcals.api.backport.domain.core.constants.LoggingTimeZone;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;

@Slf4j
@UtilityClass
public class DateUtils {
    private static final String INSTRUMENTATION_DATE_FORMAT_PATTERN = "yyMMdd HH:mm";
    public static final String DATE_FORMAT_PATTERN = "yyyy-MM-dd HH:mm:ss";
    public static final String TIMESTAMP_FORMAT_PATTERN = "yyyy-MM-dd HH:mm:ss.SSS";
    public static final TimeZone UTC_TIMEZONE = TimeZone.getTimeZone("GMT");
    public static final TimeZone LOCAL_TIMEZONE = TimeZone.getTimeZone("Europe/Paris");

    public static String formatTimestamp(Timestamp timestamp, LoggingTimeZone timeZone) {
        return timeZone.equals(LoggingTimeZone.UTC_TIME) ? formatAsUTCTime(timestamp) : formatAsLocalTime(timestamp);
    }

    public static String formatAsLocalDate(Date date) {
        return format(date, DATE_FORMAT_PATTERN, LOCAL_TIMEZONE);
    }

    public static String formatAsLocalTime(Timestamp time) {
        return format(time, TIMESTAMP_FORMAT_PATTERN, LOCAL_TIMEZONE);
    }

    public static String formatAsUTCDate(Date date) {
        return format(date, DATE_FORMAT_PATTERN, UTC_TIMEZONE);
    }

    public static String formatAsUTCTime(Timestamp time) {
        return format(time, TIMESTAMP_FORMAT_PATTERN, UTC_TIMEZONE);
    }

    private static String format(Date date, String dateFormatPattern, TimeZone localTimezone) {
        DateFormat dateFormat = new SimpleDateFormat(dateFormatPattern);
        dateFormat.setTimeZone(localTimezone);
        return dateFormat.format(date);
    }

    public static String formatForInstrumentation(Timestamp time) {
        return new SimpleDateFormat(INSTRUMENTATION_DATE_FORMAT_PATTERN).format(time);
    }

    public static List<Date> getDaylightSavingChanges(Date start, Date end) {
        List<Date> changesDST = new ArrayList<>();

        Calendar startCal = new GregorianCalendar();
        Calendar endCal = new GregorianCalendar();

        startCal.setTime(start);
        endCal.setTime(end);
        TimeZone tz = startCal.getTimeZone();
        log.info("TimeZone: " + tz.toString());

        boolean inDST = tz.inDaylightTime(startCal.getTime());
        log.info(startCal.getTime() + " in DST: " + inDST);

        // Set the minutes and seconds fields to 0
        startCal.set(Calendar.MINUTE, 0);
        startCal.set(Calendar.SECOND, 0);

        while (startCal.before(endCal)) {
            startCal.add(Calendar.HOUR_OF_DAY, 1);
            if (tz.inDaylightTime(startCal.getTime()) != inDST) {
                log.info(startCal.getTime() + " DST Change");
                changesDST.add(startCal.getTime());
                inDST = tz.inDaylightTime(startCal.getTime());
            }
        }

        return changesDST;
    }

    /**
     * Returns a String formatted according to the given timestamp
     *
     * @param timestamp
     * @param timeZone
     * @return A formatted string
     */
    public static String getFormattedStamp(Timestamp timestamp, LoggingTimeZone timeZone) {
        if (timestamp == null) {
            log.error("Cannot format a null Timestamp!");
            return null;
        }

        if (LoggingTimeZone.LOCAL_TIME.equals(timeZone)) {
            return formatAsLocalTime(timestamp);
        } else if (LoggingTimeZone.UTC_TIME.equals(timeZone)) {
            return formatAsUTCTime(timestamp);
        }
        log.error("Unsupported timeZone for formatting: " + timeZone);
        return null;
    }

    public static Calendar newUTCCalendar() {
        return new GregorianCalendar(UTC_TIMEZONE);
    }
}
