package cern.nxcals.api.backport.domain.core;

import cern.nxcals.api.backport.domain.core.constants.LoggingTimeZone;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class TimeWindowSet implements Iterable<TimeWindow>, Serializable {
    private static final long serialVersionUID = -6086750438995898759L;

    private List<TimeWindow> timeWindows;

    @SuppressWarnings("squid:S1172") // unused parameter
    public TimeWindowSet(LoggingTimeZone timeZone) {
        timeWindows = new ArrayList<>();
    }

    public void addTimeWindow(TimeWindow timeWindow) {
        timeWindows.add(timeWindow);
        Collections.sort(timeWindows);
    }

    public int count() {
        return timeWindows.size();
    }

    public TimeWindow getTimeWindowAtIndex(int index) {
        return timeWindows.get(index);
    }

    @Override
    public Iterator<TimeWindow> iterator() {
        return timeWindows.iterator();
    }

    public void removeAllTimeWindows() {
        timeWindows.clear();
    }

    public void removeTimeWindow(int idx) {
        timeWindows.remove(idx);
    }

    public void removeTimeWindow(TimeWindow timeWindow) {
        timeWindows.remove(timeWindow);
    }
}
