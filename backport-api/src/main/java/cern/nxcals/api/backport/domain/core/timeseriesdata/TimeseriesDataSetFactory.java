package cern.nxcals.api.backport.domain.core.timeseriesdata;

import cern.nxcals.api.backport.domain.core.metadata.Variable;

import java.io.Serializable;
import java.util.Collections;

/**
 * @author Daniel Teixeira Represents a TimeseriesDataSet that contains Multiple TimeseriesData for a Variable object.
 */

public class TimeseriesDataSetFactory implements Serializable {

    private static final long serialVersionUID = 488426378486715044L;

    /**
     * Returns an empty timeseries data set corresponding to a variable
     * 
     * @param variableName The variable name
     * @return An empty timeseries data set
     */
    public static TimeseriesDataSet newTimeseriesDataSet(String variableName) {
        return SparkTimeseriesDataSet.of(Variable.builder().variableName(variableName).build(), Collections.emptyList());
    }
}
