package cern.nxcals.api.backport.domain.core.metadata;

import lombok.Data;
import lombok.NonNull;

@Data
public class FundamentalData {
    public static final String FUNDAMENTAL_SEPARATOR = ":";
    public static final String FUNDAMENTAL_SUFFIX = "NXCALS_FUNDAMENTAL";
    public static final String FUNDAMENTAL_SYSTEM = "CMW";
    public static final String VIRTUAL_LSA_CYCLE_FIELD = "LSA_CYCLE";
    public static final String TGM_LSA_CYCLE_FIELD = "__LSA_CYCLE__";
    public static final String XTIM_LSA_CYCLE_FIELD = "lsaCycleName";
    public static final String USER_FIELD = "USER";
    public static final String NXCALS_FUNDAMENTAL_TIMESTAMP = "NXCALS_FUNDAMENTAL_TIMESTAMP";
    public static final String DESTINATION_FIELD = "DEST";
    public static final String FUNDAMENTAL_NAME_FIELD = "FUNDAMENTAL_NAME";
    public static final String XTIM_DESTINATION_FIELD = "DYN_DEST";


    private final String accelerator;
    private final String lsaCycle;
    private final String timingUser;

    //Accepts only A:B:C patterns.
    public static FundamentalData from(@NonNull String pattern) {
        String[] split = pattern.split(FUNDAMENTAL_SEPARATOR);
        if (split.length != 3) {
            throw new IllegalArgumentException("Wrong pattern: " + pattern
                    + ", please provide a pattern of a format <acc-pattern>:<lsa-cycle-pattern>:<timing-user-pattern>");
        }
        return new FundamentalData(split[0], split[1], split[2]);
    }

    public String getRealFundamentalName() {
        return accelerator + FUNDAMENTAL_SEPARATOR + FUNDAMENTAL_SUFFIX;
    }
}
