package cern.nxcals.api.backport.domain.core.snapshot;

import cern.nxcals.api.backport.domain.core.metadata.Variable;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public class SimpleSnapshotVariableBean {
    @Getter
    private final String propertyName;
    private final Variable variable;

    public long getVariableId() {
        return variable == null ? -1 : variable.getVariableInternalID();
    }

    @Override
    public String toString() {
        return variable == null ? super.toString() : variable.toString();
    }
}
