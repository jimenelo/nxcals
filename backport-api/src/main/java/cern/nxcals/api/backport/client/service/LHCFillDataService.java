package cern.nxcals.api.backport.client.service;

import cern.nxcals.api.backport.domain.core.constants.BeamModeValue;
import cern.nxcals.api.backport.domain.core.filldata.LHCFill;
import cern.nxcals.api.backport.domain.core.filldata.LHCFillSet;

import java.sql.Timestamp;

public interface LHCFillDataService {

    /**
     * Gets the LHC Fills which occurred within the given time window. Each LHC Fill will NOT contain the beams modes
     * which have occurred within the Fill.
     * 
     * @see #getLHCFillsAndBeamModesInTimeWindow(Timestamp, Timestamp)
     * @param startTime - The start of the time window within which to search for LHC Fills
     * @param endTime - The end of the time window within which to search for LHC Fills
     * @return an <code>LHCFillSet</code> containing all LHC Fills which occurred within the given time window
     */
    LHCFillSet getLHCFillsInTimeWindow(Timestamp startTime, Timestamp endTime);

    /**
     * Gets the LHC Fills which occurred within the given time window. Each LHC Fill will contain all beams modes which
     * have occurred within the Fill.
     * 
     * @see #getLHCFillsInTimeWindow(Timestamp, Timestamp)
     * @param startTime - The start of the time window within which to search for LHC Fills
     * @param endTime - The end of the time window within which to search for LHC Fills
     * @return an LHCFillSet containing all LHC Fills which match the search criteria
     */
    LHCFillSet getLHCFillsAndBeamModesInTimeWindow(Timestamp startTime, Timestamp endTime);

    /**
     * Gets the LHC Fills which occurred within the given time window, provided that they contain one or more of the
     * given beam modes. Each LHC Fill will contain all beams modes which have occurred within the fill.
     *
     * @see #getLHCFillsInTimeWindowContainingBeamModes(Timestamp, Timestamp, BeamModeValue[])
     * @param startTime - The start of the time window within which to search for LHC Fills
     * @param endTime - The end of the time window within which to search for LHC Fills
     * @param beamModes - The beam modes by which LHC Fills in the time window will be filtered by (i.e. only Fills
     *            containing one or more of the beam modes will be returned).
     * @return an <code>LHCFillSet</code> containing all LHC Fills which match the search criteria
     */
    LHCFillSet getLHCFillsAndBeamModesInTimeWindowContainingBeamModes(Timestamp startTime, Timestamp endTime,
            BeamModeValue[] beamModes);

    /**
     * Gets the last completed LHC Fill, containing all beams modes which have occurred within the fill. The last
     * completed LHC Fill is the Fill which has ended before the current Fill has started.
     * 
     * @return The last completed <code>LHCFill</code>
     */
    LHCFill getLastCompletedLHCFillAndBeamModes();

    /**
     * Gets the LHC Fill with the given fill number. Returns null if no Fill with the given number exists.
     * 
     * @param fillNumber - The number of the <code>LHCFill</code> which should be retrieved
     * @return The <code>LHCFill</code> with the given fill number
     */
    LHCFill getLHCFillAndBeamModesByFillNumber(int fillNumber);

    /**
     * Gets the LHC Fills which occurred within the given time window, provided that they contain one or more of the
     * given beam modes. Each LHC Fill will NOT contain the beams modes which have occurred within the Fill.
     *
     * @see #getLHCFillsAndBeamModesInTimeWindowContainingBeamModes(Timestamp, Timestamp, BeamModeValue[])
     * @param startTime - The start of the time window within which to search for LHC Fills
     * @param endTime - The end of the time window within which to search for LHC Fills
     * @param beamModes - The beam modes by which LHC Fills in the time window will be filtered by (i.e. only Fills
     *            containing one or more of the beam modes will be returned).
     * @return an <code>LHCFillSet</code> containing all LHC Fills which match the search criteria
     */
    LHCFillSet getLHCFillsInTimeWindowContainingBeamModes(Timestamp startTime, Timestamp endTime,
            BeamModeValue[] beamModes);

}