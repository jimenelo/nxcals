package cern.nxcals.api.backport.client.service;


/**
 * Internal interface used for Filters, not meant to be instantiated by the API clients.
 */
interface LegalColumnName {
    String getLegalColumnName();
}
