package cern.nxcals.api.backport.domain.core.constants;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.io.Serializable;
import java.util.StringTokenizer;

/**
 * Timescale properties class for time scaling purpose. Use the static method
 * TimeScaleProperties.getTimeScaleProperties(ScaleAlgorithm algorithm, short scaleSize, LoggingTimeInterval
 * timeInterval)
 * 
 * @author Daniel Teixeira
 * @version $Revision$, $Date$, $Author$
 */
@Getter
@AllArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class TimescalingProperties implements Serializable {
    private static final long serialVersionUID = 3740015615118378955L;

    private ScaleAlgorithm timescaleAlgorithm;
    private short scaleSize;
    private LoggingTimeInterval timescaleInterval;

    /**
     * Returns the time scale properties
     *
     * @param algorithm The algorithm
     * @param scaleSize The size
     * @param timeInterval The interval
     * @return The time scale properties
     */
    public static TimescalingProperties getTimeScaleProperties(ScaleAlgorithm algorithm, short scaleSize,
            LoggingTimeInterval timeInterval) {
        if (scaleSize <= 0) {
            scaleSize = 1;
        }
        return new TimescalingProperties(algorithm, scaleSize, timeInterval);
    }

    public long getTimeMs() {
        return timescaleInterval.getTimeMs() * scaleSize;
    }

    @Override
    public String toString() {
        return "Timescaled with " + timescaleAlgorithm.name() + " every " + scaleSize + " " + timescaleInterval.name();
    }

    public String abbrev() {
        return scaleSize + "_" + timescaleInterval.name() + "_" + timescaleAlgorithm.name();
    }

    public long getNumberOfSecondsInInterval() {
        return timescaleInterval.getTimeMs() / 1000 * scaleSize;
    }

    @EqualsAndHashCode.Include
    public String toDBString() {
        return abbrev();
    }

    public static TimescalingProperties valueOf(String string) {
        StringTokenizer st = new StringTokenizer(string, "_");

        String size = st.nextToken();
        String interval = st.nextToken();
        String alg = st.nextToken();

        return new TimescalingProperties(ScaleAlgorithm.valueOf(alg), Short.parseShort(size), LoggingTimeInterval.valueOf(interval));
    }
}
