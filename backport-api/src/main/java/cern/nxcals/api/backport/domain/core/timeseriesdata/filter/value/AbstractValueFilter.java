package cern.nxcals.api.backport.domain.core.timeseriesdata.filter.value;

import cern.accsoft.commons.util.CollectionUtils;
import cern.nxcals.api.backport.domain.core.constants.FilterOperand;
import cern.nxcals.api.backport.domain.core.metadata.Variable;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@EqualsAndHashCode
@Getter
public abstract class AbstractValueFilter<T extends Serializable> implements ValueFilter<T>, Serializable {
    private static final long serialVersionUID = 4273248644300162347L;
    private final Variable variable;
    private final List<T> values;
    private final FilterOperand operand;

    @SuppressWarnings("unchecked")
    AbstractValueFilter(Variable v, FilterOperand operand, T... value) {
        variable = Objects.requireNonNull(v, "Variable must not be null");
        values = value != null ? Arrays.asList(value) : new ArrayList<>(0);
        this.operand = operand;

    }

    private String getOperandDesc() {
        return operand != null ? " " + operand.getDBValue() + " " : "";
    }

    @Override
    public String describe() {
        return variable + getOperandDesc() + getValuesAsString();
    }

    private String getValuesAsString() {
        if (CollectionUtils.isEmpty(values)) {
            return "";
        }
        if (FilterOperand.BETWEEN.equals(this.operand)) {
            return " " + values.stream().map(Object::toString).collect(Collectors.joining(" AND ")) + " ";
        }
        if (FilterOperand.IN.equals(this.operand) || FilterOperand.NOT_IN.equals(this.operand)) {
            return " (" + values.stream().map(v -> v instanceof String ? "'" + v + "'" : v.toString())
                    .collect(Collectors.joining(",")) + ")";
        }
        if (values.size() == 1) {
            Object v = values.get(0);
            return " " + (v instanceof String ? "'" + v + "'" : v.toString());
        }

        throw new IllegalArgumentException("Wrong operator: " + this.operand + " for values: " + values);

    }

    @Override
    public String toString() {
        return describe();
    }
}
