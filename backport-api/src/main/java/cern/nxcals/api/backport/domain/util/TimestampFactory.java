package cern.nxcals.api.backport.domain.util;

import cern.nxcals.api.backport.domain.core.constants.LoggingTimeInterval;
import cern.nxcals.api.backport.domain.core.constants.LoggingTimeZone;
import lombok.extern.slf4j.Slf4j;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

@Slf4j
public class TimestampFactory {
    private static final String TIMESTAMP_FORMAT_PATTERN = "yyyy-MM-dd HH:mm:ss.SSS";
    private static final TimeZone UTC_TIMEZONE = TimeZone.getTimeZone("GMT");
    private static final TimeZone LOCAL_TIMEZONE = TimeZone.getTimeZone("Europe/Paris");

    private TimestampFactory() {
        throw new AssertionError("This should never be used");
    }

    public static Timestamp parseUTCTimestamp(String uTCTimestampAsISOString) {
        return new Timestamp(parseToMillis(getUTCTimeZoneTimestampFormat(), uTCTimestampAsISOString));
    }

    public static Timestamp parseLocalTimestamp(String localTimestampAsISOString) {
        return new Timestamp(parseToMillis(getLocalTimeZoneTimestampFormat(), localTimestampAsISOString));
    }

    public static Timestamp parseTimestamp(String timestampAsISOString, LoggingTimeZone timeZone) {
        return timeZone.equals(LoggingTimeZone.UTC_TIME) ? parseUTCTimestamp(timestampAsISOString)
                : parseLocalTimestamp(timestampAsISOString);
    }

    private static long parseToMillis(DateFormat format, String pattern) {
        try {
            return format.parse(checkFormatPattern(pattern)).getTime();
        } catch (ParseException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public static Timestamp getCurrentTime() {
        return new Timestamp(System.currentTimeMillis());
    }

    public static Timestamp getTimeOffset(int unit, LoggingTimeInterval timeInterval) {
        return timeInterval != null ? new Timestamp(System.currentTimeMillis() + (timeInterval.getTimeMs() * unit)) : getCurrentTime();
    }

    public static Timestamp maxEndTime() {
        return TimestampFactory.getTimeOffset(-1, LoggingTimeInterval.MINUTE);
    }

    private static DateFormat getLocalTimeZoneTimestampFormat() {
        DateFormat dateFormat = new SimpleDateFormat(TIMESTAMP_FORMAT_PATTERN);
        dateFormat.setTimeZone(LOCAL_TIMEZONE);
        return dateFormat;
    }

    private static DateFormat getUTCTimeZoneTimestampFormat() {
        DateFormat dateFormat = new SimpleDateFormat(TIMESTAMP_FORMAT_PATTERN);
        dateFormat.setTimeZone(UTC_TIMEZONE);
        return dateFormat;
    }

    private static String checkFormatPattern(String sIn) {
        String s = sIn.replace("T", " ");
        switch (s.length()) {
            case 4:  // yyyy
                return s + "-01-01 00:00:00.000";
            case 7:  // yyyy-MM
                return s + "-01 00:00:00.000";
            case 10:  // yyyy-MM-dd
                return s + " 00:00:00.000";
            case 13:  // yyyy-MM-dd HH
                return s + ":00:00.000";
            case 19:  // yyyy-MM-dd HH:mm:ss
                return s + ".000";
            case 23:
                return s;
            default:
                throw new IllegalArgumentException("Timestamp format must be: " + TIMESTAMP_FORMAT_PATTERN + " , but was given: " + s);
        }
    }
}
