package cern.nxcals.api.backport.domain.core.metadata;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collector;

public class VariableListSet extends TreeMap<String, VariableList> {

    private static final long serialVersionUID = -590798481059170406L;

    public static Collector<VariableList, VariableListSet, VariableListSet> collector() {
        return Collector.of(
                VariableListSet::new,
                VariableListSet::addVariableList,
                (set1, set2) -> {
                    set1.putAll(set2);
                    return set1;
                }
        );
    }

    public VariableListSet() {
        super(String.CASE_INSENSITIVE_ORDER);
    }

    /**
     * Gets a VariableList with the given name from the VariableListSet if it exists.
     * 
     * @param listName - the name of the required VariableList.
     * @return the VariableList with given name if it exists within the VariableListSet. Returns null if does not exist.
     */
    public VariableList getVariableList(String listName) {
        return get(listName);
    }

    /**
     * Gets a VariableList from the given index in the VariableListSet if it exists.
     * 
     * @param idx - the index of the required VariableList in the VariableListSet.
     * @return the VariableList with given index if it exists within the VariableListSet. Returns null if does not
     *         exist.
     */
    public VariableList getVariableList(int idx) {
        return (VariableList) values().toArray()[idx];
    }

    /**
     * Gets a count of the number of VariableList objects within the VariableListSet.
     * 
     * @return a count of the number of VariableList objects within the VariableListSet.
     */
    public int getVariableListCount() {
        return size();
    }

    /**
     * Get a set of the names of the VariableList objects within the VariableListSet.
     * 
     * @return a set of the names of the VariableList objects within the VariableListSet.
     */
    public Set<String> getVariableListNames() {
        return keySet();
    }

    /**
     * Gets a Collection of all the VariableList objects within the VariableListSet.
     * 
     * @return - a Collection of all the VariableList objects within the VariableListSet.
     */
    public Collection<VariableList> getVariableLists() {
        return values();
    }

    /**
     * Gets a long [] of all variable Ids in this VariableListSet.
     * 
     * @return a long [] of all variable Ids in this VariableListSet.
     */
    public long[] getVariableListIds() {
        return values().stream().mapToLong(VariableList::getVariableListId).toArray();
    }

    public void addVariableList(VariableList variableList) {
        put(variableList.getVariableListName(), variableList);
    }

    /**
     * Indicates whether of not this object contains a VariableList with the given name.
     * 
     * @param listName - the name of the VariableList object to search for.
     * @return - true if this object is representing a VariableList with the given name.
     */
    public boolean containsVariableList(String listName) {
        return keySet().contains(listName);
    }

    public Iterator<VariableList> iterator() {
        return values().iterator();
    }

    /**
     * Removes the VariableList with the given name from the VariableListSet.
     * 
     * @param listName - the name of the VariableList to be removed from the VariableListSet.
     * @return - true if the VariableList with the given name was removed from the VariableListSet.
     */
    public boolean removeVariableList(String listName) {
        return remove(listName) != null;
    }

    /**
     * Removes all VariableList objects from the VariableListSet.
     */
    public void removeAllVariableLists() {
        clear();
    }
}
