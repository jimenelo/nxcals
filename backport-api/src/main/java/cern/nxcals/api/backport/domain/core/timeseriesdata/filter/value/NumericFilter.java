package cern.nxcals.api.backport.domain.core.timeseriesdata.filter.value;

import cern.nxcals.api.backport.domain.core.constants.FilterOperand;
import cern.nxcals.api.backport.domain.core.metadata.Variable;

public class NumericFilter extends AbstractValueFilter<Double> {
    private static final long serialVersionUID = -2097747978167061851L;

    NumericFilter(Variable v, FilterOperand operand, Double... value) {
        super(v, operand, value);
    }

    @Override
    public void accept(FilterVisitor visitor) {
        visitor.visit(this);
    }
}
