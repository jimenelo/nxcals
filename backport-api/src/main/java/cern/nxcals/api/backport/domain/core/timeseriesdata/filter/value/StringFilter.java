package cern.nxcals.api.backport.domain.core.timeseriesdata.filter.value;

import cern.nxcals.api.backport.domain.core.constants.FilterOperand;
import cern.nxcals.api.backport.domain.core.metadata.Variable;

public class StringFilter extends AbstractValueFilter<String> {
    private static final long serialVersionUID = 805285640882421904L;

    StringFilter(Variable v, FilterOperand operand, String... value) {
        super(v, operand, value);
    }

    @Override
    public void accept(FilterVisitor visitor) {
        visitor.visit(this);
    }
}
