package cern.nxcals.api.backport.client.service;

import cern.nxcals.api.backport.domain.core.metadata.VariableList;
import cern.nxcals.api.backport.domain.core.metadata.VariableListSet;
import cern.nxcals.api.backport.domain.core.snapshot.Snapshot;
import cern.nxcals.api.backport.domain.core.snapshot.SnapshotCriteria;

import java.util.List;

public interface QuerySnapshotDataService {

    /**
     * Gets a variableList identified by its name or null if the name is not found
     * 
     * @param listNamePattern the list name pattern
     * @return the variable list or null
     */
    VariableList getVariableListWithName(String listNamePattern);

    /**
     * Gets a List of VariableList objects belonging to the given user, and with the list name and description matching
     * the given patterns. % = wildcard.
     * 
     * @param userName the user name of the user
     * @param listNamePattern the list name pattern
     * @param listDescriptionPattern the list description pattern
     * @return the user variable lists
     */
    VariableListSet getVariableListsOfUserWithNameLikeAndDescLike(String userName, String listNamePattern,
            String listDescriptionPattern);

    /**
     * Get user snapshots with the given criteria.
     * 
     * @param criteria - set of criteria to match snapshots against
     * @return returns the set of Snapshots
     */
    List<Snapshot> getSnapshotsFor(SnapshotCriteria criteria);

    /**
     * Gets the attributes for the given snapshot
     * 
     * @param snapshot - the Snapshot to get attributes for this can be found at
     *            Snapshot
     * @return the snapshot with database id set.
     */
    Snapshot getSnapshotWithAttributes(Snapshot snapshot);
}