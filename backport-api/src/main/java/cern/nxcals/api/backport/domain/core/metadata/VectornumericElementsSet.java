package cern.nxcals.api.backport.domain.core.metadata;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class VectornumericElementsSet implements Serializable {
    private static final long serialVersionUID = -3675509507174399252L;
    private final Map<Timestamp, VectornumericElements> elements = new TreeMap<>();

    /**
     * Adds the given VectornumericElements object to the VectornumericElementsSet.
     * 
     * @param validSinceUTC - The Timestamp identifying the UTC time since when the accompanying VectornumericElements
     *            are valid.
     * @param elements - The VectornumericElements to be added to this VectornumericElementsSet.
     */
    public void addVectornumericElements(Timestamp validSinceUTC, VectornumericElements elements) {
        this.elements.put(validSinceUTC, elements);
    }

    /**
     * @return - a Map of all VectornumericElements in the VectornumericElementsSet. The keys of the Map are the UTC
     *         Timestamps which indicate the time when corresponding VectornumericElements became valid.
     */
    public Map<Timestamp, VectornumericElements> getVectornumericElements() {
        return elements;
    }

    /**
     * Gets the VectornumericElements (if any) that were valid at the given UTC reference time.
     * 
     * @param referenceTimeUTC - the UTC reference time for which corresponding VectornumericElements should be
     *            returned.
     * @return - the VectornumericElements (if any) that were valid at the given UTC reference time.
     */
    public VectornumericElements getVectornumericElements(Timestamp referenceTimeUTC) {
        Timestamp key = null;
        for (Timestamp t : elements.keySet()) {
            if (!referenceTimeUTC.before(t)) {
                key = t;
            }
        }
        return elements.get(key);
    }

    /**
     * @return - the number of VectornumericElements in this VectornumericElementsSet
     */
    public int size() {
        return elements.size();
    }

    /**
     * Gets the Set of UTC Timestamps which indicate the time when corresponding VectornumericElements became valid.
     * 
     * @return - the UTC Timestamps which indicate the time when corresponding VectornumericElements became valid.
     */
    public Set<Timestamp> getTimestamps() {
        return elements.keySet();
    }

}
