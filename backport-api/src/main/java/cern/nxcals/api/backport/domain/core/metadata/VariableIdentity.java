package cern.nxcals.api.backport.domain.core.metadata;

import cern.nxcals.api.backport.domain.core.datasource.DatasourceIdentifier;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.io.Serializable;

@Getter
@AllArgsConstructor
public class VariableIdentity implements Serializable {
    private static final long serialVersionUID = 1L;
    private final DatasourceIdentifier dataLocationIdentifier;
    private final long externalId;
}