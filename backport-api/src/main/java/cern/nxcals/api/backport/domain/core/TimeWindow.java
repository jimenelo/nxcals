package cern.nxcals.api.backport.domain.core;

import cern.nxcals.api.backport.domain.core.constants.LoggingTimeZone;
import cern.nxcals.api.backport.domain.util.DateUtils;
import cern.nxcals.api.backport.domain.util.TimestampFactory;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.sql.Timestamp;

@Getter
@Setter
@AllArgsConstructor
@EqualsAndHashCode
@Slf4j
public class TimeWindow implements Comparable<TimeWindow>, Serializable {
    public static TimeWindow newTimeWindow(Timestamp startTime, Timestamp endTime) {
        return newTimeWindow(startTime, endTime, "");
    }

    public static TimeWindow newTimeWindow(Timestamp startTime, Timestamp endTime, String info) {
        Timestamp sT = startTime;
        Timestamp eT = endTime;

        if (sT == null) {
            sT = TimestampFactory.getCurrentTime();
            log.warn("Start time is null, use current time instead");
        }

        if (eT == null) {
            eT = sT;
            log.warn("End time is null, use start time");
        }

        // If the start time is after the end time
        if (sT.after(eT)) {
            log.warn("End time cut to start time");
            eT = sT;
        }

        return new TimeWindow(sT, eT, info);
    }

    private Timestamp startTime;
    private Timestamp endTime;
    private String info;

    @Override
    public int compareTo(TimeWindow tw) {
        return startTime.compareTo(tw.startTime);
    }

    public String print(LoggingTimeZone timeZone) {
        return DateUtils.formatTimestamp(startTime, timeZone) + " --> " + DateUtils
                .formatTimestamp(endTime, timeZone) + " " + timeZone;
    }
}
