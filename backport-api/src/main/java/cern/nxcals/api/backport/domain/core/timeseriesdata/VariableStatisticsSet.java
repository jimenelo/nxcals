package cern.nxcals.api.backport.domain.core.timeseriesdata;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class VariableStatisticsSet implements Serializable {
    private static final long serialVersionUID = 228287734242560966L;

    private Map<String, VariableStatistics> statistics = new TreeMap<>();

    /**
     * @param variableName - the name of the Variable for which VariableStatistics should be returned.
     * @return - VariableStatistics of the Variable with the given name within this VariableStatisticsSet
     */
    public VariableStatistics getVariableStatistics(String variableName) {
        return statistics.get(variableName);
    }

    /**
     * @param position - the position of the VariableStataistics in the VariableStatisticsSet for which
     *                 VariableStatistics should be returned.
     * @return - VariableStatistics at the position in the VariableStatisticsSet
     * @deprecated you should not try to obtain an n-th element of a set, even if it is an ordered set. Very inefficient. Never use.
     */
    @Deprecated
    public VariableStatistics getVariableStatistics(int position) {
        if (position >= size()) {
            throw new IndexOutOfBoundsException();
        }

        int i = 0;
        Iterator<VariableStatistics> iterator = iterator();
        while (i++ < position) {
            iterator.next();
        }
        return iterator.next();
    }

    /**
     * @param variableStatistics - the VariableStatistics to be added to this VariableStatisticsSet
     */
    public void addVariableStatistics(VariableStatistics variableStatistics) {
        statistics.merge(variableStatistics.getVariableName(), variableStatistics, VariableStatistics::mergeWith);
    }

    /**
     * @param variableStatistics - the VariableStatistics to be removed from this VariableStatisticsSet
     */
    public void removeVariableStatistics(VariableStatistics variableStatistics) {
        statistics.remove(variableStatistics.getVariableName());
    }

    /**
     * @return - an Iterator for all VariableStatistics in this VariableStatisticsSet
     */
    public Iterator<VariableStatistics> iterator() {
        return statistics.values().iterator();
    }

    /**
     * @return - a List containing all VariableStatistics in this VariableStatisticsSet
     * @deprecated very inefficient and conceptually wrong. Never use.
     */
    @Deprecated
    public List<VariableStatistics> getStatisticsList() {
        return new ArrayList<>(statistics.values());
    }

    /**
     * @return - the number of VariableStatistics in this VariableStatisticsSet
     */
    public int size() {
        return statistics.size();
    }

}