package cern.nxcals.api.backport.domain.core.timeseriesdata.filter.value;

import cern.nxcals.api.backport.domain.core.constants.FilterOperand;
import cern.nxcals.api.backport.domain.core.constants.VectorRestriction;
import cern.nxcals.api.backport.domain.core.metadata.Variable;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.io.Serializable;

@EqualsAndHashCode(callSuper = true)
@Getter
public class VectorValueFilter<T extends Serializable> extends AbstractValueFilter<T> implements Serializable {
    private static final long serialVersionUID = -6275193520974934407L;
    private final VectorRestriction restriction;

    VectorValueFilter(Variable v, FilterOperand operand, T[] value, VectorRestriction restriction) {
        super(v, operand, value);
        this.restriction = restriction;
    }

    @Override
    public void accept(FilterVisitor visitor) {
        visitor.visit(this);
    }


}
