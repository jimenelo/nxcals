package cern.nxcals.api.backport.domain.core.timeseriesdata.filter.value;

import cern.nxcals.api.backport.domain.core.constants.FilterOperand;
import cern.nxcals.api.backport.domain.core.metadata.Variable;

import java.io.Serializable;
import java.util.List;

public interface ValueFilter<T extends Serializable> extends Filter {
    Variable getVariable();
    List<T> getValues();
    FilterOperand getOperand();
}
