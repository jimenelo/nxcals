/**
 * Copyright (c) 2019 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.api.backport.pytimber.utils;

import cern.cmw.datax.EntryType;
import cern.cmw.datax.ImmutableData;
import cern.nxcals.api.converters.SparkRowToImmutableDataConverter;
import org.apache.commons.lang.ArrayUtils;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.types.StructType;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class SparkDataFrameConversions {
    private SparkDataFrameConversions() {
        throw new UnsupportedOperationException("static helper");
    }

    public static Object[] extractColumn(Dataset<Row> dataset, String columnName) {
        List<Row> collectAsList = dataset.select(columnName).collectAsList();
        return collectAsList.stream().map(row -> row.get(0)).toArray();
    }

    public static String[] extractStringColumn(Dataset<Row> dataset, String columnName) {
        List<Row> collectAsList = dataset.select(columnName).collectAsList();
        return collectAsList.stream().map(row -> row.getString(0)).toArray(String[]::new);
    }

    public static double[] extractDoubleColumn(Dataset<Row> dataset, String columnName) {
        return dataset.select(columnName).collectAsList().stream().mapToDouble(SparkDataFrameConversions::extractDouble)
                .toArray();
    }

    public static long[] extractLongColumn(Dataset<Row> dataset, String columnName) {
        return dataset.select(columnName).collectAsList().stream().mapToLong(SparkDataFrameConversions::extractLong)
                .toArray();
    }

    public static boolean[] extractBooleanColumn(Dataset<Row> dataset, String columnName) {
        Boolean[] booleans = dataset.select(columnName).collectAsList().stream()
                .map(SparkDataFrameConversions::extractBoolean).toArray(Boolean[]::new);
        return ArrayUtils.toPrimitive(booleans);
    }

    public static Object[] extractArrayColumn(Dataset<Row> dataset, String columnName) {
        List<Row> rows = dataset.select(columnName).collectAsList();
        List<Object> columnValues = rows.stream().map(row -> extractPrimitiveArray(row, columnName)).collect(Collectors.toList());
        return columnValues.toArray();
    }

    /**
     * Converts a Dataset Row to an array (of arrays) of primitive data.
     * <p>
     * To be used for rows containing VectorNumeric data, but works also for scalar data.
     *
     * @param row       the Dataset
     * @param entryName the name of the column to be converted
     * @return an array (of arrays) of primitive data
     */
    private static Object extractPrimitiveArray(Row row, String entryName) {
        ImmutableData immdata = SparkRowToImmutableDataConverter.convert(row);
        EntryType<?> entryType = immdata.getEntry(entryName).getType();
        if (EntryType.BOOL_WRAPPER_ARRAY.equals(entryType)) {
            return ArrayUtils.toPrimitive(immdata.getEntry(entryName).getAs(EntryType.BOOL_WRAPPER_ARRAY));
        } else if (EntryType.INT8_WRAPPER_ARRAY.equals(entryType)) {
            return ArrayUtils.toPrimitive(immdata.getEntry(entryName).getAs(EntryType.INT8_WRAPPER_ARRAY));
        } else if (EntryType.INT16_WRAPPER_ARRAY.equals(entryType)) {
            return ArrayUtils.toPrimitive(immdata.getEntry(entryName).getAs(EntryType.INT16_WRAPPER_ARRAY));
        } else if (EntryType.INT32_WRAPPER_ARRAY.equals(entryType)) {
            return ArrayUtils.toPrimitive(immdata.getEntry(entryName).getAs(EntryType.INT32_WRAPPER_ARRAY));
        } else if (EntryType.INT64_WRAPPER_ARRAY.equals(entryType)) {
            return ArrayUtils.toPrimitive(immdata.getEntry(entryName).getAs(EntryType.INT64_WRAPPER_ARRAY));
        } else if (EntryType.FLOAT_WRAPPER_ARRAY.equals(entryType)) {
            return ArrayUtils.toPrimitive(immdata.getEntry(entryName).getAs(EntryType.FLOAT_WRAPPER_ARRAY));
        } else if (EntryType.DOUBLE_WRAPPER_ARRAY.equals(entryType)) {
            return ArrayUtils.toPrimitive(immdata.getEntry(entryName).getAs(EntryType.DOUBLE_WRAPPER_ARRAY));
        }
        return immdata.getEntry(entryName).get();
    }

    /**
     * Converts a Spark Dataset to arrays of primitive data or arrays for efficient consumption with numpy.
     * <p>
     * The Spark Dataset can have one or several columns, and the columns can have names with special chars as they
     * appear in (NX)CALS Variables, e.g. "SPS.BWS.51995.H_ROT.APP.IN:BU_INTENS_AV".
     * <p>
     * A column with Scalar data is convertd to an array of primitive values, a column with VectorNumeric data is
     * converted to an array of primitive arrays.
     *
     * @param dataset a dataset with one or several columns, where the columns names can contain special chars
     * @return a SortedMap&lt;String, Object&gt; with key=ColumnName and value=Primitive Array
     */
    public static SortedMap<String, Object> extractAllColumns(Dataset<Row> dataset) {
        SortedMap<String, Object> colName2Array = new TreeMap<>();

        String[] colNames = dataset.columns();
        for (int i = 0; i < colNames.length; i++) {
            // colNames may contain special chars present in CALS Variable names, which are not compatible
            // with Spark, e.g. ":._-", therefore we just use a dummy column name instead
            String colName = colNames[i];
            String compliantName = "colName" + i;
            Dataset<Row> renamedDs = dataset.withColumnRenamed(colName, compliantName).select(compliantName);
            String typeString = extractTypeString(renamedDs.select(compliantName).schema(), compliantName);
            switch (typeString) {
                case "struct":
                    colName2Array.put(colName, extractArrayColumn(renamedDs, compliantName));
                    break;
                case "double":
                case "float":
                    colName2Array.put(colName, extractDoubleColumn(renamedDs, compliantName));
                    break;
                case "long":
                case "bigint":
                    colName2Array.put(colName, extractLongColumn(renamedDs, compliantName));
                    break;
                case "boolean":
                    colName2Array.put(colName, extractBooleanColumn(renamedDs, compliantName));
                    break;
                case "string":
                    colName2Array.put(colName, extractStringColumn(renamedDs, compliantName));
                    break;
                default:
                    colName2Array.put(colName, extractColumn(renamedDs, compliantName));
            }
        }
        return colName2Array;
    }

    private static String extractTypeString(StructType schema, String fieldName) {
        int fieldIndex = schema.fieldIndex(fieldName);
        return schema.fields()[fieldIndex].dataType().simpleString().split("<")[0];
    }

    private static double extractDouble(Row row) {
        Object data = row.get(0);
        if (data instanceof Number) {
            return ((Number) data).doubleValue();
        } else {
            return Double.NaN;
        }
    }

    private static long extractLong(Row row) {
        Object data = row.get(0);
        if (data instanceof Number) {
            return ((Number) data).longValue();
        } else {
            return 0;
        }
    }

    private static boolean extractBoolean(Row row) {
        Object data = row.get(0);
        if (data instanceof Boolean) {
            return ((Boolean) data);
        } else {
            return Boolean.FALSE;
        }
    }
}
