package cern.nxcals.api.backport.domain.core.datasource;

import java.io.Serializable;

/**
 * The DatasourceIdentifier represents a data source.
 */
public enum DatasourceIdentifier implements Serializable {
    TIMBER, LHCLOG, MEASDB;

    /**
     * Utility method used to return the DatasourceIdenfier based on the name given. If the name doesn't match any
     * datasource, null is returned.
     * 
     * @param datasource the datasource (represented as a string)
     * @return the datasource (represented as DatasourceIdentifier), if the datasource doesn't correspond, the function
     *         returns null.
     */

    public static DatasourceIdentifier getDatasource(String datasource) {
        for (DatasourceIdentifier id : values()) {
            if (String.valueOf(id).equals(datasource)) {
                return id;
            }
        }
        return null;
    }
}