package cern.nxcals.api.backport.client.service;

import cern.cmw.datax.converters.DataxToJapcConverter;
import cern.japc.core.AcquiredParameterValue;
import cern.japc.core.Parameters;
import cern.japc.core.Selectors;
import cern.japc.core.factory.AcquiredParameterValueFactory;
import cern.japc.core.factory.SelectorFactory;
import cern.japc.core.factory.ValueHeaderFactory;
import cern.japc.core.spi.ValueHeaderImpl;
import cern.japc.value.MapParameterValue;
import cern.japc.value.ParameterValue;
import cern.japc.value.SimpleParameterValue;
import cern.nxcals.api.backport.domain.core.metadata.FundamentalData;
import cern.nxcals.api.backport.domain.core.metadata.JapcParameterDefinition;
import cern.nxcals.api.backport.domain.core.metadata.VariableSet;
import cern.nxcals.api.converters.SparkRowToImmutableDataConverter;
import cern.nxcals.api.extraction.data.builders.DataQuery;
import cern.nxcals.api.extraction.data.builders.DevicePropertyDataQuery;
import cern.nxcals.api.extraction.data.builders.fluent.DeviceStage;
import cern.nxcals.api.extraction.data.builders.fluent.EntityAliasStage;
import cern.nxcals.api.extraction.data.builders.fluent.SystemStage;
import cern.nxcals.api.extraction.data.builders.fluent.TimeStartStage;
import com.google.common.annotations.VisibleForTesting;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import java.sql.Timestamp;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static cern.nxcals.api.backport.domain.util.FundamentalUtils.createDatasetFromFundamentals;
import static cern.nxcals.api.custom.domain.CmwSystemConstants.ACQSTAMP;
import static cern.nxcals.api.custom.domain.CmwSystemConstants.CMW_SYSTEM;
import static cern.nxcals.api.custom.domain.CmwSystemConstants.CYCLESTAMP;
import static cern.nxcals.api.custom.domain.CmwSystemConstants.DEVICE_KEY_NAME;
import static cern.nxcals.api.custom.domain.CmwSystemConstants.PROPERTY_KEY_NAME;
import static cern.nxcals.api.custom.domain.CmwSystemConstants.RECORD_TIMESTAMP;
import static cern.nxcals.api.custom.domain.CmwSystemConstants.SELECTOR;
import static cern.nxcals.api.utils.TimeUtils.getTimestampFromNanos;

@AllArgsConstructor
class AcquiredParameterValueServiceImpl implements AcquiredParameterValuesService {
    private static final String NO_SELECTOR = Selectors.NO_SELECTOR.getId();

    AcquiredParameterValueServiceImpl(SparkSession spark) {
        this(() -> DataQuery.builder(spark), () -> DevicePropertyDataQuery.builder(spark));
    }

    @VisibleForTesting
    private final Supplier<DataQuery> dataQuerySupplier;

    @VisibleForTesting
    private final Supplier<SystemStage<TimeStartStage<EntityAliasStage<DeviceStage<Dataset<Row>>, Dataset<Row>>, Dataset<Row>>, Dataset<Row>>> parameterQuerySupplier;

    @Override
    public List<Timestamp> getDataDistribution(JapcParameterDefinition param, Timestamp startTime, Timestamp endTime) {
        return getDataset(param, startTime, endTime).select(RECORD_TIMESTAMP).collectAsList()
                .stream()
                .map(row -> getTimestampFromNanos(row.getLong(0))).collect(Collectors.toList());
    }

    @Override
    public List<AcquiredParameterValue> getParameterValuesInTimeWindow(@NonNull JapcParameterDefinition param,
            @NonNull Timestamp startTime, @NonNull Timestamp endTime) {
        return getAsList(getDataset(param, startTime, endTime));
    }

    private List<AcquiredParameterValue> getAsList(Dataset<Row> dataset) {
        return dataset.collectAsList().stream()
                .map(SparkRowToImmutableDataConverter::convert)
                .map(DataxToJapcConverter::toMapParameterValue).map(this::toAcquiredParameterValue).collect(
                        Collectors.toList());
    }

    private Dataset<Row> getDataset(@NonNull JapcParameterDefinition param, @NonNull Timestamp startTime,
            @NonNull Timestamp endTime) {
        return parameterQuerySupplier.get().system(CMW_SYSTEM).startTime(startTime.toInstant())
                .endTime(endTime.toInstant()).entity().device(param.getDeviceName())
                .property(param.getPropertyName()).build().sort(RECORD_TIMESTAMP);
    }

    private AcquiredParameterValue toAcquiredParameterValue(ParameterValue parameterValue) {
        MapParameterValue mapParameterValue = parameterValue.castAsMap();

        ValueHeaderImpl valueHeader = new ValueHeaderImpl(mapParameterValue.getLong(ACQSTAMP),
                mapParameterValue.getLong(CYCLESTAMP),
                SelectorFactory.newSelector(getSelector(mapParameterValue)));

        //once we support SET_STAMP in NXCALSDS this can be also set
        valueHeader.setSetStamp(ValueHeaderFactory.NO_SET_STAMP);

        return AcquiredParameterValueFactory
                .newAcquiredParameterValue(getParameterName(mapParameterValue), valueHeader, mapParameterValue);
    }

    private String getSelector(MapParameterValue mapParameterValue) {
        SimpleParameterValue value = mapParameterValue.get(SELECTOR);
        if (value != null) {
            return value.getString();
        }
        return NO_SELECTOR;
    }

    private String getParameterName(MapParameterValue mapParameterValue) {
        return Parameters.buildParameterName(mapParameterValue.getString(DEVICE_KEY_NAME), mapParameterValue
                .getString(PROPERTY_KEY_NAME));
    }

    @Override
    public AcquiredParameterValue getParameterValueAtTimestamp(@NonNull JapcParameterDefinition param,
            @NonNull Timestamp stamp) {
        return getParameterValuesInTimeWindow(param, stamp, stamp).stream().findFirst().orElse(null);
    }

    @SuppressWarnings("squid:CallToDeprecatedMethod")
    // createDatasetFromFundamentals should be called with SparkSession
    @Override
    public List<AcquiredParameterValue> getParameterValuesInTimeWindowFilteredByFundamentals(
            JapcParameterDefinition param, Timestamp start, Timestamp end, VariableSet fundamentals) {

        Dataset<Row> orgDataset = getDataset(param, start, end);
        Dataset<Row> fundamentalsDataset = createDatasetFromFundamentals(start, end, fundamentals,
                dataQuerySupplier.get());

        Dataset<Row> filteredData = orgDataset.join(fundamentalsDataset,
                        orgDataset.col(RECORD_TIMESTAMP)
                                .equalTo(fundamentalsDataset.col(FundamentalData.NXCALS_FUNDAMENTAL_TIMESTAMP)))
                .drop(FundamentalData.NXCALS_FUNDAMENTAL_TIMESTAMP);

        return getAsList(filteredData);
    }
}
