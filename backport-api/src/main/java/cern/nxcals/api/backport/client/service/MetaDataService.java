package cern.nxcals.api.backport.client.service;

import cern.nxcals.api.backport.domain.core.constants.VariableDataType;
import cern.nxcals.api.backport.domain.core.metadata.Hierarchy;
import cern.nxcals.api.backport.domain.core.metadata.HierarchySet;
import cern.nxcals.api.backport.domain.core.metadata.JapcParameterDefinition;
import cern.nxcals.api.backport.domain.core.metadata.SimpleJapcParameter;
import cern.nxcals.api.backport.domain.core.metadata.Variable;
import cern.nxcals.api.backport.domain.core.metadata.VariableList;
import cern.nxcals.api.backport.domain.core.metadata.VariableSet;
import cern.nxcals.api.backport.domain.core.metadata.VectornumericElementsSet;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.List;

public interface MetaDataService {

    /**
     * Gets a set of variables which are in the specified hierarchy and of the specified datatype
     *
     * @param hier - Hierarchy to match the variables to. Methods used to extract Hierarchies can be found in this
     *            service
     * @param dataType - VariableDataType to match the variables to. This is represented by an enum which can be found
     *            at VariableDataType
     * @return VariableSet - Collection of variables returned
     */
    VariableSet getVariablesOfDataTypeAttachedToHierarchy(
            Hierarchy hier, VariableDataType dataType);

    /**
     * Get a set of variables which are in the specified hierarchy and of the specified datatype and have a name which
     * matches the given pattern
     *
     * @param hier - Hierarchy to match the variables to. Methods used to extract Hierarchies can be found in this
     *            service
     * @param dataType - VariableDataType to match the variables to. This is represented by an enum which can be found
     *            at VariableDataType
     * @param pattern - String pattern to match the variable name against % = wildcard.
     * @return VariableSet - Collection of variables returned
     */
    VariableSet getVariablesOfDataTypeWithNameLikePatternAttachedToHierarchy(
            Hierarchy hier, String pattern,
            VariableDataType dataType);

    /**
     * Get a set of variables with a given datatype which can found in a variableList
     *
     * @param variableList - VariableList object which can be extracted using
     *            cern.accsoft.backport.extr.domain.client.QuerySnapshotDataService
     * @param dataType - VariableDataType to match the variables to. This is represented by an enum which can be found
     *            at VariableDataType
     * @return VariableSet - Collection of variables returned
     */
    VariableSet getVariablesOfDataTypeInVariableList(
            VariableList variableList, VariableDataType dataType);

    /**
     * Gets a variableSet containing all fundamentals which can be found in the given time window with a name which
     * matched the pattern
     *
     * @param startTime - Start of the required time window represented by a Java Timestamp object which can be created
     *            by Timestamp startTime = Timestamp.valueOf("2012-01-01 12:00:00"); Note this should represent the
     *            local time.
     * @param endTime - End of the required time window represented by a Java Timestamp object. NOTE the format must be
     *            the JDBC escape format: yyyy-mm-dd hh:mm:ss
     * @param pattern - String pattern to match the variable name against % = wildcard.
     * @return VariableSet - Collection of variables returned
     */
    VariableSet getFundamentalsInTimeWindowWithNameLikePattern(Timestamp startTime, Timestamp endTime,
            String pattern);

    /**
     * Get set of variables of the given datatype with a name which matches the given pattern
     *
     * @param pattern - String pattern to match the variable name against % = wildcard.
     * @param dataType - VariableDataType to match the variables to. This is represented by an enum which can be found
     *            at VariableDataType
     * @return VariableSet - Collection of variables returned
     */
    VariableSet getVariablesOfDataTypeWithNameLikePattern(String pattern, VariableDataType dataType);

    /**
     * Get a set of variables whose names are in the list of strings given
     *
     * @param variableNames - List of strings to use to find variables
     * @return VariableSet - Collection of variables returned
     */
    VariableSet getVariablesWithNameInListofStrings(List<String> variableNames);

    /**
     * @param variable
     * @return
     */
    VectornumericElementsSet getVectorElements(
            Variable variable);

    /**
     * @param variable
     * @param minStamp
     * @param maxStamp
     * @return
     */
    VectornumericElementsSet getVectorElementsInTimeWindow(Variable variable, Timestamp minStamp,
            Timestamp maxStamp);

    /**
     * Gets a HierarchySet containing all available Hierarchy objects
     *
     * @return HierarchySet - Collection of hierarchies
     */
    HierarchySet getAllHierarchies();

    /**
     * Gets a HierarchySet containing all available Hierarchy objects which are children of the given Hierarchy.
     *
     * @param parentHier - Hierarchy object representing the parent hierarchy for which to get children. This can be
     *            extracted using this service.
     * @return HierarchySet - Collection of hierarchies
     */
    HierarchySet getHierarchyChildNodes(
            Hierarchy parentHier);

    /**
     * Gets a Hierarchy node which has the given node path. The elements of the node path should be separated by '-&gt;'
     * For example, to find a hierarchy node corresponding to 'Beam Intensity Monitors' attached to the node 'Primary
     * Beam' of the Top Level node 'CNGS', use a nodePath value of 'CNGS-&gt;Primary Beam-&gt;Beam Intensity Monitors'. To get
     * a Hierarchy node corresponding to the Top Level node 'CNGS', just use a nodePath value of 'CNGS'. NOTE: The
     * quotes ' should not be included in the nodePath string.
     *
     * @param nodePath - String node path to get hierarchy for
     * @return Hierarchy - Single hierarchy object
     */
    Hierarchy getHierarchyForNodePath(String nodePath);

    /**
     * Gets a HierarchySet containing all Hierarchy objects which are parents of the ROOT hierarchy
     *
     * @return HierarchySet - Collection of hierarchies
     */
    HierarchySet getTopLevelHierarchies();

    /**
     * Gets a set of all hierarchies which the given variable can be found within
     *
     * @param variable - Variable object to get hierarchies for can be extracted using this service
     * @return HierarchySet - Collection of hierarchies
     */
    HierarchySet getHierarchiesForVariable(Variable variable);

    /**
     * Gets an JapcParameterDefinition object representing a JAPC parameter with a device name and property name which
     * match the given Strings. This object is required to retrieve AcquiredParameterValue objects from the database
     * using the AcquiredParameterValuesServiceImpl
     *
     * @param deviceName - the device name for which to retrieve a JAPC parameter.
     * @param propertyName - the property name for which to retrieve a JAPC parameter.
     * @return - JapcParameterDefinition (the JAPC parameter representation - required in order to retrieve
     *         AcquiredParameterValues using the AcquiredParameterValuesServiceImpl)
     */
    JapcParameterDefinition getJapcParameterDefinitionForDeviceAndPropertyName(String deviceName,
            String propertyName);

    /**
     * Get set of variables of the given datatype with a name which matches the given pattern which can be found in the
     * given variable list.
     *
     * @param variableList - VariableList object which can be extracted using
     *            cern.accsoft.backport.extr.domain.client.QuerySnapshotDataService
     * @param pattern - String pattern to match the variable name against % = wildcard.
     * @param dataType - VariableDataType to match the variables to. This is represented by an enum which can be found
     *            at VariableDataType
     * @return VariableSet - Collection of variables returned
     */
    VariableSet getVariablesOfDataTypeWithNameLikePatternInVariableList(
            VariableList variableList,
            String pattern, VariableDataType dataType);

    List<String> getAllDeviceNames();

    List<String> getPropertyNamesForDeviceName(String deviceName);

    /**
     * Gets a collection of japc configuration for the given variable list
     * 
     * @param variableList - the given variable list
     * @return a collection of japc configuration objects
     */
    Collection<SimpleJapcParameter> getJapcDefinitionFor(VariableList variableList);

}