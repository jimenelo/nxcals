package cern.nxcals.api.backport.domain.core.metadata;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Singular;

import java.io.Serializable;
import java.util.Collections;
import java.util.Map;
import java.util.Set;

@EqualsAndHashCode
@Builder
@Getter
public class JapcParameterDefinition implements Serializable {
    private static final long serialVersionUID = 8994476037909104130L;

    private final String propertyName;
    private final String deviceName;
    @Singular("fieldVariable")
    private final Map<String, Set<Variable>> fieldMap;

    public Set<Variable> getVariablesForField(String fieldName) {
        if (this.fieldMap == null || !this.fieldMap.containsKey(fieldName)) {
            return Collections.emptySet();
        }

        return this.fieldMap.get(fieldName);
    }
}
