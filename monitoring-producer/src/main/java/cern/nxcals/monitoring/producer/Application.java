/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.monitoring.producer;

import cern.nxcals.monitoring.producer.service.DataPublisher;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.ApplicationPidFileWriter;
import org.springframework.context.ApplicationContext;

/**
 * Represents an application class that bootstraps and launches a Spring application from a Java main
 * method.
 */
@SpringBootApplication
public class Application {
    public static void main(String[] args) throws Exception {
        SpringApplication app = new SpringApplication(Application.class);
        app.addListeners(new ApplicationPidFileWriter());
        ApplicationContext ctx = app.run();

        DataPublisher dataPublisher = ctx.getBean(DataPublisher.class);
        dataPublisher.start();
    }

}
