package cern.nxcals.monitoring.producer.service.generators;

import cern.cmw.datax.DataBuilder;
import cern.cmw.datax.ImmutableData;
import cern.nxcals.monitoring.producer.service.DataGenerator;

import java.util.Map;
import java.util.Random;

public abstract class AbstractDataGenerator implements DataGenerator {
    final Random random;

    public AbstractDataGenerator() {
        this.random = new Random();
    }

    @Override
    public ImmutableData generateData(Map<String, String> requiredKeyValues, String timestampKey, long timestamp, int scale) {
        DataBuilder builder = ImmutableData.builder();
        requiredKeyValues.forEach(builder::add);
        builder.add(timestampKey, timestamp);
        return addCustomFields(builder, scale, timestamp).build();
    }

    int[] randomArray(int maxSize, double lowerMargin) {
        int size = (int) (random.nextInt((int) (lowerMargin * maxSize) + 1) + (1 - lowerMargin) * maxSize);
        int[] integers = new int[size];
        for (int i = 0; i < integers.length; i++) {
            integers[i] = random.nextInt();
        }
        return integers;
    }

    protected abstract DataBuilder addCustomFields(DataBuilder builder, int scale, long timestamp);
}
