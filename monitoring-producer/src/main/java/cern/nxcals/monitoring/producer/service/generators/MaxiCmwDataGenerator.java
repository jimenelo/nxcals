package cern.nxcals.monitoring.producer.service.generators;

import cern.cmw.datax.DataBuilder;
import org.springframework.stereotype.Component;

@Component("maxi-cmw-data")
public class MaxiCmwDataGenerator extends AbstractDataGenerator {
    @Override
    protected DataBuilder addCustomFields(DataBuilder builder, int scale, long timestamp) {
        return builder
                .add("maxiLongField1", random.nextLong())
                .add("maxiLongField2", random.nextLong())
                .add("maxiArrayField1", randomArray(100, 0.2))
                .add("maxiArrayField2", randomArray(100, 0.2))
                .add("maxiArrayField3", randomArray(100, 0.2))
                .add("maxiDoubleField1", random.nextDouble())
                .add("maxiDoubleField2", random.nextDouble())
                .add("maxiArrayField4", randomArray( 100, 0.2))
                .add("maxiArrayField5", randomArray( 1000 * scale, 0.1))
                .add("maxiDoubleField3", random.nextDouble())
                .add("maxiDoubleField4", random.nextDouble());
    }
}
