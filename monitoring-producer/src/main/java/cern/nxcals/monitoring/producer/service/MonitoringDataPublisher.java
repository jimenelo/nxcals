package cern.nxcals.monitoring.producer.service;

import cern.cmw.datax.ImmutableData;
import cern.nxcals.api.ingestion.Publisher;
import cern.nxcals.api.ingestion.PublisherFactory;
import cern.nxcals.monitoring.producer.domain.ProducerConfiguration;
import cern.nxcals.monitoring.producer.domain.RecordConfig;
import com.google.common.annotations.VisibleForTesting;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.ScheduledExecutorService;
import java.util.function.Function;

import static java.time.Instant.ofEpochMilli;
import static java.util.Objects.requireNonNull;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.NANOSECONDS;

/**
 * Created by jwozniak on 03/11/16.
 */
@Service
@Slf4j
public class MonitoringDataPublisher implements DataPublisher {
    private final Map<String, DataGenerator> generators;
    private final ProducerConfiguration config;
    private final ConcurrentHashMap<String, Publisher<ImmutableData>> publishers;
    private final Function<String, Publisher<ImmutableData>> publisherCreator;
    private final ScheduledExecutorService scheduler;
    private final Executor executor;

    @Autowired
    public MonitoringDataPublisher(Map<String, DataGenerator> generators, ProducerConfiguration config,
            Executor publisherExecutor, ScheduledExecutorService scheduledExecutor) {
        this(generators, config, MonitoringDataPublisher::createPublisher, publisherExecutor, scheduledExecutor);
    }

    @VisibleForTesting
    MonitoringDataPublisher(Map<String, DataGenerator> generators, ProducerConfiguration config,
            Function<String, Publisher<ImmutableData>> publisherCreator, Executor executor,
            ScheduledExecutorService scheduler) {
        this.generators = requireNonNull(generators);
        this.config = requireNonNull(config);
        this.executor = requireNonNull(executor);
        this.publisherCreator = requireNonNull(publisherCreator);
        this.scheduler = requireNonNull(scheduler);
        this.publishers = new ConcurrentHashMap<>();
    }

    @Override
    public void start() {
        log.info("Start publishing values");
        config.getRecords().forEach(record -> scheduler
                .scheduleAtFixedRate(() -> sendValues(record), 0, record.getDuration().toMillis(), MILLISECONDS));
    }

    /**
     *  We need to send to be sure that the first message gets sent before all the others
     *  so that the history table gets updated with the right valid_from value. Otherwise, we risk a DataConflictException.
     *  After that, all the other messages can be sent asynchronously.
     */
    @VisibleForTesting
    void sendValues(RecordConfig recordConfig) {
        try {
            long duration = recordConfig.getDuration().toNanos();
            long shiftedTime = System.currentTimeMillis() + recordConfig.getMillisToShiftCurrentTime();
            // round to the nearest start
            long startTime = (MILLISECONDS.toNanos(shiftedTime) / duration) * duration;
            long deltaTime = duration / recordConfig.getMessageCount();
            long msgCount = recordConfig.getMessageCount();

            Publisher<ImmutableData> publisher = publishers.computeIfAbsent(recordConfig.getSystem(), publisherCreator);
            log.info("Publishing {} records={} and current={}", recordConfig.getMessageCount(), recordConfig,
                    ofEpochMilli(NANOSECONDS.toMillis(startTime)));
            ImmutableData data = generateData(recordConfig, startTime);
            sendValueAsync(publisher, data, recordConfig, startTime, deltaTime, config.getMaxAttempts(), msgCount);
        } catch (Exception ex) {
            log.error("Exception while publishing values", ex);
        }
    }

    /**
     * This method asynchronously sends all the other messages after the first one of the package is sent.
     * msgCount is set to -1 in the sendValueAsync method, because we specify this way that we are dealing with
     * a message that is not the first one.
     */
    private void send(Publisher<ImmutableData> publisher, RecordConfig recordConfig, long startTime, long deltaTime,
            long msgCount) {
        long currentTime = startTime;
        long count = msgCount;
        while (count-- > 0) {
            long recTime = currentTime;
            ImmutableData data = generateData(recordConfig, recTime);
            sendValueAsync(publisher, data, recordConfig, currentTime, deltaTime, config.getMaxAttempts(), -1);
            sleepIfNeeded(recordConfig.getSleepTimeAfterNextMessage());
            currentTime += deltaTime;
        }
    }

    /**
     * This method is responsible for sending all the messages asynchronously and dealing with their eventual failure.
     * If a message fails to be sent, we try to resend it again for maxAttempts times.
     * In the case we are dealing with the first message of the pack of messages, we have that msgCount is a value
     * bigger than 0 (as it represents the number of messages in the pack of messages to be sent),
     * and therefore, in case of success, we call the send(..) method which is responsible for sending all the other messages.
     * In case of failure, we try to send it again (and this procedure is repeated for maxAttempts times if it keeps failing).
     * In the case in which we are dealing with other messages (not the first one), then msgCount will be equal to -1,
     * and in case of successfully delivery of the message, we only log the delivery, otherwise we try to send it again
     * after an eventual sleep time.
     */
    private void sendValueAsync(Publisher<ImmutableData> publisher, ImmutableData data, RecordConfig recordConfig,
                                long time, long deltaTime, int maxAttempts, long msgCount) {
        if (maxAttempts == 0) {
            log.error("Unsuccessfully tried to send message {} for {} attempts", recordConfig.getRecordKeys(), config.getMaxAttempts());
            return;
        }
        publisher.publishAsync(data, executor).whenCompleteAsync((result, throwable) -> {
            if (throwable == null) {
                log.debug("Sent message for {} at={}", recordConfig.getRecordKeys(), ofEpochMilli(NANOSECONDS.toMillis(time)));
                if (msgCount > 0) {
                    sleepIfNeeded(recordConfig.getSleepTimeAfterFirstMessage());
                    send(publisher, recordConfig, time + deltaTime, deltaTime, msgCount - 1);
                }
            } else {
                log.warn("Message for {} at={} was not sent correctly. Will retry to send it if attempts are left...",
                        recordConfig.getRecordKeys(), ofEpochMilli(NANOSECONDS.toMillis(time)), throwable);
                sleepIfNeeded(recordConfig.getSleepTimeAfterFirstMessage());
                sendValueAsync(publisher, data, recordConfig, time, deltaTime,maxAttempts - 1, msgCount);
            }
        }, executor);
    }

    private ImmutableData generateData(RecordConfig config, long time) {
        DataGenerator generator = generators.get(config.getDataFlavour());
        return generator.generateData(config.getRecordKeys(), config.getTimestampKeyName(), time, config.getScale());
    }

    private void sleepIfNeeded(long sleepMillis) {
        if (sleepMillis > 0) {
            try {
                MILLISECONDS.sleep(sleepMillis);
            } catch (InterruptedException e) {
                log.error("Interrupted while sleeping for {}", sleepMillis, e);
                Thread.currentThread().interrupt();
                throw new IllegalStateException(e.getMessage(), e);
            }
        }
    }

    private static Publisher<ImmutableData> createPublisher(String systemName) {
        return PublisherFactory.newInstance().createPublisher(systemName, Function.identity());
    }
}
