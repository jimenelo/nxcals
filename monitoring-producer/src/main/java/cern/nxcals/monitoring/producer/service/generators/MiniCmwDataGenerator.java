package cern.nxcals.monitoring.producer.service.generators;

import cern.cmw.datax.DataBuilder;
import org.springframework.stereotype.Component;

@Component("mini-cmw-data")
public class MiniCmwDataGenerator extends AbstractDataGenerator {
    @Override
    protected DataBuilder addCustomFields(DataBuilder builder, int scale, long timestamp) {
        return builder
                .add("miniLongField1", random.nextLong())
                .add("miniLongField2", random.nextLong())
                .add("miniArrayField2", randomArray(scale, 0.2));
    }
}
