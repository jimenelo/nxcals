package cern.nxcals.monitoring.producer.domain;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.Duration;
import java.util.Map;

@Data
@Validated
@NoArgsConstructor
public class RecordConfig {
    @Min(1)
    private long messageCount;
    @NotNull
    private Duration duration;
    @Min(0)
    private long sleepTimeAfterFirstMessage = 0;
    @Min(0)
    private long sleepTimeAfterNextMessage = 0;

    private long millisToShiftCurrentTime = 0;

    private String dataFlavour;
    private String system;
    private String timestampKeyName;

    @Min(1)
    private int scale = 1;
    private Map<String, String> recordKeys;
}
