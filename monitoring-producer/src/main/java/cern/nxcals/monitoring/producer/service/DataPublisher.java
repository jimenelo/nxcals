package cern.nxcals.monitoring.producer.service;

/**
 * Created by jwozniak on 03/11/16.
 */
public interface DataPublisher {
    void start();
}
