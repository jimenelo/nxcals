package cern.nxcals.monitoring.producer.service.generators;

import cern.cmw.datax.DataBuilder;
import org.springframework.stereotype.Component;

@Component("midi-cmw-data")
public class MidiCmwDataGenerator extends AbstractDataGenerator {
    @Override
    protected DataBuilder addCustomFields(DataBuilder builder, int scale, long timestamp) {
        return builder
                .add("midiLongField1", random.nextLong())
                .add("midiLongField2", random.nextLong())
                .add("midiArrayField1", randomArray( 100, 0.2))
                .add("midiArrayField2", randomArray( 100, 0.2))
                .add("midiArrayField3", randomArray( 100 * scale, 0.1))
                .add("midiDoubleField1", random.nextDouble())
                .add("midiDoubleField2", random.nextDouble());
    }
}
