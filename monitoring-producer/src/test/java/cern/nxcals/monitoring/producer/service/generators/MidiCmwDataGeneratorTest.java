package cern.nxcals.monitoring.producer.service.generators;

import cern.cmw.datax.ImmutableData;
import cern.nxcals.monitoring.producer.service.DataGenerator;
import org.junit.jupiter.api.Test;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MidiCmwDataGeneratorTest {
    @Test
    public void shouldCreateData() throws Exception {
        DataGenerator dataGenerator = new MidiCmwDataGenerator();
        ImmutableData data = dataGenerator.generateData(Collections.emptyMap(), "timestampTest", 100L, 1);
        assertEquals(8, data.getEntryCount());
    }
}