package cern.nxcals.monitoring.producer.service.generators;

import cern.cmw.datax.EntryType;
import cern.cmw.datax.ImmutableData;
import cern.nxcals.monitoring.producer.service.DataGenerator;
import org.junit.jupiter.api.Test;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class WinccoaDataGeneratorTest {
    @Test
    public void shouldCreateData() throws Exception {
        DataGenerator dataGenerator = new WinccoaDataGenerator();

        ImmutableData data = dataGenerator.generateData(Collections.emptyMap(), "timestampTest", 100L, 1);

        assertEquals(100L, data.getEntry("timestampTest").get());
        assertEquals(EntryType.DOUBLE, data.getEntry("value").getType());
    }
}