package cern.nxcals.monitoring.producer.service.generators;

import cern.cmw.datax.ImmutableData;
import cern.nxcals.monitoring.producer.service.DataGenerator;
import org.junit.jupiter.api.Test;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MaxiCmwDataGeneratorTest {
    @Test
    public void shouldCreateData() throws Exception {
        DataGenerator dataGenerator = new MaxiCmwDataGenerator();
        ImmutableData data = dataGenerator.generateData(Collections.emptyMap(), "timestampTest", 100L, 1);
        assertEquals(12, data.getEntryCount());
    }
}