package cern.nxcals.monitoring.producer.service.generators;

import cern.cmw.datax.ImmutableData;
import cern.nxcals.monitoring.producer.service.DataGenerator;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Created by jwozniak on 02/10/17.
 */
public class MonitoringDataGeneratorTest {
    @Test
    public void shouldCreateData() throws Exception {
        DataGenerator dataGenerator = new MonitoringDataGenerator();

        Map<String, String> keys = new HashMap<>();
        keys.put("test1", "value1");
        keys.put("test2", "value2");

        ImmutableData data = dataGenerator.generateData(keys, "timestampTest", 100L, 1);

        assertEquals("value1", data.getEntry("test1").get());
        assertEquals("value2", data.getEntry("test2").get());
        assertEquals(100L, data.getEntry("timestampTest").get());
    }
}
