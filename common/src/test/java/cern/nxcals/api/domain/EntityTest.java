package cern.nxcals.api.domain;

import cern.nxcals.common.utils.GlobalObjectMapperProvider;
import cern.nxcals.common.utils.ReflectionUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Sets;
import org.apache.avro.SchemaBuilder;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedSet;

import static java.util.Collections.emptySortedSet;
import static java.util.Collections.singletonMap;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class EntityTest {
    private static final SystemSpec SYSTEM_SPEC = SystemSpec.builder().name("TEST").entityKeyDefinitions("test")
            .partitionKeyDefinitions("test").timeKeyDefinitions("test").build();
    private static final Partition PARTITION = Partition.builder().systemSpec(SYSTEM_SPEC)
            .keyValues(singletonMap("a", "b")).build();
    private static final Entity ENTITY = Entity.builder().systemSpec(SYSTEM_SPEC).partition(PARTITION)
            .entityKeyValues(singletonMap("a", "b")).entityHistory(emptySortedSet()).build();
    private static final String SCHEMA = SchemaBuilder.record("schema1").fields().name("schema1").type().stringType()
            .noDefault().endRecord().toString();

    @Test
    public void shouldNotCreateEntityWithoutSystemSpec() {
        assertThrows(NullPointerException.class, () -> Entity.builder().partition(PARTITION).entityKeyValues(singletonMap("a", "b")).entityHistory(emptySortedSet()).build());
    }

    @Test
    public void shouldNotCreateEntityWithoutPartition() {
        assertThrows(NullPointerException.class, () -> Entity.builder().systemSpec(SYSTEM_SPEC).entityKeyValues(singletonMap("a", "b")).entityHistory(emptySortedSet()).build());
    }

    @Test
    public void shouldNotCreateEntityWithoutEntityKeyValues() {
        assertThrows(NullPointerException.class, () -> Entity.builder().systemSpec(SYSTEM_SPEC).partition(PARTITION).entityHistory(emptySortedSet()).build());
    }

    @Test
    public void shouldSerialize() throws IOException {
        Entity e1 = Entity.builder().systemSpec(SYSTEM_SPEC).partition(PARTITION)
                .entityKeyValues(singletonMap("a", "b")).entityHistory(emptySortedSet()).build();

        ObjectMapper ob = GlobalObjectMapperProvider.get();
        String s = ob.writeValueAsString(e1);
        Entity e2 = ob.readValue(s, Entity.class);

        assertEquals(e1.getEntityHistory(), e2.getEntityHistory());
        assertEquals(e1.getEntityKeyValues(), e2.getEntityKeyValues());
        assertEquals(e1.getLockedUntilStamp(), e2.getLockedUntilStamp());
        assertEquals(e1, e2);
    }

    @Test
    public void shouldUnlock() {
        Entity e1 = Entity.builder()
                .systemSpec(SYSTEM_SPEC)
                .partition(PARTITION)
                .entityKeyValues(singletonMap("a", "b"))
                .lockedUntilStamp(Instant.MAX)
                .unlock()
                .build();

        assertThat(e1.getLockedUntilStamp()).isNull();
        assertThat(e1.toBuilder().lockedUntilStamp(Instant.MAX).build().getLockedUntilStamp()).isNotNull();
        assertThat(e1.toBuilder().lockedUntilStamp(Instant.MAX).build().toBuilder().unlock().build().getLockedUntilStamp()).isNull();
    }

    @Test
    public void shouldCopyInputSet() {
        SortedSet<EntityHistory> histories = Sets.newTreeSet();

        histories.add(createHistory(1, TimeWindow.between(1000, 2000)));

        Entity e1 = Entity.builder()
                .systemSpec(SYSTEM_SPEC)
                .partition(PARTITION)
                .entityKeyValues(singletonMap("a", "b"))
                .entityHistory(histories)
                .build();

        assertThat(e1.getEntityHistory()).hasSize(1);

        histories.add(createHistory(2, TimeWindow.between(2000, 3000)));
        assertThat(e1.getEntityHistory()).hasSize(1);

        Entity e2 = e1.toBuilder().entityHistory(histories).build();
        assertThat(e2.getEntityHistory()).hasSize(2);

        histories.add(createHistory(3, TimeWindow.between(3000, 4000)));
        assertThat(e2.getEntityHistory()).hasSize(2);
    }

    @Test
    public void shouldOutputSetBeImmutable() {
        Entity e1 = Entity.builder()
                .systemSpec(SYSTEM_SPEC)
                .partition(PARTITION)
                .entityKeyValues(singletonMap("a", "b"))
                .entityHistory(Sets.newTreeSet(Sets.newHashSet(createHistory(1, TimeWindow.between(1000, 2000)))))
                .build();

        assertThrows(UnsupportedOperationException.class, () -> e1.getEntityHistory().add(createHistory(1, TimeWindow.between(1000, 2000))));
    }

    @Test
    public void shouldCopyInputMap() {
        Map<String, Object> keyValues = new HashMap<>();

        keyValues.put("a1", "b1");

        Entity e1 = Entity.builder()
                .systemSpec(SYSTEM_SPEC)
                .partition(PARTITION)
                .entityKeyValues(keyValues)
                .entityHistory(emptySortedSet())
                .build();

        assertThat(e1.getEntityKeyValues()).hasSize(1);

        keyValues.put("a2", "b2");
        assertThat(e1.getEntityKeyValues()).hasSize(1);

        Entity e2 = e1.toBuilder().entityKeyValues(keyValues).build();
        assertThat(e2.getEntityKeyValues()).hasSize(2);

        keyValues.put("a3", "b3");
        assertThat(e2.getEntityKeyValues()).hasSize(2);
    }

    @Test
    public void shouldOutputMapBeImmutable() {
        Entity e1 = Entity.builder()
                .systemSpec(SYSTEM_SPEC)
                .partition(PARTITION)
                .entityKeyValues(singletonMap("a", "b"))
                .entityHistory(Sets.newTreeSet(Sets.newHashSet(createHistory(1, TimeWindow.between(1000, 2000)))))
                .build();

        assertThrows(UnsupportedOperationException.class, () -> e1.getEntityKeyValues().put("c", "d"));
    }

    private EntityHistory createHistory(long id, TimeWindow between) {
        return ReflectionUtils.builderInstance(EntityHistory.InnerBuilder.class).id(id).entitySchema(createSchema())
                .entity(ENTITY).partition(PARTITION).validity(between).build();
    }

    private static EntitySchema createSchema() {
        return ReflectionUtils.builderInstance(EntitySchema.InnerBuilder.class).id(-1).schemaJson(SCHEMA).build();
    }
}