package cern.nxcals.api.domain;

import cern.nxcals.common.utils.GlobalObjectMapperProvider;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.google.common.collect.Sets;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.TreeSet;

import static com.google.common.collect.Sets.newHashSet;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class VariableTest {

    private static final SystemSpec SYSTEM_SPEC = SystemSpec.builder().name("").partitionKeyDefinitions("")
            .timeKeyDefinitions("").entityKeyDefinitions("").build();
    private static final String VARIABLE_NAME = "variable";
    private static final VariableConfig VALID_VARIABLE_CONFIG = VariableConfig.builder().entityId(1L).fieldName("field")
            .variableName(VARIABLE_NAME).validity(TimeWindow.infinite()).build();
    private static final VariableConfig INVALID_VARIABLE_CONFIG = VariableConfig.builder().entityId(1L)
            .fieldName("field").variableName(VARIABLE_NAME).validity(TimeWindow.between(10, 9)).build();
    private static final VariableConfig FIRST_VARIABLE_CONFIG = VariableConfig.builder().entityId(1L).fieldName("field")
            .variableName(VARIABLE_NAME).validity(TimeWindow.between(1000, 2000)).build();
    private static final VariableConfig SECOND_VARIABLE_CONFIG = VariableConfig.builder().entityId(1L)
            .fieldName("field").variableName(VARIABLE_NAME).validity(TimeWindow.between(1500, 2500)).build();
    private static final VariableConfig THIRD_VARIABLE_CONFIG = VariableConfig.builder().entityId(1L).fieldName("field")
            .variableName(VARIABLE_NAME).validity(TimeWindow.between(2500, 3000)).build();
    private static final VariableConfig FOURTH_VARIABLE_CONFIG = VariableConfig.builder().entityId(1L)
            .fieldName("field").variableName(VARIABLE_NAME).validity(TimeWindow.between(3001, 3500)).build();

    @Test
    public void shouldSerialize() throws IOException {
        Variable v1 = Variable.builder().variableName(VARIABLE_NAME)
                .configs(Sets.newTreeSet(newHashSet(VALID_VARIABLE_CONFIG))).systemSpec(SYSTEM_SPEC)
                .declaredType(VariableDeclaredType.NUMERIC).build();
        ObjectMapper ob = GlobalObjectMapperProvider.get();
        ob.registerModule(new JavaTimeModule());
        String s = ob.writeValueAsString(v1);
        Variable v2 = ob.readValue(s, Variable.class);

        assertEquals(v1.getConfigs(), v2.getConfigs());
        assertEquals(v1, v2);
    }

    @Test
    public void shouldCreateVariableWhereFirstAndLastTimeStampsAreNotBothNull() {
        Variable.builder().variableName(VARIABLE_NAME).configs(Sets.newTreeSet(newHashSet(INVALID_VARIABLE_CONFIG)))
                .systemSpec(SYSTEM_SPEC).declaredType(VariableDeclaredType.NUMERIC).build();
    }

    @Test
    public void shouldCreateVariableWithConsecutiveTimeWindows() {
        Variable.builder().variableName(VARIABLE_NAME)
                .configs(Sets.newTreeSet(newHashSet(THIRD_VARIABLE_CONFIG, FOURTH_VARIABLE_CONFIG)))
                .systemSpec(SYSTEM_SPEC).declaredType(VariableDeclaredType.NUMERIC).build();
    }

    @Test
    public void shouldNotCreateVariableWithInvalidConfigWhereConfigsAreOverlapping() {
        assertThrows(IllegalStateException.class, () -> Variable.builder().variableName(VARIABLE_NAME)
                .configs(Sets.newTreeSet(newHashSet(FIRST_VARIABLE_CONFIG, SECOND_VARIABLE_CONFIG)))
                .systemSpec(SYSTEM_SPEC).declaredType(VariableDeclaredType.NUMERIC).build());
    }

    @Test
    public void shouldCreateVariableWhereTimeWindowsAreWithGaps() {
        Variable.builder().variableName(VARIABLE_NAME)
                .configs(Sets.newTreeSet(newHashSet(FIRST_VARIABLE_CONFIG, THIRD_VARIABLE_CONFIG)))
                .systemSpec(SYSTEM_SPEC).declaredType(VariableDeclaredType.NUMERIC).build();
    }

    @Test
    public void shouldCreateVariableWithEmptyConfig() {
        Variable.builder().variableName(VARIABLE_NAME).configs(Sets.newTreeSet()).systemSpec(SYSTEM_SPEC)
                .declaredType(VariableDeclaredType.NUMERIC).build();
    }

    @Test
    public void shouldNotCreateVariableWithoutSystemSpec() {
        assertThrows(NullPointerException.class, () -> Variable.builder().variableName(VARIABLE_NAME).configs(Sets.newTreeSet(newHashSet(VALID_VARIABLE_CONFIG)))
                .declaredType(VariableDeclaredType.NUMERIC).build());
    }

    @Test
    public void shouldNotCreateVariableWithoutName() {
        assertThrows(NullPointerException.class, () -> Variable.builder().configs(Sets.newTreeSet(newHashSet(VALID_VARIABLE_CONFIG))).systemSpec(SYSTEM_SPEC)
                .declaredType(VariableDeclaredType.NUMERIC).build());
    }

    @Test
    public void shouldCopyInputSetAndChangeWithoutAffectingTheOriginal() {
        TreeSet<VariableConfig> configs;

        configs = Sets.newTreeSet(Sets.newHashSet(FIRST_VARIABLE_CONFIG));
        Variable v1 = Variable.builder().variableName(VARIABLE_NAME).systemSpec(SYSTEM_SPEC)
                .configs(configs)
                .build();

        configs.add(SECOND_VARIABLE_CONFIG);

        assertThat(v1.getConfigs()).hasSize(1);

        configs = Sets.newTreeSet(Sets.newHashSet(FIRST_VARIABLE_CONFIG));
        Variable v2 = v1.toBuilder().configs(configs).build();
        configs.add(SECOND_VARIABLE_CONFIG);

        assertThat(v2.getConfigs()).hasSize(1);
        assertThat(v1.getConfigs()).hasSize(1);
    }


    @Test
    public void shouldOutputSetBeImmutable() {
        Variable v1 = Variable.builder().variableName(VARIABLE_NAME).systemSpec(SYSTEM_SPEC)
                .configs(Sets.newTreeSet(Sets.newHashSet(FIRST_VARIABLE_CONFIG)))
                .build();

        assertThrows(UnsupportedOperationException.class, () -> v1.getConfigs().add(SECOND_VARIABLE_CONFIG));
    }
}
