package cern.nxcals.api.domain;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class TimeEntityPartitionTypeTest {

    @Test
    public void shouldCreateWithPartitionsAndBuckets() {
        //given
        //when
        TimeEntityPartitionType timeEntityPartitionType = TimeEntityPartitionType.of(4, 5);
        //then
        assertEquals(4, timeEntityPartitionType.getTimePartitionCount());
        assertEquals(5, timeEntityPartitionType.getEntityBucketCount());
        assertEquals(20, timeEntityPartitionType.slots());
    }

    @Test
    public void shouldCreateFromString() {
        //given
        TimeEntityPartitionType type = TimeEntityPartitionType.of(4, 5);

        String str = type.toString();

        //when
        TimeEntityPartitionType timeEntityPartitionType = TimeEntityPartitionType.from(str);

        //then
        assertEquals(4, timeEntityPartitionType.getTimePartitionCount());
        assertEquals(5, timeEntityPartitionType.getEntityBucketCount());
        assertEquals(20, timeEntityPartitionType.slots());
        assertTrue(type.equals(timeEntityPartitionType));
    }

    @Test
    public void shouldCalculateDiff() {
        //given
        TimeEntityPartitionType type1 = TimeEntityPartitionType.of(2, 5);
        TimeEntityPartitionType type2 = TimeEntityPartitionType.of(3, 5);
        //when,
        //then 50% difference
        assertEquals(50, TimeEntityPartitionType.changePercentage(type1, type2), 000.1);
    }

    @Test
    public void shouldSelectPartitionType() {

        TimeEntityPartitionType type = TimeEntityPartitionType.ofGbFiles(10);
        assertEquals(4, type.getTimePartitionCount());
        assertEquals(3, type.getEntityBucketCount());
        assertEquals(12, type.slots());

    }

    @Test
    public void shouldSelectPartitionTypeForSmallNumbers() {

        TimeEntityPartitionType type = TimeEntityPartitionType.ofGbFiles(1);
        assertEquals(1, type.getTimePartitionCount());
        assertEquals(1, type.getEntityBucketCount());
        assertEquals(1, type.slots());

    }

    @Test
    public void shouldSelectPartitionTypeForZero() {

        TimeEntityPartitionType type = TimeEntityPartitionType.ofGbFiles(0);
        assertEquals(1, type.getTimePartitionCount());
        assertEquals(1, type.getEntityBucketCount());
        assertEquals(1, type.slots());

    }

    @Test
    public void shouldSelectPartitionTypeForBigNumbers() {

        TimeEntityPartitionType type = TimeEntityPartitionType.ofGbFiles(10000);
        assertEquals(96, type.getTimePartitionCount());
        assertEquals(14, type.getEntityBucketCount());
        assertEquals(96 * 14, type.slots());

    }

    @Test
    public void shouldThrowSelectPartitionTypeForNegativeNumbers() {
        assertThrows(IllegalArgumentException.class, () -> TimeEntityPartitionType.ofGbFiles(-1));
    }

    @Test
    public void shouldNext() {
        TimeEntityPartitionType type = TimeEntityPartitionType.ofGbFiles(1);
        TimeEntityPartitionType next = TimeEntityPartitionType.next(type, 1);
        assertEquals(4, next.slots());
    }

    @Test
    public void shouldNextBigger() {
        TimeEntityPartitionType type = TimeEntityPartitionType.ofGbFiles(1);
        TimeEntityPartitionType next = TimeEntityPartitionType.next(type, 3);
        assertEquals(8, next.slots());
    }

    @Test
    public void shouldSelectMax() {
        TimeEntityPartitionType type = TimeEntityPartitionType.ofGbFiles(100000);
        TimeEntityPartitionType next = TimeEntityPartitionType.next(type, 1);
        assertEquals(96 * 14, next.slots());
    }
}