package cern.nxcals.api.domain;

import cern.nxcals.common.utils.GlobalObjectMapperProvider;
import cern.nxcals.common.utils.ReflectionUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class VariableConfigTest {

    @Test
    public void shouldNotCreateConfigWithoutValidity() {
        assertTrue(VariableConfig.builder().entityId(1L).fieldName("test").variableName("test").validity(null).build()
                .getValidity().isInfinite());
    }

    @Test
    public void shouldBeEqualWithDifferentVariableName() {
        VariableConfig v1 = variableConfig("variableA");
        VariableConfig v2 = variableConfig("variableB");
        assertEquals(v1, v2);
    }

    @Test
    public void shouldSerialize() throws IOException {
        VariableConfig v1 = VariableConfig.builder().entityId(1L).fieldName("test").variableName("test1")
                .validity(TimeWindow.between(10, 20)).build();
        ObjectMapper ob = GlobalObjectMapperProvider.get();
        String s = ob.writeValueAsString(v1);
        VariableConfig v2 = ob.readValue(s, VariableConfig.class);

        assertEqual(v1, v2);
        assertEqual(v1.toBuilder().build(), v2);
        assertEqual(v1, v2.toBuilder().build());
    }

    @Test
    public void shouldCompareTwoConfigs() {
        VariableConfig v1 = VariableConfig.builder().entityId(1L).fieldName("test").variableName("test")
                .validity(TimeWindow.between(10, 20)).build();
        VariableConfig v2 = VariableConfig.builder().entityId(1L).fieldName("test").variableName("test")
                .validity(TimeWindow.between(20, 30)).build();

        assertEquals(1, v1.compareTo(v2));
        assertEquals(0, v1.compareTo(v1));
        assertEquals(0, v2.compareTo(v2));
        assertEquals(-1, v2.compareTo(v1));
    }

    private VariableConfig variableConfig(String variableA) {
        return ReflectionUtils.builderInstance(VariableConfig.InnerBuilder.class)
                .id(100)
                .entityId(120)
                .fieldName("field")
                .variableName(variableA)
                .validity(TimeWindow.between(10, 20))
                .recVersion(110).build();
    }

    private void assertEqual(VariableConfig configA, VariableConfig configB) {
        assertEquals(configA, configB);
        assertEquals(configA.getId(), configB.getId());
        assertEquals(configA.getRecVersion(), configB.getRecVersion());
        assertEquals(configA.getFieldName(), configB.getFieldName());
        assertEquals(configA.getVariableName(), configB.getVariableName());
        assertEquals(configA.getValidity(), configB.getValidity());
        assertEquals(configA.getEntityId(), configB.getEntityId());
        assertEquals(configA.getPredicate(), configB.getPredicate());
    }
}