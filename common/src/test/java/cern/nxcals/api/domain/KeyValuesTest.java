package cern.nxcals.api.domain;

import org.apache.avro.Schema;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericRecord;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class KeyValuesTest {
    private final String schemaString = "{"
            + "\"type\": \"record\", "
            + "\"name\": \"test_pair\","
            + "\"fields\":"
            + "["
            + "{\"name\": \"my_field_1\", \"type\": \"long\"},"
            + "{\"name\": \"my_field_2\", \"type\": \"string\"}"
            + "]"
            + "}";

    private final Schema schema = new Schema.Parser().parse(schemaString);

    @Test
    public void shouldCreateRecord() {
        KeyValues record = new KeyValues(record("v1", "v2"));
        assertEquals(2, record.getKeyValues().size());
    }

    @Test
    public void shouldEqualById() {
        KeyValues record1 = new KeyValues(record("v1", "v2"));
        KeyValues record2 = new KeyValues(record("v1", "v2"));

        assertEquals(record1, record2);
        assertEquals(record1.hashCode(), record2.hashCode());

        assertEquals(record1, record1);
        assertNotNull(record1);
        assertNotNull(record2);
    }

    private GenericRecord record(String... values) {
        GenericRecord genericRecord = new GenericData.Record(schema);
        int i = 0;
        for (Schema.Field field : schema.getFields()) {
            genericRecord.put(field.name(), values[i++]);
        }
        return genericRecord;
    }
}
