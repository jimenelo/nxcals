package cern.nxcals.api.extraction.data.builders.fluent.v2;

import cern.nxcals.api.extraction.data.builders.fluent.QueryData;
import cern.nxcals.common.domain.EntityKeyValue;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;

import java.time.Instant;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static cern.nxcals.api.extraction.data.builders.fluent.QueryData.DEVICE_KEY;
import static cern.nxcals.api.extraction.data.builders.fluent.QueryData.PROPERTY_KEY;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.TestInstance.Lifecycle.PER_CLASS;

/**
 *
 */
@TestInstance(PER_CLASS)
public class StageSequenceDevicePropertyTest {
    private static final String SYSTEM = "SYSTEM";
    private static final String DEVICE_1 = "DEVICE_1.SAMPLE";
    private static final String PROPERTY_1 = "PROPERTY_1.SAMPLE";
    private static final String DEVICE_2 = "DEVICE_2.SAMPLE";
    private static final String PROPERTY_2 = "PROPERTY_2.SAMPLE";

    private static final String DEVICE_1_WITH_USER_ESCAPING = "DEVICE\\_1.SAMPLE";
    private static final String PROPERTY_1_WITH_USER_ESCAPING = "PRO\\%RTY_1.SAMPLE";

    private static final Instant TIME_1 = Instant.now();
    private static final Instant TIME_2 = Instant.now();

    @ParameterizedTest
    @MethodSource("wrongParameterFormattedStrings")
    public void shouldThrowWhenParameterHasWrongFormat(String parameter) {
        assertThrows(IllegalArgumentException.class,
                () -> testBuilder(queryData()).parameterEq(parameter));
    }

    @Test
    public void shouldAcceptParameterWithEscapedByUserWildcards() {
        // given
        QueryData<Object> query = queryData();
        // when
        testBuilder(query).parameterLike(DEVICE_1_WITH_USER_ESCAPING + "/" + PROPERTY_1_WITH_USER_ESCAPING)
                .timeWindow(TIME_1, TIME_2)
                .build();
        // then
        Map<String, Object> result = ImmutableMap
                .of(DEVICE_KEY, DEVICE_1_WITH_USER_ESCAPING, PROPERTY_KEY, PROPERTY_1_WITH_USER_ESCAPING);
        assertTrue(query.getEntities().stream()
                .anyMatch(entityKeyValues -> result.equals(entityKeyValues.getAsMap())));
        assertEquals(TIME_1, query.getStartTime());
        assertEquals(TIME_2, query.getEndTime());
    }

    @Test
    public void shouldAcceptParameter() {
        // given
        QueryData<Object> query = queryData();
        // when
        testBuilder(query).parameterEq(DEVICE_1 + "/" + PROPERTY_1)
                .timeWindow(TIME_1, TIME_2)
                .build();
        // then
        Map<String, Object> result = ImmutableMap.of(DEVICE_KEY, DEVICE_1, PROPERTY_KEY, PROPERTY_1);
        assertTrue(query.getEntities().stream()
                .anyMatch(entityKeyValues -> result.equals(entityKeyValues.getAsMap())));
        assertEquals(TIME_1, query.getStartTime());
        assertEquals(TIME_2, query.getEndTime());
    }

    @Test
    public void shouldAcceptMultipleParameters() {
        // given
        QueryData<Object> query = queryData();
        // when
        testBuilder(query)
                .parameterEq(DEVICE_1 + "/" + PROPERTY_1)
                .parameterEq(DEVICE_2 + "/" + PROPERTY_2)
                .timeWindow(TIME_1, TIME_2)
                .build();
        // then
        verifyForMultipleParameters(query);
    }

    @Test
    public void shouldAcceptMultipleParametersAsArray() {
        // given
        QueryData<Object> query = queryData();
        // when
        testBuilder(query)
                .parameterIn(DEVICE_1 + "/" + PROPERTY_1, DEVICE_2 + "/" + PROPERTY_2)
                .timeWindow(TIME_1, TIME_2)
                .build();
        // then
        verifyForMultipleParameters(query);
    }

    @Test
    public void shouldAcceptMultipleParametersAsList() {
        // given
        QueryData<Object> query = queryData();
        // when
        testBuilder(query)
                .parameterIn(ImmutableList.of(DEVICE_1 + "/" + PROPERTY_1, DEVICE_2 + "/" + PROPERTY_2))
                .timeWindow(TIME_1, TIME_2)
                .build();
        // then
        verifyForMultipleParameters(query);
    }

    @Test
    public void shouldAcceptParameterAndAlias() {
        // given
        String alias = "alias";
        String field = "field";
        QueryData<Object> query = queryData();
        // when
        testBuilder(query).parameterEq(DEVICE_1 + "/" + PROPERTY_1)
                .timeWindow(TIME_1, TIME_2)
                .fieldAliases(alias, field)
                .build();
        // then
        Map<String, Object> result = ImmutableMap.of(DEVICE_KEY, DEVICE_1, PROPERTY_KEY, PROPERTY_1);
        assertTrue(query.getEntities().stream()
                .anyMatch(entityKeyValues -> result.equals(entityKeyValues.getAsMap())));
        assertEquals(TIME_1, query.getStartTime());
        assertEquals(TIME_2, query.getEndTime());
        assertEquals(Map.of(alias, Set.of(field)), query.getAliasFields());
    }

    @ParameterizedTest
    @CsvSource({ "false, false", "true, false", "false, true", "true, true" })
    public void shouldCreateDeviceLikePropertyQuery(Boolean deviceIsPattern, Boolean propertyIsPattern) {
        // given
        QueryData<Object> query = queryData();
        DeviceStage<Object> builder = testBuilder(query);
        // when
        PropertyStage<Object> propertyStage = deviceIsPattern ?
                builder.deviceLike(DEVICE_1) :
                builder.deviceEq(DEVICE_1);

        if (propertyIsPattern) {
            propertyStage.propertyLike(PROPERTY_1).timeWindow(TIME_1, TIME_2).build();
        } else {
            propertyStage.propertyEq(PROPERTY_1).timeWindow(TIME_1, TIME_2).build();
        }
        // then
        Set<EntityKeyValue> result = ImmutableSet.of(
                EntityKeyValue.builder().key(DEVICE_KEY).value(DEVICE_1).wildcard(deviceIsPattern).build(),
                EntityKeyValue.builder().key(PROPERTY_KEY).value(PROPERTY_1).wildcard(propertyIsPattern).build());
        assertTrue(query.getEntities().stream()
                .anyMatch(entityKeyValues -> {
                    Set<EntityKeyValue> keyValues = new HashSet<>(entityKeyValues.getKeyValues().values());
                    return result.equals(keyValues);
                }));
        assertEquals(TIME_1, query.getStartTime());
        assertEquals(TIME_2, query.getEndTime());
    }

    void verifyForMultipleParameters(QueryData<Object> query) {
        assertNotNull(query);
        Map<String, Object> result1 = ImmutableMap.of(DEVICE_KEY, DEVICE_1, PROPERTY_KEY, PROPERTY_1);
        Map<String, Object> result2 = ImmutableMap.of(DEVICE_KEY, DEVICE_2, PROPERTY_KEY, PROPERTY_2);
        assertTrue(
                query.getEntities().stream().anyMatch(entityKeyValues -> result1.equals(entityKeyValues.getAsMap())));
        assertTrue(
                query.getEntities().stream().anyMatch(entityKeyValues -> result2.equals(entityKeyValues.getAsMap())));
        assertEquals(TIME_1, query.getStartTime());
        assertEquals(TIME_2, query.getEndTime());
    }

    public Object[] wrongParameterFormattedStrings() {
        return new Object[] { DEVICE_1 + "#" + PROPERTY_1, DEVICE_1 + "#/" + PROPERTY_1, DEVICE_1 + "/#" + PROPERTY_1,
                DEVICE_1 + "$/" + PROPERTY_1, DEVICE_1 + "/$" + PROPERTY_1, DEVICE_1 + "*/*" + PROPERTY_1,
                DEVICE_1 + "/*" + PROPERTY_1,

                "/" + PROPERTY_1, DEVICE_1 + "/", DEVICE_1 + "/" + PROPERTY_1 + "/",
                "/" + DEVICE_1 + "/" + PROPERTY_1 };
    }

    private QueryData<Object> queryData() {
        return new QueryData<>(x -> 1);
    }

    private DeviceStage<Object> testBuilder(QueryData<Object> queryData) {
        return StageSequenceDeviceProperty.sequence()
                .apply(queryData).system(SYSTEM);
    }
}