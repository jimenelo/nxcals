/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.api.extraction.data.builders.fluent.v2;

import cern.nxcals.api.extraction.data.builders.fluent.QueryData;
import cern.nxcals.common.domain.EntityKeyValue;
import cern.nxcals.common.domain.EntityKeyValues;
import com.google.common.collect.ImmutableMap;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.TestInstance.Lifecycle.PER_CLASS;

/**
 * Test suite for {@link StageSequenceEntities}.
 */
@TestInstance(PER_CLASS)
public class StageSequenceEntitiesTest {
    private static final String SYSTEM_1 = "CMW";

    private static final String KEY_1 = "KEY_1";
    private static final String KEY_2 = "KEY_2";
    private static final String VALUE_1 = "VALUE_1";
    private static final String VALUE_2 = "VALUE_2";
    private static final Map<String, Object> KEY_VALUES = ImmutableMap.of(KEY_1, VALUE_1, KEY_2, VALUE_2);

    private static final Instant TIME_1 = Instant.ofEpochSecond(1505313230, 123456789);
    private static final Instant TIME_2 = Instant.ofEpochSecond(1505316830, 123456789);
    private static final long ENTITY_ID_1 = 1L;
    private static final long ENTITY_ID_2 = 2L;

    @Test
    public void shouldCreateKeyValueQuery() {
        // given
        String field = "field";
        String alias = "alias";
        QueryData<Object> query = queryData();
        // when
        testBuilder(query).system(SYSTEM_1)
                .keyValuesEq(Map.of(KEY_1, VALUE_1))
                .timeWindow(TIME_1, TIME_2)
                .fieldAliases(alias, field)
                .build();
        // then
        assertEquals(SYSTEM_1, query.getSystem());
        assertTrue(query.getEntities().stream()
                .anyMatch(entityKeyValues -> KEY_1.equals(entityKeyValues.get().iterator().next().getKey())));
        assertEquals(TIME_1, query.getStartTime());
        assertEquals(TIME_2, query.getEndTime());
        assertEquals(Map.of(alias, Set.of(field)), query.getAliasFields());
    }

    @Test
    public void shouldCreateKeyValuesQueryWithParamsAsArray() {
        // given
        String field = "field";
        String alias = "alias";
        QueryData<Object> query = queryData();
        // when
        testBuilder(query).system(SYSTEM_1)
                .keyValuesIn(Map.of(KEY_1, VALUE_1), Map.of(KEY_2, VALUE_2))
                .timeWindow(TIME_1, TIME_2)
                .fieldAliases(alias, field)
                .build();
        // then
        assertEquals(SYSTEM_1, query.getSystem());
        assertEquals(2, query.getEntities().size());
        assertEquals(Set.of(
                EntityKeyValue.builder().key(KEY_1).value(VALUE_1).wildcard(false).build(),
                EntityKeyValue.builder().key(KEY_2).value(VALUE_2).wildcard(false).build()
        ), query.getEntities().stream().flatMap(kvs -> kvs.getKeyValues().values().stream())
                .collect(Collectors.toSet()));
        assertEquals(TIME_1, query.getStartTime());
        assertEquals(TIME_2, query.getEndTime());
        assertEquals(Map.of(alias, Set.of(field)), query.getAliasFields());
    }

    @Test
    public void shouldCreateKeyValuesQueryWithParamsAsList() {
        // given
        String field = "field";
        String alias = "alias";
        QueryData<Object> query = queryData();
        // when
        testBuilder(query).system(SYSTEM_1)
                .keyValuesIn(List.of(Map.of(KEY_1, VALUE_1), Map.of(KEY_2, VALUE_2)))
                .timeWindow(TIME_1, TIME_2)
                .fieldAliases(alias, field)
                .build();
        // then
        assertEquals(SYSTEM_1, query.getSystem());
        assertEquals(2, query.getEntities().size());
        assertEquals(Set.of(
                EntityKeyValue.builder().key(KEY_1).value(VALUE_1).wildcard(false).build(),
                EntityKeyValue.builder().key(KEY_2).value(VALUE_2).wildcard(false).build()
        ), query.getEntities().stream().flatMap(kvs -> kvs.getKeyValues().values().stream())
                .collect(Collectors.toSet()));
        assertEquals(TIME_1, query.getStartTime());
        assertEquals(TIME_2, query.getEndTime());
        assertEquals(Map.of(alias, Set.of(field)), query.getAliasFields());
    }

    @Test
    public void shouldCreateKeyValuesWithWildcardsQuery() {
        // given
        QueryData<Object> query = queryData();
        // when
        testBuilder(query).system(SYSTEM_1)
                .keyValuesLike(KEY_VALUES)
                .timeWindow(TIME_1, TIME_2)
                .build();
        // then
        assertEquals(SYSTEM_1, query.getSystem());
        assertTrue(query.getEntities().stream()
                .anyMatch(entityKeyValues -> KEY_VALUES.equals(entityKeyValues.getAsMap())));
        assertEquals(TIME_1, query.getStartTime());
        assertEquals(TIME_2, query.getEndTime());
    }

    @Test
    public void shouldUseKeyValuesLikeOverKeyValuesQuery() {
        // given
        QueryData<Object> query = queryData();
        // when
        testBuilder(query).system(SYSTEM_1)
                .keyValuesLike(KEY_VALUES)
                .keyValuesEq(KEY_VALUES)
                .timeWindow(TIME_1, TIME_2).build();
        // then
        assertEquals(SYSTEM_1, query.getSystem());
        assertEquals(2, query.getEntities().size());
        assertTrue(query.getEntities().stream()
                .anyMatch(entityKeyValues -> KEY_VALUES.equals(entityKeyValues.getAsMap())));
        assertTrue(query.getEntities().stream().anyMatch(EntityKeyValues::isAnyWildcard));
        assertEquals(TIME_1, query.getStartTime());
        assertEquals(TIME_2, query.getEndTime());
    }

    @Test
    public void shouldCreateQueryFromEntityId() {
        // given
        QueryData<Object> query = queryData();
        // when
        testBuilder(query).system(SYSTEM_1)
                .idEq(ENTITY_ID_1)
                .timeWindow(TIME_1, TIME_2).build();
        // then
        assertEquals(Set.of(ENTITY_ID_1), query.getEntityIds());
    }

    @ParameterizedTest
    @MethodSource("idQueryBuilders")
    public void shouldCreateQueryFromEntityIds(Consumer<QueryData<Object>> builder) {
        // given
        QueryData<Object> query = queryData();
        // when
        builder.accept(query);
        // then
        assertEquals(Set.of(ENTITY_ID_1, ENTITY_ID_2), query.getEntityIds());
    }

    static Object[] idQueryBuilders() {
        List<Consumer<QueryData<Object>>> consumers = List.of(
                (query) -> testBuilder(query).system(SYSTEM_1)
                        .idEq(ENTITY_ID_1).idIn(ENTITY_ID_2)
                        .timeWindow(TIME_1, TIME_2).build(),
                (query) -> testBuilder(query).system(SYSTEM_1)
                        .idEq(ENTITY_ID_1).idEq(ENTITY_ID_2)
                        .timeWindow(TIME_1, TIME_2).build(),
                (query) -> testBuilder(query).system(SYSTEM_1)
                        .idIn(ENTITY_ID_1).idIn(ENTITY_ID_2)
                        .timeWindow(TIME_1, TIME_2).build(),
                (query) -> testBuilder(query)
                        .idIn(ENTITY_ID_1).idIn(ENTITY_ID_2)
                        .timeWindow(TIME_1, TIME_2).build(),
                (query) -> testBuilder(query)
                        .idEq(ENTITY_ID_1).idEq(ENTITY_ID_2)
                        .timeWindow(TIME_1, TIME_2).build(),
                (query) -> testBuilder(query).idEq(ENTITY_ID_1)
                        .system(SYSTEM_1).idEq(ENTITY_ID_2)
                        .timeWindow(TIME_1, TIME_2).build(),
                (query) -> testBuilder(query).idEq(ENTITY_ID_1)
                        .idEq(ENTITY_ID_2).system(SYSTEM_1)
                        .timeWindow(TIME_1, TIME_2).build(),
                (query) -> testBuilder(query).system(SYSTEM_1)
                        .idIn(ENTITY_ID_2, ENTITY_ID_1)
                        .timeWindow(TIME_1, TIME_2).build(),
                (query) -> testBuilder(query).system(SYSTEM_1)
                        .idIn(Set.of(ENTITY_ID_2, ENTITY_ID_1))
                        .timeWindow(TIME_1, TIME_2).build(),
                (query) -> testBuilder(query)
                        .idIn(Set.of(ENTITY_ID_2, ENTITY_ID_1))
                        .timeWindow(TIME_1, TIME_2).build(),
                (query) -> testBuilder(query)
                        .idIn(Set.of(ENTITY_ID_2, ENTITY_ID_1)).idEq(ENTITY_ID_1)
                        .timeWindow(TIME_1, TIME_2).build(),
                (query) -> testBuilder(query).system(SYSTEM_1)
                        .idIn(Set.of(ENTITY_ID_2, ENTITY_ID_1)).idEq(ENTITY_ID_1)
                        .timeWindow(TIME_1, TIME_2).build(),
                (query) -> testBuilder(query)
                        .idIn(Set.of(ENTITY_ID_2, ENTITY_ID_1))
                        .system(SYSTEM_1).idEq(ENTITY_ID_1)
                        .timeWindow(TIME_1, TIME_2).build()
        );
        return consumers.toArray();
    }

    private QueryData<Object> queryData() {
        return new QueryData<>(x -> 1);
    }

    static SystemOrIdStage<KeyValueStage<Object>, KeyValueStageLoop<Object>, Object> testBuilder(
            QueryData<Object> queryData) {
        return StageSequenceEntities.sequence().apply(queryData);
    }
}
