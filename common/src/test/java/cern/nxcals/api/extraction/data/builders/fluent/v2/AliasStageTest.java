package cern.nxcals.api.extraction.data.builders.fluent.v2;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class AliasStageTest {
    private static final String alias = "alias";
    private static final String field1 = "field1.0";
    private static final String field2 = "field1.1";
    private static final Set<String> fields = Set.of(field1, field2);
    private static final Map<String, Set<String>> aliases = Map.of(alias, fields);

    private static AliasStageImpl builder() {
        return new AliasStageImpl();
    }

    @Test
    void shouldAddAliasToTwoFieldsPassedBySet() {
        AliasStageImpl builder = builder().fieldAliases(alias, fields);
        assertEquals(fields, builder.getAliases().get(alias));
    }

    @Test
    void shouldAddAliasToTwoFieldsPassedOneByOne() {
        AliasStageImpl builder = builder().fieldAliases(alias, field1).fieldAliases(alias, field2);
        assertEquals(fields, builder.getAliases().get(alias));
    }

    @Test
    void shouldAddAliasToTwoFieldsPassedAsVarargs() {
        AliasStageImpl builder = builder().fieldAliases(alias, field1, field2);
        assertEquals(fields, builder.getAliases().get(alias));
    }

    @Test
    void shouldAddAliasToTwoFieldsPassedAsMap() {
        AliasStageImpl builder = builder().fieldAliases(aliases);
        assertEquals(aliases, builder.getAliases());
    }

    static class AliasStageImpl implements AliasStage<AliasStageImpl> {
        @Getter
        private final Map<String, Set<String>> aliases = new HashMap<>();

        public AliasStageImpl fieldAliases(@NotNull Map<String, Set<String>> aliasFieldsMap) {
            aliasFieldsMap.forEach((key, value) -> {
                Set<String> newValue = aliases.getOrDefault(key, new HashSet<>());
                newValue.addAll(value);
                aliases.put(key, newValue);
            });
            return this;
        }
    }
}
