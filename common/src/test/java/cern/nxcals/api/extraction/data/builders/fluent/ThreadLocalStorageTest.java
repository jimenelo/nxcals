package cern.nxcals.api.extraction.data.builders.fluent;

import cern.nxcals.common.domain.CallDetails;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ThreadLocalStorageTest {

    @Test
    void shouldRetrieveCallDetails() {
        //given
        CallDetails callDetails = CallDetails.builder().applicationName("Application").build();

        //when
        ThreadLocalStorage.setCallDetails(callDetails);
        CallDetails actualCallDetails = ThreadLocalStorage.getCallDetails();
        ThreadLocalStorage.clear();

        //then
        assertEquals(callDetails, actualCallDetails);
        assertNull(ThreadLocalStorage.getCallDetails());
    }
}