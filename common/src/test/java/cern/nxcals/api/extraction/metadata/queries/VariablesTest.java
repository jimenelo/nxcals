package cern.nxcals.api.extraction.metadata.queries;

import cern.nxcals.api.domain.VariableDeclaredType;
import org.junit.jupiter.api.Test;

import static cern.nxcals.common.utils.RSQLUtils.toRSQL;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class VariablesTest {
    private String varNameWithWildcards = "VAR_1%";

    @Test
    public void shouldCreateCondition(){
        String stringCond = toRSQL(
                        Variables.suchThat().id().eq(1L).and()
                        .variableName().like("%").and()
                                .description().doesNotExist().and()
                                .systemId().in(1L,2L).and()
                                .systemName().like("SYSTEM").and().declaredType().eq(VariableDeclaredType.NUMERIC));

        assertEquals(6, stringCond.split(";").length);
    }

    @Test
    public void shouldNotEscapeWhenUsingEq() {
        //when
        String stringCond = toRSQL(Variables.suchThat().variableName().eq(varNameWithWildcards));
        //then
        assertTrue(stringCond.contains(varNameWithWildcards));
    }

    @Test
    public void shouldNotEscapeWhenUsingNe() {
        //when
        String stringCond = toRSQL(Variables.suchThat().variableName().ne(varNameWithWildcards));
        //then
        assertTrue(stringCond.contains(varNameWithWildcards));
    }

    @Test
    public void shouldNotEscapeWhenUsingLike() {
        //when
        String stringCond = toRSQL(Variables.suchThat().variableName().like(varNameWithWildcards));
        //then
        assertTrue(stringCond.contains(varNameWithWildcards));
    }

    @Test
    public void shouldNotEscapeWhenUsingNotLike() {
        //when
        String stringCond = toRSQL(Variables.suchThat().variableName().notLike(varNameWithWildcards));
        //then
        assertTrue(stringCond.contains(varNameWithWildcards));
    }
}