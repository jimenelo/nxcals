/*
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN),All Rights Reserved.
 */

package cern.nxcals.api.extraction.data.builders.fluent;

import cern.nxcals.api.utils.TimeUtils;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.time.Instant;
import java.util.Map;
import java.util.function.Function;

import static cern.nxcals.api.utils.TimeUtils.getInstantFromString;
import static cern.nxcals.api.utils.TimeUtils.getNanosFromInstant;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;

public class StageSequenceVariablesTest {

    private static final String SYSTEM = "SYSTEM_NAME";

    private static final String TIME_1_STRING = "2017-09-13 14:33:50.123456789";
    private static final String TIME_2_STRING = "2017-09-13 15:33:50.123456789";

    private static final Instant TIME_1 = getInstantFromString(TIME_1_STRING);
    private static final Instant TIME_2 = getInstantFromString(TIME_2_STRING);

    private static final long TIME_1_NANOS = getNanosFromInstant(TIME_1);
    private static final long TIME_2_NANOS = getNanosFromInstant(TIME_2);

    private static final String VARIABLE = "VARIABLE_NAME";
    private static final String VARIABLE_WITH_WILDCARD = "VARI%LE_NAME";
    private static final Boolean VARIABLE_WILDCARD_DISABLED = false;
    private static final Boolean VARIABLE_WILDCARD_ENABLED = true;
    private final static Map<String, Boolean> VARIABLE_MAP_SINGLE = ImmutableMap
            .of(VARIABLE, VARIABLE_WILDCARD_DISABLED);
    private final static Map<String, Boolean> VARIABLE_MAP_SINGLE_WILDCARD = ImmutableMap
            .of(VARIABLE_WITH_WILDCARD, VARIABLE_WILDCARD_ENABLED);

    @Test
    public void shouldThrowWhenSystemIsNull() {
        assertThrows(NullPointerException.class, () -> builder(null));
    }

    @Test
    public void shouldCreateVariableQueryFromInstant() {
        QueryData<Dataset<Row>> query = builder(SYSTEM).startTime(TIME_1).endTime(TIME_2).variable(VARIABLE).data();

        isValid(query, TIME_1, TIME_2);
    }

    @Test
    public void shouldCreateVariableQueryWithWildcard() {
        QueryData<Dataset<Row>> query = builder(SYSTEM).startTime(TIME_1).endTime(TIME_2)
                .variableLike(VARIABLE_WITH_WILDCARD).data();

        isValidWithWildcard(query, TIME_1, TIME_2);
    }

    @Test
    public void shouldCreateVariableQueryFromString() {
        QueryData<Dataset<Row>> query = builder(SYSTEM).startTime(TIME_1_STRING).endTime(TIME_2_STRING)
                .variable(VARIABLE).data();

        isValid(query, TIME_1, TIME_2);
    }

    @Test
    public void shouldCreateVariableQueryFromNanos() {
        QueryData<Dataset<Row>> query = builder(SYSTEM).startTime(TIME_1_NANOS).endTime(TIME_2_NANOS).variable(VARIABLE)
                .data();

        isValid(query, TIME_1, TIME_2);
    }

    @Test
    public void shouldAllowAlias() {
        QueryData<Dataset<Row>> query = builder(SYSTEM).startTime(TIME_1_NANOS).endTime(TIME_2_NANOS)
                .fieldAliases(ImmutableMap.of("ALIAS1", Lists.newArrayList("field1", "field2")))
                .fieldAliases("ALIAS2", Lists.newArrayList("field3", "field4"))
                .fieldAlias("ALIAS2", "field5")
                .fieldAlias("ALIAS3", "field6")
                .variable(VARIABLE).variable(VARIABLE_WITH_WILDCARD).data();

        assertEquals(ImmutableSet.of("field1", "field2"), query.getAliasFields().get("ALIAS1"));
        assertEquals(ImmutableSet.of("field3", "field4", "field5"), query.getAliasFields().get("ALIAS2"));
        assertEquals(ImmutableSet.of("field6"), query.getAliasFields().get("ALIAS3"));

        assertTrue(query.getVariables().containsKey(VARIABLE));
        assertTrue(query.getVariables().containsKey(VARIABLE_WITH_WILDCARD));
    }

    @Test
    public void shouldAllowMultipleVariables() {
        QueryData<Dataset<Row>> query = builder(SYSTEM).startTime(TIME_1_NANOS).endTime(TIME_2_NANOS).variable(VARIABLE)
                .variable(VARIABLE_WITH_WILDCARD).data();
        assertTrue(query.getVariables().containsKey(VARIABLE));
        assertTrue(query.getVariables().containsKey(VARIABLE_WITH_WILDCARD));
    }

    @Test
    public void shouldAllowMultipleVariablesWithSingleCall() {
        QueryData<Dataset<Row>> query = builder(SYSTEM).startTime(TIME_1_NANOS).endTime(TIME_2_NANOS)
                .variables(ImmutableList.of(VARIABLE, VARIABLE_WITH_WILDCARD)).data();
        assertTrue(query.getVariables().containsKey(VARIABLE));
        assertTrue(query.getVariables().containsKey(VARIABLE_WITH_WILDCARD));
    }

    @Test
    public void shouldCreateVariableQueryAtTimeInstant() {
        QueryData<Dataset<Row>> query = builder(SYSTEM).atTime(TIME_1).variable(VARIABLE).data();

        isValid(query, TIME_1, TIME_1);
    }

    @Test
    public void shouldCreateVariableQueryAtTimeString() {
        QueryData<Dataset<Row>> query = builder(SYSTEM).atTime(TIME_1_STRING).variable(VARIABLE).data();

        isValid(query, TIME_1, TIME_1);
    }

    @Test
    public void shouldCreateVariableQueryAtTimeNanos() {
        QueryData<Dataset<Row>> query = builder(SYSTEM).atTime(TIME_1_NANOS).variable(VARIABLE).data();

        isValid(query, TIME_1, TIME_1);
    }

    @Test
    public void shouldCreateVariableQueryAtTimeWithDuration() {
        Duration duration = Duration.ofSeconds(300);
        QueryData<Dataset<Row>> query = builder(SYSTEM).startTime(TIME_1).duration(duration).variable(VARIABLE).data();

        isValid(query, TIME_1, TIME_1.plus(duration));
    }

    @Test
    public void shouldCreateVariableQueryAtTimeWithDurationNanos() {
        long durationNanos = 300L * TimeUtils.CONVERT_EPOCH_NANOS_TO_SECONDS_VALUE;
        QueryData<Dataset<Row>> query = builder(SYSTEM).startTime(TIME_1).duration(durationNanos).variable(VARIABLE)
                .data();

        isValid(query, TIME_1, TIME_1.plusNanos(durationNanos));
    }

    private void isValid(QueryData<Dataset<Row>> query, Instant startTime, Instant endTime) {
        assertNotNull(query);
        assertEquals(SYSTEM, query.getSystem());
        assertEquals(VARIABLE_MAP_SINGLE, query.getVariables());
        assertEquals(startTime, query.getStartTime());
        assertEquals(endTime, query.getEndTime());
    }

    private void isValidWithWildcard(QueryData<Dataset<Row>> query, Instant startTime, Instant endTime) {
        assertNotNull(query);
        assertEquals(SYSTEM, query.getSystem());
        assertEquals(VARIABLE_MAP_SINGLE_WILDCARD, query.getVariables());
        assertEquals(startTime, query.getStartTime());
        assertEquals(endTime, query.getEndTime());
    }

    private TimeStartStage<VariableAliasStage<Dataset<Row>>, Dataset<Row>> builder(String system) {
        Function<QueryData<Dataset<Row>>, Dataset<Row>> producer = queryData -> mock(Dataset.class);

        return StageSequenceVariables.<Dataset<Row>>sequence()
                .apply(new QueryData<Dataset<Row>>(producer)).system(system);
    }
}
