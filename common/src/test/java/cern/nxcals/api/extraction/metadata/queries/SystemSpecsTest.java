package cern.nxcals.api.extraction.metadata.queries;

import org.junit.jupiter.api.Test;

import static cern.nxcals.common.utils.RSQLUtils.toRSQL;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class SystemSpecsTest {

    @Test
    public void shouldCreateCondition() {
        String rsql = toRSQL(SystemSpecs.suchThat().id().eq(1L).and().name().notLike("CMW"));

        assertEquals(2, rsql.split(";").length);
    }
}