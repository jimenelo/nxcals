package cern.nxcals.api.custom.domain.snapshot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.TestInstance.Lifecycle.PER_CLASS;

@TestInstance(PER_CLASS)
class PriorTimeTest {
    @ParameterizedTest
    @MethodSource("previousWeekDayParams")
    void shouldGetPreviousWeekDay(LocalDateTime refDate, DayOfWeek previousWeekDay, LocalDateTime expected) {
        assertThat(PriorTime.previousWeekDay(refDate, previousWeekDay)).isEqualTo(expected);
    }

    Object[][] previousWeekDayParams() {
        return new Object[][]{
                new Object[]{dateTimeFromString("10/10/2020 12:34:56"),
                        DayOfWeek.THURSDAY, dateTimeFromString("08/10/2020 00:00:00")},
                new Object[]{dateTimeFromString("07/10/2020 12:34:56"),
                        DayOfWeek.SATURDAY, dateTimeFromString("03/10/2020 00:00:00")},
                new Object[]{dateTimeFromString("07/10/2020 12:34:56"),
                        DayOfWeek.WEDNESDAY, dateTimeFromString("07/10/2020 00:00:00")},
        };
    }

    @ParameterizedTest
    @MethodSource("getEndTimeParams")
    void shouldGetEndTime(LocalDateTime refTime, String priorTime, LocalDateTime expected) {
        assertThat(PriorTime.getEndTime(priorTime, refTime)).isEqualTo(expected);
    }

    Object[][] getEndTimeParams() {
        return new Object[][]{
                new Object[]{dateTimeFromString("02/04/2024 12:34:56"),
                        PriorTime.START_OF_HOUR.toString(), dateTimeFromString("02/04/2024 12:00:00")},
                new Object[]{dateTimeFromString("02/04/2024 12:34:56"),
                        PriorTime.START_OF_DAY.toString(), dateTimeFromString("02/04/2024 00:00:00")},
                new Object[]{dateTimeFromString("02/04/2024 12:34:56"),
                        PriorTime.START_OF_MONTH.toString(), dateTimeFromString("01/04/2024 00:00:00")},
                new Object[]{dateTimeFromString("02/04/2024 12:34:56"),
                        PriorTime.START_OF_YEAR.toString(), dateTimeFromString("01/01/2024 00:00:00")},
                new Object[]{dateTimeFromString("02/04/2024 12:34:56"),
                        PriorTime.PREVIOUS_MONDAY.toString(), dateTimeFromString("01/04/2024 00:00:00")},
                new Object[]{dateTimeFromString("02/04/2024 12:34:56"),
                        PriorTime.PREVIOUS_TUESDAY.toString(), dateTimeFromString("02/04/2024 00:00:00")},
                new Object[]{dateTimeFromString("02/04/2024 12:34:56"),
                        PriorTime.PREVIOUS_WEDNESDAY.toString(), dateTimeFromString("27/03/2024 00:00:00")},
                new Object[]{dateTimeFromString("02/04/2024 12:34:56"),
                        PriorTime.PREVIOUS_THURSDAY.toString(), dateTimeFromString("28/03/2024 00:00:00")},
                new Object[]{dateTimeFromString("02/04/2024 12:34:56"),
                        PriorTime.PREVIOUS_FRIDAY.toString(), dateTimeFromString("29/03/2024 00:00:00")},
                new Object[]{dateTimeFromString("02/04/2024 12:34:56"),
                        PriorTime.PREVIOUS_SATURDAY.toString(), dateTimeFromString("30/03/2024 00:00:00")},
                new Object[]{dateTimeFromString("02/04/2024 12:34:56"),
                        PriorTime.PREVIOUS_SUNDAY.toString(), dateTimeFromString("31/03/2024 00:00:00")},
        };
    }

    @Test
    void shouldConvertFromString() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            PriorTime.from("UNKNOWN");
        });
    }

    private static LocalDateTime dateTimeFromString(String dateInString) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
        return LocalDateTime.parse(dateInString, formatter);
    }
}

