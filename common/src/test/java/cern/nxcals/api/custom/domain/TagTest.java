package cern.nxcals.api.custom.domain;

import cern.nxcals.api.domain.Visibility;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class TagTest {

    @Test
    void shouldCreateTag() {
        Tag tag = Tag.builder().name("tag").owner("owner").system("cmw").timestamp(Instant.now())
                .visibility(Visibility.PUBLIC).description("desc").properties(Collections.emptyMap()).build();

        assertEquals("tag", tag.getName());
        assertEquals("owner", tag.getOwner());
        assertEquals("cmw", tag.getSystem());
        assertEquals("desc", tag.getDescription());
        assertEquals(Visibility.PUBLIC, tag.getVisibility());
        assertNotNull(tag.getProperties());
    }
}