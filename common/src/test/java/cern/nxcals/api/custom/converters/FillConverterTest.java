package cern.nxcals.api.custom.converters;


import cern.nxcals.api.custom.domain.Fill;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.extraction.thin.BeamMode;
import cern.nxcals.api.extraction.thin.FillData;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FillConverterTest {

    @Test
    public void shouldConvertToFill() {
        //given
        String beamModeValue = "VALUE";
        //when
        Fill fill = FillConverter.toFill(createFillData(beamModeValue));
        //then
        assertEquals(beamModeValue, fill.getBeamModes().get(0).getBeamModeValue());
    }

    @Test
    public void shouldConvertToFillList() {
        //given
        String beamModeValue = "VALUE";

        List<FillData> fillsData = new ArrayList<>();
        fillsData.add(FillData.newBuilder()
                .addBeamModes(BeamMode.newBuilder().setBeamModeValue(beamModeValue).build())
                .build());
        //when
        List<Fill> fills = FillConverter.toFillList(fillsData);

        //them
        assertEquals(1, fills.size());
        assertEquals(beamModeValue, fills.get(0).getBeamModes().get(0).getBeamModeValue());
    }

    @Test
    public void shouldConvertToFillData() {
        //given
        int fillNumber = 1;
        String beamModeValue = "VALUE";

        List<cern.nxcals.api.custom.domain.BeamMode> beamModes = new ArrayList<>();
        beamModes.add(new cern.nxcals.api.custom.domain.BeamMode(TimeWindow.between(0, 1),
                beamModeValue));

        Fill fill = new Fill(fillNumber, TimeWindow.between(0, 1), beamModes);

        //when
        FillData fillData = FillConverter.toFillData(fill);

        //then
        assertEquals(fillNumber, fillData.getNumber());
        assertEquals(1, fillData.getBeamModesCount());
        assertEquals(beamModeValue, fillData.getBeamModes(0).getBeamModeValue());
    }

    @Test
    public void shouldConvertToFillsData() {
        //given
        int numberOfFills = 10;
        List<Fill> fills = new ArrayList<>();
        String beamModeValue = "VALUE";

        for (int i = 0; i < numberOfFills; i++) {
            List<cern.nxcals.api.custom.domain.BeamMode> beamModes = new ArrayList<>();
            beamModes.add(new cern.nxcals.api.custom.domain.BeamMode(TimeWindow.between(i, i + 1),
                    beamModeValue));

            Fill fill = new Fill(1, TimeWindow.between(i, i + 1), beamModes);
            fills.add(fill);
        }

        //when
        List<FillData> fillsData = FillConverter.toFillsData(fills);

        //then
        assertEquals(numberOfFills, fillsData.size());
        assertEquals(beamModeValue, fillsData.get(0).getBeamModes(0).getBeamModeValue());
    }

    private FillData createFillData(String beamModeValue) {
        BeamMode beamMode = BeamMode.newBuilder().setBeamModeValue(beamModeValue).build();
        return FillData.newBuilder().addBeamModes(beamMode).build();
    }
}
