package cern.nxcals.api.utils;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TimeUtilsTest {

    static Stream<Arguments> timeSplits() {
        return Stream.of(
                Arguments.of("2024-01-01 00:00:00.000", 0, TimeUtils.SECONDS_IN_DAY),
                Arguments.of("2024-01-01 00:00:00.000", 0, TimeUtils.TimeSplits.FIVE_MINUTE_SPLIT.getSize()),
                Arguments.of("2024-01-01 00:00:00.000", 0, 4),
                Arguments.of("2024-01-01 11:59:59.999999", 0, 2),
                Arguments.of("2024-01-01 12:00:00.000", 1, 2),
                Arguments.of("2024-01-01 12:00:00.000", 6, 12),
                Arguments.of("2024-01-01 12:00:00.000", 24, 48),
                Arguments.of("2024-01-01 23:59:59.999999", 1, 2),
                Arguments.of("2024-01-01 23:59:59.999999", 287, TimeUtils.TimeSplits.FIVE_MINUTE_SPLIT.getSize()),
                Arguments.of("2024-01-01 23:59:59.999999", TimeUtils.SECONDS_IN_DAY - 1, TimeUtils.SECONDS_IN_DAY),
                Arguments.of("2024-01-01 23:59:59.999999", TimeUtils.SECONDS_IN_DAY / 2 - 1,
                        TimeUtils.SECONDS_IN_DAY / 2),

                Arguments.of("2024-01-01 00:04:59.999999999", 0, TimeUtils.TimeSplits.FIVE_MINUTE_SPLIT.getSize()),
                Arguments.of("2024-01-01 00:05:00.000", 1, TimeUtils.TimeSplits.FIVE_MINUTE_SPLIT.getSize()),
                Arguments.of("2024-01-01 00:09:59.999999", 1, TimeUtils.TimeSplits.FIVE_MINUTE_SPLIT.getSize()),
                Arguments.of("2024-01-01 00:10:00.000", 2, TimeUtils.TimeSplits.FIVE_MINUTE_SPLIT.getSize()),
                Arguments.of("2024-01-01 10:00:00.000", 10 * 12 + 0, TimeUtils.TimeSplits.FIVE_MINUTE_SPLIT.getSize()),
                Arguments.of("2024-01-01 10:05:00.000", 10 * 12 + 1, TimeUtils.TimeSplits.FIVE_MINUTE_SPLIT.getSize()),
                Arguments.of("2024-01-01 10:09:59.999999", 10 * 12 + 1,
                        TimeUtils.TimeSplits.FIVE_MINUTE_SPLIT.getSize()),
                Arguments.of("2024-01-01 15:09:59.999999", 15 * 12 + 1,
                        TimeUtils.TimeSplits.FIVE_MINUTE_SPLIT.getSize()),
                Arguments.of("2024-01-01 15:29:59.999999", 15 * 12 + 5,
                        TimeUtils.TimeSplits.FIVE_MINUTE_SPLIT.getSize()),
                Arguments.of("2024-01-01 15:59:59.999999", 15 * 12 + 11,
                        TimeUtils.TimeSplits.FIVE_MINUTE_SPLIT.getSize()),
                Arguments.of("2024-01-01 23:09:59.999999", 23 * 12 + 1,
                        TimeUtils.TimeSplits.FIVE_MINUTE_SPLIT.getSize()),
                Arguments.of("2024-02-25 23:09:59.999999", 23 * 12 + 1,
                        TimeUtils.TimeSplits.FIVE_MINUTE_SPLIT.getSize())
        );
    }

    @ParameterizedTest
    @MethodSource("timeSplits")
    public void shouldCalculateCorrectTimePartition(String timestamp, long correctPartition, long maxPartitions) {
        long timePartition = TimeUtils.getTimePartitionInternal(TimeUtils.getNanosFromString(timestamp), maxPartitions);
        assertEquals(correctPartition, timePartition, () -> "Wrong value for timestamp " + timestamp);
    }


    @Test
    public void shouldGetTimestampFromInstant() {
        Instant one = TimeUtils.getInstantFromNanos(1L);

        Timestamp timestamp = TimeUtils.getTimestampFromInstant(one);

        assertEquals(timestamp.toInstant(), one);
    }

    @Test
    public void shouldGetTimestampFromNanos() {

        Timestamp timestamp = TimeUtils.getTimestampFromNanos(1L);

        assertEquals(timestamp.toInstant(), TimeUtils.getInstantFromNanos(1L));
    }

    @Test
    public void shouldGetInstant() {
        long nanos = 10_000_000_000L;

        Instant instant = TimeUtils.getInstantFromNanos(nanos);

        assertEquals(10, instant.getEpochSecond());
    }

    @Test
    public void shouldGetNanos() {
        Instant instant = Instant.ofEpochSecond(10);
        long nanos = TimeUtils.getNanosFromInstant(instant);

        assertEquals(10_000_000_000L, nanos);
    }

    @Test
    public void shouldParseString() {
        String timeString = "2000-01-02 03:04:05.2";

        Instant instant = TimeUtils.getInstantFromString(timeString);

        LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, ZoneId.of("UTC"));

        assertEquals(2000, localDateTime.getYear());
        assertEquals(Month.JANUARY, localDateTime.getMonth());
        assertEquals(2, localDateTime.getDayOfMonth());
        assertEquals(3, localDateTime.getHour());
        assertEquals(4, localDateTime.getMinute());
        assertEquals(5, localDateTime.getSecond());
        assertEquals(200000000, localDateTime.getNano());
    }

    @Test
    public void shouldFormatString() {
        LocalDateTime localDateTime = LocalDateTime.of(2000, Month.JANUARY, 2, 3, 4);
        localDateTime = localDateTime.withSecond(5);
        localDateTime = localDateTime.withNano(2);
        Instant instant = localDateTime.toInstant(ZoneOffset.UTC);

        String timeString = "2000-01-02 03:04:05.2";

        assertEquals(timeString, TimeUtils.getStringFromInstant(instant));
    }

    @Test
    public void shouldGetNanosFromString() {
        String timeString = "1970-01-01 00:00:10.000";
        long nanos = 10_000_000_000L;
        assertEquals(nanos, TimeUtils.getNanosFromString(timeString));
    }

    @Test
    public void shouldGetNanosFromNow() {
        assertEquals(ZonedDateTime.now(ZoneId.of("UTC")).toEpochSecond(), TimeUnit.NANOSECONDS.toSeconds(TimeUtils.getNanosFromNow()));
    }

    @Test
    public void shouldGetNanosFromLocal() {
        LocalDateTime localDateTime = LocalDateTime.of(1970, Month.JANUARY, 1, 0, 0, 0);
        long nanos = 3_600_000_000_000L;
        ZoneId zoneId = ZoneId.of("UTC-1");

        assertEquals(nanos, TimeUtils.getNanosFromLocal(localDateTime, zoneId));
    }
}
