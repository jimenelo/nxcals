package cern.nxcals.api.utils;

import cern.nxcals.common.utils.IllegalCharacterConverter;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static cern.nxcals.api.utils.EncodingUtils.decodeFields;
import static cern.nxcals.api.utils.EncodingUtils.desanitizeIfNeeded;
import static cern.nxcals.api.utils.EncodingUtils.encodeFields;
import static cern.nxcals.api.utils.EncodingUtils.sanitizeIfNeeded;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class EncodingUtilsTest {
    private static String encodeString(String string) {
        return IllegalCharacterConverter.get().convertToLegal(string);
    }

    private static final String pathToDecode =
            "path.to." + encodeString("decod.e") + "." + encodeString("spe.cial")
                    + ".field";
    private static final String pathToEncode = "path.to.`decod.e`.`spe.cial`.field";
    private static final String pathWithoutSpecialCharacters = "path.without.fields.to.encode";

    @Test
    void shouldCorrectlyEncodeSimpleFieldName() {
        assertEquals(pathToDecode, encodeFields(pathToEncode));
    }

    @Test
    void shouldNotEncodeIfNotNeeded() {
        assertEquals(pathWithoutSpecialCharacters,
                encodeFields(pathWithoutSpecialCharacters));
    }

    @Test
    void shouldDecodeFieldName() {
        assertEquals(pathToEncode, decodeFields(pathToDecode));
    }

    @Test
    void shouldNotDecodeFieldNameWithNotEncodedFields() {
        assertEquals(pathWithoutSpecialCharacters,
                decodeFields(pathWithoutSpecialCharacters));
    }

    @Test
    void bothOperationsShouldReverseEachOther() {
        assertAll(
                () -> assertEquals(pathToEncode,
                        decodeFields(
                                encodeFields(pathToEncode))),
                () -> assertEquals(pathToDecode,
                        encodeFields(
                                decodeFields(pathToDecode)))
        );
    }

    @Nested
    @DisplayName("Tests of EncodingUtils.decodeFieldNameWithoutEscaping")
    class DecodeFieldNameWithoutEscapingTests {
        @Test
        void shouldDecodeFieldName() {
            String baseString = "c.c";
            String encodedField = encodeString(baseString);
            assertEquals(baseString, desanitizeIfNeeded(encodedField));
        }

        @Test
        void shouldReturnStringIfIsNotEncoded() {
            String baseString = "c.c";
            assertEquals(baseString, desanitizeIfNeeded(baseString));
        }
    }

    @Nested
    @DisplayName("Tests of EncodingUtils.sanitizeIfNeeded")
    class SanitizeIfNeededTests {
        @Test
        void shouldDecodeFieldName() {
            String baseString = "c.c";
            String encodedField = sanitizeIfNeeded(baseString);
            assertTrue(IllegalCharacterConverter.isEncoded(encodedField));
            assertEquals(baseString, IllegalCharacterConverter.get().convertFromLegal(encodedField));
        }

        @Test
        void shouldReturnStringIfIsNotEncoded() {
            String baseString = "ccc";
            assertEquals(baseString, sanitizeIfNeeded(baseString));
        }
    }

}
