package cern.nxcals.api.metadata.queries;

import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.VariableDeclaredType;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.time.Instant;
import java.util.Map;

import static cern.nxcals.common.utils.RSQLUtils.toRSQL;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class VariablesTest {
    private String varNameWithWildcards = "VAR_1%";

    @Test
    public void shouldCreateCondition() {
        String stringCond = toRSQL(
                Variables.suchThat().id().eq(1L).and()
                        .variableName().like("%").and()
                        .description().doesNotExist().and()
                        .systemId().in(1L, 2L).and()
                        .systemName().like("SYSTEM").and().declaredType().eq(VariableDeclaredType.NUMERIC));

        assertEquals(6, stringCond.split(";").length);
    }

    @Test
    public void shouldNotEscapeWhenUsingEq() {
        //when
        String stringCond = toRSQL(Variables.suchThat().variableName().eq(varNameWithWildcards));
        //then
        assertTrue(stringCond.contains(varNameWithWildcards));
    }

    @Test
    public void shouldNotEscapeWhenUsingNe() {
        //when
        String stringCond = toRSQL(Variables.suchThat().variableName().ne(varNameWithWildcards));
        //then
        assertTrue(stringCond.contains(varNameWithWildcards));
    }

    @Test
    public void shouldNotEscapeWhenUsingLike() {
        //when
        String stringCond = toRSQL(Variables.suchThat().variableName().like(varNameWithWildcards));
        //then
        assertTrue(stringCond.contains(varNameWithWildcards));
    }

    @Test
    public void shouldNotEscapeWhenUsingNotLike() {
        //when
        String stringCond = toRSQL(Variables.suchThat().variableName().notLike(varNameWithWildcards));
        //then
        assertTrue(stringCond.contains(varNameWithWildcards));
    }

    @Test
    public void shouldCorrectlyCreateNestedCondition() {
        //when
        String stringCond = toRSQL(Variables.suchThat().variableName().notLike(varNameWithWildcards).or()
                .and(Variables.suchThat().variableName().eq("123"), Variables.suchThat().variableName().like("1%")));
        //then
        assertTrue(stringCond.contains(varNameWithWildcards));
    }

    public static Object[][] queries() {
        SystemSpec systemSpecs = mock(SystemSpec.class);
        when(systemSpecs.getEntityKeyDefinitions()).thenReturn(
                "{\"type\":\"record\",\"name\":\"CmwKey\",\"namespace\":\"cern.cals\",\"fields\":[{\"name\":\"device\",\"type\":\"string\"},{\"name\":\"property\",\"type\":\"string\"}]}");
        ConditionWithOptions<Variables, VariableQueryWithOptions> c1 = Variables.suchThat().id().eq(10L);
        ConditionWithOptions<Variables, VariableQueryWithOptions> c2 = Variables.suchThat().variableName().eq("name");
        ConditionWithOptions<Variables, VariableQueryWithOptions> c3 = Variables.suchThat().configsValidFromStamp()
                .after(Instant.MIN, true);
        ConditionWithOptions<Variables, VariableQueryWithOptions> c4 = Variables.suchThat().configsEntityKeyValues()
                .eq(systemSpecs, Map.of("device", "dev", "property", "prop"));
        ConditionWithOptions<Variables, VariableQueryWithOptions> c5 = Variables.suchThat().declaredType()
                .eq(VariableDeclaredType.FUNDAMENTAL);
        return new Object[][] {
                new Object[] { c1, c1.withOptions().noConfigs() },
                new Object[] { c2, c2.withOptions().noConfigs() },
                new Object[] { c3, c3.withOptions().noConfigs() },
                new Object[] { c4, c4.withOptions().noConfigs() },
                new Object[] { c5, c5.withOptions().noConfigs() },
                new Object[] {
                        c5.and().or(c1, c2),
                        c5.and().or(c1, c2).withOptions().noConfigs()
                },
        };
    }

    @ParameterizedTest
    @MethodSource("queries")
    void shouldAllowToAddOptionsAfterSpecifyingLong(ConditionWithOptions<Variables, VariableQueryWithOptions> condition,
            VariableQueryWithOptions queryWithOptions) {
        assertEquals(toRSQL(condition), toRSQL(queryWithOptions.getCondition()));
    }
}
