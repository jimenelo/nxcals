package cern.nxcals.api.metadata.queries;

import cern.nxcals.api.domain.SystemSpec;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashMap;
import java.util.Map;

import static cern.nxcals.common.utils.RSQLUtils.toRSQL;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class EntitiesTest {
    public static final String SCHEMA = "{\"type\":\"record\",\"name\":\"device\",\"fields\":[{\"name\":\"device\",\"type\":\"string\"}]}";
    @Mock
    private SystemSpec systemSpec;

    @Test
    public void shouldCreateCondition() {
        when(systemSpec.getPartitionKeyDefinitions()).thenReturn(SCHEMA);
        when(systemSpec.getEntityKeyDefinitions()).thenReturn(SCHEMA);
        Map<String, Object> keyValues = new HashMap<>();
        keyValues.put("device", "test_device");

        String rsql = toRSQL(Entities.suchThat().systemName().eq("name").and()
                        .systemId().in(1L, 2L).and()
                        .id().exists().and()
                        .keyValues().like("value").and()
                        .partitionKeyValues().ne("value").and()
                        .keyValues().like(systemSpec, keyValues).and()
                        .partitionKeyValues().ne(systemSpec, keyValues));

        assertEquals(7, rsql.split(";").length);
    }

    @Test
    public void shouldNotQuoteBackslashWithDoubleBackslashWhenConvertingToString() {
        String rsql = toRSQL(Entities.suchThat().systemName().like("\\_name\\%"));

        assertTrue(rsql.contains("\\_name\\%"));
    }

    public static Object[][] queries() {
        SystemSpec systemSpecs = mock(SystemSpec.class);
        when(systemSpecs.getEntityKeyDefinitions()).thenReturn(
                "{\"type\":\"record\",\"name\":\"CmwKey\",\"namespace\":\"cern.cals\",\"fields\":[{\"name\":\"device\",\"type\":\"string\"},{\"name\":\"property\",\"type\":\"string\"}]}");
        ConditionWithOptions<Entities, EntityQueryWithOptions> c1 = Entities.suchThat().id().eq(10L);
        ConditionWithOptions<Entities, EntityQueryWithOptions> c2 = Entities.suchThat().keyValues()
                .eq(systemSpecs, Map.of("device", "dev", "property", "prop"));
        ConditionWithOptions<Entities, EntityQueryWithOptions> c3 = Entities.suchThat().systemName().eq("");
        return new Object[][] {
                new Object[] { c1, c1.withOptions() },
                new Object[] { c2, c2.withOptions() },
                new Object[] { c3, c3.withOptions() },
                new Object[] {
                        c3.or().and(c1, c2),
                        c3.or().and(c1, c2).withOptions()
                },
        };
    }

    @ParameterizedTest
    @MethodSource("queries")
    void shouldAllowToAddOptionsAfterSpecifyingLong(ConditionWithOptions<Entities, EntityOrderOptions> condition,
            EntityQueryWithOptions queryWithOptions) {
        assertEquals(toRSQL(condition), toRSQL(queryWithOptions.getCondition()));
    }
}
