package cern.nxcals.common.domain;

import cern.nxcals.common.utils.GlobalObjectMapperProvider;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class EntityKeyValuesTest {

    @Test
    public void shouldAdd() {
        EntityKeyValues entityKeyValues = EntityKeyValues.builder()
                .keyValue(EntityKeyValue.builder().key("key").value("value").wildcard(false).build()).build();

        assertEquals(1, entityKeyValues.get().size());
    }

    @Test
    public void shouldIsWildcardBeTrue() {
        EntityKeyValues entityKeyValues = EntityKeyValues.builder()
                .keyValue(EntityKeyValue.builder().key("key").value("value").wildcard(true).build())
                .keyValue(EntityKeyValue.builder().key("key1").value("value").wildcard(false).build()).build();

        assertTrue(entityKeyValues.isAnyWildcard());
    }

    @Test
    public void shouldIsWildcardBeFalse() {
        EntityKeyValues entityKeyValues = EntityKeyValues.builder()
                .keyValue(EntityKeyValue.builder().key("key").value("value").wildcard(false).build())
                .keyValue(EntityKeyValue.builder().key("key1").value("value").wildcard(false).build()).build();
        assertFalse(entityKeyValues.isAnyWildcard());
    }

    @Test
    public void shouldIsEmptyBeFalse() {
        EntityKeyValues entityKeyValues = EntityKeyValues.builder()
                .keyValue(EntityKeyValue.builder().key("key").value("value").wildcard(false).build())
                .keyValue(EntityKeyValue.builder().key("key1").value("value").wildcard(false).build()).build();
        assertFalse(entityKeyValues.isEmpty());
    }

    @Test
    public void shouldEscapeWithLike() {
        EntityKeyValues entityKeyValues = EntityKeyValues.builder()
                .keyValue(EntityKeyValue.builder().key("key").value("\\_value_%").wildcard(true).build())
                .keyValue(EntityKeyValue.builder().key("key1").value("%value_").wildcard(false).build()).build();

        Map<String, Object> keyValuesAsMap = entityKeyValues.getAsMap();

        //This should not be escaped because it is build as a wildcard one
        assertEquals("\\_value_%", keyValuesAsMap.get("key"));
        //This should be escaped because it is not wildcard
        assertEquals("\\%value\\_", keyValuesAsMap.get("key1"));
    }

    @Test
    public void shouldThrowOnDuplicateKey() {
        assertThrows(IllegalArgumentException.class, () -> EntityKeyValues.builder()
                .keyValue(EntityKeyValue.builder().key("key").value("value").wildcard(false).build())
                .keyValue(EntityKeyValue.builder().key("key").value("value1").wildcard(true).build()).build());

    }

    @Test
    public void shouldSerialize() throws IOException {
        EntityKeyValues entityKeyValues1 = EntityKeyValues.builder()
                .keyValue(EntityKeyValue.builder().key("key").value("value").wildcard(false).build())
                .keyValue(EntityKeyValue.builder().key("key1").value("value").wildcard(false).build()).build();
        ObjectMapper ob = GlobalObjectMapperProvider.get();
        String s = ob.writeValueAsString(entityKeyValues1);
        EntityKeyValues entityKeyValues2 = ob.readValue(s, EntityKeyValues.class);

        assertEquals(entityKeyValues1.getKeyValues(), entityKeyValues2.getKeyValues());
        assertEquals(entityKeyValues1, entityKeyValues2);
    }

}
