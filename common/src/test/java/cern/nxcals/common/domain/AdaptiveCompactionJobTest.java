package cern.nxcals.common.domain;

import cern.nxcals.api.domain.TimeEntityPartitionType;
import org.apache.hadoop.fs.Path;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AdaptiveCompactionJobTest {
    private AdaptiveCompactionJob getAdaptiveCompactionJob(int timeParts, int entityBuckets) {
        return AdaptiveCompactionJob.builder()
                .partitionType(TimeEntityPartitionType.of(timeParts, entityBuckets))
                .partitionId(1)
                .schemaId(2)
                .systemId(3)
                .filePrefix("prefix")
                .destinationDir(new Path("/test").toUri())
                .sourceDir(new Path("/source").toUri())
                .files(List.of(new Path("/files").toUri()))
                .build();
    }

    @Test
    void getType() {
        //given
        AdaptiveCompactionJob job = getAdaptiveCompactionJob(2, 3);

        //when, then
        assertEquals(DataProcessingJob.JobType.ADAPTIVE_COMPACT, job.getType());
    }


    @Test
    void getPartitionCount() {
        //given
        AdaptiveCompactionJob job = getAdaptiveCompactionJob(2, 3);

        //when, then
        assertEquals(6, job.getPartitionCount());
    }
}