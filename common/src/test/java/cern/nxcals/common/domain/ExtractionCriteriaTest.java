package cern.nxcals.common.domain;

import avro.shaded.com.google.common.collect.ImmutableMap;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.common.utils.GlobalObjectMapperProvider;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.IOException;
import java.time.Instant;
import java.util.Collections;
import java.util.Map;
import java.util.Set;

import static java.time.temporal.ChronoUnit.DAYS;
import static java.util.Collections.emptyMap;
import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

class ExtractionCriteriaTest {

    @ParameterizedTest
    @MethodSource("isVariableQueryTestsParams")
    void shouldCorrectlyIndicateIfItIsVariableQuery(Map<String, Boolean> variables, Set<Long> ids,
            Boolean expectedResult) {
        ExtractionCriteria criteria = ExtractionCriteria.builder()
                .systemName("CMW")
                .timeWindow(TimeWindow.empty())
                .entities(Collections.emptyList())
                .variables(variables)
                .variableIds(ids)
                .aliasFields(Collections.emptyMap())
                .build();

        assertEquals(expectedResult, criteria.isVariableSearch());
    }

    @Test
    public void shouldSerializeToJSON() throws IOException {
        ExtractionCriteria criteria = ExtractionCriteria.builder().systemName("CMW").variables(emptyMap())
                .entities(singletonList(
                        EntityKeyValues.builder().fromMap(false, ImmutableMap.of("key1", "value1", "key2", "value2"))
                                .build()))
                .timeWindow(TimeWindow.between(Instant.now().minus(1, DAYS), Instant.now()))
                .build();

        ObjectMapper ob = GlobalObjectMapperProvider.get();

        String value = ob.writeValueAsString(criteria);

        ExtractionCriteria criteriaFromString = ob.readerFor(ExtractionCriteria.class).readValue(value);

        assertEquals(criteria, criteriaFromString);
    }

    @Test
    public void shouldCorrectlyInitializeWithDefaults() {
        ExtractionCriteria criteria = ExtractionCriteria.builder().systemName("CMW").variables(emptyMap())
                .entities(singletonList(
                        EntityKeyValues.builder().fromMap(false, ImmutableMap.of("key1", "value1")).build()))
                .aliasFields(emptyMap())
                .timeWindow(TimeWindow.between(Instant.now().minus(1, DAYS), Instant.now())).build();

        assertEquals(0, criteria.getEntityIds().size());
        assertEquals(0, criteria.getVariableIds().size());
        assertFalse(criteria.isVariableSearch());
    }

    public static Object[][] isVariableQueryTestsParams() {
        return new Object[][] { new Object[] { Collections.emptyMap(), Collections.emptySet(), false },
                new Object[] { Collections.emptyMap(), Set.of(1L), true },
                new Object[] { Map.of("VARIABLE", false), Set.of(1L), true },
                new Object[] { Map.of("VARIABLE", false), Collections.emptySet(), true },
        };
    }

}