package cern.nxcals.common.domain;

import cern.nxcals.common.utils.GlobalObjectMapperProvider;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class EntityKeyValueTest {

    @Test
    public void shouldNotCreateWithoutKey() {
        assertThrows(NullPointerException.class, () -> EntityKeyValue.builder().value("value").build());
    }

    @Test
    public void shouldNotCreateWithoutValue() {
        assertThrows(NullPointerException.class, () -> EntityKeyValue.builder().key("key").build());
    }

    @Test
    public void shouldSerialize() throws IOException {
        EntityKeyValue entityKeyValue1 = EntityKeyValue.builder().key("key").value("value").wildcard(true).build();
        ObjectMapper ob = GlobalObjectMapperProvider.get();
        String s = ob.writeValueAsString(entityKeyValue1);
        EntityKeyValue entityKeyValue2 = ob.readValue(s, EntityKeyValue.class);
        assertEquals(entityKeyValue1.getKey(), entityKeyValue2.getKey());
        assertEquals(entityKeyValue1.getValue(), entityKeyValue2.getValue());
        assertEquals(entityKeyValue1.isWildcard(), entityKeyValue2.isWildcard());
        assertEquals(entityKeyValue1, entityKeyValue2);
    }
}
