/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.common;

import cern.nxcals.common.converters.TimeConverter;
import cern.nxcals.common.converters.TimeConverterImpl;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericRecord;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class TimeConverterImplTest {
    private final TimeConverter converter = new TimeConverterImpl();

    private final Schema timeKeyDefSchema = new Schema.Parser().parse(
            "{\"type\":\"record\",\"name\":\"CmwPT\",\"namespace\":\"cern.cals\",\"fields\":[{\"name\":\"_p_t_\",\"type\":\"long\"}]}");

    private final Schema wrongTimeKeyDefSchema = new Schema.Parser()
            .parse("{\"type\":\"record\",\"name\":\"CmwPT\",\"namespace\":\"cern.cals\",\"fields\":[{\"name\":\"_p_t_\",\"type\":\"long\"},"
                    + "{\"name\":\"_p_t_1\",\"type\":\"long\"}]}");

    @Test
    public void testTimeConverter() {
        // given
        long timestamp = 123456789L;
        GenericRecord record = new GenericData.Record(timeKeyDefSchema);
        record.put(0, timestamp);

        // when
        Long recordTime = converter.convert(record);

        // then
        assertNotNull(recordTime);
        assertEquals(timestamp, recordTime, 0);
    }

    @Test
    public void shouldFailWithWrongSchema() {
        // given
        long timestamp = 123456789L;
        GenericRecord record = new GenericData.Record(wrongTimeKeyDefSchema);
        record.put(0, timestamp);

        // when
        assertThrows(RuntimeException.class, () -> converter.convert(record));
    }

    @Test
    public void shouldFailIfNoValueFound() {
        // given
        GenericRecord record = new GenericData.Record(wrongTimeKeyDefSchema);

        // when
        assertThrows(RuntimeException.class, () -> converter.convert(record));
    }

}
