package cern.nxcals.common.utils;

import cern.nxcals.api.exceptions.FatalDataConflictRuntimeException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.function.Supplier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
class CallRepeaterTest {

    @Mock
    Supplier<Long> supplier;

    @BeforeEach
    void setup() {
        Mockito.when(supplier.get()).thenThrow(new IllegalStateException("test"), new IllegalArgumentException("test2"))
                .thenReturn(10L);
    }

    @Test
    void callShouldSucceed() {
        //given

        //when
        Long val = CallRepeater.call(supplier, "repeating call", 3,
                ex -> new RuntimeException("failed"),
                List.of(NullPointerException.class, IllegalThreadStateException.class));

        //then
        assertEquals(10L, val);
    }

    @Test
    void callShouldFailOnExceedTheMaxCount() {
        //given

        //when
        assertThrows(FatalDataConflictRuntimeException.class, () -> CallRepeater.call(supplier, "repeating call", 2,
                ex -> new FatalDataConflictRuntimeException("failed", ex),
                List.of(NullPointerException.class, IllegalThreadStateException.class)));

        //then exception

    }

    @Test
    void callShouldFailOnNonRepeatableException() {
        //given

        //when
        assertThrows(IllegalArgumentException.class, () -> CallRepeater.call(supplier, "repeating call", 10,
                ex -> new FatalDataConflictRuntimeException("failed", ex),
                List.of(IllegalArgumentException.class)));

        //then exception

    }
}