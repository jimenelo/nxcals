package cern.nxcals.common.utils;

import org.apache.avro.SchemaBuilder;
import org.apache.avro.SchemaParseException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class IllegalCharacterConverterTest {

    private Map<String, String> cache = new ConcurrentHashMap<>();

    private static final String LEGAL_TEST_BASE_NAME = "test";
    private IllegalCharacterConverter converter;

    @BeforeEach
    public void setup() {
        converter = new IllegalCharacterConverter(cache);
    }

    @Test
    public void shouldNotEncodeNullString() {
        assertThrows(NullPointerException.class,
                () -> converter.convertToLegal(null),
                "this should throw NPE when provided null name"
        );
    }

    @Test
    public void shouldNotDecodeNullString() {
        assertThrows(NullPointerException.class,
                () -> converter.convertFromLegal(null),
                "this should throw NPE when provided null name"
        );
    }

    @ParameterizedTest
    @MethodSource("fieldNames")
    public void shouldEncodingBeAvroParseable(String nameToEncode) {
        final String encodedName = converter.convertToLegal(nameToEncode);
        assertTrue(encodedName.startsWith(IllegalCharacterConverter.PREFIX));
        assertAvroParseable(encodedName);
    }

    private void assertAvroParseable(String name) {
        try {
            SchemaBuilder.record("test").fields().name(name).type().stringType().noDefault().endRecord();
        } catch (SchemaParseException exception) {
            throw new AssertionError(exception);
        }
    }
    
    @ParameterizedTest
    @MethodSource("fieldNames")
    public void shouldCodeAndDecodeCorrectly(String value) {
        assertFalse(IllegalCharacterConverter.isEncoded(value));

        String encoded = converter.convertToLegal(value);
        assertTrue(IllegalCharacterConverter.isEncoded(encoded));
        assertTrue(IllegalCharacterConverter.isLegal(encoded));

        String decoded = converter.convertFromLegal(encoded);
        assertFalse(IllegalCharacterConverter.isEncoded(decoded));

        assertEquals(value, decoded);
    }

    @Test
    public void testEncodingNameWithPrefix() {
        final String nameToEncode = IllegalCharacterConverter.PREFIX + LEGAL_TEST_BASE_NAME;
        assertThrows(IllegalCharacterConverter.FieldNameException.class, () -> converter.convertToLegal(nameToEncode));
    }

    @ParameterizedTest
    @MethodSource("nonDecodableFieldNames")
    public void testDecodeIllegalValues(String toDecode) {
        assertThrows(IllegalCharacterConverter.FieldNameException.class, () -> converter.convertFromLegal(toDecode));
    }

    @Test
    public void testCachedToLegalValue() {
        assertFalse(cache.containsKey(LEGAL_TEST_BASE_NAME));
        String convertedValue = converter.convertToLegal(LEGAL_TEST_BASE_NAME);
        assertTrue(cache.containsKey(LEGAL_TEST_BASE_NAME));
        assertEquals(convertedValue, cache.get(LEGAL_TEST_BASE_NAME));
    }

    @Test
    public void testGetLegalIfCachedWithCachedValue() {
        String cachedValue = "cachedValue";
        cache.put(LEGAL_TEST_BASE_NAME, cachedValue);
        assertEquals(cachedValue, converter.getLegalIfCached(LEGAL_TEST_BASE_NAME));
    }

    @Test
    public void testGetLegalIfCachedWithNoCachedValue() {
        assertEquals(LEGAL_TEST_BASE_NAME, converter.getLegalIfCached(LEGAL_TEST_BASE_NAME));
    }

    @Test
    public void testSingletonNotNull() {
        assertNotNull(IllegalCharacterConverter.get());
    }

    @Test
    public void testSingleton() {
        assertSame(IllegalCharacterConverter.get(), IllegalCharacterConverter.get());
    }

    public static Object[][] fieldNames() {
        return new Object[][] {
                new Object[] { " " },
                new Object[] { "  " },
                new Object[] { ":" + LEGAL_TEST_BASE_NAME },
                new Object[] { LEGAL_TEST_BASE_NAME + ":" },
                new Object[] { LEGAL_TEST_BASE_NAME + ":" + LEGAL_TEST_BASE_NAME },
                new Object[] { ":" + LEGAL_TEST_BASE_NAME + ":" },
                new Object[] { ":" },
                new Object[] { "::" },
                new Object[] { "LegalName" },
                new Object[] { "12345" },    // No padding
                new Object[] { "123456" },   // 6 padding
                new Object[] { "1234567" },  // 4 padding
                new Object[] { "12345678" }, // 3 padding
                new Object[] { "123456789" } // 1 padding
        };
    }

    public static Object[][] nonDecodableFieldNames() {
        return new Object[][] {
                new Object[] { "Test" },
                new Object[] { "__NX_5" }
        };
    }
}
