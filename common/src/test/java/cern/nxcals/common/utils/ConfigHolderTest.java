package cern.nxcals.common.utils;

import com.typesafe.config.Config;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

/* *
 * @author jwozniak
 * @date Aug 04, 2016 8:07 AM
 */
public class ConfigHolderTest {

    @BeforeEach
    public void setup() {
        System.setProperty("test.prop", "1000");
        System.setProperty("test3.prop2", "1000");
        System.setProperty("test1.prop1", "true");
        System.setProperty("test1.prop2", "false");
    }

    @AfterEach
    public void teardown() {
        System.setProperty("test.prop", "");
        System.setProperty("test3.prop2", "");
        System.setProperty("test1.prop1", "");
        System.setProperty("test1.prop2", "");
    }

    @Test
    public void referenceConfigShouldBeLoadedCorrectly() {
        Config config = ConfigHolder.getConfig();
        assertEquals(100, config.getInt("test.test2.value"));
    }

    @Test
    public void shouldGetConfigIfPresent() {
        assertEquals(2, ConfigHolder.getConfigIfPresent("test").entrySet().size());
        assertEquals(0, ConfigHolder.getConfigIfPresent("test111").entrySet().size());
    }

    @Test
    public void shouldGetValue() {
        assertEquals(100, (int) ConfigHolder.getProperty("test.test2.value", 200));
    }

    @Test
    public void shouldGetEmptyValue() {
        assertEquals(200, (int) ConfigHolder.getProperty("test.empty", 200));
    }

    @Test
    public void shouldGetIntFromSysProps() {
        assertEquals(1000, ConfigHolder.getInt("test.prop", 1));
        assertEquals(1000, ConfigHolder.getInt("test3.prop2", 1));
    }

    @Test
    public void shouldGetBooleanFromSysProps() {
        assertTrue(ConfigHolder.getBoolean("test1.prop1", false));
        assertFalse(ConfigHolder.getBoolean("test1.prop2", true));
        assertTrue(ConfigHolder.getBoolean("test.empty", true));
    }
}