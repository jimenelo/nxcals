package cern.nxcals.common.utils;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.function.Function;
import java.util.function.Supplier;

import static cern.nxcals.common.utils.ParallelUtils.IOParallelUtils.IORunnableExecutor.handleIO;
import static cern.nxcals.common.utils.ParallelUtils.IOParallelUtils.doIOOperations;
import static cern.nxcals.common.utils.ParallelUtils.processParallelIfNeeded;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ParallelUtilsTest {
    ThreadFactory namedThreadFactory =
            new ThreadFactoryBuilder().setNameFormat("my-test-thread-%d").build();

    ExecutorService executorService = Executors.newFixedThreadPool(4, namedThreadFactory);

    @Test
    void doIOOperationsShouldReturnEmptyListIfNoExceptionWasThrownAndExecuteAllTasks() {
        ConcurrentLinkedQueue<Integer> queue = new ConcurrentLinkedQueue<>();
        Set<Integer> elements = Set.of(1, 2, 3, 4, 5, 6, 7, 8, 9);
        List<IOException> exceptions = doIOOperations(elements,
                x -> {
                    System.out.println(Thread.currentThread().getName() + x);
                    queue.add(x);
                }, executorService);

        assertEquals(0, exceptions.size());
        assertEquals(elements, new HashSet<>(queue));
        assertEquals(elements.size(), queue.size());
    }

    @Test
    void doIOOperationsShouldException() {
        List<IOException> exceptions = doIOOperations(List.of(1, 2, 3, 4, 5, 6, 7, 8, 9),
                x -> {
                    if (x == 1) {
                        throw new IOException("testException");
                    }
                }, executorService);

        assertEquals(1, exceptions.size());
    }

    @Test
    void doIOOperationsShouldCatchAllExceptions() {
        List<IOException> exceptions = doIOOperations(List.of(1, 2, 3, 4, 5, 6, 7, 8, 9),
                x -> {
                    throw new IOException("testException");
                }, executorService);

        assertEquals(9, exceptions.size());
    }

    @Test
    void ioHandlerShouldCatchIOExceptionForIOCallable() {
        IOException exception = new IOException("Test exception");
        Optional<IOException> maybeException = ParallelUtils.IOParallelUtils.IORunnableExecutor.handleIO(() -> {
            throw exception;
        }).get();

        assertTrue(maybeException.isPresent());
        assertEquals(exception, maybeException.get());
    }

    @Test
    void ioHandlerShouldReturnEmptyOptionalExceptionWasNotThrownForIOCallable() {
        Optional<IOException> maybeException = ParallelUtils.IOParallelUtils.IORunnableExecutor.handleIO(() -> {
        }).get();

        assertTrue(maybeException.isEmpty());
    }

    @Test
    void ioHandlerShouldNotCatchExceptionsOtherThanIOExceptionForIOCallable() {
        Supplier<Optional<IOException>> supplier = ParallelUtils.IOParallelUtils.IORunnableExecutor.handleIO(() -> {
            throw new ClassCastException();
        });

        assertThrows(ClassCastException.class, supplier::get);
    }

    @Test
    void ioHandlerShouldCatchIOExceptionForIOConsumer() {
        IOException exception = new IOException("Test exception");
        Optional<IOException> maybeException = handleIO((x) -> {
            throw exception;
        }).apply(1);

        assertTrue(maybeException.isPresent());
        assertEquals(exception, maybeException.get());
    }

    @Test
    void ioHandlerShouldReturnEmptyOptionalExceptionWasNotThrownForIOConsumer() {
        Optional<IOException> maybeException = handleIO((x) -> {
        }).apply(1);

        assertTrue(maybeException.isEmpty());
    }

    @Test
    void ioHandlerShouldNotCatchExceptionsOtherThanIOExceptionForIOConsumer() {
        Function<Integer, Optional<IOException>> function = handleIO((x) -> {
            throw new ClassCastException();
        });

        assertThrows(ClassCastException.class, () -> function.apply(1));
    }

    @Test
    void processParallelIfNeededShouldNotProcessParallelIfHasOneElement() {
        List<Integer> elements = List.of(1);
        Function<Integer, Long> function = (x) -> Thread.currentThread().getId();

        List<CompletableFuture<Long>> futures = processParallelIfNeeded(elements, function, executorService);
        Long processId = futures.get(0).join();

        assertEquals(Thread.currentThread().getId(), processId);
    }

    @Test
    void processParallelIfNeededShouldProcessParallelIfHasMoreThanOneElement() {
        List<Integer> elements = List.of(1, 2);
        Function<Integer, Long> function = (x) -> Thread.currentThread().getId();

        List<CompletableFuture<Long>> futures = processParallelIfNeeded(elements, function, executorService);

        for (CompletableFuture<Long> future : futures) {
            Long processId = future.join();
            assertNotEquals(Thread.currentThread().getId(), processId);
        }
    }
}
