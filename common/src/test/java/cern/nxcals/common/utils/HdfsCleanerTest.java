package cern.nxcals.common.utils;

import lombok.AllArgsConstructor;
import org.apache.hadoop.fs.Path;
import org.junit.jupiter.api.Test;

import java.util.Iterator;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class HdfsCleanerTest {

    @Test
    public void shouldFailOnNull() {
        assertThrows(NullPointerException.class, () -> new HdfsCleaner(null));
    }

    @Test
    public void shouldFailOnCountPriorToExecution() {
        Iterator<Path> toRemove = iteratorMockOfSize(3);

        HdfsCleaner cleaner = new HdfsCleaner(toRemove);
        assertThrows(IllegalStateException.class, () -> cleaner.removedCount());
    }

    @Test
    public void shouldFailOnTimePriorToExecution() {
        Iterator<Path> toRemove = iteratorMockOfSize(3);

        HdfsCleaner cleaner = new HdfsCleaner(toRemove);
        assertThrows(IllegalStateException.class, () -> cleaner.timeUsed());
    }

    @Test
    public void shouldRemove3Files() {
        Iterator<Path> toRemove = iteratorMockOfSize(3);

        HdfsCleaner cleaner = new HdfsCleaner(toRemove);
        cleaner.run();

        assertEquals(3, cleaner.removedCount());
    }

    @Test
    public void shouldWorkForEmptyIterator() {
        Iterator<Path> toRemove = iteratorMockOfSize(0);

        HdfsCleaner cleaner = new HdfsCleaner(toRemove);
        cleaner.run();

        assertEquals(0, cleaner.removedCount());
        assertTrue(2 > cleaner.timeUsed());
    }

    private Iterator<Path> iteratorMockOfSize(int size) {
        return new MockIterator(size);
    }

    @AllArgsConstructor
    private class MockIterator implements Iterator<Path> {
        private int left;

        @Override
        public boolean hasNext() {
            return left > 0;
        }

        @Override
        public Path next() {
            return null;
        }

        @Override
        public void remove() {
            left--;
        }
    }
}