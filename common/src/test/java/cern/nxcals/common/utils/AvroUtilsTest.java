package cern.nxcals.common.utils;

import com.google.common.collect.ImmutableSet;
import org.apache.avro.Schema;
import org.apache.spark.sql.avro.SchemaConverters;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructType;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Optional;
import java.util.Set;
import java.util.StringJoiner;

import static cern.nxcals.common.spark.MoreFunctions.createNullableField;
import static cern.nxcals.common.spark.MoreFunctions.createTypeFromFields;
import static org.apache.avro.Schema.Type.STRING;
import static org.apache.avro.Schema.create;
import static org.apache.avro.SchemaBuilder.record;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class AvroUtilsTest {

    @Test
    void shouldDetectPrimitives() {
        assertTrue(AvroUtils.isPrimitiveType(Schema.Type.DOUBLE));
        assertTrue(AvroUtils.isPrimitiveType(Schema.Type.BOOLEAN));
        assertTrue(AvroUtils.isPrimitiveType(Schema.Type.INT));
        assertTrue(AvroUtils.isPrimitiveType(Schema.Type.LONG));
        assertTrue(AvroUtils.isPrimitiveType(Schema.Type.FLOAT));
        assertTrue(AvroUtils.isPrimitiveType(Schema.Type.STRING));
        assertFalse(AvroUtils.isPrimitiveType(Schema.Type.RECORD));
        assertFalse(AvroUtils.isPrimitiveType(Schema.Type.FIXED));
        assertFalse(AvroUtils.isPrimitiveType(Schema.Type.UNION));
    }

    @Test
    void shouldDetectPrimitiveNumeric() {
        assertTrue(AvroUtils.isPrimitiveNumericType(Schema.Type.DOUBLE));
        assertTrue(AvroUtils.isPrimitiveNumericType(Schema.Type.BOOLEAN));
        assertTrue(AvroUtils.isPrimitiveNumericType(Schema.Type.INT));
        assertTrue(AvroUtils.isPrimitiveNumericType(Schema.Type.LONG));
        assertTrue(AvroUtils.isPrimitiveNumericType(Schema.Type.FLOAT));
        assertFalse(AvroUtils.isPrimitiveNumericType(Schema.Type.STRING));
        assertFalse(AvroUtils.isPrimitiveNumericType(Schema.Type.RECORD));
        assertFalse(AvroUtils.isPrimitiveNumericType(Schema.Type.FIXED));
        assertFalse(AvroUtils.isPrimitiveNumericType(Schema.Type.UNION));
    }

    @Test
    void shouldDetectUnions() {
        assertFalse(AvroUtils.isUnion(Schema.Type.DOUBLE));
        assertFalse(AvroUtils.isUnion(Schema.Type.BOOLEAN));
        assertFalse(AvroUtils.isUnion(Schema.Type.INT));
        assertFalse(AvroUtils.isUnion(Schema.Type.LONG));
        assertFalse(AvroUtils.isUnion(Schema.Type.FLOAT));
        assertFalse(AvroUtils.isUnion(Schema.Type.STRING));
        assertFalse(AvroUtils.isUnion(Schema.Type.RECORD));
        assertFalse(AvroUtils.isUnion(Schema.Type.FIXED));
        assertTrue(AvroUtils.isUnion(Schema.Type.UNION));
    }

    @Test
    void shouldDetectNonPrimitiveValues() {
        assertFalse(AvroUtils.hasNonPrimitiveTypes(ImmutableSet.of(Schema.Type.BOOLEAN, Schema.Type.DOUBLE)));
        assertTrue(AvroUtils.hasNonPrimitiveTypes(ImmutableSet.of(Schema.Type.STRING, Schema.Type.DOUBLE)));
        assertTrue(AvroUtils.hasNonPrimitiveTypes(ImmutableSet.of(Schema.Type.UNION, Schema.Type.DOUBLE)));
    }


    @Test
    void shouldExtractUnionType() {
        assertEquals(Schema.Type.STRING, AvroUtils.getSchemaUnionType(new Schema.Parser()
                .parse("[\"string\", \"null\"]")).getType());
        assertEquals(Schema.Type.STRING, AvroUtils.getSchemaTypeFor(new Schema.Parser()
                .parse("[\"string\", \"null\"]")));
    }

    @Test
    void shouldFailOnNonUnionType() {
        assertThrows(IllegalArgumentException.class,
                () -> AvroUtils.getSchemaUnionType(new Schema.Parser().parse("[\"null\"]")));
    }

    @Test
    void shouldReturnNestedField() {
        final String FIELD_NAME = "FIELD";
        final String NESTED_FIELD_NAME_1 = "NESTED_FIELD1";
        final String FIELD_NAME_2 = "FIELD2";
        final String FIELD_NAME_3 = "FIELD3";
        final String NESTED_FIELD_NAME_3 = "NESTED_FIELD3";

        StructType nestedData2Fields = createTypeFromFields(
                createNullableField(FIELD_NAME_2, DataTypes.LongType),
                createNullableField(FIELD_NAME_3, DataTypes.StringType));

        StructType nestedData1Fields = createTypeFromFields(
                createNullableField(FIELD_NAME_2, DataTypes.LongType),
                createNullableField(FIELD_NAME_3, DataTypes.StringType),
                createNullableField(NESTED_FIELD_NAME_3, nestedData2Fields));

        StructType sparkSchema = createTypeFromFields(
                createNullableField(FIELD_NAME, DataTypes.LongType),
                createNullableField(NESTED_FIELD_NAME_1, nestedData1Fields));

        String pathToField = new StringJoiner(".").add(NESTED_FIELD_NAME_1).add(NESTED_FIELD_NAME_3)
                .add(FIELD_NAME_2).toString();
        Schema avroSchema = SchemaConverters.toAvroType(sparkSchema, false, "data0", "cern.nxcals");
        Optional<Schema.Field> maybeField = AvroUtils.findField(avroSchema, pathToField);
        assertTrue(maybeField.isPresent());
        assertEquals(FIELD_NAME_2, maybeField.get().name());
    }

    @Test
    void shouldReturnEmptyIfFieldNotExits() {
        final String FIELD_NAME = "FIELD";

        StructType sparkSchema = createTypeFromFields(
                createNullableField(FIELD_NAME, DataTypes.LongType));

        String pathToField = "a.b.c";
        Schema avroSchema = SchemaConverters.toAvroType(sparkSchema, false, "data0", "cern.nxcals");
        Optional<Schema.Field> maybeField = AvroUtils.findField(avroSchema, pathToField);
        assertTrue(maybeField.isEmpty());
    }

    @Test
    void shouldReturnFirstSchemaWhenSecondIsNull() {
        Schema schema = record("TEST").namespace("ns").fields().name("class").type(create(STRING)).noDefault()
                .name("property").type(create(STRING)).noDefault().endRecord();
        Schema merged = AvroUtils.mergeSchemas(schema, null);
        assertEquals(schema, merged);
    }

    @Test
    void shouldReturnSecondSchemaWhenFirstIsNull() {
        Schema schema = record("TEST").namespace("ns").fields().name("class").type(create(STRING)).noDefault()
                .name("property").type(create(STRING)).noDefault().endRecord();
        Schema merged = AvroUtils.mergeSchemas(null, schema);
        assertEquals(schema, merged);
    }

    @Test
    void shouldMergeSchemas() {
        Schema oldSchema = record("TEST").namespace("ns").fields().name("class").type(create(STRING)).noDefault()
                .name("property").type(create(STRING)).noDefault().endRecord();
        Schema newSchema = record("TEST").fields().name("classVersion").type(create(STRING)).noDefault().endRecord();

        Schema merged = AvroUtils.mergeSchemas(oldSchema, newSchema);
        for (String name : Arrays.asList("class", "property", "classVersion")) {
            assertNotNull(merged.getField(name));
        }
    }

    @Test
    void shouldCorrectlyReturnAllFieldPathsInSchema() {
        // given
        Schema nestedSchema = record("TEST").namespace("ns").fields()
                .name("class").type(create(STRING)).noDefault()
                .name("property").type(create(STRING)).noDefault()
                .endRecord();
        Schema schema = record("TEST").namespace("ns").fields()
                .name("inner").type(Schema.createUnion(nestedSchema)).noDefault().endRecord();
        Set<String> expectedPaths = Set.of("inner", "inner.class", "inner.property");
        // when
        Set<String> fieldPaths = AvroUtils.getAllFieldPaths(schema);
        // then
        assertEquals(expectedPaths, fieldPaths);
    }

    @Test
    void shouldParseAvroSchema() {
        // given
        Schema schema = Schema.create(Schema.Type.LONG);

        // when
        Schema parsedSchema = AvroUtils.parseSchema(schema.toString());

        // then
        assertEquals(schema, parsedSchema);
    }

    @Test
    void shouldReturnNullIfNullPassed() {
        // when
        Schema returnedSchema = AvroUtils.parseSchema(null);

        // then
        assertNull(returnedSchema);
    }

    @Test
    void shouldCreateLazySchema() {
        // given
        Schema schema = Schema.create(Schema.Type.LONG);

        // when
        Lazy<Schema> returnedSchema = AvroUtils.schemaLazy(schema.toString());

        // then
        assertEquals(schema, returnedSchema.get());
    }
}