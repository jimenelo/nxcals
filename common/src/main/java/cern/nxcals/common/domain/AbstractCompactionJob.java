package cern.nxcals.common.domain;

import cern.nxcals.common.paths.StagingPath;
import lombok.experimental.SuperBuilder;
import org.apache.hadoop.fs.Path;

@SuperBuilder
public abstract class AbstractCompactionJob implements DataProcessingJob {
    /**
     * Implementation is based on a class and StagingPath.
     *
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        AbstractCompactionJob that = (AbstractCompactionJob) o;
        StagingPath thatPath = new StagingPath(new Path(that.getSourceDir()));
        StagingPath thisPath = new StagingPath(new Path(this.getSourceDir()));
        return thisPath.equals(thatPath);
    }

    /**
     * Based on a StagingPath
     *
     * @return
     */
    @Override
    public int hashCode() {
        return new StagingPath(new Path(getSourceDir())).hashCode();
    }


}
