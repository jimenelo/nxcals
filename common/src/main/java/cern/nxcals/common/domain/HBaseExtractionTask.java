package cern.nxcals.common.domain;

import lombok.Getter;
import lombok.NonNull;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import lombok.extern.jackson.Jacksonized;

@Getter
@SuperBuilder(toBuilder = true)
@Jacksonized
@ToString(callSuper = true)
public class HBaseExtractionTask extends ExtractionTask {
    @NonNull
    private final HBaseResource resource;

    @Override
    public <T> T processWith(ExtractionTaskProcessor<T> processor) {
        return processor.execute(this);
    }
}
