/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.common.domain;

import cern.nxcals.api.utils.EncodingUtils;
import cern.nxcals.common.utils.AvroUtils;
import cern.nxcals.common.utils.Lazy;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.ToString;
import lombok.extern.jackson.Jacksonized;
import org.apache.avro.Schema;
import org.apache.commons.lang3.StringUtils;

@Getter
@ToString
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Builder(toBuilder = true, builderClassName = "Builder")
@Jacksonized
public class ColumnMapping {
    @EqualsAndHashCode.Include
    private final String fieldName;
    private final String schemaJson;
    @NonNull
    @JsonIgnore
    private final Lazy<Schema> schema;
    @EqualsAndHashCode.Include
    @NonNull
    private final String alias;

    public String getQualifiedName() {
        return EncodingUtils.desanitizeIfNeeded(alias);
    }

    public static ColumnMapping fromField(Schema.Field field) {
        return ColumnMapping.builder().fieldName(field.name()).schemaJson(field.schema().toString()).build();
    }

    public static class Builder {
        public ColumnMapping build() {
            if (StringUtils.isEmpty(this.alias)) {
                this.alias = this.fieldName;
            }
            return new ColumnMapping(this.fieldName, this.schemaJson, AvroUtils.schemaLazy(schemaJson), this.alias);
        }
    }

}
