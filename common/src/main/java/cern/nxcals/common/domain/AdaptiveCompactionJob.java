package cern.nxcals.common.domain;

import cern.nxcals.api.domain.TimeEntityPartitionType;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.ToString;
import lombok.Value;
import lombok.experimental.SuperBuilder;
import lombok.extern.jackson.Jacksonized;

import java.net.URI;
import java.util.List;


/**
 * Represents the new adaptive compaction job data required by the Compactor to work
 */
@Value
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = true)
@SuperBuilder
@Jacksonized
public class AdaptiveCompactionJob extends AbstractCompactionJob {

    @NonNull
    String filePrefix;
    @NonNull
    URI destinationDir;
    @NonNull
    URI sourceDir;

    //This is a list instead of set as it is much easier to test it afterwards.
    @ToString.Exclude
    @NonNull
    List<URI> files;

    long jobSize;
    long totalSize;

    @NonNull
    TimeEntityPartitionType partitionType;
    long systemId;
    long partitionId;
    long schemaId;
    boolean sortEnabled;

    long parquetRowGroupSize;
    long hdfsBlockSize;

    @Override
    public JobType getType() {
        return JobType.ADAPTIVE_COMPACT;
    }

    @Override
    public int getPartitionCount() {
        return partitionType.slots();
    }

}
