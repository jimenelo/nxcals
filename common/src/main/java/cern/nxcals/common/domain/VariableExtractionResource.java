package cern.nxcals.common.domain;

import cern.nxcals.api.domain.VariableConfig;
import lombok.Getter;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Getter
public class VariableExtractionResource extends ExtractionResource {
    @NonNull
    private final VariableConfig variableConfig;

    public boolean isFieldBound() {
        return variableConfig.getFieldName() != null;
    }
}
