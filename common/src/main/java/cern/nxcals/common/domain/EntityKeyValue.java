package cern.nxcals.common.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

/**
 * Used to represent key value with or without wildcard in the query builders.
 */
@Data
@Builder(builderClassName = "Builder", toBuilder = true)
@JsonDeserialize(builder = EntityKeyValue.Builder.class)
public class EntityKeyValue {
    @NonNull
    private final String key;
    @NonNull
    private final Object value;
    private final boolean wildcard;

    @JsonPOJOBuilder(withPrefix = "")
    public static class Builder {

    }
}
