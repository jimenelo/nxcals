/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.common.web;

import lombok.experimental.UtilityClass;

/**
 * Constants with the HTTP headers.
 */
@UtilityClass
public class HttpHeaders {
    public static final String APPLICATION_JSON_DATA_FORMAT = "application/json";
    public static final String TEXT_PLAIN_DATA_FORMAT = "text/plain";
    public static final String ACCEPT_APPLICATION_JSON = "Accept: " + APPLICATION_JSON_DATA_FORMAT;
    public static final String CONTENT_TYPE_APPLICATION_JSON = "Content-Type: " + APPLICATION_JSON_DATA_FORMAT;
    public static final String CONTENT_TYPE_TEXT_PLAIN = "Content-Type: " + TEXT_PLAIN_DATA_FORMAT;
}