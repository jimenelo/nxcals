package cern.nxcals.common.paths;

import cern.nxcals.common.utils.HdfsFileUtils;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.apache.hadoop.fs.Path;

import java.net.URI;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeParseException;
import java.util.Objects;
import java.util.function.Supplier;

public class StagingPath implements Comparable<StagingPath> {
    private static final String ERROR_MSG =
            "The path must be in the format of: .../systemId/partitionId/schemaId/" + HdfsFileUtils.STAGE_DIRECTORY_DATE_PATTERN;

    private final Path path;
    @Getter
    private final Path datePath;
    @Getter
    private final Path schemaPath;
    @Getter
    private final Path partitionPath;
    @Getter
    private final Path systemPath;
    @Getter
    private final Path root;

    private final Supplier<ZonedDateTime> dateSupplier;
    private final Supplier<Long> schemaSupplier;
    private final Supplier<Long> partitionSupplier;
    private final Supplier<Long> systemSupplier;

    public StagingPath(Path date) {
        this(date, false);
    }

    public StagingPath(Path path, boolean verify) {
        this.path = path;

        this.datePath = verifyExists(path, "Date");
        this.schemaPath = verifyExists(path.getParent(), "Schema");
        this.partitionPath = verifyExists(schemaPath.getParent(), "Partition");
        this.systemPath = verifyExists(partitionPath.getParent(), "System");
        this.root = systemPath.getParent();

        this.dateSupplier = () -> parseDate(datePath);
        this.schemaSupplier = () -> parseNode(schemaPath, "Schema");
        this.partitionSupplier = () -> parseNode(partitionPath, "Partition");
        this.systemSupplier = () -> parseNode(systemPath, "System");

        if (verify) {
            getDate();
            getSchemaId();
            getPartitionId();
            getSystemId();
        }
    }

    public static Instant toInstant(URI uri) {
        StagingPath stagingPath = new StagingPath(new Path(uri));
        return stagingPath.getDate().toInstant();
    }

    public ZonedDateTime getDate() {
        return dateSupplier.get();
    }

    public long getSchemaId() {
        return schemaSupplier.get();
    }

    public long getPartitionId() {
        return partitionSupplier.get();
    }

    public long getSystemId() {
        return systemSupplier.get();
    }

    public Path toPath() {
        return path;
    }

    private ZonedDateTime parseDate(Path node) {
        try {
            return LocalDate.parse(node.getName(), HdfsFileUtils.STAGE_DIRECTORY_DATE_FORMATTER).atStartOfDay(ZoneOffset.UTC);
        } catch (DateTimeParseException ex) {
            throw notParseable("Date", ex);
        }
    }

    private Long parseNode(Path node, String nodeName) {
        try {
            return Long.parseLong(node.getName());
        } catch (NumberFormatException ex) {
            throw notParseable(nodeName, ex);
        }
    }

    private Path verifyExists(Path node, String name) {
        if (node == null || StringUtils.isEmpty(node.getName())) {
            throw notFound(name);
        }
        return node;
    }

    private RuntimeException notFound(String node) {
        return new IllegalArgumentException(StringUtils.joinWith(" ", ERROR_MSG, node, "not found"));
    }

    private RuntimeException notParseable(String nodeName, Exception ex) {
        return new IllegalArgumentException(StringUtils.joinWith(" ", ERROR_MSG, nodeName, "not parseable value"), ex);
    }

    public String toString() {
        return this.path.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        StagingPath that = (StagingPath) o;
        return Objects.equals(this.datePath.getName(), that.datePath.getName())
                && Objects.equals(this.schemaPath.getName(), that.schemaPath.getName())
                && Objects.equals(this.partitionPath.getName(), that.partitionPath.getName())
                && Objects.equals(this.systemPath.getName(), that.systemPath.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(datePath.getName(), schemaPath.getName(), partitionPath.getName(), systemPath.getName());
    }

    @Override
    public int compareTo(StagingPath that) {
        if (that == null) {
            return -1;
        }
        int cmp = that.datePath.getName().compareTo(this.datePath.getName()); // the most recent first
        return cmp == 0 ? that.path.compareTo(this.path) : cmp; // afterwards by path
    }
}
