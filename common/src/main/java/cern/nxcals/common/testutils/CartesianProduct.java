package cern.nxcals.common.testutils;

import lombok.experimental.UtilityClass;
import org.junit.jupiter.params.provider.Arguments;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@UtilityClass
public class CartesianProduct {
    @SafeVarargs
    public static <E> Stream<Arguments> compose(E[]... arrays) {
        return CartesianProduct.compose(
                Arrays.stream(arrays).map(Arrays::asList).collect(Collectors.toList()));
    }

    public static <E> Stream<Arguments> compose(List<List<E>> listOfLists) {
        // Guava implementation Lists.cartesianProduct complains for null values in list
        List<List<E>> resultLists = new ArrayList<>();
        resultLists.add(new ArrayList<>());
        for (List<E> inputList : listOfLists) {
            List<List<E>> newResultLists = new ArrayList<>();
            resultLists.forEach(resultList ->
                    inputList.forEach(element -> {
                                List<E> newResultList = new ArrayList<>(resultList);
                                newResultList.add(element);
                                newResultLists.add(newResultList);
                            }
                    )
            );
            resultLists = newResultLists;
        }
        return resultLists.stream().map(list -> Arguments.of(list.toArray()));
    }
}
