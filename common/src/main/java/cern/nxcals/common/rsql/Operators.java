package cern.nxcals.common.rsql;

import lombok.experimental.UtilityClass;

@UtilityClass
public class Operators {
    public static final String IS_NULL_OPERATOR = "=ex="; //from RSQLVisitor class.
    public static final String PATTERN_OPERATOR = "=re="; //from RSQLVisitor class.
    public static final String STRING_EQUALS    = "=seq=";
    public static final String STRING_NOT_EQUALS= "=sne=";
    public static final String LIKE             = "=like=";
    public static final String NOT_LIKE         = "=nlike=";

    public static final String CASE_INSENSITIVE_STRING_EQUALS     = "=cinseq=";
    public static final String CASE_INSENSITIVE_STRING_NOT_EQUALS = "=cinsne=";
    public static final String CASE_INSENSITIVE_LIKE              = "=cinlike=";
    public static final String CASE_INSENSITIVE_NOT_LIKE          = "=cinnlike=";

    public static final String IN                                 = "=in="; //from RSQLVisitor class.
    public static final String NOT_IN                             = "=out="; //from RSQLVisitor class.
    public static final String CASE_INSENSITIVE_IN                = "=cin=";
    public static final String CASE_INSENSITIVE_NOT_IN            = "=cout=";
}