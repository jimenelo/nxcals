/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.common.converters;

import org.apache.avro.Schema.Field;
import org.apache.avro.Schema.Type;
import org.apache.avro.SchemaBuilder;
import org.apache.avro.generic.GenericRecord;

import java.util.List;

public class TimeConverterImpl implements TimeConverter {
    private static final Type LONG_TYPE = SchemaBuilder.builder().longType().getType();

    @Override
    public Long convert(GenericRecord record) {
        List<Field> fields = record.getSchema().getFields();
        if (fields.size() == 1) {
            Field field = fields.get(0);
            if (LONG_TYPE.equals(field.schema().getType())) {
                return (Long) record.get(field.pos());
            } else {
                throw new IllegalArgumentException("Unsupported field type for time conversion " + field);
            }
        } else {
            throw new IllegalArgumentException("Unsupported field list for time conversion " + fields);
        }
    }
}
