package cern.nxcals.common.metrics;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

@RequiredArgsConstructor
@Slf4j
public class MetricsPulse {
    private final ScheduledExecutorService executor;
    private final MetricsRegistry metricsRegistry;

    public void register(String metricName, Supplier<Long> metricSupplier, int unitCount, TimeUnit unit) {
        executor.scheduleAtFixedRate(() -> publishMetric(metricName, metricSupplier), 1, unitCount, unit);
    }

    private void publishMetric(String metricName, Supplier<Long> metricSupplier) {
        long value = metricSupplier.get();
        log.debug("Metric {}={}", metricName, value);
        metricsRegistry.updateGaugeTo(metricName, value);
    }
}
