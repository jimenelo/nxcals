package cern.nxcals.common.metrics;

import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.config.NamingConvention;
import io.micrometer.jmx.JmxMeterRegistry;
import org.springframework.boot.actuate.autoconfigure.metrics.MeterRegistryCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MetricsConfig {

    @Bean
    public MetricsRegistry metricsRegistry(MeterRegistry meterRegistry) {
        return new MicrometerMetricsRegistry(meterRegistry);
    }

    @Bean
    public MeterRegistryCustomizer<JmxMeterRegistry> jmxMetricsNamingConvention() {
        return registry -> registry.config().namingConvention(NamingConvention.snakeCase);
    }
}
