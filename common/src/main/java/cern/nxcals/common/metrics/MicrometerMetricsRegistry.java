package cern.nxcals.common.metrics;

import com.google.common.util.concurrent.AtomicDouble;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Represents a metrics management wrapper for the Micrometer framework capable of
 * exporting and metrics without exposing directly the enclosed {@link MetricsRegistry}.
 * Moreover, this unit efficiently manages the already defined gauge metrics and permitting the unnecessary call
 * to re-register them, since it stores the reference of the gauge values and directly updates those instead.
 *
 * @see <a href="https://micrometer.io/docs/concepts#_manually_incrementing_decrementing_a_gauge">Micrometer Docs</a>
 */
@Slf4j
public final class MicrometerMetricsRegistry implements MetricsRegistry {
    private static final String EXCEPTION_MESSAGE_METRIC_NAME_REQUIRED =
            "Metric name should not be null, empty or whitespace only!";

    /**
     * A structure to keep references to the gauge metrics exposed by the client service
     */
    private final Map<String, AtomicDouble> gaugeMetricsDoubleMap = new ConcurrentHashMap<>();

    /**
     * A structure to keep references to the gauge metrics exposed by the client service
     */
    private final Map<String, AtomicLong> gaugeMetricsLongMap = new ConcurrentHashMap<>();

    /**
     * A structure to keep references to the counter metrics exposed by the client service
     */
    private final Map<String, Counter> counterMetricsMap = new ConcurrentHashMap<>();

    /**
     * The micrometer metrics registry and management service
     */
    @NonNull
    private final MeterRegistry meterRegistry;

    public MicrometerMetricsRegistry(@NonNull MeterRegistry meterRegistry) {
        this.meterRegistry = meterRegistry;
    }

    @Override
    public void updateGaugeTo(String metricName, double value) {
        if (StringUtils.isBlank(metricName)) {
            throw new IllegalArgumentException(EXCEPTION_MESSAGE_METRIC_NAME_REQUIRED);
        }
        gaugeMetricsDoubleMap.computeIfAbsent(metricName, key ->
                meterRegistry.gauge(key, new AtomicDouble(0))).set(value);
    }

    @Override
    public void updateGaugeTo(String metricName, long value) {
        if (StringUtils.isBlank(metricName)) {
            throw new IllegalArgumentException(EXCEPTION_MESSAGE_METRIC_NAME_REQUIRED);
        }
        gaugeMetricsLongMap.computeIfAbsent(metricName, key ->
                meterRegistry.gauge(key, new AtomicLong(0))).set(value);
    }

    @Override
    public void incrementCounterBy(String metricName, double amountToIncrement) {
        if (StringUtils.isBlank(metricName)) {
            throw new IllegalArgumentException(EXCEPTION_MESSAGE_METRIC_NAME_REQUIRED);
        }
        counterMetricsMap.computeIfAbsent(metricName, meterRegistry::counter).increment(amountToIncrement);
    }

}
