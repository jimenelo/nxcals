package cern.nxcals.common.qbuilders.delegates;

import cern.nxcals.api.metadata.qbuilders.properties.GenericLongProperty;
import com.github.rutledgepaulv.qbuilders.builders.QBuilder;
import com.github.rutledgepaulv.qbuilders.conditions.Condition;
import com.github.rutledgepaulv.qbuilders.delegates.concrete.LongPropertyDelegate;
import com.github.rutledgepaulv.qbuilders.properties.concrete.LongProperty;
import com.github.rutledgepaulv.qbuilders.structures.FieldPath;

import java.util.Collection;
import java.util.function.Function;

public class GenericLongPropertyDelegate<T extends QBuilder<T>, C extends Condition<T>>
        extends GenericPropertyDelegate<T, C>
        implements GenericLongProperty<T, C> {
    private final LongProperty<T> longProperty;

    public GenericLongPropertyDelegate(FieldPath field, T canonical, Function<Condition<T>, C> wrapper) {
        super(field, canonical, wrapper);
        longProperty = new LongPropertyDelegate<>(field, canonical);
    }

    public C gt(Long number) {
        return wrapResult(longProperty.gt(number));
    }

    public C lt(Long number) {
        return wrapResult(longProperty.lt(number));
    }

    public C gte(Long number) {
        return wrapResult(longProperty.gte(number));
    }

    public C lte(Long number) {
        return wrapResult(longProperty.lte(number));
    }

    public C in(Long... values) {
        return wrapResult(longProperty.in(values));
    }

    public C in(Collection<Long> values) {
        return wrapResult(longProperty.in(values));
    }

    public C nin(Long... values) {
        return wrapResult(longProperty.nin(values));
    }

    public C nin(Collection<Long> values) {
        return wrapResult(longProperty.nin(values));
    }

    public C eq(Long value) {
        return wrapResult(longProperty.eq(value));
    }

    public C ne(Long value) {
        return wrapResult(longProperty.ne(value));
    }

    public C exists() {
        return wrapResult(longProperty.exists());
    }

    public C doesNotExist() {
        return wrapResult(longProperty.doesNotExist());
    }
}
