package cern.nxcals.common.qbuilders;

import cern.nxcals.common.rsql.Operators;
import com.github.rutledgepaulv.qbuilders.operators.ComparisonOperator;
import lombok.Getter;
import lombok.NonNull;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Getter
public enum CustomOperatorFactory {
    CASE_INSENSITIVE_SEQ(Operators.CASE_INSENSITIVE_STRING_EQUALS),
    CASE_INSENSITIVE_SNE(Operators.CASE_INSENSITIVE_STRING_NOT_EQUALS),
    CASE_INSENSITIVE_LIKE(Operators.CASE_INSENSITIVE_LIKE),
    CASE_INSENSITIVE_NLIKE(Operators.CASE_INSENSITIVE_NOT_LIKE),
    SEQ(Operators.STRING_EQUALS),
    SNE(Operators.STRING_NOT_EQUALS),
    LIKE(Operators.LIKE),
    NLIKE(Operators.NOT_LIKE),
    CASE_INSENSITIVE_IN(Operators.CASE_INSENSITIVE_IN),
    CASE_INSENSITIVE_NOT_IN(Operators.CASE_INSENSITIVE_NOT_IN);

    private ComparisonOperator operator;
    private String symbol;

    private static final Map<ComparisonOperator, CustomOperatorFactory> CACHE = Collections.synchronizedMap(new HashMap<>());

    static {
        Arrays.stream(values()).forEach(op -> CACHE.put(op.getOperator(), op));
    }

    CustomOperatorFactory(@NonNull String operatorSymbol) {
       operator = new ComparisonOperator(operatorSymbol);
       symbol = operatorSymbol;
    }

    public static String symbolOf(ComparisonOperator operator) {
        return isCustom(operator) ? CACHE.get(operator).getSymbol() : null;
    }

    public static boolean isCustom(ComparisonOperator operator) {
        return CACHE.containsKey(operator);
    }

    public static boolean isList(ComparisonOperator operator) {
        return isCustom(operator) && (CASE_INSENSITIVE_IN.getOperator().equals(operator) || CASE_INSENSITIVE_NOT_IN.getOperator().equals(operator));
    }

    public static ComparisonOperator stringEquals(boolean caseSensitive) {
        return caseSensitive ? SEQ.getOperator() : CASE_INSENSITIVE_SEQ.getOperator();
    }

    public static ComparisonOperator stringNotEquals(boolean caseSensitive) {
        return caseSensitive ? SNE.getOperator() : CASE_INSENSITIVE_SNE.getOperator();
    }

    public static ComparisonOperator stringLike(boolean caseSensitive) {
        return caseSensitive ? LIKE.getOperator() : CASE_INSENSITIVE_LIKE.getOperator();
    }

    public static ComparisonOperator stringNotLike(boolean caseSensitive) {
        return caseSensitive ? NLIKE.getOperator() : CASE_INSENSITIVE_NLIKE.getOperator();
    }

    public static ComparisonOperator stringIn(boolean caseSensitive) {
        return caseSensitive ? ComparisonOperator.IN : CASE_INSENSITIVE_IN.getOperator();
    }

    public static ComparisonOperator stringNotIn(boolean caseSensitive) {
        return caseSensitive ? ComparisonOperator.NIN : CASE_INSENSITIVE_NOT_IN.getOperator();
    }
}
