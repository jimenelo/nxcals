package cern.nxcals.common.qbuilders;

import com.github.rutledgepaulv.qbuilders.nodes.ComparisonNode;
import com.github.rutledgepaulv.qbuilders.operators.ComparisonOperator;
import com.github.rutledgepaulv.qbuilders.visitors.RSQLVisitor;

import java.util.Objects;
import java.util.function.Function;

/**
 * Custom visitor that allows to add our own operators for String eq & ne and like, not like.
 * We need them to properly implement those operators in JPA.
 * The default implementation used always 'like' operator for eq that required fancy escaping for true equals.
 */
public class CustomRSQLVisitor extends RSQLVisitor {

    public CustomRSQLVisitor() {
        super(CustomSerializationStrategy.INSTANCE);
    }
    @Override
    protected String visit(ComparisonNode node) {
        ComparisonOperator operator = node.getOperator();

        if (!CustomOperatorFactory.isCustom(operator)) {
            return super.visit(node);
        }

        if (CustomOperatorFactory.isList(operator)) {
            return list(node, CustomOperatorFactory.symbolOf(operator));
        } else {
            return single(node, CustomOperatorFactory.symbolOf(operator));
        }
    }

    private static class CustomSerializationStrategy implements Function<Object, String> {

        private static final CustomSerializationStrategy INSTANCE = new CustomSerializationStrategy();

        private static final CharSequence DOUBLE_QUOTE = "\"";
        private static final CharSequence SINGLE_QUOTE = "\'";

        @Override
        public String apply(Object value) {

            String string = Objects.toString(value);

            //Here in the original serialization the '\' was replaced with '\\' for some reason (maybe json?).
            //This created problems for like quoting, so this code was removed (jwozniak)

            boolean containsDoubleQuotes = string.contains("\"");
            boolean containsSingleQuotes = string.contains("'");

            if (!containsDoubleQuotes) {
                return DOUBLE_QUOTE + string + DOUBLE_QUOTE;
            } else if (!containsSingleQuotes) {
                return SINGLE_QUOTE + string + SINGLE_QUOTE;
            } else {
                string = string.replaceAll("\"", "\\\\\"");
                return DOUBLE_QUOTE + string + DOUBLE_QUOTE;
            }

        }
    }
}
