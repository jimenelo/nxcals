package cern.nxcals.common.utils;

import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.apache.hadoop.fs.FileStatus;

import java.util.Objects;
import java.util.function.Predicate;

/**
 * Represents an enumeration of {@link Predicate} instances, capable of testing
 * a given {@link FileStatus}. The result of the check, defines whether the file that it points to,
 * is one of the supported types that can be stored to HDFS as business data.
 */
@Getter
public enum SupportedFileTypePredicate implements Predicate<FileStatus> {
    AVRO(".avro"),
    PARQUET(".parquet");

    private final String fileExtension;

    SupportedFileTypePredicate(String fileExtension) {
        if (StringUtils.isBlank(fileExtension)) {
            throw new IllegalArgumentException("file extension cannot be null or blank!");
        }
        this.fileExtension = fileExtension;
    }

    @Override
    public boolean test(FileStatus fileStatus) {
        Objects.requireNonNull(fileStatus, "File status can not be null!");
        return fileStatus.isFile() && fileStatus.getPath().getName().endsWith(fileExtension);
    }

}
