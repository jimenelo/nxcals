package cern.nxcals.common.utils;

import lombok.experimental.UtilityClass;

import java.util.Collection;
import java.util.stream.Stream;

@UtilityClass
public class StreamUtils {
    /**
     * Returns normal or parallel stream depending on the collection size limit.
     *
     * @param collection
     * @param noParallelizationSizeLimit - about this size collection will be parallelized
     * @param <T>
     * @return
     */
    public <T> Stream<T> of(Collection<T> collection, int noParallelizationSizeLimit) {
        return collection.size() <= noParallelizationSizeLimit ? collection.stream() : collection.parallelStream();
    }
}
