package cern.nxcals.common.utils;

import lombok.experimental.UtilityClass;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;

@UtilityClass
public class CollectionUtils {

    public static <T> List<List<T>> splitIntoParts(List<T> elements, long maxPartSizeInBytes, long maxPartSize,
            Function<T, Long> recordSizeFunction) {
        if (elements.isEmpty()) {
            return Collections.emptyList();
        }

        ArrayList<List<T>> outputLists = new ArrayList<>();

        long currentPartitionSize = 0;
        ArrayList<T> currentPartition = new ArrayList<>();
        for (T element : elements) {
            if (currentPartitionSize >= maxPartSizeInBytes || currentPartition.size() >= maxPartSize) {
                outputLists.add(currentPartition);
                currentPartition = new ArrayList<>();
                currentPartitionSize = 0;
            }
            currentPartition.add(element);
            currentPartitionSize += recordSizeFunction.apply(element);
        }
        outputLists.add(currentPartition);
        return outputLists;
    }
}
