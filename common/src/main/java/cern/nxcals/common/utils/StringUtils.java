package cern.nxcals.common.utils;

import lombok.NonNull;
import lombok.experimental.UtilityClass;

/**
 * Utility class providing escaping characters for sql like clause
 */
@UtilityClass
public final class StringUtils {
    // add one backslash
    public static final Character ESCAPE_CHARACTER = '\\';

    public static String escapeWildcards(@NonNull String rawString) {
            return rawString
                    .replace("_", ESCAPE_CHARACTER + "_")
                    .replace("%", ESCAPE_CHARACTER + "%");
    }

}
