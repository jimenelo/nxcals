package cern.nxcals.common.utils;

import org.apache.hadoop.fs.Path;

import java.util.Iterator;
import java.util.Objects;

public class HdfsCleaner {
    private final Iterator<Path> toRemove;
    private boolean executed = false;

    private int removedCount = 0;
    private long startTime;
    private long endTime;

    public HdfsCleaner(Iterator<Path> toRemove) {
        this.toRemove = Objects.requireNonNull(toRemove);
    }

    public void run() {
        if (executed) {
            return;
        }

        startTime = System.currentTimeMillis();
        while (toRemove.hasNext()) {
            removedCount++;
            toRemove.next();
            toRemove.remove();
        }
        endTime = System.currentTimeMillis();
        executed = true;
    }

    public int removedCount() {
        verifyExecution();
        return removedCount;
    }

    public long timeUsed() {
        verifyExecution();
        return endTime - startTime;
    }

    private void verifyExecution() {
        if (!executed) {
            throw new IllegalStateException("Hdfs Cleaner not executed");
        }
    }
}
