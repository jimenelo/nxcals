package cern.nxcals.common.utils;

import lombok.experimental.UtilityClass;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Collection;
import java.util.List;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collectors;

@UtilityClass
public class MapUtils {
    public static <K1, K2, V> Map<K2, V> mapKeys(Map<K1, V> map, Function<K1, K2> keyMapper) {
        HashMap<K2, V> newMap = new HashMap<>(map.size());
        for (Map.Entry<K1, V> entry : map.entrySet()) {
            newMap.put(keyMapper.apply(entry.getKey()), entry.getValue());
        }
        return newMap;
    }

    public static <K, V> Collector<Map.Entry<K, V>, ?, Map<K, V>> mapCollector() {
        return Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue);
    }

    public static <K, V, C extends Collection<V>> Map<V, K> revertMap(Map<K, C> map) {
        return map.entrySet().stream().flatMap(entry ->
                entry.getValue().stream().map(timeWindow -> Pair.of(timeWindow, entry.getKey()))
        ).collect(MapUtils.mapCollector());
    }

    public static <K1, K2, V> Map<K2, List<Map.Entry<K1, V>>> groupBy(Map<K1, V> map,
            Function<Map.Entry<K1, V>, K2> keyExtractor) {
        return map.entrySet().stream().collect(Collectors.groupingBy(keyExtractor));
    }
}
