/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.common.utils;

import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocatedFileStatus;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.RemoteIterator;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;
import java.time.temporal.TemporalAccessor;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.StringJoiner;
import java.util.function.Predicate;

import static java.lang.String.format;

@Slf4j
@UtilityClass
public class HdfsFileUtils {
    public final String STAGE_DIRECTORY_DATE_PATTERN = "yyyy-MM-dd";
    public final DateTimeFormatter STAGE_DIRECTORY_DATE_FORMATTER = DateTimeFormatter
            .ofPattern(STAGE_DIRECTORY_DATE_PATTERN).withZone(ZoneOffset.UTC);


    public enum SearchType {
        RECURSIVE(true), NONRECURSIVE(false);

        private final boolean value;

        SearchType(boolean value) {
            this.value = value;
        }

        public boolean getValue() {
            return this.value;
        }
    }

    public List<FileStatus> findFilesInPath(FileSystem fileSystem, Path path, SearchType type,
            Predicate<FileStatus> fileStatusPredicate) {
        return findFilesInPath(fileSystem, path, type, fileStatusPredicate, Integer.MAX_VALUE);
    }

    /**
     * Provides status information about all files located under an HDFS directory path.
     *
     * @param fileSystem          the Hadoop filesystem
     * @param path                the {@link Path} instance that represents the directory path
     * @param type                a {@link SearchType} instance that dictates the type (recursive or not) of the search action
     * @param fileStatusPredicate a {@link Predicate} to tests against the fetched files
     * @param maxFiles            a maximum number of files to return
     * @return a {@link List} of all available {@link FileStatus} instances, located under the given path,
     * that their status, match the provided search predicate
     */
    public List<FileStatus> findFilesInPath(FileSystem fileSystem, Path path, SearchType type,
            Predicate<FileStatus> fileStatusPredicate, int maxFiles) {
        log.trace("Searching files for path {}", path);
        List<FileStatus> fileStatuses = new ArrayList<>();
        try {
            if (!fileSystem.exists(path)) {
                log.debug("No such path {}", path);
                return Collections.emptyList();
            }

            RemoteIterator<LocatedFileStatus> iterator = fileSystem.listFiles(path, type.getValue());
            while (iterator.hasNext() && fileStatuses.size() < maxFiles) {
                LocatedFileStatus stat = iterator.next();
                if (fileStatusPredicate.test(stat)) {
                    fileStatuses.add(stat);
                }
            }
            log.debug("Found status information for {} filtered files from max requested {} under HDFS directory: {}",
                    fileStatuses.size(), maxFiles, path);
            return fileStatuses;
        } catch (IOException ex) {
            throw new UncheckedIOException(format("Cannot list files in base path: %s", path), ex);
        }
    }

    public static boolean hasDataFiles(FileSystem fileSystem, Path path) {
        return !findFilesInPath(fileSystem, path, SearchType.RECURSIVE, HdfsFileUtils::isDataFile, 1).isEmpty();
    }

    /**
     * Finds files in the base path directory that match the predicate.
     */
    public List<FileStatus> findDirsAndFilesInPath(FileSystem fileSystem, Path path, SearchType type,
                                                   Predicate<FileStatus> predicate) {
        log.info("Searching dirs for path: {}", path);
        List<FileStatus> output = new ArrayList<>();
        try {
            if (fileSystem.exists(path)) {
                RemoteIterator<LocatedFileStatus> iterator = fileSystem.listFiles(path, type.getValue());
                while (iterator.hasNext()) {
                    LocatedFileStatus stat = iterator.next();
                    if (predicate.test(stat)) {
                        output.add(stat);
                    }
                }
                return output;
            } else {
                log.info("No such path {}", path);
                return Collections.emptyList();
            }
        } catch (IOException ex) {
            throw new UncheckedIOException(format("Cannot list files in base path: %s", path), ex);
        }
    }

    /**
     * Finds files in the base path directory that match the predicate.
     */
    public List<FileStatus> findFilesInPath(FileSystem fileSystem, Path path, SearchType type) {
        return findFilesInPath(fileSystem, path, type, file -> true);
    }

    public boolean isDataFile(FileStatus stat) {
        return isAvroDataFile(stat) || isParquetDataFile(stat);
    }

    public boolean isDataFile(Path stat) {
        return isAvroDataFile(stat) || isParquetDataFile(stat);
    }

    public boolean isAvroDataFile(FileStatus stat) {
        return stat.isFile() && isAvroDataFile(stat.getPath());
    }

    public boolean isParquetDataFile(FileStatus stat) {
        return stat.isFile() && isParquetDataFile(stat.getPath());
    }

    public boolean isAvroDataFile(Path stat) {
        return stat.getName().endsWith(SupportedFileTypePredicate.AVRO.getFileExtension());
    }

    public boolean isParquetDataFile(Path stat) {
        return stat.getName().endsWith(SupportedFileTypePredicate.PARQUET.getFileExtension());
    }

    /**
     * Expands a provided date instance to a nested directory layout that derives out of it's chronological units.
     * The constructed directory layout with a pattern like {@code year/month/dayOfMonth} ex. {@code 2018/2/25},
     * where each 'slash' is representing a directory separator.
     *
     * @param date the {@link String} instance that represents a given date
     * @return a {@link String} representation of the date-derived nested directory layout.
     */
    public String expandDateToNestedPaths(String date) {
        if (StringUtils.isBlank(date)) {
            throw new IllegalArgumentException("Provided date cannot be a null or blank string!");
        }
        TemporalAccessor directoryDate = STAGE_DIRECTORY_DATE_FORMATTER.parse(date);

        return new StringJoiner(Path.SEPARATOR).add(Integer.toString(directoryDate.get(ChronoField.YEAR)))
                .add(Integer.toString(directoryDate.get(ChronoField.MONTH_OF_YEAR)))
                .add(Integer.toString(directoryDate.get(ChronoField.DAY_OF_MONTH))).toString();
    }

    public List<FileStatus> getParquetFilesIn(FileSystem fs, Path path) {
        return getParquetFilesIn(fs, path, HdfsFileUtils.SearchType.NONRECURSIVE);
    }

    public List<FileStatus> getParquetFilesIn(FileSystem fs, Path path, HdfsFileUtils.SearchType searchType) {
        return HdfsFileUtils
                .findFilesInPath(fs, path, searchType, HdfsFileUtils::isParquetDataFile);
    }

    public void rename(FileSystem fs, Path oldName, Path newName) throws IOException {
        fs.rename(oldName, newName);
    }
}
