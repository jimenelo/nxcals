package cern.nxcals.common.utils;

import cern.nxcals.common.qbuilders.CustomRSQLVisitor;
import com.github.rutledgepaulv.qbuilders.conditions.Condition;
import lombok.experimental.UtilityClass;

@UtilityClass
public final class RSQLUtils {
    private static final CustomRSQLVisitor VISITOR = new CustomRSQLVisitor();

    public static String toRSQL(Condition<?> cond) {
        return cond.query(VISITOR);
    }
}