package cern.nxcals.common.utils;

import cern.nxcals.api.exceptions.NotFoundRuntimeException;
import lombok.experimental.UtilityClass;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * Proxyfies calls to delegate.
 */
@UtilityClass
public final class Proxyficator {
    public static <T> T createProxyFor(T delegate, Class<T> clazz) {
        return createProxyFor(delegate, clazz, delegate.getClass().getClassLoader());
    }

    public static <T> T createProxyFor(T delegate, Class<T> clazz, ClassLoader classLoader) {
        return clazz.cast(Proxy.newProxyInstance(classLoader, new Class<?>[] { clazz }, exceptionHandler(delegate)));
    }

    private static <T> InvocationHandler exceptionHandler(T delegate) {
        return (proxy, method, args) -> ignoreNotFoundRuntimeException(delegate, method, args);
    }

    @SuppressWarnings("squid:S00112") // generic exception
    private static <T> Object ignoreNotFoundRuntimeException(T delegate, Method method, Object[] args) throws Throwable {
        try {
            return method.invoke(delegate, args);
        } catch (InvocationTargetException exception) {
            if (exception.getCause() instanceof NotFoundRuntimeException) {
                return null;
            }
            throw exception.getCause();
        }
    }
}
