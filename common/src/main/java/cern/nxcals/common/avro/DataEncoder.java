/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.common.avro;

import cern.cmw.data.Data;
import cern.cmw.datax.ImmutableData;

/**
 * The role of this component is to encode the @see {@link Data} record meta information into the internal service
 * representation. The implementation of this interface is required to know the definition of the system (entity &
 * partition key information).
 *
 * @param <K> Entity key values encoding type.
 * @param <P> Partition key values type.
 * @param <S> Record field definitions type.
 * @param <T> Time key values type.
 */
public interface DataEncoder<K, P, S, T> {

    /**
     * Provide encoding of {@code record} for entity key values.
     *
     * @param record Data to encode.
     * @return an internal representation of the entity key values
     */
    K encodeEntityKeyValues(ImmutableData record);

    /**
     * Provide encoding of {@code record} for partition key values.
     *
     * @param record Data to encode.
     * @return an internal representation of the partition key values
     */
    P encodePartitionKeyValues(ImmutableData record);

    /**
     * Provide encoding of {@code record} for record field definitions.
     *
     * @param record Data to encode.
     * @return an internal representation of the record definition (fields + types), aka schema.
     */
    S encodeRecordFieldDefinitions(ImmutableData record);

    /**
     * Provide encoding of {@code record} for time key values.
     *
     * @param record Data to encode.
     * @return an internal representation of the time key values.
     */
    T encodeTimeKeyValues(ImmutableData record);

}
