package cern.nxcals.common.spark;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * The UDFValueFiltering defines the possible types of filters to apply, this is a copy helper class from FilterOperand.
 * It is done this way to avoid moving the backport domain to common (that creates a circular dependency).
 */
@Getter
@AllArgsConstructor
@SuppressWarnings("squid:S00116")
public enum UDFFilterOperand {
    /**
     * Applicable for NUMERIC, STRING
     */
    IN("IN"), NOT_IN("NOT IN"),

    /**
     * Applicable for NUMERIC
     */
    GREATER(">"), LESS("<"), GREATER_OR_EQUALS(">="), LESS_OR_EQUALS("<="),
    BETWEEN("BETWEEN"), EQUALS("="), AND("AND"), OR("OR"), NONE("IS NOT NULL");

    private String stringValue;

    public static UDFFilterOperand getFilterOperandFor(String value) {
        for (UDFFilterOperand op : UDFFilterOperand.values()) {
            if (op.getStringValue().equals(value)) {
                return op;
            }
        }
        return null;
    }
}
