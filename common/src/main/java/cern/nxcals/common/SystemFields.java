/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.common;

import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import java.util.function.Function;

import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.toMap;

@Getter
public enum SystemFields {
    /**
     * @formatter:off
     */
    NXC_SYSTEM_ID("__sys_nxcals_system_id__"),
    NXC_PARTITION_ID("__sys_nxcals_partition_id__"),
    NXC_ENTITY_ID("__sys_nxcals_entity_id__"),
    NXC_SCHEMA_ID("__sys_nxcals_schema_id__"),
    NXC_TIMESTAMP("__sys_nxcals_timestamp__"),

    //for adaptive compaction & extraction
    NXC_TIME_PARTITION_COLUMN("__sys_nxcals_time_partition__"),
    NXC_ENTITY_BUCKET_COLUMN("__sys_nxcals_entity_bucket__"),
    //This is used only in the new Kafka key as timestamp field name
    NXC_KAFKA_KEY_TIMESTAMP("timestamp"),

    // reserved system fields used for extraction
    NXC_EXTR_VARIABLE_NAME("nxcals_variable_name"),
    NXC_EXTR_TIMESTAMP("nxcals_timestamp"),
    NXC_EXTR_VALUE("nxcals_value"),
    NXC_EXTR_ENTITY_ID("nxcals_entity_id"),
    NXC_EXTR_VARIABLE_FIELD_NAME("nxcals_field_name"),
    ;

    private static final Map<String, SystemFields> SYSTEM_FIELDS_MAP = Arrays.stream(SystemFields.values())
            .collect(collectingAndThen(toMap(SystemFields::getValue, Function.identity()),
                    Collections::unmodifiableMap));

    private final String value;

    SystemFields(String value) {
        if (StringUtils.isBlank(value)) {
            throw new IllegalArgumentException("System field name should be a valid string!");
        }
        this.value = value;
    }

    public static boolean isSystemField(String fieldName) {
        if (fieldName == null) {
            return false;
        }
        return SYSTEM_FIELDS_MAP.containsKey(fieldName);
    }

    public static SystemFields getByName(String fieldName) {
        if (!isSystemField(fieldName)) {
            throw new IllegalArgumentException("No system field exists by the provided name!");
        }
        return SYSTEM_FIELDS_MAP.get(fieldName);
    }

}
