package cern.nxcals.api.utils;

import cern.nxcals.common.config.SparkSessionModifier;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.spark.SparkConf;
import org.apache.spark.sql.SparkSession;

import java.util.List;
import java.util.function.Supplier;

/**
 * SparkSession supplier that lazily creates a new instance of SparkSession and stores it locally for the next retrievals.
 * It also recreates and returns a new instance of the SparkSession in case the current one was closed.
 */
@Slf4j
@RequiredArgsConstructor
public class AutoRecreatingSparkSessionSupplier implements Supplier<SparkSession> {
    private SparkSession currentSession;
    private final SparkConf sparkConf;

    private final List<SparkSessionModifier> sessionModifiers;

    @Override
    public synchronized SparkSession get() {
        if (currentSession == null) {
            log.debug("Initial creation of the SparkSession on the first get() call");
            currentSession = createNewSparkSession();
        } else if (currentSession.sparkContext().isStopped()) {
            log.info("Existing SparkContext has been shutdown. Creating a new spark session");
            currentSession = createNewSparkSession();
        }
        return currentSession;
    }

    private SparkSession createNewSparkSession() {
        SparkSession session = SparkUtils.createSparkSession(sparkConf);
        SparkUtils.modifySparkSession(session, sessionModifiers);
        return session;
    }
}

