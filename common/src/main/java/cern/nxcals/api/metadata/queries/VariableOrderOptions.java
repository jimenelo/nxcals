package cern.nxcals.api.metadata.queries;

import cern.nxcals.api.extraction.metadata.queries.VariableFields;

public final class VariableOrderOptions extends OrderOptions<VariableQueryWithOptions> implements VariableFields<
        OrderOptions<VariableQueryWithOptions>.OrderAction,
        OrderOptions<VariableQueryWithOptions>.OrderAction,
        OrderOptions<VariableQueryWithOptions>.OrderAction,
        OrderOptions<VariableQueryWithOptions>.OrderAction,
        OrderOptions<VariableQueryWithOptions>.OrderAction> {
    VariableOrderOptions(VariableQueryWithOptions returnedBuilder) {
        super(returnedBuilder);
    }

    public OrderOptions<VariableQueryWithOptions>.OrderAction variableDeclaredTypeEnum(String fieldName) {
        return enumerationField(fieldName);
    }
}
