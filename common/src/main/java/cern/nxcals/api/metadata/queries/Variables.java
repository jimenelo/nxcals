package cern.nxcals.api.metadata.queries;

import cern.nxcals.api.domain.VariableDeclaredType;
import cern.nxcals.api.extraction.metadata.queries.VariableFields;
import cern.nxcals.api.metadata.qbuilders.properties.GenericEnumProperty;
import cern.nxcals.api.metadata.qbuilders.properties.GenericExtendedInstantProperty;
import cern.nxcals.api.metadata.qbuilders.properties.GenericKeyValuesProperty;
import cern.nxcals.api.metadata.qbuilders.properties.GenericLongProperty;
import cern.nxcals.api.metadata.qbuilders.properties.GenericStringLikeProperty;
import cern.nxcals.common.annotation.Experimental;

/**
 * Query builder for Variable meta-data.
 * Build your queries Variables.suchThat().variableName().eq("name") etc.
 * Please note that eq() operator also allows for 'like' queries with characters '%' and '_'.
 * You can use regexp with pattern() operator.
 */
@Experimental
public final class Variables
        extends ExtendedBuilder<Variables, ConditionWithOptions<Variables, VariableQueryWithOptions>>
        implements VariableFields<
        GenericStringLikeProperty<Variables, ConditionWithOptions<Variables, VariableQueryWithOptions>>,
        GenericExtendedInstantProperty<Variables, ConditionWithOptions<Variables, VariableQueryWithOptions>>,
        GenericLongProperty<Variables, ConditionWithOptions<Variables, VariableQueryWithOptions>>,
        GenericEnumProperty<Variables, VariableDeclaredType, ConditionWithOptions<Variables, VariableQueryWithOptions>>,
        GenericKeyValuesProperty<Variables, ConditionWithOptions<Variables, VariableQueryWithOptions>>> {
    public static Variables suchThat() {
        return new Variables();
    }

    private Variables() {
        super((condition, canonical) -> new ConditionWithOptions<>(condition, canonical,
                VariableQueryWithOptions::new));
    }

    public GenericEnumProperty<Variables, VariableDeclaredType, ConditionWithOptions<Variables, VariableQueryWithOptions>> enumerationField(
            String fieldName) {
        return super.genericEnumeration(fieldName);
    }
}
