package cern.nxcals.api.metadata.queries;

import cern.nxcals.api.extraction.metadata.queries.EntityFields;
import cern.nxcals.api.metadata.qbuilders.properties.GenericKeyValuesProperty;
import cern.nxcals.api.metadata.qbuilders.properties.GenericLongProperty;
import cern.nxcals.api.metadata.qbuilders.properties.GenericStringLikeProperty;
import cern.nxcals.common.annotation.Experimental;

/**
 * Query builder for Entity meta-data.
 * Build your queries like Entities.suchThat().keyValues().eq("keyValuesWithPattern") etc.
 * Please note that eq() operator also allows for 'like' queries with characters '%' and '_'.
 * You can use regexp with pattern() operator.
 */
@Experimental
public final class Entities extends ExtendedBuilder<Entities, ConditionWithOptions<Entities, EntityQueryWithOptions>>
        implements EntityFields<
        GenericStringLikeProperty<Entities, ConditionWithOptions<Entities, EntityQueryWithOptions>>,
        GenericLongProperty<Entities, ConditionWithOptions<Entities, EntityQueryWithOptions>>,
        GenericKeyValuesProperty<Entities, ConditionWithOptions<Entities, EntityQueryWithOptions>>
        > {

    private Entities() {
        super((condition, canonical) -> new ConditionWithOptions<>(condition, canonical, EntityQueryWithOptions::new));
    }

    public static Entities suchThat() {
        return new Entities();
    }
}
