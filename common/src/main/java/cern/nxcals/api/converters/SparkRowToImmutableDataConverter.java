/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.api.converters;

import cern.cmw.data.DataFactory;
import cern.cmw.data.DiscreteFunction;
import cern.cmw.data.DiscreteFunctionList;
import cern.cmw.datax.DataBuilder;
import cern.cmw.datax.EntryType;
import cern.cmw.datax.ImmutableData;
import cern.cmw.datax.ImmutableEntry;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.ImmutableMap;
import lombok.NonNull;
import lombok.experimental.UtilityClass;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.types.ArrayType;
import org.apache.spark.sql.types.DataType;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import scala.collection.mutable.WrappedArray;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static cern.nxcals.common.avro.SchemaConstants.ARRAY_DIMENSIONS_FIELD_NAME;
import static cern.nxcals.common.avro.SchemaConstants.ARRAY_ELEMENTS_FIELD_NAME;
import static cern.nxcals.common.avro.SchemaConstants.DDF_X_ARRAY_FIELD_NAME;
import static cern.nxcals.common.avro.SchemaConstants.DDF_Y_ARRAY_FIELD_NAME;
import static cern.nxcals.common.avro.SchemaConstants.DF_LIST_FIELD_NAME;
import static org.apache.commons.lang3.ArrayUtils.contains;

@UtilityClass
public class SparkRowToImmutableDataConverter {
    private static final Map<String, EntryType<?>> typeMap = ImmutableMap.<String, EntryType<?>>builder()
            .put(DataTypes.BooleanType.typeName(), EntryType.BOOL)
            .put(DataTypes.ByteType.typeName(), EntryType.INT8)
            .put(DataTypes.ShortType.typeName(), EntryType.INT16)
            .put(DataTypes.IntegerType.typeName(), EntryType.INT32)
            .put(DataTypes.LongType.typeName(), EntryType.INT64)
            .put(DataTypes.FloatType.typeName(), EntryType.FLOAT)
            .put(DataTypes.DoubleType.typeName(), EntryType.DOUBLE)
            .put(DataTypes.StringType.typeName(), EntryType.STRING)
            // StructField from HDFS has 'containsNull=true' for 'elements' and 'dimensions'
            .put(createArraySchema(DataTypes.BooleanType, true).mkString(), EntryType.BOOL_WRAPPER_ARRAY)
            // StructField from HBase has 'containsNull=false' for 'elements' and 'dimensions'
            .put(createArraySchema(DataTypes.BooleanType, false).mkString(), EntryType.BOOL_WRAPPER_ARRAY)
            .put(createArraySchema(DataTypes.ByteType, true).mkString(), EntryType.INT8_WRAPPER_ARRAY)
            .put(createArraySchema(DataTypes.ByteType, false).mkString(), EntryType.INT8_WRAPPER_ARRAY)
            .put(createArraySchema(DataTypes.ShortType, true).mkString(), EntryType.INT16_WRAPPER_ARRAY)
            .put(createArraySchema(DataTypes.ShortType, false).mkString(), EntryType.INT16_WRAPPER_ARRAY)
            .put(createArraySchema(DataTypes.IntegerType, true).mkString(), EntryType.INT32_WRAPPER_ARRAY)
            .put(createArraySchema(DataTypes.IntegerType, false).mkString(), EntryType.INT32_WRAPPER_ARRAY)
            .put(createArraySchema(DataTypes.LongType, true).mkString(), EntryType.INT64_WRAPPER_ARRAY)
            .put(createArraySchema(DataTypes.LongType, false).mkString(), EntryType.INT64_WRAPPER_ARRAY)
            .put(createArraySchema(DataTypes.FloatType, true).mkString(), EntryType.FLOAT_WRAPPER_ARRAY)
            .put(createArraySchema(DataTypes.FloatType, false).mkString(), EntryType.FLOAT_WRAPPER_ARRAY)
            .put(createArraySchema(DataTypes.DoubleType, true).mkString(), EntryType.DOUBLE_WRAPPER_ARRAY)
            .put(createArraySchema(DataTypes.DoubleType, false).mkString(), EntryType.DOUBLE_WRAPPER_ARRAY)
            .put(createArraySchema(DataTypes.StringType, true).mkString(), EntryType.STRING_ARRAY)
            .put(createArraySchema(DataTypes.StringType, false).mkString(), EntryType.STRING_ARRAY)
            .put(createDiscreteFunctionSchema(true).mkString(), EntryType.DISCRETE_FUNCTION)
            .put(createDiscreteFunctionSchema(false).mkString(), EntryType.DISCRETE_FUNCTION)
            .put(createDiscreteFunctionListSchema(true).mkString(), EntryType.DISCRETE_FUNCTION_LIST)
            .put(createDiscreteFunctionListSchema(false).mkString(), EntryType.DISCRETE_FUNCTION_LIST)
            .put(createArraySchema(createDiscreteFunctionSchema(true), true).mkString(),
                    EntryType.DISCRETE_FUNCTION_ARRAY)
            .put(createArraySchema(createDiscreteFunctionSchema(false), false).mkString(),
                    EntryType.DISCRETE_FUNCTION_ARRAY)
            .put(createArraySchema(createDiscreteFunctionListSchema(true), true).mkString(),
                    EntryType.DISCRETE_FUNCTION_LIST_ARRAY)
            .put(createArraySchema(createDiscreteFunctionListSchema(false), false).mkString(),
                    EntryType.DISCRETE_FUNCTION_LIST_ARRAY)
            .build();

    @VisibleForTesting
    static StructType createDiscreteFunctionListSchema(boolean arrayTypesNullable) {
        StructField discreteFunctions = structField(
                DataTypes.createArrayType(createDiscreteFunctionSchema(arrayTypesNullable), arrayTypesNullable),
                DF_LIST_FIELD_NAME);
        return new StructType(new StructField[] { discreteFunctions });
    }

    @VisibleForTesting
    static StructType createDiscreteFunctionSchema(boolean arrayTypesNullable) {
        StructField xArrayField = structField(DataTypes.createArrayType(DataTypes.DoubleType, arrayTypesNullable),
                DDF_X_ARRAY_FIELD_NAME);
        StructField yArrayField = structField(DataTypes.createArrayType(DataTypes.DoubleType, arrayTypesNullable),
                DDF_Y_ARRAY_FIELD_NAME);
        return new StructType(new StructField[] { xArrayField, yArrayField });
    }

    @VisibleForTesting
    static StructType createArraySchema(DataType dataType, boolean arrayTypesNullable) {
        StructField elementsField = structField(DataTypes.createArrayType(dataType, arrayTypesNullable),
                ARRAY_ELEMENTS_FIELD_NAME);
        StructField dimensionsField = structField(DataTypes.createArrayType(DataTypes.IntegerType, arrayTypesNullable),
                ARRAY_DIMENSIONS_FIELD_NAME);
        return new StructType(new StructField[] { elementsField, dimensionsField });

    }

    private static StructField structField(DataType dataType, String testName) {
        return new StructField(testName, dataType, true, null);
    }

    public static ImmutableData convert(@NonNull Row source) {
        final DataBuilder builder = ImmutableData.builder();
        Arrays.stream(source.schema().fields())
                .map(field -> convert(field.name(), source.getAs(field.name()), field.dataType()))
                .forEach(builder::add);
        return builder.build();
    }

    private static ImmutableEntry convert(@NonNull String name, Object value, @NonNull DataType type) {
        if (value == null) {
            return ImmutableEntry.ofNull(name, toEntryType(type));
        } else if (value instanceof Row) {
            return convert(name, (Row) value);
        } else {
            return ImmutableEntry.of(name, unwrapIfArray(value));
        }
    }

    private static EntryType<?> toEntryType(DataType type) {
        String typeLabel = typeLabel(type);
        if (!typeMap.containsKey(typeLabel)) {
            throw new IllegalArgumentException("Error mapping Spark Type: " + typeLabel);
        }
        return typeMap.get(typeLabel);
    }

    private static String typeLabel(DataType type) {
        return type instanceof StructType ? ((StructType) type).mkString() : type.typeName();
    }

    private static ImmutableEntry convert(@NonNull String name, @NonNull Row row) {
        if (isArray(row.schema())) {
            return ImmutableEntry.of(name, toArrayElements(row), toArrayDimensions(row));
        } else if (isDiscreteFunction(row.schema())) {
            return ImmutableEntry.of(name, toDiscreteFunction(row));
        } else if (isDiscreteFunctionList(row.schema())) {
            return ImmutableEntry.of(name, toDiscreteFunctionList(row));
        } else {
            return ImmutableEntry.of(name, convert(row));
        }
    }

    private static DiscreteFunctionList toDiscreteFunctionList(Row row) {
        Object elements = unwrapIfArray(row.getAs(DF_LIST_FIELD_NAME));
        if (elements instanceof Row[]) {
            Row[] rows = (Row[]) elements;
            List<DiscreteFunction> discreteFunctions = Arrays.stream(rows).map(SparkRowToImmutableDataConverter::toDiscreteFunction).collect(Collectors.toList());
            return DataFactory.createDiscreteFunctionList(discreteFunctions);
        } else {
            throw new IllegalArgumentException("Row " + row.json() + " not a DiscreteFunctionList type");
        }
    }


    @VisibleForTesting
    static boolean isDiscreteFunctionList(StructType type) {
        final String[] fieldNames = type.fieldNames();
        return contains(fieldNames, DF_LIST_FIELD_NAME) && fieldNames.length == 1;
    }

    @VisibleForTesting
    static boolean isArray(StructType type) {
        final String[] fieldNames = type.fieldNames();
        return contains(fieldNames, ARRAY_ELEMENTS_FIELD_NAME) && contains(fieldNames, ARRAY_DIMENSIONS_FIELD_NAME);
    }


    @VisibleForTesting
    static boolean isDiscreteFunction(StructType type) {
        final String[] fieldNames = type.fieldNames();
        return contains(fieldNames, DDF_X_ARRAY_FIELD_NAME) && contains(fieldNames, DDF_Y_ARRAY_FIELD_NAME);
    }

    private static Object toArrayElements(Row value) {
        Object elements = unwrapIfArray(value.getAs(ARRAY_ELEMENTS_FIELD_NAME));
        int index = value.fieldIndex(ARRAY_ELEMENTS_FIELD_NAME);
        DataType dataType = ((ArrayType) value.schema().fields()[index].dataType()).elementType();
        if (dataType instanceof StructType) {
            StructType type = (StructType) dataType;
            if (isDiscreteFunction(type)) {
                return toDiscreteFunctionArray((Row[]) elements);
            } else if (isDiscreteFunctionList(type)) {
                return toDiscreteFunctionListArray((Row[]) elements);
            } else {
                throw new IllegalStateException("Unknown row data type " + type.mkString());
            }
        } else {
            return elements;
        }
    }

    private static int[] toArrayDimensions(Row value) {
        return (int[]) ArrayUtils.toPrimitive(unwrapIfArray(value.getAs(ARRAY_DIMENSIONS_FIELD_NAME)));
    }

    private static DiscreteFunction toDiscreteFunction(Row value) {
        double[] xArray = (double[]) ArrayUtils.toPrimitive(unwrapIfArray(value.getAs(DDF_X_ARRAY_FIELD_NAME)));
        double[] yArray = (double[]) ArrayUtils.toPrimitive(unwrapIfArray(value.getAs(DDF_Y_ARRAY_FIELD_NAME)));
        return DataFactory.createDiscreteFunction(xArray, yArray);
    }

    private static DiscreteFunction[] toDiscreteFunctionArray(Row[] value) {

        return Arrays.stream(value).map(SparkRowToImmutableDataConverter::toDiscreteFunction)
                .toArray(DiscreteFunction[]::new);
    }

    private static DiscreteFunctionList[] toDiscreteFunctionListArray(Row[] value) {
        return Arrays.stream(value).map(SparkRowToImmutableDataConverter::toDiscreteFunctionList)
                .toArray(DiscreteFunctionList[]::new);
    }

    @SuppressWarnings("unchecked")
    private static Object unwrapIfArray(Object val) {
        if (val == null) {
            throw new IllegalArgumentException("Illegal Array: null elements");
        }
        return (val instanceof WrappedArray ? ((WrappedArray) val).array() : val);
    }

}
