package cern.nxcals.api.converters;

import avro.shaded.com.google.common.primitives.Booleans;
import cern.cmw.data.DiscreteFunction;
import cern.cmw.data.DiscreteFunctionList;
import cern.cmw.datax.EntryType;
import cern.cmw.datax.ImmutableData;
import cern.cmw.datax.ImmutableEntry;
import cern.cmw.datax.enumeration.EnumValue;
import cern.nxcals.common.utils.IllegalCharacterConverter;
import com.google.common.collect.ImmutableMap;
import com.google.common.primitives.Doubles;
import com.google.common.primitives.Floats;
import com.google.common.primitives.Ints;
import com.google.common.primitives.Longs;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.apache.avro.Schema;
import org.apache.avro.SchemaBuilder;
import org.apache.avro.SchemaParseException;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericRecord;
import org.apache.commons.lang3.ArrayUtils;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static cern.cmw.datax.EntryType.BOOL;
import static cern.cmw.datax.EntryType.BOOL_ARRAY;
import static cern.cmw.datax.EntryType.DATA;
import static cern.cmw.datax.EntryType.DATA_ARRAY;
import static cern.cmw.datax.EntryType.DISCRETE_FUNCTION;
import static cern.cmw.datax.EntryType.DISCRETE_FUNCTION_ARRAY;
import static cern.cmw.datax.EntryType.DISCRETE_FUNCTION_LIST;
import static cern.cmw.datax.EntryType.DISCRETE_FUNCTION_LIST_ARRAY;
import static cern.cmw.datax.EntryType.DOUBLE;
import static cern.cmw.datax.EntryType.DOUBLE_ARRAY;
import static cern.cmw.datax.EntryType.ENUM;
import static cern.cmw.datax.EntryType.ENUM_ARRAY;
import static cern.cmw.datax.EntryType.ENUM_SET;
import static cern.cmw.datax.EntryType.FLOAT;
import static cern.cmw.datax.EntryType.FLOAT_ARRAY;
import static cern.cmw.datax.EntryType.INT16;
import static cern.cmw.datax.EntryType.INT16_ARRAY;
import static cern.cmw.datax.EntryType.INT32;
import static cern.cmw.datax.EntryType.INT32_ARRAY;
import static cern.cmw.datax.EntryType.INT64;
import static cern.cmw.datax.EntryType.INT64_ARRAY;
import static cern.cmw.datax.EntryType.INT8;
import static cern.cmw.datax.EntryType.INT8_ARRAY;
import static cern.cmw.datax.EntryType.STRING;
import static cern.cmw.datax.EntryType.STRING_ARRAY;
import static cern.nxcals.common.avro.SchemaConstants.ARRAY_DIMENSIONS_FIELD_NAME;
import static cern.nxcals.common.avro.SchemaConstants.ARRAY_ELEMENTS_FIELD_NAME;
import static cern.nxcals.common.avro.SchemaConstants.BOOLEAN_MULTI_ARRAY_SCHEMA_NAME;
import static cern.nxcals.common.avro.SchemaConstants.DDF_X_ARRAY_FIELD_NAME;
import static cern.nxcals.common.avro.SchemaConstants.DDF_Y_ARRAY_FIELD_NAME;
import static cern.nxcals.common.avro.SchemaConstants.DF_LIST_FIELD_NAME;
import static cern.nxcals.common.avro.SchemaConstants.DF_LIST_MULTI_ARRAY_SCHEMA_NAME;
import static cern.nxcals.common.avro.SchemaConstants.DF_LIST_SCHEMA_NAME;
import static cern.nxcals.common.avro.SchemaConstants.DF_MULTI_ARRAY_SCHEMA_NAME;
import static cern.nxcals.common.avro.SchemaConstants.DF_SCHEMA_NAME;
import static cern.nxcals.common.avro.SchemaConstants.DOUBLE_MULTI_ARRAY_SCHEMA_NAME;
import static cern.nxcals.common.avro.SchemaConstants.FLOAT_MULTI_ARRAY_SCHEMA_NAME;
import static cern.nxcals.common.avro.SchemaConstants.INT_MULTI_ARRAY_SCHEMA_NAME;
import static cern.nxcals.common.avro.SchemaConstants.LONG_MULTI_ARRAY_SCHEMA_NAME;
import static cern.nxcals.common.avro.SchemaConstants.RECORD_NAMESPACE;
import static cern.nxcals.common.avro.SchemaConstants.STRING_MULTI_ARRAY_SCHEMA_NAME;
import static java.util.Arrays.asList;

@Slf4j
@UtilityClass
public class ImmutableDataToAvroConverter {
    /**
     * All Data related possible schema types definitions.
     */
    public static final Schema booleanFieldSchemaNullable = SchemaBuilder.nullable().booleanType();
    public static final Schema intFieldSchemaNullable = SchemaBuilder.nullable().intType();
    public static final Schema longFieldSchemaNullable = SchemaBuilder.nullable().longType();
    public static final Schema floatFieldSchemaNullable = SchemaBuilder.nullable().floatType();
    public static final Schema doubleFieldSchemaNullable = SchemaBuilder.nullable().doubleType();
    public static final Schema stringFieldSchemaNullable = SchemaBuilder.nullable().stringType();
    public static final Schema booleanArrayFieldSchemaNullable = SchemaBuilder.nullable().array()
            .items(SchemaBuilder.builder().booleanType());
    public static final Schema intArrayFieldSchemaNullable = SchemaBuilder.nullable().array()
            .items(SchemaBuilder.builder().intType());
    public static final Schema longArrayFieldSchemaNullable = SchemaBuilder.nullable().array()
            .items(SchemaBuilder.builder().longType());
    public static final Schema floatArrayFieldSchemaNullable = SchemaBuilder.nullable().array()
            .items(SchemaBuilder.builder().floatType());
    public static final Schema doubleArrayFieldSchemaNullable = SchemaBuilder.nullable().array()
            .items(SchemaBuilder.builder().doubleType());
    public static final Schema stringArrayFieldSchemaNullable = SchemaBuilder.nullable().array()
            .items(stringFieldSchemaNullable);
    public static final Schema ddfFieldSchema = createDiscreteFunctionSchemaType();
    public static final Schema ddfFieldSchemaNullable = SchemaBuilder.nullable().type(ddfFieldSchema);
    public static final Schema ddfArrayFieldSchema = SchemaBuilder.array().items(ddfFieldSchema);
    public static final Schema ddfArrayFieldSchemaNullable = SchemaBuilder.nullable().type(ddfArrayFieldSchema);
    public static final Schema ddfListFieldSchema = createDiscreteFunctionListSchemaType();
    public static final Schema ddfListFieldSchemaNullable = SchemaBuilder.nullable().type(ddfListFieldSchema);
    public static final Schema ddfListArrayFieldSchema = SchemaBuilder.array().items(ddfListFieldSchema);
    public static final Schema ddfListArrayFieldSchemaNullable = SchemaBuilder.nullable().type(ddfListArrayFieldSchema);

    public static final Schema booleanMultiArrayFieldSchema = createMultiArraySchemaType(
            BOOLEAN_MULTI_ARRAY_SCHEMA_NAME, booleanArrayFieldSchemaNullable);
    public static final Schema intMultiArrayFieldSchema = createMultiArraySchemaType(INT_MULTI_ARRAY_SCHEMA_NAME,
            intArrayFieldSchemaNullable);
    public static final Schema longMultiArrayFieldSchema = createMultiArraySchemaType(LONG_MULTI_ARRAY_SCHEMA_NAME,
            longArrayFieldSchemaNullable);
    public static final Schema floatMultiArrayFieldSchema = createMultiArraySchemaType(FLOAT_MULTI_ARRAY_SCHEMA_NAME,
            floatArrayFieldSchemaNullable);
    public static final Schema doubleMultiArrayFieldSchema = createMultiArraySchemaType(DOUBLE_MULTI_ARRAY_SCHEMA_NAME,
            doubleArrayFieldSchemaNullable);
    public static final Schema stringMultiArrayFieldSchema = createMultiArraySchemaType(STRING_MULTI_ARRAY_SCHEMA_NAME,
            stringArrayFieldSchemaNullable);
    public static final Schema ddfMultiArrayFieldSchema = createMultiArraySchemaType(DF_MULTI_ARRAY_SCHEMA_NAME,
            ddfArrayFieldSchemaNullable);
    public static final Schema ddfListMultiArrayFieldSchema = createMultiArraySchemaType(DF_LIST_MULTI_ARRAY_SCHEMA_NAME,
            ddfListArrayFieldSchemaNullable);

    public static final Schema booleanMultiArrayFieldSchemaNullable = SchemaBuilder.nullable()
            .type(booleanMultiArrayFieldSchema);
    public static final Schema intMultiArrayFieldSchemaNullable = SchemaBuilder.nullable()
            .type(intMultiArrayFieldSchema);
    public static final Schema longMultiArrayFieldSchemaNullable = SchemaBuilder.nullable()
            .type(longMultiArrayFieldSchema);
    public static final Schema floatMultiArrayFieldSchemaNullable = SchemaBuilder.nullable()
            .type(floatMultiArrayFieldSchema);
    public static final Schema doubleMultiArrayFieldSchemaNullable = SchemaBuilder.nullable()
            .type(doubleMultiArrayFieldSchema);
    public static final Schema stringMultiArrayFieldSchemaNullable = SchemaBuilder.nullable()
            .type(stringMultiArrayFieldSchema);
    public static final Schema ddfMultiArrayFieldSchemaNullable = SchemaBuilder.nullable()
            .type(ddfMultiArrayFieldSchema);
    public static final Schema ddfListMultiArrayFieldSchemaNullable = SchemaBuilder.nullable()
            .type(ddfListMultiArrayFieldSchema);



    public static final Map<EntryType<?>, Schema> TYPE_TO_SCHEMA_MAP = new ImmutableMap.Builder<EntryType<?>, Schema>()
            .put(BOOL, booleanFieldSchemaNullable)
            .put(BOOL_ARRAY, booleanMultiArrayFieldSchemaNullable)
            .put(INT8, intFieldSchemaNullable)
            .put(INT16, intFieldSchemaNullable)
            .put(INT32, intFieldSchemaNullable)
            .put(INT8_ARRAY, intMultiArrayFieldSchemaNullable)
            .put(INT16_ARRAY, intMultiArrayFieldSchemaNullable)
            .put(INT32_ARRAY, intMultiArrayFieldSchemaNullable)
            .put(INT64, longFieldSchemaNullable)
            .put(INT64_ARRAY, longMultiArrayFieldSchemaNullable)
            .put(FLOAT, floatFieldSchemaNullable)
            .put(FLOAT_ARRAY, floatMultiArrayFieldSchemaNullable)
            .put(DOUBLE, doubleFieldSchemaNullable)
            .put(DOUBLE_ARRAY, doubleMultiArrayFieldSchemaNullable)
            .put(STRING, stringFieldSchemaNullable)
            .put(STRING_ARRAY, stringMultiArrayFieldSchemaNullable)
            .put(DISCRETE_FUNCTION, ddfFieldSchemaNullable)
            .put(DISCRETE_FUNCTION_ARRAY, ddfMultiArrayFieldSchemaNullable)
            .put(DISCRETE_FUNCTION_LIST, ddfListFieldSchemaNullable)
            .put(DISCRETE_FUNCTION_LIST_ARRAY, ddfListMultiArrayFieldSchemaNullable)
            .put(ENUM, stringFieldSchemaNullable)
            .put(ENUM_SET, stringMultiArrayFieldSchemaNullable)
            .put(ENUM_ARRAY, stringMultiArrayFieldSchemaNullable)

            .build();



    private static Schema createMultiArraySchemaType(String typeName, Schema elementType) {
        return SchemaBuilder.record(typeName).namespace(RECORD_NAMESPACE).fields().name(ARRAY_ELEMENTS_FIELD_NAME)
                .type(elementType).noDefault().name(ARRAY_DIMENSIONS_FIELD_NAME).type(intArrayFieldSchemaNullable)
                .noDefault().endRecord();
    }

    private static Schema createDiscreteFunctionSchemaType() {
        return SchemaBuilder.record(DF_SCHEMA_NAME).namespace(RECORD_NAMESPACE).fields()
                .name(DDF_X_ARRAY_FIELD_NAME).type(doubleArrayFieldSchemaNullable).noDefault()
                .name(DDF_Y_ARRAY_FIELD_NAME).type(doubleArrayFieldSchemaNullable).noDefault().endRecord();
    }

    /*
    df_list schema is a record with a field "discrete_functions" of a value of type Array of DiscreteFunction type.
     */
    private static Schema createDiscreteFunctionListSchemaType() {
        return SchemaBuilder.record(DF_LIST_SCHEMA_NAME).namespace(RECORD_NAMESPACE).fields()
                .name(DF_LIST_FIELD_NAME).type(ddfArrayFieldSchemaNullable).noDefault()
                .endRecord();
    }

    /*
     * It has to iterate over the fields from the Data data as the schema contains here more fields (our system
     * fields) that are not present in the data.
     */
    public static GenericRecord createDataGenericRecord(ImmutableData data, Schema schema) {
        GenericRecord genericRecord = new GenericData.Record(schema);
        for (ImmutableEntry entry : data.getEntries()) {
            String fieldName = IllegalCharacterConverter.get().getLegalIfCached(entry.getName());
            try {
                genericRecord.put(fieldName, createGenericRecordFieldValue(entry, schema, fieldName));
            } catch (SchemaParseException exception) {
                fieldName = IllegalCharacterConverter.get().convertToLegal(entry.getName());
                genericRecord.put(fieldName, createGenericRecordFieldValue(entry, schema, fieldName));
            }
        }
        return genericRecord;
    }

    /*
     * Used to create a value for the GenericRecord field from the cmw Data Entry.
     */
    public static Object createGenericRecordFieldValue(ImmutableEntry entry, Schema schema, String fieldName) {
        Object value = entry.get();
        if (value == null) {
            return null;
        }
        if (isScalar(entry)) {
            return handleScalars(entry, value, schema, fieldName);
        } else {
            return handleArrays(entry, value);
        }
    }

    private static boolean isScalar(ImmutableEntry entry) {
        return ArrayUtils.isEmpty(entry.getDims());
    }

    @SuppressWarnings({ "squid:S1698" }) //== instead of equals
    private static Object handleScalars(ImmutableEntry data, Object value, Schema schema, String fieldName) {
        EntryType<?> type = data.getType();
        if (INT8.equals(type)) {
            return Integer.valueOf(data.getAs(INT8));
        } else if (INT16.equals(type)) {
            return Integer.valueOf(data.getAs(INT16));
        } else if (DATA.equals(type)) {
            return createDataGenericRecord(data.getAs(DATA),
                    getRecordSchemaFromNullableUnion(schema.getField(fieldName).schema()));
        } else if (DISCRETE_FUNCTION.equals(type)) {
            return createDiscreteFunctionGenericRecord(ddfFieldSchema, data.getAs(DISCRETE_FUNCTION));
        }  else if (DISCRETE_FUNCTION_LIST.equals(type)) {
                return createDiscreteFunctionListRecord((DiscreteFunctionList) value, ddfListFieldSchema);
        } else if (ENUM.equals(type)) {
            return data.getAs(ENUM).getName();
        } else if (ENUM_SET.equals(type)) {
            return handleEnumCollection(data.getAs(ENUM_SET).toArray());
        }
        return value;
    }

    private static Schema getRecordSchemaFromNullableUnion(Schema schema) {
        Schema output = getSchemaFromNullableUnion(schema);
        if (Schema.Type.RECORD.equals(output.getType())) {
            return output;
        } else {
            throw new IllegalArgumentException(
                    "This schema is not an expected RECORD or UNION with RECORD type: " + schema.toString());
        }
    }

    private static Schema getSchemaFromNullableUnion(Schema schema) {
        Schema.Type type = schema.getType();
        if (Schema.Type.UNION.equals(type)) {
            for (Schema sch : schema.getTypes()) {
                if (!Schema.Type.NULL.equals(sch.getType())) {
                    // return the first non NULL type from this UNION, expects to be a union of only 2 types, normal +
                    // NULL.
                    return sch;
                }
            }
            throw new IllegalArgumentException("There is no not NULL type in this schema " + schema.toString());
        } else {
            // not an union, just return it
            return schema;
        }
    }

    private static Object handleEnumCollection(EnumValue[] array) {
        List<String> values = new LinkedList<>();
        for (EnumValue val : array) {
            values.add(val.getName());
        }
        return createMultiArrayGenericRecord(stringMultiArrayFieldSchema, values, new int[] { values.size() });
    }

    @SuppressWarnings({ "squid:MethodCyclomaticComplexity" })
    private static Object handleArrays(ImmutableEntry data, Object value) {
        EntryType<?> type = data.getType();
        if (DATA_ARRAY.equals(type)) {
            // this must be built dynamically - should we have array of the same types or different???
            throw new IllegalArgumentException("DataType not supported: DATA_ARRAY");
        }
        if (BOOL_ARRAY.equals(type)) {
            return createMultiArrayGenericRecord(booleanMultiArrayFieldSchema, Booleans.asList((boolean[]) value),
                    data.getDims());
        } else if (INT8_ARRAY.equals(type)) {
            return createMultiArrayGenericRecord(intMultiArrayFieldSchema, convertToInt((byte[]) value),
                    data.getDims());
        } else if (INT16_ARRAY.equals(type)) {
            return createMultiArrayGenericRecord(intMultiArrayFieldSchema, convertToInt((short[]) value),
                    data.getDims());
        } else if (INT32_ARRAY.equals(type)) {
            return createMultiArrayGenericRecord(intMultiArrayFieldSchema, Ints.asList((int[]) value), data.getDims());
        } else if (INT64_ARRAY.equals(type)) {
            return createMultiArrayGenericRecord(longMultiArrayFieldSchema, Longs.asList((long[]) value),
                    data.getDims());
        } else if (FLOAT_ARRAY.equals(type)) {
            return createMultiArrayGenericRecord(floatMultiArrayFieldSchema, Floats.asList((float[]) value),
                    data.getDims());
        } else if (DOUBLE_ARRAY.equals(type)) {
            return createMultiArrayGenericRecord(doubleMultiArrayFieldSchema, Doubles.asList((double[]) value),
                    data.getDims());
        } else if (DISCRETE_FUNCTION_ARRAY.equals(type)) {
            return createMultiArrayGenericRecord(ddfMultiArrayFieldSchema,
                    createArrayOfDiscreteFunctionGenericRecord(ddfFieldSchema, asList((DiscreteFunction[]) value)),
                    data.getDims());

        } else if (DISCRETE_FUNCTION_LIST_ARRAY.equals(type)) {
            DiscreteFunctionList[] ddfListArray = (DiscreteFunctionList[]) value;
            List<Object> convertedToGenericRecords = new ArrayList<>(ddfListArray.length);
            for (DiscreteFunctionList discreteFunctionList : ddfListArray) {
                convertedToGenericRecords
                        .add(createDiscreteFunctionListRecord(discreteFunctionList, ddfListFieldSchema));
            }
            return createMultiArrayGenericRecord(ddfListMultiArrayFieldSchema,
                    convertedToGenericRecords, data.getDims());
        } else if (STRING_ARRAY.equals(type)) {
            return createMultiArrayGenericRecord(stringMultiArrayFieldSchema, asList((String[]) value), data.getDims());
        } else if (ENUM_ARRAY.equals(type)) {
            return handleEnumCollection(data.getAs(ENUM_ARRAY));
        } else {
            throw new IllegalArgumentException(MessageFormat.format("DataType {0} not supported for arrays ", type));
        }
    }

    private static GenericRecord createDiscreteFunctionListRecord(DiscreteFunctionList value,
            Schema ddfListFieldSchema) {
        GenericRecord record = new GenericData.Record(ddfListFieldSchema);
        record.put(DF_LIST_FIELD_NAME, createArrayOfDiscreteFunctionGenericRecord(ddfFieldSchema,value.getFunctions()));
        return record;
    }

    private static Collection<Integer> convertToInt(byte[] bytes) {
        int[] ret = new int[bytes.length];
        for (int i = 0; i < bytes.length; i++) {
            ret[i] = bytes[i];
        }
        return Ints.asList(ret);
    }

    private static Collection<Integer> convertToInt(short[] shorts) {
        int[] ret = new int[shorts.length];
        for (int i = 0; i < shorts.length; i++) {
            ret[i] = shorts[i];
        }
        return Ints.asList(ret);
    }

    private static List<Object> createArrayOfDiscreteFunctionGenericRecord(Schema internalSchema,
            Collection<DiscreteFunction> entries) {
        List<Object> ret = new ArrayList<>(entries.size());
        for (DiscreteFunction df : entries) {
            ret.add(createDiscreteFunctionGenericRecord(internalSchema, df));
        }
        return ret;
    }

    private static GenericRecord createMultiArrayGenericRecord(Schema internalSchema, Collection<?> records,
            int[] dims) {
        GenericRecord record = new GenericData.Record(internalSchema);
        record.put(ARRAY_ELEMENTS_FIELD_NAME, records);
        record.put(ARRAY_DIMENSIONS_FIELD_NAME, Ints.asList(dims));
        return record;
    }

    private static Object createDiscreteFunctionGenericRecord(Schema internalSchema,
            DiscreteFunction discreteFunction) {
        GenericRecord record = new GenericData.Record(internalSchema);
        record.put(DDF_X_ARRAY_FIELD_NAME, Doubles.asList(discreteFunction.getXArray()));
        record.put(DDF_Y_ARRAY_FIELD_NAME, Doubles.asList(discreteFunction.getYArray()));
        return record;
    }
}
