package cern.nxcals.api.converters;

import lombok.experimental.UtilityClass;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@UtilityClass
public class Py4jLongConverter {

    public Set<Long> convert(Set input) {
        Set<Long> convertedVarIds = new HashSet<>();
        for (Object var : input) {
            if (var instanceof Integer) {
                convertedVarIds.add(((Integer) var).longValue());
            } else if (var instanceof Long) {
                convertedVarIds.add((Long) var);
            } else {
                throw new IllegalArgumentException("Set of " + var.getClass().getName() +
                        " not supported as an argument");
            }
        }
        return convertedVarIds;
    }

    public Map<String, Set<Long>> convertMap(Map<String, Set> input) {

        Map<String, Set<Long>> convertedMap = new HashMap<>();

        for (Map.Entry entry : input.entrySet()) {
            convertedMap.put((String) entry.getKey(), convert((Set) entry.getValue()));
        }

        return convertedMap;
    }
}
