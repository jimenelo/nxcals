package cern.nxcals.api.extraction.data.builders.fluent;

import lombok.NonNull;

import java.util.Map;

import static java.util.Objects.requireNonNull;

public class KeyValueStage<T> extends Stage<KeyValueStageLoop<T>, T> {
    KeyValueStage(QueryData<T> data) {
        super(data);
    }

    /**
     * Add key and value to entity query.
     *
     * @param key   key which entity must contain
     * @param value value, which entity must contain under given key
     * @return builder with opened query for given entity
     */
    public KeyValueStageLoop<T> keyValue(@NonNull String key, @NonNull Object value) {
        data().addToEntity(key, value, false);
        return new KeyValueStageLoop<>(data());
    }

    /**
     * Add key and value pattern to entity query. Patterns should be in Oracle SQL form (% and _ as wildcards).
     *
     * @param key   key pattern
     * @param value value pattern
     * @return builder with opened query for given entity
     */
    public KeyValueStageLoop<T> keyValueLike(@NonNull String key, @NonNull Object value) {
        data().addToEntity(key, value, true);
        return new KeyValueStageLoop<>(data());
    }

    /**
     * Add keys and values to entity query.
     *
     * @param keyValues map with keys and values which certain entity must contain
     * @return builder with opened query for given entity
     */
    public KeyValueStageLoop<T> keyValues(@NonNull Map<String, Object> keyValues) {
        requireNonNull(keyValues).forEach((key, value) -> data().addToEntity(key, value, false));
        return new KeyValueStageLoop<>(data());
    }

    /**
     * Add key and value patterns to entity query. Patterns should be in Oracle SQL form (% and _ as wildcards).
     *
     * @param keyValues map with keys and values patterns which certain entity must contain
     * @return builder with opened query for given entity
     */
    public KeyValueStageLoop<T> keyValuesLike(@NonNull Map<String, Object> keyValues) {
        requireNonNull(keyValues).forEach((key, value) -> data().addToEntity(key, value, true));
        return new KeyValueStageLoop<>(data());
    }
}