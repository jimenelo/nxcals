package cern.nxcals.api.extraction.data.builders.fluent.v2;

import cern.nxcals.api.extraction.data.builders.fluent.QueryData;

public abstract class AbstractDataQuery<Z> {

    public SystemOrIdStage<VariableStage<Z>, VariableStageLoop<Z>, Z> variables() {
        return StageSequenceVariables.<Z>sequence().apply(queryData());
    }

    public SystemOrIdStage<KeyValueStage<Z>, KeyValueStageLoop<Z>, Z> entities() {
        return StageSequenceEntities.<Z>sequence().apply(queryData());
    }

    protected abstract QueryData<Z> queryData();
}
