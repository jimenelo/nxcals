package cern.nxcals.api.extraction.data;

import cern.cmw.datax.ImmutableData;
import cern.nxcals.api.converters.AvroToImmutableDataConverter;
import cern.nxcals.api.extraction.thin.AvroData;
import lombok.NonNull;
import lombok.experimental.UtilityClass;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Converter from {@link AvroData} to List of {@link ImmutableData}.
 */
@UtilityClass
public class Datax {
    public List<ImmutableData> records(@NonNull AvroData source) {
        return Avro.records(source).stream().map(AvroToImmutableDataConverter::convert).collect(Collectors.toList());
    }
}
