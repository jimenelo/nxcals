package cern.nxcals.api.extraction.metadata.queries;

public interface VariableFields<S, I, L, E, K> {
    L longField(String fieldName);

    S stringLike(String fieldName);

    I instantField(String fieldName);

    K entityKeyValues(String fieldName);

    E enumerationField(String fieldName);

    default L id() {
        return longField("id");
    }

    default L systemId() {
        return longField("system.id");
    }

    default L configsEntityId() {
        return longField("variableConfigs.entity.id");
    }

    default K configsEntityKeyValues() {
        return entityKeyValues("variableConfigs.entity.keyValues");
    }

    /**
     * @deprecated this method will be removed in a future release. Please use configsEntityKeyValues()
     */
    @Deprecated
    default S configsEntityKeyValuesString() {
        return stringLike("variableConfigs.entity.keyValues");
    }

    default S configsFieldName() {
        return stringLike("variableConfigs.fieldName");
    }

    default I configsValidFromStamp() {
        return instantField("variableConfigs.validFromStamp");
    }

    default I configsValidToStamp() {
        return instantField("variableConfigs.validToStamp");
    }

    default S systemName() {
        return stringLike("system.name");
    }

    default S variableName() {
        return stringLike("variableName");
    }

    default S unit() {
        return stringLike("unit");
    }

    default S description() {
        return stringLike("description");
    }

    default E declaredType() {
        return enumerationField("declaredType");
    }
}
