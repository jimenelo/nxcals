package cern.nxcals.api.extraction.metadata.queries;

import com.github.rutledgepaulv.qbuilders.builders.QBuilder;
import com.github.rutledgepaulv.qbuilders.properties.concrete.StringProperty;

public interface StringLikeProperty<T extends QBuilder<T>> extends StringLikeCaseInsensitiveProperty<T>, StringProperty<T> {
    StringLikeCaseInsensitiveProperty<T> caseInsensitive();
}