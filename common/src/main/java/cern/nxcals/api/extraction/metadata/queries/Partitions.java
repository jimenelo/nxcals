package cern.nxcals.api.extraction.metadata.queries;

import com.github.rutledgepaulv.qbuilders.properties.concrete.BooleanProperty;
import com.github.rutledgepaulv.qbuilders.properties.concrete.LongProperty;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * Query builder for Partition meta-data.
 * Build your queries Partitions.suchThat().keyValues().eq("xxx") etc.
 * Please note that eq() operator also allows for 'like' queries with characters '%' and '_'.
 * You can use regexp with pattern() operator.
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class Partitions extends BaseBuilder<Partitions> {
    public static Partitions suchThat() {
        return new Partitions();
    }

    public LongProperty<Partitions> id() {
        return longNum("id");
    }

    /**
     * @deprecated this method will be removed in a future release. Please use keyValues()
     */
    @Deprecated
    public StringLikeProperty<Partitions> keyValuesString() {
        return stringLike("keyValues");
    }

    public LongProperty<Partitions> systemId() {
        return longNum("system.id");
    }

    public StringLikeProperty<Partitions> systemName() {
        return stringLike("system.name");
    }

    public KeyValuesProperty<Partitions> keyValues() {
        return partitionKeyValues("keyValues");
    }

    public BooleanProperty<Partitions> isUpdatable() {
        return bool("properties.updatable");
    }
}