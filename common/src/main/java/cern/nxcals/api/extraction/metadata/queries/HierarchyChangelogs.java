package cern.nxcals.api.extraction.metadata.queries;

import cern.nxcals.api.domain.OperationType;
import com.github.rutledgepaulv.qbuilders.properties.concrete.EnumProperty;
import com.github.rutledgepaulv.qbuilders.properties.concrete.LongProperty;
import com.github.rutledgepaulv.qbuilders.structures.FieldPath;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * Query builder for HierarchyChangelog meta-data.
 * Build your queries HierarchyChangelogs.suchThat().newHierarchyName().eq("name") etc.
 * Please note that eq() operator also allows for 'like' queries with characters '%' and '_'.
 * You can use regexp with pattern() operator.
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class HierarchyChangelogs extends BaseBuilder<HierarchyChangelogs> {
    public static HierarchyChangelogs suchThat() {
        return new HierarchyChangelogs();
    }

    public InstantPropertyDelegate<HierarchyChangelogs> creationTimeUtc() {
        return new InstantPropertyDelegate<>(new FieldPath("createTimeUtc"), self());
    }

    public StringLikeProperty<HierarchyChangelogs> newHierarchyName() {
        return stringLike("newHierarchyName");
    }

    public StringLikeProperty<HierarchyChangelogs> oldHierarchyName() {
        return stringLike("oldHierarchyName");
    }

    public LongProperty<HierarchyChangelogs> hierarchyId() {
        return longNum("hierarchyId");
    }

    public LongProperty<HierarchyChangelogs> id() {
        return longNum("id");
    }

    public LongProperty<HierarchyChangelogs> oldParentId() {
        return longNum("oldParentId");
    }

    public LongProperty<HierarchyChangelogs> newParentId() {
        return longNum("newParentId");
    }

    public LongProperty<HierarchyChangelogs> oldGroupId() {
        return longNum("oldGroupId");
    }

    public LongProperty<HierarchyChangelogs> newGroupId() {
        return longNum("newGroupId");
    }

    public LongProperty<HierarchyChangelogs> oldSystemId() {
        return longNum("oldSystemId");
    }

    public LongProperty<HierarchyChangelogs> newSystemId() {
        return longNum("newSystemId");
    }

    public EnumProperty<HierarchyChangelogs, OperationType> opType() {
        return enumeration("opType");
    }

    public StringLikeProperty<HierarchyChangelogs> clientInfo() {
        return stringLike("clientInfo");
    }

}
