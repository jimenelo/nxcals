package cern.nxcals.api.extraction.metadata.queries;

import com.github.rutledgepaulv.qbuilders.properties.concrete.LongProperty;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * Query builder for EntitySchema meta-data.
 * Build your queries like EntitySchemas.suchThat().schema().eq("xxx") etc.
 * Please note that eq() operator also allows for 'like' queries with characters '%' and '_'.
 * You can use regexp with pattern() operator.
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class EntitySchemas extends BaseBuilder<EntitySchemas> {
    public static EntitySchemas suchThat() {
        return new EntitySchemas();
    }
    public LongProperty<EntitySchemas> id() {
        return longNum("id");
    }
    public StringLikeProperty<EntitySchemas> schema() {
        return stringLike("content");
    }

    public StringLikeProperty<EntitySchemas> schemaHash() {
        return stringLike("contentHash");
    }
}