package cern.nxcals.api.extraction.data.builders.fluent;

import cern.nxcals.common.domain.CallDetails;
import lombok.experimental.UtilityClass;

@UtilityClass
public class ThreadLocalStorage {
    private final ThreadLocal<CallDetails> threadLocal = new ThreadLocal<>();

    public CallDetails getCallDetails() {
        return threadLocal.get();
    }

    public void setCallDetails(CallDetails callDetails) {
        threadLocal.set(callDetails);
    }

    public void clear() {
        threadLocal.remove();
    }
}
