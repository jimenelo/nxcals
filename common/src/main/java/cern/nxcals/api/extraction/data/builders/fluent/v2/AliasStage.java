package cern.nxcals.api.extraction.data.builders.fluent.v2;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import lombok.NonNull;

import java.util.Map;
import java.util.Set;

/** 
 * @param <S> implementing class type in most cases
 */
interface AliasStage<S> {

    S fieldAliases(@NonNull Map<String, Set<String>> aliasFieldsMap);

    default S fieldAliases(@NonNull String alias, @NonNull Set<String> fields) {
        return fieldAliases(ImmutableMap.of(alias, fields));
    }

    default S fieldAliases(@NonNull String alias, String... fields) {
        return fieldAliases(alias, ImmutableSet.copyOf(fields));
    }
}
