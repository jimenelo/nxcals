/*
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.api.extraction.data.builders.fluent;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.function.Function;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class StageSequenceVariables {
    public static <T> Function<QueryData<T>, SystemStage<TimeStartStage<VariableAliasStage<T>, T>, T>> sequence() {
        return data -> new SystemStage<>(new TimeStartStage<>(new VariableAliasStage<>(data)));
    }
}