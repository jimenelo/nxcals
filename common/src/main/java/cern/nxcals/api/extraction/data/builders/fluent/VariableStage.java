package cern.nxcals.api.extraction.data.builders.fluent;

import lombok.NonNull;

import java.util.List;

public class VariableStage<T> extends Stage<VariableStageLoop<T>, T> {
    VariableStage(QueryData<T> data) {
        super(data);
    }

    /**
     * Search for given variable name.
     *
     * @param name variable name
     * @return next query builder stage
     */
    public VariableStageLoop<T> variable(@NonNull String name) {
        data().setVariableKey(name, false);
        return new VariableStageLoop<>(data());
    }

    /**
     * Search for given variable names.
     *
     * @param names list of variable names
     * @return next query builder stage
     */
    public VariableStageLoop<T> variables(@NonNull List<String> names) {
        names.forEach(name -> data().setVariableKey(name, false));
        return new VariableStageLoop<>(data());
    }

    /**
     * Search for variables, which names match to pattern. Patterns should be in Oracle SQL form (% and _ as wildcards).
     *
     * @param pattern variable name pattern
     * @return next query builder stage
     */
    public VariableStageLoop<T> variableLike(@NonNull String pattern) {
        data().setVariableKey(pattern, true);
        return new VariableStageLoop<>(data());
    }

    /**
     * Search for variables, which names match to given patterns. Patterns should be in Oracle SQL form (% and _ as wildcards).
     *
     * @param patterns list of variable name patterns
     * @return next query builder stage
     */
    public VariableStageLoop<T> variablesLike(@NonNull List<String> patterns) {
        patterns.forEach(pattern -> data().setVariableKey(pattern, true));
        return new VariableStageLoop<>(data());
    }
}