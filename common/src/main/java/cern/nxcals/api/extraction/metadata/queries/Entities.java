package cern.nxcals.api.extraction.metadata.queries;

import com.github.rutledgepaulv.qbuilders.properties.concrete.LongProperty;

/**
 * Query builder for Entity meta-data.
 * Build your queries like Entities.suchThat().keyValues().eq("keyValuesWithPattern") etc.
 * Please note that eq() operator also allows for 'like' queries with characters '%' and '_'.
 * You can use regexp with pattern() operator.
 */
public final class Entities extends BaseBuilder<Entities>
        implements EntityFields<
        StringLikeProperty<Entities>,
        LongProperty<Entities>,
        KeyValuesProperty<Entities>
        > {

    private Entities() {
        super();
    }

    public static Entities suchThat() {
        return new Entities();
    }

    @Override
    public LongProperty<Entities> longField(String fieldName) {
        return super.longNum(fieldName);
    }

    @Override
    public StringLikeProperty<Entities> stringLike(String fieldName) {
        return super.stringLike(fieldName);
    }

    @Override
    public KeyValuesProperty<Entities> entityKeyValues(String fieldName) {
        return super.entityKeyValues(fieldName);
    }

    @Override
    public KeyValuesProperty<Entities> partitionKeyValues(String fieldName) {
        return super.partitionKeyValues(fieldName);
    }
}
