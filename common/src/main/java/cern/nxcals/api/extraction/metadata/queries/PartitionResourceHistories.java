package cern.nxcals.api.extraction.metadata.queries;

import com.github.rutledgepaulv.qbuilders.properties.concrete.InstantProperty;
import com.github.rutledgepaulv.qbuilders.properties.concrete.LongProperty;
import com.github.rutledgepaulv.qbuilders.properties.concrete.StringProperty;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class PartitionResourceHistories extends BaseBuilder<PartitionResourceHistories> {
    public static PartitionResourceHistories suchThat() {
        return new PartitionResourceHistories();
    }

    public LongProperty<PartitionResourceHistories> id() {
        return longNum("id");
    }

    public LongProperty<PartitionResourceHistories> partitionResourceId() {
        return longNum("partitionResource.id");
    }

    public StringProperty<PartitionResourceHistories> storageType() {
        return stringLike("storageType");
    }

    public InstantProperty<PartitionResourceHistories> validFromStamp() {
        return instant("validFromStamp");
    }

    public InstantProperty<PartitionResourceHistories> validToStamp() {
        return instant("validToStamp");
    }
}
