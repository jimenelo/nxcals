package cern.nxcals.api.extraction.metadata.queries;

import com.github.rutledgepaulv.qbuilders.builders.QBuilder;
import com.github.rutledgepaulv.qbuilders.conditions.Condition;
import com.github.rutledgepaulv.qbuilders.delegates.virtual.InstantLikePropertyDelegate;
import com.github.rutledgepaulv.qbuilders.structures.FieldPath;

import java.time.Duration;
import java.time.Instant;

@SuppressWarnings("squid:MaximumInheritanceDepth")
public class InstantPropertyDelegate<T extends QBuilder<T>> extends InstantLikePropertyDelegate<T, Instant> {

    public InstantPropertyDelegate(FieldPath field, T canonical) {
        super(field, canonical);
    }

    @Override
    protected Instant normalize(Instant dateTime) {
        return dateTime;
    }

    public Condition<T> notOlderThan(Duration value){
        return after(Instant.from(value.subtractFrom(Instant.now())), false);
    }

}
