package cern.nxcals.api.extraction.data.builders.fluent;

import com.google.common.base.Preconditions;
import lombok.NonNull;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DeviceStage<T> extends Stage<DeviceStageLoop<T>, T> {
    private static final String ILLEGAL_FORMAT_ERROR = "Illegal parameter name, expected <device>/<property> got %s";

    private static final Pattern PARAMETER_PATTERN = Pattern.compile("^([.a-zA-Z0-9_-]+)/([.a-zA-Z0-9_-]+)$");
    private static final Pattern PARAMETER_PATTERN_WITH_WILDCARD = Pattern
            .compile("^([%.a-zA-Z0-9\\\\_-]+)/([%.a-zA-Z0-9\\\\_-]+)$");

    DeviceStage(QueryData<T> data) {
        super(data);
    }

    /**
     * Search for given device. Equivalent to <pre>keyValue("device", device)</pre>
     *
     * @param device device name
     * @return stage to define property in query
     */
    public PropertyStage<T> device(@NonNull String device) {
        data().addDevice(device, false);
        return new PropertyStage<>(data());
    }

    /**
     * Search for given device name pattern.
     * Pattern should be in Oracle SQL form (% and _ as wildcards).
     * Equivalent to <pre>keyValueLike("device", device)</pre>
     *
     * @param pattern device name pattern
     * @return stage to define property in query
     */
    public PropertyStage<T> deviceLike(@NonNull String pattern) {
        data().addDevice(pattern, true);
        return new PropertyStage<>(data());
    }

    /**
     * Search for given parameter. Parameter should be in form "device/property". Equivalent to
     * <pre>keyValues(ImmutableMap.of("device", device, "property", property))</pre>
     *
     * @param parameter parameter in form device/property
     * @return next device query stage
     */
    public DeviceStageLoop<T> parameter(@NonNull String parameter) {
        extractDeviceProperty(parameter, PARAMETER_PATTERN, false);
        return new DeviceStageLoop<>(data());
    }

    /**
     * Search for given parameters. Every parameter should be in form "device/property". Equivalent to
     * <pre>keyValues(ImmutableMap.of("device", device1, "property", property1...))</pre>
     *
     * @param parameters list of parameters in form device/property
     * @return next device query stage
     */
    public DeviceStageLoop<T> parameters(@NonNull List<String> parameters) {
        parameters.forEach(parameter -> extractDeviceProperty(parameter, PARAMETER_PATTERN, false));
        return new DeviceStageLoop<>(data());
    }

    /**
     * Search for given parameters, which match given pattern. Parameter pattern should be in form "device_pattern/property_pattern".
     * Patterns should be in Oracle SQL form (% and _ as wildcards). Equivalent to:
     * <pre>keyValuesLike(ImmutableMap.of("device", device1, "property", property1...))</pre>
     *
     * @param pattern parameter pattern in form device_pattern/property_pattern
     * @return next device query stage
     */
    public DeviceStageLoop<T> parameterLike(@NonNull String pattern) {
        extractDeviceProperty(pattern, PARAMETER_PATTERN_WITH_WILDCARD, true);
        return new DeviceStageLoop<>(data());
    }

    private void extractDeviceProperty(String parameter, Pattern pattern, boolean wildcard) {
        Matcher matcher = pattern.matcher(parameter);
        Preconditions.checkArgument(matcher.matches(), String.format(ILLEGAL_FORMAT_ERROR, parameter));
        data().addDevice(matcher.group(1), wildcard);
        data().addProperty(matcher.group(2), wildcard);
        data().closeEntity();
    }
}