package cern.nxcals.api.extraction.data.builders.fluent.v2;

import cern.nxcals.api.extraction.data.builders.fluent.QueryData;
import cern.nxcals.api.extraction.data.builders.fluent.Stage;
import lombok.NonNull;

public class PropertyStage<T> extends Stage<DeviceStageLoop<T>, T> {
    PropertyStage(QueryData<T> data) {
        super(data);
    }

    /**
     * Specify property in device-property query. Equivalent to keyValues(Map.of("property", property)).
     *
     * @param property property name
     * @return device-property query builder
     */
    public DeviceStageLoop<T> propertyEq(@NonNull String property) {
        data().addProperty(property, false);
        data().closeEntity();
        return new DeviceStageLoop<>(data());
    }

    /**
     * Specify property name pattern in device-property query. Equivalent to keyValuesLike(Map.of("property", property)).
     * Device pattern should be in Oracle SQL form (% and _ as wildcards).
     *
     * @param pattern property name pattern
     * @return device-property query builder
     */
    public DeviceStageLoop<T> propertyLike(@NonNull String pattern) {
        data().addProperty(pattern, true);
        data().closeEntity();
        return new DeviceStageLoop<>(data());
    }
}