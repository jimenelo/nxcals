package cern.nxcals.api.extraction.data.builders.fluent.v2;

import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.utils.TimeUtils;
import lombok.NonNull;

import java.time.Instant;

/**
 * Interface providing some methods to specify time in different formats in query. All formats are converted to Instant
 * and only method accepting them must be implemented
 *
 * @param <N> next stage, which should appear after specifying the time
 */ 
interface TimeStage<N> {

    N timeWindow(@NonNull Instant fromNanos, @NonNull Instant toNanos);

    /**
     * @param fromNanos start time in nanoseconds elapsed since 1970-01-01 00:00:00 in the UTC timezone
     * @param toNanos   end time in nanoseconds elapsed since 1970-01-01 00:00:00 in the UTC timezone
     */
    default N timeWindow(long fromNanos, long toNanos) {
        return timeWindow(TimeUtils.getInstantFromNanos(fromNanos), TimeUtils.getInstantFromNanos(toNanos));
    }

    /**
     * @param fromTimeStampUtc at time in UTC timezone as string using "yyyy-MM-dd HH:mm:ss.n" format
     */
    default N timeWindow(@NonNull String fromTimeStampUtc, @NonNull String toTimeStampUtc) {
        return timeWindow(TimeUtils.getInstantFromString(fromTimeStampUtc),
                TimeUtils.getInstantFromString(toTimeStampUtc));
    }

    default N timeWindow(@NonNull TimeWindow timeWindow) {
        return timeWindow(timeWindow.getStartTime(), timeWindow.getEndTime());
    }

    default N atTime(@NonNull Instant timeStamp) {
        return timeWindow(timeStamp, timeStamp);
    }

    /**
     * @param nanos start time in nanoseconds elapsed since 1970-01-01 00:00:00 in the UTC timezone
     */
    default N atTime(long nanos) {
        return atTime(TimeUtils.getInstantFromNanos(nanos));
    }

    /**
     * @param timeStampUtc at time in UTC timezone as string using "yyyy-MM-dd HH:mm:ss.n" format
     */
    default N atTime(@NonNull String timeStampUtc) {
        return atTime(TimeUtils.getInstantFromString(timeStampUtc));
    }
}
