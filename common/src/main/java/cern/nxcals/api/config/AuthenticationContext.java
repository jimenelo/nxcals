package cern.nxcals.api.config;

import cern.nxcals.api.security.KerberosRelogin;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.AllNestedConditions;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;

import static cern.nxcals.common.Constants.KERBEROS_AUTH_DISABLE;

@Configuration
@Validated
public class AuthenticationContext {

    /**
     * Creates a kerberos token re-loader if the kerberos principal and keytab properties
     * have been specified by the user
     */
    @Bean(name = "kerberos", initMethod = "start")
    @Conditional(KerberosReloginCondition.class)
    public KerberosRelogin kerberosRelogin(@NotBlank @Value("${kerberos.principal}") String principal,
            @NotBlank @Value("${kerberos.keytab}") String keytab,
            @Value("${kerberos.relogin:true}") boolean enableRelogin) {
        return new KerberosRelogin(principal, keytab, enableRelogin);
    }

    static class KerberosReloginCondition extends AllNestedConditions {
        KerberosReloginCondition() {
            super(ConfigurationPhase.REGISTER_BEAN);
        }

        @ConditionalOnProperty({ "kerberos.principal", "kerberos.keytab" })
        static class KerberosKeytabAndPrincipalPresent {
        }

        //should be false (prevent from bean creation) only when "kerberos.auth.disable" property is set to true
        @ConditionalOnExpression("#{not T(java.lang.Boolean).parseBoolean('${" + KERBEROS_AUTH_DISABLE + "}')}")
        static class KerberosAuthNotDisabled {
        }
    }
}
