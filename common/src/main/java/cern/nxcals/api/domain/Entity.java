/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.api.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;

import java.time.Instant;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@JsonDeserialize(builder = Entity.InnerBuilder.class)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class Entity implements Identifiable {
    private final long id;
    @NonNull
    @EqualsAndHashCode.Include
    private final Map<String, Object> entityKeyValues;
    @NonNull
    @EqualsAndHashCode.Include
    private final SystemSpec systemSpec;
    @NonNull
    private final Partition partition;
    private final SortedSet<EntityHistory> entityHistory;
    private final Instant lockedUntilStamp;
    private final long recVersion;
    private final Integer kafkaPartition;

    @JsonIgnore
    public EntityHistory getFirstEntityHistory() {
        return entityHistory.last();
    }

    private static InnerBuilder innerBuilder() {
        return new InnerBuilder();
    }

    @lombok.Builder(builderMethodName = "innerBuilder", builderClassName = "InnerBuilder")
    @SuppressWarnings("squid:S00107") // methods should not have too many parameters
    private static Entity innerBuilder(long id, Map<String, Object> entityKeyValues, SystemSpec systemSpec,
            Partition partition, SortedSet<EntityHistory> entityHistory, Instant lockedUntilStamp, long recVersion,
            Integer kafkaPartition) {
        return new Entity(id, immutable(entityKeyValues), systemSpec, partition, immutable(entityHistory),
                lockedUntilStamp, recVersion, kafkaPartition);
    }

    @lombok.Builder(builderMethodName = "builder", builderClassName = "Builder")
    private static Entity userBuilder(Map<String, Object> entityKeyValues, SystemSpec systemSpec, Partition partition, SortedSet<EntityHistory> entityHistory, Instant lockedUntilStamp) {
        return new Entity(Identifiable.NOT_SET, immutable(entityKeyValues), systemSpec, partition,
                immutable(entityHistory), lockedUntilStamp, Versionable.NOT_SET, null);
    }

    public Entity.Builder toBuilder() {
        return Entity.innerBuilder()
                .id(id)
                .entityKeyValues(entityKeyValues)
                .systemSpec(systemSpec)
                .partition(partition)
                .entityHistory(entityHistory)
                .lockedUntilStamp(lockedUntilStamp)
                .kafkaPartition(kafkaPartition)
                .recVersion(recVersion);
    }

    private static <S> SortedSet<S> immutable(SortedSet<S> in) {
        if (in == null) {
            return Collections.emptySortedSet();
        } else {
            return Collections.unmodifiableSortedSet(new TreeSet<>(in));
        }
    }

    private static <K, V> Map<K, V> immutable(Map<K, V> in) {
        return Collections.unmodifiableMap(new HashMap<>(in));
    }

    public static class Builder {
        public Builder unlock() {
            lockedUntilStamp = null;
            return this;
        }
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static class InnerBuilder extends Entity.Builder {
        @Override
        public InnerBuilder unlock() {
            lockedUntilStamp = null;
            return this;
        }
    }
}
