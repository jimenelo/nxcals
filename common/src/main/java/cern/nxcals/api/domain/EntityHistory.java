package cern.nxcals.api.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;

import java.util.Comparator;

@Data
@JsonDeserialize(builder = EntityHistory.InnerBuilder.class)
@EqualsAndHashCode()
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class EntityHistory implements Comparable<EntityHistory>, Identifiable {
    @EqualsAndHashCode.Exclude
    private final long id;
    @NonNull
    private final EntitySchema entitySchema;
    @NonNull
    private final Partition partition;
    @NonNull
    private final TimeWindow validity;
    @NonNull
    private final Entity entity;

    @Override
    public int compareTo(EntityHistory o) {
        return Comparator.<Long>reverseOrder().compare(validity.getStartTimeNanos(), o.validity.getStartTimeNanos());
    }

    private static InnerBuilder innerBuilder() {
        return new InnerBuilder();
    }

    @lombok.Builder(builderMethodName = "innerBuilder", builderClassName = "InnerBuilder")
    private static EntityHistory innerBuilder(long id, EntitySchema entitySchema, Partition partition, TimeWindow validity, Entity entity) {
        return new EntityHistory(id, entitySchema, partition, validity, entity);
    }

    @lombok.Builder(builderMethodName = "builder", builderClassName = "Builder")
    private static EntityHistory userBuilder(EntitySchema entitySchema, Partition partition, TimeWindow validity, Entity entity) {
        return new EntityHistory(Identifiable.NOT_SET, entitySchema, partition, validity, entity);
    }

    public Builder toBuilder() {
        return EntityHistory.innerBuilder()
                .id(id)
                .entitySchema(entitySchema)
                .partition(partition)
                .validity(validity)
                .entity(entity);
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static class InnerBuilder extends Builder { }
}
