package cern.nxcals.api.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum OperationType {
    INSERT("I"),
    UPDATE("U"),
    DELETE("D");

    private String shortName;

    public static OperationType fromShortName(String shortName) {
        switch (shortName) {
        case "I":
            return OperationType.INSERT;

        case "U":
            return OperationType.UPDATE;

        case "D":
            return OperationType.DELETE;

        default:
            throw new IllegalArgumentException("Operation Type [" + shortName
                    + "] not supported.");
        }
    }
}
