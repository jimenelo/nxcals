package cern.nxcals.api.domain;

import cern.nxcals.common.utils.Lazy;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.ToString;
import org.apache.avro.Schema;

@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@JsonDeserialize(builder = EntitySchema.InnerBuilder.class)
@ToString(onlyExplicitlyIncluded = true)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class EntitySchema implements Identifiable {
    private final long id;

    @ToString.Include
    @EqualsAndHashCode.Include
    @NonNull
    private final String schemaJson;

    private final Lazy<Schema> schema;

    @JsonIgnore
    public Schema getSchema() {
        return schema.get();
    }

    private static EntitySchema.InnerBuilder innerBuilder() {
        return new EntitySchema.InnerBuilder();
    }

    @lombok.Builder(builderMethodName = "innerBuilder", builderClassName = "InnerBuilder")
    private static EntitySchema innerBuilder(long id, String schemaJson) {
        return new EntitySchema(id, schemaJson, schemaLazy(schemaJson));
    }

    @lombok.Builder(builderMethodName = "builder", builderClassName = "Builder")
    private static EntitySchema userBuilder(String schemaJson) {
        return new EntitySchema(Identifiable.NOT_SET, schemaJson, schemaLazy(schemaJson));
    }

    private static Lazy<Schema> schemaLazy(String schemaJson) {
        return new Lazy<>(() -> new Schema.Parser().parse(schemaJson));
    }

    public EntitySchema.Builder toBuilder() {
        return EntitySchema.innerBuilder().id(id).schemaJson(schemaJson);
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static class InnerBuilder extends Builder { }
}
