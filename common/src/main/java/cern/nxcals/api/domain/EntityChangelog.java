package cern.nxcals.api.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.Instant;
import java.util.Map;

@Data
@JsonDeserialize(builder = EntityChangelog.InnerBuilder.class)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class EntityChangelog implements Identifiable {

    private final long id;
    private long entityId;
    private Map<String, Object> oldKeyValues;
    private Map<String, Object> newKeyValues;
    private Long oldPartitionId;
    private Long newPartitionId;
    private Long oldSystemId;
    private Long newSystemId;
    private Instant newLockedUntilStamp;
    private Instant oldLockedUntilStamp;
    private Instant createTimeUtc;
    private OperationType opType;
    private String clientInfo;

    private static InnerBuilder innerBuilder() {
        return new InnerBuilder();
    }

    @SuppressWarnings("squid:S00107") // too many parameters
    @lombok.Builder(builderMethodName = "innerBuilder", builderClassName = "InnerBuilder")
    private static EntityChangelog innerBuilder(long id, long entityId, Map<String, Object> oldKeyValues,
            Map<String, Object> newKeyValues, Long oldPartitionId, Long newPartitionId, Long oldSystemId,
            Long newSystemId, Instant newLockedUntilStamp, Instant oldLockedUntilStamp, Instant createTimeUtc,
            OperationType opType, String clientInfo) {
        return new EntityChangelog(id, entityId, oldKeyValues, newKeyValues, oldPartitionId, newPartitionId,
                oldSystemId,
                newSystemId, newLockedUntilStamp, oldLockedUntilStamp, createTimeUtc, opType, clientInfo);
    }

    @SuppressWarnings("squid:S00107") // too many parameters
    @lombok.Builder(builderMethodName = "builder", builderClassName = "Builder")
    private static EntityChangelog userBuilder(long entityId, Map<String, Object> oldKeyValues,
            Map<String, Object> newKeyValues, Long oldPartitionId, Long newPartitionId, Long oldSystemId,
            Long newSystemId, Instant newLockedUntilStamp, Instant oldLockedUntilStamp, Instant createTimeUtc,
            OperationType opType, String clientInfo) {
        return new EntityChangelog(Identifiable.NOT_SET, entityId, oldKeyValues, newKeyValues, oldPartitionId,
                newPartitionId, oldSystemId,
                newSystemId, newLockedUntilStamp, oldLockedUntilStamp, createTimeUtc, opType, clientInfo);
    }

    public Builder toBuilder() {
        return EntityChangelog.innerBuilder()
                .id(id)
                .entityId(entityId)
                .oldKeyValues(oldKeyValues)
                .newKeyValues(newKeyValues)
                .oldPartitionId(oldPartitionId)
                .newPartitionId(newPartitionId)
                .oldSystemId(oldSystemId)
                .newSystemId(newSystemId)
                .oldLockedUntilStamp(oldLockedUntilStamp)
                .newLockedUntilStamp(newLockedUntilStamp)
                .createTimeUtc(createTimeUtc)
                .opType(opType)
                .clientInfo(clientInfo);
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static class InnerBuilder extends Builder {
    }
}
