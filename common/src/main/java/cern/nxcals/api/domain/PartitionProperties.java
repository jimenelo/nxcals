package cern.nxcals.api.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Builder;
import lombok.Data;

@Data
@JsonDeserialize
@Builder(builderClassName = "Builder")
public class PartitionProperties {

    private final boolean updatable;

    @JsonCreator
    public PartitionProperties(@JsonProperty("updatable") boolean updatable) {
        this.updatable = updatable;
    }
}