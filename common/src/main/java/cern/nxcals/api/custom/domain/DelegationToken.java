/**
 * Copyright (c) 2021 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.api.custom.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
// Jackson expects the "with" prefix on builder setter methods.
@Builder(setterPrefix = "with")
@JsonDeserialize(builder = DelegationToken.DelegationTokenBuilder.class)
public class DelegationToken {

    @NonNull
    private final byte[] storage; 

}
