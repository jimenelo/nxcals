package cern.nxcals.api.custom.extraction.metadata.queries;

import cern.nxcals.api.custom.domain.GroupType;
import cern.nxcals.api.domain.Visibility;
import cern.nxcals.api.extraction.metadata.queries.BaseBuilder;
import cern.nxcals.api.extraction.metadata.queries.KeyValuesProperty;
import cern.nxcals.api.extraction.metadata.queries.StringLikeProperty;
import com.github.rutledgepaulv.qbuilders.properties.concrete.EnumProperty;
import com.github.rutledgepaulv.qbuilders.properties.concrete.LongProperty;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Tags  extends BaseBuilder<Tags> {
    
    public static Tags suchThat() {
        return new Tags().stringLike("label").eq(GroupType.TAG.toString()).and();
    }

    public LongProperty<Tags> id() {
        return longNum("id");
    }

    public LongProperty<Tags> systemId() {
        return longNum("system.id");
    }


    public EnumProperty<Tags, Visibility> visibility() {
        return enumeration("visibility");
    }

    public StringLikeProperty<Tags> systemName() {
        return stringLike("system.name");
    }

    public StringLikeProperty<Tags> name() {
        return stringLike("name");
    }

    public StringLikeProperty<Tags> description() {
        return stringLike("description");
    }

    public StringLikeProperty<Tags> variableName() {
        return stringLike("variables.variable.variableName");
    }

    public KeyValuesProperty<Tags> entityKeyValues() {
        return entityKeyValues("entities.entity.keyValues");
    }


    public StringLikeProperty<Tags> ownerName() {
        return stringLike("owner");
    }
}

