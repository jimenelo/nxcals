package cern.nxcals.api.custom.domain.constants;

import java.io.Serializable;

/**
 * The Enum ScaleAlgorithm is used to retrieve scale algorithms for time scaling operations
 */
public enum ScaleAlgorithm implements Serializable {
    AVG, MIN, MAX, REPEAT, INTERPOLATE, SUM, COUNT;
}
