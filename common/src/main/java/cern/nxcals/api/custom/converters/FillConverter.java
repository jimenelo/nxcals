package cern.nxcals.api.custom.converters;

import cern.nxcals.api.custom.domain.BeamMode;
import cern.nxcals.api.custom.domain.Fill;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.extraction.thin.FillData;
import lombok.experimental.UtilityClass;

import java.util.List;

import static java.util.stream.Collectors.toList;

@UtilityClass
public class FillConverter {

    public static Fill toFill(FillData fillData) {

        List<BeamMode> beamModeList = fillData.getBeamModesList().stream().map(FillConverter::toBeamMode).collect(toList());

        return new Fill(fillData.getNumber(), TimeWindow.between(
                fillData.getValidity().getStartTime(),
                fillData.getValidity().getEndTime()), beamModeList);
    }

    public static List<Fill> toFillList(List<FillData> fillDataStream) {
        return fillDataStream.stream().map(FillConverter::toFill).collect(toList());
    }


    public static FillData toFillData(Fill fill) {
        TimeWindow validity = fill.getValidity();

        List<cern.nxcals.api.extraction.thin.BeamMode> beamModes = fill.getBeamModes().stream()
                .map(FillConverter::fromBeamMode).collect(toList());

        return FillData.newBuilder()
                .addAllBeamModes(beamModes)
                .setNumber(fill.getNumber())
                .setValidity(cern.nxcals.api.extraction.thin.TimeWindow.newBuilder()
                        .setStartTime(validity.getStartTimeNanos())
                        .setEndTime(validity.getEndTimeNanos())
                ).build();
    }

    public static List<FillData> toFillsData(List<Fill> fills) {
        return fills.stream().map(FillConverter::toFillData).collect(toList());
    }

    private static BeamMode toBeamMode(cern.nxcals.api.extraction.thin.BeamMode bmode) {
        return new BeamMode(TimeWindow.between(
                bmode.getValidity().getStartTime(),
                bmode.getValidity().getEndTime()), bmode.getBeamModeValue());
    }

    private static cern.nxcals.api.extraction.thin.BeamMode fromBeamMode(BeamMode bmode) {
        return cern.nxcals.api.extraction.thin.BeamMode.newBuilder()
                .setValidity(cern.nxcals.api.extraction.thin.TimeWindow.newBuilder()
                        .setStartTime(bmode.getValidity().getStartTimeNanos())
                        .setEndTime(bmode.getValidity().getEndTimeNanos())
                        .build())
                .setBeamModeValue(bmode.getBeamModeValue())
                .build();
    }
}
