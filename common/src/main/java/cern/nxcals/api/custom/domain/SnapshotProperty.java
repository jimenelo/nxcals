package cern.nxcals.api.custom.domain;

import cern.nxcals.api.utils.TimeUtils;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.Instant;
import java.util.function.Function;

@Getter
@AllArgsConstructor
@SuppressWarnings("squid:S00115") // incorrect name format
public enum SnapshotProperty {
    BEAM_MODE_END("beamModeEnd", SnapshotProperty::safeToString),
    BEAM_MODE_START("beamModeStart", SnapshotProperty::safeToString),
    FILL_NUMBER("fillNumber", SnapshotProperty::safeToString),
    DYNAMIC_DURATION("getDynamicDuration", SnapshotProperty::safeToString),
    END_TIME("getEndTime", SnapshotProperty::instantToString),
    PRIOR_TIME("getPriorTime", SnapshotProperty::safeToString),
    START_TIME("getStartTime", SnapshotProperty::instantToString),
    TIME("getTime", SnapshotProperty::safeToString),
    TIME_ZONE("getTimeZone", SnapshotProperty::safeToString),
    IS_END_TIME_DYNAMIC("isEndTimeDynamic", SnapshotProperty::safeToString);

    private final String value;

    private final Function<Object, String> serializer;

    public String valueToJson(Object value) {
        return serializer.apply(value);
    }

    private static String safeToString(Object obj) {
        return obj == null ? null : obj.toString();
    }

    @SuppressWarnings("java:S1874") // deprecated method
    private static String instantToString(Object obj) {
        return obj == null ? null : TimeUtils.getStringFromInstant((Instant) obj);
    }
}

