package cern.nxcals.api.extraction.thin.data.builders;

import org.junit.jupiter.api.Test;

import java.time.Instant;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertTrue;

class DevicePropertyDataQueryTest {
    @Test
    void shouldBuild() {
        //given
        //when
        String script = DevicePropertyDataQuery.builder().system("CMW")
                .startTime(Instant.now())
                .endTime(Instant.now())
                .fieldAlias("alias", "field")
                .fieldAlias("aliasOther", "fieldOther")
                .entity()
                .parameter("dev1/param1")
                .entity()
                .device("dev2")
                .propertyLike("prop2")
                .build();

        //then
        assertAll(
                () -> assertTrue(script.contains(
                        ".fieldAliases({\"alias\":Set.of(\"field\"),\"aliasOther\":Set.of(\"fieldOther\")})")),
                () -> assertTrue(script.contains("keyValuesEq({\"property\":\"param1\",\"device\":\"dev1\"})")),
                () -> assertTrue(script.contains("keyValuesLike({\"property\":\"prop2\",\"device\":\"dev2\"})"))
        );
    }
}
