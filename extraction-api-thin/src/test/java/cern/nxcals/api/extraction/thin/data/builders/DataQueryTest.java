package cern.nxcals.api.extraction.thin.data.builders;

import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class DataQueryTest {
    private final static String KEY = "key";
    private final static String VALUE = "value";
    private final static String SYSTEM = "CMW";
    private final static String VARIABLE_NAME = "var";
    private final static String VARIABLE_NAME_PATTERN = "var2";
    private final static String ALIAS = "alias";
    private final static String ALIAS1 = "alias1";
    private final static String ALIAS2 = "alias2";
    private final static String FIELD1 = "field1";
    private final static String FIELD2 = "field2";
    private final static String FIELD3 = "field3";
    private final static long ID1 = 1;
    private final static long ID2 = 2;

    private final Map<String, Object> keyValue = Map.of(KEY, VALUE);

    @Test
    void byVariables() {
        //given
        //when
        String script = DataQuery.builder().byVariables().system(SYSTEM)
                .atTime(Instant.now())
                .variable(VARIABLE_NAME)
                .variableLike(VARIABLE_NAME_PATTERN)
                .build();
        //then
        checkSimpleVariableQuery(script);
    }

    @Test
    void variables() {
        //given
        //when
        String script = DataQuery.builder().variables()
                .system(SYSTEM)
                .nameEq(VARIABLE_NAME)
                .nameLike(VARIABLE_NAME_PATTERN)
                .atTime(Instant.now())
                .build();
        //then
        checkSimpleVariableQuery(script);
    }

    private void checkSimpleVariableQuery(String script) {
        assertAll(
                () -> assertTrue(script.contains("variables()")),
                () -> assertTrue(script.contains(
                        String.format(".nameEq(\"%s\").nameLike(\"%s\")", VARIABLE_NAME, VARIABLE_NAME_PATTERN)))
        );
    }

    @Test
    void variablesById() {
        //given
        //when
        String script = DataQuery.builder().variables().system(SYSTEM)
                .idEq(ID1).idEq(ID2)
                .atTime(Instant.now())
                .build();
        //then
        assertAll(
                () -> assertTrue(script.contains("variables()")),
                () -> assertTrue(script.contains(String.format("idIn(%s,%s)", ID1, ID2)))
        );
    }

    @Test
    void byVariablesWithAliases() {
        //given
        Map<String, List<String>> extraAliases = new HashMap<>();
        extraAliases.put(ALIAS1, Arrays.asList(FIELD1, FIELD2));
        extraAliases.put(ALIAS2, List.of(FIELD3));
        //when
        String script = DataQuery.builder().byVariables().system(SYSTEM)
                .atTime(Instant.now())
                .fieldAliases(ALIAS, Arrays.asList(FIELD1, FIELD2))
                .fieldAliases(extraAliases)
                .variable(VARIABLE_NAME)
                .build();
        //then
        checkVariableWithAliasesQuery(script);
    }

    @Test
    void variablesWithAliases() {
        //given
        Map<String, Set<String>> extraAliases = new HashMap<>();
        extraAliases.put(ALIAS1, new LinkedHashSet(List.of(FIELD1, FIELD2)));
        extraAliases.put(ALIAS2, Set.of(FIELD3));
        //when
        String script = DataQuery.builder().variables().system(SYSTEM)
                .nameEq(VARIABLE_NAME)
                .atTime(Instant.now())
                .fieldAliases(ALIAS, new LinkedHashSet<>(List.of(FIELD1, FIELD2)))
                .fieldAliases(extraAliases)
                .build();
        //then
        checkVariableWithAliasesQuery(script);
    }

    private void checkVariableWithAliasesQuery(String script) {
        assertTrue(script.contains(String.format(
                "fieldAliases({\"%s\":Set.of(\"%s\",\"%s\"),\"%s\":Set.of(\"%s\",\"%s\"),\"%s\":Set.of(\"%s\")})",
                ALIAS, FIELD1, FIELD2, ALIAS1, FIELD1, FIELD2, ALIAS2, FIELD3)));
    }

    @Test
    void byVariablesWithEmptyAliasesShouldContainNoAliases() {
        //given
        Map<String, List<String>> emptyAliases = Collections.emptyMap();
        //when
        String script = DataQuery.builder().byVariables().system(SYSTEM)
                .atTime(Instant.now())
                .fieldAliases(emptyAliases)
                .variable(VARIABLE_NAME)
                .build();
        //then
        assertFalse(script.contains("fieldAlias"));
    }

    @Test
    void variablesWithEmptyAliasesShouldContainNoAliases() {
        //given
        Map<String, Set<String>> emptyAliases = Collections.emptyMap();
        //when
        String script = DataQuery.builder().variables().system(SYSTEM)
                .nameEq(VARIABLE_NAME)
                .atTime(Instant.now())
                .fieldAliases(emptyAliases)
                .build();
        //then
        assertFalse(script.contains("fieldAlias"));
    }

    @Test
    void byEntities() {
        //given
        //when
        String script = DataQuery.builder().byEntities().system(SYSTEM)
                .atTime(Instant.now())
                .entity()
                .keyValue(KEY, VALUE).build();
        //then
        checkEntitiesQuery(script);
    }

    @Test
    void entities() {
        //given
        //when
        String script = DataQuery.builder().entities().system(SYSTEM)
                .keyValuesEq(keyValue)
                .atTime(Instant.now())
                .build();
        //then
        checkEntitiesQuery(script);
    }

    private void checkEntitiesQuery(String script) {
        assertAll(
                () -> assertTrue(script.contains("entities()")),
                () -> assertTrue(script.contains(String.format("keyValuesEq({\"%s\":\"%s\"})", KEY, VALUE)))
        );
    }

    @Test
    void entitiesById() {
        //given
        //when
        String script = DataQuery.builder().entities().system(SYSTEM)
                .idEq(ID1).idEq(ID2)
                .atTime(Instant.now())
                .build();
        //then
        assertAll(
                () -> assertTrue(script.contains("entities()")),
                () -> assertTrue(script.contains(String.format(".idIn(%s,%s)", ID1, ID2)))
        );
    }

    @Test
    void byEntitiesWithAliases() {
        //given
        //when
        String script = DataQuery.builder().byEntities().system(SYSTEM)
                .atTime(Instant.now())
                .fieldAliases(ALIAS, Arrays.asList(FIELD1, FIELD2))
                .entity()
                .keyValue(KEY, VALUE).build();
        //then
        checkEntitiesWithAliasesQuery(script);
    }

    @Test
    void entitiesWithAliases() {
        //when
        String script = DataQuery.builder().entities().system(SYSTEM)
                .keyValuesEq(keyValue)
                .atTime(Instant.now())
                .fieldAliases(ALIAS, new LinkedHashSet<>(List.of(FIELD1, FIELD2)))
                .build();
        //then
        checkEntitiesWithAliasesQuery(script);
    }

    private void checkEntitiesWithAliasesQuery(String script) {
        assertAll(
                () -> assertTrue(script.contains(
                        String.format("fieldAliases({\"%s\":Set.of(\"%s\",\"%s\")})", ALIAS, FIELD1, FIELD2))),
                () -> assertTrue(script.contains(String.format("keyValuesEq({\"%s\":\"%s\"})", KEY, VALUE)))
        );
    }
}
