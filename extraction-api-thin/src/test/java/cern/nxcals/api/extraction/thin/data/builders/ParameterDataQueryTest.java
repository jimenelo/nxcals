package cern.nxcals.api.extraction.thin.data.builders;

import org.junit.jupiter.api.Test;

import java.time.Instant;

import static cern.nxcals.api.extraction.data.builders.fluent.QueryData.DEVICE_KEY;
import static cern.nxcals.api.extraction.data.builders.fluent.QueryData.PROPERTY_KEY;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ParameterDataQueryTest {
    @Test
    void shouldBuild() {
        //given
        final String dev1 = "dev1";
        final String prop1 = "prop1";
        final String dev2 = "dev2";
        final String prop2 = "prop2";
        //when
        String script = ParameterDataQuery.builder().system("CMW")
                .deviceEq(dev1).propertyEq(prop1)
                .parameterLike(String.format("%s/%s", dev2, prop2))
                .atTime(Instant.now())
                .fieldAliases("alias", "field")
                .fieldAliases("aliasOther", "fieldOther")
                .build();

        //then
        assertAll(
                () -> assertTrue(script.contains(".entities()")),
                () -> assertTrue(script.contains(".system(\"CMW\")")),
                () -> assertTrue(script.contains(
                        ".fieldAliases({\"alias\":Set.of(\"field\"),\"aliasOther\":Set.of(\"fieldOther\")})")),
                () -> assertTrue(script.contains(
                        String.format("keyValuesEq({\"%s\":\"%s\",\"%s\":\"%s\"})", PROPERTY_KEY, prop1, DEVICE_KEY,
                                dev1))),
                () -> assertTrue(script.contains(
                        String.format("keyValuesLike({\"%s\":\"%s\",\"%s\":\"%s\"})", PROPERTY_KEY, prop2, DEVICE_KEY,
                                dev2)))
        );
    }
}
