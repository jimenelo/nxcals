package cern.nxcals.api.extraction.thin;

import cern.nxcals.api.extraction.thin.ExtractionServiceGrpc.ExtractionServiceBlockingStub;
import cern.nxcals.common.grpc.MultiAddressNameResolverFactory;
import cern.nxcals.common.grpc.RbacTokenInterceptor;
import cern.rbac.common.RbaToken;
import cern.rbac.util.lookup.RbaTokenLookup;
import com.google.common.annotations.VisibleForTesting;
import io.grpc.ManagedChannel;
import io.grpc.NameResolverRegistry;
import io.grpc.netty.shaded.io.grpc.netty.GrpcSslContexts;
import io.grpc.netty.shaded.io.grpc.netty.NettyChannelBuilder;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;

import javax.net.ssl.SSLException;
import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.net.URL;

@Slf4j
@UtilityClass
public class ServiceFactory {
    private final String CERN_CA_CRT = "/CERN_CA.crt";
    public final String LOAD_BALANCING_POLICY = "round_robin";
    public final int DEFAULT_MAX_MESSAGE_SIZE = 500_000_000;
    public final int DEFAULT_MAX_METADATA_SIZE = 100_000;
    public final int DEFAULT_MAX_RETRY = 5;
    public final String CERN_CERT_URL = "https://cafiles.cern.ch/cafiles/certificates/CERN%20Grid%20Certification%20Authority(1).crt";

    public InputStream getDefaultSSLCertificate() {
        try {
            // This is to make sure this certificate is always valid.
            // Otherwise it can expire (for old jars) and the calls with embedded cert might fail.
            URL url = new URL(CERN_CERT_URL);
            return url.openConnection().getInputStream();
        } catch (IOException ex) {
            log.trace("Cannot load URL {}, trying jar-embedded certificate", CERN_CERT_URL, ex);
            return ServiceFactory.class.getResourceAsStream(CERN_CA_CRT);
        }
    }

    public ExtractionServiceBlockingStub createExtractionService(String address) {
        try (InputStream cert = getDefaultSSLCertificate()) {
            return ExtractionServiceGrpc
                    .newBlockingStub(buildChannel(address, cert));
        } catch (IOException ex) {
            throw new UncheckedIOException(ex);
        }
    }

    public FillServiceGrpc.FillServiceBlockingStub createFillService(String address) {
        try (InputStream cert = getDefaultSSLCertificate()) {
            return FillServiceGrpc
                    .newBlockingStub(buildChannel(address, cert));
        } catch (IOException ex) {
            throw new UncheckedIOException(ex);
        }
    }

    private ManagedChannel buildChannel(String address, InputStream sslCertificate) throws SSLException {
        registerMultiAddressResolver();
        NettyChannelBuilder channelBuilder = NettyChannelBuilder
                .forTarget(MultiAddressNameResolverFactory.SCHEME + "://" + address)
                .useTransportSecurity();
        return channelBuilder
                .sslContext(GrpcSslContexts.forClient().trustManager(sslCertificate).build())
                .defaultLoadBalancingPolicy(LOAD_BALANCING_POLICY)
                .intercept(RbacTokenInterceptor.builder().token(ServiceFactory::getToken).build())
                .maxInboundMessageSize(ServiceFactory.DEFAULT_MAX_MESSAGE_SIZE)
                .maxInboundMetadataSize(ServiceFactory.DEFAULT_MAX_METADATA_SIZE)
                .enableRetry()
                .maxRetryAttempts(ServiceFactory.DEFAULT_MAX_RETRY)
                .build();
    }

    private void registerMultiAddressResolver() {
        NameResolverRegistry registry = NameResolverRegistry.getDefaultRegistry();
        registry.register(new MultiAddressNameResolverFactory());
    }

    @VisibleForTesting
    byte[] getToken() {
        RbaToken rbaToken = RbaTokenLookup.findRbaToken();
        if (rbaToken != null) {
            return rbaToken.getEncoded();
        } else {
            throw new IllegalArgumentException(
                    "No RBAC token found, it is required to authenticate the call on the Spark Server side");
        }
    }
}
