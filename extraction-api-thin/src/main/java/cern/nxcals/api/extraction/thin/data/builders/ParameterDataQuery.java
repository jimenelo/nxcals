package cern.nxcals.api.extraction.thin.data.builders;

import cern.nxcals.api.extraction.data.builders.fluent.QueryData;
import cern.nxcals.api.extraction.data.builders.fluent.v2.DeviceStage;
import cern.nxcals.api.extraction.data.builders.fluent.v2.StageSequenceDeviceProperty;
import cern.nxcals.api.extraction.data.builders.fluent.v2.SystemStage;
import lombok.experimental.UtilityClass;

@UtilityClass
public class ParameterDataQuery {
    public SystemStage<DeviceStage<String>, String> builder() {
        return StageSequenceDeviceProperty.<String>sequence().apply(new QueryData<>(new ScriptProducer()));
    }
}
