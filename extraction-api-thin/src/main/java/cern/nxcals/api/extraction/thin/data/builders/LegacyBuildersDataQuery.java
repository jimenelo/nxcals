package cern.nxcals.api.extraction.thin.data.builders;

import cern.nxcals.api.extraction.data.builders.fluent.EntityAliasStage;
import cern.nxcals.api.extraction.data.builders.fluent.KeyValueStage;
import cern.nxcals.api.extraction.data.builders.fluent.QueryData;
import cern.nxcals.api.extraction.data.builders.fluent.StageSequenceEntities;
import cern.nxcals.api.extraction.data.builders.fluent.StageSequenceVariables;
import cern.nxcals.api.extraction.data.builders.fluent.SystemStage;
import cern.nxcals.api.extraction.data.builders.fluent.TimeStartStage;
import cern.nxcals.api.extraction.data.builders.fluent.VariableAliasStage;
import cern.nxcals.api.extraction.data.builders.fluent.v2.AbstractDataQuery;

@SuppressWarnings("java:S1610")
abstract class LegacyBuildersDataQuery extends AbstractDataQuery<String> {
    /**
     * @return query builder
     */
    // * @deprecated in favour of {@link DataQuery#variables()}
    //    @Deprecated
    public SystemStage<TimeStartStage<VariableAliasStage<String>, String>, String> byVariables() {
        return StageSequenceVariables.<String>sequence().apply(new QueryData<>(new ScriptProducer()));
    }

    /**
     * @return query builder
     */
    //* @deprecated in favour of {@link DataQuery#entities()} ()}
    //    @Deprecated
    public SystemStage<TimeStartStage<EntityAliasStage<KeyValueStage<String>, String>, String>, String> byEntities() {
        return StageSequenceEntities.<String>sequence().apply(new QueryData<>(new ScriptProducer()));
    }
}
