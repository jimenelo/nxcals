package cern.nxcals.api.extraction.thin.data.builders;

import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.extraction.data.builders.fluent.QueryData;
import cern.nxcals.common.domain.EntityKeyValues;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Joiner;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Creates an executable script that can be sent to the Thin Api server for execution.
 */
@RequiredArgsConstructor
public class ScriptProducer implements Function<QueryData<String>, String> {
    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.nnnnnnnnn")
            .withZone(ZoneId.of("UTC"));
    @VisibleForTesting
    static final String TIME_METHOD = ".timeWindow(TimeUtils.getInstantFromString(\"%s\"),TimeUtils.getInstantFromString(\"%s\"))";

    @Override
    public String apply(@NonNull QueryData<String> queryData) {
        String queryType = queryData.isVariableSearch() ? ".variables()" : ".entities()";
        String entriesStr = queryData.isVariableSearch() ?
                variablesStr(queryData.getVariables()) + idsStr(queryData.getVariableIds()) :
                entitiesStr(queryData.getEntities()) + idsStr(queryData.getEntityIds());

        return String.format("DataQuery.builder(sparkSession)\n"
                        + "%s.system(\"%s\")\n"
                        + "        %s\n"
                        + "        %s\n"
                        + "        %s.build()",
                queryType,
                queryData.getSystem(),
                entriesStr,
                timeStr(queryData.getTimeWindow()),
                fieldAliasesStr(queryData.getAliasFields()));
    }

    @VisibleForTesting
    String idsStr(Set<Long> ids) {
        return ids.isEmpty() ? "" : String.format(".idIn(%s)",
                Joiner.on(",").join(ids.stream().sorted().collect(Collectors.toList()))
        );
    }

    @VisibleForTesting
    String variablesStr(Map<String, Boolean> variables) {
        StringBuilder variablesInvoking = new StringBuilder();
        variables.forEach((key, value) ->
                variablesInvoking.append(value ? ".nameLike(\"" : ".nameEq(\"").append(key).append("\")"));
        return variablesInvoking.toString();
    }

    @VisibleForTesting
    String entitiesStr(List<EntityKeyValues> entities) {
        StringBuilder entitiesInvoking = new StringBuilder();
        entities.forEach(entityKeyValues -> entitiesInvoking
                .append(entityKeyValues.isAnyWildcard() ? ".keyValuesLike(" : ".keyValuesEq(")
                .append(toJS(entityKeyValues.getAsMap()))
                .append(")\n"));
        return entitiesInvoking.toString();
    }

    @VisibleForTesting
    String fieldAliasesStr(Map<String, Set<String>> fieldAliases) {
        if (fieldAliases.isEmpty())
            return "";

        return String.format(".fieldAliases(%s)", toJS(fieldAliases));
    }

    @VisibleForTesting
    String toJS(Object object) {
        if (object instanceof Set) {
            return toJSSet((Set<Object>) object);
        }
        if (object instanceof String) {
            return String.format("\"%s\"", (String) object);
        }
        if (object instanceof Map) {
            return toJSMap((Map<Object, Object>) object);
        }
        return object.toString();
    }

    private String toJSMap(Map<Object, Object> map) {
        String content = Joiner.on(",").withKeyValueSeparator(":")
                .join(map.entrySet().stream().collect(Collectors.toMap(
                        entry -> toJS(entry.getKey()),
                        entry -> toJS(entry.getValue())
                )));
        return String.format("{%s}", content);
    }

    private String toJSSet(Set<Object> set) {
        List<String> setElementsAsJSStrings = set.stream().map(this::toJS)
                .collect(Collectors.toList());
        String setElements = Joiner.on(",").join(setElementsAsJSStrings);
        return String.format("Set.of(%s)", setElements);
    }

    @VisibleForTesting
    String timeStr(TimeWindow timeWindow) {
        return String.format(
                TIME_METHOD, FORMATTER.format(timeWindow.getStartTime()), FORMATTER.format(timeWindow.getEndTime()));
    }
}
