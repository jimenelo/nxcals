#!/usr/bin/env bash
set -euo pipefail
#useful for bash debug, please leave commented out
#set -x
source test_utils.sh

ANSIBLE=1
REMOVE=1
TESTS=1
TESTS_STATUS=0
ANSIBLE_PARAMS_AUX=
GRADLE_PARAMS="-DNXCALS_RBAC_AUTH=true"
REPORT_FILE="jmh/reports/result.json"

while [[ "$#" -gt 0 ]]; do
	case "$1" in
		-a|--ansible) ANSIBLE_PARAMS_AUX="$ANSIBLE_PARAMS_AUX $2"; shift ;;
		-g|--gradle) GRADLE_PARAMS="$GRADLE_PARAMS $2"; shift ;;
		-st|--skip-tests) TESTS=0 ;;
		-sa|--skip-ansible) ANSIBLE=0 ;;
		-sr|--skip-remove) REMOVE=0  ;;
		-r|--report-file) REPORT_FILE="$2"; shift;;
		-h|--help)
		<<-EOF cat
		-----------------------------------------
		Usage: performance-tests.sh <options>
		-----------------------------------------
		Options:
		-a|--ansible <some ansible params> - additional paramenters passed to Ansible
		-g|--gradle <some gradle params> - additional parameters passed to Gradle
		-sa|--skip-ansible - does not run ansible to generate application.yml files
		-st|--skip-tests - does not run tests
		-sr|--skip-remove - does not remove config file with credentials
		-r|--report-file - specify path in gradle build dir to report file
		-h|--help - displays help
		-----------------------------------------
		EOF
		exit 0 ;;
		*) echo "Unknown parameter passed: $1"; exit 1 ;;
	esac
	shift
done

INVENTORY=inventory/perf-test
SERVICE_URL=https://cs-513-nxcals1.cern.ch:19093,https://cs-513-nxcals2.cern.ch:19093
KAFKA_SERVERS_URL=
SPARK_SERVERS_URL=
HOME=/tmp/"$USER"/nxcals_performance_tests
NAMESPACE=nxcals_perf_test
RBAC_ENV=test

if [ -z "${JENKINS_ENV:-}" ]; then	
	VAULT_PASS=.pass
fi

FILE=performance-tests.yml

set_nxcals_version
set_pytimber_version
set_hadoop_cluster

run_ansible

#	Setting the user to 'acclog' - all the tests below should run under this user.
USER=acclog

if [ "$TESTS" -eq 1 ]; then
  run_gradle clean jmh junitPerformanceTests -PjmhReportFile="$REPORT_FILE" || TESTS_STATUS="$?"
fi

if [ "$TESTS_STATUS" -gt 0 ]; then
	<<-EOF cat
	-----------------------------------------
	!!! PERFORMANCE TESTS FAILED
	-----------------------------------------
	EOF
else
	<<-EOF cat
	-----------------------------------------
	PERFORMANCE TESTS SUCCESSFUL
	-----------------------------------------
	EOF
fi

if [ "$REMOVE" -eq 1 ]; then
	rm performance-tests/src/jmh/resources/application.conf || true
else
	<<-EOF cat
	-----------------------------------------
	Password file is not removed, please do this manually!
	-----------------------------------------
	EOF
fi

exit "$TESTS_STATUS"
