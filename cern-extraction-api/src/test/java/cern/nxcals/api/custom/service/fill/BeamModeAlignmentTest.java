package cern.nxcals.api.custom.service.fill;

import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.utils.TimeUtils;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.Comparator;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Random;
import java.util.TreeMap;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class BeamModeAlignmentTest {
    private final Comparator<TimeWindow> byStart = TimeWindow.compareByStartTime()
            .thenComparing(TimeWindow.compareByEndTime());

    private final BeamModeAlignment alignment = new BeamModeAlignment();

    @Test
    public void shouldReturnEmptyMapWithNullModeMap() {
        NavigableMap<TimeWindow, String> modesForTimeRange = alignment
                .getBeamModesForTimeRange(Collections.emptyNavigableMap(), TimeWindow.infinite());
        assertTrue(modesForTimeRange.isEmpty());
    }

    @Test
    public void shouldReturnEmptyMapWithNullFillMap() {
        NavigableMap<TimeWindow, Integer> fillsForTimeRange = alignment
                .getFillsForTimeRange(Collections.emptyNavigableMap(), TimeWindow.infinite());
        assertTrue(fillsForTimeRange.isEmpty());
    }

    @Test
    public void shouldReturnBeamModes() {
        int from = 1000;
        int to = 2000;

        NavigableMap<TimeWindow, String> modes = randomModeData(0, 12000);
        NavigableMap<TimeWindow, String> result = alignment
                .getBeamModesForTimeRange(modes, TimeWindow.between(from, to));

        for (Map.Entry<TimeWindow, String> beamMode: result.entrySet()) {
            assertTrue(beamMode.getKey().getEndTime().isAfter(TimeUtils.getInstantFromNanos(from)));
            assertTrue(beamMode.getKey().getStartTime().isBefore(TimeUtils.getInstantFromNanos(to)));
        }
    }

    @Test
    public void shouldReturnEmptyBeamModes() {
        int from = 1000;
        int to = 2000;

        NavigableMap<TimeWindow, String> modes = randomModeData(3000, 12000);

        assertTrue(alignment.getBeamModesForTimeRange(modes, TimeWindow.between(from, to)).isEmpty());
        assertTrue(alignment.getBeamModesForTimeRange(modes, TimeWindow.between(to, from)).isEmpty());
    }

    @Test
    public void shouldNotReturnTooEarlyBeamModes() {
        NavigableMap<TimeWindow, String> modes = new TreeMap<>(byStart);
        modes.put(TimeWindow.between(900, 950), "A");
        modes.put(TimeWindow.between(1050, 1150), "B");

        NavigableMap<TimeWindow, String> result = alignment
                .getBeamModesForTimeRange(modes, TimeWindow.between(1000, 1100));

        assertEquals(1, result.size());
        assertFalse(result.values().contains("A"));
        assertTrue(result.values().contains("B"));
    }

    @Test
    public void shouldNotReturnTooLateBeamModes() {
        NavigableMap<TimeWindow, String> modes = new TreeMap<>(byStart);
        modes.put(TimeWindow.between(900, 950), "A");
        modes.put(TimeWindow.between(1050, 1150), "B");

        NavigableMap<TimeWindow, String> result = alignment
                .getBeamModesForTimeRange(modes, TimeWindow.between(850, 1000));

        assertEquals(1, result.size());
        assertTrue(result.values().contains("A"));
        assertFalse(result.values().contains("B"));
    }

    @Test
    public void shouldBeamModesOnEquality() {
        NavigableMap<TimeWindow, String> modes = new TreeMap<>(byStart);
        modes.put(TimeWindow.between(900, 950), "A");

        assertTrue(
                alignment.getBeamModesForTimeRange(modes, TimeWindow.between(850, 950)).values().contains("A"));
        assertTrue(
                alignment.getBeamModesForTimeRange(modes, TimeWindow.between(900, 990)).values().contains("A"));
        assertTrue(
                alignment.getBeamModesForTimeRange(modes, TimeWindow.between(900, 950)).values().contains("A"));
        assertFalse(
                alignment.getBeamModesForTimeRange(modes, TimeWindow.between(950, 1000)).values().contains("A"));
    }

    @Test
    public void shouldReturnLongBeamMode() {
        NavigableMap<TimeWindow, String> modes = new TreeMap<>(byStart);
        modes.put(TimeWindow.between(500, 1000), "A");

        assertTrue(
                alignment.getBeamModesForTimeRange(modes, TimeWindow.between(600, 800)).values().contains("A"));
    }


    @Test
    public void shouldReturnFills() {
        int from = 1000;
        int to = 2000;

        NavigableMap<TimeWindow, Integer> fills = randomFillData(0, 12000);
        NavigableMap<TimeWindow, Integer> result = alignment.getFillsForTimeRange(fills, TimeWindow.between(from, to));

        assertFalse(result.isEmpty());
        for (Map.Entry<TimeWindow, Integer> fill : result.entrySet()) {
            assertTrue(fill.getKey().getEndTime().isAfter(TimeUtils.getInstantFromNanos(from)));
            assertTrue(fill.getKey().getStartTime().isBefore(TimeUtils.getInstantFromNanos(to)));
        }
    }

    @Test
    public void shouldReturnEmptyFills() {
        int from = 1000;
        int to = 2000;

        NavigableMap<TimeWindow, Integer> fills = randomFillData(3000, 12000);

        assertTrue(alignment.getFillsForTimeRange(fills, TimeWindow.between(from, to)).isEmpty());
        assertTrue(alignment.getFillsForTimeRange(fills, TimeWindow.between(to, from)).isEmpty());
    }

    @Test
    public void shouldNotReturnTooEarlyFills() {
        NavigableMap<TimeWindow, Integer> fills = new TreeMap<>(byStart);
        fills.put(TimeWindow.between(900, 950), 1);
        fills.put(TimeWindow.between(1050, 1150), 2);

        NavigableMap<TimeWindow, Integer> result = alignment
                .getFillsForTimeRange(fills, TimeWindow.between(1000, 1100));

        assertEquals(1, result.size());
        assertFalse(result.containsKey(TimeWindow.between(900, 950)));
        assertTrue(result.containsKey(TimeWindow.between(1050, 1150)));
        assertTrue(result.containsValue(2));
    }

    @Test
    public void shouldNotReturnTooLateFills() {
        NavigableMap<TimeWindow, Integer> fills = new TreeMap<>(byStart);
        fills.put(TimeWindow.between(900, 950), 1);
        fills.put(TimeWindow.between(1050, 1150), 2);

        NavigableMap<TimeWindow, Integer> result = alignment.getFillsForTimeRange(fills, TimeWindow.between(850, 1000));

        assertEquals(1, result.size());
        assertTrue(result.containsKey(TimeWindow.between(900, 950)));
        assertFalse(result.containsKey(TimeWindow.between(1050, 1150)));
        assertTrue(result.containsValue(1));
    }

    @Test
    public void shouldFillsOnEquality() {
        NavigableMap<TimeWindow, Integer> fills = new TreeMap<>(byStart);
        fills.put(TimeWindow.between(900, 950), 1);

        assertTrue(alignment.getFillsForTimeRange(fills, TimeWindow.between(850, 950)).containsValue(1));
        assertTrue(alignment.getFillsForTimeRange(fills, TimeWindow.between(900, 990)).containsValue(1));
        assertTrue(alignment.getFillsForTimeRange(fills, TimeWindow.between(900, 950)).containsValue(1));
        assertFalse(alignment.getFillsForTimeRange(fills, TimeWindow.between(950, 1000)).containsValue(1));
    }

    @Test
    public void shouldReturnLongFill() {
        NavigableMap<TimeWindow, Integer> fills = new TreeMap<>(byStart);
        fills.put(TimeWindow.between(500, 1000), 1);

        assertTrue(alignment.getFillsForTimeRange(fills, TimeWindow.between(600, 800)).containsValue(1));
    }

    @Test
    public void shouldReturnFillsWithBeamModes() {
        int from = 1000;
        int to = 2000;

        for(int i = 0; i < 500; i++) {
            NavigableMap<TimeWindow, Integer> fills = randomFillData(0, 10000);
            NavigableMap<TimeWindow, String> modes = randomModeData(0, 10000);

            NavigableMap<TimeWindow, Integer> result = alignment
                    .getFillsForTimeRange(fills, TimeWindow.between(from, to));

            assertFalse(result.isEmpty());
            for (Map.Entry<TimeWindow, Integer> fill : result.entrySet()) {
                assertTrue(fill.getKey().getEndTime().isAfter(TimeUtils.getInstantFromNanos(from)));
                assertTrue(fill.getKey().getStartTime().isBefore(TimeUtils.getInstantFromNanos(to)));

                for (Map.Entry<TimeWindow, String> mode : alignment.getBeamModesForTimeRange(modes, fill.getKey())
                        .entrySet()) {
                    assertTrue(mode.getKey().getEndTime().isAfter(fill.getKey().getStartTime()));
                    assertTrue(mode.getKey().getStartTime().isBefore(fill.getKey().getEndTime()));
                }
            }
        }
    }

    @Test
    public void shouldReturnFillsWithEmptyBeamModes() {
        int from = 1000;
        int to = 2000;

        NavigableMap<TimeWindow, Integer> fills = randomFillData(0, 10000);
        NavigableMap<TimeWindow, String> modes = randomModeData(3000, 10000);

        NavigableMap<TimeWindow, Integer> result = alignment.getFillsForTimeRange(fills, TimeWindow.between(from, to));

        for (Map.Entry<TimeWindow, Integer> fill : result.entrySet()) {
            NavigableMap<TimeWindow, String> beamModes = alignment.getBeamModesForTimeRange(modes, fill.getKey());
            assertNotNull(beamModes);
            assertTrue(beamModes.isEmpty());
        }
    }

    @Test
    public void shouldReturnShortFillWithLongBeamMode() {
        TimeWindow fill1 = TimeWindow.between(1000, 1500);
        TimeWindow fill2 = TimeWindow.between(1500, 2000);
        NavigableMap<TimeWindow, Integer> fills = new TreeMap<>(byStart);
        fills.put(fill1, 1);
        fills.put(fill2, 2);

        NavigableMap<TimeWindow, String> modes = new TreeMap<>(byStart);

        TimeWindow mode = TimeWindow.between(900, 2100);
        modes.put(mode, "A");

        NavigableMap<TimeWindow, Integer> result1 = alignment
                .getFillsForTimeRange(fills, TimeWindow.between(1100, 1200));
        assertTrue(result1.containsKey(fill1));
        assertFalse(result1.containsKey(fill2));

        NavigableMap<TimeWindow, Integer> result2 = alignment
                .getFillsForTimeRange(fills, TimeWindow.between(1600, 1700));
        assertTrue(result2.containsKey(fill2));
        assertFalse(result2.containsKey(fill1));

        assertTrue(alignment.getBeamModesForTimeRange(modes, fill1).containsKey(mode.intersect(fill1)));
        assertTrue(alignment.getBeamModesForTimeRange(modes, fill2).containsKey(mode.intersect(fill2)));
    }

    @Test
    public void shouldFitBeamModeToFill() {
        NavigableMap<TimeWindow, Integer> fills = new TreeMap<>(byStart);
        TimeWindow fill = TimeWindow.between(1000, 1500);
        fills.put(fill, 1);

        TimeWindow mode1 = TimeWindow.between(900, 1100);
        TimeWindow mode2 = TimeWindow.between(1100, 1300);
        TimeWindow mode3 = TimeWindow.between(1300, 1700);
        NavigableMap<TimeWindow, String> modes = new TreeMap<>(byStart);
        modes.put(mode1, "A");
        modes.put(mode2, "B");
        modes.put(mode3, "C");

        NavigableMap<TimeWindow, Integer> result = alignment
                .getFillsForTimeRange(fills, TimeWindow.between(1100, 1200));

        assertTrue(result.containsKey(fill));
        NavigableMap<TimeWindow, String> beamModes = alignment.getBeamModesForTimeRange(modes, fill);

        assertTrue(beamModes.containsKey(mode1.intersect(fill)));
        assertTrue(beamModes.containsKey(mode2.intersect(fill)));
        assertTrue(beamModes.containsKey(mode3.intersect(fill)));
        assertEquals("A", beamModes.get(mode1.intersect(fill)));
        assertEquals("B", beamModes.get(mode2.intersect(fill)));
        assertEquals("C", beamModes.get(mode3.intersect(fill)));
    }

    private NavigableMap<TimeWindow, Integer> randomFillData(long fromTimestamp, long toTimestamp) {
        NavigableMap<TimeWindow, Integer> fills = new TreeMap<>(byStart);
        int fillNumber = 1;
        long t1 = fromTimestamp;
        while (t1 < toTimestamp) {
            long d = (new Random()).nextInt(1000) + 1;
            fills.put(TimeWindow.between(t1, t1 + d), fillNumber++);
            t1 += d;
        }
        return fills;
    }

    private NavigableMap<TimeWindow, String> randomModeData(long fromTimestamp, long toTimestamp) {
        NavigableMap<TimeWindow, String> modes = new TreeMap<>(byStart);
        long t1 = fromTimestamp;
        while(t1 < toTimestamp) {
            long d = (new Random()).nextInt(20) + 1;
            modes.put(TimeWindow.between(t1, t1 + 1), "a");
            t1 += d + (new Random()).nextInt(20);
        }
        return modes;
    }
}
