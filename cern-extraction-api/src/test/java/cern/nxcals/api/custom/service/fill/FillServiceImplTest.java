package cern.nxcals.api.custom.service.fill;

import cern.nxcals.api.custom.domain.BeamMode;
import cern.nxcals.api.custom.domain.Fill;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.utils.TimeUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.Instant;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class FillServiceImplTest {

    @Mock
    private FillProvider fillProvider;

    @InjectMocks
    private FillServiceImpl service;

    @Test
    public void shouldWorkForEmptyData() {
        when(fillProvider.findFillsByRange(any(), any())).thenReturn(Collections.emptyNavigableMap());

        assertTrue(service.findFills(Instant.ofEpochMilli(100L), Instant.ofEpochMilli(200L)).isEmpty());
        assertFalse(service.findFill(1).isPresent());
    }

    @Test
    public void getFillsAndBeamModesInTimeWindow() {
        long fromNanos = 100L;
        long toNanos = 200L;

        TimeWindow timeWindow = TimeWindow.between(fromNanos, toNanos);
        NavigableMap<TimeWindow, Integer> fills = FillUtils.navigableMapFrom(Collections
                .singletonMap(timeWindow, 3));
        when(fillProvider.findFillsByRange(any(), any())).thenReturn(fills);

        NavigableMap<TimeWindow, String> mode = FillUtils
                .navigableMapFrom(Collections.singletonMap(timeWindow, "MODE"));
        Map<TimeWindow, NavigableMap<TimeWindow, String>> modes = Collections.singletonMap(timeWindow, mode);
        when(fillProvider.findBeamModesByTimeWindows(any())).thenReturn(modes);

        Instant from = TimeUtils.getInstantFromNanos(fromNanos);
        Instant to = TimeUtils.getInstantFromNanos(toNanos);

        List<Fill> foundFills = service.findFills(from, to);

        assertEquals(1, foundFills.size());
        Fill fill = foundFills.iterator().next();
        assertEquals(TimeWindow.between(from, to), fill.getValidity());
        assertEquals(3L, fill.getNumber());

        assertEquals(1, fill.getBeamModes().size());
        assertEquals("MODE", fill.getBeamModes().iterator().next().getBeamModeValue());
    }

    @Test
    public void getFillByFillNumber() {
        NavigableMap<TimeWindow, Integer> fills = FillUtils.navigableMapFrom(Collections.emptyMap());
        fills.put(TimeWindow.between(0, 100), 2);
        fills.put(TimeWindow.between(100, 200), 3);

        when(fillProvider.findFillsByRange(any(), any())).thenReturn(fills);
        when(fillProvider.findBeamModesByTimeWindows(any())).thenReturn(Collections.emptyMap());

        Optional<Fill> foundFill = service.findFill(3);

        assertTrue(foundFill.isPresent());
        assertEquals(3L, foundFill.get().getNumber());
        assertEquals(TimeWindow.between(100, 200), foundFill.get().getValidity());

        assertFalse(service.findFill(1).isPresent());

        verify(fillProvider, times(1)).findBeamModesByTimeWindows(any());
    }

    @Test
    public void shouldLoadFills() {
        NavigableMap<TimeWindow, Integer> fills = FillUtils.navigableMapFrom(Collections.emptyMap());
        fills.put(TimeWindow.between(100, 150), 2);
        fills.put(TimeWindow.between(150, 200), 2);
        fills.put(TimeWindow.between(200, 300), 3);

        when(fillProvider.findFillsByRange(any(), any())).thenReturn(fills);

        Optional<Fill> foundFill = service.findFill(2);
        assertTrue(foundFill.isPresent());
        assertEquals(2, foundFill.get().getNumber());
        assertEquals(TimeWindow.between(100, 200), foundFill.get().getValidity());

        Optional<Fill> foundFill2 = service.findFill(20);
        assertFalse(foundFill2.isPresent());

        verify(fillProvider, times(1)).findBeamModesByTimeWindows(any());
    }

    @Test
    public void shouldLoadFillsAndBeamModes() {
        NavigableMap<TimeWindow, Integer> fills = FillUtils.navigableMapFrom(Collections.emptyMap());
        fills.put(TimeWindow.between(100, 150), 2);
        fills.put(TimeWindow.between(170, 200), 2);
        fills.put(TimeWindow.between(200, 250), 3);
        fills.put(TimeWindow.between(250, 300), 3);
        fills.put(TimeWindow.between(300, 350), 4);
        when(fillProvider.findFillsByRange(any(), any())).thenReturn(fills);

        NavigableMap<TimeWindow, String> modes = FillUtils.navigableMapFrom(Collections.emptyMap());
        modes.put(TimeWindow.between(100, 150), "A");
        modes.put(TimeWindow.between(150, 180), "B");
        modes.put(TimeWindow.between(180, 280), "C");
        when(fillProvider.findBeamModesByTimeWindows(any())).thenReturn(
                Collections.singletonMap(TimeWindow.between(100, 200), modes));

        Optional<Fill> foundFill = service.findFill(2);
        assertTrue(foundFill.isPresent());
        assertEquals(2, foundFill.get().getNumber());
        assertEquals(TimeWindow.between(100, 200), foundFill.get().getValidity());

        assertEquals("A", foundFill.get().getBeamModes().get(0).getBeamModeValue());
        assertEquals("B", foundFill.get().getBeamModes().get(1).getBeamModeValue());
        assertEquals("C", foundFill.get().getBeamModes().get(2).getBeamModeValue());

        verify(fillProvider, times(1)).findBeamModesByTimeWindows(any());
    }

    @Test
    public void shouldLoadAndMergeDuplicateFills() {
        NavigableMap<TimeWindow, Integer> fills = FillUtils.navigableMapFrom(Collections.emptyMap());
        fills.put(TimeWindow.between(100, 200), 2);
        fills.put(TimeWindow.between(200, 250), 3);
        fills.put(TimeWindow.between(250, 280), 3);
        fills.put(TimeWindow.between(280, 300), 3);
        fills.put(TimeWindow.between(300, 350), 4);
        when(fillProvider.findFillsByRange(any(), any())).thenReturn(fills);

        NavigableMap<TimeWindow, String> modes = FillUtils.navigableMapFrom(Collections.emptyMap());
        modes.put(TimeWindow.between(180, 260), "C");
        modes.put(TimeWindow.between(260, 290), "D");
        modes.put(TimeWindow.between(290, 300), "E");
        when(fillProvider.findBeamModesByTimeWindows(any())).thenReturn(
                Collections.singletonMap(TimeWindow.between(200, 300), modes));

        Fill foundFill = service.findFill(3).orElse(null);
        assertNotNull(foundFill);
        assertEquals(3, foundFill.getNumber());
        assertEquals(TimeWindow.between(200, 300), foundFill.getValidity());

        assertEquals(3, foundFill.getBeamModes().size());
        assertEquals("C", foundFill.getBeamModes().get(0).getBeamModeValue());
        assertEquals("D", foundFill.getBeamModes().get(1).getBeamModeValue());
        assertEquals("E", foundFill.getBeamModes().get(2).getBeamModeValue());
    }

    @Test
    public void shouldGetEmptyFillOnLastCompletedFillWhenNoData() {
        when(fillProvider.findFillsByRange(any(), any())).thenReturn(Collections.emptyNavigableMap());

        Optional<Fill> lastCompleted = service.getLastCompleted();
        assertFalse(lastCompleted.isPresent());
    }

    @Test
    public void shouldGetLastCompletedFill() {
        TimeWindow expectedLastFillTImeWindow = TimeWindow.between(100, 300);

        NavigableMap<TimeWindow, Integer> fills = FillUtils.navigableMapFrom(Collections.emptyMap());
        fills.put(TimeWindow.between(0, 100), 1);
        fills.put(TimeWindow.between(100, 150), 2);
        fills.put(TimeWindow.between(200, 300), 2);
        when(fillProvider.findFillsByRange(any(), any())).thenReturn(fills);

        NavigableMap<TimeWindow, String> modes = FillUtils.navigableMapFrom(Collections.emptyMap());
        modes.put(TimeWindow.between(100, 150), "A");
        modes.put(TimeWindow.between(150, 180), "B");
        modes.put(TimeWindow.between(180, 200), "C");
        modes.put(TimeWindow.between(200, 290), "D");
        when(fillProvider.findBeamModesByTimeWindows(any())).thenReturn(
                Collections.singletonMap(expectedLastFillTImeWindow, modes));

        Fill lastFill = service.getLastCompleted().orElse(null);
        assertNotNull(lastFill);
        assertEquals(2, lastFill.getNumber());
        assertEquals(expectedLastFillTImeWindow, lastFill.getValidity());

        List<BeamMode> beamModes = lastFill.getBeamModes();
        assertFalse(beamModes.isEmpty());
        assertEquals(4, beamModes.size());
        assertEquals(Arrays.asList("A", "B", "C", "D"), beamModes.stream().map(BeamMode::getBeamModeValue).collect(
                Collectors.toList()));
    }

}
