package cern.nxcals.api.custom.service.extraction;

import cern.nxcals.api.domain.TimeWindow;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ExtractionPropertiesTest {

    @Test
    public void shouldThrowWhenTimeWindowNotSet() {
        assertThrows(NullPointerException.class, () -> ExtractionProperties.builder()
                .lookupStrategy(LookupStrategy.LAST_BEFORE_START)
                .build());
    }

    @Test
    public void shouldThrowWhenTimeWindowFromIsNull() {
        Instant from = null;
        Instant to = Instant.now();
        assertThrows(NullPointerException.class, () -> ExtractionProperties.builder()
                .timeWindow(from, to)
                .lookupStrategy(LookupStrategy.LAST_BEFORE_START)
                .build());
    }

    @Test
    public void shouldThrowWhenTimeWindowToIsNull() {
        Instant from = Instant.now();
        Instant to = null;
        assertThrows(NullPointerException.class, () -> ExtractionProperties.builder()
                .timeWindow(from, to)
                .lookupStrategy(LookupStrategy.LAST_BEFORE_START)
                .build());
    }

    @Test
    public void shouldThrowWhenRangeFromIsAfterEnd() {
        Instant from = Instant.now();
        Instant to = from.minus(1, ChronoUnit.HOURS);
        assertThrows(IllegalArgumentException.class, () -> ExtractionProperties.builder()
                .timeWindow(from, to)
                .lookupStrategy(LookupStrategy.LAST_BEFORE_START)
                .build());
    }

    @Test
    public void shouldThrowWhenRangeFromIsSameAsEnd() {
        Instant from = Instant.now();
        Instant to = from;
        assertThrows(IllegalArgumentException.class, () -> ExtractionProperties.builder()
                .timeWindow(from, to)
                .lookupStrategy(LookupStrategy.LAST_BEFORE_START)
                .build());
    }

    @Test
    public void shouldBuildExtractionProperties() {
        Instant from = Instant.now();
        Instant to = from.plus(1, ChronoUnit.HOURS);
        LookupStrategy lookupStrategy = LookupStrategy.LAST_BEFORE_START;
        ExtractionProperties extractionProps = ExtractionProperties.builder()
                .timeWindow(from, to)
                .lookupStrategy(lookupStrategy)
                .build();
        assertNotNull(extractionProps);

        TimeWindow timeWindow = extractionProps.getTimeWindow();
        assertNotNull(timeWindow);
        assertEquals(from, timeWindow.getStartTime());
        assertEquals(to, timeWindow.getEndTime());

        assertEquals(lookupStrategy, extractionProps.getLookupStrategy());
    }

    @Test
    public void shouldDefaultToNoLookupStrategyIfNoStrategyIsSpecified() {
        Instant from = Instant.now();
        Instant to = from.plus(1, ChronoUnit.HOURS);
        ExtractionProperties extractionProps = ExtractionProperties.builder()
                .timeWindow(from, to)
                .build();
        assertNotNull(extractionProps);

        TimeWindow timeWindow = extractionProps.getTimeWindow();
        assertNotNull(timeWindow);
        assertEquals(from, timeWindow.getStartTime());
        assertEquals(to, timeWindow.getEndTime());

        assertEquals(LookupStrategy.NONE, extractionProps.getLookupStrategy());
    }
}
