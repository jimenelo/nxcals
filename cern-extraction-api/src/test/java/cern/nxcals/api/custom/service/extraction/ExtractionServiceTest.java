package cern.nxcals.api.custom.service.extraction;

import avro.shaded.com.google.common.collect.Sets;
import cern.nxcals.api.custom.domain.util.QueryDatasetProvider;
import cern.nxcals.api.custom.service.ExtractionService;
import cern.nxcals.api.custom.service.TestSparkSession;
import cern.nxcals.api.custom.service.TestUtils;
import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.Partition;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.domain.VariableDeclaredType;
import cern.nxcals.api.utils.TimeUtils;
import org.apache.avro.SchemaBuilder;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.AdditionalMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.function.Supplier;

import static cern.nxcals.common.SystemFields.NXC_EXTR_TIMESTAMP;
import static cern.nxcals.common.SystemFields.NXC_EXTR_VALUE;
import static org.apache.spark.sql.functions.col;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ExtractionServiceTest {
    private static final Supplier<SparkSession> SESSION_SUPPLIER = TestSparkSession.JUNIT;

    private static final Instant START = TimeUtils.getInstantFromString("2020-01-01 00:00:00.000");
    public static final Instant LAST_BEFORE_START = START.minusSeconds(3);

    private static final String ENTITY_TIMESTAMP_FIELD_NAME = "__record_timestamp__";
    private static final String ENTITY_DATA_FIELD_NAME = "entityField";

    private static final StructType VARIABLE_SCHEMA = createVariableSchema();
    private static final StructType ENTITY_SCHEMA = createEntitySchema();

    private static final String SYSTEM_TIME_KEY_STRING = SchemaBuilder.record("test").fields()
            .name(ENTITY_TIMESTAMP_FIELD_NAME).type().longType().noDefault().endRecord().toString();

    private static final SystemSpec SYSTEM = SystemSpec.builder()
            .timeKeyDefinitions(SYSTEM_TIME_KEY_STRING)
            .partitionKeyDefinitions("")
            .entityKeyDefinitions("")
            .name("TEST_SYSTEM")
            .build();

    private static final Partition PARTITION = Partition.builder()
            .keyValues(Collections.emptyMap())
            .systemSpec(SYSTEM)
            .build();

    private static final Set<String> EXPECTED_VARIABLE_COLUMNS = Sets
            .newHashSet(NXC_EXTR_TIMESTAMP.getValue(), NXC_EXTR_VALUE.getValue());

    private static final Set<String> EXPECTED_ENTITY_COLUMNS = Sets
            .newHashSet(ENTITY_TIMESTAMP_FIELD_NAME, ENTITY_DATA_FIELD_NAME);

    private static StructType createVariableSchema() {
        StructField timestampField = new StructField(NXC_EXTR_TIMESTAMP.getValue(), DataTypes.LongType, true,
                Metadata.empty());
        StructField valueField = new StructField(NXC_EXTR_VALUE.getValue(), DataTypes.LongType, true, Metadata.empty());
        return new StructType(new StructField[] { timestampField, valueField });
    }

    private static StructType createEntitySchema() {
        StructField timestampField = new StructField(ENTITY_TIMESTAMP_FIELD_NAME, DataTypes.LongType, true,
                Metadata.empty());
        StructField valueField = new StructField(ENTITY_DATA_FIELD_NAME, DataTypes.LongType, true, Metadata.empty());
        return new StructType(new StructField[] { timestampField, valueField });
    }

    @Mock
    private QueryDatasetProvider<Variable> variableDataProvider;
    @Mock
    private QueryDatasetProvider<Entity> entityDataProvider;

    private ExtractionService extractionService;

    @BeforeEach
    public void setUp() {
        extractionService = new ExtractionServiceImpl(SESSION_SUPPLIER, variableDataProvider, entityDataProvider);
    }

    // test: getData - variable

    @Test
    public void shouldThrowOnGetDataWhenVariableIsNull() {
        Variable variable = null;
        Instant from = Instant.now();
        Instant to = from.plus(1, ChronoUnit.MINUTES);
        ExtractionProperties properties = ExtractionProperties.builder()
                .timeWindow(from, to).lookupStrategy(LookupStrategy.LAST_BEFORE_START).build();
        assertThrows(NullPointerException.class, () -> extractionService.getData(variable, properties));
    }

    @Test
    public void shouldThrowOnGetDataForVariableWhenExtractionPropertiesAreNull() {
        Variable variable = Mockito.mock(Variable.class);
        ExtractionProperties properties = null;
        assertThrows(NullPointerException.class, () -> extractionService.getData(variable, properties));
    }

    @Test
    public void shouldGetEmptyDatasetForVariableWhenNoDataAvailable() {
        Variable variable = getVariable();
        ExtractionProperties properties = ExtractionProperties.builder()
                .lookupStrategy(LookupStrategy.LAST_BEFORE_START)
                .timeWindow(START, START.plus(1, ChronoUnit.HOURS)).build();
        when(variableDataProvider.get(any(), any(), eq(variable)))
                .thenReturn(buildEmptyDataset(VARIABLE_SCHEMA));
        Dataset<Row> dataset = extractionService.getData(variable, properties);
        assertNotNull(dataset);
        assertTrue(dataset.isEmpty());
    }

    @Test
    public void shouldGetDataForVariableWithoutLookupIfPriorDataDoNotExist() {
        Variable variable = getVariable();
        Instant from = START.plusSeconds(1);
        Instant to = START.plus(1, ChronoUnit.HOURS);
        LookupStrategy lookupStrategy = LookupStrategy.LAST_BEFORE_START;
        ExtractionProperties properties = ExtractionProperties.builder()
                .lookupStrategy(lookupStrategy)
                .timeWindow(from, to).build();

        Dataset<Row> expectedDataset = buildDataset(VARIABLE_SCHEMA);

        when(variableDataProvider.get(any(), eq(TimeWindow.between(from.minusNanos(1), to)), eq(variable)))
                .thenReturn(expectedDataset);

        when(variableDataProvider.get(any(), AdditionalMatchers.not(eq(TimeWindow.between(from.minusNanos(1), to))),
                eq(variable)))
                .thenReturn(buildEmptyDataset(VARIABLE_SCHEMA));
        Dataset<Row> dataset = extractionService.getData(variable, properties);
        assertNotNull(dataset);
        assertVariableDatasetResult(dataset, expectedDataset.count());
    }

    @Test
    public void shouldGetLastValueForVariableWithLookupIfNoDataExist() {
        Variable variable = getVariable();
        Instant from = START.plus(1, ChronoUnit.HOURS);
        Instant to = from.plus(1, ChronoUnit.HOURS);
        LookupStrategy lookupStrategy = LookupStrategy.LAST_BEFORE_START;
        ExtractionProperties properties = ExtractionProperties.builder()
                .lookupStrategy(lookupStrategy)
                .timeWindow(from, to).build();

        Dataset<Row> last = limitToLast(buildLookupDataset(VARIABLE_SCHEMA), NXC_EXTR_TIMESTAMP.getValue());

        when(variableDataProvider
                .get(any(), eq(TimeWindow.between(from.truncatedTo(ChronoUnit.DAYS), from.minusNanos(1))),
                        eq(variable)))
                .thenReturn(last);

        when(variableDataProvider
                .get(any(), eq(TimeWindow.between(LAST_BEFORE_START, to)), eq(variable)))
                .thenReturn(last);

        Dataset<Row> dataset = extractionService.getData(variable, properties);
        assertNotNull(dataset);
        assertVariableDatasetResult(dataset, 1L);
        long expectedLastValueTimestampNanos = TimeUtils.getNanosFromInstant(LAST_BEFORE_START);
        long lastValueTimestampNanos = dataset.collectAsList().get(0).getAs(NXC_EXTR_TIMESTAMP.getValue());
        assertEquals(expectedLastValueTimestampNanos, lastValueTimestampNanos);
    }

    @Test
    public void shouldGetDataForVariableWithoutLookupIfDataExist() {
        Variable variable = getVariable();
        Instant from = START;
        Instant to = from.plus(1, ChronoUnit.HOURS);
        LookupStrategy lookupStrategy = LookupStrategy.LAST_BEFORE_START_IF_EMPTY;
        ExtractionProperties properties = ExtractionProperties.builder()
                .lookupStrategy(lookupStrategy)
                .timeWindow(from, to).build();

        Dataset<Row> expectedDataset = buildDataset(VARIABLE_SCHEMA);

        when(variableDataProvider.get(any(), eq(TimeWindow.between(from, to)), eq(variable)))
                .thenReturn(expectedDataset);

        Dataset<Row> dataset = extractionService.getData(variable, properties);
        assertNotNull(dataset);
        assertVariableDatasetResult(dataset, expectedDataset.count());

        verify(variableDataProvider, only()).get(any(), any(), any());
    }

    @Test
    public void shouldGetDataForVariableWithLookupIfDataDoNotExist() {
        Variable variable = getVariable();
        Instant from = START.plus(1, ChronoUnit.HOURS);
        Instant to = from.plus(1, ChronoUnit.MINUTES);
        LookupStrategy lookupStrategy = LookupStrategy.LAST_BEFORE_START_IF_EMPTY;
        ExtractionProperties properties = ExtractionProperties.builder()
                .lookupStrategy(lookupStrategy)
                .timeWindow(from, to).build();

        when(variableDataProvider.get(any(), eq(TimeWindow.between(from, to)), eq(variable)))
                .thenReturn(buildEmptyDataset(VARIABLE_SCHEMA));

        Dataset<Row> last = limitToLast(buildLookupDataset(VARIABLE_SCHEMA), NXC_EXTR_TIMESTAMP.getValue());
        when(variableDataProvider
                .get(any(), eq(TimeWindow.between(from.truncatedTo(ChronoUnit.DAYS), from.minusNanos(1))),
                        eq(variable)))
                .thenReturn(last);
        when(variableDataProvider
                .get(any(), eq(TimeWindow.between(LAST_BEFORE_START, from.minusNanos(1))), eq(variable)))
                .thenReturn(last);

        Dataset<Row> dataset = extractionService.getData(variable, properties);
        assertNotNull(dataset);
        assertVariableDatasetResult(dataset, 1L);

        verify(variableDataProvider, times(3)).get(any(), any(), any());
    }

    @Test
    public void shouldGetDataForVariable() {
        Variable variable = getVariable();
        Instant from = START.plusSeconds(1);
        Instant to = START.plus(1, ChronoUnit.HOURS);
        LookupStrategy lookupStrategy = LookupStrategy.LAST_BEFORE_START;
        ExtractionProperties properties = ExtractionProperties.builder()
                .lookupStrategy(lookupStrategy)
                .timeWindow(from, to).build();

        Dataset<Row> expectedActualDataset = buildDataset(VARIABLE_SCHEMA);
        Dataset<Row> last = limitToLast(buildLookupDataset(VARIABLE_SCHEMA), NXC_EXTR_TIMESTAMP.getValue());
        when(variableDataProvider
                .get(any(), eq(TimeWindow.between(from.truncatedTo(ChronoUnit.DAYS), from.minusNanos(1))),
                        eq(variable)))
                .thenReturn(last);
        when(variableDataProvider
                .get(any(), eq(TimeWindow.between(LAST_BEFORE_START, to)), eq(variable)))
                .thenReturn(last.union(expectedActualDataset));

        Dataset<Row> dataset = extractionService.getData(variable, properties);
        assertNotNull(dataset);
        long expectedDatasetSize = expectedActualDataset.count() + 1; // actual plus the last value from lookup dataset
        assertVariableDatasetResult(dataset, expectedDatasetSize);
    }

    // test: getData - entity

    @Test
    public void shouldThrowOnGetDataWhenEntityIsNull() {
        Entity entity = null;
        Instant from = Instant.now();
        Instant to = from.plus(1, ChronoUnit.MINUTES);
        ExtractionProperties properties = ExtractionProperties.builder()
                .timeWindow(from, to).lookupStrategy(LookupStrategy.LAST_BEFORE_START).build();
        assertThrows(NullPointerException.class, () -> extractionService.getData(entity, properties));
    }

    @Test
    public void shouldThrowOnGetDataForEntityWhenExtractionPropertiesAreNull() {
        Entity entity = Mockito.mock(Entity.class);
        ExtractionProperties properties = null;
        assertThrows(NullPointerException.class, () -> extractionService.getData(entity, properties));
    }

    @Test
    public void shouldGetEmptyDatasetForEntityWhenNoDataAvailable() {
        Entity entity = getEntity();
        ExtractionProperties properties = ExtractionProperties.builder()
                .lookupStrategy(LookupStrategy.LAST_BEFORE_START)
                .timeWindow(START, START.plus(1, ChronoUnit.HOURS)).build();
        when(entityDataProvider.get(any(), any(), eq(entity)))
                .thenReturn(buildEmptyDataset(ENTITY_SCHEMA));
        Dataset<Row> dataset = extractionService.getData(entity, properties);
        assertNotNull(dataset);
        assertTrue(dataset.isEmpty());
    }

    @Test
    public void shouldGetDataForEntityWithoutLookupIfPriorDataDoNotExist() {
        Entity entity = getEntity();
        Instant from = START;
        Instant to = START.plus(1, ChronoUnit.HOURS);
        LookupStrategy lookupStrategy = LookupStrategy.LAST_BEFORE_START;
        ExtractionProperties properties = ExtractionProperties.builder()
                .lookupStrategy(lookupStrategy)
                .timeWindow(from, to).build();

        Dataset<Row> expectedDataset = buildDataset(ENTITY_SCHEMA);

        when(entityDataProvider.get(any(), eq(TimeWindow.between(from.minusNanos(1), to)), eq(entity)))
                .thenReturn(expectedDataset);
        when(entityDataProvider.get(any(), AdditionalMatchers.not(eq(TimeWindow.between(from.minusNanos(1), to))),
                eq(entity)))
                .thenReturn(buildEmptyDataset(ENTITY_SCHEMA));

        Dataset<Row> dataset = extractionService.getData(entity, properties);
        assertNotNull(dataset);
        assertEntityDatasetResult(dataset, expectedDataset.count());
    }

    @Test
    public void shouldGetLastValueForEntityWithLookupIfNoDataExist() {
        Entity entity = getEntity();
        Instant from = START.plus(1, ChronoUnit.HOURS);
        Instant to = from.plus(1, ChronoUnit.HOURS);
        LookupStrategy lookupStrategy = LookupStrategy.LAST_BEFORE_START;
        ExtractionProperties properties = ExtractionProperties.builder()
                .lookupStrategy(lookupStrategy)
                .timeWindow(from, to).build();

        Dataset<Row> last = limitToLast(buildLookupDataset(ENTITY_SCHEMA), ENTITY_TIMESTAMP_FIELD_NAME);
        when(entityDataProvider
                .get(any(), eq(TimeWindow.between(from.truncatedTo(ChronoUnit.DAYS), from.minusNanos(1))), eq(entity)))
                .thenReturn(last);
        when(entityDataProvider.get(any(), eq(TimeWindow.between(LAST_BEFORE_START, to)), eq(entity)))
                .thenReturn(last);
        Dataset<Row> dataset = extractionService.getData(entity, properties);
        assertNotNull(dataset);
        assertEntityDatasetResult(dataset, 1L);
        long expectedLastValueTimestampNanos = TimeUtils.getNanosFromInstant(LAST_BEFORE_START);
        long lastValueTimestampNanos = dataset.collectAsList().get(0).getAs(ENTITY_TIMESTAMP_FIELD_NAME);
        assertEquals(expectedLastValueTimestampNanos, lastValueTimestampNanos);
    }

    @Test
    public void shouldGetDataForEntityWithoutLookupIfDataExist() {
        Entity entity = getEntity();
        Instant from = START;
        Instant to = from.plus(1, ChronoUnit.HOURS);
        LookupStrategy lookupStrategy = LookupStrategy.LAST_BEFORE_START_IF_EMPTY;
        ExtractionProperties properties = ExtractionProperties.builder()
                .lookupStrategy(lookupStrategy)
                .timeWindow(from, to).build();

        Dataset<Row> expectedDataset = buildDataset(ENTITY_SCHEMA);

        when(entityDataProvider.get(any(), eq(TimeWindow.between(from, to)), eq(entity)))
                .thenReturn(expectedDataset);
        Dataset<Row> dataset = extractionService.getData(entity, properties);
        assertNotNull(dataset);
        assertEntityDatasetResult(dataset, expectedDataset.count());

        verify(entityDataProvider, only()).get(any(), any(), any());
    }

    @Test
    public void shouldGetDataForEntityWithLookupIfDataDoNotExist() {
        Entity entity = getEntity();
        Instant from = START.plus(1, ChronoUnit.HOURS);
        Instant to = from.plus(1, ChronoUnit.MINUTES);
        LookupStrategy lookupStrategy = LookupStrategy.LAST_BEFORE_START_IF_EMPTY;
        ExtractionProperties properties = ExtractionProperties.builder()
                .lookupStrategy(lookupStrategy)
                .timeWindow(from, to).build();

        when(entityDataProvider.get(any(), eq(TimeWindow.between(from, to)), eq(entity)))
                .thenReturn(buildEmptyDataset(ENTITY_SCHEMA));
        Dataset<Row> last = limitToLast(buildLookupDataset(ENTITY_SCHEMA), ENTITY_TIMESTAMP_FIELD_NAME);
        when(entityDataProvider
                .get(any(), eq(TimeWindow.between(from.truncatedTo(ChronoUnit.DAYS), from.minusNanos(1))), eq(entity)))
                .thenReturn(last);
        when(entityDataProvider.get(any(), eq(TimeWindow.between(LAST_BEFORE_START, from.minusNanos(1))), eq(entity)))
                .thenReturn(last);
        Dataset<Row> dataset = extractionService.getData(entity, properties);
        assertNotNull(dataset);
        assertEntityDatasetResult(dataset, 1L);

        verify(entityDataProvider, times(3)).get(any(), any(), any());
    }

    @Test
    public void shouldGetDataForEntity() {
        Entity entity = getEntity();
        Instant from = START.plusSeconds(1);
        Instant to = START.plus(1, ChronoUnit.HOURS);
        LookupStrategy lookupStrategy = LookupStrategy.LAST_BEFORE_START;
        ExtractionProperties properties = ExtractionProperties.builder()
                .lookupStrategy(lookupStrategy)
                .timeWindow(from, to).build();

        Dataset<Row> expectedActualDataset = buildDataset(ENTITY_SCHEMA);

        Dataset<Row> last = limitToLast(buildLookupDataset(ENTITY_SCHEMA), ENTITY_TIMESTAMP_FIELD_NAME);
        when(entityDataProvider
                .get(any(), eq(TimeWindow.between(from.truncatedTo(ChronoUnit.DAYS), from.minusNanos(1))), eq(entity)))
                .thenReturn(last);
        when(entityDataProvider.get(any(), eq(TimeWindow.between(LAST_BEFORE_START, to)), eq(entity)))
                .thenReturn(last.union(expectedActualDataset));

        Dataset<Row> dataset = extractionService.getData(entity, properties);
        assertNotNull(dataset);
        long expectedDatasetSize = expectedActualDataset.count() + 1; // actual plus the last value from lookup dataset
        assertEntityDatasetResult(dataset, expectedDatasetSize);
    }

    // helper methods

    private Variable getVariable() {
        return Variable.builder()
                .variableName("test")
                .configs(Collections.emptySortedSet())
                .systemSpec(SYSTEM)
                .declaredType(VariableDeclaredType.NUMERIC)
                .build();
    }

    private Entity getEntity() {
        return Entity.builder()
                .entityKeyValues(Collections.emptyMap())
                .systemSpec(SYSTEM)
                .partition(PARTITION)
                .entityHistory(Collections.emptySortedSet())
                .build();
    }

    private Dataset<Row> buildDataset(StructType schema) {
        return TestUtils.buildDataset(schema, new Object[][] {
                { TimeUtils.getNanosFromInstant(START), 1L },
                { TimeUtils.getNanosFromInstant(START.plusSeconds(3)), 1L },
                { TimeUtils.getNanosFromInstant(START.plusSeconds(25)), 2L },
                { TimeUtils.getNanosFromInstant(START.plusSeconds(40)), 2L },
                { TimeUtils.getNanosFromInstant(START.plusSeconds(50)), 3L }

        });
    }

    private Dataset<Row> buildLookupDataset(StructType schema) {
        return TestUtils.buildDataset(schema, new Object[][] {
                { TimeUtils.getNanosFromInstant(LAST_BEFORE_START), 1L },
                { TimeUtils.getNanosFromInstant(START.minusSeconds(25)), 2L },
                { TimeUtils.getNanosFromInstant(START.minusSeconds(40)), 2L },
                { TimeUtils.getNanosFromInstant(START.minusSeconds(50)), 3L }
        });
    }

    private Dataset<Row> buildEmptyDataset(StructType schema) {
        return TestUtils.buildDataset(schema);
    }

    private void assertVariableDatasetResult(Dataset<Row> result, long expectedResultSize) {
        assertResult(result, expectedResultSize, EXPECTED_VARIABLE_COLUMNS);
    }

    private void assertEntityDatasetResult(Dataset<Row> result, long expectedResultSize) {
        assertResult(result, expectedResultSize, EXPECTED_ENTITY_COLUMNS);
    }

    private void assertResult(Dataset<Row> result, long expectedResultSize, Set<String> expectedColumns) {
        assertNotNull(result);

        List<String> actualColumns = Arrays.asList(result.columns());
        assertEquals(expectedColumns.size(), actualColumns.size());
        assertTrue(expectedColumns.containsAll(actualColumns));

        assertEquals(expectedResultSize, result.count());
    }

    private Dataset<Row> limitToLast(Dataset<Row> dataset, String timestampField) {
        return dataset.orderBy(col(timestampField).desc()).limit(1);
    }

}
