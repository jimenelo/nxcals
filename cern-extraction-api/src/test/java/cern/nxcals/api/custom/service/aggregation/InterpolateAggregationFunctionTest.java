package cern.nxcals.api.custom.service.aggregation;

import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static cern.nxcals.api.custom.service.aggregation.AggregationConstants.INTERVAL_RANGE_FIELDS;
import static cern.nxcals.api.custom.service.aggregation.AggregationFunction.TIMESTAMP_COLUMN_NAME;
import static cern.nxcals.api.custom.service.aggregation.AggregationFunction.VALUE_COLUMN_NAME;
import static cern.nxcals.api.custom.service.aggregation.InterpolateAggregationFunction.INTERPOLATED_VALUE_COLUMN_NAME;
import static cern.nxcals.api.custom.service.aggregation.InterpolateAggregationFunction.NEXT_TIMESTAMP_COLUMN_NAME;
import static cern.nxcals.api.custom.service.aggregation.InterpolateAggregationFunction.NEXT_VALUE_COLUMN_NAME;
import static cern.nxcals.api.custom.service.aggregation.InterpolateAggregationFunction.PREV_TIMESTAMP_COLUMN_NAME;
import static cern.nxcals.api.custom.service.aggregation.InterpolateAggregationFunction.PREV_VALUE_COLUMN_NAME;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class InterpolateAggregationFunctionTest extends BaseAggregationFunctionTest {
    private static final StructType INTERPOLATE_SCHEMA = createInterpolateSchema();

    private static StructType createInterpolateSchema() {
        StructField timestampField = new StructField(TIMESTAMP_COLUMN_NAME, DataTypes.LongType, true, Metadata.empty());
        StructField valueField = new StructField(VALUE_COLUMN_NAME, DataTypes.LongType, true, Metadata.empty());
        StructField intervalStartField = new StructField(INTERVAL_RANGE_FIELDS.getLeft(), DataTypes.LongType, true,
                Metadata.empty());
        StructField intervalsEndField = new StructField(INTERVAL_RANGE_FIELDS.getRight(), DataTypes.LongType, true,
                Metadata.empty());

        StructField prevTimestampField = new StructField(PREV_TIMESTAMP_COLUMN_NAME, DataTypes.LongType, true,
                Metadata.empty());
        StructField prevValueField = new StructField(PREV_VALUE_COLUMN_NAME, DataTypes.LongType, true,
                Metadata.empty());

        StructField nextTimestampField = new StructField(NEXT_TIMESTAMP_COLUMN_NAME, DataTypes.LongType, true,
                Metadata.empty());
        StructField nextValueField = new StructField(NEXT_VALUE_COLUMN_NAME, DataTypes.LongType, true,
                Metadata.empty());
        StructField interpolateField = new StructField(INTERPOLATED_VALUE_COLUMN_NAME, DataTypes.DoubleType, true,
                Metadata.empty());
        return new StructType(new StructField[] { timestampField, valueField, intervalStartField, intervalsEndField,
                prevTimestampField, prevValueField, nextTimestampField, nextValueField, interpolateField });
    }

    private static final List<String> EXPECTED_INTERPOLATE_COLUMNS = Arrays.asList(PREV_TIMESTAMP_COLUMN_NAME,
            PREV_VALUE_COLUMN_NAME, NEXT_TIMESTAMP_COLUMN_NAME, NEXT_VALUE_COLUMN_NAME);

    private final ExpandableAggregationFunction interpolateFunction = new InterpolateAggregationFunction();

    @Test
    public void shouldThrowOnPrepareWhenDatasetIsNull() {
        assertThrows(NullPointerException.class, () -> interpolateFunction.prepare(null));
    }

    @Test
    public void shouldPrepareDatasetAddingNullAsPrevColumnsOnStartWhenNoPreviousDataPoint() {
        long rangeStartTimestamp = 1L;
        long rangeEndTimestamp = 3L;
        Dataset<Row> inputDs = buildDataset(AGGREGATION_SCHEMA, new Object[][] {
                { 2L, 1L, rangeStartTimestamp, rangeEndTimestamp },
                { 3L, 2L, rangeStartTimestamp, rangeEndTimestamp },
                { 4L, 3L, rangeEndTimestamp + 1, rangeEndTimestamp + 2 },
        });
        Dataset<Row> result = interpolateFunction.prepare(inputDs);
        assertNotNull(result);
        assertTrue(Arrays.asList(result.columns()).containsAll(EXPECTED_INTERPOLATE_COLUMNS));

        Dataset<Row> expectedAdaptedDs = buildDataset(INTERPOLATE_SCHEMA, new Object[][] {
                { 2L, 1L, rangeStartTimestamp, rangeEndTimestamp, null, null, 3L, 2L, null },
                { 3L, 2L, rangeStartTimestamp, rangeEndTimestamp, 2L, 1L, 4L, 3L,
                        calculateInterpolation(rangeStartTimestamp, 2L, 1L, 4L, 3L) },
                { 4L, 3L, rangeEndTimestamp + 1, rangeEndTimestamp + 2, 4L, 3L, null, null, null },
        });
        assertArrayEquals(expectedAdaptedDs.collectAsList().toArray(), result.collectAsList().toArray());
    }

    @Test
    public void shouldAdaptDatasetAddingCurrentValueAsPrevColumnWhenRowMatchingIntervalStart() {
        long rangeStartTimestamp = 1L;
        long rangeEndTimestamp = 2L;
        Dataset<Row> inputDs = buildDataset(AGGREGATION_SCHEMA, new Object[][] {
                { 1L, 1L, rangeStartTimestamp, rangeEndTimestamp }, //row matching interval start
                { 2L, 2L, rangeStartTimestamp, rangeEndTimestamp },
                { 3L, 3L, rangeEndTimestamp, rangeEndTimestamp + 1 }
        });
        Dataset<Row> result = interpolateFunction.prepare(inputDs);
        assertNotNull(result);
        assertTrue(Arrays.asList(result.columns()).containsAll(EXPECTED_INTERPOLATE_COLUMNS));

        Dataset<Row> expectedAdaptedDs = buildDataset(INTERPOLATE_SCHEMA, new Object[][] {
                { 1L, 1L, rangeStartTimestamp, rangeEndTimestamp, 1L, 1L, 2L, 2L,
                        calculateInterpolation(rangeStartTimestamp, 1L, 1L, 2L, 2L) },
                { 2L, 2L, rangeStartTimestamp, rangeEndTimestamp, 1L, 1L, 3L, 3L,
                        calculateInterpolation(rangeStartTimestamp, 2L, 2L, 3L, 3L) },
                { 3L, 3L, rangeEndTimestamp, rangeEndTimestamp + 1, 2L, 2L, null, null, null }
        });
        assertArrayEquals(expectedAdaptedDs.collectAsList().toArray(), result.collectAsList().toArray());
    }

    @Test
    public void shouldAdaptDatasetAddingInterpolateColumns() {
        long rangeStartTimestamp = 1L;
        long rangeEndTimestamp = 3L;
        Dataset<Row> inputDs = buildDataset(AGGREGATION_SCHEMA, new Object[][] {
                { 0L, 1L, null, null }, //previous row (before range start)
                { 1L, 1L, rangeStartTimestamp, rangeEndTimestamp },
                { 2L, 2L, rangeStartTimestamp, rangeEndTimestamp },
                { 5L, 5L, rangeEndTimestamp + 1, rangeEndTimestamp + 2 } //next row (after range end)
        });
        Dataset<Row> result = interpolateFunction.prepare(inputDs);
        assertNotNull(result);
        assertTrue(Arrays.asList(result.columns()).containsAll(EXPECTED_INTERPOLATE_COLUMNS));

        Dataset<Row> expectedAdaptedDs = buildDataset(INTERPOLATE_SCHEMA, new Object[][] {
                { 0L, 1L, null, null, null, null, 1L, 1L, null },
                { 1L, 1L, rangeStartTimestamp, rangeEndTimestamp, 1L, 1L, 2L, 2L,
                        calculateInterpolation(rangeStartTimestamp, 1L, 1L, 2L, 2L) },
                { 2L, 2L, rangeStartTimestamp, rangeEndTimestamp, 1L, 1L, 5L, 5L,
                        calculateInterpolation(rangeStartTimestamp, 1L, 1L, 5L, 5L) },
                { 5L, 5L, rangeEndTimestamp + 1, rangeEndTimestamp + 2, 2L, 2L, null, null, null }
        });
        assertArrayEquals(expectedAdaptedDs.collectAsList().toArray(), result.collectAsList().toArray());
    }

    @Test
    public void shouldGetAggregationExpression() {
        Column interpolateExpr = interpolateFunction.getAggregationExpr();
        assertNotNull(interpolateExpr);

        double expectedInterpolationResult = calculateInterpolation(1L, 1L, 5L, 3L, 2L);

        Dataset<Row> inputInterpolateDs = buildDataset(INTERPOLATE_SCHEMA, new Object[][] {
                { 2L, 1L, 1L, 3L, 1L, 5L, 3L, 2L, expectedInterpolationResult },
                { 3L, 2L, 1L, 3L, 2L, 1L, 5L, 4L, calculateInterpolation(1L, 2L, 2L, 5L, 4L) },
        });

        List<Row> resultRows = inputInterpolateDs.agg(interpolateExpr).collectAsList();
        assertEquals(1, resultRows.size());

        double interpolationResult = resultRows.get(0).getDouble(0);
        assertEquals(expectedInterpolationResult, interpolationResult, 0.001D);
    }

    private double calculateInterpolation(long generatedPointTs, long prevPointTs,
            long prevPointValue, long nextPointTs, long nextPointValue) {
        double coefficient = (double) (nextPointValue - prevPointValue) / (nextPointTs - prevPointTs);

        return prevPointValue + (coefficient * (generatedPointTs - prevPointTs));
    }
}
