package cern.nxcals.api.custom.service.aggregation;

import cern.nxcals.api.custom.service.TestSparkSession;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;

import java.util.ArrayList;
import java.util.List;

import static cern.nxcals.api.custom.service.aggregation.AggregationConstants.INTERVAL_RANGE_FIELDS;
import static cern.nxcals.api.custom.service.aggregation.AggregationFunction.TIMESTAMP_COLUMN_NAME;
import static cern.nxcals.api.custom.service.aggregation.AggregationFunction.VALUE_COLUMN_NAME;

abstract class BaseAggregationFunctionTest {
    protected static final SparkSession SPARK_SESSION = TestSparkSession.JUNIT.get();

    protected static Dataset<Row> buildDataset(StructType schema, Object[]... data) {
        List<Row> rows = new ArrayList<>(data.length);
        for (Object[] datum : data) {
            rows.add(RowFactory.create(datum));
        }
        return SPARK_SESSION.createDataFrame(rows, schema);
    }

    protected static final StructType AGGREGATION_SCHEMA = createBaseAggregationSchema();

    private static StructType createBaseAggregationSchema() {
        StructField timestampField = new StructField(TIMESTAMP_COLUMN_NAME, DataTypes.LongType, true, Metadata.empty());
        StructField valueField = new StructField(VALUE_COLUMN_NAME, DataTypes.LongType, true, Metadata.empty());
        StructField intervalStartField = new StructField(INTERVAL_RANGE_FIELDS.getLeft(), DataTypes.LongType, true,
                Metadata.empty());
        StructField intervalsEndField = new StructField(INTERVAL_RANGE_FIELDS.getRight(), DataTypes.LongType, true,
                Metadata.empty());
        return new StructType(new StructField[] { timestampField, valueField, intervalStartField, intervalsEndField });
    }

}
