package cern.nxcals.api.custom.service.aggregation;

import cern.nxcals.api.custom.service.TestSparkSession;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.utils.TimeUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static cern.nxcals.api.custom.service.aggregation.AggregationConstants.INTERVAL_GROUP_BY_FIELD;
import static cern.nxcals.api.custom.service.aggregation.AggregationConstants.INTERVAL_RANGE_FIELDS;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
public class AggregationUtilsTest {
    private static final SparkSession SPARK_SESSION = TestSparkSession.JUNIT.get();

    @Test
    public void shouldThrowOnGenerateIntervalsWhenScalePropertiesNull() {
        assertThrows(NullPointerException.class, () -> AggregationUtils.generateFixedIntervals(SPARK_SESSION, null, ImmutablePair.nullPair()));
    }

    @Test
    public void shouldThrowOnGenerateIntervalsWhenSparkSessionIsNull() {
        Instant end = Instant.now();
        WindowAggregationProperties properties = WindowAggregationProperties.builder()
                .function(AggregationFunctions.COUNT)
                .timeWindow(end.minus(1, ChronoUnit.HOURS), end)
                .interval(1, ChronoUnit.MINUTES).build();
        assertThrows(NullPointerException.class, () -> AggregationUtils.generateFixedIntervals(null, properties, ImmutablePair.nullPair()));
    }

    @Test
    public void shouldThrowOnGenerateIntervalsWhenLimitsPairIsNull() {
        Instant end = Instant.now();
        WindowAggregationProperties properties = WindowAggregationProperties.builder()
                .function(AggregationFunctions.COUNT)
                .timeWindow(end.minus(1, ChronoUnit.HOURS), end)
                .interval(1, ChronoUnit.MINUTES).build();
        assertThrows(NullPointerException.class, () -> AggregationUtils.generateFixedIntervals(SPARK_SESSION, properties, null));
    }

    @Test
    public void shouldAdjustIntervalDurationIfRangeIsSmallerAndGenerateSingleRange() {
        Instant end = Instant.now();
        Instant start = end.minus(30, ChronoUnit.MINUTES);
        WindowAggregationProperties properties = WindowAggregationProperties.builder()
                .function(AggregationFunctions.AVG)
                .timeWindow(start, end)
                .interval(2, ChronoUnit.HOURS).build();

        Dataset<Row> dataset = AggregationUtils
                .generateFixedIntervals(SPARK_SESSION, properties, Pair.of(start, end));
        assertNotNull(dataset);
        assertTrue(Arrays.asList(dataset.columns()).containsAll(Arrays.asList(
                INTERVAL_RANGE_FIELDS.getLeft(), INTERVAL_RANGE_FIELDS.getRight(), INTERVAL_GROUP_BY_FIELD)));
        assertEquals(1, dataset.count());

        TimeWindow timeWindow = toIntervals(dataset).get(0);
        assertEquals(TimeWindow.between(start, end), timeWindow);

        Duration interval = Duration.between(timeWindow.getStartTime(), timeWindow.getEndTime());
        assertEquals(Duration.of(30, ChronoUnit.MINUTES).toNanos(), interval.toNanos());
    }

    @Test
    public void shouldGenerateFixedIntervals() {
        long intervalDurationMinutes = 22;
        Instant start = TimeUtils.getInstantFromString("2020-01-01 00:00:00.000");
        Instant end = start.plus(1, ChronoUnit.HOURS);

        WindowAggregationProperties properties = WindowAggregationProperties.builder()
                .function(AggregationFunctions.COUNT)
                .timeWindow(start, end)
                .interval(intervalDurationMinutes, ChronoUnit.MINUTES).build();
        Dataset<Row> dataset = AggregationUtils
                .generateFixedIntervals(SPARK_SESSION, properties, Pair.of(start, end));
        assertNotNull(dataset);
        assertTrue(Arrays.asList(dataset.columns()).containsAll(Arrays.asList(
                INTERVAL_RANGE_FIELDS.getLeft(), INTERVAL_RANGE_FIELDS.getRight(), INTERVAL_GROUP_BY_FIELD)));

        int expectedIntervals = (int) Math
                .ceil((double) Duration.between(start, end).getSeconds() / (60 * intervalDurationMinutes));
        assertEquals(expectedIntervals, dataset.count());

        Duration expectedDuration = Duration.of(intervalDurationMinutes, ChronoUnit.MINUTES);
        List<TimeWindow> expectedTimeWindows = generateExpectedTimeWindows(start, end, intervalDurationMinutes,
                expectedDuration);

        List<TimeWindow> timeWindows = toIntervals(dataset);
        for (int i = 0; i < timeWindows.size(); i++) {
            assertEquals(expectedTimeWindows.get(i), timeWindows.get(i));
        }

    }

    @Test
    public void shouldGenerateFixedIntervalsAndLimitBasedOnProvidedRange() {
        long intervalDurationMinutes = 22;
        Instant start = TimeUtils.getInstantFromString("2020-01-01 00:00:00.000");
        Instant end = start.plus(1, ChronoUnit.HOURS);

        WindowAggregationProperties properties = WindowAggregationProperties.builder()
                .function(AggregationFunctions.INTERPOLATE)
                .timeWindow(start, end)
                .interval(intervalDurationMinutes, ChronoUnit.MINUTES).build();
        Pair<Instant, Instant> limits = Pair.of(start.minus(12, ChronoUnit.HOURS),
                end.plus(20, ChronoUnit.MINUTES));
        Dataset<Row> dataset = AggregationUtils.generateFixedIntervals(SPARK_SESSION, properties, limits);
        assertNotNull(dataset);
        assertTrue(Arrays.asList(dataset.columns()).containsAll(Arrays.asList(
                INTERVAL_RANGE_FIELDS.getLeft(), INTERVAL_RANGE_FIELDS.getRight(), INTERVAL_GROUP_BY_FIELD)));

        Duration expectedDuration = Duration.of(intervalDurationMinutes, ChronoUnit.MINUTES);
        List<TimeWindow> expectedTimeWindows = new ArrayList<>();
        expectedTimeWindows
                .addAll(generateExpectedTimeWindows(limits.getLeft(), start.minusNanos(1), intervalDurationMinutes,
                        expectedDuration));
        expectedTimeWindows.addAll(generateExpectedTimeWindows(start, end, intervalDurationMinutes,
                expectedDuration));
        expectedTimeWindows
                .addAll(generateExpectedTimeWindows(end.plusNanos(1), limits.getRight(), intervalDurationMinutes,
                        expectedDuration));

        List<TimeWindow> timeWindows = toIntervals(dataset);
        for (int i = 0; i < timeWindows.size(); i++) {
            assertEquals(expectedTimeWindows.get(i), timeWindows.get(i));
        }

    }

    private List<TimeWindow> generateExpectedTimeWindows(Instant start, Instant end, long intervalDurationMinutes,
            Duration expectedDuration) {
        List<TimeWindow> expectedTimeWindows = new ArrayList<>();

        int expectedIntervals = (int) Math
                .ceil((double) Duration.between(start, end).getSeconds() / (60 * intervalDurationMinutes));
        for (int i = 0; i < expectedIntervals; i++) {
            Instant startInterval = start
                    .plus(expectedDuration.multipliedBy(i));
            Instant endInterval = (i == (expectedIntervals - 1)) ?
                    end :
                    start.plus(expectedDuration.multipliedBy(i + 1)).minusNanos(1);
            expectedTimeWindows.add(TimeWindow.between(startInterval, endInterval));
        }
        return expectedTimeWindows;
    }

    private List<TimeWindow> toIntervals(Dataset<Row> dataset) {
        return dataset.collectAsList().stream()
                .map(r -> TimeWindow.between(r.getLong(0), r.getLong(1)))
                .sorted(TimeWindow.compareByStartAndEndTime())
                .collect(Collectors.toList());
    }
}
