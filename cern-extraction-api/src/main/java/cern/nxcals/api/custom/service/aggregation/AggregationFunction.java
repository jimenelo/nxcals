package cern.nxcals.api.custom.service.aggregation;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

import java.time.Duration;

import static cern.nxcals.api.custom.service.aggregation.AggregationConstants.AGGREGATION_SOURCE_TIMESTAMP_FIELD;
import static cern.nxcals.api.custom.service.aggregation.AggregationConstants.AGGREGATION_SOURCE_VALUE_FIELD;

@RequiredArgsConstructor(access = AccessLevel.PROTECTED)
public abstract class AggregationFunction {
    protected static final String VALUE_COLUMN_NAME = AGGREGATION_SOURCE_VALUE_FIELD;
    protected static final String TIMESTAMP_COLUMN_NAME = AGGREGATION_SOURCE_TIMESTAMP_FIELD;

    @NonNull
    @Getter
    private final Pair<Duration, Duration> timeWindowExpansion;

    abstract Dataset<Row> prepare(@NonNull Dataset<Row> dataset);

    abstract Column getAggregationExpr();
}
