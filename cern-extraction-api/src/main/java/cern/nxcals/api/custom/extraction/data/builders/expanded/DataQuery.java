/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.api.custom.extraction.data.builders.expanded;

import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.EntityQuery;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.extraction.data.builders.fluent.QueryData;
import cern.nxcals.api.extraction.data.builders.fluent.v2.AbstractDataQuery;
import cern.nxcals.api.extraction.data.builders.fluent.v2.KeyValueStage;
import cern.nxcals.api.extraction.data.builders.fluent.v2.KeyValueStageLoop;
import cern.nxcals.api.extraction.data.builders.fluent.v2.VariableStageLoop;
import cern.nxcals.common.annotation.Experimental;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static java.util.Arrays.asList;

@Experimental
@RequiredArgsConstructor
public class DataQuery extends AbstractDataQuery<List<Dataset<Row>>> {
    private final SparkSession session;

    public static DataQuery builder(@NonNull SparkSession session) {
        return new DataQuery(session);
    }

    protected QueryData<List<Dataset<Row>>> queryData() {
        return new QueryData<>(new SparkDatasetProducer(session));
    }

    public static List<Dataset<Row>> getFor(@NonNull SparkSession sparkSession, @NonNull TimeWindow timeWindow,
            @NonNull Variable variable) {
        return DataQuery
                .getFor(sparkSession, timeWindow, variable.getSystemSpec().getName(),
                        variable.getVariableName());
    }

    public static List<Dataset<Row>> getFor(@NonNull SparkSession sparkSession, @NonNull TimeWindow timeWindow,
            @NonNull Entity entity) {
        return DataQuery
                .getFor(sparkSession, timeWindow, entity.getSystemSpec().getName(),
                        new EntityQuery(entity.getEntityKeyValues()));
    }

    @SafeVarargs
    public static List<Dataset<Row>> getFor(@NonNull SparkSession spark, @NonNull TimeWindow timeWindow,
            @NonNull String system, @NonNull Map<String, Object>... keyValuesArr) {
        return DataQuery.getFor(spark, timeWindow, system,
                Arrays.stream(keyValuesArr).map(EntityQuery::new).toArray(EntityQuery[]::new));
    }

    public static List<Dataset<Row>> getFor(@NonNull SparkSession spark, @NonNull TimeWindow timeWindow,
            @NonNull String system, @NonNull EntityQuery... entitiesQueries) {
        KeyValueStage<List<Dataset<Row>>> dataQuery = DataQuery.builder(spark)
                .entities().system(system);
        KeyValueStageLoop<List<Dataset<Row>>> entities = null;
        for (EntityQuery query : entitiesQueries) {
            entities = query.hasPatterns() ?
                    dataQuery.keyValuesLike(query.toMap()) :
                    dataQuery.keyValuesEq(query.getKeyValues());
        }
        if (entities == null) {
            throw new IllegalArgumentException("No entity query passed");
        }
        return entities.timeWindow(timeWindow).build();
    }

    public static List<Dataset<Row>> getFor(@NonNull SparkSession spark, @NonNull TimeWindow timeWindow,
            @NonNull String system, @NonNull String... variables) {
        return DataQuery.getFor(spark, timeWindow, system, asList(variables));
    }

    public static List<Dataset<Row>> getFor(@NonNull SparkSession spark, @NonNull TimeWindow timeWindow,
            @NonNull String system, @NonNull List<String> variables) {
        return DataQuery
                .getFor(spark, timeWindow, system, variables, Collections.emptyList());
    }

    public static List<Dataset<Row>> getFor(@NonNull SparkSession spark, @NonNull TimeWindow timeWindow,
            @NonNull String system, @NonNull List<String> variables,
            @NonNull List<String> variablesLike) {
        if (variables.isEmpty() && variablesLike.isEmpty()) {
            throw new IllegalArgumentException("No variable names nor variable name patterns given");
        }
        VariableStageLoop<List<Dataset<Row>>> builder = DataQuery.builder(spark).variables()
                .system(system)
                .nameIn(variables);

        variablesLike.forEach(builder::nameLike);

        return builder.timeWindow(timeWindow).build();
    }

}
