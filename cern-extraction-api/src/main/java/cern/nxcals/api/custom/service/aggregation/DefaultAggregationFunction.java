package cern.nxcals.api.custom.service.aggregation;

import lombok.NonNull;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

import java.util.function.UnaryOperator;

import static org.apache.spark.sql.functions.col;

final class DefaultAggregationFunction extends AggregationFunction {
    @NonNull
    private final UnaryOperator<Column> aggregationColumnFunc;

    DefaultAggregationFunction(@NonNull UnaryOperator<Column> aggregationColumnFunc) {
        super(ExpandTimeWindow.NONE);
        this.aggregationColumnFunc = aggregationColumnFunc;
    }

    @Override
    Dataset<Row> prepare(@NonNull Dataset<Row> dataset) {
        return dataset;
    }

    @Override
    Column getAggregationExpr() {
        return this.aggregationColumnFunc.apply(col(VALUE_COLUMN_NAME));
    }

}
