package cern.nxcals.api.custom.extraction.data.builders.expanded;

import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.extraction.data.builders.fluent.QueryData;
import cern.nxcals.api.extraction.data.builders.fluent.v2.DeviceStage;
import cern.nxcals.api.extraction.data.builders.fluent.v2.StageSequenceDeviceProperty;
import cern.nxcals.api.extraction.data.builders.fluent.v2.SystemStage;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import java.util.List;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ParameterDataQuery {
    public static SystemStage<DeviceStage<List<Dataset<Row>>>, List<Dataset<Row>>> builder(
            SparkSession session) {
        return StageSequenceDeviceProperty.<List<Dataset<Row>>>sequence()
                .apply(new QueryData<>(new SparkDatasetProducer(session)));
    }

    /**
     * Search for given device/property in given system within time window.
     *
     * @param sparkSession spark session
     * @param system       system name (CMW, PM ...)
     * @param device       device name
     * @param property     property name
     * @param timeWindow   time window to query for
     * @return Dataset with result in {@link Row}
     */
    public static List<Dataset<Row>> getFor(@NonNull SparkSession sparkSession, @NonNull String system,
            @NonNull String device, @NonNull String property, @NonNull TimeWindow timeWindow) {
        return ParameterDataQuery.builder(sparkSession)
                .system(system)
                .deviceEq(device)
                .propertyEq(property)
                .timeWindow(timeWindow)
                .build();
    }

    /**
     * Query for given parameter and return result dataset
     *
     * @param sparkSession spark session
     * @param system       system name
     * @param parameter    in form device/property
     * @param timeWindow   time window for query
     * @return result dataset
     */
    public static List<Dataset<Row>> getFor(@NonNull SparkSession sparkSession, @NonNull String system,
            @NonNull String parameter, @NonNull TimeWindow timeWindow) {
        return ParameterDataQuery.builder(sparkSession)
                .system(system)
                .parameterEq(parameter)
                .timeWindow(timeWindow)
                .build();
    }
}
