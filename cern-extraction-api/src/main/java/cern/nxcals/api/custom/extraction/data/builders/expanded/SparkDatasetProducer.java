package cern.nxcals.api.custom.extraction.data.builders.expanded;

import cern.nxcals.api.authorization.RbacHadoopDelegationTokenProvider;
import cern.nxcals.api.extraction.data.builders.CallDetailsProvider;
import cern.nxcals.api.extraction.data.builders.fluent.QueryData;
import cern.nxcals.api.extraction.data.spark.SparkExtractionTaskExecutor;
import cern.nxcals.api.extraction.metadata.InternalServiceClientFactory;
import cern.nxcals.common.domain.ExtractionCriteria;
import cern.nxcals.common.domain.ExtractionUnit;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import java.util.List;
import java.util.function.Function;

@Slf4j
@RequiredArgsConstructor
public class SparkDatasetProducer implements Function<QueryData<List<Dataset<Row>>>, List<Dataset<Row>>> {
    private final CallDetailsProvider detailsProvider = new CallDetailsProvider();
    private final SparkSession session;

    @Override
    public List<Dataset<Row>> apply(QueryData<List<Dataset<Row>>> queryData) {
        // We need to obtain initial delegation tokens before Spark tries to communicate with Hadoop
        RbacHadoopDelegationTokenProvider.obtainAndSetDelegationTokensViaRbacIfRequired();

        detailsProvider.setCallDetails(session);

        SparkExtractionTaskExecutor executor = new SparkExtractionTaskExecutor(session);
        ExtractionCriteria criteria = queryData.toExtractionCriteria();
        ExtractionUnit unit = InternalServiceClientFactory.createEntityResourceService().findBy(criteria);
        return executor.executeExpanded(unit);
    }
}
