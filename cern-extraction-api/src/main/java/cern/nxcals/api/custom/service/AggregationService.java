package cern.nxcals.api.custom.service;

import cern.nxcals.api.custom.service.aggregation.AggregationFunctions;
import cern.nxcals.api.custom.service.aggregation.DatasetAggregationProperties;
import cern.nxcals.api.custom.service.aggregation.WindowAggregationProperties;
import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.extraction.data.builders.DataQuery;
import cern.nxcals.api.extraction.data.builders.DevicePropertyDataQuery;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

/**
 * Represents a service for data aggregation actions, as configured by the provided properties.
 * Actions defined on this service operate on top of raw data that are directly extractable
 * via {@link DataQuery} or {@link DevicePropertyDataQuery}.
 *
 * @see Variable
 * @see Entity
 * @see WindowAggregationProperties
 * @see DatasetAggregationProperties
 * @see AggregationFunctions
 */
public interface AggregationService {

    /**
     * Returns a dataset where each row contains an aggregated variable data point on fixed, distinct timestamp.
     * The timestamp (in utc nanos) represents the start of an interval, part of a generated sequence of time windows.
     * Each data point contains the result of an applied aggregation function, as specified by the provided properties.
     * The aggregation function is applied on values present within the right-open intervals <b>[startInclusive, endExclusive)</b>
     *
     * @param variable   an instance of {@link Variable} pointing to raw data for the extraction
     * @param properties the aggregation properties containing all semantics to control the action
     * @return a dataset of aggregated rows
     */
    Dataset<Row> getData(Variable variable, WindowAggregationProperties properties);

    /**
     * Returns a dataset where each row contains an aggregated entity data point on fixed, distinct timestamp.
     * The timestamp (in utc nanos) represents the start of an interval, part of a generated sequence of time windows.
     * Each data point contains the result of an applied aggregation function, as specified by the provided properties.
     * The aggregation function is applied on values present within the right-open intervals <b>[startInclusive, endExclusive)</b>
     *
     * @param entity     an instance of {@link Entity} pointing to raw data for the extraction
     * @param properties the aggregation properties containing all semantics to control the action
     * @return a dataset of aggregated rows
     */
    Dataset<Row> getData(Entity entity, WindowAggregationProperties properties);

    /**
     * Returns a dataset where each row contains an aligned variable data point on timestamp, extracted from a provided driving dataset.
     * If the driving timestamp matches an actual data row timestamp, that matching value will be assigned to the aligned entry.
     * Otherwise, the process will try to identify the closest previous actual value and assign this one instead (repeat value).
     *
     * @param variable   an instance of {@link Variable} pointing to raw data for the extraction
     * @param properties the aggregation properties containing all semantics to control the action
     * @return a dataset containing the aligned rows
     */
    Dataset<Row> getData(Variable variable, DatasetAggregationProperties properties);

    /**
     * Returns a dataset where each row contains an aligned entity data point on timestamp, extracted from a provided driving dataset.
     * If the driving timestamp matches an actual data row timestamp, that matching value will be assigned to the aligned entry.
     * Otherwise, the process will try to identify the closest previous actual value and assign this one instead (repeat value).
     *
     * @param entity     an instance of {@link Entity} pointing to raw data for the extraction
     * @param properties the aggregation properties containing all semantics to control the action
     * @return a dataset containing the aligned rows
     */
    Dataset<Row> getData(Entity entity, DatasetAggregationProperties properties);
}
