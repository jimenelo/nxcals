package cern.nxcals.api.custom.service;

import cern.nxcals.api.custom.service.extraction.ExtractionProperties;
import cern.nxcals.api.custom.service.extraction.LookupStrategy;
import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.extraction.data.builders.DataQuery;
import cern.nxcals.api.extraction.data.builders.DevicePropertyDataQuery;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

/**
 * Represents a service for data extraction actions, as configured by the provided properties.
 * Actions defined on this service operate on top of raw data that are directly extractable via
 * {@link DataQuery} or {@link DevicePropertyDataQuery}.
 *
 * @see Variable
 * @see Entity
 * @see ExtractionProperties
 */
public interface ExtractionService {

    /**
     * Returns a {@link Dataset dataset} based on the provided {@link Variable variable},
     * controlled by the given {@link ExtractionProperties extraction properties}.
     * This action extracts variable data for a given time window, enhanced by a configurable {@link LookupStrategy lookup strategy}.
     *
     * @param variable   an instance of {@link Variable} pointing to raw data for the extraction
     * @param properties the {@link ExtractionProperties extraction properties} containing all semantics to control the action
     * @return a dataset of the extracted rows
     */
    Dataset<Row> getData(Variable variable, ExtractionProperties properties);

    /**
     * Returns a {@link Dataset} based on the provided {@link Entity entity},
     * controlled by the given {@link ExtractionProperties extraction properties}.
     * This action extracts entity data for a given time window, enhanced by a configurable {@link LookupStrategy lookup strategy}.
     *
     * @param entity     an instance of {@link Entity} pointing to raw data for the extraction
     * @param properties the {@link ExtractionProperties extraction properties} containing all semantics to control the action
     * @return a dataset of the extracted rows
     */
    Dataset<Row> getData(Entity entity, ExtractionProperties properties);

}
