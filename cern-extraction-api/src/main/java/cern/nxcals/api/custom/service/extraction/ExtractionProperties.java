package cern.nxcals.api.custom.service.extraction;

import cern.nxcals.api.domain.TimeWindow;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import org.apache.commons.lang3.Validate;

import java.time.Instant;

@Value
@Builder
@SuppressWarnings("squid:S1170")
public final class ExtractionProperties {
    @NonNull
    private final TimeWindow timeWindow;

    @NonNull
    @Builder.Default
    private final LookupStrategy lookupStrategy = LookupStrategy.NONE;

    public static class ExtractionPropertiesBuilder {
        public ExtractionPropertiesBuilder timeWindow(@NonNull Instant startTime, @NonNull Instant endTime) {
            Validate.isTrue(startTime.isBefore(endTime), "Invalid time window! Start time must be before end time.");
            this.timeWindow = TimeWindow.between(startTime, endTime);
            return this;
        }
    }

}
