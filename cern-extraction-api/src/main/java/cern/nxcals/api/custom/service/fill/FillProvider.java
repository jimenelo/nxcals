package cern.nxcals.api.custom.service.fill;

import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.extraction.data.builders.DataQuery;
import cern.nxcals.api.utils.TimeUtils;
import com.google.common.annotations.VisibleForTesting;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import java.time.Duration;
import java.time.Instant;
import java.util.Collections;
import java.util.EnumMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

import static cern.nxcals.api.custom.service.fill.FillUtils.TIME_WINDOW_COMPARATOR;
import static cern.nxcals.common.SystemFields.NXC_EXTR_TIMESTAMP;
import static cern.nxcals.common.SystemFields.NXC_EXTR_VALUE;
import static java.util.stream.Collectors.toMap;

@RequiredArgsConstructor
class FillProvider {
    static final Instant FIRST_FILL = TimeUtils.getInstantFromString("2009-11-27 00:00:00.000");
    private static final String SYSTEM = "CMW";

    // Fill numbers of 1 and bellow are testing values and should be excluded from results
    private static final Predicate<FillNumber> VALID_FILL_NUMBER_PREDICATE = fn -> fn.getNumber() > 1;
    private static final Predicate<String> ALL_BMODES_PREDICATE = v -> true;
    private static final String TIMESTAMP = NXC_EXTR_TIMESTAMP.getValue();
    private static final String VALUE = NXC_EXTR_VALUE.getValue();

    private final Map<Source, TimeWindow> fetchedDataWindow = new EnumMap<>(Source.class);

    private final NavigableMap<Long, FillNumber> rawFills = new ConcurrentSkipListMap<>();
    private final NavigableMap<Long, String> rawModes = new ConcurrentSkipListMap<>();

    private final BeamModeAlignment alignment = new BeamModeAlignment();

    @NonNull
    private final Supplier<SparkSession> sessionSupplier;

    public NavigableMap<TimeWindow, Integer> findFillsByRange(Instant startTime, Instant endTime) {
        load(startTime.minus(Source.FILL.getMargin()), endTime.plus(Source.FILL.getMargin()), Source.FILL, rawFills,
                VALID_FILL_NUMBER_PREDICATE, FillNumber::from);
        TreeMap<Long, Integer> converted = convertRawFills();
        return alignment.getFillsForTimeRange(transform(converted), TimeWindow.between(startTime, endTime));
    }

    public Map<TimeWindow, NavigableMap<TimeWindow, String>> findBeamModesByTimeWindows(Set<TimeWindow> timeWindows) {
        if (CollectionUtils.isEmpty(timeWindows)) {
            return Collections.emptyMap();
        }
        TimeWindow mergedTimeWindow = timeWindows.stream().reduce(TimeWindow::expand)
                .filter(tw -> !TimeWindow.infinite().equals(tw)).orElse(TimeWindow.empty());
        load(mergedTimeWindow.getStartTime().minus(Source.BEAM_MODE.getMargin()),
                mergedTimeWindow.getEndTime().plus(Source.BEAM_MODE.getMargin()), Source.BEAM_MODE, rawModes,
                ALL_BMODES_PREDICATE, row -> row.getAs(VALUE));
        NavigableMap<TimeWindow, String> beamModes = transform(rawModes);
        return timeWindows.stream().collect(
                toMap(Function.identity(), tw -> alignment.getBeamModesForTimeRange(beamModes, tw)));
    }

    private synchronized <T> void load(Instant from, Instant to, Source source, NavigableMap<Long, T> rawData,
            Predicate<T> predicate, Function<Row, T> extractor) {
        from = from.isAfter(FIRST_FILL) ? from : FIRST_FILL;
        to = to.isAfter(FIRST_FILL) ? to : FIRST_FILL;

        NavigableMap<Long, T> acc = new TreeMap<>();
        if (rawData.isEmpty()) {
            extractData(from, to, source).stream().filter(row -> predicate.test(extractor.apply(row)))
                    .forEach(row -> acc.put(row.getAs(TIMESTAMP), extractor.apply(row)));
        } else {
            TimeWindow cachedDataWindow = fetchedDataWindow.getOrDefault(source, TimeWindow.empty());
            TimeWindow searchTimeWindow = TimeWindow.between(from, to);
            if (cachedDataWindow.bounds(searchTimeWindow)) {
                return; //no need to search - data already in cache
            }
            extractData(from, TimeUtils.getInstantFromNanos(rawData.firstKey()), source).stream()
                    .filter(row -> predicate.test(extractor.apply(row)))
                    .forEach(row -> acc.put(row.getAs(TIMESTAMP), extractor.apply(row)));
            extractData(TimeUtils.getInstantFromNanos(rawData.lastKey()), to, source).stream()
                    .filter(row -> predicate.test(extractor.apply(row)))
                    .forEach(row -> acc.put(row.getAs(TIMESTAMP), extractor.apply(row)));
        }
        acc.forEach(rawData::putIfAbsent); //populate cache with new elements
        if (!rawData.isEmpty()) {
            long cacheStartTime = Math.min(TimeUtils.getNanosFromInstant(from), rawData.firstKey());
            fetchedDataWindow.put(source, TimeWindow.between(cacheStartTime, rawData.lastKey()));
        }
    }

    private <T> NavigableMap<TimeWindow, T> transform(NavigableMap<Long, T> input) {
        TreeMap<TimeWindow, T> transformed = new TreeMap<>(TIME_WINDOW_COMPARATOR);
        for (Pair<TimeWindow, T> pair : new PairIterable<>(input)) {
            transformed.put(pair.getLeft(), pair.getRight());
        }
        return transformed;
    }

    @VisibleForTesting
    List<Row> extractData(Instant from, Instant to, Source source) {
        if (!from.isBefore(to)) {
            return Collections.emptyList();
        }
        return DataQuery.builder(sessionSupplier.get()).byVariables().system(SYSTEM).startTime(from).endTime(to)
                .variable(source.getVariable()).build().collectAsList();
    }

    private TreeMap<Long, Integer> convertRawFills() {
        return rawFills.entrySet().stream()
                .collect(toMap(Map.Entry::getKey, e -> e.getValue().getNumber(), throwOnDuplicate(), TreeMap::new));
    }

    private BinaryOperator<Integer> throwOnDuplicate() {
        return (u, v) -> {
            throw new IllegalStateException("no duplicate keys allowed");
        };
    }

    @AllArgsConstructor
    @Getter
    enum Source {
        //@formatter:off
        FILL("HX:FILLN", Duration.ofDays(10)),
        BEAM_MODE("HX:BMODE", Duration.ofDays(5));
        //@formatter:on
        private final String variable;
        private final Duration margin;
    }

    private static class PairIterable<T> implements Iterable<Pair<TimeWindow, T>> {
        private final Iterator<Map.Entry<Long, T>> starts;
        private final Iterator<Map.Entry<Long, T>> ends;

        PairIterable(Map<Long, T> iterable) {
            starts = iterable.entrySet().iterator();
            ends = iterable.entrySet().iterator();
            if (ends.hasNext()) {
                ends.next();
            }
        }

        @Override
        public Iterator<Pair<TimeWindow, T>> iterator() {
            return new Iterator<Pair<TimeWindow, T>>() {
                @Override
                public boolean hasNext() {
                    return ends.hasNext();
                }

                @Override
                public Pair<TimeWindow, T> next() {
                    Map.Entry<Long, T> start = starts.next();
                    Map.Entry<Long, T> end = ends.next();
                    return Pair.of(TimeWindow.between(start.getKey(), end.getKey()), start.getValue());
                }
            };
        }
    }

    @Data
    private static class FillNumber {
        private final int number;

        static FillNumber of(long value) {
            return new FillNumber((int) value);
        }

        static FillNumber from(Row row) {
            Object value = row.getAs(VALUE);
            if (value instanceof Integer) {
                return new FillNumber((int) value);
            } else if (value instanceof Long) {
                return FillNumber.of((long) value);
            }
            throw new IllegalArgumentException("Fill number must be either long or int not " + value);
        }
    }
}
