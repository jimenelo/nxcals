package cern.nxcals.api.custom.service.fill;

import cern.nxcals.api.domain.TimeWindow;
import org.apache.commons.lang3.tuple.MutablePair;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Objects;
import java.util.TreeMap;

import static cern.nxcals.api.custom.service.fill.FillUtils.TIME_WINDOW_COMPARATOR;

/**
 * Traverse input and sequentially merge time windows for entries of matching fill number.
 * The goal here is to achieve a normalized Fill Number representation that spawns on top of a calculated range,
 * avoiding as many duplicate entries as possible.
 * The merge process is handled by invoking {@link TimeWindow#expand(TimeWindow)} method on adjacent matching entries.
 */
class FillProcessor {

    public NavigableMap<TimeWindow, Integer> process(NavigableMap<TimeWindow, Integer> rawFills) {
        Objects.requireNonNull(rawFills, "Raw fills to process must not be null!");
        Deque<MutablePair<TimeWindow, Integer>> entries = new ArrayDeque<>();
        for (Map.Entry<TimeWindow, Integer> entry : rawFills.entrySet()) {
            if (entries.isEmpty() || !entry.getValue().equals(entries.peek().getValue())) {
                entries.push(new MutablePair<>(entry.getKey(), entry.getValue()));
            } else {
                MutablePair<TimeWindow, Integer> peek = entries.peek();
                peek.setLeft(peek.getKey().expand(entry.getKey()));
            }
        }
        NavigableMap<TimeWindow, Integer> result = new TreeMap<>(TIME_WINDOW_COMPARATOR);
        entries.forEach(e -> result.put(e.getKey(), e.getValue()));
        return result;
    }

}
