package cern.nxcals.api.custom.service;

import cern.nxcals.api.custom.domain.FundamentalFilter;
import cern.nxcals.api.domain.TimeWindow;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

import java.util.Set;

public interface FundamentalService {

    /**
     * Return a {@link Dataset} for a given {@link TimeWindow time window} that contains data
     * for fundamental variables that are matching the provided set of {@link FundamentalFilter fundamental filters}.
     * Please, note that variables are obtained from each filter separately and then joined/unioned together.
     * I.e. variable will be included if it satisfies any of the provided filters.
     *
     * @param timeWindow         a {@link TimeWindow} that defines time bounds for the data search
     * @param fundamentalFilters a set of {@link FundamentalFilter} based on which fundamental variables are selected.
     * @return a {@link Dataset} with data for fundamental variables based on the fundamental filters
     */
    Dataset<Row> getAll(TimeWindow timeWindow, Set<FundamentalFilter> fundamentalFilters);

}
