package cern.nxcals.api.custom.domain.util;

import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.utils.TimeUtils;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import lombok.experimental.UtilityClass;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.functions;

import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Function;

@UtilityClass
public final class DataLookupUtils {
    private static final Duration BASE_LOOKUP_DELTA = Duration.ofDays(1);

    public static Optional<Instant> getAdjacentTimestampBefore(@NonNull Instant timestamp,
            @NonNull Function<TimeWindow, Dataset<Row>> dataProvider,
            @NonNull String timestampField, @NonNull Duration lookupDuration) {
        Instant upper = timestamp.minusNanos(1);
        Instant lower = upper.truncatedTo(ChronoUnit.DAYS);
        return getAdjacentTimestamp(timestamp, timestampField, dataProvider, lookupDuration, lower, upper,
                LookupType.BACKWARD);
    }

    public static Optional<Instant> getAdjacentTimestampAfter(@NonNull Instant timestamp,
            @NonNull Function<TimeWindow, Dataset<Row>> dataProvider,
            @NonNull String timestampField, @NonNull Duration lookupDuration) {
        Instant lower = timestamp.plusNanos(1);
        Instant upper = lower.truncatedTo(ChronoUnit.DAYS).plus(BASE_LOOKUP_DELTA);
        return getAdjacentTimestamp(timestamp, timestampField, dataProvider, lookupDuration,
                lower, upper, LookupType.FORWARD);
    }

    private static Optional<Instant> getAdjacentTimestamp(@NonNull Instant timestamp, @NonNull String timestampField,
            @NonNull Function<TimeWindow, Dataset<Row>> dataProvider, @NonNull Duration lookupDuration,
            @NonNull Instant lowerStart, @NonNull Instant upperStart, @NonNull LookupType lookupType) {
        Duration delta = BASE_LOOKUP_DELTA;
        Instant upper = upperStart;
        Instant lower = lowerStart;

        long beginTimestampNanos = TimeUtils.getNanosFromInstant(timestamp);
        Row timestampRow;
        do {
            Instant lookupWatermark = LookupType.BACKWARD.equals(lookupType) ? lower : upper;
            if (isOutsideLookupBounds(beginTimestampNanos, lookupWatermark, lookupDuration)) {
                return Optional.empty();
            }
            timestampRow = dataProvider.apply(TimeWindow.between(lower, upper))
                    .agg(lookupType.getAggFunction().apply(timestampField)).first();

            upper = lower;
            lower = lookupType.getStepFunction().apply(upper, delta);

            delta = delta.multipliedBy(2); //progressively enlarge lookup step
        } while (timestampRow.get(0) == null);
        return Optional.of(TimeUtils.getInstantFromNanos(timestampRow.getLong(0)));
    }

    private static boolean isOutsideLookupBounds(long beginTimestampNanos,
            Instant lookupWatermark, Duration lookupDuration) {
        return Math.abs(TimeUtils.getNanosFromInstant(lookupWatermark) - beginTimestampNanos) > lookupDuration
                .toNanos();
    }

    @AllArgsConstructor
    @Getter
    private enum LookupType {
        BACKWARD(functions::max, Instant::minus),
        FORWARD(functions::min, Instant::plus);

        private final Function<String, Column> aggFunction;
        private final BiFunction<Instant, Duration, Instant> stepFunction;
    }

}
