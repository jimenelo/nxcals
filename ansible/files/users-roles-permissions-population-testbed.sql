delete from roles_permissions;
delete from users_roles;
delete from users;
delete from roles;
delete from permissions;
delete from realms;

insert into realms(ID, REALM_NAME) values (1, 'CERN.CH');

insert into users (ID, REALM_ID, USER_NAME) values (1, 1, 'acclog');
insert into users (ID, REALM_ID, USER_NAME) values (2, 1,'gavgitid');
insert into users (ID, REALM_ID, USER_NAME) values (3, 1,'jwozniak');
insert into users (ID, REALM_ID, USER_NAME) values (4, 1,'mamajews');
insert into users (ID, REALM_ID, USER_NAME) values (5, 1,'msobiesz');
insert into users (ID, REALM_ID, USER_NAME) values (6, 1,'ntsvetko');
insert into users (ID, REALM_ID, USER_NAME) values (7, 1,'psowinsk');
insert into users (ID, REALM_ID, USER_NAME) values (8, 1,'timartin');

insert into users (ID, REALM_ID, USER_NAME) values (10, 1, 'mpocwier');
insert into users (ID, REALM_ID, USER_NAME) values (11, 1, 'garnierj');
insert into users (ID, REALM_ID, USER_NAME) values (12, 1, 'mgalilee');
insert into users (ID, REALM_ID, USER_NAME) values (13, 1, 'maosinsk');
insert into users (ID, REALM_ID, USER_NAME) values (14, 1, 'astanisz');
insert into users (ID, REALM_ID, USER_NAME) values (15, 1, 'thbuffet');
insert into users (ID, REALM_ID, USER_NAME) values (16, 1, 'cfreisch');
insert into users (ID, REALM_ID, USER_NAME) values (17, 1, 'fbogyai');
insert into users (ID, REALM_ID, USER_NAME) values (18, 1, 'abalatso');
insert into users (ID, REALM_ID, USER_NAME) values (19, 1, 'mpeops');
insert into users (ID, REALM_ID, USER_NAME) values (20, 1, 'mpesoft');
insert into users (ID, REALM_ID, USER_NAME) values (35, 1, 'mgabriel');

insert into roles (ID, ROLE_NAME) values (1, 'ROLE_ADMIN');
insert into roles (ID, ROLE_NAME) values (2, 'ROLE_PM_USER');

insert into PERMISSIONS (ID, PERMISSION_NAME) values (1, 'TEST-CMW:READ');
insert into PERMISSIONS (ID, PERMISSION_NAME) values (2, 'TEST-CMW:WRITE');

insert into PERMISSIONS (ID, PERMISSION_NAME) values (3, 'MOCK-SYSTEM:READ');
insert into PERMISSIONS (ID, PERMISSION_NAME) values (4, 'MOCK-SYSTEM:WRITE');

insert into PERMISSIONS (ID, PERMISSION_NAME) values (5, 'CMW:READ');
insert into PERMISSIONS (ID, PERMISSION_NAME) values (6, 'CMW:WRITE');

insert into PERMISSIONS (ID, PERMISSION_NAME) values (7, 'PM:READ');
insert into PERMISSIONS (ID, PERMISSION_NAME) values (8, 'PM:WRITE');

insert into PERMISSIONS (ID, PERMISSION_NAME) values (9, 'VARIABLE:READ');
insert into PERMISSIONS (ID, PERMISSION_NAME) values (10, 'VARIABLE:WRITE');

insert into PERMISSIONS (ID, PERMISSION_NAME) values (11, 'CMW-TEST:READ');
insert into PERMISSIONS (ID, PERMISSION_NAME) values (12, 'CMW-TEST:WRITE');

insert into PERMISSIONS (ID, PERMISSION_NAME) values (13, 'CMW-STAGING:READ');
insert into PERMISSIONS (ID, PERMISSION_NAME) values (14, 'CMW-STAGING:WRITE');

insert into PERMISSIONS (ID, PERMISSION_NAME) values (15, 'CMW-MIG-DEV-JWOZNIAK:READ');
insert into PERMISSIONS (ID, PERMISSION_NAME) values (16, 'CMW-MIG-DEV-JWOZNIAK:WRITE');

insert into PERMISSIONS (ID, PERMISSION_NAME) values (17, 'CMW-MIG-DEV-GAVGITID:READ');
insert into PERMISSIONS (ID, PERMISSION_NAME) values (18, 'CMW-MIG-DEV-GAVGITID:WRITE');

insert into PERMISSIONS (ID, PERMISSION_NAME) values (19, 'CMW-MIG-DEV-MAMAJEWS:READ');
insert into PERMISSIONS (ID, PERMISSION_NAME) values (20, 'CMW-MIG-DEV-MAMAJEWS:WRITE');

insert into PERMISSIONS (ID, PERMISSION_NAME) values (21, 'CMW-MIG-DEV-MSOBIESZ:READ');
insert into PERMISSIONS (ID, PERMISSION_NAME) values (22, 'CMW-MIG-DEV-MSOBIESZ:WRITE');

insert into PERMISSIONS (ID, PERMISSION_NAME) values (23, 'CMW-MIG-DEV-NTSVETKO:READ');
insert into PERMISSIONS (ID, PERMISSION_NAME) values (24, 'CMW-MIG-DEV-NTSVETKO:WRITE');

insert into PERMISSIONS (ID, PERMISSION_NAME) values (25, 'CMW-MIG-DEV-PSOWINSK:READ');
insert into PERMISSIONS (ID, PERMISSION_NAME) values (26, 'CMW-MIG-DEV-PSOWINSK:WRITE');

insert into PERMISSIONS (ID, PERMISSION_NAME) values (27, 'CMW-MIG-DEV-TIMARTIN:READ');
insert into PERMISSIONS (ID, PERMISSION_NAME) values (28, 'CMW-MIG-DEV-TIMARTIN:WRITE');

insert into PERMISSIONS (ID, PERMISSION_NAME) values (31, 'CMW-MIG-TEST:READ');
insert into PERMISSIONS (ID, PERMISSION_NAME) values (32, 'CMW-MIG-TEST:WRITE');

insert into PERMISSIONS (ID, PERMISSION_NAME) values (33, 'CMW-MIG-STAGING:READ');
insert into PERMISSIONS (ID, PERMISSION_NAME) values (34, 'CMW-MIG-STAGING:WRITE');

insert into PERMISSIONS (ID, PERMISSION_NAME) values (35, 'CMW-EXAMPLE:READ');
insert into PERMISSIONS (ID, PERMISSION_NAME) values (36, 'CMW-EXAMPLE:WRITE');

insert into PERMISSIONS (ID, PERMISSION_NAME) values (37, 'PM-TEST:READ');
insert into PERMISSIONS (ID, PERMISSION_NAME) values (38, 'PM-TEST:WRITE');

insert into PERMISSIONS (ID, PERMISSION_NAME) values (59, 'PM-TEST1:READ');
insert into PERMISSIONS (ID, PERMISSION_NAME) values (60, 'PM-TEST1:WRITE');

insert into PERMISSIONS (ID, PERMISSION_NAME) values (63, 'PM-ANALYSIS:READ');
insert into PERMISSIONS (ID, PERMISSION_NAME) values (64, 'PM-ANALYSIS:WRITE');

insert into PERMISSIONS (ID, PERMISSION_NAME) values (65, 'PM-EVENTS:READ');
insert into PERMISSIONS (ID, PERMISSION_NAME) values (66, 'PM-EVENTS:WRITE');

insert into PERMISSIONS (ID, PERMISSION_NAME) values (67, 'HIERARCHY:READ');
insert into PERMISSIONS (ID, PERMISSION_NAME) values (68, 'HIERARCHY:WRITE');

insert into PERMISSIONS (ID, PERMISSION_NAME) values (69, 'GROUP:READ');
insert into PERMISSIONS (ID, PERMISSION_NAME) values (70, 'GROUP:WRITE');

insert into PERMISSIONS (ID, PERMISSION_NAME) values (71, 'CERN:READ');
insert into PERMISSIONS (ID, PERMISSION_NAME) values (72, 'CERN:WRITE');


insert into ROLES_PERMISSIONS (ROLE_ID, PERMISSION_ID)
  select 1, id from permissions;

insert into ROLES_PERMISSIONS (ROLE_ID, PERMISSION_ID) VALUES (2, 7);
insert into ROLES_PERMISSIONS (ROLE_ID, PERMISSION_ID) VALUES (2, 8);
insert into ROLES_PERMISSIONS (ROLE_ID, PERMISSION_ID) VALUES (2, 37);
insert into ROLES_PERMISSIONS (ROLE_ID, PERMISSION_ID) VALUES (2, 38);
insert into ROLES_PERMISSIONS (ROLE_ID, PERMISSION_ID) VALUES (2, 59);
insert into ROLES_PERMISSIONS (ROLE_ID, PERMISSION_ID) VALUES (2, 60);
insert into ROLES_PERMISSIONS (ROLE_ID, PERMISSION_ID) VALUES (2, 63);
insert into ROLES_PERMISSIONS (ROLE_ID, PERMISSION_ID) VALUES (2, 64);
insert into ROLES_PERMISSIONS (ROLE_ID, PERMISSION_ID) VALUES (2, 65);
insert into ROLES_PERMISSIONS (ROLE_ID, PERMISSION_ID) VALUES (2, 66);
insert into ROLES_PERMISSIONS (ROLE_ID, PERMISSION_ID) VALUES (2, 67);
insert into ROLES_PERMISSIONS (ROLE_ID, PERMISSION_ID) VALUES (2, 68);
insert into ROLES_PERMISSIONS (ROLE_ID, PERMISSION_ID) VALUES (2, 69);
insert into ROLES_PERMISSIONS (ROLE_ID, PERMISSION_ID) VALUES (2, 70);
insert into ROLES_PERMISSIONS (ROLE_ID, PERMISSION_ID) VALUES (2, 71);
insert into ROLES_PERMISSIONS (ROLE_ID, PERMISSION_ID) VALUES (2, 72);


insert into USERS_ROLES (USER_ID, ROLE_ID) VALUES (1, 1);
insert into USERS_ROLES (USER_ID, ROLE_ID) VALUES (2, 1);
insert into USERS_ROLES (USER_ID, ROLE_ID) VALUES (3, 1);
insert into USERS_ROLES (USER_ID, ROLE_ID) VALUES (4, 1);
insert into USERS_ROLES (USER_ID, ROLE_ID) VALUES (5, 1);
insert into USERS_ROLES (USER_ID, ROLE_ID) VALUES (6, 1);
insert into USERS_ROLES (USER_ID, ROLE_ID) VALUES (7, 1);
insert into USERS_ROLES (USER_ID, ROLE_ID) VALUES (8, 1);

insert into USERS_ROLES (USER_ID, ROLE_ID) VALUES (10, 2);
insert into USERS_ROLES (USER_ID, ROLE_ID) VALUES (11, 2);
insert into USERS_ROLES (USER_ID, ROLE_ID) VALUES (12, 2);
insert into USERS_ROLES (USER_ID, ROLE_ID) VALUES (13, 2);
insert into USERS_ROLES (USER_ID, ROLE_ID) VALUES (14, 2);
insert into USERS_ROLES (USER_ID, ROLE_ID) VALUES (15, 2);
insert into USERS_ROLES (USER_ID, ROLE_ID) VALUES (16, 2);
insert into USERS_ROLES (USER_ID, ROLE_ID) VALUES (17, 2);
insert into USERS_ROLES (USER_ID, ROLE_ID) VALUES (18, 2);
insert into USERS_ROLES (USER_ID, ROLE_ID) VALUES (19, 2);
insert into USERS_ROLES (USER_ID, ROLE_ID) VALUES (20, 2);

insert into USERS_ROLES (USER_ID, ROLE_ID) VALUES (35, 4);
