#!/usr/bin/env bash

ANSIBLE_OPTIONS=""
INPUT_FILE=ansible-parallel-input.txt
OUTPUT_LOGS_DIR=/tmp

while [[ "$#" -gt 0 ]]; do
    case $1 in
        --parallel) INPUT_FILE="$2"; shift ;;
        --output-dir) OUTPUT_LOGS_DIR="$2"; shift ;;
        -h|--help) echo "Usage: ansible-parallel.sh --parallel <file-with-parallel-playbooks> --output-dir <output-dir-for-logs> < <other-ansible-options>";
        echo "Ansible-playbook help: ";
        ansible-playbook --help;
        exit 0 ;;
        *) ANSIBLE_OPTIONS="$ANSIBLE_OPTIONS $1"; ;;
    esac
    shift
done

echo $ANSIBLE_OPTIONS

#Killing all process of a given group on exit...
trap "exit" INT TERM ERR
trap "kill 0" EXIT


while IFS= read -r line
do
  echo "Playbooks to run in parallel: $line"

  for playbook in $(echo $line | tr " " "\n")
  do
    echo "Procesing $playbook in parallel, please check logs in $OUTPUT_LOGS_DIR/${playbook}.log"
    ansible-playbook $ANSIBLE_OPTIONS "$playbook" &> ${OUTPUT_LOGS_DIR}/${playbook}.log &
  done
  wait;
  echo "Done processing playbooks: $line"
done < "$INPUT_FILE"


echo "Finished!";

grep "PLAY RECAP" -A 100 ${OUTPUT_LOGS_DIR}/*.yml.log


