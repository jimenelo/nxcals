#!/usr/bin/env bash



for i in `seq 1 5`; do
    for host in `seq 7 10 `; do
        num=$(($host-6))
        echo $i $host $num
        scp ../kafka-etl/build/libs/nxcals-kafka-etl-0.1.137-SNAPSHOT.jar acclog@cs-ccr-oa21ilo${host}.cern.ch:/opt/nxcals/nxcals-kafka-etl-hbase-${i}-${num}-0.1.137-SNAPSHOT/lib/.

    done
done


for i in `seq 1 5`; do
    for host in `seq 4 7 `; do
        num=$(($host+1))
        echo $i $host $num
        scp ../kafka-etl/build/libs/nxcals-kafka-etl-0.1.137-SNAPSHOT.jar acclog@ithdp212${host}.cern.ch:/opt/nxcals/nxcals-kafka-etl-hbase-${i}-${num}-0.1.137-SNAPSHOT/lib/.

    done
done
