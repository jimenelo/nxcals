#
# Licensed to the Apache Software Foundation (ASF) under one or more
# contributor license agreements.  See the NOTICE file distributed with
# this work for additional information regarding copyright ownership.
# The ASF licenses this file to You under the Apache License, Version 2.0
# (the "License"); you may not use this file except in compliance with
# the License.  You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Default system properties included when running spark-submit.
# This is useful for setting default environmental settings.

# Example:
# spark.master                     spark://master:7077
# spark.eventLog.enabled           true
# spark.eventLog.dir               hdfs://namenode:8021/directory
# spark.serializer                 org.apache.spark.serializer.KryoSerializer
spark.driver.memory              1500m
# spark.executor.extraJavaOptions  -XX:+PrintGCDetails -Dkey=value -Dnumbers="one two three"
#


#####################################################
# Spark defaults                                    #
#####################################################

spark.driver.extraJavaOptions   {{spark_driver_java_opts}}

spark.sql.caseSensitive true
spark.sql.execution.arrow.pyspark.enabled true
spark.debug.maxToStringFields 50

spark.eventLog.enabled  {{spark_hdfs_log_enabled}}
spark.eventLog.dir  {{spark_hdfs_log_dir}}
spark.history.fs.logDirectory   {{spark_hdfs_log_dir}}

spark.history.fs.cleaner.enabled    {{spark_hdfs_log_enabled}}
spark.history.fs.cleaner.maxAge     {{spark_history_max_age}}

spark.driver.port                       5001
spark.blockManager.port                 5101
spark.ui.port                           5201
spark.port.maxRetries                   99

spark.executor.instances 4
spark.executor.cores 4
spark.executor.memory 6g

spark.jars {% for spark_jar in spark_nxcals_jars.stdout_lines %}{{spark_nxcals_jars_dir_name}}/{{spark_jar}}{%- if not loop.last %},{% endif -%}{% endfor %}

#####################################################
# Spark YARN mode properties                        #
#####################################################

spark.yarn.appMasterEnv.JAVA_HOME   {{spark_java_home}}
spark.executorEnv.JAVA_HOME {{spark_java_home}}

spark.yarn.dist.archives # Leave blank as it will be dynamically filled when the script spark-env.sh will be launched

spark.yarn.jars {{spark_yarn_jar_locations_property}}


spark.yarn.am.extraLibraryPath    {{spark_yarn_native_hadoop_libs}}
spark.executor.extraLibraryPath   {{spark_yarn_native_hadoop_libs}}

spark.yarn.historyServer.address        {{hadoop_history_server_address}}
spark.kerberos.access.hadoopFileSystems {{hdfs_namenode}}

spark.task.maxDirectResultSize: {{spark_task_max_direct_result_size}}
spark.driver.maxResultSize: {{spark_driver_max_result_size}}

#Speed up options
spark.locality.wait 0ms
spark.locality.wait.node 0
spark.sql.parquet.enableNestedColumnVectorizedReader true

#Fix for corruption of shuffle NXCALS-7507
spark.file.transferTo false

# You can uncomment the following spark properties to directly
# configure spark deploy mode and master type.
#
# NOTE: this is the configuration point of view, equivalent to following spark command:
# $ /path/to/spark/bin/spark-shell --master yarn
#
# spark.master

{%if with_kerberos_properties %}
spark.kerberos.keytab   {{kerberos_keytab_location}}
spark.kerberos.principal    {{kerberos_principal}}
{% else %}
# You can uncomment the following kerberos properties to directly
# configure spark against a kerberos keytab/principal entry

# spark.yarn.keytab   <file_location>
# spark.yarn.principal <login@CERN.CH>
{% endif %}
