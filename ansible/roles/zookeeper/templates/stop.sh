#!/bin/bash

export JAVA_HOME={{java_path_to_alias}}

export ZOOPIDFILE={{install_dir}}/{{module}}-{{version}}/application.pid
export ZOOCFGDIR={{install_dir}}/{{module}}-{{version}}/config

./bin/zkServer.sh stop
