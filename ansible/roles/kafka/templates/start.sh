#!/bin/bash

export JAVA_HOME={{java_path_to_alias}}

export KAFKA_HEAP_OPTS="{{ kafka_memory }}"
export KAFKA_OPTS="$KAFKA_OPTS {{ java_opts }} -XX:+CrashOnOutOfMemoryError -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=./log/ -XX:OnOutOfMemoryError='{{install_dir}}/{{instance}}/oom_handler.sh'"

bin/kafka-server-start.sh -daemon config/server.properties --override broker.id={{kafka_broker_id}}

sleep 10
PIDS=$(ps ax | grep -i 'kafka\.Kafka' | grep java | grep -v grep | awk '{print $1}')
echo $PIDS > application.pid

