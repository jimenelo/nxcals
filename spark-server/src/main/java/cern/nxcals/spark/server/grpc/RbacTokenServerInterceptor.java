package cern.nxcals.spark.server.grpc;

import cern.rbac.common.RbaToken;
import cern.rbac.common.RbacConfiguration;
import cern.rbac.common.TokenFormatException;
import com.google.common.annotations.VisibleForTesting;
import io.grpc.Context;
import io.grpc.Contexts;
import io.grpc.Metadata;
import io.grpc.ServerCall;
import io.grpc.ServerCall.Listener;
import io.grpc.ServerCallHandler;
import io.grpc.ServerInterceptor;
import io.grpc.Status;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.nio.ByteBuffer;

@Slf4j
@RequiredArgsConstructor
public class RbacTokenServerInterceptor implements ServerInterceptor {
    static final Context.Key<RbaToken> AUTH_TOKEN = Context.key("auth_token");

    @VisibleForTesting
    @Setter(AccessLevel.PACKAGE)
    private boolean validateToken = true;

    @Override
    public <R, S> Listener<R> interceptCall(ServerCall<R, S> call, Metadata headers,
            ServerCallHandler<R, S> next) {
        try {
            byte[] encodedToken = headers.get(cern.nxcals.common.grpc.RbacTokenInterceptor.TOKEN_KEY);
            if (encodedToken == null) {
                throw new IllegalArgumentException("Cannot find RBAC token in request");
            }
            RbaToken rbaToken = RbaToken
                    .parseAndValidate(ByteBuffer.wrap(encodedToken), RbacConfiguration.getCurrent());
            if (validateToken && !rbaToken.isValid()) {
                throw new IllegalArgumentException("Invalid RBAC token found " + rbaToken);
            }
            log.debug("Found rbac token {}", rbaToken);
            Context context = Context.current().withValue(AUTH_TOKEN, rbaToken);
            return Contexts.interceptCall(context, call, headers, next);
        } catch (TokenFormatException | IllegalArgumentException e) {
            throw Status.INVALID_ARGUMENT.withCause(e).withDescription(e.getMessage()).asRuntimeException();
        }
    }

}
