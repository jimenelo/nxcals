package cern.nxcals.spark.server.grpc;

import cern.nxcals.api.extraction.thin.AvroData;
import cern.nxcals.api.extraction.thin.AvroQuery;
import cern.nxcals.api.extraction.thin.ExtractionServiceGrpc;
import cern.nxcals.spark.server.service.QueryExecutor;
import cern.rbac.common.RbaToken;
import com.google.common.annotations.VisibleForTesting;
import io.grpc.stub.StreamObserver;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.lognet.springboot.grpc.GRpcService;

import java.util.function.Supplier;

@RequiredArgsConstructor
@GRpcService
@Slf4j
public class ExtractionServiceGrpcController extends ExtractionServiceGrpc.ExtractionServiceImplBase {
    private final QueryExecutor queryExecutor;

    @VisibleForTesting
    @Setter(AccessLevel.PACKAGE)
    Supplier<RbaToken> tokenSupplier = RbacTokenServerInterceptor.AUTH_TOKEN::get;

    @Override
    public void query(AvroQuery request, StreamObserver<AvroData> responseObserver) {
        log.debug("Start client request={}", request);
        GrpcControllerProcessor.process(() -> queryExecutor.query(request), responseObserver, tokenSupplier);
        log.debug("Finished client request={}", request);
    }
}
