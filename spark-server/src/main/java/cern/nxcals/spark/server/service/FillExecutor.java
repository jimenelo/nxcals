package cern.nxcals.spark.server.service;

import cern.nxcals.api.custom.converters.FillConverter;
import cern.nxcals.api.custom.domain.Fill;
import cern.nxcals.api.custom.service.FillService;
import cern.nxcals.api.extraction.thin.FillData;
import cern.nxcals.api.extraction.thin.TimeWindow;
import cern.nxcals.api.utils.TimeUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class FillExecutor {
    private final FillService fillService;

    public FillData findFill(int fillNumber) {

        Optional<Fill> fill = this.fillService.findFill(fillNumber);
        if (fill.isPresent()) {
            return FillConverter.toFillData(fill.get());
        }
        return FillData.newBuilder().build();
    }

    public List<FillData> findFills(TimeWindow timeWindow ) {
        List<Fill> fills = fillService.findFills(
                TimeUtils.getInstantFromNanos(timeWindow.getStartTime()),
                TimeUtils.getInstantFromNanos(timeWindow.getEndTime()));

        return FillConverter.toFillsData(fills);
    }

    public FillData getLastCompleted() {
        Optional<Fill> fill = this.fillService.getLastCompleted();
        return fill.map(FillConverter::toFillData).orElseGet(() -> FillData.newBuilder().build());
    }
}