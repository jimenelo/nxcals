package cern.nxcals.spark.server.service;

import cern.nxcals.api.extraction.data.builders.DataQuery;
import cern.nxcals.api.extraction.data.builders.DevicePropertyDataQuery;
import lombok.SneakyThrows;
import org.apache.spark.SparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.script.Bindings;
import javax.script.ScriptEngine;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ScriptExecutorTest {
    @Mock
    private ScriptEngine engine;
    @Mock
    private SparkSession sparkSession;

    @Mock
    private SparkContext sparkContext;

    @Mock
    private Bindings bindings;
    @Mock
    private Dataset<Row> dataset;


    @SneakyThrows
    @Test
    public void shouldThrowIllegalArgumentException() {
        //given
        ScriptExecutor scriptExecutor = new ScriptExecutor(engine, sparkSession);
        when(engine.createBindings()).thenReturn(bindings);
        when(sparkSession.sparkContext()).thenReturn(sparkContext);

        when(engine.eval(argThat((String s) -> s.contains(DataQuery.class.getCanonicalName())),
                (Bindings) argThat(b -> true))).thenThrow(new RuntimeException("Test"));
        //when
        assertThrows(IllegalArgumentException.class, () -> scriptExecutor.executeForDataset("DataQuery"));

        //then exception
    }

    @SneakyThrows
    @Test
    public void shouldReplaceDataQueryWithPackage() {
        //given
        ScriptExecutor scriptExecutor = new ScriptExecutor(engine, sparkSession);
        when(engine.createBindings()).thenReturn(bindings);
        when(sparkSession.sparkContext()).thenReturn(sparkContext);

        when(engine.eval(argThat((String s) -> s.contains(DataQuery.class.getCanonicalName())),
                (Bindings) argThat(b -> true))).thenReturn(dataset);
        //when
        Dataset<Row> rowDataset = scriptExecutor.executeForDataset("DataQuery");

        //then
        assertEquals(rowDataset,dataset);

    }

    @SneakyThrows
    @Test
    public void shouldReplaceDevicePropertyDataQueryWithPackage() {
        //given
        ScriptExecutor scriptExecutor = new ScriptExecutor(engine, sparkSession);
        when(sparkSession.sparkContext()).thenReturn(sparkContext);

        when(engine.createBindings()).thenReturn(bindings);
        when(engine.eval(argThat((String s) -> s.contains(DevicePropertyDataQuery.class.getCanonicalName())),
                (Bindings) argThat(b -> true))).thenReturn(dataset);

        //when
        Dataset<Row> rowDataset = scriptExecutor.executeForDataset("DevicePropertyDataQuery DevicePropertyDataQuery");

        //then
        assertEquals(dataset,rowDataset);
    }

    @Test
    public void shouldExecuteErrorHandler() {
        //given
        ScriptExecutor scriptExecutor = new ScriptExecutor(engine, sparkSession);
        Runnable handler = mock(Runnable.class);
        Mockito.doThrow(new InternalError()).when(handler).run();

        scriptExecutor.setErrorHandler(handler);
        when(sparkSession.sparkContext()).thenReturn(sparkContext);
        when(sparkContext.isStopped()).thenReturn(true);
        //when
        assertThrows(InternalError.class, () ->
                scriptExecutor.executeForDataset("DevicePropertyDataQuery DevicePropertyDataQuery"));

        //then exception from handler

    }

}
