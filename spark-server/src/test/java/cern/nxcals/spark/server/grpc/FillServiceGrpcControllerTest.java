package cern.nxcals.spark.server.grpc;

import cern.nxcals.api.extraction.thin.FillData;
import cern.nxcals.api.extraction.thin.FillQueryByNumber;
import cern.nxcals.api.extraction.thin.FillQueryByWindow;
import cern.nxcals.api.extraction.thin.TimeWindow;
import cern.nxcals.spark.server.service.FillExecutor;
import cern.rbac.common.RbaToken;
import com.google.protobuf.Empty;
import io.grpc.stub.StreamObserver;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class FillServiceGrpcControllerTest {
    @Mock
    private FillExecutor fillExecutor;
    @Mock
    private StreamObserver<FillData> fillDataObserver;

    @Test
    public void shouldFindFill() {
        //given
        FillServiceGrpcController controller = getFillServiceGrpcController();

        FillQueryByNumber request = FillQueryByNumber.newBuilder().build();
        FillData response = FillData.newBuilder().build();

        when(fillExecutor.findFill(0)).thenReturn(response);

        //when
        controller.findFill(request, fillDataObserver);

        //then
        verify(fillDataObserver, times(1)).onNext(response);
        verify(fillDataObserver, times(1)).onCompleted();
        verify(fillDataObserver, times(0)).onError(argThat(e -> true));
    }

    @Test
    public void shouldFindFills() {
        //given
        FillServiceGrpcController controller = getFillServiceGrpcController();

        FillQueryByWindow request = FillQueryByWindow.newBuilder().build();

        List<FillData> response = new ArrayList<>();
        response.add( FillData.newBuilder().build());

        when(fillExecutor.findFills(TimeWindow.newBuilder().build())).thenReturn(response);

        //when
        controller.findFills(request, fillDataObserver);

        //then
        verify(fillDataObserver, times(1)).onNext(response.get(0));
        verify(fillDataObserver, times(1)).onCompleted();
        verify(fillDataObserver, times(0)).onError(argThat(e -> true));
    }

    @Test
    public void shouldGetLastCompleted() {
        //given
        FillServiceGrpcController controller = getFillServiceGrpcController();

        FillData response = FillData.newBuilder().build();

        when(fillExecutor.getLastCompleted()).thenReturn(response);

        //when
        controller.getLastCompleted(Empty.newBuilder().build(), fillDataObserver);

        //then
        verify(fillDataObserver, times(1)).onNext(response);
        verify(fillDataObserver, times(1)).onCompleted();
        verify(fillDataObserver, times(0)).onError(argThat(e -> true));

    }

    private FillServiceGrpcController getFillServiceGrpcController() {
        FillServiceGrpcController controller = new FillServiceGrpcController(fillExecutor);
        controller.setTokenSupplier(() -> RbaToken.EMPTY_TOKEN);
        return controller;
    }
}