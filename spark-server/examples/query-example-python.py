# requires grpio & grpcio-tools, fastavro or avro installed with pip in the venv

import grpc
from extraction_service_pb2 import *
from extraction_service_pb2_grpc import *
from io import BytesIO
from fastavro import reader

# Create connection & client
channel = grpc.insecure_channel('localhost:9999')
client = ExtractionServiceStub(channel)

# Create request
request = AvroQuery(compression='BZIP2', script='DataQuery.builder(sparkSession).byVariables().system("CMW").startTime("2020-06-15 00:00:00.999999").endTime("2020-06-15 00:10:00.888888").variable("SPS:NXCALS_FUNDAMENTAL").build().sort("nxcals_timestamp")')

# Query data
response = client.query(request)

# Convert bytes to avro records
for record in reader(BytesIO(response.avro_bytes)):
   #Print record
   print(record)

