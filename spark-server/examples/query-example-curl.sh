#!/usr/bin/env bash
HOST=localhost:10000
curl  -d "{ 'script': 'DataQuery.builder(sparkSession).byVariables().system(\"CMW\").startTime(\"2020-06-15 00:00:00.999999\").endTime(\"2020-06-15 00:10:00.888888\").variable(\"SPS:NXCALS_FUNDAMENTAL\").build().sort(\"nxcals_timestamp\")' }" -H "Content-Type: application/json;"  -X POST http://$HOST/api/execute/avro2
