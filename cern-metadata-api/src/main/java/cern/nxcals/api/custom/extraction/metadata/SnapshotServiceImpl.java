package cern.nxcals.api.custom.extraction.metadata;

import cern.nxcals.api.custom.domain.GroupType;
import cern.nxcals.api.custom.domain.Snapshot;
import cern.nxcals.api.custom.domain.SnapshotProperty;
import cern.nxcals.api.custom.domain.util.FundamentalMapper;
import cern.nxcals.api.custom.extraction.metadata.queries.Snapshots;
import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.Group;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.extraction.metadata.GroupService;
import cern.nxcals.api.extraction.metadata.SystemSpecService;
import cern.nxcals.common.utils.ReflectionUtils;
import cern.nxcals.internal.custom.extraction.metadata.InternalBaseGroupService;
import com.google.common.collect.ImmutableMap;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import java.util.Collection;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
class SnapshotServiceImpl extends InternalBaseGroupService<Snapshot, Snapshots> implements SnapshotService {
    private final SystemSpecService systemSpecService;

    public SnapshotServiceImpl(GroupService groupService, SystemSpecService systemSpecService) {
        super(groupService);
        this.systemSpecService = systemSpecService;
    }

    @Override
    protected Snapshot fromGroup(Group group) {
        Map<String, String> groupProperties = group.getProperties();

        EnumMap<SnapshotProperty, String> timeProperties = new EnumMap<>(SnapshotProperty.class);
        for (SnapshotProperty property : SnapshotProperty.values()) {
            timeProperties.put(property, groupProperties.get(property.getValue()));
        }

        TimeDefinitionProperties timeDefinitionProperties = new TimeDefinitionProperties(
                timeProperties
        );


        Snapshot snapshot = Snapshot.builder()
                .name(group.getName())
                .description(group.getDescription())
                .system(group.getSystemSpec().getName())
                .owner(group.getOwner())
                .visibility(group.getVisibility())
                .fundamentalFilterSet(FundamentalMapper.toFundamentals(groupProperties))
                .timeDefinition(timeDefinitionProperties)
                .build();

        ReflectionUtils.enhanceWithId(snapshot, group.getId());
        ReflectionUtils.enhanceWithRecVersion(snapshot, group.getRecVersion());
        return snapshot;
    }

    @AllArgsConstructor
    private static class TimeDefinitionProperties implements Snapshot.TimeDefinition {
        EnumMap<SnapshotProperty, String> timeProperties;

        @Override
        public TimeWindow getTimeWindow() {
            return TimeWindow.infinite();
        }

        @Override
        public EnumMap<SnapshotProperty, String> getProperties() {
            return timeProperties;
        }
    }

    @Override
    protected Group toGroup(Snapshot snapshot) {
        Group group = Group.builder().name(snapshot.getName())
                .description(snapshot.getDescription())
                .systemSpec(systemSpecService.findByName(snapshot.getSystem())
                        .orElseThrow(() -> new RuntimeException("Cannot find system: " + snapshot.getSystem())))
                .properties(new HashMap<>(snapshot.getProperties()))
                .owner(snapshot.getOwner())
                .visibility(snapshot.getVisibility())
                .label(GroupType.SNAPSHOT.toString())
                .build();

        ReflectionUtils.enhanceWithId(group, snapshot.getId());
        ReflectionUtils.enhanceWithRecVersion(group, snapshot.getRecVersion());
        return group;
    }

    @Override
    public Snapshot create(Snapshot snap, @NonNull Collection<Entity> entities,
                           @NonNull Collection<Variable> variables,
                           @NonNull Collection<Variable> xAxisVariables,
                           @NonNull Collection<Variable> fundamentalFiltersVariables,
                           @NonNull Collection<Variable> drivingVariables) {

        Snapshot created = create(snap);
        if (!entities.isEmpty()) {
            setEntities(created.getId(),
                    ImmutableMap.of(AssociationType.DEFAULT.getValue(),
                            entities.stream().map(Entity::getId).collect(Collectors.toSet())));
        }

        Map<String, Set<Long>> variableIdsPerAssociation = new HashMap<>();

        variableIdsPerAssociation.put(AssociationType.DEFAULT.getValue(), getVariableIds(variables));
        variableIdsPerAssociation.put(AssociationType.X_AXIS_VARIABLE.getValue(), getVariableIds(xAxisVariables));
        variableIdsPerAssociation.put(AssociationType.FUNDAMENTAL_FILTERS.getValue(), getVariableIds(fundamentalFiltersVariables));
        variableIdsPerAssociation.put(AssociationType.DRIVING_VARIABLE.getValue(), getVariableIds(drivingVariables));
        setVariables(created.getId(), variableIdsPerAssociation);

        return created;
    }

    private Set<Long> getVariableIds(Collection<Variable> variables) {
        return variables.stream().map(Variable::getId).collect(
                Collectors.toSet());
    }
}
