alter table partition_resource_infos add constraint partition_resource_id_fk foreign key (partition_resource_id)
    references partition_resources (partition_resource_id) on delete cascade;