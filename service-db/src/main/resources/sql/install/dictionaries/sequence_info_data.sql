-- Initialize SEQUENCE_INFO table 


insert into sequence_info(seq_name, seq_last_value, seq_rec_version) values ('schema_seq',1,0);
insert into sequence_info(seq_name, seq_last_value, seq_rec_version) values ('default_seq',1,0);
insert into sequence_info(seq_name, seq_last_value, seq_rec_version) values ('entity_seq',1,0);
insert into sequence_info(seq_name, seq_last_value, seq_rec_version) values ('entity_hist_seq',1,0);
insert into sequence_info(seq_name, seq_last_value, seq_rec_version) values ('partition_seq',1,0);
insert into sequence_info(seq_name, seq_last_value, seq_rec_version) values ('system_seq',3,0);
insert into sequence_info(seq_name, seq_last_value, seq_rec_version) values ('variable_seq',1,0);