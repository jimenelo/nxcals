create table partition_resource_infos_changelog (
  id                                     number not null,
  partition_resource_info_id             number,
  old_partition_resource_id              number,
  new_partition_resource_id              number,
  old_partition_resource_information     varchar2(1024),
  new_partition_resource_information     varchar2(1024),
  old_compaction_type                    varchar2(32),
  new_compaction_type                    varchar2(32),
  old_storage_type                       varchar2(32),
  new_storage_type                       varchar2(32),
  old_is_fixed                           char(1),
  new_is_fixed                           char(1),
  old_valid_from_stamp                   timestamp(9),
  new_valid_from_stamp                   timestamp(9),
  old_valid_to_stamp                     timestamp(9),
  new_valid_to_stamp                     timestamp(9),
  old_rec_version                        number,
  new_rec_version                        number,
  op_type                                char(1),
  create_time_utc                        timestamp(9),
  transaction_id                         varchar2(48),
  module                                 varchar2(48),
  action                                 varchar2(32),
  client_info                            varchar2(64)
)

partition by range (create_time_utc) interval(interval '1' month) (
    partition pos_data_initial values less than (timestamp '2019-12-01 00:00:00')
);