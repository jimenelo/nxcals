create table partition_resources (
    partition_resource_id number,
    system_id number,
    partition_id number,
    schema_id number,
    rec_version number
)