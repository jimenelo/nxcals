package cern.nxcals.service.db;

import cern.nxcals.db.Application;
import com.typesafe.config.ConfigFactory;

/**
 * Created by msobiesz on 04/08/17.
 */
public class ApplicationDev {
    static {
        String user = System.getProperty("user.name");
        System.setProperty("db.user", user);
    }

    public static void main(String[] args) {
        new Application().run(ConfigFactory.load());
    }

}
