package cern.nxcals.performancetests.kafkaetl;

import cern.nxcals.kafka.etl.config.HbaseProperties;
import cern.nxcals.kafka.etl.domain.PartitionInfo;
import cern.nxcals.kafka.etl.hbase.ConnectionSupplier;
import cern.nxcals.kafka.etl.hbase.HbaseTableProvider;
import cern.nxcals.kafka.etl.hbase.HbaseWriter;
import cern.nxcals.performancetests.common.AbstractPerformanceTest;
import cern.nxcals.performancetests.common.BenchmarkThreshold;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.BufferedMutatorParams;
import org.apache.hadoop.hbase.client.Put;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Level;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.TearDown;
import org.openjdk.jmh.annotations.Warmup;
import org.openjdk.jmh.infra.Blackhole;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static cern.nxcals.common.utils.CollectionUtils.splitIntoParts;
import static cern.nxcals.common.utils.ParallelUtils.IOParallelUtils.IORunnableExecutor.handleIO;

@Warmup(iterations = 5, time = 100, timeUnit = TimeUnit.MILLISECONDS)
@Measurement(iterations = 5, time = 100, timeUnit = TimeUnit.MILLISECONDS)
public class HBaseWriterTest extends AbstractPerformanceTest {
    private static final String tableName = "PERF-TEST-JMH";
    private static final String namespace = "nxcals_perf_test";

    private static final int fieldsNumber = 5;
    private static final int fieldSize = 200_000;
    private static final int potentialPartitionNumber = 4;
    private static final int partitionSize = 2000;
    private static final int putsNumber = partitionSize * potentialPartitionNumber;
    private static final double totalPutsSize = 1.0 * putsNumber * fieldsNumber * fieldSize / 1000_000_000;

    private static final PartitionInfo partitionInfo = new PartitionInfo(10000L, 10L, 1000L, 0L, 288L, false);
    private static final String columnFamily = "data";

    HbaseWriter writer;

    ExecutorService singleThreadExecutorService = Executors.newSingleThreadExecutor();
    ExecutorService parallelExecutor = Executors.newFixedThreadPool(10);
    List<Put> puts = createPuts();
    List<List<Put>> parts = splitIntoParts(puts, 1000_000, 10000, x -> (long) fieldSize * fieldsNumber);

    @Setup(Level.Trial)
    public void setup() {
        HbaseProperties.WriterProperties writerProperties = createWriterProperties();
        HbaseProperties.ConnectionProperties connectionProperties = createConnectionProperties();

        HbaseProperties hbaseProperties = createHbaseProperties(writerProperties, connectionProperties);

        ConnectionSupplier connectionSupplier = new ConnectionSupplier(connectionProperties);
        HbaseTableProvider tableProvider = new HbaseTableProvider(x -> tableName, hbaseProperties, connectionSupplier);
        TableName hbaseTable = tableProvider.tableFor(partitionInfo);
        BufferedMutatorParams mutatorParams = new BufferedMutatorParams(hbaseTable);
        writer = new HbaseWriter(connectionSupplier, mutatorParams, 10, hbaseTable, 1000_000);
    }

    private HbaseProperties.WriterProperties createWriterProperties() {
        HbaseProperties.WriterProperties writerProperties = new HbaseProperties.WriterProperties();
        writerProperties.setExecutorName("PerfTestExecutor");
        writerProperties.setMutatorPoolSize(10);
        writerProperties.setConnectionPoolName("ConnectionPool");
        writerProperties.setWriteBufferSize(10_000_000);

        return writerProperties;
    }

    private HbaseProperties.ConnectionProperties createConnectionProperties() {
        HbaseProperties.ConnectionProperties connectionProperties = new HbaseProperties.ConnectionProperties();
        connectionProperties.setMaxConnections(5);

        connectionProperties.setMaxPerregionTasks(1_000);
        connectionProperties.setMaxPerserverTasks(1_000);
        connectionProperties.setMaxTotalTasks(2_000);

        connectionProperties.setMaxPerrequestHeapsize(200_000_000);
        connectionProperties.setMaxKeyvalueSize(50_000_000);
        connectionProperties.setMaxSubmitHeapsize(200_000_000);
        return connectionProperties;
    }

    private HbaseProperties createHbaseProperties(HbaseProperties.WriterProperties writerProperties,
            HbaseProperties.ConnectionProperties connectionProperties) {
        HbaseProperties hbaseProperties = new HbaseProperties();
        hbaseProperties.setMobEnabled(true);
        hbaseProperties.setWriters(Map.of("writer", writerProperties));
        hbaseProperties.setConnectionPools(Map.of("connection", connectionProperties));
        hbaseProperties.setColumnFamily(columnFamily);
        hbaseProperties.setNamespace(namespace);
        hbaseProperties.setTtlHours(1);
        hbaseProperties.setMobEnabled(true);
        hbaseProperties.setMobThreshold(20971520);
        hbaseProperties.setSaltBuckets(4);
        hbaseProperties.setTableProviderConnectionPoolName("connection");
        hbaseProperties.setSubmitExecutorThreadPoolSize(10);
        return hbaseProperties;
    }

    private List<Put> createPuts() {
        byte[] columnFamilyBytes = columnFamily.getBytes();

        byte[] fieldValue = new byte[fieldSize];
        //        Random r = new Random();
        //        r.nextBytes(fieldValue); // this can increase complexity
        for (int i = 0; i < fieldSize; i++) {
            fieldValue[i] = (byte) i;
        }

        long now = System.currentTimeMillis();
        List<Put> putsList = new ArrayList<>(putsNumber);

        for (int partitionId = 0; partitionId < potentialPartitionNumber; partitionId++) {
            for (int i = 0; i < partitionSize; i++) {
                String key = partitionId + "_" + now + "_" + i;
                Put put = new Put(key.getBytes(), now + i);

                for (int fieldCounter = 0; fieldCounter < fieldsNumber; fieldCounter++) {
                    //                    r.nextBytes(fieldValue); // this can increase complexity
                    put.addColumn(columnFamilyBytes, ("field-" + fieldCounter).getBytes(), fieldValue);
                }

                putsList.add(put);
            }
        }
        Collections.shuffle(putsList);
        return putsList;
    }

    @BenchmarkThreshold(threshold = 140)
    @Benchmark
    public void pushPutsInOneThread(Blackhole blackhole) throws IOException {
        long start = System.currentTimeMillis();

        writer.submit(puts);
        writer.flush(singleThreadExecutorService);

        printSummary(System.currentTimeMillis() - start);
    }

    @BenchmarkThreshold(threshold = 20)
    @Benchmark
    public void pushPutsInMultipleThreads(Blackhole blackhole)
            throws IOException, ExecutionException, InterruptedException {
        long start = System.currentTimeMillis();

        sendPutsParallel(parts, parallelExecutor);
        writer.flush(parallelExecutor);

        printSummary(System.currentTimeMillis() - start);
    }

    void sendPutsParallel(List<List<Put>> parts, ExecutorService executorService)
            throws IOException, InterruptedException, ExecutionException {
        CompletableFuture<Optional<IOException>>[] futures = new CompletableFuture[parts.size()];
        for (int i = 0; i < parts.size(); i++) {
            final List<Put> part = parts.get(i);
            futures[i] = CompletableFuture.supplyAsync(handleIO(() -> writer.submit(part)), executorService);
        }

        for (int i = 0; i < parts.size(); i++) {
            Optional<IOException> maybeException = futures[i].get();
            if (maybeException.isPresent()) {
                throw maybeException.get();
            }
        }
    }

    void printSummary(long totalTime) {
        System.out.println("Inserted " + totalPutsSize + " GB " + totalPutsSize / totalTime * 1000 + "GB/s");
    }

    @TearDown(Level.Trial)
    public void stopExecutors() {
        parallelExecutor.shutdown();
        singleThreadExecutorService.shutdown();
    }
}
