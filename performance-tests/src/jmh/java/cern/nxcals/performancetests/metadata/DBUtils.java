package cern.nxcals.performancetests.metadata;

import cern.nxcals.common.utils.ConfigHolder;
import cern.nxcals.db.ConnectionFactory;
import lombok.experimental.UtilityClass;

import java.sql.Connection;
import java.sql.Statement;
import java.util.Base64;

@UtilityClass
public class DBUtils {
    private static final String USERNAME = ConfigHolder.getProperty("db.user", "");
    private static final String PASSWORD = ConfigHolder.getProperty("db.password", "");
    private static final String URL = ConfigHolder.getProperty("db.url", "");
    private static final String DRIVER = ConfigHolder.getProperty("db.driver.class", "");

    public static void executeSQL(String script) {
        try (Connection connection = ConnectionFactory.forDriver(DRIVER)
                .getConnectionFor(URL, USERNAME, decodeBase64(PASSWORD))) {
            try (Statement statement = connection.createStatement()) {
                statement.execute(script);
            }
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    public static void truncateTable(String tableName) {
        DBUtils.executeSQL("truncate table " + tableName + " drop all storage");
    }

    private static String decodeBase64(String toDecode) {
        return new String(Base64.getDecoder().decode(toDecode));
    }
}
