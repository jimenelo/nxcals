package cern.nxcals.performancetests.metadata;

import cern.nxcals.api.domain.Hierarchy;
import cern.nxcals.api.domain.HierarchyView;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.domain.VariableDeclaredType;
import cern.nxcals.api.extraction.metadata.HierarchyService;
import cern.nxcals.api.extraction.metadata.ServiceClientFactory;
import cern.nxcals.api.extraction.metadata.SystemSpecService;
import cern.nxcals.api.extraction.metadata.VariableService;
import cern.nxcals.api.extraction.metadata.queries.Hierarchies;
import cern.nxcals.performancetests.common.AbstractPerformanceTest;
import cern.nxcals.performancetests.common.BenchmarkThreshold;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.TearDown;
import org.openjdk.jmh.infra.Blackhole;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class HierarchyTest extends AbstractPerformanceTest {
    private static final String CMW = "CMW";
    private static final int NUMBER_OF_VARIABLES = 1000;
    private final HierarchyService hierarchyService = ServiceClientFactory.createHierarchyService();
    private final SystemSpecService systemService = ServiceClientFactory.createSystemSpecService();
    private final VariableService variableService = ServiceClientFactory.createVariableService();
    private Set<Hierarchy> parents;
    private Set<Hierarchy> leafs;
    private Set<Long> variableIds;
    private long variableIdWithHierarchies;

    @Setup
    public void createHierarchies() {
        SystemSpec system = systemService.findByName(CMW).orElseThrow();
        parents = createParents(10, "HierarchyTest_Read_", system);
        leafs = createNodes(parents, 100, "HierarchyTest_Read_", system);
        Set<Variable> variables = createVariables(NUMBER_OF_VARIABLES, system);
        assertEquals(NUMBER_OF_VARIABLES, variables.size());
        variableIds = variables.stream().map(Variable::getId).collect(Collectors.toSet());
        variableIdWithHierarchies = variableIds.stream().findFirst().get();
        for (Hierarchy hierarchy : leafs) {
            hierarchyService.addVariables(hierarchy.getId(), Set.of(variableIdWithHierarchies));
        }
    }

    @BenchmarkThreshold(threshold = 10)
    @Benchmark
    public void shouldCreateAndDeleteMoreThan1kHierarchies() {
        SystemSpec system = systemService.findByName(CMW).orElseThrow();
        // create parents
        Set<Hierarchy> parents = createParents(10, "HierarchyTest_shouldCreateAndDeleteMoreThan1kHierarchies_", system);
        assertEquals(10, parents.size());

        // create leafs
        Set<Hierarchy> hierarchies = createNodes(parents, 101,
                "HierarchyTest_shouldCreateAndDeleteMoreThan1kHierarchies_", system);
        assertEquals(1010, hierarchies.size());

        // clean up
        hierarchyService.deleteAllLeaves(hierarchies);
        hierarchyService.deleteAllLeaves(parents);
    }

    @BenchmarkThreshold(threshold = 10)
    @Benchmark
    public void shouldUpdate1kHierarchies() {
        // update
        Set<Hierarchy> changedHierarchies = leafs.stream().map(hierarchy ->
                        hierarchy.toBuilder().description(hierarchy.getDescription() + "-updated").build())
                .collect(Collectors.toSet());

        leafs = hierarchyService.updateAll(changedHierarchies);
    }

    @BenchmarkThreshold(threshold = 2)
    @Benchmark
    public void shouldAttachMoreThan1kVariablesToHierarchy() {
        // find any hierarchy
        Hierarchy hierarchy = parents.stream().findFirst().get();

        // update
        hierarchyService.addVariables(hierarchy.getId(), variableIds);
    }

    @BenchmarkThreshold(threshold = 10)
    @Benchmark
    public void shouldGetAllHierarchiesForVariable(Blackhole blackhole) {
        blackhole.consume(hierarchyService.getHierarchiesForVariable(variableIdWithHierarchies));
    }

    @BenchmarkThreshold(threshold = 0.5)
    @Benchmark
    public void shouldFindMoreThan1kHierarchiesById(Blackhole blackhole) {
        Set<Hierarchy> fetchedChildren = getByIdIn(
                leafs.stream().map(HierarchyView::getId).collect(Collectors.toSet()));
        blackhole.consume(fetchedChildren);
    }

    @BenchmarkThreshold(threshold = 0.5)
    @Benchmark
    public void shouldFindMoreThan1kHierarchiesByPath(Blackhole blackhole) {
        blackhole.consume(getByPathIn(
                leafs.stream().map(Hierarchy::getNodePath).collect(Collectors.toSet())
        ));
    }

    @TearDown
    public void cleanUp() {
        hierarchyService.deleteAllLeaves(leafs);
        hierarchyService.deleteAllLeaves(parents);
        variableService.deleteAll(variableIds);
        cleanUpDatabase();
    }

    private Set<Hierarchy> createParents(int number, String namePrefix, SystemSpec system) {
        Set<Hierarchy> parentsToCreate = new HashSet<>();
        for (int i = 0; i < number; i++) {
            parentsToCreate.add(Hierarchy.builder()
                    .name(getRandomString(namePrefix + "Parent_"))
                    .systemSpec(system).build());
        }
        return hierarchyService.createAll(parentsToCreate);
    }

    private Set<Hierarchy> createNodes(Set<Hierarchy> parents, int nodesPerParent, String namePrefix,
            SystemSpec system) {
        Set<Hierarchy> hierarchiesToCreate = new HashSet<>();
        for (Hierarchy parent : parents) {
            for (int i = 0; i < nodesPerParent; i++) {
                hierarchiesToCreate.add(Hierarchy.builder()
                        .name(getRandomString(namePrefix + "Leaf_"))
                        .parent(parent).systemSpec(system).build());
            }
        }
        return hierarchyService.createAll(hierarchiesToCreate);
    }

    private String getRandomString(String prefix) {
        return prefix + UUID.randomUUID();
    }

    private Set<Hierarchy> getByPathIn(Set<String> paths) {
        return hierarchyService.findAll(Hierarchies.suchThat().path().in(paths));
    }

    private Set<Hierarchy> getByIdIn(Set<Long> ids) {
        return hierarchyService.findAll(Hierarchies.suchThat().id().in(ids));
    }

    private Set<Variable> createVariables(int count, SystemSpec system) {
        Set<Variable> variablesToCreate = new HashSet<>();
        for (int i = 0; i < count; i++) {
            Variable variable = Variable.builder()
                    .variableName(getRandomString("HierarchyTestVariable"))
                    .declaredType(VariableDeclaredType.NUMERIC)
                    .systemSpec(system)
                    .build();
            variablesToCreate.add(variable);
        }
        return variableService.createAll(variablesToCreate);
    }

    private void cleanUpDatabase() {
        DBUtils.truncateTable("hierarchies_changelog");
        DBUtils.truncateTable("groups_changelog");
        DBUtils.truncateTable("variables_changelog");
    }
}
