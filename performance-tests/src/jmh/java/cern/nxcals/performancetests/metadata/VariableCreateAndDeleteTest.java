package cern.nxcals.performancetests.metadata;

import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.domain.VariableConfig;
import cern.nxcals.api.domain.VariableDeclaredType;
import cern.nxcals.api.extraction.metadata.EntityService;
import cern.nxcals.api.extraction.metadata.ServiceClientFactory;
import cern.nxcals.api.extraction.metadata.VariableService;
import cern.nxcals.api.extraction.metadata.queries.Entities;
import cern.nxcals.api.extraction.metadata.queries.Variables;
import cern.nxcals.performancetests.common.AbstractPerformanceTest;
import cern.nxcals.performancetests.common.BenchmarkThreshold;
import com.github.rutledgepaulv.qbuilders.conditions.Condition;
import lombok.extern.slf4j.Slf4j;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.TearDown;

import java.time.OffsetDateTime;
import java.util.Set;
import java.util.StringJoiner;
import java.util.TreeSet;
import java.util.stream.Collectors;

@Slf4j
public class VariableCreateAndDeleteTest extends AbstractPerformanceTest {
    private static final String WINCCOA = "WINCCOA-PERF";
    @Param(value = { "1", "10", "100", "1000", "10000" })
    private int numberOfVariablesToRegister;
    private final EntityService entityService = ServiceClientFactory.createEntityService();
    private final VariableService variableService = ServiceClientFactory.createVariableService();
    private Set<Variable> variablesToRegister;

    @Setup
    public void setup() {
        super.setup();
        Condition<Entities> entityQuery = Entities.suchThat().systemName().eq(WINCCOA);
        Set<Entity> winccoaEntities = entityService.findAll(entityQuery).stream().limit(numberOfVariablesToRegister)
                .collect(Collectors.toSet());
        if (winccoaEntities.size() != numberOfVariablesToRegister) {
            throw new RuntimeException("Not enough entities in WINCCOA system to run benchmark");
        }
        variablesToRegister = winccoaEntities.stream().map(entity -> createVariable(entity, getClass().getName()))
                .collect(Collectors.toSet());
    }

    public static Variable createVariable(Entity entity, String namePrefix) {
        String name = new StringJoiner("_")
                .add(namePrefix)
                .add(Long.toString(entity.getId()))
                .add(OffsetDateTime.now().toString()).toString();
        VariableConfig config = VariableConfig.builder()
                .variableName(name)
                .fieldName("value")
                .validity(TimeWindow.infinite())
                .entityId(entity.getId())
                .build();

        return Variable.builder()
                .variableName(name)
                .configs(new TreeSet<>(Set.of(config)))
                .declaredType(VariableDeclaredType.NUMERIC)
                .systemSpec(entity.getSystemSpec())
                .build();
    }

    @BenchmarkThreshold(threshold = 1)
    @BenchmarkThreshold(threshold = 60, params = { "numberOfVariablesToRegister" }, paramsValues = { "10000" })
    @BenchmarkThreshold(threshold = 10, params = { "numberOfVariablesToRegister" }, paramsValues = { "1000" })
    @Benchmark
    public void shouldCreateAndDeleteVariablesForWinccoaEntities() {
        Set<Variable> createdVariables = variableService.createAll(variablesToRegister);
        variableService.deleteAll(createdVariables.stream().map(Variable::getId).collect(Collectors.toSet()));
    }

    @TearDown
    public void cleanUp() {
        Set<Variable> variables = variableService.findAll(
                Variables.suchThat().variableName().like(getClass().getName() + "%"));
        log.info("Found " + variables.size() + " to remove.");
        variableService.deleteAll(variables.stream().map(Variable::getId).collect(Collectors.toSet()));
        log.info("Variables removed.");
        cleanUpChangelogs();
        log.info("Changelogs truncated");
    }

    private void cleanUpChangelogs() {
        DBUtils.truncateTable("variables_changelog");
        DBUtils.truncateTable("variable_configs_changelog");
    }
}
