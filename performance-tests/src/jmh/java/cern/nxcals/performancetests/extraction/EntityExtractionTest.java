package cern.nxcals.performancetests.extraction;

import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.extraction.data.builders.DataQuery;
import cern.nxcals.performancetests.common.AbstractPerformanceTest;
import cern.nxcals.performancetests.common.BenchmarkThreshold;

import java.time.Instant;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.infra.Blackhole;

public class EntityExtractionTest extends AbstractPerformanceTest {
    @Param({ "3600", "86400" })
    public int timeWindowInSeconds;

    private final TimeWindow hdfsTimeWindow = TimeWindow.between(TIMESTAMP,TIMESTAMP.plusSeconds(timeWindowInSeconds));

    private final Instant now = Instant.now();
    private final TimeWindow hbaseTimeWindow = TimeWindow.between(now.minusSeconds(timeWindowInSeconds), now);


    @BenchmarkThreshold(threshold = 5, params = { "timeWindowInSeconds" }, paramsValues = { "3600" })
    @BenchmarkThreshold(threshold = 10, params = { "timeWindowInSeconds" }, paramsValues = { "86400" })
    @Benchmark
    public void simpleReadEntity(Blackhole blackhole) {
        Dataset<Row> dataset = DataQuery.builder(sparkSession).entities().system(MOCK_SYSTEM)
                .keyValuesEq(KEY_VALUES).timeWindow(hdfsTimeWindow).build();
        blackhole.consume(dataset.collect());
    }

    @BenchmarkThreshold(threshold = 5, params = { "timeWindowInSeconds" }, paramsValues = { "3600" })
    @BenchmarkThreshold(threshold = 10, params = { "timeWindowInSeconds" }, paramsValues = { "86400" })
    @Benchmark
    public void simpleReadEntityFromHBase(Blackhole blackhole) {
        Dataset<Row> dataset = DataQuery.builder(sparkSession).entities().system(MOCK_SYSTEM)
                .keyValuesEq(KEY_VALUES).timeWindow(hbaseTimeWindow).build();
        blackhole.consume(dataset.collect());
    }

    @BenchmarkThreshold(threshold = 10, params = { "timeWindowInSeconds" }, paramsValues = { "3600" })
    @BenchmarkThreshold(threshold = 25, params = { "timeWindowInSeconds" }, paramsValues = { "86400" })
    @Benchmark
    public void readAllMonitoringEntities(Blackhole blackhole) {
        Dataset<Row> dataset = DataQuery.builder(sparkSession).entities().system(MOCK_SYSTEM)
                .keyValuesLike(KEY_VALUES_PATTERN).timeWindow(hdfsTimeWindow).build();
        blackhole.consume(dataset.collect());
    }

    @BenchmarkThreshold(threshold = 10, params = { "timeWindowInSeconds" }, paramsValues = { "3600" })
    @BenchmarkThreshold(threshold = 25, params = { "timeWindowInSeconds" }, paramsValues = { "86400" })
    @Benchmark
    public void readAllMonitoringEntitiesFromHBase(Blackhole blackhole) {
        Dataset<Row> dataset = DataQuery.builder(sparkSession).entities().system(MOCK_SYSTEM)
                .keyValuesLike(KEY_VALUES_PATTERN).timeWindow(hbaseTimeWindow).build();
        blackhole.consume(dataset.collect());
    }

    @BenchmarkThreshold(threshold = 10, params = { "timeWindowInSeconds" }, paramsValues = { "3600" })
    @BenchmarkThreshold(threshold = 25, params = { "timeWindowInSeconds" }, paramsValues = { "86400" })
    @Benchmark
    public void countAllMonitoringEntitiesFromHBase(Blackhole blackhole) {
        Dataset<Row> dataset = DataQuery.builder(sparkSession).entities().system(MOCK_SYSTEM)
                .keyValuesLike(KEY_VALUES_PATTERN).timeWindow(hbaseTimeWindow).build();
        blackhole.consume(dataset.count());
    }

    @BenchmarkThreshold(threshold = 10, params = { "timeWindowInSeconds" }, paramsValues = { "3600" })
    @BenchmarkThreshold(threshold = 25, params = { "timeWindowInSeconds" }, paramsValues = { "86400" })
    @Benchmark
    public void countAndFilterAllMonitoringEntitiesFromHBase(Blackhole blackhole) {
        Dataset<Row> dataset = DataQuery.builder(sparkSession).entities().system(MOCK_SYSTEM)
                .keyValuesLike(KEY_VALUES_PATTERN).timeWindow(hbaseTimeWindow).build().where("device IS NOT NULL");
        blackhole.consume(dataset.count());
    }
}
