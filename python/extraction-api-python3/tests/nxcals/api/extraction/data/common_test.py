from typing import Any, List, Tuple
from unittest.mock import MagicMock
import numpy as np
from datetime import datetime
from unittest import TestCase

from nxcals.api.extraction.data.common import nanos


class NopClass:
    def __init__(self) -> None:
        self.last_invocation = ""
        self.invocations: List[Tuple[str, List[Any]]] = []

    def __call__(self, *args):
        self.invocations = self.invocations + [(self.last_invocation, list(args))]
        return self

    def __getattr__(self, *args):
        self.last_invocation = args[0]
        return self


spark_mock = NopClass()
builder_mock = NopClass()


class should_accept_datetime64(TestCase):
    def runTest(self):
        # nanosecond precision
        input = np.datetime64("2015-01-01T12:30:30.123456789")
        out = nanos(input)
        assert 1420115430123456789 == out


class should_accept_datetime(TestCase):
    def runTest(self):
        # microsecond precision
        input = datetime(2015, 1, 1, 12, 30, 30, 123456)
        out = nanos(input)
        assert 1420115430123456000 == out


class should_accept_numerics(TestCase):
    def runTest(self):
        # nanosecond precision
        input = 1420115430123456789
        out = nanos(input)
        assert 1420115430123456789 == out


class should_accept_numpy_int64(TestCase):
    def runTest(self):
        # nanosecond precision
        input = np.int64(1420115430123456789)
        out = nanos(input)
        assert 1420115430123456789 == out


class should_accept_strings_with_nanos(TestCase):
    def runTest(self):
        # nanosecond precision
        input = "2015-01-01 12:30:30.123456789"
        out = nanos(input)
        assert 1420115430123456789 == out


class should_accept_strings_with_micros(TestCase):
    def runTest(self):
        # microsecond precision
        input = "2015-01-01 12:30:30.123456"
        out = nanos(input)
        assert 1420115430123456000 == out


class should_accept_strings_of_different_format_with_micros(TestCase):
    def runTest(self):
        # microsecond precision
        input = "2015-01-01T12:30:30.123456"
        out = nanos(input)
        assert 1420115430123456000 == out


class should_accept_strings_of_different_format_with_nanos(TestCase):
    def runTest(self):
        # nanosecond precision
        input = "2015-01-01T12:30:30.123456789"
        out = nanos(input)
        assert 1420115430123456789 == out


class should_not_accept_unsupported_format(TestCase):
    def runTest(self):
        with self.assertRaises(ValueError):
            nanos((1, 2, 3, 4))


def get_invocations(mock: MagicMock) -> List[Tuple[str, List[Any]]]:
    return [
        (methodPath.split(".")[-1], list(args) + list(kwargs.values()))
        for (methodPath, args, kwargs) in mock.mock_calls
    ]
