from __future__ import annotations
from typing import Any, List, Dict, Iterable, overload, Optional

from py4j.java_gateway import JavaObject

from .common import (
    Builder,
    SystemStage,
    StartStage,
    KeyValuesStage,
    VariableAliasStage,
    EntityAliasStage,
    VariableStage,
    EntityQuery,
    DeviceStage,
    TimeType,
    nanos,
)
from pyspark.sql import DataFrame, SparkSession
from .stages import (
    EntityStage,
    BuildStage,
    EntityStageLoop,
    VariableStageLoop,
    SystemOrIdStage,
    DeviceStage as DeviceStageV2,
    SystemStage as SystemStageV2,
    VariableStage as VariableStageV2,
)


def builders_package(spark):
    return spark._jvm.cern.nxcals.api.extraction.data.builders


class DataQuery:
    """
    A class that provides methods for building queries on data that has been loaded into a Spark dataframe.
    """

    @staticmethod
    def builder(spark):
        return DataQuery(spark)

    def __init__(self, spark):
        self.spark = spark

    def byEntities(self):
        builder = Builder(
            self.spark,
            builders_package(self.spark)
            .DataQuery.builder(self.spark._jsparkSession)
            .byEntities(),
        )
        return SystemStage(
            StartStage(EntityAliasStage(KeyValuesStage(builder=builder)))
        )

    def byVariables(self):
        builder = Builder(
            self.spark,
            builders_package(self.spark)
            .DataQuery.builder(self.spark._jsparkSession)
            .byVariables(),
        )
        return SystemStage(
            StartStage(VariableAliasStage(VariableStage(builder=builder)))
        )

    def variables(
        self,
    ) -> SystemOrIdStage[
        VariableStageV2[BuildStage[DataFrame]],
        VariableStageLoop[BuildStage[DataFrame]],
        BuildStage[DataFrame],
    ]:
        builder = Builder(
            self.spark,
            builders_package(self.spark)
            .DataQuery.builder(self.spark._jsparkSession)
            .variables(),
        )
        return SystemOrIdStage(VariableStageV2(BuildStage(builder=builder)))

    def entities(
        self,
    ) -> SystemOrIdStage[
        EntityStage[BuildStage[DataFrame]],
        EntityStageLoop[BuildStage[DataFrame]],
        BuildStage[DataFrame],
    ]:
        builder = Builder(
            self.spark,
            builders_package(self.spark)
            .DataQuery.builder(self.spark._jsparkSession)
            .entities(),
        )
        return SystemOrIdStage(EntityStage(BuildStage(builder=builder)))

    @staticmethod
    def getForVariables(
        spark: SparkSession,
        start_time: TimeType,
        end_time: TimeType,
        system: str,
        variables: List[str] = [],
        variables_like: List[str] = [],
        field_aliases: Dict[str, List[str]] = {},
    ) -> DataFrame:
        if len(variables + variables_like) == 0:
            raise ValueError("No variables nor variable patterns")

        query = DataQuery.builder(spark).variables().system(system).nameIn(variables)

        for pattern in variables_like:
            query.nameLike(pattern)

        return (
            query.timeWindow(start_time, end_time)
            .fieldAliases(
                {alias: set(fields) for alias, fields in field_aliases.items()}
            )
            .build()
        )

    @staticmethod
    def getForEntities(
        spark: SparkSession,
        start_time: TimeType,
        end_time: TimeType,
        system: str,
        entity_queries: List[EntityQuery],
        field_aliases: Dict[str, List[str]] = {},
    ) -> DataFrame:
        query = DataQuery.builder(spark).entities().system(system)
        for entity_query in entity_queries:
            if entity_query.has_wildcards():
                key_values = {
                    key: DataQuery.__escape_wildcard_character(value)
                    for key, value in entity_query.key_values.items()
                }
                patterns = dict(key_values, **entity_query.key_values_like)
                query = query.keyValuesLike(patterns)
            else:
                query = query.keyValuesEq(entity_query.key_values)
        return (
            query.timeWindow(start_time, end_time)
            .fieldAliases(
                {alias: set(fields) for alias, fields in field_aliases.items()}
            )
            .build()
        )

    @staticmethod
    def __escape_wildcard_character(maybe_str: Any):
        if isinstance(maybe_str, str):
            return maybe_str.replace("%", "\\%").replace("_", "\\_")
        else:
            return maybe_str

    @staticmethod
    def getAsPivot(
        spark: SparkSession,
        start_time: TimeType,
        end_time: TimeType,
        variables: Iterable[Any] = (),
        system: Optional[str] = None,
        variable_names: Iterable[str] = (),
    ) -> DataFrame:
        """
        /**
        Create a dataset with variable names as columns. Values are joined on timestamps. Experimental, may be changed
        in the future.
        :param spark: spark session
        :param start_time: begin of extraction time window
        :param end_time: end of extraction time window
        :param variables: iterable, which contains Variable objects (JavaObject). If not specified, then system and
        variable_names are used.
        :param system: system name, where variables will be searched from variable_names
        :param variable_names: iterable containing variable names, which must be registered in system
        :return: PySpark DataFrame, with one column "nxcals_timestamp" and other columns named after variables, e.g.
        "nxcals_timestamp", "BLM1:arr", "BLM2:voltage".
        """
        time_window = DataQuery.__to_time_window(spark, start_time, end_time)

        if variables:
            java_df = builders_package(spark).DataQuery.getAsPivot(
                spark._jsparkSession, time_window, set(variables)
            )
        else:
            java_df = builders_package(spark).DataQuery.getAsPivot(
                spark._jsparkSession, time_window, system, variable_names
            )

        return DataFrame(java_df, spark)

    @staticmethod
    def __to_time_window(
        spark: SparkSession, start_time: TimeType, end_time: TimeType
    ) -> JavaObject:
        return spark._jvm.cern.nxcals.api.domain.TimeWindow.between(
            nanos(start_time), nanos(end_time)
        )


class DevicePropertyDataQuery:
    @staticmethod
    def builder(spark):
        builder = Builder(
            spark,
            builders_package(spark).DevicePropertyDataQuery.builder(
                spark._jsparkSession
            ),
        )
        return SystemStage(StartStage(EntityAliasStage(DeviceStage(builder=builder))))


class ParameterDataQuery:
    @staticmethod
    def builder(spark) -> SystemStageV2[DeviceStageV2[BuildStage[DataFrame]]]:
        builder = Builder(
            spark,
            builders_package(spark).ParameterDataQuery.builder(spark._jsparkSession),
        )
        return SystemStageV2(DeviceStageV2(BuildStage(builder=builder)))
