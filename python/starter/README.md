The main NXCALS package containing extraction api, spark session builder and type stubs.

# Access to NXCALS
To read data, you need to subscribe to egroup: [it-hadoop-nxcals-pro-analytics](https://e-groups.cern.ch/e-groups/Egroup.do?egroupId=10249154).

To be able to modify metadata (save snapshots, modify hierarchies) please subscribe to [nxcals-pro-metadata-auth](https://e-groups.cern.ch/e-groups/Egroup.do?egroupId=10527329) for PRO environment and to [nxcals-testbed-metadata-auth](https://e-groups.cern.ch/e-groups/Egroup.do?egroupId=10527330) for TESTBED.

# Examples

The first step is creating spark session - it is essential to access NXCALS data. In case of Python, it also allows to use Java classes with Py4j.

```python
from nxcals.spark_session_builder import get_or_create

spark = get_or_create()
```

As simple as that. You will use this session later, with all calls to NXCALS API.

## Extraction variable

To extract data, use dedicated class `DataQuery`.

```python
from nxcals.api.extraction.data.builders import DataQuery

dataset = DataQuery.builder(spark).variables() \
    .system("CMW") \
    .nameEq("BLM.XPOC.B1.MKB:LOSS_MAX") \
    .timeWindow("2024-03-05 00:00:00", "2024-03-07 00:00:00.0000") \
    .build()
```

`dataset` is Spark `DataFrame`. It contains four columns:
```console
>>> dataset.columns
['nxcals_value', 'nxcals_entity_id', 'nxcals_timestamp', 'nxcals_variable_name']
```

If you prefer `pandas` API, you might be interested in `pandas_api` method, which creates so called "pandas on spark".

```console
>>> dataset.pandas_api()
   nxcals_value  nxcals_entity_id     nxcals_timestamp      nxcals_variable_name
0         6.325             66408  1709655490807000000  BLM.XPOC.B1.MKB:LOSS_MAX
1         6.325             66408  1709638742969000000  BLM.XPOC.B1.MKB:LOSS_MAX
```

Then you can use most of the pandas API (not all methods are covered):
```console
>>> dataset.pandas_api().plot(x="nxcals_timestamp", y="nxcals_value").show()
```

"Pandas on Spark" is more robust in case of bigger datasets and processing than `toPandas` or `collect`, since it can take advantage of Spark cluster in processing data, instead of processing everything on local machine.

More information and examples can be found on project wiki page [nxcals-docs.web.cern.ch](https://nxcals-docs.web.cern.ch).