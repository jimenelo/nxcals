import sys
import unittest
from contextlib import contextmanager
from pathlib import Path
from typing import Set, Union, Optional
from unittest.mock import patch, Mock

import pytest
from py4j.java_gateway import JavaPackage, JavaClass, JavaObject, JVMView  # type: ignore

from py4j_utils import py4j_importer
from py4j_utils.py4j_importer import (
    Py4jImporter,
    Py4jModuleSpec,
    _find_packages_with_stubs_in_sys_path,
    _to_string_set,
    JavaClassProxy,
    _add_or_update,
    _find_stub_package_fqns,
    PY4JGW_SUBS,
    enable_py4j_imports,
)


def _remove_from_sys_modules(package_name: str):
    # cleanup between tests, typically called from fixture
    to_remove = [mod for mod in sys.modules.keys() if mod.startswith(package_name)]
    for mod in to_remove:
        sys.modules.pop(mod)


def _remove_py4j_importers():
    to_remove = [imp for imp in sys.meta_path if isinstance(imp, Py4jImporter)]
    for imp in to_remove:
        sys.meta_path.remove(imp)


@contextmanager
def _spark_context_provider():
    from pyspark.sql import SparkSession

    spark = SparkSession.builder.appName("unit_test").getOrCreate()
    yield spark
    spark.stop()


@contextmanager
def py4j_imports_enabled(
    py4j_jvm: JVMView,
    py4j_stubs_package_name: Union[str, Set[str]] = PY4JGW_SUBS,
    py4j_valid_packages: Optional[Set[str]] = None,
):
    sys_importer = enable_py4j_imports(
        py4j_jvm, None, py4j_stubs_package_name, py4j_valid_packages
    )
    yield sys_importer
    sys.meta_path.remove(sys_importer)


def test__find_packages_with_stubs_in_sys_path_correctly_handles_package_without_stubs_suffix():
    pyspark_stubbed_packages = _find_packages_with_stubs_in_sys_path({"pyspark"})
    expected = frozenset({"pyspark.mllib", "pyspark.sql", "pyspark.sql.pandas"})
    assert pyspark_stubbed_packages.intersection(expected) == expected


def test__find_packages_with_stubs_in_sys_path_with_stubs_correctly_handles_package_with_stubs_suffix():
    pyspark_stubbed_packages = _find_packages_with_stubs_in_sys_path({"pony-stubs"})
    expected = frozenset(
        {
            "pony.flask",
            "pony.orm",
            "pony.orm.integration",
            "pony.orm.dbproviders",
            "pony.thirdparty",
            "pony.utils",
        }
    )
    assert pyspark_stubbed_packages.intersection(expected) == expected


class TestPy4jImportUtils:
    def test__add_or_update_importers_are_unique(self):
        importers = []
        importer1 = _add_or_update(importers, None, None, None)
        assert len(importers) == 1
        importer2 = _add_or_update(importers, None, None, None)
        assert len(importers) == 1
        assert importer1 == importer2

    def test_add_or_update_importers_updating_works(self):
        importers = []
        importer1 = _add_or_update(importers, None, None, None)
        mockedJvm = Mock("jvmView")
        fqn = {"one", "two"}
        importer2 = _add_or_update(importers, mockedJvm, None, py4j_packages_fqn=fqn)
        assert importer1 == importer2
        assert importer2.py4j_jvm == mockedJvm
        assert importer2._valid_py_packages_fqn == fqn


# patch only the method '_resolve_import'
@patch.object(py4j_importer.Py4jImporter, "_resolve_import")
class TestPy4jImporter(unittest.TestCase):
    @pytest.fixture(autouse=True)
    def _remove_imports(self):
        _remove_py4j_importers()
        _remove_from_sys_modules("cern.nxcals")
        yield
        _remove_from_sys_modules("cern.nxcals")
        _remove_py4j_importers()

    def test_find_spec_resolves_py4j_package(self, patched_resolve_import):
        package_name = "py4jgw.cern.nxcals"
        java_fqn = "cern.nxcals"
        jpackage = JavaPackage(java_fqn, None)
        patched_resolve_import.return_value = jpackage
        pi = Py4jImporter(None)
        spec = pi.find_spec(package_name, None)
        assert spec.name == package_name
        assert spec.loader == pi
        assert isinstance(spec, Py4jModuleSpec)
        assert spec.py4j_java_entity == jpackage
        patched_resolve_import.assert_called_once_with(java_fqn)

    def test_find_package_does_not_resolve_modules_already_in_sys_modules(
        self, patched_resolve_import
    ):
        pi = Py4jImporter(None)
        module_name = "unittest"
        spec = pi.find_spec(module_name, None)
        patched_resolve_import.assert_not_called()
        assert spec.name == module_name
        assert spec.has_location

    def test_find_unknown_package_returns_none(self, patched_resolve_import):
        pi = Py4jImporter(None)
        module_name = "unknown_package"
        spec = pi.find_spec(module_name, None)
        assert spec is None
        patched_resolve_import.assert_not_called()

    # @pytest.fixture()
    def test_py4j_importer_with_unknown_package(self, patched_resolve_import):
        with py4j_imports_enabled(None):
            with pytest.raises(ModuleNotFoundError):
                import unknown_package  # type: ignore # noqa

        patched_resolve_import.assert_not_called()

    def test_py4j_importer_not_called_on_package_import_when_py4j_valid_packages_are_declared(
        self, patched_resolve_import
    ):
        with py4j_imports_enabled(
            None, py4j_valid_packages={"py4jgw", "py4jgw.java", "py4jgw.java.util"}
        ):
            import py4jgw.java.util  # type: ignore # noqa
            from py4jgw import java  # noqa

        patched_resolve_import.assert_not_called()

    def test_py4j_importer_not_called_on_package_import__when_stub_library_is_specified(
        self, patched_resolve_import
    ):
        with py4j_imports_enabled(None, py4j_stubs_package_name="pony-stubs"):
            import pony.orm  # noqa

        patched_resolve_import.assert_not_called()


class TestPy4jImporterWithGateway(unittest.TestCase):
    @pytest.fixture(autouse=True)
    def _remove_imports(self):
        _remove_py4j_importers()
        _remove_from_sys_modules("py4jgw")
        yield
        _remove_from_sys_modules("py4jgw")
        _remove_py4j_importers()

    def test_py4j_imports_end_to_end_with_gateway(self):
        self._do_test_py4j_imports_end_to_end_with_gateway(set())

    def test_py4j_imports_end_to_end_with_gateway_and_valid_packages(self):
        self._do_test_py4j_imports_end_to_end_with_gateway(
            {"py4jgw", "py4jgw.java", "py4jgw.java.util"}
        )

    def _do_test_py4j_imports_end_to_end_with_gateway(self, packages: Set[str]):
        with _spark_context_provider() as spark:
            with py4j_imports_enabled(spark._jvm, py4j_valid_packages=packages):
                from py4jgw.java.util import Date  # noqa

                date = Date(60)
                assert str(date) == "Thu Jan 01 01:00:00 CET 1970"

    def test_py4j_imports_lazy_init(self):
        with py4j_imports_enabled(
            None, py4j_valid_packages={"py4jgw", "py4jgw.java", "py4jgw.java.util"}
        ) as py4jimp:
            from py4jgw.java.util import Date  # noqa

            assert isinstance(Date, JavaClassProxy)
            with _spark_context_provider() as spark:
                py4jimp.py4j_jvm = spark._jvm
                date = Date(60)
                assert str(date) == "Thu Jan 01 01:00:00 CET 1970"

    def test_initialization_using_import_statement_instead_of_context_manager(self):
        from tests.import_file_that_initalizes_py4j_imports import py4j_test_importer
        from py4jgw.java.util import Date  # noqa

        with _spark_context_provider() as spark:
            py4j_test_importer.py4j_jvm = spark._jvm
            date = Date(60)
            assert str(date) == "Thu Jan 01 01:00:00 CET 1970"


def test_find_packages_with_stubs_in_sys_path():
    expected = frozenset(
        [
            "pony",
            "pony.flask",
            "pony.orm",
            "pony.orm.integration",
            "pony.orm.dbproviders",
            "pony.thirdparty",
            "pony.utils",
        ]
    )
    pony_stubs = _find_packages_with_stubs_in_sys_path({"pony-stubs"})
    assert expected == pony_stubs


@patch("logging.warning")
def test_find_packages_with_stubs_in_sys_path__gives_warning_if_no_stubs_found(
    mocked_logging_warning,
):
    non_existing_stub_package = "non-existing-stub-package"
    assert not _find_packages_with_stubs_in_sys_path({non_existing_stub_package})
    _assert_called_once_with_string_in_arg(
        mocked_logging_warning, non_existing_stub_package
    )


@patch("logging.warning")
def test_find_packages_with_stubs_in_sys_path__gives_warning_if_one_stub_lib_not_found(
    mocked_logging_warning,
):
    non_existing_stub_package = "non-existing-stub-package"
    assert _find_packages_with_stubs_in_sys_path(
        {"pony-stubs", non_existing_stub_package}
    )
    _assert_called_once_with_string_in_arg(
        mocked_logging_warning, non_existing_stub_package
    )


def _assert_called_once_with_string_in_arg(
    mocked_logging_warning, missing_package: str
):
    assert mocked_logging_warning.called_once()
    warning_msg = mocked_logging_warning.call_args[0][0]
    assert missing_package in warning_msg


def test__to_string_set():
    assert {"foo"} == _to_string_set("foo")
    assert {"foo", "bar"} == _to_string_set(["foo", "bar"])


@pytest.fixture
def tmpdir_path(tmpdir) -> Path:
    return Path(tmpdir)


def test__find_stub_package_fqns(tmpdir_path: Path):
    my_stubs_dir = tmpdir_path / "my-stubs"
    (my_stubs_dir / "a/b/c").mkdir(parents=True, exist_ok=True)  # subdir c is empty
    (my_stubs_dir / "a/b/dummy_file").write_text("")
    fqns = _find_stub_package_fqns(my_stubs_dir)
    expected_fqn_dirs = {"my", "my.a", "my.a.b"}
    assert fqns == expected_fqn_dirs
