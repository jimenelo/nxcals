# this is an example of a file that a package provider should create to simplify the use of py4jimporter
#
from py4j_utils.py4j_importer import enable_py4j_imports

# this line is executed when this file is imported. It initializes the py4j import mechanism
# in this case we initialize it with package names that we use in the tests
py4j_test_importer = enable_py4j_imports(
    None,
    importer_unique_name="py4j_utils_tests",
    py4j_stubs_package_names=None,
    py4j_valid_packages={"py4jgw", "py4jgw.java", "py4jgw.java.util"},
)
