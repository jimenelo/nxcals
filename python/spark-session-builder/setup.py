import os
import setuptools

nxcals_version = os.environ["NXCALS_VERSION"]
spark_version = os.environ["SPARK_VERSION"]
spark_home_dir = os.environ["SPARK_HOME_DIR"]


def dir_files(dirpath, files):
    bundle_dir = os.path.relpath(path=dirpath, start=spark_home_dir)
    subdir_in_venv = os.path.join("nxcals-bundle", bundle_dir)
    files_absolute = (os.path.join(dirpath, file) for file in files)
    return (subdir_in_venv, files_absolute)


bundle_files = (
    dir_files(dirpath, files) for dirpath, _, files in os.walk(spark_home_dir)
)

setuptools.setup(
    name="nxcals-spark-session-builder",
    version=nxcals_version,
    description="NXCALS PySpark bundle",
    long_description="NXCALS PySpark bundle helps to setup PySpark for NXCALS",
    author="NXCALS Team (BE-CSS-CPA)",
    author_email="acc-logging-support@cern.ch",
    url="https://confluence.cern.ch/display/NXCALS",
    packages=("nxcals.spark_session_builder",),
    data_files=(("bin", ("find_spark_home.py",)), *bundle_files),
    install_requires=(f"pyspark=={spark_version}", "pyhocon~=0.3", "venv-pack~=0.2.0"),
    extras_require={},
    python_requires=">= 3.9",
    classifiers=[
        "Programming Language :: Python :: 3.9",
        "Programming Language :: Python :: 3.11",
    ],
)
