from py4j.java_collections import register_input_converter
from pyspark.sql import SparkSession


class SparkSessionConverter:
    @staticmethod
    def can_convert(obj):
        return isinstance(obj, SparkSession)

    @staticmethod
    def convert(session: SparkSession, gateway_client):
        return ProxyNonDetachable(session._jsparkSession)


class ProxyNonDetachable:
    def __init__(self, object):
        self._object = object

    def __getattr__(self, item):
        if item == "_detach":
            raise AttributeError()
        return getattr(self._object, item)


def register_spark_session_converter():
    register_input_converter(SparkSessionConverter)
