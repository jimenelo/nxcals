from typing import Any

import pytest
import numpy as np
from pytimber.data import DataManager
from numpy import ndarray


class TestData:
    @pytest.mark.parametrize(
        "value, expected_output",
        [
            ([[1, 2, 3, 4, 5, 6], [2, 3]], [[1, 2, 3], [4, 5, 6]]),
            ([[1, 2, 3], [3, 1]], [[1], [2], [3]]),
        ],
    )
    def test_extract_matrix_value(
        self, value: ndarray[Any, Any], expected_output: ndarray[Any, Any]
    ) -> None:
        result = DataManager._extract_matrix_value(value)
        assert np.array_equal(result, expected_output)

    @pytest.mark.parametrize(
        "raw_values, is_value_array, expected_output",
        [
            (np.array([]), False, np.array([])),
            (np.array([]), True, np.array([])),
        ],
    )
    def test_extract_empty_values(
        self,
        raw_values: ndarray[Any, Any],
        is_value_array: bool,
        expected_output: ndarray[Any, Any],
    ) -> None:
        result = DataManager._extract_array_values(raw_values)
        assert np.array_equal(result, expected_output)

    @pytest.mark.parametrize(
        "raw_values, is_value_array, expected_output",
        [
            (
                np.array(
                    [
                        np.array([np.array([1, 2, 3]), np.array([3])], dtype=object),
                        np.array([np.array([1, 2, 3, 4]), np.array([4])], dtype=object),
                    ],
                    dtype=object,
                ),
                True,
                np.array(
                    [
                        np.array([1, 2, 3], dtype=object),
                        np.array([1, 2, 3, 4], dtype=object),
                    ],
                    dtype=object,
                ),
            ),
            (
                np.array(
                    [
                        np.array(
                            [np.array([1, 2, 3, 4, 5, 6]), np.array([2, 3])],
                            dtype=object,
                        ),
                        np.array(
                            [np.array([1, 2, 3, 4, 5, 6, 7, 8]), np.array([4, 2])],
                            dtype=object,
                        ),
                    ],
                    dtype=object,
                ),
                True,
                [[[1, 2, 3], [4, 5, 6]], [[1, 2], [3, 4], [5, 6], [7, 8]]],
            ),
        ],
    )
    def test_extract_values(
        self,
        raw_values: ndarray[Any, Any],
        is_value_array: bool,
        expected_output: ndarray[Any, Any],
    ) -> None:
        result = DataManager._extract_array_values(raw_values)

        import numpy.testing as npt

        for arr1, arr2 in zip(result, expected_output):
            npt.assert_array_equal(arr1, arr2)
