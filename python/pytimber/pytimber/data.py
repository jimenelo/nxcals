import logging
import warnings
from datetime import datetime
from enum import Enum, IntEnum, auto
from functools import lru_cache
from itertools import groupby
from typing import Any, Dict, List, Optional, Tuple, Union, Generic, TypeVar

import abc

import numpy as np
import pandas as pd
from numpy import ndarray
from nxcals.api.extraction.data.builders import DataQuery
from pandas.core.series import Series
from pyspark.sql import DataFrame, SparkSession

from .fundamentals import FundamentalsManager, FundamentalsType
from .utils import (
    AGGREGATION_TIMESTAMP_FIELD,
    AGGREGATION_VALUE_FIELD,
    TIMESTAMP,
    VALUE,
    VARIABLE_NAME,
    ValueArray,
)
from .utils import (
    PyTimberTimeType,
    PatternOrList,
    VariableDataFrameResultType,
    VariableQueryResultType,
    get_nanos_from,
    to_time_window,
    is_value_array,
)
from .variable import VariableManager

TIMESTAMPS_KEY = "timestamps"
T = TypeVar("T")


class AutoName(Enum):
    @staticmethod
    def _generate_next_value_(
        name: str, start: int, count: int, last_values: List[Any]
    ) -> Any:
        return name


class ScaleAlgorithm(AutoName):
    MAX = auto()
    MIN = auto()
    AVG = auto()
    COUNT = auto()
    SUM = auto()
    REPEAT = auto()
    INTERPOLATE = auto()

    @staticmethod
    def value_of(value: str) -> "ScaleAlgorithm":
        if value in ScaleAlgorithm.__members__:
            return ScaleAlgorithm[value]

        raise ValueError(
            f"Could not find ScaleAlgorithm {value} among {[d for d in ScaleAlgorithm]}",
        )


class ScaleInterval(IntEnum):
    SECOND = 1
    MINUTE = 60 * SECOND
    HOUR = 60 * MINUTE
    DAY = 24 * HOUR
    WEEK = 7 * DAY
    MONTH = 30 * DAY
    YEAR = 365 * DAY

    @staticmethod
    def value_of(value: str) -> "ScaleInterval":
        if value in ScaleInterval.__members__:
            return ScaleInterval[value]

        raise ValueError(
            f"Could not find ScaleInterval {value} among {[d for d in ScaleInterval]}",
        )


class _Strategy(Enum):
    NEXT = "next"
    LAST = "last"


class DataConverter(Generic[T]):
    @abc.abstractmethod
    def convert_from_get(
        self,
        dataset_dict: Dict[Tuple[str], VariableDataFrameResultType],
        unix_time: bool,
    ) -> Dict[str, T]:
        pass

    @abc.abstractmethod
    def convert_from_get_aligned(
        self,
        dataset_dict: Dict[str, VariableDataFrameResultType],
        master_name: str,
        variables: List[Any],
    ) -> Dict[str, T]:
        pass

    @abc.abstractmethod
    def convert_from_get_scaled(
        self, dataset_dict: Dict[str, VariableDataFrameResultType], unix_time: bool
    ) -> Dict[str, T]:
        pass

    @abc.abstractmethod
    def convert_from_get_as_pivot(
        self,
        dataset: DataFrame,
        unix_time: bool,
    ) -> T:
        pass


class SparkDataConverter(DataConverter[VariableDataFrameResultType]):
    def convert_from_get(
        self,
        dataset_dict: Dict[Tuple[str], VariableDataFrameResultType],
        unix_time: bool,
    ) -> Dict[str, VariableDataFrameResultType]:
        result = {}

        for variables in dataset_dict:
            grouped_ds = dataset_dict[variables]

            for variable_name in variables:
                variable_ds = grouped_ds.filter(
                    grouped_ds.nxcals_variable_name == variable_name
                )
                result[variable_name] = variable_ds
        return result

    def convert_from_get_aligned(
        self,
        dataset_dict: Dict[str, VariableDataFrameResultType],
        master_name: str,
        variables: List[Any],
    ) -> Dict[str, VariableDataFrameResultType]:
        return dataset_dict

    def convert_from_get_scaled(
        self,
        dataset_dict: Dict[str, VariableDataFrameResultType],
        unix_time: bool,
    ) -> Dict[str, VariableDataFrameResultType]:
        return dataset_dict

    def convert_from_get_as_pivot(
        self,
        dataset: DataFrame,
        unix_time: bool,
    ) -> DataFrame:
        return dataset


class NumpyDataConverter(DataConverter[VariableQueryResultType]):
    def convert_from_get(
        self,
        dataset_dict: Dict[Tuple[str], VariableDataFrameResultType],
        unix_time: bool,
    ) -> Dict[str, VariableQueryResultType]:
        result: Dict[str, VariableQueryResultType] = {}

        for variables in dataset_dict:
            ret = (
                self._process_dataset(
                    variables, dataset_dict[variables].sort(TIMESTAMP), unix_time
                )
                if dataset_dict[variables].sort(TIMESTAMP)
                else {}
            )
            result = {**result, **ret}
        return result

    def convert_from_get_aligned(
        self,
        dataset_dict: VariableDataFrameResultType,
        master_name: str,
        variables: List[Any],
    ) -> Dict[str, VariableQueryResultType]:
        if not dataset_dict or dataset_dict[TIMESTAMPS_KEY].isEmpty():
            return {}

        pandas_df_timestamps = dataset_dict[TIMESTAMPS_KEY].select(TIMESTAMP).toPandas()
        ret = {
            TIMESTAMPS_KEY: DataManager._timestamps_to_datetime_np_array(
                pandas_df_timestamps[TIMESTAMP], True
            ),
            master_name: dataset_dict[master_name]
            .select(
                f"{VALUE}.elements",
                f"{VALUE}.dimensions",
            )
            .toPandas()
            .toNumpy()
            if is_value_array(dataset_dict[master_name].schema, VALUE)
            else dataset_dict[master_name].select(VALUE).toPandas()[VALUE].to_numpy(),
        }

        for var in variables:
            if master_name != var.getVariableName():
                ret[var.getVariableName()] = (
                    self._filter_by_variable(dataset_dict, var)
                    .select(
                        f"{AGGREGATION_VALUE_FIELD}.elements",
                        f"{AGGREGATION_VALUE_FIELD}.dimensions",
                    )
                    .toPandas()
                    .to_numpy()
                    if is_value_array(
                        self._filter_by_variable(dataset_dict, var).schema,
                        AGGREGATION_VALUE_FIELD,
                    )
                    else self._filter_by_variable(dataset_dict, var)
                    .select(AGGREGATION_VALUE_FIELD)
                    .toPandas()[AGGREGATION_VALUE_FIELD]
                    .to_numpy()
                )

        return ret

    def convert_from_get_scaled(
        self, dataset_dict: Dict[str, VariableDataFrameResultType], unix_time: bool
    ) -> Dict[str, VariableQueryResultType]:
        ret = {}

        for var_name in dataset_dict:
            ts_val = dataset_dict[var_name].toPandas()

            ret[var_name] = (
                DataManager._timestamps_to_datetime_np_array(
                    ts_val.timestamp, unix_time
                ),
                ts_val.value.to_numpy(),
            )
        return ret

    def convert_from_get_as_pivot(
        self,
        dataset: DataFrame,
        unix_time: bool,
    ) -> Dict[int, Dict[str, Any]]:
        return dataset.rdd.map(
            lambda row: (
                row.nxcals_timestamp,
                {k: row[k] for k in row.asDict() if k != "nxcals_timestamp"},
            )
        ).collectAsMap()

    def _process_dataset(
        self, variables: Tuple[str], ds: DataFrame, unix_time: bool
    ) -> Dict[str, VariableQueryResultType]:
        is_array = is_value_array(ds.schema)

        if is_array:
            vds = ds.select(
                TIMESTAMP,
                f"{VALUE}.elements",
                f"{VALUE}.dimensions",
                "nxcals_variable_name",
            )
        else:
            vds = ds.select(TIMESTAMP, VALUE, "nxcals_variable_name")

        pandas_df = vds.toPandas()
        variable_names_series = pandas_df[VARIABLE_NAME]

        result = {}

        for variable_name in variables:
            variable_ds = pandas_df.loc[variable_names_series == variable_name]

            if not variable_ds.empty:
                if is_array:
                    values = DataManager._extract_array_values(
                        variable_ds[["elements", "dimensions"]].to_numpy()
                    )
                else:
                    values = variable_ds[VALUE].to_numpy()

                result[variable_name] = (
                    DataManager._timestamps_to_datetime_np_array(
                        variable_ds[TIMESTAMP], unix_time
                    ),
                    values,
                )
        return result

    def _filter_by_variable(
        self, data_dict: VariableDataFrameResultType, variable: Any
    ) -> VariableDataFrameResultType:
        return data_dict[variable.getVariableName()]


class DataManager:
    def __init__(self, spark: SparkSession):
        self._spark = spark
        self._variable_manager = VariableManager(spark)
        self._funds_manager = FundamentalsManager(spark)
        self._agg_service = DataManager._init_agg_service(
            spark,
            self._funds_manager.fund_service,
        )
        self._agg_mappings = DataManager._init_agg_mappings(spark)

    def _get_as_dataset(
        self,
        pattern: PatternOrList,
        t1: PyTimberTimeType,
        t2: Optional[Union[PyTimberTimeType, str]] = None,
        fundamental: Optional[FundamentalsType] = None,
    ) -> Dict[Tuple[str], VariableDataFrameResultType]:
        result = {}

        if not self._is_valid_time_window(fundamental, t2):
            logging.warning(
                "Unsupported: if filtering by fundamentals you must provide a correct time window",
            )
            return {}

        all_variables = self._variable_manager.get_variables(pattern)

        vars_by_type = groupby(all_variables, lambda v: v.declared_type)

        for _, variables in vars_by_type:
            variable_list = [var.name for var in list(variables)]

            if not t2 or t2 == _Strategy.LAST.value:
                ds = self._get_lookup_data(variable_list, t1, _Strategy.LAST)
            elif t2 == _Strategy.NEXT.value:
                ds = self._get_lookup_data(variable_list, t1, _Strategy.NEXT)
            else:
                ds = self._get_dataset_for(variable_list, t1, t2, fundamental)

            ret = {tuple(variable_list): ds} if ds else {}

            result = {**result, **ret}

        return result

    def _get_aligned_as_dataset(
        self,
        pattern: PatternOrList,
        t1: PyTimberTimeType,
        t2: PyTimberTimeType,
        fundamental: Optional[FundamentalsType] = None,
        master: Optional[str] = None,
    ) -> Dict[str, VariableDataFrameResultType]:
        master_name, variables = self._get_master_and_variables_for(
            pattern,
            master,
        )
        master_ds = self._get_dataset_for(
            master_name,
            t1,
            t2,
            fundamental,
            to_df=False,
        )
        if not master_ds or master_ds.isEmpty():
            return {}

        master_df = DataFrame(master_ds, self._spark).sort([TIMESTAMP])
        ret: Dict[str, VariableDataFrameResultType] = {
            TIMESTAMPS_KEY: master_df.select(TIMESTAMP),
            master_name: master_df,
        }

        props = self._get_aligned_properties(master_ds)
        for var in variables:
            if master_name != var.getVariableName():
                ds = self._agg_service.getData(var, props)
                df = DataFrame(ds, self._spark)

                df = df.sort(AGGREGATION_TIMESTAMP_FIELD)
                ret[var.getVariableName()] = df

        return ret

    def _get_scaled_as_dataset(
        self,
        pattern: PatternOrList,
        t1: PyTimberTimeType,
        t2: PyTimberTimeType,
        scale_algorithm: ScaleAlgorithm = ScaleAlgorithm.SUM,
        scale_interval: ScaleInterval = ScaleInterval.MINUTE,
        scale_size: int = 1,
    ) -> Dict[str, VariableDataFrameResultType]:
        variables = self._variable_manager.get_nxcals_variables(pattern)
        if not variables:
            raise ValueError(f"No variables found for given {pattern}")

        props = self._get_scaled_properties(
            t1,
            t2,
            scale_algorithm,
            scale_interval,
            scale_size,
        )
        ret: Dict[str, VariableDataFrameResultType] = {}
        for var in variables:
            var_name = var.getVariableName()
            ds = self._agg_service.getData(var, props)

            ret[var_name] = DataFrame(ds, self._spark).select(
                AGGREGATION_TIMESTAMP_FIELD, AGGREGATION_VALUE_FIELD
            )
        return ret

    def _get_pivot_as_dataset(
        self,
        pattern: PatternOrList,
        t1: PyTimberTimeType,
        t2: Optional[Union[PyTimberTimeType, str]] = None,
    ) -> DataFrame:
        variables = self._variable_manager.get_nxcals_variables(pattern)
        if not variables:
            raise ValueError(f"No variables found for given {pattern}")

        return DataQuery.getAsPivot(self._spark, t1, t2, variables)

    # ############################### private stuff #############################################

    @staticmethod
    def _timestamps_to_datetime_np_array(
        raw_timestamps: Series, unix_time: bool
    ) -> ndarray[Any, Any]:
        with warnings.catch_warnings():
            warnings.simplefilter("ignore", FutureWarning)
            datetimes_as_np_array = np.array(
                pd.to_datetime(raw_timestamps, unit="ns", utc=True).dt.to_pydatetime()
            )

        if unix_time:
            timestamps = np.array([x.timestamp() for x in datetimes_as_np_array])
        else:
            timestamps = datetimes_as_np_array
        return timestamps

    def _get_dataset_for(
        self,
        pattern: PatternOrList,
        t1: PyTimberTimeType,
        t2: PyTimberTimeType,
        fundamental: Optional[FundamentalsType] = None,
        to_df: bool = True,
    ) -> Optional[Union[DataFrame, Any]]:
        all_variables = self._variable_manager.get_variables(pattern)

        if not all_variables:
            return None

        vars_by_system = groupby(all_variables, lambda v: v.system)
        ds: Optional[Union[DataFrame, Any]] = None
        for system, variables in vars_by_system:
            ds_by_system = (
                DataQuery.builder(self._spark)
                .variables()
                .system(system)
                .nameIn(list(map(lambda v: v.name, variables)))
                .timeWindow(get_nanos_from(t1), get_nanos_from(t2))
                .build(to_df)
            )
            ds = ds.union(ds_by_system) if ds else ds_by_system

        if fundamental and ds:
            f_ds = self._funds_manager.get_fundamentals_ds(
                t1,
                t2,
                fundamental,
                to_df,
            )
            return ds.join(f_ds.drop(VARIABLE_NAME), TIMESTAMP)

        return ds

    @staticmethod
    def _extract_array_values(
        raw_values: ndarray[Any, Any],
    ) -> Union[ndarray[Any, Any], List[ndarray[Any, Any]]]:
        if len(raw_values) == 0:
            return []

        dims = len(raw_values[0][1])

        if dims == 1:
            return raw_values[:, 0]
        elif dims == 2:
            return [DataManager._extract_matrix_value(value) for value in raw_values]

        raise ValueError(
            f"Max 2D arrays are supported, obtained {dims}D array",
        )

    @staticmethod
    def _extract_matrix_value(value: ndarray[Any, Any]) -> ndarray[Any, Any]:
        data = value[0]
        rows, cols = value[1]

        return np.reshape(data, [rows, cols])

    def _is_valid_time_window(
        self,
        fundamental: Optional[FundamentalsType] = None,
        t2: Optional[PyTimberTimeType] = None,
    ) -> bool:
        if t2 in (_Strategy.NEXT.value, _Strategy.LAST.value, None) and fundamental:
            return False
        return True

    def _get_lookup_data(
        self, pattern: PatternOrList, t1: PyTimberTimeType, strategy: _Strategy
    ) -> Optional[DataFrame]:
        service = self._spark._jvm.cern.nxcals.api.custom.service.extraction.ExtractionServiceFactory.getInstance(
            self._spark._jsparkSession,
        )
        tw = to_time_window(self._spark, t1, datetime.now())
        props = self._create_extraction_properties(tw, strategy)

        ds: Optional[Any] = None
        for var in self._variable_manager.get_nxcals_variables(pattern):
            df = service.getData(var, props)
            ds = df if not ds else ds.union(df)

        if ds:
            return DataFrame(ds, self._spark)
        return None

    def _create_extraction_properties(
        self, java_time_window: Any, strategy: _Strategy
    ) -> Any:
        extraction = self._spark._jvm.cern.nxcals.api.custom.service.extraction

        builder = extraction.ExtractionProperties.builder()
        ls = self._get_strategy_for(extraction, strategy)

        return (
            builder.timeWindow(
                java_time_window.getStartTime(),
                java_time_window.getEndTime(),
            )
            .lookupStrategy(ls)
            .build()
        )

    def _get_strategy_for(self, extraction: Any, strategy: _Strategy) -> Any:
        if strategy == _Strategy.LAST:
            return extraction.LookupStrategy.LAST_BEFORE_START_ONLY
        elif strategy == _Strategy.NEXT:
            return extraction.LookupStrategy.NEXT_AFTER_START_ONLY

        raise ValueError(f"{strategy} unknown strategy")

    def _get_master_and_variables_for(
        self,
        pattern: PatternOrList,
        master: Optional[str] = None,
    ) -> Tuple[str, List[Any]]:
        variables = self._variable_manager.get_nxcals_variables(pattern)
        if not variables:
            raise ValueError(f"No variables found for given {pattern}")

        master_var = (
            self._variable_manager.get_nxcals_variables(master)[0]
            if master
            else sorted(variables, key=lambda v: v.getVariableName())[0]
        )

        if not master_var:
            raise ValueError("Cannot obtain a master variable")

        return master_var.getVariableName(), variables

    def _get_aligned_properties(self, master_ds: Any) -> Any:
        builder = self._spark._jvm.cern.nxcals.api.custom.service.aggregation.DatasetAggregationProperties.builder()
        return builder.drivingDataset(master_ds).build()

    def _get_scaled_properties(
        self,
        t1: PyTimberTimeType,
        t2: PyTimberTimeType,
        scale_algorithm: ScaleAlgorithm,
        scale_interval: ScaleInterval,
        scale_size: int,
    ) -> Any:
        builder = self._spark._jvm.cern.nxcals.api.custom.service.aggregation.WindowAggregationProperties.builder()
        builder.function(self._agg_mappings[scale_algorithm]).timeWindow(
            to_time_window(self._spark, t1, t2)
        ).interval(
            scale_interval.value * scale_size,
            self._spark._jvm.java.time.temporal.ChronoUnit.SECONDS,
        )

        return builder.build()

    @staticmethod
    def _init_agg_service(spark: SparkSession, fund_service: Any) -> Any:
        factory = spark._jvm.cern.nxcals.api.custom.service.aggregation.AggregationServiceFactory
        return factory.getInstance(spark._jsparkSession, fund_service)

    @staticmethod
    @lru_cache(maxsize=None)
    # if the function is called multiple times with the same spark argument, the cached result will be returned
    # instead of recomputing the mappings
    def _init_agg_mappings(spark: SparkSession) -> Dict[ScaleAlgorithm, Any]:
        package = spark._jvm.cern.nxcals.api.custom.service.aggregation
        return {
            ScaleAlgorithm.SUM: package.AggregationFunctions.SUM,
            ScaleAlgorithm.MIN: package.AggregationFunctions.MIN,
            ScaleAlgorithm.MAX: package.AggregationFunctions.MAX,
            ScaleAlgorithm.COUNT: package.AggregationFunctions.COUNT,
            ScaleAlgorithm.AVG: package.AggregationFunctions.AVG,
            ScaleAlgorithm.REPEAT: package.AggregationFunctions.REPEAT,
            ScaleAlgorithm.INTERPOLATE: package.AggregationFunctions.INTERPOLATE,
        }
