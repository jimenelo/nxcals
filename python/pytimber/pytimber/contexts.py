from typing import Any, List, Tuple

from nxcals.api.extraction.data.builders import DataQuery
from pyspark.sql import SparkSession
from .utils import (
    PyTimberTimeType,
    get_nanos_from,
    nanos_to_timestamp,
    TIMESTAMP,
    VALUE,
)
from .variable import Variable


class ContextManager:
    def __init__(self, spark: SparkSession):
        self.spark = spark

    def get_metadata_for(
        self,
        var: Variable,
        from_time: PyTimberTimeType,
        to_time: PyTimberTimeType,
    ) -> List[Tuple[float, Any]]:
        var_prefix = "NXCALS_CONTEXT:"
        var_name = var_prefix + str(var.id)
        ds = (
            DataQuery.builder(self.spark)
            .variables()
            .system("CMW")
            .nameEq(var_name)
            .timeWindow(get_nanos_from(from_time), get_nanos_from(to_time))
            .build()
        )
        return [
            (nanos_to_timestamp(row[TIMESTAMP]), row[VALUE])
            for row in ds.sort(TIMESTAMP).collect()
        ]
