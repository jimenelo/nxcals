import logging
from typing import Any, Iterable, List, Set

from pyspark.sql import SparkSession
from pytimber.utils import DEFAULT_HIERARCHY_NXCALS_SYSTEM, PatternOrList


class HierarchyManager:
    def __init__(self, spark: SparkSession):
        self.spark = spark
        self._metadata = self.spark._jvm.cern.nxcals.api.extraction.metadata
        self._hierarchy_service = (
            self._metadata.ServiceClientFactory.createHierarchyService()
        )

    def get_hierarchies(
        self,
        pattern_or_list: PatternOrList,
        system: str = DEFAULT_HIERARCHY_NXCALS_SYSTEM,
    ) -> List[Any]:
        logging.info(f"Getting hierarchies (pattern: {pattern_or_list})")

        hierarchies = self._metadata.queries.Hierarchies

        if isinstance(pattern_or_list, str):
            return list(
                self._hierarchy_service.findAll(
                    getattr(hierarchies.suchThat().systemName().eq(system), "and")()
                    .path()
                    .like(pattern_or_list),
                ),
            )
        elif isinstance(pattern_or_list, Iterable):
            return list(
                self._hierarchy_service.findAll(
                    getattr(
                        getattr(
                            hierarchies.suchThat().systemName().eq(system), "and"
                        )().path(),
                        "in",
                    )(pattern_or_list),
                ),
            )
        else:
            raise ValueError(f"{pattern_or_list} neither pattern nor list")

    def get_variables(self, hierarchy_id: int) -> Set[Any]:
        return set(self._hierarchy_service.getVariables(hierarchy_id))

    def get_hierarchies_for_variable(self, variable_id: int) -> Set[Any]:
        return set(self._hierarchy_service.getHierarchiesForVariable(variable_id))
