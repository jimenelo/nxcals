import json
from datetime import datetime, timezone
from typing import Any, Dict, Iterable, List, NamedTuple, Optional, Tuple

from dateutil.relativedelta import relativedelta
from pyspark.sql import SparkSession
from pytimber import utils
from pytimber.utils import (
    TIME_FORMAT,
    PatternOrList,
    PyTimberTimeType,
)


class _SnapshotSearchCriteria(NamedTuple):
    fundamental_pattern: Optional[str] = None
    start_time: Optional[PyTimberTimeType] = None
    end_time: Optional[PyTimberTimeType] = None

    def get_search_criteria(
        self,
    ) -> Tuple[Optional[str], Optional[PyTimberTimeType], Optional[PyTimberTimeType]]:
        return (self.fundamental_pattern, self.start_time, self.end_time)


class SnapshotManager:
    def __init__(self, spark: SparkSession):
        self.spark = spark
        self._nxcals_metadata_api = self.spark._jvm.cern.nxcals.api.extraction.metadata
        self._nxcals_custom_api = self.spark._jvm.cern.nxcals.api.custom
        self._group_service = (
            self._nxcals_metadata_api.ServiceClientFactory.createGroupService()
        )

    def get_snapshots(
        self,
        pattern_or_list: PatternOrList,
        owner_pattern: Optional[str] = None,
        description_pattern: Optional[str] = None,
    ) -> List[Any]:
        groups = self._nxcals_metadata_api.queries.Groups
        group_type = self._nxcals_custom_api.domain.GroupType

        if isinstance(pattern_or_list, str):
            snap_condition = (
                getattr(
                    groups.suchThat().label().eq(str(group_type.SNAPSHOT)),
                    "and",
                )()
                .name()
                .like(pattern_or_list)
            )
        elif isinstance(pattern_or_list, Iterable):
            snap_condition = getattr(
                getattr(
                    groups.suchThat().label().eq(str(group_type.SNAPSHOT)), "and"
                )().name(),
                "in",
            )(pattern_or_list)
        else:
            raise ValueError(f"{pattern_or_list} neither pattern nor list")

        if owner_pattern:
            snap_condition = (
                getattr(
                    snap_condition,
                    "and",
                )()
                .ownerName()
                .like(owner_pattern)
            )

        if description_pattern is not None:
            snap_condition = (
                getattr(
                    snap_condition,
                    "and",
                )()
                .description()
                .like(description_pattern)
            )

        return list(self._group_service.findAll(snap_condition))

    def get_variables(self, snapshot: Any) -> Any:
        group_property_name = self._nxcals_custom_api.domain.GroupPropertyName

        return self._group_service.getVariables(
            snapshot.getId(),
        )[group_property_name.getSelectedVariables.toString()]

    # Do not rename parameters. They have to match values stored in the database.
    # python:S117 Local variable and function parameter names should comply with a naming convention
    def create_snapshot_search_criteria(
        self,
        fillNumber: Optional[str] = None,  # NOSONAR
        isEndTimeDynamic: Optional[str] = None,  # NOSONAR
        getTimeZone: Optional[str] = None,  # NOSONAR
        beamModeStart: Optional[str] = None,  # NOSONAR
        beamModeEnd: Optional[str] = None,  # NOSONAR
        getPriorTime: Optional[str] = None,  # NOSONAR
        getTime: Optional[str] = None,  # NOSONAR
        getDynamicDuration: Optional[str] = None,  # NOSONAR
        getStartTime: Optional[str] = None,  # NOSONAR
        getEndTime: Optional[str] = None,  # NOSONAR
        fundamentalFilter: Optional[str] = None,  # NOSONAR
        **kwargs: Any,
    ) -> _SnapshotSearchCriteria:
        """
        Please note that:
        fillNumber attribute is mutually exclusive with isEndTimeDynamic since they represent 2 different ways of
        specyfing the timew indow: "by fill" vs "dynamic". If both are not present, a search with a fixed time window
        will be performed.
        The fundamentalFilter attribute is independent of the search method.
        """

        # Note: accessing kwargs from this function is not encouraged in order to keep the signature readable.

        if fillNumber is not None:
            t1, t2 = SnapshotManager._get_range_based_on_fills(
                beamModeStart, beamModeEnd
            )
        elif isEndTimeDynamic is not None:
            if isEndTimeDynamic == "true":
                if getDynamicDuration is None:
                    raise ValueError("getDynamicDuration cannot be None")

                now = (
                    datetime.now(timezone.utc)
                    if getTimeZone == "UTC_TIME"
                    else datetime.now()
                )

                t1, t2 = SnapshotManager._get_dynamic_range(
                    now,
                    getPriorTime,
                    getTime,
                    int(getDynamicDuration),
                )
            else:
                # Get fixed range
                t1 = utils.get_nanos_from(
                    SnapshotManager._to_datetime(getTimeZone, getStartTime)
                )
                t2 = utils.get_nanos_from(
                    SnapshotManager._to_datetime(getTimeZone, getEndTime)
                )
        else:
            raise ValueError(
                "Neither 'fillNumber' nor 'isEndTimeDynamic' is present in the snapshot attributes"
            )

        fundamental_pattern: Optional[str] = None
        if fundamentalFilter is not None:
            fund_filter = json.loads(fundamentalFilter)
            fundamental_pattern = ":".join(
                [
                    fund_filter["accelerator"],
                    fund_filter["lsaCycle"],
                    fund_filter["timingUser"],
                ],
            )

        return _SnapshotSearchCriteria(fundamental_pattern, t1, t2)

    def get_snapshot_names(
        self,
        pattern_or_list: PatternOrList,
        owner_pattern: str = "%",
        description_pattern: str = "%",
    ) -> List[str]:
        return [
            snapshot.getName()
            for snapshot in self.get_snapshots(
                pattern_or_list,
                owner_pattern,
                description_pattern,
            )
        ]

    def get_variables_for_snapshots(
        self,
        pattern_or_list: PatternOrList,
        owner_pattern: str = "%",
    ) -> Dict[str, List[str]]:
        result = {}

        snapshots = self.get_snapshots(
            pattern_or_list,
            owner_pattern,
        )

        for snapshot in snapshots:
            variables = self.get_variables(snapshot)
            if variables:
                result[snapshot.getName()] = [
                    variable.getVariableName() for variable in variables
                ]

        return result

    @staticmethod
    def _get_range_based_on_fills(
        beam_mode_start: Optional[str], beam_mode_end: Optional[str]
    ) -> Tuple[int, int]:
        if beam_mode_start is None:
            raise ValueError("beamModeStart cannot be None")
        if beam_mode_end is None:
            raise ValueError("beamModeEnd cannot be None")

        t1 = utils.get_nanos_from(
            SnapshotManager._adjust_timestamp_format(
                json.loads(beam_mode_start)["validity"]["startTime"]
            ),
        )
        t2 = utils.get_nanos_from(
            SnapshotManager._adjust_timestamp_format(
                json.loads(beam_mode_end)["validity"]["endTime"]
            ),
        )
        return t1, t2

    @staticmethod
    def _get_dynamic_range(
        ref_time: datetime,
        prior_time: Optional[str],
        time_unit: Optional[str],
        dynamic_duration: int,
    ) -> Tuple[int, int]:
        if prior_time is None:
            raise ValueError("prior_time cannot be None")

        if time_unit is None:
            raise ValueError("time_unit cannot be None")

        if prior_time == "Now":
            t2 = ref_time
        elif prior_time == "Start of hour":
            t2 = ref_time.replace(microsecond=0, second=0, minute=0)
        elif prior_time == "Start of day":
            t2 = ref_time.replace(microsecond=0, second=0, minute=0, hour=0)
        elif prior_time == "Start of month":
            t2 = ref_time.replace(microsecond=0, second=0, minute=0, hour=0, day=1)
        elif prior_time == "Start of year":
            t2 = ref_time.replace(
                microsecond=0, second=0, minute=0, hour=0, day=1, month=1
            )
        else:
            raise ValueError(prior_time + " not supported as a prior time")

        # Dynamic timewindow
        args: Any = {time_unit.lower(): dynamic_duration}
        t1 = t2 - relativedelta(**args)

        return utils.get_nanos_from(t1), utils.get_nanos_from(t2)

    @staticmethod
    def _to_datetime(time_zone: Optional[str], ts_str: Optional[str]) -> datetime:
        if time_zone is None:
            raise ValueError("Time zone cannot be None")

        if ts_str is None:
            raise ValueError("Timestamp string cannot be None")

        if time_zone == "UTC_TIME":
            utc_time = datetime.strptime(ts_str, TIME_FORMAT)
            return utc_time.replace(tzinfo=timezone.utc)

        return datetime.strptime(ts_str, TIME_FORMAT)

    @staticmethod
    def _adjust_timestamp_format(ts: str) -> str:
        return ts.replace("T", " ").replace("Z", "")
