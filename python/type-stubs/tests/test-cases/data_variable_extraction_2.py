import py4jgw.java.util
from py4jgw.cern.nxcals.api.domain import TimeWindow
from py4jgw.cern.nxcals.api.extraction.data.builders import DataQuery
from pyspark.sql import SparkSession

spark_session: SparkSession

timeWindow = TimeWindow.before(1000)

variables: py4jgw.java.util.List
(
    DataQuery.getFor(
        spark_session._jsparkSession,
        timeWindow=timeWindow,
        system="CMW",
        variables=variables,  # problem - typing requires explicit conversion to Java list
    )
)
