from py4jgw.cern.nxcals.api.ingestion import PublisherFactory
from py4jgw.java.util.function import Function

PublisherFactory.newInstance().createPublisher("CMW", Function.identity())
