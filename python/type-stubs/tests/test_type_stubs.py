import pytest

from . import should_compile, should_fail
import os


def get_test_files():
    folder_with_test_cases = "tests/test-cases"
    files = os.listdir(folder_with_test_cases)
    for file in files:
        yield f"{folder_with_test_cases}/{file}"


@pytest.mark.parametrize("file_path", get_test_files())
def test_codes_which_should_compile(file_path):
    with open(file_path) as fh:
        should_compile(fh.read())


def test_invalid_code_is_detected():
    should_fail(
        "from py4jgw.cern.nxcals.api.extraction.metadata import non_existing_moduley"
    )
